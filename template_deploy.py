from __future__ import print_function
import sys
import boto3
import os
import mimetypes
from botocore.exceptions import ClientError
from time import time


def update_bucket(bucket_name, folder):
    try:
        #Instancias de los clientes Boto3
        s3 = boto3.client('s3')
        s3bucket = boto3.resource('s3').Bucket(bucket_name)

        #Se eliminan todos los archivos en el Bucket
        s3bucket.objects.all().delete()
        print("archivos borrados del bucket \n ")

        #Se listan los archivos en el filesystem
        allfiles = list()
        list_files(folder, allfiles)

        #Se suben los archivos al bucket
        print("Intentando subir archivos")
        for name in allfiles:
            mime = mimetypes.guess_type(name)[0] if mimetypes.guess_type(
                name)[0] != None else 'application/octet-stream'
            print(mime + " - " + name)
            s3.upload_file(name,
                           bucket_name,
                           name.replace(folder + '/', ''),
                           ExtraArgs={"ContentType": mime})
            print(name + " subido")
        print("Archivos subidos correctamente")
        return True
    except ClientError as err:
        print("Failed to create s3 boto3 client.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access files.\n" + str(err))
        return False
    except Exception as err:
        print("General error uploading files to S3.\n" + str(err))
        return False


def list_files(folder, file_list):
    files = os.listdir(folder)
    for name in files:
        fullpath = folder + "/" + name
        if (os.path.isdir(fullpath)):
            list_files(fullpath, file_list)
        else:
            file_list.append(fullpath)

def cloudFrontCleanCache(dist_name):
    print("Limpiando cache de cloudfront.. (%s)" % dist_name)
    client = boto3.client('cloudfront')
    response = client.create_invalidation(

    DistributionId= dist_name,
    InvalidationBatch={
        'Paths': {
            'Quantity': 1,
            'Items': ['/*'],
            },
        'CallerReference': str(time()).replace(".", "")
        }
    )
    print(response)
    

def main():
    print("subiendo archivos a bucket s3...")
    if not update_bucket(sys.argv[1], sys.argv[2]):
        sys.exit(1)
    else:
        cloudFrontCleanCache(sys.argv[3])


if __name__ == "__main__":
    main()
