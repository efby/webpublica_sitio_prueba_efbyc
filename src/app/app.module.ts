import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from "ngx-toastr";
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { Ng2Rut } from "ng2-rut";
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SidebarModule } from "ng-sidebar";
import { NgSelectModule } from "@ng-select/ng-select";
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { ApiGatewayService } from "./providers/api-gateway/api-gateway.service";
import { ApiServicesService } from "./providers/api-gateway/api-services.service";
import { IngresoService } from './servicios/ingreso/ingreso.service';
import { HomeService } from "./servicios/home/home.service";
import { VariablesGenerales } from "./globales/variables-generales";
import { MarcadoresTest } from './paginas/home/estaciones/mapa-estaciones/markerEstaciones';
import { Constantes } from "./globales/constantes";
import { MaquinaIngreso } from "./maquinas/maquina-ingreso";
import { MaquinaHome } from "./maquinas/maquina-home";

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { IngresoCargandoComponent } from './paginas/ingreso/ingreso-cargando/ingreso-cargando.component';
import { IngresoTelefonoIdComponent } from './paginas/ingreso/ingreso-telefono-id/ingreso-telefono-id.component';
import { IngresoSMSValorComponent } from './paginas/ingreso/ingreso-smsvalor/ingreso-smsvalor.component';
import { IngresoContrasenaComponent } from './paginas/ingreso/ingreso-contrasena/ingreso-contrasena.component';
import { IngresoRutIdComponent } from './paginas/ingreso/ingreso-rut-id/ingreso-rut-id.component';
import { IngresoNombreComponent } from './paginas/ingreso/ingreso-nombre/ingreso-nombre.component';
import { IngresoEmailComponent } from './paginas/ingreso/ingreso-email/ingreso-email.component';
import { IngresoCreaContrasenaComponent } from './paginas/ingreso/ingreso-crea-contrasena/ingreso-crea-contrasena.component';
import { IngresoPinValorComponent } from './paginas/ingreso/ingreso-pin-valor/ingreso-pin-valor.component';
import { IngresoConfirmaPinValorComponent } from './paginas/ingreso/ingreso-confirma-pin-valor/ingreso-confirma-pin-valor.component';
import { IngresoComponent } from './paginas/ingreso/ingreso.component';
import { HomeComponent } from './paginas/home/home.component';
import { ReintentarSalirComponent } from './modales/reintentar-salir/reintentar-salir.component';
import { NroDocumentoComponent } from './modales/nro-documento/nro-documento.component';
import { ErrorLecturaStorageComponent } from './paginas/paginasError/error-lectura-storage/error-lectura-storage.component';
import { ErrorLecturaDispositivoComponent } from './paginas/paginasError/error-lectura-dispositivo/error-lectura-dispositivo.component';
import { ErrorNOKComponent } from './paginas/paginasError/error-nok/error-nok.component';
import { ErrorSAComponent } from './paginas/paginasError/error-sa/error-sa.component';
import { ErrorEscrituraStorageComponent } from './paginas/paginasError/error-escritura-storage/error-escritura-storage.component';
import { ErrorComponent } from './paginas/paginasError/error/error.component';
import { ModalSCComponent } from './modales/modal-sc/modal-sc.component';
import { ErrorSCComponent } from './paginas/paginasError/error-sc/error-sc.component';
import { IngresoValidaPinComponent } from './paginas/ingreso/ingreso-valida-pin/ingreso-valida-pin.component';
import { IngresoRutIdRecuperacionComponent } from './paginas/ingreso/ingreso-rut-id-recuperacion/ingreso-rut-id-recuperacion.component';
import { IngresoRecuperarContrasenaComponent } from './paginas/ingreso/ingreso-recuperar-contrasena/ingreso-recuperar-contrasena.component';
import { GuiasDespachoComponent } from './paginas/home/documentos/guias-despacho/guias-despacho.component';
import { BoletasComponent } from './paginas/home/documentos/boletas/boletas.component';
import { IngresoModificaContrasenaComponent } from './paginas/ingreso/ingreso-modifica-contrasena/ingreso-modifica-contrasena.component';
import { VehiculoPermisosComponent } from './paginas/home/vehiculos/vehiculo-permisos/vehiculo-permisos.component';
import { ConductorConfirmarComponent } from './paginas/home/conductores/conductor-confirmar/conductor-confirmar.component';
import { DashboardComponent } from './paginas/home/dashboard/dashboard.component';
import { SolicitudesPermisosComponent } from './paginas/home/dashboard/solicitudes-permisos/solicitudes-permisos.component';
import { DetalleGastosComponent } from './paginas/home/dashboard/detalle-gastos/detalle-gastos.component';
import { GraficoGastoGeneralComponent } from './paginas/home/dashboard/grafico-gasto-general/grafico-gasto-general.component';
import { CupoDisponibleUtilizadoComponent } from './paginas/home/dashboard/cupo-disponible-utilizado/cupo-disponible-utilizado.component';
import { DetalleVehiculosComponent } from './paginas/home/dashboard/detalle-gastos/detalle-vehiculos/detalle-vehiculos.component';
import { ConductorPresentacionComponent } from './paginas/home/conductores/conductor-presentacion/conductor-presentacion.component';
import { ConductorCrearComponent } from './paginas/home/conductores/conductor-crear/conductor-crear.component';
import { ModalSubirArchivoComponent } from './modales/modal-subir-archivo/modal-subir-archivo.component';
import { AbonoPagoComponent } from './paginas/home/abono/abono-pago/abono-pago.component';
import { AbonoPagoWebpayComponent } from './paginas/home/abono/abono-pago/abono-pago-webpay/abono-pago-webpay.component';
import { AbonoPagoTransferenciaComponent } from './paginas/home/abono/abono-pago/abono-pago-transferencia/abono-pago-transferencia.component';
import { AbonoResultadoComponent } from './paginas/home/abono/abono-resultado/abono-resultado.component';
import { CreditoResultadoComponent } from './paginas/home/credito/credito-resultado/credito-resultado.component';
import { CreditoPagoComponent } from './paginas/home/credito/credito-pago/credito-pago.component';
import { CreditoPagoTransferenciaComponent } from './paginas/home/credito/credito-pago/credito-pago-transferencia/credito-pago-transferencia.component';
import { CreditoPagoWebpayComponent } from './paginas/home/credito/credito-pago/credito-pago-webpay/credito-pago-webpay.component';
import { ConfigurarPerfilComponent } from './paginas/home/perfil/configurar-perfil/configurar-perfil.component';
import { ConfigurarEmpresaComponent } from './paginas/home/perfil/configurar-empresa/configurar-empresa.component';
import { DatosUsuarioComponent } from './paginas/home/perfil/configurar-perfil/datos-usuario/datos-usuario.component';
import { NotificacionesUsuarioComponent } from './paginas/home/perfil/configurar-perfil/notificaciones-usuario/notificaciones-usuario.component';
import { AlertasUsuarioComponent } from './paginas/home/perfil/configurar-perfil/alertas-usuario/alertas-usuario.component';
import { ActividadUsuarioComponent } from './paginas/home/perfil/configurar-perfil/actividad-usuario/actividad-usuario.component';
import { EmpresasUsuarioComponent } from './paginas/home/perfil/configurar-perfil/empresas-usuario/empresas-usuario.component';
import { InvitacionesUsuarioComponent } from './paginas/home/perfil/configurar-perfil/invitaciones-usuario/invitaciones-usuario.component';
import { DatosEmpresaComponent } from './paginas/home/perfil/configurar-empresa/datos-empresa/datos-empresa.component';
import { RolSuperAdminComponent } from './paginas/home/perfil/configurar-empresa/rol-super-admin/rol-super-admin.component';
import { RolAdminComponent } from './paginas/home/perfil/configurar-empresa/rol-admin/rol-admin.component';
import { RolAnalistaComponent } from './paginas/home/perfil/configurar-empresa/rol-analista/rol-analista.component';
import { InvitarUsuarioComponent } from './paginas/home/perfil/configurar-empresa/invitar-usuario/invitar-usuario.component';
import { DatosFacturacionComponent } from './paginas/home/perfil/configurar-empresa/datos-facturacion/datos-facturacion.component';
import { NuevaEmpresaComponent } from './paginas/home/perfil/configurar-empresa/nueva-empresa/nueva-empresa.component';
import { FiltroEstacionesComponent } from './paginas/home/estaciones/filtro-estaciones/filtro-estaciones.component';
import { MapaEstacionesComponent } from './paginas/home/estaciones/mapa-estaciones/mapa-estaciones.component';
import { EstacionesComponent } from './paginas/home/estaciones/estaciones.component';
import { ModalAceptarInvitacionComponent } from './modales/modal-aceptar-invitacion/modal-aceptar-invitacion.component';
import { AceptarCancelarComponent } from './modales/aceptar-cancelar/aceptar-cancelar.component';
import { NuevoAdministradorMuevoComponent } from './paginas/ingreso/nuevo-administrador-muevo/nuevo-administrador-muevo.component';
import { NuevoContrasenaMuevoComponent } from './paginas/ingreso/nuevo-contrasena-muevo/nuevo-contrasena-muevo.component';
import { NuevoLoginMuevoComponent } from './paginas/ingreso/nuevo-login-muevo/nuevo-login-muevo.component';
import { HomeMuevoComponent } from './paginas/home-muevo/home-muevo.component';
import { MuevoDashboardComponent } from './paginas/home-muevo/muevo-dashboard/muevo-dashboard.component';
import { NuevoMailMuevoComponent } from './paginas/ingreso/nuevo-mail-muevo/nuevo-mail-muevo.component';
import { NuevoLoginPersistenteMuevoComponent } from './paginas/ingreso/nuevo-login-persistente-muevo/nuevo-login-persistente-muevo.component';
import { MuevoGraficoGastoComponent } from './paginas/home-muevo/muevo-dashboard/muevo-grafico-gasto/muevo-grafico-gasto.component';
import { MuevoCupoDisponibleComponent } from './paginas/home-muevo/muevo-dashboard/muevo-cupo-disponible/muevo-cupo-disponible.component';
import { MuevoGastosComponent } from './paginas/home-muevo/muevo-dashboard/muevo-gastos/muevo-gastos.component';
import { MuevoConductoresComponent } from './paginas/home-muevo/muevo-dashboard/muevo-gastos/muevo-conductores/muevo-conductores.component';
import { MuevoVehiculosComponent } from './paginas/home-muevo/muevo-dashboard/muevo-gastos/muevo-vehiculos/muevo-vehiculos.component';
import { MuevoConductorCrearComponent } from './paginas/home-muevo/conductores/muevo-conductor-crear/muevo-conductor-crear.component';
import { MuevoConductorConfirmarComponent } from './paginas/home-muevo/conductores/muevo-conductor-confirmar/muevo-conductor-confirmar.component';
import { MuevoVehiculoConfirmarComponent } from './paginas/home-muevo/vehiculos/muevo-vehiculo-confirmar/muevo-vehiculo-confirmar.component';
import { NuevoDatosFacturacionComponent } from './paginas/home/perfil/configurar-empresa/nuevo-datos-facturacion/nuevo-datos-facturacion.component';
import { MuevoMisConductoresComponent } from './paginas/home-muevo/conductores/muevo-mis-conductores/muevo-mis-conductores.component';
import { MuevoMisVehiculosComponent } from './paginas/home-muevo/vehiculos/muevo-mis-vehiculos/muevo-mis-vehiculos.component';
import { MuevoMiFacturacionComponent } from './paginas/home-muevo/facturacion/muevo-mi-facturacion/muevo-mi-facturacion.component';
import { PerfilComponent } from './paginas/home-muevo/perfil/perfil.component';
import { MuevoMiPerfilComponent } from './paginas/home-muevo/perfil/muevo-mi-perfil/muevo-mi-perfil.component';
import { MuevoMisNotificacionesComponent } from './paginas/home-muevo/perfil/muevo-mis-notificaciones/muevo-mis-notificaciones.component';
import { MuevoMisEmpresasComponent } from './paginas/home-muevo/perfil/muevo-mis-empresas/muevo-mis-empresas.component';
import { MuevoTerminosyCondicionesComponent } from './paginas/home-muevo/perfil/muevo-terminosy-condiciones/muevo-terminosy-condiciones.component';
import { MuevoAyudaComponent } from './paginas/home-muevo/perfil/muevo-ayuda/muevo-ayuda.component';
import { MetodosPagoComponent } from './paginas/home-muevo/metodos-pago/metodos-pago.component';
import { MuevoEquipoSeleccionaCuentaComponent } from './paginas/home-muevo/equipos/muevo-equipo-selecciona-cuenta/muevo-equipo-selecciona-cuenta.component';
import { MuevoEquipoNombreComponent } from './paginas/home-muevo/equipos/muevo-equipo-nombre/muevo-equipo-nombre.component';
import { MuevoFacturasComponent } from './paginas/home-muevo/transacciones/muevo-facturas/muevo-facturas.component';
import { MuevoBoletasComponent } from './paginas/home-muevo/transacciones/muevo-boletas/muevo-boletas.component';
import { MuevoGuiasComponent } from './paginas/home-muevo/transacciones/muevo-guias/muevo-guias.component';
import { TransaccionesComponent } from './paginas/home-muevo/transacciones/transacciones.component';
import { MuevoMisInvitacionesComponent } from './paginas/home-muevo/conductores/muevo-mis-invitaciones/muevo-mis-invitaciones.component';
import { OneClickComponent } from './modales/one-click/one-click.component';
import { SafePipe } from "./safe.pipe";
import { AbonarComponent } from './paginas/home-muevo/abonar/abonar.component';
import { MuevoAbonoTarjetasComponent } from './paginas/home-muevo/abonar/muevo-abono-tarjetas/muevo-abono-tarjetas.component';
import { MuevoAbonoTransferenciaComponent } from './paginas/home-muevo/abonar/muevo-abono-transferencia/muevo-abono-transferencia.component';
import { MuevoAbonoProcesandoComponent } from './paginas/home-muevo/abonar/muevo-abono-procesando/muevo-abono-procesando.component';
import { RolesPerfilesComponent } from './paginas/home-muevo/roles-perfiles/roles-perfiles.component';
import { MuevoFormasdePagoComponent } from './paginas/home-muevo/muevo-dashboard/muevo-formasde-pago/muevo-formasde-pago.component';
import { MuevoFormaPagoSoloBilleteraComponent } from './paginas/home-muevo/muevo-dashboard/muevo-formasde-pago/muevo-forma-pago-solo-billetera/muevo-forma-pago-solo-billetera.component';
import { MuevoFormaPagoBilleteraAtendedorComponent } from './paginas/home-muevo/muevo-dashboard/muevo-formasde-pago/muevo-forma-pago-billetera-atendedor/muevo-forma-pago-billetera-atendedor.component';
import { MuevoFormaPagoTodosComponent } from './paginas/home-muevo/muevo-dashboard/muevo-formasde-pago/muevo-forma-pago-todos/muevo-forma-pago-todos.component';
import { HistorialComponent } from './paginas/home-muevo/historial/historial.component';
import { HistorialAbonosComponent } from './paginas/home-muevo/historial/historial-abonos/historial-abonos.component';
import { HistorialCargosComponent } from './paginas/home-muevo/historial/historial-cargos/historial-cargos.component';
import { HistorialTodosComponent } from './paginas/home-muevo/historial/historial-todos/historial-todos.component';
import { MuevoRecuperaPinComponent } from './paginas/ingreso/muevo-recupera-pin/muevo-recupera-pin.component';
import { NuevoIngresoSoporteComponent } from './paginas/ingreso/nuevo-ingreso-soporte/nuevo-ingreso-soporte.component';
import { MenuSideBarComponent } from './paginas/home-muevo/layout/menu-side-bar/menu-side-bar.component';
import { MatTableModule, MatCheckboxModule, MatInputModule, MatSlideToggleModule, MatPaginatorModule, MatSortModule, MatNativeDateModule, MatProgressSpinnerModule } from '@angular/material';
import { NgKnifeModule } from 'ng-knife';

import { registerLocaleData, DatePipe } from '@angular/common';
import localeEsCL from '@angular/common/locales/es-CL';
import { LoadingIndicatorDirective } from './directives/loading-indicator.directive';
import { DisplayErrorFieldComponent } from './components/display-error-field/display-error-field.component';
import { ValidationBorderModule } from './modules/validation-border';
import { ValidationBorderConfiguration } from './configurations/validation-border.configuration';
import { OrderByPipe } from './pipes/order-by.pipe';
import { MuevoInvitacionesConductoresComponent } from './paginas/home-muevo/conductores/muevo-invitaciones-conductores/muevo-invitaciones-conductores.component';
import { CuentasComponent } from './paginas/home-muevo/cuentas/cuentas.component';
import { CuentasInvitacionesComponent } from './paginas/home-muevo/cuentas/cuentas-invitaciones/cuentas-invitaciones.component';
import { CuentasCrearComponent } from './paginas/home-muevo/cuentas/cuentas-crear/cuentas-crear.component';
import { HistorialesComponent } from './paginas/home-muevo/historiales/historiales.component';
import { ComprasComponent } from './paginas/home-muevo/historiales/compras/compras.component';
import { PatentePipe } from './pipes/patente.pipe';
import { FacturasComponent } from './paginas/home-muevo/historiales/facturas/facturas.component';
import { MuevoCalendarComponent } from './components/muevo-calendar/muevo-calendar.component';
import { ComprasDetalleComponent } from './paginas/home-muevo/historiales/compras/compras-detalle/compras-detalle.component';
import { FacturasDetalleComponent } from './paginas/home-muevo/historiales/facturas/facturas-detalle/facturas-detalle.component';
import { MetodosPagoMovimientosComponent } from './paginas/home-muevo/metodos-pago/metodos-pago-movimientos/metodos-pago-movimientos.component';
import { MuevoConductoresRootComponent } from './paginas/home-muevo/conductores/muevo-conductores-root/muevo-conductores-root.component';
import { MuevoConductoresListComponent } from './paginas/home-muevo/conductores/muevo-conductores-list/muevo-conductores-list.component';
import { MuevoConductoresOtrosComponent } from './paginas/home-muevo/conductores/muevo-conductores-otros/muevo-conductores-otros.component';
import { ConductorDetalleComponent } from './paginas/home-muevo/conductores/conductor-detalle/conductor-detalle.component';
import { RestriccionesDiasHorariosComponent } from './paginas/home-muevo/conductores/restricciones-dias-horarios/restricciones-dias-horarios.component';
import { ConductorAjustesCombustibleComponent } from './paginas/home-muevo/conductores/conductor-ajustes-combustible/conductor-ajustes-combustible.component';
import { ConductorCrearInvitacionComponent } from './paginas/home-muevo/conductores/conductor-crear-invitacion/conductor-crear-invitacion.component';
import { ConductorEstacionesComponent } from './paginas/home-muevo/conductores/conductor-estaciones/conductor-estaciones.component';
import { LimiteGastosComponent } from './paginas/home-muevo/conductores/conductor-crear-invitacion/limite-gastos/limite-gastos.component';
import { VehiculosAsignadosComponent } from './paginas/home-muevo/vehiculos/vehiculos-asignados/vehiculos-asignados.component';
import { VehiculosAsignacionComponent } from './paginas/home-muevo/vehiculos/vehiculos-asignacion/vehiculos-asignacion.component';
import { FilterPipe } from './pipes/filter.pipe';
import { VehiculoDetalleComponent } from './paginas/home-muevo/vehiculos/vehiculo-detalle/vehiculo-detalle.component';
import { VehiculoCrearComponent } from './paginas/home-muevo/vehiculos/vehiculo-crear/vehiculo-crear.component';
import { ComponentsUiComponent } from './paginas/home-muevo/ui/components-ui/components-ui.component';
import { AbonoProcesarComponent } from './paginas/home-muevo/metodos-pago/abono-procesar/abono-procesar.component';
import { AbonoExitoComponent } from './paginas/home-muevo/metodos-pago/abono-exito/abono-exito.component';
import { CuentaDetalleComponent } from './paginas/home-muevo/cuentas/cuenta-detalle/cuenta-detalle.component';
import { LimitTextPipe } from './pipes/limit-text.pipe';
import { AbonoErrorComponent } from './paginas/home-muevo/metodos-pago/abono-error/abono-error.component';
import { MuevoConductoresGeneralesComponent } from './paginas/home-muevo/conductores/muevo-conductores-generales/muevo-conductores-generales.component';
import { LogPipe } from './pipes/log.pipe';
import { SeleccionMedioComponent } from './paginas/home-muevo/metodos-pago/abono/seleccion-medio/seleccion-medio.component';
import { CuentasBancariasComponent } from './paginas/home-muevo/metodos-pago/abono/cuentas-bancarias/cuentas-bancarias.component';
import { MuevoGenericComponent } from './modales/muevo-generic/muevo-generic.component';
import { InfoBancariaComponent } from './paginas/home-muevo/metodos-pago/abono/info-bancaria/info-bancaria.component';
import { TarjetaAbonoComponent } from './paginas/home-muevo/metodos-pago/abono/tarjeta-abono/tarjeta-abono.component';
import { TarjetaAbonoAbonarComponent } from './paginas/home-muevo/metodos-pago/abono/tarjeta-abono-abonar/tarjeta-abono-abonar.component';
import { ScrollPaginatorComponent } from './components/scroll-paginator/scroll-paginator.component';
import { IngresoTerminosComponent } from './paginas/ingreso/ingreso-terminos/ingreso-terminos.component';
import { EditarPerfilComponent } from './paginas/home-muevo/perfil/editar-perfil/editar-perfil.component';
import { SidebarService } from './services/sidebar-services';
import { ModificarNombreComponent } from './paginas/home-muevo/perfil/editar-perfil/modificar-nombre/modificar-nombre.component';
import { UtilModuleModule } from './utils/util-module.module';
import { ModificarMailComponent } from './paginas/home-muevo/perfil/editar-perfil/modificar-mail/modificar-mail.component';
import { RetirarAbonoComponent } from './paginas/home-muevo/abonar/retirar-abono/retirar-abono.component';
import { SeleccionarCuentaBancariaRetiroComponent } from './paginas/home-muevo/abonar/seleccionar-cuenta-bancaria-retiro/seleccionar-cuenta-bancaria-retiro.component';
import { RevisaEnviaRetiroComponent } from './paginas/home-muevo/abonar/revisa-envia-retiro/revisa-envia-retiro.component';
import { MisAdministradoresComponent } from './paginas/home-muevo/mis-administradores/mis-administradores.component';
import { DetalleAdministradorComponent } from './paginas/home-muevo/mis-administradores/detalle-administrador/detalle-administrador.component';
import { InvitacionesAdministradorComponent } from './paginas/home-muevo/mis-administradores/invitaciones-administrador/invitaciones-administrador.component';
import { CrearInvitacionComponent } from './paginas/home-muevo/mis-administradores/crear-invitacion/crear-invitacion.component';
import { EnviaInvitacionComponent } from './paginas/home-muevo/mis-administradores/crear-invitacion/envia-invitacion/envia-invitacion.component';
import { DatosAdministradorComponentextends } from './paginas/home-muevo/mis-administradores/crear-invitacion/datos-administrador/datos-administrador.component';
import { MiPerfilSidebarComponent } from './paginas/home-muevo/perfil/mi-perfil-sidebar/mi-perfil-sidebar.component';
import { AyudaComponent } from './paginas/home-muevo/perfil/ayuda/ayuda.component';
import {MatBadgeModule} from '@angular/material/badge';
import { ConfigNotificacionesComponent } from './paginas/home-muevo/perfil/config-notificaciones/config-notificaciones.component';
import { InvitacionesRecibidasComponent } from './paginas/home-muevo/cuentas/invitaciones-recibidas/invitaciones-recibidas.component';
import { DatalleInvitacionRecibidaComponent } from './paginas/home-muevo/cuentas/invitaciones-recibidas/datalle-invitacion-recibida/datalle-invitacion-recibida.component';
import { NotificacionesComponent } from './paginas/home-muevo/notificaciones/notificaciones.component';


registerLocaleData(localeEsCL, 'es-CL');

@NgModule({
    declarations: [
        AppComponent,
        IngresoCargandoComponent,
        IngresoTelefonoIdComponent,
        IngresoSMSValorComponent,
        IngresoContrasenaComponent,
        IngresoRutIdComponent,
        IngresoNombreComponent,
        IngresoEmailComponent,
        IngresoCreaContrasenaComponent,
        IngresoPinValorComponent,
        IngresoConfirmaPinValorComponent,
        IngresoComponent,
        HomeComponent,
        ReintentarSalirComponent,
        NroDocumentoComponent,
        ErrorLecturaStorageComponent,
        ErrorLecturaDispositivoComponent,
        ErrorNOKComponent,
        ErrorSAComponent,
        ErrorEscrituraStorageComponent,
        ErrorComponent,
        ModalSCComponent,
        ErrorSCComponent,
        IngresoValidaPinComponent,
        IngresoRutIdRecuperacionComponent,
        IngresoRecuperarContrasenaComponent,
        FacturasComponent,
        GuiasDespachoComponent,
        BoletasComponent,
        IngresoModificaContrasenaComponent,
        VehiculoPermisosComponent,
        ConductorConfirmarComponent,
        DashboardComponent,
        SolicitudesPermisosComponent,
        DetalleGastosComponent,
        GraficoGastoGeneralComponent,
        CupoDisponibleUtilizadoComponent,
        DetalleVehiculosComponent,
        ConductorPresentacionComponent,
        ConductorCrearComponent,
        ModalSubirArchivoComponent,
        AbonoPagoComponent,
        AbonoPagoWebpayComponent,
        AbonoPagoTransferenciaComponent,
        AbonoResultadoComponent,
        CreditoResultadoComponent,
        CreditoPagoComponent,
        CreditoPagoTransferenciaComponent,
        CreditoPagoWebpayComponent,
        ConfigurarPerfilComponent,
        ConfigurarEmpresaComponent,
        DatosUsuarioComponent,
        NotificacionesUsuarioComponent,
        AlertasUsuarioComponent,
        ActividadUsuarioComponent,
        EmpresasUsuarioComponent,
        InvitacionesUsuarioComponent,
        DatosEmpresaComponent,
        RolSuperAdminComponent,
        RolAdminComponent,
        RolAnalistaComponent,
        InvitarUsuarioComponent,
        DatosFacturacionComponent,
        NuevaEmpresaComponent,
        FiltroEstacionesComponent,
        MapaEstacionesComponent,
        EstacionesComponent,
        ModalAceptarInvitacionComponent,
        AceptarCancelarComponent,
        NuevoAdministradorMuevoComponent,
        NuevoContrasenaMuevoComponent,
        NuevoLoginMuevoComponent,
        HomeMuevoComponent,
        MuevoDashboardComponent,
        NuevoMailMuevoComponent,
        NuevoLoginPersistenteMuevoComponent,
        MuevoGraficoGastoComponent,
        MuevoCupoDisponibleComponent,
        MuevoGastosComponent,
        MuevoConductoresComponent,
        MuevoVehiculosComponent,
        MuevoConductorCrearComponent,
        MuevoConductorConfirmarComponent,
        MuevoVehiculoConfirmarComponent,
        NuevoDatosFacturacionComponent,
        MuevoMisConductoresComponent,
        MuevoMisVehiculosComponent,
        MuevoMiFacturacionComponent,
        PerfilComponent,
        MuevoMiPerfilComponent,
        MuevoMisNotificacionesComponent,
        MuevoMisEmpresasComponent,
        MuevoTerminosyCondicionesComponent,
        MuevoAyudaComponent,
        MetodosPagoComponent,
        MuevoEquipoSeleccionaCuentaComponent,
        MuevoEquipoNombreComponent,
        MuevoFacturasComponent,
        MuevoBoletasComponent,
        MuevoGuiasComponent,
        TransaccionesComponent,
        MuevoMisInvitacionesComponent,
        OneClickComponent,
        SafePipe,
        AbonarComponent,
        MuevoAbonoTarjetasComponent,
        MuevoAbonoTransferenciaComponent,
        MuevoAbonoProcesandoComponent,
        RolesPerfilesComponent,
        MuevoFormasdePagoComponent,
        MuevoFormaPagoSoloBilleteraComponent,
        MuevoFormaPagoBilleteraAtendedorComponent,
        MuevoFormaPagoTodosComponent,
        HistorialComponent,
        HistorialAbonosComponent,
        HistorialCargosComponent,
        HistorialTodosComponent,
        MuevoRecuperaPinComponent,
        NuevoIngresoSoporteComponent,
        MenuSideBarComponent,
        LoadingIndicatorDirective,
        DisplayErrorFieldComponent,
        OrderByPipe,
        MuevoInvitacionesConductoresComponent,
        CuentasComponent,
        CuentasInvitacionesComponent,
        CuentasCrearComponent,
        HistorialesComponent,
        ComprasComponent,
        PatentePipe,
        MuevoCalendarComponent,
        ComprasDetalleComponent,
        FacturasDetalleComponent,
        MetodosPagoMovimientosComponent,
        MuevoConductoresRootComponent,
        MuevoConductoresListComponent,
        MuevoConductoresOtrosComponent,
        ConductorDetalleComponent,
        RestriccionesDiasHorariosComponent,
        ConductorAjustesCombustibleComponent,
        ConductorCrearInvitacionComponent,
        ConductorEstacionesComponent,
        LimiteGastosComponent,
        VehiculosAsignadosComponent,
        VehiculosAsignacionComponent,
        FilterPipe,
        VehiculoDetalleComponent,
        VehiculoCrearComponent,
        ComponentsUiComponent,
        AbonoProcesarComponent,
        AbonoExitoComponent,
        CuentaDetalleComponent,
        LimitTextPipe,
        
        AbonoErrorComponent,
        MuevoConductoresGeneralesComponent,
        LogPipe,
        SeleccionMedioComponent,
        CuentasBancariasComponent,
        MuevoGenericComponent,
        InfoBancariaComponent,
        TarjetaAbonoComponent,
        TarjetaAbonoAbonarComponent,
        ScrollPaginatorComponent,
        IngresoTerminosComponent,

        //NUEVAS COMPONENTES
        EditarPerfilComponent,
        ModificarNombreComponent,
        ModificarMailComponent,
        RetirarAbonoComponent,
        SeleccionarCuentaBancariaRetiroComponent,
        RevisaEnviaRetiroComponent,
        MisAdministradoresComponent,
        DetalleAdministradorComponent,
        InvitacionesAdministradorComponent,
        CrearInvitacionComponent,
        EnviaInvitacionComponent,
        DatosAdministradorComponentextends,
        MiPerfilSidebarComponent,
        AyudaComponent,
        ConfigNotificacionesComponent,
        InvitacionesRecibidasComponent,
        DatalleInvitacionRecibidaComponent,
        NotificacionesComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        NgxSpinnerModule,
        NgSelectModule,
        FormsModule,
        ToastrModule.forRoot(),
        HttpClientModule,
        NgbModule,
        Ng2Rut,
        UiSwitchModule,
        NgxChartsModule,
        SidebarModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAqR5c23DyLVCnaiWsWkmQZrc-rqNvZK_Y',
            libraries: ["places", "geometry"]
        }),
        AgmDirectionModule,
        MatTableModule,
        MatCheckboxModule,
        MatInputModule,
        MatSlideToggleModule,
        MatPaginatorModule,
        MatSortModule,
        MatNativeDateModule,
        MatProgressSpinnerModule,
        MatDatepickerModule,
        MatBadgeModule,
        UtilModuleModule,
        
        // MatRangeDatepickerModule,
        // MatRangeNativeDateModule,
        
        NgKnifeModule,
        ValidationBorderModule.forRoot(ValidationBorderConfiguration)
    ],
    providers: [
        ApiGatewayService,
        ApiServicesService,
        SidebarService,
        IngresoService,
        HomeService,
        VariablesGenerales,
        Constantes,
        MaquinaIngreso,
        MaquinaHome,
        { provide: LOCALE_ID, useValue: 'es-CL' }
    ],
    bootstrap: [AppComponent],
    entryComponents: [
        //AQUI  NUEVOS COMPONENTES
        ModificarMailComponent,EditarPerfilComponent,ModificarNombreComponent,RetirarAbonoComponent,SeleccionarCuentaBancariaRetiroComponent,CuentasBancariasComponent,
        RevisaEnviaRetiroComponent,DetalleAdministradorComponent,InvitacionesAdministradorComponent,CrearInvitacionComponent,EnviaInvitacionComponent,
        DatosAdministradorComponentextends,MiPerfilSidebarComponent,AyudaComponent,ConfigNotificacionesComponent,InvitacionesRecibidasComponent,DatalleInvitacionRecibidaComponent,
        NotificacionesComponent,
        //FIN NUEVOS COMPONENTES
        NroDocumentoComponent, ModalSCComponent, ModalSubirArchivoComponent, NuevaEmpresaComponent, InvitarUsuarioComponent, ModalAceptarInvitacionComponent, AceptarCancelarComponent, NuevoDatosFacturacionComponent, OneClickComponent, AbonoProcesarComponent, AbonoExitoComponent, AbonoErrorComponent, MuevoGenericComponent]
})
export class AppModule { }
