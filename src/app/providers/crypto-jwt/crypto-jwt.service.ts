import { TextEncoder } from 'text-encoding';
import { Jose, JoseJWE } from 'jose-jwe-jws';
import { JweTipo } from "src/app/models/jwe-tipo";
import { Respuestas } from 'src/app/globales/respuestas';


export class CryptoJWTService {

  private static cryptographer: any = new Jose.WebCryptographer();
  private static AlgorithmImportKey: string = "AES-KW";
  private static KeyEncryptionAlgorithm: string = "A256KW";
  private static ContentEncryptionAlgorithm: string = "A256CBC-HS512";
  private static MovileOS: string = "";

  public static JWEInit(_movileOS: string, KeyEncryptionAlgorithm?: string, ContentEncryptionAlgorithm?: string, AlgorithmImportKey?: string) {
    this.cryptographer.setKeyEncryptionAlgorithm(KeyEncryptionAlgorithm != null ? KeyEncryptionAlgorithm : this.KeyEncryptionAlgorithm);
    this.cryptographer.setContentEncryptionAlgorithm(ContentEncryptionAlgorithm != null ? ContentEncryptionAlgorithm : this.ContentEncryptionAlgorithm);
    if (AlgorithmImportKey != null) {
      this.AlgorithmImportKey = AlgorithmImportKey;
    }
    this.MovileOS = _movileOS;
  }

  public static key2Base64UrlEncode(str): string {
    return btoa(str)
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/=+$/, "");
  };

  public static JWEEncrypter(bodyJSON: any, k: string): Promise<JweTipo> {
    // console.log(bodyJSON, "key: " + k);
    let shared_key = { "kty": "oct", "k": this.key2Base64UrlEncode(k) };
    // let shared_key = { "kty": "oct", "k": k };
    try {
      let shared_key2;

      if ((<any>window).crypto.webkitSubtle != null) {

        var enc = new TextEncoder(); // always utf-8;
        var jwkKeyAsArrayBuffer = enc.encode(JSON.stringify(shared_key));
        shared_key2 = (<any>window).crypto.webkitSubtle.importKey("jwk", jwkKeyAsArrayBuffer, { name: this.AlgorithmImportKey }, true, ["wrapKey", "unwrapKey"]);//Jose.Utils.importRsaPrivateKey(shared_key,"A256CBC-HS512")//
        (<any>window).crypto.subtle = (<any>window).crypto.webkitSubtle
      }
      else {

        shared_key2 = crypto.subtle.importKey("jwk", shared_key, this.AlgorithmImportKey, true, ["wrapKey", "unwrapKey"]);
      }


      let encrypter = new JoseJWE.Encrypter(this.cryptographer, shared_key2);

      return new Promise((resolve) => {

        encrypter.encrypt(JSON.stringify(bodyJSON))
          .then((encripted_plain_text: any) => {
            //resolve({ resultado: VariableConstante.RESPUESTA_OK, text: encripted_plain_text })
            resolve({ resultado: Respuestas.RESPUESTA_OK, text: encripted_plain_text })
          }
          )

      })

    } catch (error) {
      console.log(error)
    }

  }

  public static async JWEDecrypter(bodyJWE: string, k: string): Promise<JweTipo> {
    // console.log(bodyJWE, " / ", k);
    let shared_key = { "kty": "oct", "k": this.key2Base64UrlEncode(k) };
    // let shared_key = { "kty": "oct", "k": k };

    let shared_key2;

    if ((<any>window).crypto.webkitSubtle != null) {

      var enc = new TextEncoder(); // always utf-8;
      var jwkKeyAsArrayBuffer = enc.encode(JSON.stringify(shared_key));
      shared_key2 = (<any>window).crypto.webkitSubtle.importKey("jwk", jwkKeyAsArrayBuffer, { name: this.AlgorithmImportKey }, true, ["wrapKey", "unwrapKey"]);//Jose.Utils.importRsaPrivateKey(shared_key,"A256CBC-HS512")//
      (<any>window).crypto.subtle = (<any>window).crypto.webkitSubtle
    }
    else {

      shared_key2 = crypto.subtle.importKey("jwk", shared_key, this.AlgorithmImportKey, true, ["wrapKey", "unwrapKey"]);
    }


    let decrypter = new JoseJWE.Decrypter(this.cryptographer, shared_key2);
    return new Promise((resolve) => {
      decrypter.decrypt(bodyJWE)
        .then((decrypted_plain_text: string) => {
          //resolve({ resultado: VariableConstante.RESPUESTA_OK, text: decrypted_plain_text });
          resolve({ resultado: Respuestas.RESPUESTA_OK, text: decrypted_plain_text });
        }
        )
    });
  }
}
