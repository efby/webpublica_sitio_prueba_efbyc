import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { CryptoJWTService } from "../crypto-jwt/crypto-jwt.service";
import { VariablesGenerales } from "../../globales/variables-generales";
import { timeout } from 'rxjs/operators';
import { Constantes } from 'src/app/globales/constantes';
import { Respuestas } from 'src/app/globales/respuestas';
import * as crypto from "crypto-js";
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  constructor(private http: HttpClient, private variablesApi: VariablesGenerales, private contantes: Constantes) {
    CryptoJWTService.JWEInit("none");
  }

  generaIdEquipo(bodyStr: string, key: string): string {

    let idEquipo = crypto.HmacSHA256(bodyStr, key).toString(crypto.enc.Hex).substr(0, 40);
    return idEquipo;
  }

  async decrypt(error) {
    let strBodyDecripter = CryptoJWTService.JWEDecrypter(error.error, this.variablesApi.equipoSecret);
    return await strBodyDecripter.then((strDesc: any) => {
      return strDesc;
    });
  }

  async callApiCript(url: string, request: any, cabeceras: HttpHeaders): Promise<any> {
    return new Promise((resolve, reject) => {
      let strBodyEncripter = CryptoJWTService.JWEEncrypter(request, this.variablesApi.equipoSecret);
      strBodyEncripter.then(respuesta => {
        if (respuesta.resultado === Respuestas.RESPUESTA_OK) {
          let str = respuesta.text;
          //console.log("send", str);
          this.http.post(url, str, { headers: cabeceras, responseType: 'text' })
            .pipe(timeout(30000))
            .subscribe(
              async resp => {
                try {
                  let strBodyDecripter = await CryptoJWTService.JWEDecrypter(resp.toString(), this.variablesApi.equipoSecret);
                  resolve(strBodyDecripter);
                  // console.log(strBodyDecripter);
                } catch (error) {
                  reject(resp);
                }

              },
              async error => {
                try {
                  if (error.error.indexOf(" ") > 0) {
                    reject(error);
                  } else {
                    let strBodyDecripter = await CryptoJWTService.JWEDecrypter(error.error, this.variablesApi.equipoSecret);
                    // console.log(strBodyDecripter);
                    reject(strBodyDecripter)
                  }
                } catch (ex) {
                  reject(error);
                }
              }
            );
        } else {
          // console.log("ERROR AL ENCRIPTAR");
          reject(respuesta);
        }
      });
    });
  }

  async callApiNoHeaders(url: string, request: any): Promise<any> {
    if (environment.Ambiente !== "PR") {
      console.log(request);
    }
    return new Promise((resolve, reject) => {
      let cabeceras = new HttpHeaders({
        "Content-Type": "application/json"
      });

      this.http.post(url, JSON.stringify(request), { headers: cabeceras })
        .pipe(timeout(30000))
        .subscribe(response => {
          let respuesta = JSON.parse(JSON.stringify(response));
          // console.log(respuesta);
          resolve(respuesta)
        }, error => {
          // console.log(error);
          reject(error);
        });
    });
  }

  async callApiHeadersJSON(url: string, request: any): Promise<any> {
    if (environment.Ambiente !== "PR") {
      console.log(request);
    }
    console.log("URL: "+url);

    let bodyStr = JSON.stringify(request);
    let firma = crypto.HmacSHA256(bodyStr, this.variablesApi.equipoSecret).toString(crypto.enc.Hex);
    
    let cabeceras = new HttpHeaders({
      access_token: this.variablesApi.authorizationToken,
      firma: firma,
      "Content-Type": "application/json"
    });

    return new Promise((resolve, reject) => {
      this.http.post(url, bodyStr, { headers: cabeceras, observe: "response" })
        .pipe(timeout(30000))
        .subscribe((response: HttpResponse<any>) => {
          let respuesta = JSON.parse(response.body);
          let respFirma = response.headers.get('firma');
          let bodyFirma = crypto.HmacSHA256(response.body, this.variablesApi.equipoSecret).toString(crypto.enc.Hex);
          if (environment.Ambiente !== "PR") {
            console.log(respuesta);
          }
          if (respFirma === bodyFirma) {
            resolve(respuesta)
          } else {
            reject({ status: 403 });
          }
        }, error => {
          if (environment.Ambiente !== "PR") {
            console.log(error);
          }
          reject(error);
        });
    });
  }

  async callApiHeaders(url: string, request: any): Promise<any> {
    if (environment.Ambiente !== "PR") {
      console.log(request);
    }
    console.log("URL: "+url);

    let bodyStr = JSON.stringify(request);
    let firma = crypto.HmacSHA256(bodyStr, this.variablesApi.equipoSecret).toString(crypto.enc.Hex);
    
    let cabeceras = new HttpHeaders({
      access_token: this.variablesApi.authorizationToken,
      firma: firma,
      "Content-Type": "application/json"
    });

    return new Promise((resolve, reject) => {
      this.http.post(url, bodyStr, { headers: cabeceras, observe: "response", responseType: "text" })
        .pipe(timeout(30000))
        .subscribe((response: HttpResponse<any>) => {
          let respuesta = JSON.parse(response.body);
          let respFirma = response.headers.get('firma');
          let bodyFirma = crypto.HmacSHA256(response.body, this.variablesApi.equipoSecret).toString(crypto.enc.Hex);
          if (environment.Ambiente !== "PR") {
            console.log(respuesta);
          }
          if (respFirma === bodyFirma) {
            resolve(respuesta)
          } else {
            reject({ status: 403 });
          }
        }, error => {
          if (environment.Ambiente !== "PR") {
            console.log(error);
          }
          reject(error);
        });
    });
  }

  async callApiHeadersPut(url: string, request: any): Promise<any> {
    if (environment.Ambiente !== "PR") {
      console.log(request);
    }
    console.log("URL: "+url);
    let bodyStr = JSON.stringify(request);
    let firma = crypto.HmacSHA256(bodyStr, this.variablesApi.equipoSecret).toString(crypto.enc.Hex);

    let cabeceras = new HttpHeaders({
      access_token: this.variablesApi.authorizationToken,
      firma: firma,
      "Content-Type": "application/json"
    });

    return new Promise((resolve, reject) => {
      this.http.put(url, bodyStr, { headers: cabeceras, observe: "response", responseType: "text" })
        .pipe(timeout(30000))
        .subscribe((response: HttpResponse<any>) => {
          let respuesta = JSON.parse(response.body);
          let respFirma = response.headers.get('firma');
          let bodyFirma = crypto.HmacSHA256(response.body, this.variablesApi.equipoSecret).toString(crypto.enc.Hex);
          if (environment.Ambiente !== "PR") {
            console.log(respuesta);
          }
          if (respFirma === bodyFirma) {
            resolve(respuesta)
          } else {
            reject({ status: 403 });
          }
        }, error => {
          if (environment.Ambiente !== "PR") {
            console.log(error);
          }
          reject(error);
        });
    });
  }

  async callApiGet(url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(response => {
        resolve(response);
      }, error => {
        reject(error);
      });
    });
  }

}
