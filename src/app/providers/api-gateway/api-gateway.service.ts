import { Injectable } from '@angular/core';
import { ApiServicesService } from './api-services.service';
import { HttpHeaders } from '@angular/common/http';
import { VariablesGenerales, Cuentas, Flotas, Grupos, Maestros, Bancos, TipoCuentaBancaria, TipoDocumento, TipoRestriccion, TipoSaldo, TipoVehiculo, CuentaDatosDte, Roles, Notificaciones, Movimiento } from 'src/app/globales/variables-generales';
import { ResponseTipo, RequestTipo } from 'src/app/models/respuestas-tipo';
import { Respuestas } from 'src/app/globales/respuestas';
import { Constantes } from 'src/app/globales/constantes';
import { Vehiculos } from 'src/app/models/vehiculos';
import { TipoMedioPago, TipoUsuarioCuentaEstado } from 'src/app/globales/enumeradores';
import { Conductor, IConfiguracionCombustible, IRestriccionesCombustible } from 'src/app/models/conductores';
import { Facturas, DatosFacturacion, DetalleFactura } from 'src/app/models/facturas';
import { Guias, DetalleGuia } from 'src/app/models/guias';
import { Boletas, DetalleBoleta } from 'src/app/models/boletas';
import { environment } from '../../../environments/environment';
import { Cuenta, Menu, CuentaSeleccionada, TipoFormaPago, Saldo, CuentaCorriente, MedioPago, Tarjeta, UsuarioLogin, PeriodosFacturacion, Invitacion, InvitacionValidaciones, Transaccion, TransaccionDetalle, MedioPagoMovimiento, Vehiculo, IInvitacionConductor } from 'src/app/globales/business-objects';

import * as moment from 'moment';
import 'moment/min/locales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
moment.locale('es')

@Injectable({
    providedIn: 'root'
})

export class ApiGatewayService {
    MAX_ERRORES: number = 5;
    MAX_EXEC_TIME: number = 180;
    _maxDate: Date;


    constructor(private apiService: ApiServicesService, private variablesApi: VariablesGenerales, private constantes: Constantes) {
        this.variablesApi.equipoSecret = localStorage.getItem("equipoSecret");
        this.variablesApi.authorizationToken = localStorage.getItem("authorizationToken");
        this._maxDate = new Date();

        if (localStorage.getItem("telefonoId") !== "undefined" && localStorage.getItem("telefonoId") !== null) {
            this.variablesApi.ingreso.usuario.telefonoId = localStorage.getItem("telefonoId");
        } else {
            this.variablesApi.ingreso.usuario.telefonoId = "";
        }

    }
    //#region Ingreso

    async ingresoEquipoCrear(equipoSecretA: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_EQUIPO_CREAR_NUEVO_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_EQUIPO_CREAR_NUEVO_QA;
        }

        let datosDeEquipo = JSON.parse(localStorage.datosEquipo);
        let bodyStr = datosDeEquipo.nameOs + "_" + datosDeEquipo.nameBrowser + "_" + datosDeEquipo.majorVersionBrowser;
        let equipoId = localStorage.getItem("EquipoID") == null ? this.apiService.generaIdEquipo(bodyStr, equipoSecretA) : localStorage.getItem("EquipoID");
        localStorage.setItem("EquipoID", equipoId)
        let request: any;

        request = {
            "idEquipo": equipoId,
            "idCliente": "WebEmpresas",
            "pushToken": "eD-dqgI6FEw:APA91bFZ2-jY7DVrUKT1OctVJX1",
            "versionAplicacion": "1.0.4",
            "dataEquipo_SO": datosDeEquipo.nameOs,
            "dataEquipo_SOVersion": datosDeEquipo.majorVersionBrowser,
            "dataEquipo_marca": datosDeEquipo.nameBrowser,
            "dataEquipo_modelo": datosDeEquipo.nameOs + "_" + datosDeEquipo.nameBrowser + "_" + datosDeEquipo.majorVersionBrowser,
            "dataEquipo_isPhysical": "T"
        };

        if (equipoSecretA !== "") {
            request.secretA = equipoSecretA;
        }
        else {
            request.access_token = this.variablesApi.authorizationToken;
        }



        // if (equipoSecretA !== "") {
        //     request = {
        //         "idEquipo": equipoId,
        //         "idCliente": "WebEmpresas",
        //         "pushToken": "eD-dqgI6FEw:APA91bFZ2-jY7DVrUKT1OctVJX1",
        //         "secretA": equipoSecretA,
        //         "versionAplicacion": "1.0.4",
        //         "dataEquipo_SO": datosDeEquipo.nameOs,
        //         "dataEquipo_SOVersion": datosDeEquipo.majorVersionBrowser,
        //         "dataEquipo_marca": datosDeEquipo.nameBrowser,
        //         "dataEquipo_modelo": datosDeEquipo.nameOs + "_" + datosDeEquipo.nameBrowser + "_" + datosDeEquipo.majorVersionBrowser,
        //         "dataEquipo_isPhysical": "T"
        //     };
        // } else if (equipoSecretA == "") {
        //     request = {
        //         "idEquipo": equipoId,
        //         "idCliente": "WebEmpresas",
        //         "pushToken": "eD-dqgI6FEw:APA91bFZ2-jY7DVrUKT1OctVJX1",
        //         "access_token": this.variablesApi.authorizationToken,
        //         "versionAplicacion": "1.0.4",
        //         "dataEquipo_SO": datosDeEquipo.nameOs,
        //         "dataEquipo_SOVersion": datosDeEquipo.majorVersionBrowser,
        //         "dataEquipo_marca": datosDeEquipo.nameBrowser,
        //         "dataEquipo_modelo": datosDeEquipo.nameOs + "_" + datosDeEquipo.nameBrowser + "_" + datosDeEquipo.majorVersionBrowser,
        //         "dataEquipo_isPhysical": "T"
        //     };
        // } else {
        //     request = {
        //         "idEquipo": equipoId,
        //         "idCliente": "WebEmpresas",
        //         "pushToken": "eD-dqgI6FEw:APA91bFZ2-jY7DVrUKT1OctVJX1",
        //         "access_token": this.variablesApi.authorizationToken,
        //         "versionAplicacion": "1.0.4",
        //         "dataEquipo_SO": datosDeEquipo.nameOs,
        //         "dataEquipo_SOVersion": datosDeEquipo.majorVersionBrowser,
        //         "dataEquipo_marca": datosDeEquipo.nameBrowser,
        //         "dataEquipo_modelo": datosDeEquipo.nameOs + "_" + datosDeEquipo.nameBrowser + "_" + datosDeEquipo.majorVersionBrowser,
        //         "dataEquipo_isPhysical": "T"
        //     };
        // }

        this.variablesApi.ingreso.usuario.equipoId = datosDeEquipo.nameOs + "_" + datosDeEquipo.nameBrowser + "_" + datosDeEquipo.majorVersionBrowser;


        return await this.apiService.callApiNoHeaders(url, request).then(response => {
            let retorno = new ResponseTipo();
            if (response.statusCode === "200") {

                retorno.estado = Respuestas.RESPUESTA_OK;

                retorno.data = {
                    tipoToken: this.constantes.TIPOTOKENEQUIPO,
                    authorizationToken: response.data.access_token,
                    codigoRespuesta: response.data.codigoRespuesta
                };

                if (typeof response.data.secretB !== 'undefined')
                    retorno.data.secret = response.data.secretB;
            }
            else if (response.statusCode === 0)
                retorno.estado = Respuestas.RESPUESTA_SC;
            else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: response.userMessage };
            }
            return retorno;
        },
            error => {
                let retorno = this.manejaError(error);
                return retorno;
            }
        );

    }

    async ingresoTelefonoCrear(telefonoId: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TELEFONO_CREAR_NUEVO_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TELEFONO_CREAR_NUEVO_QA;
        }
        var request = {
            telefono: parseInt(telefonoId),
            tipoMensaje: "sms"
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                if (_data.technicalMessage !== "OK") {
                    retorno.estado = Respuestas.RESPUESTA_NOK;
                    retorno.data = { message: _data.userMessage };
                } else {
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    retorno.data = { smsToken: _data.data.access_token };
                }
            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async ingresoTelefonoLlamada(telefonoId: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TELEFONO_CREAR_NUEVO_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TELEFONO_CREAR_NUEVO_QA;
        }
        var request = {
            telefono: parseInt(telefonoId),
            tipoMensaje: "voice"
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = { smsToken: _data.data.access_token };
            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        },
            error => {
                let retorno = this.manejaError(error);
                return retorno;
            });
    }

    async IngresoTelefonoReenviar(telefonoId: string, smsToken: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_TELEFONO_REENVIAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_TELEFONO_REENVIAR_QA;
        }
        var request = {
            telefonoId: telefonoId,
            smsToken: smsToken
        };

        let cabeceras = new HttpHeaders({
            authorizationtoken: this.variablesApi.authorizationToken,
            "Content-Type": "text/plain"
        });


        return await this.apiService.callApiCript(url, request, cabeceras).then(respuesta => {
            let retorno = new ResponseTipo();
            let _data = JSON.parse(respuesta.text);
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = { smsToken: _data.data.smsToken };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        },
            error => {
                let retorno = this.manejaError(error);
                return retorno;
            });
    }

    async IngresoTelefonoValidar(telefonoId: string, smsToken: string, smsValor: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TELEFONO_VALIDAR_NUEVO_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TELEFONO_VALIDAR_NUEVO_QA;
        }

        var request = {
            sms: smsValor
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                if (_data.technicalMessage === "NOK") {
                    retorno.estado = Respuestas.RESPUESTA_NOK;
                    retorno.data = { message: _data.userMessage };
                } else {
                    this.variablesApi.authorizationToken = _data.data.authorizationToken;
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    let usuario = "";
                    if (_data.data.codigoRespuesta === 4) {
                        usuario = _data.data.usuario.nombre
                    }
                    retorno.data = {
                        authorizationtokenTelefono: _data.data.access_token,
                        codigoRespuesta: _data.data.codigoRespuesta,
                        usuario: usuario
                    }
                }
            }
            else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            }
            else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async IngresoMail(mail: string,): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_MAIL_NUEVO_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_MAIL_NUEVO_QA;
        }

        var request = {
            mail: mail
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                if (_data.data.codigoRespuesta == 3) {
                    retorno.data = {
                        authorizationtokenTelefono: _data.data.access_token,
                        codigoRespuesta: _data.data.codigoRespuesta,
                        nombre: _data.data.usuario.nombre
                    }
                } else {
                    retorno.data = {
                        authorizationtokenTelefono: _data.data.access_token,
                        codigoRespuesta: 4,
                    }
                }

            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        },
            error => {
                let retorno = this.manejaError(error);
                return retorno;
            });
    }

    async ingresoUsuarioConsultarEstado(telefonoId: string, usuarioId: string, numeroDocumento: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_USUARIO_CONSULTAR_ESTADO_NUEVO_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_USUARIO_CONSULTAR_ESTADO_NUEVO_QA;
        }
        var request = {
            rut: usuarioId,
            numeroSerieRut: numeroDocumento
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode === "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = { authorizationtokenUsuario: _data.data.access_token, codigoRespuesta: _data.data.codigoRespuesta, usuario: _data.data.usuario, message: _data.userMessage };
            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async ingresoUsuarioCrear(telefonoId: string, usuarioId: string, nombre: string, apellido: string, mail: string, password: string, numeroDocumento: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_USUARIO_CREAR_NUEVO_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_USUARIO_CREAR_NUEVO_QA;
        }

        var request = {
            nombre: nombre,
            apellido: apellido,
            password: password
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            console.log(_data)
            let retorno = new ResponseTipo();
            if (_data.statusCode === "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = { authorizationtokenUsuario: _data.data.access_token };
            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async ingresoUsuarioModificar(telefonoId: string, usuarioId: string, password: string, numeroDocumento: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_USUARIO_MODIFICAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_USUARIO_MODIFICAR_QA;
        }
        var request = {
            telefonoId: telefonoId,
            usuarioId: usuarioId,
            password: password,
            numeroDocumento: numeroDocumento
        };

        let cabeceras = new HttpHeaders({
            authorizationtoken: this.variablesApi.authorizationToken,
            "Content-Type": "text/plain"
        });


        return await this.apiService.callApiCript(url, request, cabeceras).then(respuesta => {
            let retorno = new ResponseTipo();
            let _data = JSON.parse(respuesta.text);
            if (_data.code === "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        }
        );
    }

    async ingresoUsuarioValidar(telefonoId: string, usuarioId: string, password: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_USUARIO_VALIDAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_USUARIO_VALIDAR_QA;
        }
        var request = {
            telefonoId: telefonoId,
            usuarioId: usuarioId,
            password: password
        };

        let cabeceras = new HttpHeaders({
            authorizationtoken: this.variablesApi.authorizationToken,
            "Content-Type": "text/plain"
        });


        return await this.apiService.callApiCript(url, request, cabeceras).then(respuesta => {
            let retorno = new ResponseTipo();
            let _data = JSON.parse(respuesta.text);

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = { authorizationtokenUsuario: _data.data.authorizationToken };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        }
        );
    }

    async ingresoUsuarioValidarLogin(telefonoId: string, password: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_USUARIO_VALIDAR_LOGIN_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_USUARIO_VALIDAR_LOGIN_QA;
        }
        var request = {
            telefonoId: telefonoId,
            password: password
        };

        let cabeceras = new HttpHeaders({
            authorizationtoken: this.variablesApi.authorizationToken,
            "Content-Type": "text/plain"
        });


        return await this.apiService.callApiCript(url, request, cabeceras).then(respuesta => {
            let retorno = new ResponseTipo();
            let _data = JSON.parse(respuesta.text);

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = { authorizationtokenUsuario: _data.data.authorizationToken };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        }
        );
    }

    async ingresoPinCrear(telefonoId: string, usuarioId: string, pinValor: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_PIN_CREAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_PIN_CREAR_QA;
        }
        var request = {
            telefonoId: telefonoId,
            usuarioId: usuarioId,
            pinValor: pinValor
        };

        let cabeceras = new HttpHeaders({
            authorizationtoken: this.variablesApi.authorizationToken,
            "Content-Type": "text/plain"
        });


        return await this.apiService.callApiCript(url, request, cabeceras).then(respuesta => {
            let retorno = new ResponseTipo();
            let _data = JSON.parse(respuesta.text);
            if (_data.code === "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        }
        );
    }

    async ingresoPinValidar(telefonoId: string, usuarioId: string, pinValor: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_PIN_VALIDAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_INGRESO_PIN_VALIDAR_QA;
        }
        var request = {
            telefonoId: telefonoId,
            usuarioId: usuarioId,
            pinValor: pinValor
        };

        let cabeceras = new HttpHeaders({
            authorizationtoken: this.variablesApi.authorizationToken,
            "Content-Type": "text/plain"
        });


        return await this.apiService.callApiCript(url, request, cabeceras).then(respuesta => {
            let retorno = new ResponseTipo();
            let _data = JSON.parse(respuesta.text);

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = { authorizationTokenPin: _data.data.authorizationToken };

            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        }
        );
    }

    async obtenerWeb(esPrimerIngreso: boolean = false, aceptaTerminos: boolean = false): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_USUARIO_OBTENER_PR : this.constantes.COPEC_EMPRESAS_USUARIO_OBTENER_QA

        var request = {};

        if (esPrimerIngreso)
            request['cuentaConfigurada'] = 1;

        if (aceptaTerminos)
            request['aceptaTerminos'] = 1;

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.code == "200") {

                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = this.crearObjetoUsuarioObtenerIngreso(_data.data); //original crearObjetoUsuarioObtener

            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.data = { message: _data.userMessage };
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }


    async ingresoObtenerMaestros(cuentaId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_OBTENER_MAESTROS_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_OBTENER_MAESTROS_QA;
        }

        var request = {
            cuentaId: cuentaId
        };


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = this.crearMaestros(_data.data);

            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async IngresoTokenPost(password: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TOKEN_POST_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TOKEN_POST_QA;
        }

        var request = {
            password: password.substring(0, 4)
        };


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                if (_data.technicalMessage !== "OK") {
                    retorno.estado = Respuestas.RESPUESTA_NOK;
                    retorno.data = { message: _data.userMessage };
                } else {
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    retorno.data = { authorizationtokenTelefono: _data.data.access_token }
                }
            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async IngresoTokenMailPost(): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TOKENMAIL_POST_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TOKENMAIL_POST_QA;
        }

        var request = {};


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                if (_data.technicalMessage !== "") {
                    retorno.estado = Respuestas.RESPUESTA_NOK;
                    retorno.data = { message: _data.userMessage };
                } else {
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    retorno.data = {
                        authorizationtokenTelefono: _data.data.access_token,
                        codigoRespuesta: _data.data.codigoRespuesta,
                        nombre: _data.data.usuario.nombre,
                        mail: _data.data.usuario.mail
                    }
                }

            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async IngresoTokenTelefonoPost(rut: string, numeroSerieRut: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TOKENTELEFONO_POST_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TOKENTELEFONO_POST_QA;
        }

        var request = {
            rut: rut,
            numeroSerieRut: numeroSerieRut,
            tipoMensaje: 'sms'
        };


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            console.log(_data);
            if (_data.statusCode == "200") {
                if (_data.technicalMessage !== "") {
                    retorno.estado = Respuestas.RESPUESTA_NOK;
                    retorno.data = { message: _data.userMessage };
                } else {
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    retorno.data = {
                        codigoRespuesta: _data.data.codigoRespuesta,
                        nombre: _data.data.usuario.nombre,
                        mail: _data.data.usuario.mail,
                        telefono: _data.data.usuario.telefono,
                    }
                }

            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async IngresoTokenPassPost(codigo: string, password: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TOKENPASS_POST_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_INGRESO_TOKENPASS_POST_QA;
        }

        var request = {
            codigo: codigo,
            password: password
        };


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                if (_data.technicalMessage !== "") {
                    retorno.estado = Respuestas.RESPUESTA_NOK;
                    retorno.data = { message: _data.userMessage };
                } else {
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    retorno.data = {
                        codigoRespuesta: _data.data.codigoRespuesta,
                        nombre: _data.data.usuario.nombre,
                        mail: _data.data.usuario.mail
                    }
                }
            } else if (_data.statusCode === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    //#endregion

    //#region Home
    async homeCrearCuentaCorriente(cuentaId: number, cuentaCorrienteNumero: number, cuentaCorrienteRut: string, cuentaCorrienteNombreTitular: string, cuentaCorrienteEmailTitular: string, cuentaCorrienteNombre: string, cuentaCorrienteTipo: number, bancoId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CREA_CUENTA_CORRIENTE_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CREA_CUENTA_CORRIENTE_QA;
        }

        var request = {
            cuentaId: cuentaId,
            cuentaCorrienteNumero: cuentaCorrienteNumero,
            cuentaCorrienteRut: cuentaCorrienteRut,
            cuentaCorrienteNombreTitular: cuentaCorrienteNombreTitular,
            cuentaCorrienteEmailTitular: cuentaCorrienteEmailTitular,
            cuentaCorrienteNombre: cuentaCorrienteNombre,
            cuentaCorrienteTipo: cuentaCorrienteTipo,
            bancoId: bancoId
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCrearCuentaEmpresa(cuentaNombre: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CREAR_CUENTA_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CREAR_CUENTA_QA;
        }

        var request = {
            cuentaNombre: cuentaNombre
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    cuentaId: _data.data.cuentaCreadaId
                };

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeModificarCuentaEmpresa(cuenta: Cuenta, estado: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_MODIFICAR_CUENTA_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_MODIFICAR_CUENTA_QA;
        }

        var request = {
            cuentaId: cuenta.cuentaId,
            cuentaNombre: cuenta.cuentaNombre,
            cuentaEstadoId: estado
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeConsultarDatosFacturacion(cuentaId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CONSULTAR_DATOS_FACTURACION_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CONSULTAR_DATOS_FACTURACION_QA;
        }

        var request = {
            cuentaId: cuentaId
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    actividadesEconomicas: _data.data.actividadesEconomicas,
                    periodosFacturacion: _data.data.periodosFacturacion,
                    distritos: _data.data.distritos,
                    datosCuenta: _data.data.cuentaDatosDte
                };

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCrearDatosFacturacion(cuentaId: number, datosFacturacionRut: string, datosFacturacionEmail: string, datosFacturacionGiro: string, datosFacturacionDistrito: string, datosFacturacionDireccion: string, datosFacturacionRazonSocial: string, datosFacturacionCodigoPostal: string, datosFacturacionNombreFantasia: string, datosFacturacionActividadEconomica: string, datosFacturacionPeriodoFacturacion: string, restringeEleccionDte: string, dtePuedeFacturar: boolean, dteNserie: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CREAR_DATOS_FACTURACION_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CREAR_DATOS_FACTURACION_QA;
        }

        var request = {
            cuentaId: cuentaId,
            datosFacturacionRut: datosFacturacionRut,
            datosFacturacionEmail: datosFacturacionEmail,
            datosFacturacionGiro: datosFacturacionGiro,
            datosFacturacionDistrito: datosFacturacionDistrito,
            datosFacturacionDireccion: datosFacturacionDireccion,
            datosFacturacionRazonSocial: datosFacturacionRazonSocial,
            datosFacturacionNombreFantasia: datosFacturacionNombreFantasia,
            datosFacturacionActividadEconomica: parseInt(datosFacturacionActividadEconomica),
            datosFacturacionPeriodoFacturacion: parseInt(datosFacturacionPeriodoFacturacion),
            restringeEleccionDte: parseInt(restringeEleccionDte),
            dtePuedeFacturar: dtePuedeFacturar ? 1 : 0,
            dteNumeroSerie: dteNserie,
            dteDeclaracion: 1
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCambiarCuentaSeleccionada(cuentaId: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_CUENTA_MODIFICAR_PR : this.constantes.COPEC_EMPRESAS_CUENTA_MODIFICAR_QA;

        var request = {
            cuentaId: cuentaId
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioInvitarConductor(cuentaId: number, rolId: number, invitacionTelefonoId: string, invitacionNombre: string, invitacionApellido: string, estadoCombustible: number, montoMaximoDiaValor: number, montoMaximoMesValor: number, usuarioCuentaConduce: boolean): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CUENTA_INVITAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CUENTA_INVITAR_QA;
        }

        let request = {
            cuentaId: cuentaId,
            rolId: rolId,
            invitacionTelefonoId: invitacionTelefonoId,
            invitacionNombre: invitacionNombre,
            invitacionApellido: invitacionApellido,
            invitacionConduce: usuarioCuentaConduce ? 1 : 0,
            restriccionCombustible: {
                montosMaximosEstado: [estadoCombustible],
                montoMaximoDiaValor: montoMaximoDiaValor > 0 ? [montoMaximoDiaValor] : [0],
                montoMaximoMesValor: montoMaximoMesValor > 0 ? [montoMaximoMesValor] : [0],
                permiteComprarValor: [estadoCombustible]
            }
        }

        return await this.apiService.callApiHeaders(url, request).then(_data => {

            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;

        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });


    }

    async homeUsuarioInvitarConductores(cuentaId: number, invitaciones: IInvitacionConductor[]): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_CUENTA_INVITAR_PR : this.constantes.COPEC_EMPRESAS_CUENTA_INVITAR_QA;

        let request = {
            invitaciones: []
        }

        invitaciones.forEach(i => {
            let inv: any = {
                cuentaId: cuentaId,
                rolId: 4,
                invitacionTelefonoId: '569' + i.telefono,
                invitacionNombre: i.nombre,
                invitacionApellido: i.apellido,
                invitacionConduce: 1,
                vehiculosAsignados: i.vehiculos.map(v => v.vehiculoId),

            }

            inv.restriccionCombustible = {
                permiteComprarValor: [i.combustible.permiteComprar ? 1 : 0],
                montosMaximosEstado: 1,
                montoMaximoDiaValor: i.combustible.permiteComprar ? [parseInt(i.combustible.diario)] : [0],
                montoMaximoMesValor: i.combustible.permiteComprar ? [parseInt(i.combustible.mensual)] : [0]
            }

            request.invitaciones.push(inv);
        });

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {userMessage:_data.userMessage};
            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    //async homeUsuarioInvitar(cuentaId: number, rolId: number, invitacionTelefonoId: string, invitacionNombre: string, invitacionApellido: string, invitacionSolicitaFoto: number, invitacionSolicitaLicencia: number): Promise<ResponseTipo> {
    // async homeUsuarioInvitar(cuentaId: number, rolId: number, invitacionTelefonoId: string, invitacionNombre: string, invitacionApellido: string, estadoCombustible: number, montoMaximoDiaValor: number, montoMaximoMesValor: number, usuarioCuentaConduce: boolean): Promise<ResponseTipo> {
    //     let url = "";
    //     if (environment.Ambiente === "PR") {
    //         url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_INVITAR_PR;
    //     } else {
    //         url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_INVITAR_QA;
    //     }
    //     var request;
    //     if (typeof estadoCombustible !== 'undefined') {
    //         request = {
    //             cuentaId: cuentaId,
    //             rolId: rolId,
    //             invitacionConduce: usuarioCuentaConduce ? 1 : 0,
    //             invitacionTelefonoId: invitacionTelefonoId,
    //             invitacionNombre: invitacionNombre,
    //             invitacionApellido: invitacionApellido,
    //             restriccionCombustible: {
    //                 estado: estadoCombustible,
    //                 montoMaximoDiaValor: [montoMaximoDiaValor],
    //                 montoMaximoMesValor: [montoMaximoMesValor]
    //             }
    //         };
    //     } else {
    //         request = {
    //             cuentaId: cuentaId,
    //             rolId: rolId,
    //             invitacionConduce: usuarioCuentaConduce ? 1 : 0,
    //             invitacionTelefonoId: invitacionTelefonoId,
    //             invitacionNombre: invitacionNombre,
    //             invitacionApellido: invitacionApellido,
    //             restriccionCombustible: {
    //                 estado: 0,
    //                 montoMaximoDiaValor: [],
    //                 montoMaximoMesValor: []
    //             }
    //         };
    //     }


    //     return await this.apiService.callApiHeaders(url, request).then(_data => {
    //         let retorno = new ResponseTipo();


    //         if (_data.code == "200") {
    //             retorno.estado = Respuestas.RESPUESTA_OK;
    //             retorno.data = {};

    //         } else if (_data.code == "201") {
    //             retorno.estado = Respuestas.RESPUESTA_OK;
    //             retorno.data = {
    //                 message: _data.userMessage
    //             };
    //         } else if (_data.code === 0) {
    //             retorno.estado = Respuestas.RESPUESTA_SC;
    //         } else {
    //             retorno.estado = Respuestas.RESPUESTA_SA;
    //             retorno.data = { message: _data.userMessage };
    //         }
    //         return retorno
    //     },error => {
    //         let retorno = this.manejaError(error);
    //         return retorno;
    //     });
    // }

    async homeUsuarioAceptaInvitacion(invitacionId: number, invitacionAceptada: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_VALIDA_INVITACION_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_VALIDA_INVITACION_QA;
        }

        var request = {
            invitacionId: invitacionId,
            invitacionAceptada: invitacionAceptada
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioCancelaInvitacion(cuentaId: number, invitacionId: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_USUARIO_CANCELA_INVITACION_PR : this.constantes.COPEC_EMPRESAS_USUARIO_CANCELA_INVITACION_QA;

        var request = {
            cuentaId: cuentaId,
            invitacionId: invitacionId
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioAutorizaInvitacion(cuentaId: number, invitacionId: number, invitacionAutorizada: number, combustibleEstado: number, combustibleMontoMaximoDiaValor: number, combustibleMontoMaximoMesValor: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_AUTORIZA_INVITACION_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_AUTORIZA_INVITACION_QA;
        }

        var request = {
            cuentaId: cuentaId,
            invitacionId: invitacionId,
            invitacionAutorizada: invitacionAutorizada,
            combustible: {
                estado: combustibleEstado,
                montoMaximoDiaValor: [combustibleMontoMaximoDiaValor],
                montoMaximoMesValor: [combustibleMontoMaximoMesValor]
            }
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioConsultarInvitacion(cuentaId: number, posicionInicial: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CONSULTAR_INVITACIONES_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CONSULTAR_INVITACIONES_QA;
        }

        var request = {
            cuentaId: cuentaId,
            posicionInicial: posicionInicial
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let invitaciones: Invitacion[] = [];

                _data.data.conductoresInvitados.forEach(invita => {
                    let _invitacionValidacion = new InvitacionValidaciones()
                    let _invitacion = new Invitacion();

                    _invitacionValidacion.invitacionNombre = invita.invitacionDatos.invitacionNombre;
                    _invitacionValidacion.invitacionApellido = invita.invitacionDatos.invitacionApellido;
                    // _invitacionValidacion.invitacionSolicitaFoto = invita.invitacionDatos.invitacionSolicitaFoto;
                    // _invitacionValidacion.invitacionSolicitaLicencia = invita.invitacionDatos.invitacionSolicitaLicencia;


                    if (invita.invitacionDatos != undefined) {
                        _invitacion.vehiculosAsignados = invita.invitacionDatos.vehiculosAsignados != undefined ? invita.invitacionDatos.vehiculosAsignados : [];

                        if (invita.invitacionDatos.restriccionCombustible != undefined) {
                            _invitacion.activaCombustible = invita.invitacionDatos.restriccionCombustible.permiteComprarValor[0] == 1;
                            _invitacion.montosMaximosEstado = invita.invitacionDatos.restriccionCombustible.montosMaximosEstado[0] == 1;
                            _invitacion.combustibleMontoDiario = invita.invitacionDatos.restriccionCombustible.montoMaximoDiaValor[0]; //Array.isArray(invita.invitacionDatos.restriccionCombustible.montoMaximoDiaValor) ? invita.invitacionDatos.restriccionCombustible.montoMaximoDiaValor[0] : invita.invitacionDatos.restriccionCombustible.montoMaximoDiaValor;
                            _invitacion.combustibleMontoMensual = invita.invitacionDatos.restriccionCombustible.montoMaximoMesValor[0];
                        }
                    }

                    _invitacion.invitacionValidaciones = _invitacionValidacion;
                    _invitacion.invitacionId = invita.invitacionId;
                    _invitacion.invitacionFechaCaducidad = invita.invitacionFechaCaducidad;
                    _invitacion.invitacionTelefonoInvitadoId = invita.invitacionTelefonoInvitadoId;
                    _invitacion.invitacionRolId = invita.rolId;
                    _invitacion.invitacionRol = invita.rolNombre;
                    _invitacion.invitacionEstadoInvitado = invita.invitacionEstadoId;

                    invitaciones.push(_invitacion);
                });

                let pagination = {
                    registrosPorPagina: _data.data.cantidadRegistrosPorConsulta,
                    posicionFinal: _data.data.posicionFinal,
                    registrosObtenidos: invitaciones.length

                    //totalRegistros: _data.data.cantidadTotalTrx,
                }

                retorno.data = {
                    invitados: invitaciones,
                    pagination: pagination
                };

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioConsultarRestricciones(cuentaId: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_USUARIO_RESTRICCIONES_OBTENER_PR : this.constantes.COPEC_EMPRESAS_USUARIO_RESTRICCIONES_OBTENER_QA;

        var request = {
            cuentaId: cuentaId
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let result: [] = [];

                if (_data.data.restriccionesCombustible != undefined) {
                    result = _data.data.restriccionesCombustible.map(x => {
                        return {
                            usuarioId: x.usuarioId,
                            tipoRestriccionId: x.tipoRestriccionId,
                            restriccionValor: x.restriccionUsuarioValor
                        }
                    });
                }

                retorno.data = {
                    restricciones: result
                };

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioConsultarUsuarios2(cuentaId: number, posicionInicial: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_USUARIO_CONDUCTORES_OBTENER_PR : this.constantes.COPEC_EMPRESAS_USUARIO_CONDUCTORES_OBTENER_QA;

        var request = {
            cuentaId: cuentaId,
            posicionInicial: posicionInicial
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let invitaciones: Invitacion[] = [];
                let conductores: Conductor[] = [];

                _data.data.conductores.forEach(c => {

                    let conductor: Conductor = Conductor.crearObjetoConductor(c, null);

                    const restriccionesCombustible = _data.data.restriccionesCombustible.filter(x => x.usuarioId === conductor.usuarioId);
                    conductor = Conductor.cargarRestriccionesDiariasMensuales(conductor, restriccionesCombustible);

                    conductores.push(conductor);
                });

                let pagination = {
                    registrosPorPagina: _data.data.cantidadRegistrosPorConsulta,
                    totalRegistros: _data.data.cantidadTotalTrx,
                    posicionFinal: _data.data.posicionFinal,

                    registrosObtenidos: _data.data.conductores.length
                }

                conductores.sort((a, b) => a.apellido.localeCompare(b.apellido));
                retorno.data = {
                    //invitados: invitaciones,
                    conductores: conductores,
                    tieneInvitaciones: _data.data.tieneInvitaciones != undefined && _data.data.tieneInvitaciones > 0,
                    pagination: pagination
                };

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioConsultarUsuarios(cuentaId: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_USUARIO_INVITACIONES_OBTENER_PR : this.constantes.COPEC_EMPRESAS_USUARIO_INVITACIONES_OBTENER_QA;

        var request = {
            cuentaId: cuentaId
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let invitaciones: Invitacion[] = [];
                let conductores: Conductor[] = [];

                // _data.data.conductoresInvitados.forEach(invita => {
                //     let _invitacionValidacion = new InvitacionValidaciones()
                //     let _invitacion = new Invitacion();
                //     _invitacionValidacion.invitacionNombre = invita.invitacionDatos.invitacionNombre;
                //     _invitacionValidacion.invitacionApellido = invita.invitacionDatos.invitacionApellido;
                //     // _invitacionValidacion.invitacionSolicitaFoto = invita.invitacionDatos.invitacionSolicitaFoto;
                //     // _invitacionValidacion.invitacionSolicitaLicencia = invita.invitacionDatos.invitacionSolicitaLicencia;


                //     if(invita.invitacionDatos != undefined){
                //         _invitacion.vehiculosAsignados = invita.invitacionDatos.vehiculosAsignados != undefined ? invita.invitacionDatos.vehiculosAsignados : [];

                //         if (invita.invitacionDatos.restriccionCombustible != undefined) {
                //             _invitacion.activaCombustible = invita.invitacionDatos.restriccionCombustible.permiteComprarValor[0] == 1;
                //             _invitacion.montosMaximosEstado = invita.invitacionDatos.restriccionCombustible.montosMaximosEstado[0] == 1;
                //             _invitacion.combustibleMontoDiario = invita.invitacionDatos.restriccionCombustible.montoMaximoDiaValor[0]; //Array.isArray(invita.invitacionDatos.restriccionCombustible.montoMaximoDiaValor) ? invita.invitacionDatos.restriccionCombustible.montoMaximoDiaValor[0] : invita.invitacionDatos.restriccionCombustible.montoMaximoDiaValor;
                //             _invitacion.combustibleMontoMensual = invita.invitacionDatos.restriccionCombustible.montoMaximoMesValor[0];
                //         }
                //     }

                //     _invitacion.invitacionValidaciones = _invitacionValidacion;
                //     _invitacion.invitacionId = invita.invitacionId;
                //     _invitacion.invitacionFechaCaducidad = invita.invitacionFechaCaducidad;
                //     _invitacion.invitacionTelefonoInvitadoId = invita.invitacionTelefonoInvitadoId;
                //     _invitacion.invitacionRolId = invita.rolId;
                //     _invitacion.invitacionRol = invita.rolNombre;
                //     _invitacion.invitacionEstadoInvitado = invita.invitacionEstadoId;

                //     invitaciones.push(_invitacion);
                // });
                // invitaciones.sort((a, b) => b.invitacionId - a.invitacionId);

                _data.data.conductores.forEach(c => {
                    // let diasResult = _data.data.generales.diaHabilitado.filter(x => x.usuarioId == c.usuarioId)
                    // let horasResult = _data.data.generales.horarioHabilitado.filter(x => x.usuarioId == c.usuarioId)

                    // c.restriccionGeneral = {
                    //     diasHabilitado: diasResult.length > 0 ? diasResult[0].diaHabilitado : [],
                    //     horarioHabilitado: horasResult.length > 0 ? horasResult[0].horarioHabilitado : []
                    // }

                    let conductor: Conductor = Conductor.crearObjetoConductor(c, null);
                    //conductor = Conductor.cargarRestriccionesDiariasMensuales(conductor, _data.data.combustible);
                    conductores.push(conductor);
                });

                conductores.sort((a, b) => a.apellido.localeCompare(b.apellido));
                retorno.data = {
                    //invitados: invitaciones,
                    conductores: conductores,
                    tieneInvitaciones: _data.data.tieneInvitaciones && _data.data.tieneInvitaciones > 0
                };

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    /** modifica un conductor */
    async homeUsuarioModificar2(
        cuentaId: number,
        usuarioId: number,
        estado: number,
        rolId: number,
        configuracionCombustible?: IConfiguracionCombustible,
        restriccionesCombustible?: IRestriccionesCombustible,
    ) {

        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_USUARIO_MODIFICAR_PR : this.constantes.COPEC_EMPRESAS_USUARIO_MODIFICAR_QA;

        let request: any;

        request = {
            cuentaId: cuentaId,
            usuarioId: usuarioId,
            rolId: rolId,
        }

        if (rolId != 1)
            request.usuarioCuentaEstadoId = estado

        // AJUSTES DIARIO MENSUAL
        if (configuracionCombustible != undefined && configuracionCombustible.estadoCombustible != undefined && configuracionCombustible.permiteComprar != undefined) {
            request.restriccionCombustible = {
                montosMaximosEstado: rolId == 1 ? (configuracionCombustible.estadoCombustible ? 1 : 0) : 1,  // configuracionCombustible.estadoCombustible ? 1 : 0,
                montoMaximoDiaValor: [configuracionCombustible.montoMaximoDiaValor < 0 ? 0 : configuracionCombustible.montoMaximoDiaValor],
                montoMaximoMesValor: [configuracionCombustible.montoMaximoMesValor < 0 ? 0 : configuracionCombustible.montoMaximoMesValor]
            }

            request.restriccionCombustible.permiteComprarValor = rolId == 1 ? [1] : (configuracionCombustible.permiteComprar ? [1] : [0])
        }

        // RESTRICCIONES
        if (restriccionesCombustible) {
            request.restriccionGeneral = {};

            // DIAS
            if (restriccionesCombustible.diasHabilitar) {
                request.restriccionGeneral.diaHabilitadoEstado = restriccionesCombustible.diasHabilitar ? 1 : 0;
                request.restriccionGeneral.diaHabilitadoValor = restriccionesCombustible.diasHabilitados;
            }

            // HORARIOS
            if (restriccionesCombustible.horarioHabilitar) {
                request.restriccionGeneral.horarioHabilitadoEstado = restriccionesCombustible.horarioHabilitar ? 1 : 0;
                request.restriccionGeneral.horarioHabilitadoValor = restriccionesCombustible.horariosHabilitados;
            }

            // ESTACIONES
            if (restriccionesCombustible.estacionHabilitar) {
                request.restriccionGeneral.estacionNoHabilitadaEstado = restriccionesCombustible.estacionHabilitar ? 1 : 0;
                request.restriccionGeneral.estacionNoHabilitadaValor = restriccionesCombustible.estacionesHabilitadas;
            }
        }

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });

    }

    async homeUsuarioModificar(cuentaId: number, usuarioId: string, grupoCuentaId: number, usuarioCuentaEstadoId: number, rolId: number, usuarioCuentaConduce: number, usuarioCuentaRindeGasto: number, usuarioCuentaCreaVehiculo: number, usuarioCuentaEligeDTE: number, usuarioCuentaSeleccionado: number, vehiculosAsignados: any, estadoCombustible: boolean, montoMaximoDiaValor: number, montoMaximoMesValor: number): Promise<ResponseTipo> {

        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_MODIFICAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_MODIFICAR_QA;
        }

        var request: any;
        if (grupoCuentaId > 0) {
            if (estadoCombustible) {
                if (rolId == 1) {
                    request = {
                        cuentaId: cuentaId,
                        usuarioId: usuarioId,
                        grupoCuentaId: grupoCuentaId,
                        rolId: rolId,
                        restriccionCombustible: {
                            estado: estadoCombustible ? 1 : 0,
                            montoMaximoDiaValor: [montoMaximoDiaValor < 0 ? 0 : montoMaximoDiaValor],
                            montoMaximoMesValor: [montoMaximoMesValor < 0 ? 0 : montoMaximoMesValor]
                        }
                    };
                } else {
                    request = {
                        cuentaId: cuentaId,
                        usuarioId: usuarioId,
                        grupoCuentaId: grupoCuentaId,
                        usuarioCuentaEstadoId: usuarioCuentaEstadoId,
                        rolId: rolId,
                        usuarioCuentaConduce: usuarioCuentaConduce,
                        restriccionCombustible: {
                            estado: estadoCombustible ? 1 : 0,
                            montoMaximoDiaValor: [montoMaximoDiaValor < 0 ? 0 : montoMaximoDiaValor],
                            montoMaximoMesValor: [montoMaximoMesValor < 0 ? 0 : montoMaximoMesValor]
                        }
                    };
                }
            } else {
                request = {
                    cuentaId: cuentaId,
                    usuarioId: usuarioId,
                    grupoCuentaId: grupoCuentaId,
                    usuarioCuentaEstadoId: usuarioCuentaEstadoId,
                    rolId: rolId,
                    usuarioCuentaConduce: usuarioCuentaConduce,
                };
            }
        } else {
            if (estadoCombustible) {
                if (rolId == 1) {
                    request = {
                        cuentaId: cuentaId,
                        usuarioId: usuarioId,
                        rolId: rolId,
                        restriccionCombustible: {
                            estado: estadoCombustible ? 1 : 0,
                            montoMaximoDiaValor: [montoMaximoDiaValor < 0 ? 0 : montoMaximoDiaValor],
                            montoMaximoMesValor: [montoMaximoMesValor < 0 ? 0 : montoMaximoMesValor]
                        }
                    };
                } else {
                    if (rolId == 1) {
                        request = {
                            cuentaId: cuentaId,
                            usuarioId: usuarioId,
                            rolId: rolId,
                            restriccionCombustible: {
                                estado: estadoCombustible ? 1 : 0,
                                montoMaximoDiaValor: [montoMaximoDiaValor < 0 ? 0 : montoMaximoDiaValor],
                                montoMaximoMesValor: [montoMaximoMesValor < 0 ? 0 : montoMaximoMesValor]
                            }
                        };
                    } else {
                        request = {
                            cuentaId: cuentaId,
                            usuarioId: usuarioId,
                            usuarioCuentaEstadoId: usuarioCuentaEstadoId,
                            rolId: rolId,
                            usuarioCuentaConduce: usuarioCuentaConduce,
                            restriccionCombustible: {
                                estado: estadoCombustible ? 1 : 0,
                                montoMaximoDiaValor: [montoMaximoDiaValor < 0 ? 0 : montoMaximoDiaValor],
                                montoMaximoMesValor: [montoMaximoMesValor < 0 ? 0 : montoMaximoMesValor]
                            }
                        };
                    }
                }
            } else {
                if (rolId == 1) {
                    request = {
                        cuentaId: cuentaId,
                        usuarioId: usuarioId,
                        rolId: rolId,
                        restriccionCombustible: {
                            estado: estadoCombustible ? 1 : 0,
                            montoMaximoDiaValor: [montoMaximoDiaValor < 0 ? 0 : montoMaximoDiaValor],
                            montoMaximoMesValor: [montoMaximoMesValor < 0 ? 0 : montoMaximoMesValor]
                        }
                    };
                } else {
                    request = {
                        cuentaId: cuentaId,
                        usuarioId: usuarioId,
                        usuarioCuentaEstadoId: usuarioCuentaEstadoId,
                        rolId: rolId,
                        usuarioCuentaConduce: usuarioCuentaConduce,
                    };
                }
            }
        }




        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeVehiculoCrear(cuentaId: number, vehiculo: Vehiculo) {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_VEHICULO_CREAR_PR : this.constantes.COPEC_EMPRESAS_VEHICULO_CREAR_QA;

        var request = {
            cuentaId: cuentaId,
            vehiculoCuentaPatente: vehiculo.patente.toUpperCase(),
            vehiculoCuentaAlias: vehiculo.alias,
            vehiculoCuentaTipo: vehiculo.tipo,
            vehiculoCuentaDeclaracion: 1,

            restriccionOdometro: {
                exigeOdometroEstado: vehiculo.requireOdometro ? 1 : 0,
                exigeOdometroValor: vehiculo.requireOdometro ? [vehiculo.frecuenciaOdometro] : [1]
            },
        }

        // if(vehiculo.requireOdometro)
        //     request.restriccionOdometro['exigeOdometroValor'] = [vehiculo.frecuenciaOdometro]

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });

    }

    /** Obsoleto */
    // async homeVehiculoCrear(cuentaId: number, flotaCuentaId: number, vehiculoCuentaPatente: string, vehiculoCuentaModelo: string, vehiculoCuentaMarca: string, vehiculoCuentaAno: number, vehiculoCuentaTanque: number, vehiculoCuentaAlias: string, vehiculoCuentaTipo: number): Promise<ResponseTipo> {

    //     let url = "";
    //     if (environment.Ambiente === "PR") {
    //         url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_VEHICULO_CREAR_PR;
    //     } else {
    //         url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_VEHICULO_CREAR_QA;
    //     }

    //     var request = {};

    //     request["cuentaId"] = cuentaId;
    //     request["vehiculoCuentaPatente"] = vehiculoCuentaPatente;
    //     request["vehiculoCuentaAlias"] = vehiculoCuentaAlias;

    //     if (flotaCuentaId !== 0)
    //     request["flotaCuentaId"] = flotaCuentaId;
    //     if (vehiculoCuentaModelo !== "")
    //     request["vehiculoCuentaModelo"] = vehiculoCuentaModelo;
    //     if (vehiculoCuentaMarca !== "")
    //     request["vehiculoCuentaMarca"] = vehiculoCuentaMarca;
    //     if (vehiculoCuentaAno >= 0)
    //     request["vehiculoCuentaAno"] = vehiculoCuentaAno;
    //     if (vehiculoCuentaTanque >= 0)
    //     request["vehiculoCuentaTanque"] = vehiculoCuentaTanque;
    //     if (vehiculoCuentaTipo >= 0)
    //     request["vehiculoCuentaTipo"] = vehiculoCuentaTipo;




    //     return await this.apiService.callApiHeaders(url, request).then(_data => {
    //         let retorno = new ResponseTipo();


    //         if (_data.code == "200") {
    //             retorno.estado = Respuestas.RESPUESTA_OK;
    //             retorno.data = {};

    //         } else if (_data.code == "201") {
    //             retorno.estado = Respuestas.RESPUESTA_OK;
    //             retorno.data = {
    //                 message: _data.userMessage
    //             };
    //         } else if (_data.code === 0) {
    //             retorno.estado = Respuestas.RESPUESTA_SC;
    //         } else {
    //             retorno.estado = Respuestas.RESPUESTA_SA;
    //             retorno.data = { message: _data.userMessage };
    //         }
    //         return retorno
    //     },error => {
    //         let retorno = this.manejaError(error);
    //         return retorno;
    //     });
    // }

    async homeVehiculoModificar(cuentaId: number, estado: number, vehiculo: Vehiculo): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_VEHICULO_MODIFICAR_PR : this.constantes.COPEC_EMPRESAS_VEHICULO_MODIFICAR_QA;

        let request = {
            cuentaId: cuentaId,
            vehiculoCuentaId: vehiculo.vehiculoId,
            vehiculoCuentaPatente: vehiculo.patente,
            vehiculoCuentaAlias: vehiculo.alias,
            vehiculoCuentaEstado: estado,
            vehiculoCuentaTipo: vehiculo.tipo,

            restriccionOdometro: {
                exigeOdometroEstado: vehiculo.requireOdometro ? 1 : 0,
                exigeOdometroValor: [vehiculo.frecuenciaOdometro]
            },
        }

        if (vehiculo.modelo != '')
            request['vehiculoCuentaModelo'] = vehiculo.modelo;
        if (vehiculo.marca != '')
            request['vehiculoCuentaMarca'] = vehiculo.marca;
        if (vehiculo.anio != '')
            request['vehiculoCuentaAno'] = vehiculo.anio;

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    // async homeVehiculoModificar(vehiculoCuentaId: number, cuentaId: number, flotaCuentaId: number, vehiculoCuentaPatente: string, vehiculoCuentaModelo: string, vehiculoCuentaMarca: string, vehiculoCuentaAno: number, vehiculoCuentaTanque: number, vehiculoCuentaAlias: string, vehiculoCuentaTipo: string, vehiculoCuentaValidado: number, vehiculoCuentaEstado: number): Promise<ResponseTipo> {

    //     let url = "";
    //     if (environment.Ambiente === "PR") {
    //         url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_VEHICULO_MODIFICAR_PR;
    //     } else {
    //         url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_VEHICULO_MODIFICAR_QA;
    //     }

    //     let request = {};
    //     request["cuentaId"] = cuentaId;
    //     request["vehiculoCuentaId"] = vehiculoCuentaId;
    //     request["vehiculoCuentaPatente"] = vehiculoCuentaPatente;
    //     request["vehiculoCuentaAlias"] = vehiculoCuentaAlias;

    //     if (flotaCuentaId !== 0)
    //     request["flotaCuentaId"] = flotaCuentaId;
    //     if (vehiculoCuentaModelo !== "")
    //     request["vehiculoCuentaModelo"] = vehiculoCuentaModelo;
    //     if (vehiculoCuentaMarca !== "")
    //     request["vehiculoCuentaMarca"] = vehiculoCuentaMarca;
    //     if (vehiculoCuentaAno > 0)
    //     request["vehiculoCuentaAno"] = vehiculoCuentaAno;
    //     if (vehiculoCuentaTanque >= 0)
    //     request["vehiculoCuentaTanque"] = vehiculoCuentaTanque;
    //     if (vehiculoCuentaTipo !== "")
    //     request["vehiculoCuentaTipo"] = vehiculoCuentaTipo;
    //     if (vehiculoCuentaValidado >= 0)
    //     request["vehiculoCuentaValidado"] = vehiculoCuentaValidado;
    //     if (vehiculoCuentaEstado >= 0)
    //     request["vehiculoCuentaEstado"] = vehiculoCuentaEstado



    //     return await this.apiService.callApiHeaders(url, request).then(_data => {
    //         let retorno = new ResponseTipo();


    //         if (_data.code == "200") {
    //             retorno.estado = Respuestas.RESPUESTA_OK;
    //             retorno.data = {};

    //         } else if (_data.code == "201") {
    //             retorno.estado = Respuestas.RESPUESTA_OK;
    //             retorno.data = {
    //                 message: _data.userMessage
    //             };
    //         } else if (_data.code === 0) {
    //             retorno.estado = Respuestas.RESPUESTA_SC;
    //         } else {
    //             retorno.estado = Respuestas.RESPUESTA_SA;
    //             retorno.data = { message: _data.userMessage };
    //         }
    //         return retorno
    //     },error => {
    //         let retorno = this.manejaError(error);
    //         return retorno;
    //     });
    // }

    async homeVehiculoAsignar(cuentaId: number, usuarioId: number, vehiculos: number[]): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_VEHICULO_ASIGNAR_PR : this.constantes.COPEC_EMPRESAS_VEHICULO_ASIGNAR_QA;

        let request = {
            cuentaId: cuentaId,
            usuarioPorModificar: usuarioId,
            vehiculosPorAsignar: vehiculos
        }

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeVehiculoDesasignar(cuentaId: number, usuarioId: number, vehiculos: number[]): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_VEHICULO_DESASIGNAR_PR : this.constantes.COPEC_EMPRESAS_VEHICULO_DESASIGNAR_QA;

        let request = {
            cuentaId: cuentaId,
            usuarioPorModificar: usuarioId,
            vehiculosPorAsignar: vehiculos
        }

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeFlotaCrear(cuentaId: number, flotaCuentaNombre: string): Promise<ResponseTipo> {

        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_FLOTACUENTA_CREAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_FLOTACUENTA_CREAR_QA;
        }

        var request = {
            cuentaId: cuentaId,
            flotaCuentaNombre: flotaCuentaNombre
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeFlotaModificar(cuentaId: number, flotaCuentaId: number, flotaCuentaNombre: string, flotaCuentaEstado: number): Promise<ResponseTipo> {

        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_FLOTACUENTA_MODIFICAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_FLOTACUENTA_MODIFICAR_QA;
        }

        let request: any;
        if (flotaCuentaEstado === 0) {
            request = {
                cuentaId: cuentaId,
                flotaCuentaId: flotaCuentaId,
                flotaCuentaEstado: flotaCuentaEstado
            };
        } else {
            if (flotaCuentaNombre === "") {
                request = {
                    cuentaId: cuentaId,
                    flotaCuentaId: flotaCuentaId,
                    flotaCuentaEstado: flotaCuentaEstado
                };
            } else {
                request = {
                    cuentaId: cuentaId,
                    flotaCuentaId: flotaCuentaId,
                    flotaCuentaNombre: flotaCuentaNombre,
                    flotaCuentaEstado: flotaCuentaEstado
                };
            }
        }




        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeGrupoCrear(cuentaId: number, grupoCuentaNombre: string): Promise<ResponseTipo> {

        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_GRUPOCUENTA_CREAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_GRUPOCUENTA_CREAR_QA;
        }

        var request = {
            cuentaId: cuentaId,
            grupoCuentaNombre: grupoCuentaNombre
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeGrupoModificar(cuentaId: number, grupoCuentaId: number, grupoCuentaNombre: string, grupoCuentaEstado: number): Promise<ResponseTipo> {

        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_GRUPOCUENTA_MODIFICAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_GRUPOCUENTA_MODIFICAR_QA;
        }
        let request: any;
        if (grupoCuentaEstado === 0) {
            request = {
                cuentaId: cuentaId,
                grupoCuentaId: grupoCuentaId,
                grupoCuentaEstado: grupoCuentaEstado
            };
        } else {
            if (grupoCuentaNombre === "") {
                request = {
                    cuentaId: cuentaId,
                    grupoCuentaId: grupoCuentaId,
                    grupoCuentaEstado: grupoCuentaEstado
                };
            } else {
                request = {
                    cuentaId: cuentaId,
                    grupoCuentaId: grupoCuentaId,
                    grupoCuentaNombre: grupoCuentaNombre
                };
            }
        }




        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    private crearObjetoFactura(element: any) {
        let f = new Facturas();

        f.facturaFechaCase = element.facturaFechaCase;
        f.facturaEstadoId = element.facturaEstadoId;
        f.facturaPagada = this.obtenerEstadoPagoFactura(element.facturaEstadoId),
            f.facturaPagadaString = f.facturaPagada ? 'Pagada' : 'Sin pagar'
        f.facturaNroDocumento = element.facturaNumeroDocumento;
        f.facturaFechaEmision = this.convertDateToISOString(element.facturaFechaEmision);
        f.facturaFechaVencimiento = this.convertDateToISOString(element.facturaFechaVencimiento);
        f.facturaId = element.facturaId;
        f.facturaMontoTotal = element.facturaMontoTotal;
        f.facturaUrl = element.facturaUrl;
        f.tieneDocumento = element.facturaUrl !== "" && element.facturaUrl.indexOf(" ") < 0;
        f.tipoOperacionId = element.tipoOperacionId;


        if (element.facturaDatosFacturacion) {
            f.facturaDatosFacturacion.datosFacturacionActividadEconomica = element.facturaDatosFacturacion.dteActividadEconomica
            f.facturaDatosFacturacion.datosFacturacionCodigoPostal = element.facturaDatosFacturacion.datosFacturacionCodigoPostal
            f.facturaDatosFacturacion.datosFacturacionDestinatarioFacturaSap = element.facturaDatosFacturacion.cuentaDestinatarioFacturaSap
            f.facturaDatosFacturacion.datosFacturacionDeudorSap = element.facturaDatosFacturacion.cuentaDeudorSap
            f.facturaDatosFacturacion.datosFacturacionDiasCredito = element.facturaDatosFacturacion.datosFacturacionDiasCredito
            f.facturaDatosFacturacion.datosFacturacionDireccion = element.facturaDatosFacturacion.dteDireccionReceptor
            f.facturaDatosFacturacion.datosFacturacionDistrito = element.facturaDatosFacturacion.dteComunaReceptor
            f.facturaDatosFacturacion.datosFacturacionEmail = element.facturaDatosFacturacion.dteEmail
            f.facturaDatosFacturacion.datosFacturacionFechaUpdate = element.facturaDatosFacturacion.datosFacturacionFechaUpdate
            f.facturaDatosFacturacion.datosFacturacionGiro = element.facturaDatosFacturacion.dteGiroReceptor
            f.facturaDatosFacturacion.datosFacturacionNombreFantasia = element.facturaDatosFacturacion.dteNombreFantasia
            f.facturaDatosFacturacion.datosFacturacionPeriodoFacturacion = element.facturaDatosFacturacion.datosFacturacionPeriodoFacturacion
            f.facturaDatosFacturacion.datosFacturacionRazonSocial = element.facturaDatosFacturacion.dteRazonSocialReceptor
            f.facturaDatosFacturacion.datosFacturacionRut = element.facturaDatosFacturacion.dteRutReceptor
        }

        if (element.detalleFactura) {
            element.detalleFactura.forEach(d => {
                let _detalle = new DetalleFactura();
                _detalle.ventaId = d.ventaId;
                _detalle.documentoUrl = d.documentoUrl;
                _detalle.ventaPagoTotal = d.ventaPagoTotal;
                _detalle.vehiculoPatente = d.vehiculoPatente;
                _detalle.documentoEntrega = d.documentoEntrega;
                _detalle.ventaFechaCreacion = d.ventaFechaCreacion;
                _detalle.ventaDocumentoFolio = d.ventaDocumentoFolio;
                f.detalleFactura.push(_detalle);
            });
        }

        return f;
    }

    async homeUsuarioConsultarFacturas(cuentaId: number, fechaConsultaInicio: string, fechaConsulta: string, posicionFinal: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_FACTURAS_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_FACTURAS_QA;
        }

        var request = {
            cuentaId: cuentaId,
            fechaConsulta: fechaConsulta,
            posicionInicial: posicionFinal
            //clienteApp: "WEB"
        };

        if (fechaConsultaInicio != null)
            request['fechaConsultaInicio'] = fechaConsultaInicio;

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                //this.variablesApi.home.facturasPendientesPago = _data.data.cantidadFacturas;
                let facturas: Facturas[] = [];

                _data.data.transacciones.forEach(element => {
                    let f = this.crearObjetoFactura(element);
                    facturas.push(f);
                });

                let pagination = {
                    registrosPorPagina: _data.data.cantidadRegistrosPorConsulta,
                    totalRegistros: _data.data.cantidadTotalTrx,
                    posicionFinal: _data.data.posicionFinal,
                    registrosObtenidos: facturas.length
                }

                retorno.data = {
                    facturas: facturas,
                    pagination: pagination
                };

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioConsultarDetalleFacturas(cuentaId: number, facturaId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_DETALLE_FACTURAS_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_DETALLE_FACTURAS_QA;
        }

        var request = {
            cuentaId: cuentaId,
            facturaId: facturaId
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;

                let _factura = this.crearObjetoFactura(_data.data.factura)
                // let _datos = new DatosFacturacion();
                // let _detalles: DetalleFactura[] = [];
                // if (Object.entries(_data.data.factura).length !== 0) {
                //     _datos.datosFacturacionActividadEconomica = _data.data.factura.facturaDatosFacturacion.datosFacturacionActividadEconomica
                //     _datos.datosFacturacionCodigoPostal = _data.data.factura.facturaDatosFacturacion.datosFacturacionCodigoPostal
                //     _datos.datosFacturacionDestinatarioFacturaSap = _data.data.factura.facturaDatosFacturacion.datosFacturacionDestinatarioFacturaSap
                //     _datos.datosFacturacionDeudorSap = _data.data.factura.facturaDatosFacturacion.datosFacturacionDeudorSap
                //     _datos.datosFacturacionDiasCredito = _data.data.factura.facturaDatosFacturacion.datosFacturacionDiasCredito
                //     _datos.datosFacturacionDireccion = _data.data.factura.facturaDatosFacturacion.datosFacturacionDireccion
                //     _datos.datosFacturacionDistrito = _data.data.factura.facturaDatosFacturacion.datosFacturacionDistrito
                //     _datos.datosFacturacionEmail = _data.data.factura.facturaDatosFacturacion.datosFacturacionEmail
                //     _datos.datosFacturacionFechaUpdate = _data.data.factura.facturaDatosFacturacion.datosFacturacionFechaUpdate
                //     _datos.datosFacturacionGiro = _data.data.factura.facturaDatosFacturacion.datosFacturacionGiro
                //     _datos.datosFacturacionNombreFantasia = _data.data.factura.facturaDatosFacturacion.datosFacturacionNombreFantasia
                //     _datos.datosFacturacionPeriodoFacturacion = _data.data.factura.facturaDatosFacturacion.datosFacturacionPeriodoFacturacion
                //     _datos.datosFacturacionRazonSocial = _data.data.factura.facturaDatosFacturacion.datosFacturacionRazonSocial
                //     _datos.datosFacturacionRut = _data.data.factura.facturaDatosFacturacion.datosFacturacionRut
                //     _factura.facturaDatosFacturacion = _datos;
                //     _factura.facturaEstadoId = _data.data.factura.facturaEstadoId;;
                //     _factura.facturaFechaEmision = _data.data.factura.facturaFechaEmision;
                //     _factura.facturaFechaVencimiento = _data.data.factura.facturaFechaVencimiento;
                //     _factura.facturaId = _data.data.factura.facturaId;
                //     _factura.facturaMontoTotal = _data.data.factura.facturaMontoTotal;
                //     _factura.facturaUrl = _data.data.factura.facturaUrl;
                //     _factura.tipoOperacionId = _data.data.factura.tipoOperacionId;

                //     _data.data.factura.detalleFactura.forEach(element => {
                //         let _detalle = new DetalleFactura();
                //         _detalle.ventaId = element.ventaId;
                //         _detalle.documentoUrl = element.documentoUrl;
                //         _detalle.ventaPagoTotal = element.ventaPagoTotal;
                //         _detalle.vehiculoPatente = element.vehiculoPatente;
                //         _detalle.documentoEntrega = element.documentoEntrega;
                //         _detalle.ventaFechaCreacion = element.ventaFechaCreacion;
                //         _detalle.ventaDocumentoFolio = element.ventaDocumentoFolio;
                //         _detalles.push(_detalle);
                //     });
                // }

                //_factura.detalleFactura = _detalles;
                retorno.data = {
                    factura: _factura
                };


            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioConsultarTransacciones(cuentaId: number, fechaConsultaInicio: string, fechaConsulta: string, posicionFinal: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_TRANSACCIONES_PR : this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_TRANSACCIONES_QA;

        var request = {
            cuentaId: cuentaId,
            fechaConsulta: fechaConsulta,
            posicionInicial: posicionFinal
            //clienteApp: "WEB"
        };

        if (fechaConsultaInicio != null)
            request['fechaConsultaInicio'] = fechaConsultaInicio;

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;

                let transacciones: Transaccion[] = [];

                _data.data.transacciones.forEach(element => {
                    let trx = this.crearObjetoTransaccion(element);
                    transacciones.push(trx);
                });

                let pagination = {
                    registrosPorPagina: _data.data.cantidadRegistrosPorConsulta,
                    totalRegistros: _data.data.cantidadTotalTrx,
                    posicionFinal: _data.data.posicionFinal,
                    registrosObtenidos: transacciones.length
                }

                retorno.data = {
                    transacciones: transacciones,
                    pagination: pagination
                };
            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    private crearObjetoTransaccion(element: any) {
        let trx = new Transaccion();

        trx.cuentaId = element.cuentaId;
        trx.comercioId = element.comercioId;
        trx.cuentaNombre = element.cuentaNombre;
        trx.documentoUrl = element.documentoUrl;
        trx.medioDePago = element.medioDePago;
        trx.movimientoId = element.movimientoId;
        trx.tipoDocumentoId = parseInt(element.tipoDocumentoId);
        trx.tipoDocumentoNombre = this.getTipoDocumentoNombre(element.tipoDocumentoId);
        trx.tipoFormaPagoId = element.tipoFormaPagoId;
        trx.tipoFormaPagoNombre = this.getFormaPagoNombre(element.tipoFormaPagoId);
        trx.usuarioNombreApellido = element.usuarioNombreApellido;
        trx.usuarioRut = element.usuarioRut;
        trx.vehiculoPatente = element.vehiculoPatente;
        trx.ventaDocumentoFolio = element.ventaDocumentoFolio;
        trx.ventaFechaCreacion = this.convertDateToISOString(element.ventaFechaCreacion);
        trx.ventaHoraCreacion = moment(trx.ventaFechaCreacion).format('HH:mm');
        trx.ventaId = element.ventaId;
        trx.ventaPagoTotal = element.ventaPagoTotal;
        trx.ventaSitioDireccion = element.ventaSitioDireccion;
        trx.ventaSitioNombre = element.ventaSitioNombre;
        trx.tieneDocumento = element.documentoUrl !== "" && element.documentoUrl.indexOf(" ") < 0
        trx.ventaDocumentoId = element.ventaDocumentoId,
            trx.siiMedioPagoId = element.siiMedioPagoId;
        trx.ventaAtendedorNombre = element.ventaAtendedorNombre;
        trx.ventaCalificacionComentario = element.ventaCalificacionComentario;
        trx.ventaCalificacionNota = element.ventaCalificacionNota;
        trx.ventaDescuentoTotal = element.ventaDescuentoTotal;
        trx.ventaMontoTotal = element.ventaMontoTotal;
        trx.ventaNumeroTrxComercio = element.ventaNumeroTrxComercio;
        trx.vehiculoOdometro = element.vehiculoOdometro;
        trx.ventaEstacion = element.ventaEstacion;
        trx.ventaFecha = element.ventaFecha;
        trx.ventaHora = element.ventaHora;
        trx.ventaNumeroProceso = element.ventaNumeroProceso;
        trx.facturaNumeroDocumento = element.facturaNumeroDocumento;

        trx.detalle = [];

        if (element.detalleTransaccion !== undefined) {
            element.detalleTransaccion.forEach(d => {
                let detalle = new TransaccionDetalle();

                detalle.lineaCantidad = d.cantidad;
                detalle.lineaId = d.lineaId;
                detalle.lineaPrecioUnitario = d.precioUnitario;
                detalle.productoId = d.productoId;
                detalle.productoNombre = d.productoNombre;
                detalle.lineaUnidadMedida = d.unidadMedida;

                trx.detalle.push(detalle);
            })
        }

        return trx;
    }

    async homeUsuarioConsultarMovimientos(cuentaId: number, fechaConsulta: Date): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_MOVIMIENTOS_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_MOVIMIENTOS_QA;
        }

        var request = {
            cuentaId: cuentaId,
            fechaConsulta: fechaConsulta,
            clienteApp: "WEB"
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let movimientos: Movimiento[] = [];
                if (typeof _data.data.movimientos !== 'undefined') {
                    if (_data.data.movimientos.length > 0) {
                        _data.data.movimientos.forEach(_movimiento => {
                            let movimiento: Movimiento = new Movimiento();
                            movimiento.movimientoFechaCreacion = _movimiento.movimientoFechaCreacion;
                            movimiento.movimientoId = _movimiento.movimientoId;
                            movimiento.movimientoMonto = _movimiento.movimientoMonto;
                            movimiento.movimientoSaldoFinal = _movimiento.movimientoSaldoFinal;
                            movimiento.tipoMovimientoNombre = _movimiento.tipoMovimientoNombre;

                            movimientos.push(movimiento);
                        });
                    }

                }
                retorno.data = {
                    movimientos: movimientos
                };
            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioConsultarDetalleTransacciones(cuentaId: number, ventaId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_DETALLE_TRANSACCIONES_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_CONSULTAR_DETALLE_TRANSACCIONES_QA;
        }

        var request = {
            cuentaId: cuentaId,
            idTransaccionCliente: ventaId
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let trx = this.crearObjetoTransaccion(_data.data.transaccion);

                retorno.data = trx;
            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCuentaConsultarMedioPago(cuentaId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CONSULTAR_MEDIO_PAGO_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_CONSULTAR_MEDIO_PAGO_QA;
        }

        var request = {
            cuentaId: cuentaId
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let mediosPago: MedioPago[] = [];
                let cuentasCorrientes: CuentaCorriente[] = [];
                _data.data.formasPago.forEach(mp => {
                    let medioPago: MedioPago = new MedioPago();
                    medioPago.formaPagoTipo = (mp.tipoFormaPagoId === 1) ? "ESTACION" : "SALDO";
                    medioPago.formaPagoId = mp.tipoFormaPagoId;
                    medioPago.formaPagoActiva = true;
                    mediosPago.push(medioPago);
                });

                _data.data.cuentasCorriente.forEach(cc => {
                    let cuenta: CuentaCorriente = new CuentaCorriente();
                    cuenta.bancoId = cc.bancoId;
                    cuenta.bancoNombre = cc.bancoNombre;
                    cuenta.cuentaCorrienteRut = cc.cuentaCorrienteRut;
                    cuenta.tipoCuentaBancariaId = cc.tipoCuentaBancariaId;
                    cuenta.cuentaCorrienteNumero = cc.cuentaCorrienteNumero;
                    cuenta.tipoCuentaBancariaNombre = cc.tipoCuentaBancariaNombre;
                    cuenta.cuentaCorrienteNombreTitular = cc.cuentaCorrienteNombreTitular;
                    cuentasCorrientes.push(cuenta);
                });

                retorno.data = {
                    mediosPago: mediosPago,
                    cuentasCorrientes: cuentasCorrientes
                }

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCuentaNuevoConsultarMedioPago(cuentaId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_NUEVO_GET_MEDIO_PAGO_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_NUEVO_GET_MEDIO_PAGO_QA;
        }

        var request = {
            cuentaId: cuentaId
        };


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let mediosPago: MedioPago[] = [];
                let cuentasCorrientes: CuentaCorriente[] = [];
                let tarjetas: Tarjeta[] = [];
                let tarjetaCompartidaId: string = "";
                let saldo: Saldo = new Saldo();

                _data.data.formasPago.forEach(mp => {
                    let medioPago: MedioPago = new MedioPago();
                    medioPago.formaPagoTipo = (mp.tipoFormaPagoId === 1) ? "ESTACION" : "SALDO";
                    medioPago.formaPagoId = mp.tipoFormaPagoId;
                    medioPago.formaPagoActiva = true;
                    mediosPago.push(medioPago);
                });

                _data.data.cuentasCorriente.forEach(cc => {
                    let cuenta: CuentaCorriente = new CuentaCorriente();
                    cuenta.inscripcionId = cc.inscripcionId;
                    cuenta.bancoId = cc.inscripcionCodigoBanco;     //cc.bancoId;
                    cuenta.bancoNombre = this.variablesApi.home.maestros.bancos.filter(x => x.bancoId == cc.inscripcionCodigoBanco)[0].bancoNombre;
                    cuenta.cuentaCorrienteRut = cc.inscripcionRutCuenta;
                    cuenta.tipoCuentaBancariaId = cc.inscripcionCodigoTipoCuenta;
                    cuenta.cuentaCorrienteNumero = cc.inscripcionNumeroCuenta;
                    cuenta.tipoCuentaBancariaNombre = this.variablesApi.home.maestros.tipoCuentaBancaria.filter(x => x.tipoCuentaBancariaId == cc.inscripcionCodigoTipoCuenta)[0].tipoCuentaBancariaNombre;  //cc.tipoCuentaBancariaNombre;
                    //TODO: crojas - falta el nombre del titulas de la cta
                    cuenta.cuentaCorrienteNombreTitular = cc.cuentaCorrienteNombreTitular;
                    cuentasCorrientes.push(cuenta);
                });

                _data.data.datosTarjetas.formasDePago.forEach(ta => {
                    let tarjeta: Tarjeta = new Tarjeta();
                    tarjeta.idInscripcion = ta.idInscripcion;
                    tarjeta.tipoTarjetaCredito = ta.tipoTarjetaCredito;
                    tarjeta.ultimos4Digitos = ta.ultimos4Digitos;

                    tarjetas.push(tarjeta);
                });

                if (_data.data.datosSaldo) {
                    saldo.saldo = _data.data.datosSaldo.saldo;
                    saldo.saldoFinal = _data.data.datosSaldo.saldoFinal;
                    saldo.lineaCredito = _data.data.datosSaldo.lineaCredito;
                }

                tarjetaCompartidaId = _data.data.tarjetaCompartidaId == null ? "" : _data.data.tarjetaCompartidaId;

                retorno.data = {
                    mediosPago: mediosPago,
                    cuentasCorrientes: cuentasCorrientes,
                    tarjetas: tarjetas,
                    tarjetaCompartidaId: tarjetaCompartidaId,
                    saldo: saldo
                }

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCuentaMovimientosMedioPago(cuentaId: number, posicionInicial: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_NUEVO_GET_MEDIO_PAGO_MOVIMIENTOS_PR : this.constantes.COPEC_EMPRESAS_NUEVO_GET_MEDIO_PAGO_MOVIMIENTOS_QA;

        var request = {
            cuentaId: cuentaId,
            posicionInicial: posicionInicial
        };


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;

                let movimientos: MedioPagoMovimiento[] = [];
                _data.data.movimientos.forEach(x => {
                    let m: MedioPagoMovimiento = new MedioPagoMovimiento();
                    m.movimientoFechaCreacion = x.movimientoFechaCreacion;
                    m.movimientoId = x.movimientoId;
                    m.movimientoMonto = x.movimientoMonto;
                    m.movimientoSaldoFinal = x.movimientoSaldoFinal;
                    m.tipoMovimientoNombre = x.tipoMovimientoNombre;
                    movimientos.push(m);
                });

                let pagination = {
                    registrosPorPagina: _data.data.cantidadRegistrosPorConsulta,
                    totalRegistros: _data.data.cantidadTotalTrx,
                    posicionFinal: _data.data.posicionFinal,

                    registrosObtenidos: movimientos.length
                }

                retorno.data = {
                    movimientos: movimientos,
                    pagination: pagination
                }

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });

    }

    async homeCuentaNuevoPostMedioPago(cuentaId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_NUEVO_POST_MEDIO_PAGO_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_NUEVO_POST_MEDIO_PAGO_QA;
        }

        var request = {
            cuentaId: cuentaId
        };


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;


                retorno.data = {
                    url: _data.data.transbankUrl
                }

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCuentaModificarMedioPago(formaPagoSaldo: number, formaPagoEstacion: number, cuentaId: number, tipoFormaPagoOneclickEstado: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_MODIFICAR_MEDIO_PAGO_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_MODIFICAR_MEDIO_PAGO_QA;
        }

        var request = {
            tipoFormaPagoSaldoEstado: formaPagoSaldo,
            tipoFormaPagoEstacionEstado: formaPagoEstacion,
            tipoFormaPagoOneclickEstado: tipoFormaPagoOneclickEstado,
            cuentaId: cuentaId
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {}

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCuentaNuevoModificarMedioPago(formaPagoSaldo: number, formaPagoEstacion: number, cuentaId: number, tipoFormaPagoOneclickEstado: number, tarjetaCompartidaId: string, tarjetaCompartidaUltimos4Digitos: string, tarjetaCompartidaTipo: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_NUEVO_MODIFICAR_MEDIO_PAGO_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_NUEVO_MODIFICAR_MEDIO_PAGO_QA;
        }

        var request = {
            tipoFormaPagoSaldoEstado: formaPagoSaldo,
            tipoFormaPagoEstacionEstado: formaPagoEstacion,
            tipoFormaPagoOneclickEstado: tipoFormaPagoOneclickEstado,
            tarjetaCompartidaId: tarjetaCompartidaId,
            tarjetaCompartidaUltimos4Digitos: tarjetaCompartidaUltimos4Digitos,
            tarjetaCompartidaTipo: tarjetaCompartidaTipo,
            cuentaId: cuentaId
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {}

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCuentaNuevoEliminarMedioPago(cuentaId: number, idInscripcion: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_NUEVO_ELIMINAR_MEDIO_PAGO_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_NUEVO_ELIMINAR_MEDIO_PAGO_QA;
        }

        var request = {
            cuentaId: cuentaId,
            idInscripcion: idInscripcion
        };



        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            console.log(_data.data);
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {}

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeCuentaDesasociarCuentaCorriente(cuentaId: number, inscripcionId: number) {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_DESASOCIAR_CUENTA_CORRIENTE_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_DESASOCIAR_CUENTA_CORRIENTE_QA;
        }

        let request = {
            cuentaId: cuentaId,
            inscripcionId: inscripcionId
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {}

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });

    }

    async homeCuentaAsociarCuentaCorriente(cuentaId: number, cuentaCorrienteNumero: number, cuentaCorrienteRut: string, tipoCuentaBancariaId: number, bancoId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_ASOCIAR_CUENTA_CORRIENTE_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_ASOCIAR_CUENTA_CORRIENTE_QA;
        }

        var request = {
            inscripcionNumeroCuenta: cuentaCorrienteNumero,
            inscripcionCodigoBanco: bancoId,
            inscripcionCodigoTipoCuenta: tipoCuentaBancariaId,
            inscripcionRutCuenta: cuentaCorrienteRut,
            cuentaId: cuentaId,
            //cuentaCorrienteNombreTitular: cuentaCorrienteNombreTitular,
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {}

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioCuentaObtener(cuentaId: number, usuarioObtenerId: number): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIOCUENTA_OBTENER_PR
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIOCUENTA_OBTENER_QA;
        }

        var request = {
            cuentaId: cuentaId,
            usuarioObtenerId: usuarioObtenerId
        };


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let conductor: Conductor = new Conductor();

                _data.data.usuarioCuenta.cantidadVehiculosAsignados = _data.data.cantidadVehiculosAsignados;

                conductor = Conductor.crearObjetoConductor(_data.data.usuarioCuenta, _data.data.instruccionesDibujo);
                conductor = Conductor.cargarRestriccionesDiariasMensualesFromDetail(conductor, _data.data.usuarioCuenta.restriccionCombustible);


                retorno.data = {
                    conductor: conductor
                }

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeVehiculosCuentaObtener(cuentaId: number, posicionInicial: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_CUENTA_OBTENER_VEHICULOS_PR : this.constantes.COPEC_EMPRESAS_CUENTA_OBTENER_VEHICULOS_QA

        var request = {
            cuentaId: cuentaId,
            posicionInicial: posicionInicial
        }

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let vehiculos: Vehiculo[] = [];

                _data.data.vehiculos.forEach(v => {
                    let vehiculo: Vehiculo = this.crearObjetoVehiculo(v);

                    vehiculos.push(vehiculo);
                });

                let pagination = {
                    registrosPorPagina: _data.data.cantidadRegistrosPorConsulta,
                    totalRegistros: _data.data.cantidadTotalTrx,
                    posicionFinal: _data.data.posicionFinal,

                    registrosObtenidos: _data.data.vehiculos.length
                }

                retorno.data = {
                    vehiculos: vehiculos,
                    pagination: pagination
                }

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno;

        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeVehiculoCuentaObtener(cuentaId: number, vehiculoCuentaId: number): Promise<ResponseTipo> {
        let url = this.constantes.UrlCopec;
        url += environment.Ambiente === "PR" ? this.constantes.COPEC_EMPRESAS_VEHICULO_OBTENER_PR : this.constantes.COPEC_EMPRESAS_VEHICULO_OBTENER_QA;

        var request = {
            cuentaId: cuentaId,
            vehiculoCuentaId: vehiculoCuentaId
        };

        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;

                let vehiculo: Vehiculo = this.crearObjetoVehiculo(_data.data.vehiculos)

                retorno.data = {
                    vehiculo: vehiculo
                }

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }

    async homeUsuarioCuentaModificarRestricciones(cuentaId: number, usuarioId: number, diaNoHabilitadoEstado: number = 0, diaNoHabilitadoValor?: number[], horarioNoHabilitadoEstado: number = 0, horarioNoHabilitadoValor?: number[], estacionNoHabilitadaEstado: number = 0, estacionNoHabilitadaValor?: number[]): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_MODIFICAR_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_USUARIO_MODIFICAR_QA;
        }

        var request: any;
        if (diaNoHabilitadoEstado === 1 && horarioNoHabilitadoEstado === 1 && estacionNoHabilitadaEstado === 1) {
            request = {
                cuentaId: cuentaId,
                usuarioId: usuarioId,
                restriccionGeneral: {
                    diaHabilitadoEstado: diaNoHabilitadoEstado,
                    diaHabilitadoValor: diaNoHabilitadoValor,
                    horarioHabilitadoEstado: horarioNoHabilitadoEstado,
                    horarioHabilitadoValor: horarioNoHabilitadoValor,
                    estacionHabilitadaEstado: estacionNoHabilitadaEstado,
                    estacionHabilitadaValor: estacionNoHabilitadaValor,
                }
            };
        } else if (diaNoHabilitadoEstado === 1 && horarioNoHabilitadoEstado === 1 && estacionNoHabilitadaEstado === 0) {
            request = {
                cuentaId: cuentaId,
                usuarioId: usuarioId,
                restriccionGeneral: {
                    diaHabilitadoEstado: diaNoHabilitadoEstado,
                    diaHabilitadoValor: diaNoHabilitadoValor,
                    horarioHabilitadoEstado: horarioNoHabilitadoEstado,
                    horarioHabilitadoValor: horarioNoHabilitadoValor,
                }
            };
        } else if (diaNoHabilitadoEstado === 1 && horarioNoHabilitadoEstado === 0 && estacionNoHabilitadaEstado === 0) {
            request = {
                cuentaId: cuentaId,
                usuarioId: usuarioId,
                restriccionGeneral: {
                    diaHabilitadoEstado: diaNoHabilitadoEstado,
                    diaHabilitadoValor: diaNoHabilitadoValor,
                }
            };
        } else if (diaNoHabilitadoEstado === 0 && horarioNoHabilitadoEstado === 1 && estacionNoHabilitadaEstado === 0) {
            request = {
                cuentaId: cuentaId,
                usuarioId: usuarioId,
                restriccionGeneral: {
                    horarioHabilitadoEstado: horarioNoHabilitadoEstado,
                    horarioHabilitadoValor: horarioNoHabilitadoValor,
                }
            };
        } else if (diaNoHabilitadoEstado === 0 && horarioNoHabilitadoEstado === 0 && estacionNoHabilitadaEstado === 1) {
            request = {
                cuentaId: cuentaId,
                usuarioId: usuarioId,
                restriccionGeneral: {
                    estacionHabilitadaEstado: estacionNoHabilitadaEstado,
                    estacionHabilitadaValor: estacionNoHabilitadaValor,
                }
            };
        } else {
            request = {
                cuentaId: cuentaId,
                usuarioId: usuarioId,
                restriccionGeneral: {
                    diaHabilitadoEstado: 0,
                    diaHabilitadoValor: [0, 1, 2, 3, 4, 5, 6],
                    horarioHabilitadoEstado: 0,
                    horarioHabilitadoValor: [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
                }
            };
        }


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();


            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {};

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });

    }

    async homeUsuarioCuentaAbonoPost(cuentaId: number, idInscripcion: string, monto: number): Promise<ResponseTipo> {
        let url = "";
        let fechaMaximaEjecucion = new Date();
        this._maxDate.setTime((fechaMaximaEjecucion.getTime() + this.MAX_EXEC_TIME * 1000));
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_ABONO_POST_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_ABONO_POST_QA;
        }


        var request = {
            cuentaId: cuentaId,
            idInscripcion: idInscripcion,
            monto: monto
        };


        return await this.apiService.callApiHeaders(url, request).then(async _data => {
            let retorno = new ResponseTipo();

            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                let nroProceso = _data.data.numeroProceso;
                retorno = await this.homeUsuarioCuentaAbonoGet(cuentaId, nroProceso);

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });

    }

    async homeUsuarioCuentaAbonoGet(cuentaId: number, numeroProceso: string, retryError: number = 0): Promise<ResponseTipo> {
        let url = "";
        let dateNow = (new Date()).getTime();
        let date15 = (dateNow + 1500);
        let dateEnd = this._maxDate.getTime();

        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_ABONO_GET_PR;
        } else {
            url = this.constantes.UrlCopec + this.constantes.COPEC_EMPRESAS_ABONO_GET_QA;
        }

        var request = {
            cuentaId: cuentaId,
            numeroProceso: numeroProceso
        };

        if (dateNow < dateEnd) {

            let response = await this.apiService.callApiHeaders(url, request).then(_data => {
                let retorno = new ResponseTipo();

                if (_data.code == "200") {
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    retorno.data = {
                        respuesta: "200",
                        estadoCierre: _data.data.estadoCierre,
                        userMessage: _data.userMessage
                    };

                } else if (_data.code == "201") {
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    retorno.data = {
                        message: _data.userMessage
                    };
                } else if (_data.code == "206") {
                    retorno.estado = Respuestas.RESPUESTA_OK;
                    retorno.data = {
                        respuesta: "206"
                    };
                } else if (_data.code === 0) {
                    retorno.estado = Respuestas.RESPUESTA_SC;
                } else {
                    retorno.estado = Respuestas.RESPUESTA_SA;
                    retorno.data = { message: _data.userMessage };
                }
                return retorno
            }, error => {
                let retorno = this.manejaError(error);
                return retorno;
            });

            if (response.estado == Respuestas.RESPUESTA_OK) {
                if (response.data.respuesta === "206") {

                    return await this.homeUsuarioCuentaAbonoGet(cuentaId, numeroProceso);
                } else if (response.data.respuesta === "200") {
                    return response;
                }
            } else {
                if (retryError < this.MAX_ERRORES) {
                    retryError++;
                    this.homeUsuarioCuentaAbonoGet(cuentaId, numeroProceso, retryError);
                }
            }

        } else {
            let retorno = new ResponseTipo();
            retorno.estado = Respuestas.RESPUESTA_SC;
            return retorno;
        }

    }
    async homeCuentaUsuarioModificarDatos(nombre: string, apellido: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_EMPRESAS_USUARIO_MODIFICAR_DATOS_PR;
        } else {
            url = this.constantes.UrlCopecIngreso + this.constantes.COPEC_EMPRESAS_USUARIO_MODIFICAR_DATOS_QA;
        }

        var request = {
            nombre: nombre,
            apellido: apellido,
            enviarPush: 1,
            enviarRecibos: 1,
        };

        return await this.apiService.callApiHeadersPut(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = _data.userMessage

            } else if (_data.statusCode == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }
    //#endregion

    //#APIGENERICA
    /** NUEVA API GENERICA */
    async llamarPostIngreso(request: any, urlEntrada: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + "PR/" + urlEntrada;
        } else {
            url = this.constantes.UrlCopecIngreso + "QA/" + urlEntrada;
        }
        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = _data.userMessage

            } else if (_data.statusCode == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }
    async llamarPUTIngreso(request: string, urlEntrada: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopecIngreso + "PR/" + urlEntrada;
        } else {
            url = this.constantes.UrlCopecIngreso + "QA/" + urlEntrada;
        }


        return await this.apiService.callApiHeadersPut(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.statusCode == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = _data.userMessage

            } else if (_data.statusCode == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }
    async llamarPOSTHome(request: string, urlEntrada: string): Promise<ResponseTipo> {
        let url = "";
        if (environment.Ambiente === "PR") {
            url = this.constantes.UrlCopec + "EM1/PR/" + urlEntrada;
        } else {
            url = this.constantes.UrlCopec + "EM1/QA/" + urlEntrada;
        }


        return await this.apiService.callApiHeaders(url, request).then(_data => {
            let retorno = new ResponseTipo();
            if (_data.code == "200") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage,
                    data: _data.data
                };

            } else if (_data.code == "201") {
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.data = {
                    message: _data.userMessage,
                    data: _data.data
                };
            } else if (_data.code === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: _data.userMessage };
            }
            return retorno
        }, error => {
            let retorno = this.manejaError(error);
            return retorno;
        });
    }
    /** FIN NUEVA API GENERICA */

    //#endAPIGENERICA

    //#region Google

    async googleAutoComplete(input: string, lugar: { lat: string, long: string }) {
        let location = lugar.lat + " , " + lugar.long;
        let url = this.constantes.GOOGLE_AUTOCOMPLETE.replace("#input", input).replace("#location", location).replace("#key", this.constantes.GOOGLE_KEY);
        return await this.apiService.callApiGet(url).then(respuesta => {
            console.log(respuesta);
        }
            , error => {
                console.log(error);
            });
    }


    //#endregion

    //#region Funciones
    manejaError(error) {
        let retorno = new ResponseTipo();
        try {
            if (error.resultado != null) {
                let _data = JSON.parse(error.error);
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.data = { message: _data.userMessage };
            } else if (error.status === 401 || error.status === 403) {
                retorno.estado = Respuestas.RESPUESTA_SA;
                retorno.data = { message: "No Tiene Autorización para esta Accion" };
            } else if (error.status === 0) {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else if (error.name === "TimeoutError") {
                retorno.estado = Respuestas.RESPUESTA_SC;
            } else if (error.error.indexOf(" ") < 0) {
                let _data = JSON.parse(error.error);
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.data = { message: _data.userMessage };
            } else {
                let _data = JSON.parse(error.error);
                retorno.estado = Respuestas.RESPUESTA_NOK;
                if (_data.code == 500) {
                    retorno.data = { message: "Tenemos un problema en nuestros servicios." };
                } else if (typeof _data.userMessage !== 'undefined') {
                    retorno.data = { message: _data.userMessage };
                } else {
                    retorno.data = { message: "Error inesperado, compruebe su conexión a internet." };
                }
            }
        } catch (error) {
            retorno.estado = Respuestas.RESPUESTA_NOK;
            retorno.data = { message: "Error inesperado, compruebe su conexión a internet" };
        }
        return retorno;
    }

    crearObjetoVehiculo(_data) {
        let vehiculo: Vehiculo = new Vehiculo();

        vehiculo.vehiculoId = _data.vehiculoCuentaId;
        vehiculo.alias = _data.vehiculoCuentaAlias;
        vehiculo.anio = _data.vehiculoCuentaAno;
        vehiculo.tipo = _data.vehiculoCuentaTipo;
        vehiculo.estado = _data.vehiculoCuentaEstado == 1;
        vehiculo.marca = _data.vehiculoCuentaMarca;
        vehiculo.modelo = _data.vehiculoCuentaModelo;
        vehiculo.patente = _data.vehiculoCuentaPatente;

        vehiculo.fechaCreacion = _data.vehiculoCuentaFechaCreacion ? this.convertDateToISOString(_data.vehiculoCuentaFechaCreacion) : null;

        if (this.variablesApi.home.maestros.tipoVehiculo.length > 0) {
            let tipos = this.variablesApi.home.maestros.tipoVehiculo.filter(x => x.tipoVehiculoId == vehiculo.tipo);
            vehiculo.tipoString = tipos.length > 0 ? tipos[0].tipoVehiculoNombre : '';
        }

        //odometro
        vehiculo.requireOdometro = _data.restriccionOdometro != undefined && _data.restriccionOdometro.exigeOdometroEstado == 1;
        vehiculo.frecuenciaOdometro = _data.restriccionOdometro != undefined && _data.restriccionOdometro.exigeOdometroValor != undefined && Array.isArray(_data.restriccionOdometro.exigeOdometroValor) && _data.restriccionOdometro.exigeOdometroValor.length > 0 ? _data.restriccionOdometro.exigeOdometroValor[0] : 1

        return vehiculo;
    }

    crearObjetoUsuarioObtenerIngreso(_data: any) {
        let cuentas: Cuenta[] = [];
        let cuentaSeleccionada: CuentaSeleccionada = new CuentaSeleccionada();
        let menu: Menu = new Menu();
        let tiposFormaPago: TipoFormaPago[] = [];
        let saldo: Saldo = new Saldo();
        let usuarioLogin: UsuarioLogin = new UsuarioLogin();


        if (_data.cuentaSeleccionada) {
            cuentaSeleccionada.cuentaId = _data.cuentaSeleccionada.cuentaId;
            cuentaSeleccionada.cuentaNombre = _data.cuentaSeleccionada.cuentaNombre;
            cuentaSeleccionada.rolId = _data.cuentaSeleccionada.usuarioCuentaRolId;

            //cuentaSeleccionada.gastoTotalMes = _data.cuentaSeleccionada.gastoTotalMes;

            if (_data.cuentaSeleccionada.tiposFormaPago && _data.cuentaSeleccionada.tiposFormaPago.length > 0) {
                _data.cuentaSeleccionada.tiposFormaPago.forEach(item => {
                    let tfp: TipoFormaPago = new TipoFormaPago();
                    tfp.tipoFormaPagoId = item.tipoFormaPagoId;
                    tfp.tipoFormaPagoNombre = item.tipoFormaPagoNombre;
                    tfp.tipoFormaPagoDescripcion = item.tipoFormaPagoDescripcion;

                    tiposFormaPago.push(tfp);
                });
            }

            //SALDO
            if (_data.cuentaSeleccionada.saldo) {
                saldo.saldo = _data.cuentaSeleccionada.saldo.saldo;
                saldo.saldoFinal = _data.cuentaSeleccionada.saldo.saldoFinal;
                saldo.lineaCredito = _data.cuentaSeleccionada.saldo.lineaCredito
            }

            //INDICADORES
            if (_data.cuentaSeleccionada.indicadores != undefined) {
                cuentaSeleccionada.indicadores.gastoTotalMes = _data.cuentaSeleccionada.indicadores.gastoTotalMes;
                cuentaSeleccionada.indicadores.porcentajeAumentoGastoMes = _data.cuentaSeleccionada.indicadores.porcentajeAumentoGastoMes;
                cuentaSeleccionada.indicadores.gastoTotalMesAnterior = _data.cuentaSeleccionada.indicadores.gastoTotalMesAnterior;
            }

        }


        if (_data.usuarioLogin) {

            //CUENTAS
            if (_data.usuarioLogin.cuentas && _data.usuarioLogin.cuentas.length > 0) {
                _data.usuarioLogin.cuentas.forEach(item => {

                    let cuenta: Cuenta = new Cuenta();
                    cuenta.rolId = item.rolId;
                    //cuenta.rolNombre = this.variablesApi.home.maestros.roles.filter((x:Roles) => x.rolId == cuenta.rolId)[0].rolNombre;

                    cuenta.cuentaId = item.cuentaId;
                    cuenta.cuentaNombre = item.cuentaNombre;
                    cuenta.usuarioCuentaEstado = item.usuarioCuentaEstadoId == 1 ? true : false;
                    cuenta.usuarioCuentaFechaCreacion = item.usuarioCuentaFechaCreacion;

                    if (_data.cuentaSeleccionada && _data.cuentaSeleccionada.cuentaId == item.cuentaId)
                        // cuentaSeleccionada.cuentaId = cuenta.cuentaId;
                        // cuentaSeleccionada.cuentaNombre = cuenta.cuentaNombre;
                        cuentaSeleccionada.rolId = cuenta.rolId;
                    cuentaSeleccionada.usuarioCuentaEstado = cuenta.usuarioCuentaEstado;
                    cuentaSeleccionada.usuarioCuentaFechaCreacion = cuenta.usuarioCuentaFechaCreacion;

                    cuentas.push(cuenta);
                });
            }

            //USUARIO LOGIN
            usuarioLogin.usuarioId = _data.usuarioLogin.usuarioId;
            usuarioLogin.rolId = cuentaSeleccionada.rolId,
                usuarioLogin.rolNombre = _data.usuarioLogin.rolNombre;
            usuarioLogin.usuarioNombre = _data.usuarioLogin.usuarioNombre;
            usuarioLogin.usuarioApellido = _data.usuarioLogin.usuarioApellido;
            usuarioLogin.usuarioEmail = _data.usuarioLogin.usuarioEmail;
            usuarioLogin.usuarioTelefono = _data.usuarioLogin.usuarioTelefonoId;
            usuarioLogin.usuarioCuentaEstado = (_data.usuarioLogin.usuarioCuentaEstadoId == 1) ? TipoUsuarioCuentaEstado.ACTIVO : TipoUsuarioCuentaEstado.INACTIVO;
            usuarioLogin.usuarioRut = _data.usuarioLogin.usuarioRut;
            usuarioLogin.invitaciones = _data.usuarioLogin.invitaciones ? _data.usuarioLogin.invitaciones : [];
            usuarioLogin.solicitaTerminos = _data.usuarioLogin.solicitaTerminos;
            usuarioLogin.terminosMensaje = _data.usuarioLogin.solicitaTerminosMensaje;

        }

        //MENU
        if (_data.menu) {
            menu.abonarEstado = _data.menu.abonarEstado == 1;
            menu.conductoresEstado = _data.menu.conductoresEstado == 1;
            menu.documentosEstado = _data.menu.documentosEstado == 1;
            menu.facturacionEstado = _data.menu.facturacionEstado == 1;
            menu.formasDePagoEstado = _data.menu.formasDePagoEstado == 1;
            menu.historialEstado = _data.menu.historialEstado == 1;
            menu.notificacionesEstado = _data.menu.notificacionesEstado == 1;
            menu.pagarEstado = _data.menu.pagarEstado == 1;
            menu.vehiculosEstado = _data.menu.vehiculosEstado == 1;
            menu.tieneInvitaciones = _data.menu.tieneInvitaciones != 0;
            menu.tieneNotificiones = _data.menu.tieneNotificiones != 0;
        }



        let retorno = {
            usuarioLogin: usuarioLogin,
            cuentaSeleccionada: cuentaSeleccionada,
            cuentas: cuentas,
            menu: menu,
            tiposFormaPago: tiposFormaPago,
            saldo: saldo,
            rolPrivilegiosFront: _data.rolPrivilegiosFront

        }
        return retorno;
    }

    // crearObjetoUsuarioObtener(_data) {
    //     let cuentas: Cuentas[] = [];
    //     let invitaciones: Invitacion[] = [];
    //     let usuarioLogin: UsuarioLogin = new UsuarioLogin();
    //     let vehiculos: Vehiculos[] = [];
    //     let flotas: Flotas[] = [];
    //     let conductores: Conductor[] = [];
    //     let usuarios: Conductor[] = [];
    //     let grupos: Grupos[] = [];
    //     let mediosPago: MedioPago[] = [];
    //     let facturasPendientes: number = 0;
    //     let primerIngreso: boolean = false;
    //     let montoTotalMes: number = 0;
    //     let now = new Date();
    //     let menu: Menu = new Menu();
    //     let datoSaldo: TipoSaldo = new TipoSaldo();
    //     let tiposFormaPago: TipoFormaPago[] = [];
    //     let tipoDocumento: TipoDocumento[] = [];
    //     let cuentaDatosDte: CuentaDatosDte = new CuentaDatosDte();
    //     let notificaciones: Notificaciones[] = [];
    //     let fuerzaCambioCuenta: boolean = false;


    //     if (typeof _data.primerIngreso !== 'undefined') {
    //         primerIngreso = (_data.primerIngreso === 1) ? true : false;
    //     }

    //     if (typeof _data.montoTotalMes !== 'undefined') {
    //         montoTotalMes = _data.montoTotalMes == null ? 0 : _data.montoTotalMes;
    //     }

    //     if (typeof _data.tiposFormaPago !== 'undefined') {
    //         if (_data.tiposFormaPago.length > 0) {
    //             _data.tiposFormaPago.forEach(_tipo => {
    //                 let tipo: TipoFormaPago = new TipoFormaPago();
    //                 tipo.tipoFormaPagoId = _tipo.tipoFormaPagoId;
    //                 tipo.tipoFormaPagoNombre = _tipo.tipoFormaPagoNombre;
    //                 tipo.tipoFormaPagoDescripcion = _tipo.tipoFormaPagoDescripcion;

    //                 tiposFormaPago.push(tipo);
    //             });
    //         }
    //     }

    //     if (typeof _data.cuentas !== 'undefined') {
    //         if (_data.cuentas.length > 0) {
    //             _data.cuentas.forEach(_cuentas => {
    //                 let cuenta: Cuentas = new Cuentas();
    //                 cuenta.cuentaId = _cuentas.cuentaId;
    //                 cuenta.cuentaEstado = _cuentas.cuentaEstado == 1 ? true : false;
    //                 cuenta.cuentaNombre = _cuentas.cuentaNombre;
    //                 cuenta.cuentaDefault = _cuentas.usuarioCuentaSeleccionado === 1 ? true : false;
    //                 cuenta.tipoDocumento = _cuentas.tipoDocumentoId;
    //                 cuenta.cuentaFechaCreacion = _cuentas.usuarioCuentaFechaCreacion;
    //                 cuenta.rolId = _cuentas.rolId;
    //                 cuentas.push(cuenta);
    //             });
    //         }
    //     }

    //     if (typeof _data.invitaciones !== 'undefined') {
    //         if (_data.invitaciones.length > 0) {
    //             _data.invitaciones.forEach(_invitaciones => {
    //                 let invitacion: Invitacion = new Invitacion();
    //                 invitacion.invitacionId = _invitaciones.invitacionId;
    //                 invitacion.invitacionNombreCuenta = _invitaciones.invitacionCuentaNombre;
    //                 invitacion.invitacionCuentaId = _invitaciones.cuentaId;
    //                 invitacion.invitacionFechaCaducidad = _invitaciones.invitacionFechaCaducidad;
    //                 invitacion.invitacionTelefonoInvitadoId = _invitaciones.invitacionTelefonoInvitadoId;
    //                 invitacion.invitacionUsuarioCreadorNombre = _invitaciones.invitacionUsuarioCreadorNombre;
    //                 // if (invitacion.invitacionFechaCaducidad.getTime() > now.getTime()) {
    //                 //   invitacion.invitacionEstado = "VIGENTE";
    //                 // } else {
    //                 //   invitacion.invitacionEstado = "CADUCADA";
    //                 // }

    //                 invitaciones.push(invitacion);
    //             });
    //         }
    //     }

    //     if (typeof _data.usuarioLogin !== 'undefined') {
    //         usuarioLogin.usuarioId = _data.usuarioLogin.usuarioId;
    //         usuarioLogin.rolId = _data.usuarioLogin.rolId;
    //         usuarioLogin.rolNombre = _data.usuarioLogin.rolNombre;
    //         usuarioLogin.usuarioNombre = _data.usuarioLogin.usuarioNombre;
    //         usuarioLogin.usuarioApellido = _data.usuarioLogin.usuarioApellido;
    //         usuarioLogin.usuarioEmail = _data.usuarioLogin.usuarioEmail;
    //         usuarioLogin.usuarioTelefono = _data.usuarioLogin.usuarioTelefonoId;
    //         usuarioLogin.usuarioCuentaEstado = (_data.usuarioLogin.usuarioCuentaEstadoId == 1) ? TipoUsuarioCuentaEstado.ACTIVO : TipoUsuarioCuentaEstado.INACTIVO;
    //     }

    //     if (typeof _data.vehiculos !== 'undefined') {
    //         if (_data.vehiculos.length > 0) {
    //             _data.vehiculos.forEach(_vehiculos => {
    //                 let vehiculo: Vehiculos = new Vehiculos();
    //                 vehiculo.vehiculoId = _vehiculos.vehiculoCuentaId;
    //                 vehiculo.flotaId = _vehiculos.flotaCuentaId;
    //                 vehiculo.estado = _vehiculos.vehiculoCuentaEstado;
    //                 vehiculo.patente = _vehiculos.vehiculoCuentaPatente;
    //                 vehiculo.alias = _vehiculos.vehiculoCuentaAlias;
    //                 vehiculo.marca = _vehiculos.vehiculoCuentaMarca;
    //                 vehiculo.modelo = _vehiculos.vehiculoCuentaModelo;
    //                 vehiculo.ano = _vehiculos.vehiculoCuentaAno;
    //                 vehiculo.tipo = _vehiculos.vehiculoCuentaTipo;
    //                 vehiculo.validado = _vehiculos.vehiculoCuentaValidado === 1 ? true : false;

    //                 vehiculos.push(vehiculo);
    //             });
    //             vehiculos.sort((a, b) => a.patente.localeCompare(b.patente));
    //         }
    //     }

    //     if (typeof _data.flotas !== 'undefined') {
    //         if (_data.flotas.length > 0) {
    //             _data.flotas.forEach(_flotas => {
    //                 let flota: Flotas = new Flotas();
    //                 flota.flotaId = _flotas.flotaCuentaId;
    //                 flota.flotaCuentaId = _flotas.flotaCuentaId;
    //                 flota.flotaCuentaNombre = _flotas.flotaCuentaNombre;
    //                 flota.estado = _flotas.flotaCuentaEstado === 1 ? true : false;

    //                 flotas.push(flota);
    //             });
    //         }
    //     }

    //     if (typeof _data.conductores !== 'undefined') {
    //         if (_data.conductores.length > 0) {
    //             let filterConductores = _data.conductores.filter(r => r.usuarioCuentaConduce == 1);
    //             let filterUsuarios = _data.conductores.filter(r => r.rolId != 4);
    //             filterConductores.forEach(_conductores => {
    //                 let conductor = new Conductores();
    //                 conductor.idConductor = _conductores.usuarioId;
    //                 conductor.rolId = _conductores.rolId;
    //                 if (this.variablesApi.home.maestros.roles.filter(r => r.rolId == conductor.rolId).length > 0) {
    //                     conductor.rolNombre = this.variablesApi.home.maestros.roles.filter(r => r.rolId == conductor.rolId)[0].rolNombre;
    //                 }
    //                 conductor.grupoId = _conductores.grupoCuentaId;
    //                 conductor.nombreConductor = _conductores.usuarioNombre;
    //                 conductor.rutConductor = _conductores.usuarioRut;
    //                 conductor.apellidosConductor = _conductores.usuarioApellido;
    //                 conductor.nombreCompleto = _conductores.usuarioNombre + " " + _conductores.usuarioApellido;
    //                 conductor.telefonoConductor = _conductores.usuarioTelefonoId;
    //                 conductor.estadoConductor = (_conductores.usuarioCuentaEstadoId == 1) ? true : false;
    //                 conductor.usuarioCuentaConduce = _conductores.usuarioCuentaConduce;
    //                 conductor.maximoVentaMes = _conductores.maximoVentaMes;
    //                 conductor.limiteMensualCombustible = _conductores.maximoVentaMes;
    //                 conductor.porcentajeVentaMes = Math.round(_conductores.porcentajeVentaMes * 100);
    //                 conductor.sumaVentaMes = parseInt(_conductores.sumaVentaMes);
    //                 let paso = _data.conductoresSaldosDiarios.filter(r => r.usuarioId == conductor.idConductor)[0];
    //                 conductor.maximoVentaDia = parseInt(paso.maximoVentaDia);
    //                 conductor.limiteDiarioCombustible = parseInt(paso.maximoVentaDia);
    //                 conductor.porcentajeVentaDia = Math.round(paso.porcentajeVentaDia * 100);
    //                 conductor.sumaVentaDia = parseInt(paso.sumaVentaDia);
    //                 let restriccion = new RestriccionConsumoCombustible();
    //                 conductor.restriccionConductor = restriccion;
    //                 conductores.push(conductor);
    //             });

    //             conductores.sort((a, b) => a.apellidosConductor.localeCompare(b.apellidosConductor));

    //             filterUsuarios.forEach(_conductores => {
    //                 let conductor = new Conductores();
    //                 conductor.idConductor = _conductores.usuarioId;
    //                 conductor.rolId = _conductores.rolId;
    //                 if (this.variablesApi.home.maestros.roles.filter(r => r.rolId == conductor.rolId).length > 0) {
    //                     conductor.rolNombre = this.variablesApi.home.maestros.roles.filter(r => r.rolId == conductor.rolId)[0].rolNombre;
    //                 }
    //                 conductor.grupoId = _conductores.grupoCuentaId;
    //                 conductor.nombreConductor = _conductores.usuarioNombre;
    //                 conductor.rutConductor = _conductores.usuarioRut;
    //                 conductor.apellidosConductor = _conductores.usuarioApellido;
    //                 conductor.nombreCompleto = _conductores.usuarioNombre + " " + _conductores.usuarioApellido;
    //                 conductor.telefonoConductor = _conductores.usuarioTelefonoId;
    //                 conductor.estadoConductor = (_conductores.usuarioCuentaEstadoId == 1) ? true : false;
    //                 conductor.usuarioCuentaConduce = _conductores.usuarioCuentaConduce;
    //                 conductor.maximoVentaMes = _conductores.maximoVentaMes;
    //                 conductor.limiteMensualCombustible = parseInt(_conductores.maximoVentaMes);
    //                 conductor.porcentajeVentaMes = Math.round(_conductores.porcentajeVentaMes * 100);
    //                 conductor.sumaVentaMes = parseInt(_conductores.sumaVentaMes);
    //                 let paso = _data.conductoresSaldosDiarios.filter(r => r.usuarioId == conductor.idConductor)[0];
    //                 conductor.maximoVentaDia = parseInt(paso.maximoVentaDia);
    //                 conductor.limiteDiarioCombustible = parseInt(paso.maximoVentaDia);
    //                 conductor.porcentajeVentaDia = Math.round(paso.porcentajeVentaDia * 100);
    //                 conductor.sumaVentaDia = parseInt(paso.sumaVentaDia);
    //                 let restriccion = new RestriccionConsumoCombustible();
    //                 conductor.restriccionConductor = restriccion;
    //                 usuarios.push(conductor);
    //             });
    //             usuarios.sort((a, b) => a.apellidosConductor.localeCompare(b.apellidosConductor));
    //         }
    //     }

    //     if (typeof _data.grupos !== 'undefined') {
    //         if (_data.grupos.length > 0) {
    //             _data.grupos.forEach(_grupos => {
    //                 let grupo: Grupos = new Grupos();
    //                 grupo.grupoId = _grupos.grupoCuentaId;
    //                 grupo.grupoCuentaId = _grupos.grupoCuentaId;
    //                 grupo.grupoCuentaNombre = _grupos.grupoCuentaNombre;
    //                 grupo.cantidadConductores = _grupos.grupoCuentaCantidadConductores;
    //                 grupo.estado = _grupos.grupoCuentaEstado === 1 ? true : false;
    //                 grupos.push(grupo);
    //             });
    //         }
    //     }

    //     if (typeof _data.facturasPendientesDePago !== 'undefined') {
    //         facturasPendientes = _data.facturasPendientesDePago;
    //     }

    //     if (typeof _data.datosSaldo !== 'undefined') {
    //         if (Object.keys(_data.datosSaldo).length > 0) {
    //             datoSaldo.lineaCredito = _data.datosSaldo.lineaCredito;
    //             datoSaldo.saldo = _data.datosSaldo.saldo;
    //             datoSaldo.saldoFinal = _data.datosSaldo.saldoFinal;
    //             // datoSaldo.tipoSaldoId = _data.datoSaldo.tipoSaldoId;
    //             // datoSaldo.tipoSaldoNombre = _data.datoSaldo.tipoSaldoNombre;
    //         }
    //     }

    //     if (typeof _data.tiposDocumento !== 'undefined') {
    //         _data.tiposDocumento.forEach(element => {
    //             let tipo: TipoDocumento = new TipoDocumento();
    //             tipo.tipoDocumentoId = element.tipoDocumentoId;
    //             tipo.tipoDocumentoNombre = element.tipoDocumentoNombre;
    //             tipoDocumento.push(tipo);
    //         });
    //     }

    //     if (typeof _data.tiposFormaPago !== 'undefined') {
    //         _data.tiposFormaPago.forEach(element => {
    //             let tipo: MedioPago = new MedioPago();
    //             tipo.formaPagoId = element.tipoFormaPagoId;
    //             mediosPago.push(tipo);
    //         });

    //     }

    //     // if (typeof _data.menu !== 'undefined') {
    //     //     menu.abonarEstado = _data.menu.abonarEstado;
    //     //     menu.datosDeFacturacionEstado = _data.menu.datosDeFacturacionEstado;
    //     //     menu.formasDePagoEstado = _data.menu.formasDePagoEstado;
    //     //     menu.invitarConductoresEstado = _data.menu.invitarConductoresEstado;
    //     //     menu.misCargasEstado = _data.menu.misCargasEstado;
    //     //     menu.misConductoresEstado = _data.menu.misConductoresEstado;
    //     //     menu.misDocumentosEstado = _data.menu.misDocumentosEstado;
    //     //     menu.misVehiculosEstado = _data.menu.misVehiculosEstado;
    //     //     menu.pagarEstado = _data.menu.pagarEstado;
    //     //     menu.subTitulo = _data.menu.subTitulo;
    //     //     menu.titulo = _data.menu.titulo;
    //     // }

    //     if (typeof _data.cuentaDatosDte !== 'undefined') {
    //         cuentaDatosDte.dteActividadEconomica = _data.cuentaDatosDte.dteActividadEconomica;
    //         cuentaDatosDte.dteDireccion = _data.cuentaDatosDte.dteDireccion;
    //         cuentaDatosDte.dteDistrito = _data.cuentaDatosDte.dteDistrito;
    //         cuentaDatosDte.dteEmail = _data.cuentaDatosDte.dteEmail;
    //         cuentaDatosDte.dteFechaUpdate = _data.cuentaDatosDte.dteFechaUpdate;
    //         cuentaDatosDte.dteGiro = _data.cuentaDatosDte.dteGiro;
    //         cuentaDatosDte.dteNombreFantasia = _data.cuentaDatosDte.dteNombreFantasia;
    //         cuentaDatosDte.dteRazonSocial = _data.cuentaDatosDte.dteRazonSocial;
    //         cuentaDatosDte.dteRut = _data.cuentaDatosDte.dteRut;
    //         cuentaDatosDte.dteRestringeEleccion = _data.cuentaDatosDte.dteRestringeEleccion;
    //     }

    //     if (typeof _data.notificaciones !== 'undefined') {
    //         _data.notificaciones.forEach(noti => {
    //             let notifica: Notificaciones = new Notificaciones();
    //             notifica.tipoNotificacionId = noti.tipoNotificacionId;
    //             notifica.tipoNotificacionEstado = (noti.tipoNotificacionEstado == 1) ? true : false;
    //             notifica.tipoNotificacionNombre = noti.tipoNotificacionNombre;
    //             notifica.tipoNotificacionDetalle = noti.tipoNotificacionDetalle;
    //             notificaciones.push(notifica);
    //         });
    //     }

    //     if (typeof _data.fuerzaCambioCuenta !== 'undefined') {
    //         if (_data.fuerzaCambioCuenta == 1) {
    //             fuerzaCambioCuenta = true;
    //         } else {
    //             fuerzaCambioCuenta = false;
    //         }
    //     }

    //     let retorno = {
    //         cuentas: cuentas,
    //         invitaciones: invitaciones,
    //         usuarioLogin: usuarioLogin,
    //         vehiculos: vehiculos,
    //         flotas: flotas,
    //         conductores: conductores,
    //         usuarios: usuarios,
    //         grupos: grupos,
    //         mediosPago: mediosPago,
    //         facturasPendientes: facturasPendientes,
    //         primerIngreso: primerIngreso,
    //         montoTotalMes: montoTotalMes,
    //         tiposFormaPago: tiposFormaPago,
    //         menu: menu,
    //         datosSaldo: datoSaldo,
    //         tipoDocumento: tipoDocumento,
    //         cuentaDatosDte: cuentaDatosDte,
    //         notificaciones: notificaciones,
    //         fuerzaCambioCuenta: fuerzaCambioCuenta
    //     };
    //     return retorno;
    // }

    crearMaestros(_data) {
        let maestros: Maestros = new Maestros();
        let bancos: Bancos[] = [];
        let periodosFacturacion: PeriodosFacturacion[] = [];
        let tipoCuentaBancaria: TipoCuentaBancaria[] = [];
        let tipoDocumento: TipoDocumento[] = [];
        let tipoRestriccion: TipoRestriccion[] = [];
        let tipoSaldo: TipoSaldo[] = [];
        let tipoVehiculo: TipoVehiculo[] = [];
        let tipoFormaPago: MedioPago[] = [];
        let roles: Roles[] = [];



        _data.bancos.forEach(element => {
            let banco: Bancos = new Bancos();
            banco.bancoId = element.codigoBanco;
            banco.bancoNombre = element.bancoNombre;
            bancos.push(banco);
        });

        _data.periodosFacturacion.forEach(element => {
            let periodo: PeriodosFacturacion = new PeriodosFacturacion();
            periodo.periodoFacturacionId = element.periodoFacturacionId;
            periodo.periodoFacturacionNombre = element.periodoFacturacionNombre;
            periodosFacturacion.push(periodo);
        });

        _data.tipoCuentaBancaria.forEach(element => {
            let tipoCuenta: TipoCuentaBancaria = new TipoCuentaBancaria();
            tipoCuenta.tipoCuentaBancariaId = element.tipoCuentaBancariaId;
            tipoCuenta.tipoCuentaBancariaNombre = element.tipoCuentaBancariaNombre;
            tipoCuentaBancaria.push(tipoCuenta);
        });

        _data.tipoDocumento.forEach(element => {
            let tipo: TipoDocumento = new TipoDocumento();
            tipo.tipoDocumentoId = element.tipoDocumentoId;
            tipo.tipoDocumentoNombre = element.tipoDocumentoNombre;
            tipoDocumento.push(tipo);
        });
        _data.tipoRestriccion.forEach(element => {
            let tipo: TipoRestriccion = new TipoRestriccion();
            tipo.tipoRestriccionId = element.tipoRestriccionId;
            tipo.tipoDocumentoNombre = element.tipoDocumentoNombre;
            tipoRestriccion.push(tipo);
        });
        _data.tipoSaldo.forEach(element => {
            let tipo: TipoSaldo = new TipoSaldo();
            tipo.tipoSaldoId = element.tipoSaldoId;
            tipo.tipoSaldoNombre = element.tipoSaldoNombre;
            tipoSaldo.push(tipo);

        });
        _data.tipoVehiculo.forEach(element => {
            let tipo: TipoVehiculo = new TipoVehiculo();
            tipo.tipoVehiculoId = element.tipoVehiculoId;
            tipo.tipoVehiculoNombre = element.tipoVehiculoNombre;
            tipoVehiculo.push(tipo);
        });

        roles = _data.roles;

        _data.tipoFormaPago.forEach(element => {
            let formaPago: MedioPago = new MedioPago();
            formaPago.formaPagoId = element.tipoFormaPagoId;
            formaPago.formaPagoTipo = element.tipoFormaPagoNombre;
            formaPago.formaPagoDescripcion = element.tipoFormaPagoDescripcion
            tipoFormaPago.push(formaPago);
        });

        let formaPago: MedioPago = new MedioPago();
        formaPago.formaPagoId = 0;
        formaPago.formaPagoTipo = "ANTIGUO";
        formaPago.formaPagoDescripcion = "FORMA ANTIGUA DE PAGO"
        tipoFormaPago.push(formaPago);


        maestros.bancos = bancos;
        maestros.periodosFacturacion = periodosFacturacion;
        maestros.tipoCuentaBancaria = tipoCuentaBancaria;
        maestros.tipoDocumento = tipoDocumento;
        maestros.tipoRestriccion = tipoRestriccion;
        maestros.tipoSaldo = tipoSaldo;
        maestros.tipoVehiculo = tipoVehiculo;
        maestros.roles = roles;
        maestros.tipoFormaPago = tipoFormaPago;

        maestros.tiposNotificaciones = _data.tiposNotificacion
        let retorno = {
            maestros: maestros
        }
        return retorno;

    }

    private obtenerEstadoPagoFactura(estado: string): boolean {
        switch (estado) {
            case 'CASADA':
                return true;
            case 'FACTURADA':
            case 'DOCUMENTADA':
                return false;
            default:
                console.log('Estado factura: ', estado);
                alert('estado de pago no implementado');
                return false;
        }
    }

    /** input: "2020-04-09 22:03:16.000000" */
    private convertDateToISOString(date: string): string {
        let d = moment(date, "YYYY-MM-DD HH:mm:ss")
        //console.log(date, '->', d.toISOString());
        return d.toISOString();
    }

    private getTipoDocumentoNombre = (id: number): string => {
        let res = this.variablesApi.home.maestros.tipoDocumento.filter(x => x.tipoDocumentoId == id);
        return res && res.length > 0 ? res[0].tipoDocumentoNombre : "";
    }

    private getFormaPagoNombre = (id: number): string => {
        let res = this.variablesApi.home.maestros.tipoFormaPago.filter(x => x.formaPagoId == id);
        return res && res.length > 0 ? res[0].formaPagoDescripcion : "";

    }

    //#endregion
}
