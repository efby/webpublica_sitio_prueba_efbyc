import { Injectable } from '@angular/core';
import { ApiGatewayService } from 'src/app/providers/api-gateway/api-gateway.service';
import { ResponseTipo } from 'src/app/models/respuestas-tipo';
import { IModificarConductor } from 'src/app/models/conductores';
import { IInvitacionConductor, Vehiculo, Cuenta } from 'src/app/globales/business-objects';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private apiGateway: ApiGatewayService) { }

  async homeUsuarioObtener(): Promise<ResponseTipo> {
    return await this.apiGateway.obtenerWeb();
  }
  public async homeObtenerMaestros(cuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoObtenerMaestros(cuentaId);
  }

  async homeCambiarCuentaSeleccionada(cuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCambiarCuentaSeleccionada(cuentaId);
  }

  async homeCrearCuentaCorriente(cuentaId: number, cuentaCorrienteNumero: number, cuentaCorrienteRut: string, cuentaCorrienteNombreTitular: string, cuentaCorrienteEmailTitular: string, cuentaCorrienteNombre: string, cuentaCorrienteTipo: number, bancoId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCrearCuentaCorriente(cuentaId, cuentaCorrienteNumero, cuentaCorrienteRut, cuentaCorrienteNombreTitular, cuentaCorrienteEmailTitular, cuentaCorrienteNombre, cuentaCorrienteTipo, bancoId);
  }

  async homeCrearCuentaEmpresa(cuentaNombre: string): Promise<ResponseTipo> {
    return await this.apiGateway.homeCrearCuentaEmpresa(cuentaNombre);
  }

  async homeModificarCuentaEmpresa(cuenta:Cuenta, estado: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeModificarCuentaEmpresa(cuenta, estado);
  }

  async homeConsultarDatosFacturacion(cuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeConsultarDatosFacturacion(cuentaId);
  }


  async homeCrearDatosFacturacion(cuentaId: number, datosFacturacionRut: string, datosFacturacionEmail: string, datosFacturacionGiro: string, datosFacturacionDistrito: string, datosFacturacionDireccion: string, datosFacturacionRazonSocial: string, datosFacturacionCodigoPostal: string, datosFacturacionNombreFantasia: string, datosFacturacionActividadEconomica: string, datosFacturacionPeriodoFacturacion: string, restringeEleccionDte: string, dtePuedeFacturar: boolean, dteNserie:string): Promise<ResponseTipo> {
    return await this.apiGateway.homeCrearDatosFacturacion(cuentaId, datosFacturacionRut, datosFacturacionEmail, datosFacturacionGiro, datosFacturacionDistrito, datosFacturacionDireccion, datosFacturacionRazonSocial, datosFacturacionCodigoPostal, datosFacturacionNombreFantasia, datosFacturacionActividadEconomica, datosFacturacionPeriodoFacturacion, restringeEleccionDte, dtePuedeFacturar ,dteNserie);
  }

  //async homeUsuarioInvitar(cuentaId: number, rolId: number, invitacionTelefonoId: string, invitacionNombre: string, invitacionApellido: string, estadoCombustible: number, montoMaximoDiaValor: number, montoMaximoMesValor: number, usuarioCuentaConduce: boolean): Promise<ResponseTipo> {
  //   return await this.apiGateway.homeUsuarioInvitar(cuentaId, rolId, invitacionTelefonoId, invitacionNombre, invitacionApellido, estadoCombustible, montoMaximoDiaValor, montoMaximoMesValor, usuarioCuentaConduce);
  // }

  async homeUsuarioInvitarConductor(cuentaId: number, rolId: number, invitacionTelefonoId: string, invitacionNombre: string, invitacionApellido: string, estadoCombustible: number, montoMaximoDiaValor: number, montoMaximoMesValor: number, usuarioCuentaConduce: boolean): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioInvitarConductor(cuentaId, rolId, invitacionTelefonoId, invitacionNombre, invitacionApellido, estadoCombustible, montoMaximoDiaValor, montoMaximoMesValor, usuarioCuentaConduce);
  }

  async homeUsuarioInvitarConductores(cuentaId:number, invitaciones:IInvitacionConductor[]): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioInvitarConductores(cuentaId, invitaciones);
  }

  async homeUsuarioAceptaInvitacion(invitacionId: number, invitacionAceptada: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioAceptaInvitacion(invitacionId, invitacionAceptada);
  }

  async homeUsuarioCancelaInvitacion(cuentaId: number, invitacionId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioCancelaInvitacion(cuentaId, invitacionId);
  }

  async homeUsuarioAutorizaInvitacion(cuentaId: number, invitacionId: number, invitacionAutorizada: number, combustibleEstado: number, combustibleMontoMaximoDiaValor: number, combustibleMontoMaximoMesValor: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioAutorizaInvitacion(cuentaId, invitacionId, invitacionAutorizada, combustibleEstado, combustibleMontoMaximoDiaValor, combustibleMontoMaximoMesValor);
  }

  async homeUsuarioConsultarInvitacion(cuentaId: number, posicionInicial:number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarInvitacion(cuentaId, posicionInicial);
  }

  async homeUsuarioConsultarRestricciones(cuentaId:number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarRestricciones(cuentaId);
  }

  async homeUsuarioConsultarUsuarios(cuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarUsuarios(cuentaId);
  }

  async homeUsuarioConsultarUsuarios2(cuentaId: number, posicionInicial:number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarUsuarios2(cuentaId, posicionInicial);
  }

  async homeUsuarioModificar(cuentaId: number, usuarioId: string, grupoCuentaId: number, usuarioCuentaEstadoId: number, rolId: number, usuarioCuentaConduce: number, usuarioCuentaRindeGasto: number, usuarioCuentaCreaVehiculo: number, usuarioCuentaEligeDTE: number, usuarioCuentaSeleccionado: number, vehiculosAsignados: any, estadoCombustible: boolean, montoMaximoDiaValor: number, montoMaximoMesValor: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioModificar(cuentaId, usuarioId, grupoCuentaId, usuarioCuentaEstadoId, rolId, usuarioCuentaConduce, usuarioCuentaRindeGasto, usuarioCuentaCreaVehiculo, usuarioCuentaEligeDTE, usuarioCuentaSeleccionado, vehiculosAsignados, estadoCombustible, montoMaximoDiaValor, montoMaximoMesValor);
  }

  // async homeVehiculoCrear(cuentaId: number, flotaCuentaId: number, vehiculoCuentaPatente: string, vehiculoCuentaModelo: string, vehiculoCuentaMarca: string, vehiculoCuentaAno: number, vehiculoCuentaTanque: number, vehiculoCuentaAlias: string, vehiculoCuentaTipo: number): Promise<ResponseTipo> {
  //   return await this.apiGateway.homeVehiculoCrear(cuentaId, flotaCuentaId, vehiculoCuentaPatente, vehiculoCuentaModelo, vehiculoCuentaMarca, vehiculoCuentaAno, vehiculoCuentaTanque, vehiculoCuentaAlias, vehiculoCuentaTipo);
  // }

  async homeVehiculoCrear(cuentaId: number, vehiculo:Vehiculo): Promise<ResponseTipo> {
    return await this.apiGateway.homeVehiculoCrear(cuentaId, vehiculo);
  }

  async homeVehiculoModificar(cuentaId:number, estado:number, vehiculo:Vehiculo){
    return await this.apiGateway.homeVehiculoModificar(cuentaId, estado, vehiculo);
  }

  async homeVehiculoAsignar(cuentaId:number, usuarioId:number, vehiculos:number[]): Promise<ResponseTipo>{
    return await this.apiGateway.homeVehiculoAsignar(cuentaId, usuarioId, vehiculos);
  }

  async homeVehiculoDesasignar(cuentaId:number, usuarioId:number, vehiculos:number[]): Promise<ResponseTipo>{
    return await this.apiGateway.homeVehiculoDesasignar(cuentaId, usuarioId, vehiculos);
  }

  async homeFlotaCrear(cuentaId: number, flotaCuentaNombre: string): Promise<ResponseTipo> {
    return await this.apiGateway.homeFlotaCrear(cuentaId, flotaCuentaNombre);
  }

  async homeGrupoCrear(cuentaId: number, grupoCuentaNombre: string): Promise<ResponseTipo> {
    return await this.apiGateway.homeGrupoCrear(cuentaId, grupoCuentaNombre);
  }
  async homeGrupoModificar(cuentaId: number, grupoCuentaId: number, grupoCuentaNombre: string, grupoCuentaEstado: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeGrupoModificar(cuentaId, grupoCuentaId, grupoCuentaNombre, grupoCuentaEstado);
  }

  async homeFlotaModificar(cuentaId: number, flotaCuentaId: number, flotaCuentaNombre: string, flotaCuentaEstado: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeFlotaModificar(cuentaId, flotaCuentaId, flotaCuentaNombre, flotaCuentaEstado);
  }

  async homeUsuarioConsultarFacturas(cuentaId: number, fechaConsultaInicio: string, fechaConsulta: string, posicionFinal:number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarFacturas(cuentaId, fechaConsultaInicio, fechaConsulta, posicionFinal);
  }

  async homeUsuarioConsultarDetalleFacturas(cuentaId: number, facturaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarDetalleFacturas(cuentaId, facturaId);
  }

  async homeUsuarioConsultarTransacciones(cuentaId: number, fechaConsultaInicio: string, fechaConsulta: string, posicionFinal:number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarTransacciones(cuentaId, fechaConsultaInicio, fechaConsulta, posicionFinal);
  }

  async homeUsuarioConsultarMovimientos(cuentaId: number, fechaConsulta: Date): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarMovimientos(cuentaId, fechaConsulta);
  }

  async homeUsuarioConsultarDetalleTransacciones(cuentaId: number, ventaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioConsultarDetalleTransacciones(cuentaId, ventaId);
  }

  async homeCuentaConsultarMedioPago(cuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaConsultarMedioPago(cuentaId);
  }

  async homeCuentaNuevoConsultarMedioPago(cuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaNuevoConsultarMedioPago(cuentaId);
  }

  async homeCuentaMovimientosMedioPago(cuentaId:number, posicionInicial:number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaMovimientosMedioPago(cuentaId, posicionInicial);
  }

  async homeCuentaNuevoPostMedioPago(cuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaNuevoPostMedioPago(cuentaId);
  }

  async homeCuentaModificarMedioPago(formaPagoSaldo: number, formaPagoEstacion: number, cuentaId: number, tipoFormaPagoOneclickEstado: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaModificarMedioPago(formaPagoSaldo, formaPagoEstacion, cuentaId, tipoFormaPagoOneclickEstado);
  }

  async homeCuentaNuevoModificarMedioPago(formaPagoSaldo: number, formaPagoEstacion: number, cuentaId: number, tipoFormaPagoOneclickEstado: number, tarjetaCompartidaId: string, tarjetaCompartidaUltimos4Digitos: string, tarjetaCompartidaTipo: string): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaNuevoModificarMedioPago(formaPagoSaldo, formaPagoEstacion, cuentaId, tipoFormaPagoOneclickEstado, tarjetaCompartidaId, tarjetaCompartidaUltimos4Digitos, tarjetaCompartidaTipo);
  }

  async homeCuentaNuevoEliminarMedioPago(cuentaId: number, idInscripcion: string): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaNuevoEliminarMedioPago(cuentaId, idInscripcion);
  }

  async homeCuentaAsociarCuentaCorriente(cuentaId: number, cuentaCorrienteNumero: number, cuentaCorrienteRut: string, tipoCuentaBancariaId: number, bancoId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaAsociarCuentaCorriente(cuentaId, cuentaCorrienteNumero, cuentaCorrienteRut, tipoCuentaBancariaId, bancoId);
  }

  async homeCuentaDesasociarCuentaCorriente(cuentaId: number, inscripcionId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaDesasociarCuentaCorriente(cuentaId, inscripcionId);
  }

  async homeUsuarioCuentaObtener(cuentaId: number, usuarioObtenerId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioCuentaObtener(cuentaId, usuarioObtenerId);
  }

  /** creado por crojas para modificar usuario */
  async homeUsuarioModificar2(data:IModificarConductor): Promise<ResponseTipo>{
    return await this.apiGateway.homeUsuarioModificar2(data.cuentaId, data.usuarioId, data.estado, data.rolId, data.configuracionCombustible, data.restriccionesCombustible);
  }

  async homeVehiculosCuentaObtener(cuentaId:number, posicionInicial:number): Promise<ResponseTipo> {
    return await this.apiGateway.homeVehiculosCuentaObtener(cuentaId, posicionInicial);
  }

  async homeVehiculoCuentaObtener(cuentaId: number, vehiculoCuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeVehiculoCuentaObtener(cuentaId, vehiculoCuentaId);
  }

  async homeUsuarioCuentaModificarRestricciones(cuentaId: number, usuarioId: number, diaNoHabilitadoEstado: number = 0, diaNoHabilitadoValor?: number[], horarioNoHabilitadoEstado: number = 0, horarioNoHabilitadoValor?: number[], estacionNoHabilitadaEstado: number = 0, estacionNoHabilitadaValor?: number[]): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioCuentaModificarRestricciones(cuentaId, usuarioId, diaNoHabilitadoEstado, diaNoHabilitadoValor, horarioNoHabilitadoEstado, horarioNoHabilitadoValor, estacionNoHabilitadaEstado, estacionNoHabilitadaValor);
  }

  async homeUsuarioCuentaAbonoPost(cuentaId: number, idInscripcion: string, monto: number): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioCuentaAbonoPost(cuentaId, idInscripcion, monto);
  }

  async homeUsuarioCuentaAbonoGet(cuentaId: number, numeroProceso: string): Promise<ResponseTipo> {
    return await this.apiGateway.homeUsuarioCuentaAbonoGet(cuentaId, numeroProceso);
  }

  async homeCuentaUsuarioModificarDatos(nombre:string, apellido:string): Promise<ResponseTipo> {
    return await this.apiGateway.homeCuentaUsuarioModificarDatos(nombre, apellido);
  }

  //funciones nuevas
  async llamarPostIngreso(request,url): Promise<ResponseTipo> {
    return await this.apiGateway.llamarPostIngreso(request, url);
  }
  async llamarPUTIngreso(request,url): Promise<ResponseTipo> {
    return await this.apiGateway.llamarPUTIngreso(request, url);
  }
  async llamarPOSTHome(request,url): Promise<ResponseTipo> {
    return await this.apiGateway.llamarPOSTHome(request, url);
  }



  

  // async homeCuentaUsuarioModificar(cuentaId:number, nombre:string,apellido:string): Promise<ResponseTipo>{
  //   return await this.apiGateway.cuentaUsuarioModificar
  // }

}
