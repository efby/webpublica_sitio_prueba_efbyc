import { Injectable } from '@angular/core';
import { HomeService } from './home/home.service';
import { VariablesGenerales } from '../globales/variables-generales';
import { Respuestas } from '../globales/respuestas';
import { RequestTipo, ResponseTipo } from '../models/respuestas-tipo';
import { IModificarConductor } from '../models/conductores';
import { IInvitacionConductor, Vehiculo, IAbonoPost, Cuenta } from '../globales/business-objects';

@Injectable({
    providedIn: 'root'
})
export class RxService {
    
    constructor(private homeService: HomeService, private variablesApi: VariablesGenerales) { 
        
    }

    private evaluateResponseGeneric(result, resolve, reject){
        if(result.estado == Respuestas.RESPUESTA_OK)
            resolve(result);
        else
            reject(result);
    }

    //MEDIOS DE PAGO
    /** Obtiene los medios de pago, cuentas corrientes */
    async mediosPagoGet(cuentaId){
        let result = await this.homeService.homeCuentaNuevoConsultarMedioPago(cuentaId);
        
        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async mediosPagoNuevaTarjeta(cuentaId){
        let result = await this.homeService.homeCuentaNuevoPostMedioPago(cuentaId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        })
    }

    async mediosPagoEliminarMedioPago(cuentaId: number, idInscripcion:string){
        let result = await this.homeService.homeCuentaNuevoEliminarMedioPago(cuentaId, idInscripcion);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        })
    }

    async mediosPagoModificarMedioPago(request:RequestTipo){
        let result = await this.homeService.homeCuentaNuevoModificarMedioPago(this.convertNumber(request.datos.formaPagoSaldo), this.convertNumber(request.datos.formaPagoEstacion), this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.formaPagoOneclick), request.datos.tarjetaCompartidaId, request.datos.tarjetaCompartidaUltimos4Digitos, request.datos.tarjetaCompartidaTipo);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    async mediosPagoAsociarCtaCte(request:RequestTipo){
        let result = await this.homeService.homeCuentaAsociarCuentaCorriente(request.datos.cuentaId, request.datos.cuentaCorrienteNumero, request.datos.cuentaCorrienteRut, request.datos.tipoCuentaBancariaId, request.datos.bancoId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    async mediosPagoDesasociarCtaCte(request:RequestTipo){
        let result = await this.homeService.homeCuentaDesasociarCuentaCorriente(request.datos.cuentaId, request.datos.inscripcionId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    async mediosPagoMovimientosGet(cuentaId: number, posicionInicial:number){
        let result = await this.homeService.homeCuentaMovimientosMedioPago(cuentaId, posicionInicial);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    async mediosPagoAbonoPost(abono:IAbonoPost){
        let result = await this.homeService.homeUsuarioCuentaAbonoPost(abono.cuentaId, abono.idTarjeta, abono.monto);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    //CONDUCTORES
    async conductoresGet(cuentaId: number, posicionInicial:number){
        let result = await this.homeService.homeUsuarioConsultarUsuarios2(cuentaId, posicionInicial);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    async conductoresRestriccionesGet(cuentaId:number){
        let result = await this.homeService.homeUsuarioConsultarRestricciones(cuentaId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    async conductoresGetConductor(usuarioId:number, cuentaId:number){
        let result = await this.homeService.homeUsuarioCuentaObtener(cuentaId, usuarioId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    async conductoresModificaConductor(data:IModificarConductor){
        let result = await this.homeService.homeUsuarioModificar2(data);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject)
        });
    }

    //FACTURACION
    async facturacionGet(cuentaId:number){
        let result = await this.homeService.homeConsultarDatosFacturacion(cuentaId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async facturacionCrearDatos(request:RequestTipo){
        let result = await this.homeService.homeCrearDatosFacturacion(request.datos.cuentaId, request.datos.datosFacturacionRut, request.datos.datosFacturacionEmail, request.datos.datosFacturacionGiro, request.datos.datosFacturacionDistrito, request.datos.datosFacturacionDireccion, request.datos.datosFacturacionRazonSocial, request.datos.datosFacturacionCodigoPostal, request.datos.datosFacturacionNombreFantasia, request.datos.datosFacturacionActividadEconomica, request.datos.datosFacturacionPeriodoFacturacion, request.datos.restringeEleccionDte, request.datos.dtePuedeFacturar, request.datos.datosFacturacionNserie);
    
        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    //CUENTA
    async cuentaUsuarioModificarDatos(nombre:string, apellido:string){
        let result = await this.homeService.homeCuentaUsuarioModificarDatos(nombre, apellido);

        return new Promise<ResponseTipo>((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    
  

    async cuentaUsuarioModificar(cuenta:Cuenta, estado:number){
        let result = await this.homeService.homeModificarCuentaEmpresa(cuenta, estado);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    //INVITACIONES
    async invitacionesGet(cuentaId:number, posicionInicial:number){
        let result = await this.homeService.homeUsuarioConsultarInvitacion(cuentaId, posicionInicial);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async invitacionInvitarConductor(request:RequestTipo){
        let result = await this.homeService.homeUsuarioInvitarConductor(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.rolId), request.datos.invitacionTelefonoId, request.datos.invitacionNombre, request.datos.invitacionApellido, this.convertNumber(request.datos.estado), this.convertNumber(request.datos.montoMaximoDiaValor), this.convertNumber(request.datos.montoMaximoMesValor), request.datos.usuarioCuentaConduce);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async invitacionCancelarInvitacionConductor(cuentaId:number, invitacionId:number){
        let result = await this.homeService.homeUsuarioCancelaInvitacion(cuentaId, invitacionId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async invitacionCrearInvitaciones(cuentaId:number, invitaciones: IInvitacionConductor[]){
        let result = await this.homeService.homeUsuarioInvitarConductores(cuentaId,invitaciones);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    //CUENTAS
    async obtenerWeb(){
        let result = await this.homeService.homeUsuarioObtener();
        
        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async cuentasGet(){
        
        let result = await this.homeService.homeUsuarioObtener();

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async cuentasInvitacionAceptarRechazar(data:any){
        let result = await this.homeService.homeUsuarioAceptaInvitacion(data.invitacionId, data.invitacionAceptada);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async cuentasCrear(nombre:string){
        let result  = await this.homeService.homeCrearCuentaEmpresa(nombre);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async cuentasCambiarCuentaSeleccionada(cuentaId:number){
        let result = await this.homeService.homeCambiarCuentaSeleccionada(cuentaId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    //HISTORIAL
    async transaccionesGet(cuentaId: number, fechaInicio: string, fechaFin: string, posicionFinal:number){
        let result = await this.homeService.homeUsuarioConsultarTransacciones(cuentaId, fechaInicio, fechaFin, posicionFinal);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async transaccionesDetalleGet(cuentaId: number, ventaId:number){
        let result = await this.homeService.homeUsuarioConsultarDetalleTransacciones(cuentaId, ventaId);
    
        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async facturasGet(cuentaId:number, fechaInicio: string, fechaFin: string, posicionFinal:number){
        let result = await this.homeService.homeUsuarioConsultarFacturas(cuentaId, fechaInicio, fechaFin, posicionFinal);
        
        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async facturasDetalleGet(cuentaId:number, facturaId:number){
        let result = await this.homeService.homeUsuarioConsultarDetalleFacturas(cuentaId, facturaId);
    
        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    //VEHICULOS
    async vehiculosGet(cuentaId:number, posicionInicial:number){
        let result = await this.homeService.homeVehiculosCuentaObtener(cuentaId, posicionInicial);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async vehiculoGetVehiculo(cuentaId:number, vehiculoId:number){
        let result = await this.homeService.homeVehiculoCuentaObtener(cuentaId, vehiculoId);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async vehiculosCrear(cuentaId:number, vehiculo:Vehiculo){
        let result = await this.homeService.homeVehiculoCrear(cuentaId, vehiculo);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async vehiculosModificar(cuentaId:number, estado:number, vehiculo:Vehiculo){
        let result = await this.homeService.homeVehiculoModificar(cuentaId, estado, vehiculo);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async vehiculosAsignar(cuentaId:number, usuarioId:number, vehiculos:number[]){
        let result = await this.homeService.homeVehiculoAsignar(cuentaId, usuarioId, vehiculos);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }
    
    async vehiculosDesasignar(cuentaId:number,usuarioId:number, vehiculos:number[]){
        let result = await this.homeService.homeVehiculoDesasignar(cuentaId, usuarioId, vehiculos);

        return new Promise((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }


    //FUNCIONES NUEVAS

    async llamarPUTIngreso(request,url){
        let result = await this.homeService.llamarPUTIngreso(request, url);

        return new Promise<ResponseTipo>((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }
    async llamarPOSTIngreso(request,url){
        let result = await this.homeService.llamarPostIngreso(request, url);

        return new Promise<ResponseTipo>((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    async llamarPOSTHome(request,url){
        let result = await this.homeService.llamarPOSTHome(request, url);

        return new Promise<ResponseTipo>((resolve, reject) => {
            this.evaluateResponseGeneric(result, resolve, reject);
        });
    }

    //LOCAL UTILS
    private convertNumber(valor: string): number {
        let val = parseInt(valor);
        return !isNaN(val) ? val : -1;
    }

}
