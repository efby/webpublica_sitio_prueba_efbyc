import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Respuestas } from '../globales/respuestas';
import { ModalSCComponent } from '../modales/modal-sc/modal-sc.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Paginas } from '../globales/paginas';
import { NroDocumentoComponent } from '../modales/nro-documento/nro-documento.component';
import { ModalSubirArchivoComponent } from "../modales/modal-subir-archivo/modal-subir-archivo.component";
import { NuevaEmpresaComponent } from "../paginas/home/perfil/configurar-empresa/nueva-empresa/nueva-empresa.component";
import { InvitarUsuarioComponent } from "../paginas/home/perfil/configurar-empresa/invitar-usuario/invitar-usuario.component";
import { ModalAceptarInvitacionComponent } from '../modales/modal-aceptar-invitacion/modal-aceptar-invitacion.component';
import { AceptarCancelarComponent } from "../modales/aceptar-cancelar/aceptar-cancelar.component";
import { OneClickComponent } from "../modales/one-click/one-click.component";
import { formatDate } from '@angular/common';
import { NuevoDatosFacturacionComponent } from '../paginas/home/perfil/configurar-empresa/nuevo-datos-facturacion/nuevo-datos-facturacion.component';
import { DeviceInfo } from '../models/global';
import { DeviceOrientation } from '../globales/enumeradores';
import { FormGroup, FormControl } from '@angular/forms';

import * as moment from 'moment';
import 'moment/min/locales';
import { MuevoGenericComponent } from '../modales/muevo-generic/muevo-generic.component';
moment.locale('es')

@Injectable({
    providedIn: 'root'
})
export class UtilitariosService {
    timeOutSpinner: any;
    constructor(private toastr: ToastrService, private spinner: NgxSpinnerService, private modalService: NgbModal) { }
    
    msg(mensaje: any) {
        switch (mensaje.tipoMensaje) {
            case Respuestas.toastrError:
            this.toastr.error(mensaje.textoMensaje, mensaje.tituloMensaje, { timeOut: 4000, tapToDismiss: true, positionClass: "toast-top-full-width" })
            break;
            case Respuestas.toastrInfo:
            this.toastr.info(mensaje.textoMensaje, mensaje.tituloMensaje, { timeOut: 4000, tapToDismiss: true, positionClass: "toast-top-full-width" })
            break;
            case Respuestas.toastrWarning:
            this.toastr.warning(mensaje.textoMensaje, mensaje.tituloMensaje, { timeOut: 4000, tapToDismiss: true, positionClass: "toast-top-full-width" })
            break;
            case Respuestas.toastrSuccess:
            this.toastr.success(mensaje.textoMensaje, mensaje.tituloMensaje, { timeOut: 4000, tapToDismiss: true, positionClass: "toast-top-full-width" })
            break;
            default:
            break;
        }
    }
    
    msgPlain(mensaje: any): boolean {
        let retorno: boolean = false;
        switch (mensaje.tipoMensaje) {
            case 'alert':
            alert(mensaje.textoMensaje);
            retorno = true;
            break;
            case 'confirm':
            retorno = confirm(mensaje.textoMensaje);
            break;
            default:
            break;
        }
        
        return retorno;
    }
    
    msgPlainModal(title:string, message:string[]): Promise<any> {
        let modalRef = this.openModal(Paginas.ModalAceptarCancelarComponent, {
            title: title,
            messages: message
        });
        return modalRef;
    }

    msgPlainModalAccept(title:string, message:string[]): Promise<any>{
        let modalRef = this.openModal(Paginas.ModalAceptar, {
            title: title,
            messages: message
        });
        return modalRef;
    }
    
    pintaValidacion(objeto: any, valor: string, tipo: string = 'body', tipoVal = 0) {
        let color: string;
        
        if (objeto == null) {
            console.log("error objeto no existente");
            return true;
        }
        
        let textoAyuda: any = document.getElementById(objeto.id + "_");
        
        switch (tipo) {
            case 'body':
            color = "#c5c9df";
            break;
            case 'sidebar':
            color = "#979BAD";
            break;
            case 'facturacion':
            color = '#CBCFE3';
            break;
            default:
            break;
        }
        if (tipoVal != 0) {
            if (valor === "") {
                objeto.style.color = "#EB5F5F";
                if (textoAyuda != null) {
                    textoAyuda.style.display = 'block';
                }
                return true;
            } else if (isNaN(parseInt(valor))) {
                objeto.style.color = color;
                if (textoAyuda != null) {
                    textoAyuda.style.display = 'none';
                }
                return false;
            } else if (parseInt(valor) <= 0) {
                objeto.style.color = "#eb5f5f";
                if (textoAyuda != null) {
                    textoAyuda.style.display = 'block';
                }
                return true;
            } else {
                objeto.style.color = color;
                if (textoAyuda != null) {
                    textoAyuda.style.display = 'none';
                }
                return false;
            }
        } else {
            if (valor === "") {
                objeto.style.border = "1px solid #EB5F5F";
                if (textoAyuda != null) {
                    textoAyuda.style.display = 'block';
                }
                return true;
            } else if (isNaN(parseInt(valor))) {
                objeto.style.border = "1px solid " + color;
                if (textoAyuda != null) {
                    textoAyuda.style.display = 'none';
                }
                return false;
            } else if (parseInt(valor) <= 0) {
                objeto.style.border = "1px solid #eb5f5f";
                if (textoAyuda != null) {
                    textoAyuda.style.display = 'block';
                }
                return true;
            } else {
                objeto.style.border = "1px solid " + color;
                if (textoAyuda != null) {
                    textoAyuda.style.display = 'none';
                }
                return false;
            }
        }
        
        
    }
    
    showSpinner(name: string = "Interno") {
        this.spinner.show(name, {
            fullScreen: true
        });
        this.timeOutSpinner = setTimeout(() => {
            this.hideSpinner(name);
        }, 30000);
    }
    
    hideSpinner(name: string = "Interno") {
        setTimeout(() => {
            this.spinner.hide(name);
            clearInterval(this.timeOutSpinner);
        }, 500);
    }

    
    
    formatMaxInput(item: string, maxValue: number) {
        let retorno = item;
        if (item.length > maxValue) {
            retorno = item.slice(0, maxValue);
        }
        return retorno;
    }
    
    
    formatCurrency(item: string) {
        item = item.replace(/\./g, "");
        let res = item.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        return res;
    }
    
    formatPatente(item: string) {
        let re = new RegExp('^([a-zA-Z]{2}[a-zA-Z0-9]{2}[0-9]{2}$)');
        return re.test(item);
    }
    
    formatNumbers(item: string) {
        if (typeof item == 'undefined') {
            return "";
        } else if (item !== "") {
            return item.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
        } else {
            return item;
        }
    }
    
    date2Human(date: any, format = 'dd/MM/yyyy', origin?: any) {
        if (typeof date == 'undefined') {
            date = new Date().toDateString();
        }
        
        if (typeof date == "string") {
            
            if (date.length < 19) {
                return date;
            }
            
            date = date.replace(/-/g, "/").substring(0, 19);
        }
        const locale = 'en-us';
        return formatDate(date, format, locale);
    }
    
    async openModal(tipoModal, data?: any) {
        return await new Promise((resolve, reject) => {
            let modalRef;
            switch (tipoModal) {
                case Paginas.ModalSCComponent:
                    modalRef = this.modalService.open(ModalSCComponent);
                    break;
                case Paginas.ModalNroDocumentoComponent:
                    modalRef = this.modalService.open(NroDocumentoComponent);
                    modalRef.componentInstance.modal = {
                        title: "¿Donde encontrar su número de documento?"
                    };
                    break;
                case Paginas.ModalSubirArchivoComponent:
                    modalRef = this.modalService.open(ModalSubirArchivoComponent);
                    break;
                case Paginas.ModalAgregarEmpresaComponent:
                    modalRef = this.modalService.open(NuevaEmpresaComponent, { size: 'lg' });
                    break;
                case Paginas.ModalAgregarDatosFacturacionComponent:
                    modalRef = this.modalService.open(NuevoDatosFacturacionComponent, { size: 'lg' });
                    modalRef.componentInstance.nvaCuentaId = data.nvaCuentaId;
                    modalRef.componentInstance.actividadesEconomicas = data.actividadesEconomicas;
                    modalRef.componentInstance.periodosFacturacion = data.periodosFacturacion;
                    break;
                case Paginas.ModalInvitarAdminComponent:
                    modalRef = this.modalService.open(InvitarUsuarioComponent, { size: 'lg' });
                    break;
                case Paginas.ModalAceptarInvitacionComponent:
                    modalRef = this.modalService.open(ModalAceptarInvitacionComponent);
                    modalRef.componentInstance.modal = {
                        idInvitacion: data.idInvitacion,
                        nombreInvitado: data.nombreInvitado,
                        rolInvitado: data.rolInvitado,
                        vigenciaInvitacion: data.vigenciaInvitacion,
                        nombreEmpresa: data.nombreEmpresa
                    };
                    break;
                case Paginas.ModalAceptarCancelarComponent:
                case Paginas.ModalAceptar:
                    modalRef = this.modalService.open(AceptarCancelarComponent, { centered: true });
                    modalRef.componentInstance.useCancel = tipoModal !== Paginas.ModalAceptar;
                    modalRef.componentInstance.modal = data;
                break;
                case Paginas.ModalOneClickComponent:
                    modalRef = this.modalService.open(OneClickComponent, { size: 'lg' });
                    modalRef.componentInstance.url = data.url;
                    break;
                case Paginas.ModalMuevo:
                    modalRef = this.modalService.open(MuevoGenericComponent, { centered: true });
                    modalRef.componentInstance.modal = {
                        title: data.title,
                        messages: data.messages,
                        
                        okText: data.ok || 'Aceptar',
                        cancelText: data.cancel || 'Cancelar'
                    };
                    
                    break;
                default:
                    break;
            }
            
            modalRef.componentInstance.passEntry.subscribe((receivedEntry) => {
                resolve(receivedEntry);
                modalRef.close();
            });
        }
        );
    }
    
    public static async obtenerSecretA(uuid: string) {
        
        let date = new Date();
        let pad = "00"
        let mes: string = pad.substring(0, pad.length - (date.getMonth() + 1).toString().length).toString() + (date.getMonth() + 1).toString();
        let dia: string = pad.substring(0, pad.length - (date.getDate()).toString().length).toString() + (date.getDate()).toString();
        let ano: string = date.getFullYear().toString();
        let semilla: string = uuid + ano + mes + dia + Math.random().toString();
        
        let digest = await crypto.subtle.digest("SHA-256", new TextEncoder().encode(semilla)).then((data) => {
            return btoa(new Uint8Array(data).reduce((s, b) => s + String.fromCharCode(b), ''))
        });
        let str = "";
        for (var i = 0; i < digest.length; i++) {
            str += digest[i].charCodeAt(0).toString(16);
        }
        // console.log(digest.slice(0, 20))
        // console.log(str.slice(0, 16))
        // console.log(date.getFullYear() + "_" + mes + "_" + dia)
        
        
        return str.slice(0, 20);
    }
    
    public getDistancia(coord1: coordinate, coord2: coordinate) {
        let a1 = this.toRadians(coord1.lat);
        let a2 = this.toRadians(coord2.lat);
        let delta = this.toRadians(coord1.lng - coord2.lng);
        let factorKM = 111.13384;
        let distancia = Math.acos((Math.sin(a1) * Math.sin(a2)) + (Math.cos(a1) * Math.cos(a2) * delta)) * factorKM
        return distancia
    }
    
    private toRadians(value) {
        return (value * (Math.PI / 180));
    }
    
    public getMeses(month) {
        let meses = [];
        switch (month) {
            case 1:
            meses = [
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
            ]
            break;
            case 2:
            meses = [
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
            ]
            break;
            case 3:
            meses = [
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
            ]
            break;
            case 4:
            meses = [
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
            ]
            break;
            case 5:
            meses = [
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
            ]
            break;
            case 6:
            meses = [
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
            ]
            break;
            case 7:
            meses = [
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
            ]
            break;
            case 8:
            meses = [
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
            ]
            break;
            case 9:
            meses = [
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
            ]
            break;
            case 10:
            meses = [
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
            ]
            break;
            case 11:
            meses = [
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
                { id: 12, text: 'Diciembre' },
            ]
            break;
            case 12:
            meses = [
                { id: 12, text: 'Diciembre' },
                { id: 11, text: 'Noviembre' },
                { id: 10, text: 'Octubre' },
                { id: 9, text: 'Septiembre' },
                { id: 8, text: 'Agosto' },
                { id: 7, text: 'Julio' },
                { id: 6, text: 'Junio' },
                { id: 5, text: 'Mayo' },
                { id: 4, text: 'Abril' },
                { id: 3, text: 'Marzo' },
                { id: 2, text: 'Febrero' },
                { id: 1, text: 'Enero' },
            ]
            break;
            default:
            break;
        }
        return meses;
    }
    
    public static getPlatformInfo(): DeviceInfo{
        let res: DeviceInfo = {
            orientation: window.innerWidth < window.innerHeight ? DeviceOrientation.VERTICAL : DeviceOrientation.HORIZONTAL,
            isMobile: window.innerWidth <= 767,
            width: window.innerWidth,
            heigth: window.innerHeight
        }
        return res;
    }

    arrayContains(collection: Array<any>, propertie: string, value: any, options?: SearchArrayOptions): boolean {
        return this.arrayIndexOf(collection, propertie, value, options) > -1;
    }

    arrayIndexOf(collection: Array<any>, propertie: string, value: any, options?: SearchArrayOptions): number {
        options = options || {};
        options.stringCaseInsensitive = typeof options.stringCaseInsensitive !== 'undefined' ? options.stringCaseInsensitive : false;
        if (options.stringCaseInsensitive)
            value = String(value).toUpperCase();

        for (let i in collection) {
            if (propertie == null || propertie == '') {
                if (collection[i] === value)
                    return +i;
            }
            else if (options.stringCaseInsensitive) {
                if (String(collection[i][propertie]).toUpperCase() === value)
                    return +i;
            }
            else if(propertie.indexOf('.') > -1){
                 let val = this.getObjectPropertie(collection[i], propertie);
                 if(val === value)
                    return +i;
            }
            else {
                if (collection[i][propertie] === value)
                    return +i;
            }
        }
        return -1;
    }

    removeOfList(list, keyName: string, value:any){
        let index = this.arrayIndexOf(list, keyName, value);
        if(index > -1){
            list.splice(index, 1);
            return true;
        }
        return false;
    }

    getObjectPropertie(o: any, propertieName: string){
        let res = o;
        let nav = propertieName.split('.');
        for(let i = 0; i <= nav.length; i++){
            res = this.getObjectValue(res, nav[0]);
            nav = nav.splice(1, nav.length);
        }
        return res;   
    }

    getObjectValue(o, propertiename: string){
        return o[propertiename];
    }

    isNullOrWithSpace(value:string){
        let result = false;

        if(value == undefined)
            result = true;
        else if(value.trim() == '')
            result = true
        
        return result;
    }

    /** Clona un objeto sin usar su referencia */
    clone(source): any{
        let result = JSON.parse(JSON.stringify(source));
        return result;
    }

    /** Indica si debe mostrar un error de validacion de formulario */
    showControlError = (control:FormControl, force: boolean = false) => {
        // form: FormGroup, fieldName: string, ) : boolean => {
        //let control = form.controls[fieldName];
        let res = control != undefined
            && !control.valid && (control.dirty || force)
           
        return res;
    }

    validateRut(rutCliente: string) {
        rutCliente = rutCliente.toUpperCase();
        let rutSinDV = parseInt(rutCliente.slice(0, -1)), loop = 0, resto = 1;
        while (rutSinDV > 0) {
            resto = (resto + rutSinDV % 10 * (9 - loop++ % 6)) % 11;
            rutSinDV = Math.floor(rutSinDV / 10);
        }
        let digitoVerificador = (resto > 0) ? (resto - 1) + '' : 'K';
        return (digitoVerificador === rutCliente.slice(-1));
    }

    validatePatente(event:any){
        let pattern = /[^0-9a-zA-Z]/g;

        if (pattern.test(event.key))
            event.preventDefault();

        // let item = control.srcElement;
        // item.value = item.value.replace(/[^0-9a-zA-Z]/g, '').toUpperCase();

        // if (item.value.length > item.maxLength) {
        //     item.value = item.value.slice(0, item.maxLength);
        // }
    }


    //Valida textos: numeros letras y espacio
    validaMaxInputText(event:any){
        let pattern = /[^0-9a-zA-ZáéíñóúÁÉÍÑÓÚÜ_-\s]/g;

        if (pattern.test(event.key))
            event.preventDefault();
    }

    validaMaxInputCurrency(event:any){
        let pattern = /[^0-9\.]/g;

        if ((event.key != 'Backspace' && event.key != 'Tab') && pattern.test(event.key))
            event.preventDefault();
    }
    

    validaMaxInputNumber(event:any){
        let pattern = /[^0-9]/g;

        if ((event.key != 'Backspace' && event.key != 'Tab') && pattern.test(event.key))
            event.preventDefault();
    }

    // validaMaxInputText(ingreso: any) {
    //     let item = ingreso.srcElement;
    
    //     item.value = item.value.replace(/[^0-9a-zA-ZáéíñóúüÁÉÍÑÓÚÜ_-\s]/g, '');
    
    //     if (item.value.length > item.maxLength) 
    //         item.value = item.value.slice(0, item.maxLength);
        
    //     console.log(item.value)
    // }
    
    //Valida textos: numeros letras, @ y .
    validaMaxInputMail(event: any) {

        let pattern = /[^0-9a-zA-Z_\.-@\s]/g;

        if (pattern.test(event.key)){
            event.preventDefault();
            console.log('cancel key: ', event.key)
        }

        // let item = ingreso.srcElement;
        // console.log(item.value)
        // item.value = item.value.replace(/[^0-9a-zA-Z_\.-@\s]/g, '');
    
        // if (item.value.length > item.maxLength) {
        //   item.value = item.value.slice(0, item.maxLength);
        // }
    }
    
}

export interface coordinate {
    lat: number;
    lng: number;
}

export class SearchArrayOptions{
    stringCaseInsensitive?: boolean
}
