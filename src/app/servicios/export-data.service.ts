import { Injectable } from '@angular/core';
import { AngularCsv } from 'angular7-csv/dist/Angular-csv'

@Injectable({
    providedIn: 'root'
})
export class ExportDataService {

    constructor() { }
    
    public static ExportCsv(filename:string, rows:any[], header:string[], charSeparator:string = ',', decimalSeparator: string = '.', title: string = ''){
        let opts = {
            fieldSeparator: charSeparator,
            quoteStrings: '"',
            decimalSeparator: decimalSeparator,
            showLabels: true,
            showTitle: true,
            title: title,
            useBom: true,
            noDownload: false,
            headers: header
        }
        new AngularCsv(rows, filename, opts);
    }
}
