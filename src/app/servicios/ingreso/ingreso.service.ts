import { Injectable } from '@angular/core';
import { ResponseTipo } from 'src/app/models/respuestas-tipo';
import { Respuestas } from 'src/app/globales/respuestas';
import { GetInfoService } from 'src/app/providers/getInfo/get-info.service';
import { ApiGatewayService } from 'src/app/providers/api-gateway/api-gateway.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';


@Injectable({
  providedIn: 'root'
})
export class IngresoService {

  constructor(private apiGateway: ApiGatewayService, private variablesApi: VariablesGenerales) { }

  public async obtenerStorage() {
    let retorno: ResponseTipo = new ResponseTipo();
    if (typeof localStorage !== 'undefined') {
      if (localStorage.getItem("authorizationToken") !== null && localStorage.getItem("equipoSecret") !== null && localStorage.getItem("tipoToken") !== null) {
        retorno.estado = Respuestas.RESPUESTA_OK;
      } else {
        retorno.estado = Respuestas.RESPUESTA_NOE;
        retorno.paginaRespuesta = "";
      }
    } else {
      // localStorage no Disponible
      retorno.estado = Respuestas.RESPUESTA_NOK;
      retorno.paginaRespuesta = "";
      retorno.mensajeSalida = { tipoMensaje: Respuestas.toastrError, tituloMensaje: "", textoMensaje: "Storage No Activo" };
    }
    return await retorno;
  }

  public async obtieneInfoDispositivo(): Promise<ResponseTipo> {
    let retorno: ResponseTipo = new ResponseTipo();
    retorno.estado = Respuestas.RESPUESTA_OK;
    retorno.data = GetInfoService.getBrowserData();
    localStorage.setItem("datosEquipo", GetInfoService.getBrowserData());
    return await retorno;
  }

  public async creaSecretB(equipoSecretA: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoEquipoCrear(equipoSecretA);
  }

  public async ingresoTelefonoCrear(telefonoId: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoTelefonoCrear(telefonoId);
  }

  public async ingresoTelefonoLlamada(telefonoId: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoTelefonoLlamada(telefonoId);
  }

  public async ingresoCodigoSMS(telefonoId: string, smsToken: string, smsValor: string): Promise<ResponseTipo> {
    return await this.apiGateway.IngresoTelefonoValidar(telefonoId, smsToken, smsValor);
  }

  public async reenviarCodigoSMS(telefonoId: string, smsToken: string): Promise<ResponseTipo> {
    return await this.apiGateway.IngresoTelefonoReenviar(telefonoId, smsToken);
  }

  public async ingresoRutIdRegistro(telefonoId: string, usuarioId: string, numeroDocumento: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoUsuarioConsultarEstado(telefonoId, usuarioId, numeroDocumento);
  }

  public async ingresoRutIdRecuperacion(telefonoId: string, usuarioId: string, numeroDocumento: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoUsuarioConsultarEstado(telefonoId, usuarioId, numeroDocumento);
  }

  public async ingresoMail(mail: string): Promise<ResponseTipo> {
    return await this.apiGateway.IngresoMail(mail);
  }

  public async ingresoCreaContrasena(telefonoId: string, usuarioId: string, nombre: string, apellido: string, mail: string, password: string, numeroDocumento: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoUsuarioCrear(telefonoId, usuarioId, nombre, apellido, mail, password, numeroDocumento);
  }

  public async ingresoModificaContrasena(telefonoId: string, usuarioId: string, password: string, numeroDocumento: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoUsuarioModificar(telefonoId, usuarioId, password, numeroDocumento);
  }

  public async ingresoUsuarioValidar(telefonoId: string, usuarioId: string, password: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoUsuarioValidar(telefonoId, usuarioId, password)
  }

  public async ingresoUsuarioValidarLogin(telefonoId: string, password: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoUsuarioValidarLogin(telefonoId, password)
  }

  public async ingresoCrearPin(telefonoId: string, usuarioId: string, pinValor: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoPinCrear(telefonoId, usuarioId, pinValor);
  }

  public async ingresoValidarPin(telefonoId: string, usuarioId: string, pinValor: string): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoPinValidar(telefonoId, usuarioId, pinValor);
  }

  public async ingresoUsuarioObtener(esPrimerIngreso:boolean = false, aceptarTerminos:boolean = false): Promise<ResponseTipo> {
    
    return await this.apiGateway.obtenerWeb(esPrimerIngreso, aceptarTerminos);
  }

  public async ingresoObtenerMaestros(cuentaId: number): Promise<ResponseTipo> {
    return await this.apiGateway.ingresoObtenerMaestros(cuentaId);
  }

  public async IngresoTokenPost(password: string): Promise<ResponseTipo> {
    return await this.apiGateway.IngresoTokenPost(password);
  }

  public async IngresoTokenMailPost(): Promise<ResponseTipo> {
    return await this.apiGateway.IngresoTokenMailPost();
  }

  public async IngresoTokenTelefonoPost(rut: string, numeroSerieRut: string): Promise<ResponseTipo> {
    return await this.apiGateway.IngresoTokenTelefonoPost(rut, numeroSerieRut);
  }

  public async IngresoTokenPassPost(codigo: string, password: string): Promise<ResponseTipo> {
    return await this.apiGateway.IngresoTokenPassPost(codigo, password);
  }

}
