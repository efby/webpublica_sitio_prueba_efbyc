import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BreadCrumbsService {

  constructor() { }

  private emitChangeSource = new Subject<any>();
  private emmitCallback = new Subject<any>();

  callbackEmmited$ = this.emmitCallback.asObservable();
  changeEmitted$ = this.emitChangeSource.asObservable();

  emitChange(data: any): any {
    this.emitChangeSource.next(data);
  }

  emmitChange(data: IEmmitModel): any {
    this.emitChangeSource.next(data);
  }

  emitCallback(data:IEmmitCallbackModel){
    this.emmitCallback.next(data);
  }
}

export interface IEmmitModel{
  tipo: string,
  action: string,
  data?: any
}

export interface IEmmitCallbackModel{
    from: string,
    action: string,
    data?: any;
}
