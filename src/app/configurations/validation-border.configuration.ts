export const ValidationBorderConfiguration = {
    borderStyle: 'dashed',
    borderWidth: '1px',
    colors: {
        VALID: { value: 'green', enabled: false },
        INVALID: { value: 'red', enabled: true },
        PENDING: { value: 'yellow', enabled: false },
        DISABLED: { value: 'silver', enabled: false }
    }
}