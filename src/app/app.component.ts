import { Component } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError, RouterOutlet } from '@angular/router';
import { Paginas } from './globales/paginas';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'appEmpresas';
  msgCarga = "Cargando ..."
  _true: boolean = true;

  constructor(private router: Router) {

    router.events.subscribe((event) => {

      if (event instanceof NavigationStart) {
        if (event.navigationTrigger === 'popstate') {
        }
      }

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
        let url = this.router.url.substr(1);
        if (url != Paginas.IngresoInicioPage) {
          if (localStorage.getItem("authorizationToken") == null) {
            this.router.navigate([Paginas.IngresoInicioPage])
          }
        }
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator

        // Present error to user
        console.log(event.error);
      }
    });

  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

    this.router.navigate([Paginas.IngresoInicioPage])
    //window.location.href = 'http://muevocopec.cl/';
  }
}
