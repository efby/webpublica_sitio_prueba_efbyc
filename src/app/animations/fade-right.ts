import { trigger, state, animate, transition, style } from '@angular/animations';

export const fadeOutToRight = trigger('fadeOutToRight', [
        // trigger name for attaching this animation to an element using the [@triggerName] syntax
        state('false' , style({ opacity: 1 })), 
        state('true', style({ 
            opacity: 0.3,
            transform: 'translateX(300px)'
         })),

        transition('0 => 1', animate('.3s')),
        //transition('1 => 0', animate('4.99s'))
    ]
);