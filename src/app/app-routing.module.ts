import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IngresoCargandoComponent } from "src/app/paginas/ingreso/ingreso-cargando/ingreso-cargando.component";
import { IngresoTelefonoIdComponent } from "src/app/paginas/ingreso/ingreso-telefono-id/ingreso-telefono-id.component";
import { IngresoComponent } from "src/app/paginas/ingreso/ingreso.component";
import { IngresoConfirmaPinValorComponent } from './paginas/ingreso/ingreso-confirma-pin-valor/ingreso-confirma-pin-valor.component';
import { IngresoContrasenaComponent } from './paginas/ingreso/ingreso-contrasena/ingreso-contrasena.component';
import { IngresoCreaContrasenaComponent } from './paginas/ingreso/ingreso-crea-contrasena/ingreso-crea-contrasena.component';
import { IngresoEmailComponent } from './paginas/ingreso/ingreso-email/ingreso-email.component';
import { IngresoNombreComponent } from './paginas/ingreso/ingreso-nombre/ingreso-nombre.component';
import { IngresoPinValorComponent } from './paginas/ingreso/ingreso-pin-valor/ingreso-pin-valor.component';
import { IngresoRutIdComponent } from './paginas/ingreso/ingreso-rut-id/ingreso-rut-id.component';
import { IngresoSMSValorComponent } from './paginas/ingreso/ingreso-smsvalor/ingreso-smsvalor.component';
import { HomeComponent } from './paginas/home/home.component';
import { ErrorComponent } from './paginas/paginasError/error/error.component';
import { ErrorEscrituraStorageComponent } from './paginas/paginasError/error-escritura-storage/error-escritura-storage.component';
import { ErrorLecturaDispositivoComponent } from './paginas/paginasError/error-lectura-dispositivo/error-lectura-dispositivo.component';
import { ErrorLecturaStorageComponent } from './paginas/paginasError/error-lectura-storage/error-lectura-storage.component';
import { ErrorNOKComponent } from './paginas/paginasError/error-nok/error-nok.component';
import { ErrorSAComponent } from './paginas/paginasError/error-sa/error-sa.component';
import { IngresoValidaPinComponent } from './paginas/ingreso/ingreso-valida-pin/ingreso-valida-pin.component';
import { IngresoRutIdRecuperacionComponent } from './paginas/ingreso/ingreso-rut-id-recuperacion/ingreso-rut-id-recuperacion.component';
import { IngresoRecuperarContrasenaComponent } from './paginas/ingreso/ingreso-recuperar-contrasena/ingreso-recuperar-contrasena.component';
import { ErrorSCComponent } from './paginas/paginasError/error-sc/error-sc.component';
import { GuiasDespachoComponent } from './paginas/home/documentos/guias-despacho/guias-despacho.component';
import { BoletasComponent } from './paginas/home/documentos/boletas/boletas.component';
import { IngresoModificaContrasenaComponent } from './paginas/ingreso/ingreso-modifica-contrasena/ingreso-modifica-contrasena.component';
import { VehiculoPermisosComponent } from './paginas/home/vehiculos/vehiculo-permisos/vehiculo-permisos.component';
import { ConductorConfirmarComponent } from './paginas/home/conductores/conductor-confirmar/conductor-confirmar.component';
import { ConductorCrearComponent } from './paginas/home/conductores/conductor-crear/conductor-crear.component';
import { ConductorPresentacionComponent } from './paginas/home/conductores/conductor-presentacion/conductor-presentacion.component';
import { DashboardComponent } from './paginas/home/dashboard/dashboard.component';
import { AbonoPagoComponent } from './paginas/home/abono/abono-pago/abono-pago.component';
import { AbonoResultadoComponent } from './paginas/home/abono/abono-resultado/abono-resultado.component';
import { CreditoPagoComponent } from './paginas/home/credito/credito-pago/credito-pago.component';
import { CreditoResultadoComponent } from './paginas/home/credito/credito-resultado/credito-resultado.component';
import { ConfigurarPerfilComponent } from './paginas/home/perfil/configurar-perfil/configurar-perfil.component';
import { ConfigurarEmpresaComponent } from './paginas/home/perfil/configurar-empresa/configurar-empresa.component';
import { EstacionesComponent } from './paginas/home/estaciones/estaciones.component';
import { NuevoAdministradorMuevoComponent } from './paginas/ingreso/nuevo-administrador-muevo/nuevo-administrador-muevo.component';
import { NuevoContrasenaMuevoComponent } from './paginas/ingreso/nuevo-contrasena-muevo/nuevo-contrasena-muevo.component';
import { NuevoLoginMuevoComponent } from './paginas/ingreso/nuevo-login-muevo/nuevo-login-muevo.component';
import { HomeMuevoComponent } from './paginas/home-muevo/home-muevo.component';
import { MuevoDashboardComponent } from './paginas/home-muevo/muevo-dashboard/muevo-dashboard.component';
import { NuevoMailMuevoComponent } from './paginas/ingreso/nuevo-mail-muevo/nuevo-mail-muevo.component';
import { NuevoLoginPersistenteMuevoComponent } from './paginas/ingreso/nuevo-login-persistente-muevo/nuevo-login-persistente-muevo.component';
import { MuevoConductorCrearComponent } from './paginas/home-muevo/conductores/muevo-conductor-crear/muevo-conductor-crear.component';
import { MuevoConductorConfirmarComponent } from './paginas/home-muevo/conductores/muevo-conductor-confirmar/muevo-conductor-confirmar.component';
import { MuevoVehiculoConfirmarComponent } from './paginas/home-muevo/vehiculos/muevo-vehiculo-confirmar/muevo-vehiculo-confirmar.component';
import { MuevoMisConductoresComponent } from './paginas/home-muevo/conductores/muevo-mis-conductores/muevo-mis-conductores.component';
import { MuevoMisVehiculosComponent } from './paginas/home-muevo/vehiculos/muevo-mis-vehiculos/muevo-mis-vehiculos.component';
import { MuevoMiFacturacionComponent } from './paginas/home-muevo/facturacion/muevo-mi-facturacion/muevo-mi-facturacion.component';
import { PerfilComponent } from './paginas/home-muevo/perfil/perfil.component';
import { MetodosPagoComponent } from './paginas/home-muevo/metodos-pago/metodos-pago.component';
import { MuevoEquipoSeleccionaCuentaComponent } from './paginas/home-muevo/equipos/muevo-equipo-selecciona-cuenta/muevo-equipo-selecciona-cuenta.component';
import { MuevoEquipoNombreComponent } from './paginas/home-muevo/equipos/muevo-equipo-nombre/muevo-equipo-nombre.component';
import { TransaccionesComponent } from './paginas/home-muevo/transacciones/transacciones.component';
import { MuevoMisInvitacionesComponent } from './paginas/home-muevo/conductores/muevo-mis-invitaciones/muevo-mis-invitaciones.component';
import { AbonarComponent } from './paginas/home-muevo/abonar/abonar.component';
import { MuevoAbonoProcesandoComponent } from './paginas/home-muevo/abonar/muevo-abono-procesando/muevo-abono-procesando.component';
import { RolesPerfilesComponent } from './paginas/home-muevo/roles-perfiles/roles-perfiles.component';
import { HistorialComponent } from './paginas/home-muevo/historial/historial.component';
import { MuevoRecuperaPinComponent } from './paginas/ingreso/muevo-recupera-pin/muevo-recupera-pin.component';
import { NuevoIngresoSoporteComponent } from './paginas/ingreso/nuevo-ingreso-soporte/nuevo-ingreso-soporte.component';
import { CuentasComponent } from './paginas/home-muevo/cuentas/cuentas.component';
import { HistorialesComponent } from './paginas/home-muevo/historiales/historiales.component';
import { FacturasComponent } from './paginas/home-muevo/historiales/facturas/facturas.component';
import { MetodosPagoMovimientosComponent } from './paginas/home-muevo/metodos-pago/metodos-pago-movimientos/metodos-pago-movimientos.component';
import { MuevoConductoresListComponent } from './paginas/home-muevo/conductores/muevo-conductores-list/muevo-conductores-list.component';
import { MuevoConductoresRootComponent } from './paginas/home-muevo/conductores/muevo-conductores-root/muevo-conductores-root.component';
import { ConductorCrearInvitacionComponent } from './paginas/home-muevo/conductores/conductor-crear-invitacion/conductor-crear-invitacion.component';
import { ComponentsUiComponent } from './paginas/home-muevo/ui/components-ui/components-ui.component';
import { IngresoTerminosComponent } from './paginas/ingreso/ingreso-terminos/ingreso-terminos.component';
import { EditarPerfilComponent } from './paginas/home-muevo/perfil/editar-perfil/editar-perfil.component';
import { MisAdministradoresComponent } from './paginas/home-muevo/mis-administradores/mis-administradores.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: "Ingreso/Cargando",
    pathMatch: "full"
  },
  {
    path: 'Ingreso',
    component: IngresoComponent,
    children: [
      {
        path: 'Cargando',
        component: IngresoCargandoComponent,
        data: { animation: 'general' }
      },
      {
        path: 'Cargando/:navigate',
        component: IngresoCargandoComponent,
        data: { animation: 'general' }
      },
      {
        path: 'ConfirmaPinValor',
        component: IngresoConfirmaPinValorComponent,
        data: { animation: 'Home' }
      },
      {
        path: 'ValidarContrasena',
        component: IngresoContrasenaComponent
      },
      {
        path: 'ValidaPin',
        component: IngresoValidaPinComponent
      },
      {
        path: 'CrearContrasena',
        component: IngresoCreaContrasenaComponent
      },
      {
        path: 'ModificarContrasena',
        component: IngresoModificaContrasenaComponent
      },
      {
        path: 'RecuperarContrasena',
        component: IngresoRecuperarContrasenaComponent
      },
      {
        path: 'Email',
        component: IngresoEmailComponent
      },
      {
        path: 'Nombre',
        component: IngresoNombreComponent
      },
      {
        path: 'PinValor',
        component: IngresoPinValorComponent
      },
      {
        path: 'RutId',
        component: IngresoRutIdComponent
      },
      {
        path: 'RecuperaRutId',
        component: IngresoRutIdRecuperacionComponent
      },
      {
        path: 'SMSValor',
        component: IngresoSMSValorComponent
      },
      {
        path: 'TelefonoId',
        component: IngresoTelefonoIdComponent
      },
      {
        path: 'AdministradorMuevo',
        component: NuevoAdministradorMuevoComponent,
        data: { animation: 'admin1' }
      },
      {
        path: 'AdministradorMuevo/:valido',
        component: NuevoAdministradorMuevoComponent,
        data: { animation: 'admin1' }
      },
      {
        path: 'MailMuevo',
        component: NuevoMailMuevoComponent,
        data: { animation: 'admin2' }
      },
      {
        path: 'ContrasenaMuevo',
        component: NuevoContrasenaMuevoComponent,
        data: { animation: 'admin3' }
      },
      {
        path: 'LogInMuevo',
        component: NuevoLoginMuevoComponent,
        data: { animation: 'login' }
      },
      {
        path: 'LogInMuevo/:valido',
        component: NuevoLoginMuevoComponent,
        data: { animation: 'login' }
      },
      {
        path: 'LogInPersMuevo',
        component: NuevoLoginPersistenteMuevoComponent,
        data: { animation: 'login' }
      },
      {
        path: 'RecuperaPinMuevo',
        component: MuevoRecuperaPinComponent,
        data: { animation: 'admin2' }
      },
      {
        path: 'Soporte',
        component: NuevoIngresoSoporteComponent,
        data: { animation: 'admin2' }
      },
      {
        path: 'TerminosCondiciones',
        component: IngresoTerminosComponent,
        data: { animation: 'admin2' }
      }
    ]
  },
  {
    path: 'Home',
    component: HomeComponent,
    children: [
      {
        path: 'Facturas',
        component: FacturasComponent
      },
      {
        path: 'GuiasDespacho',
        component: GuiasDespachoComponent
      },
      {
        path: 'Boletas',
        component: BoletasComponent
      },
      {
        path: 'VehiculoPermisos',
        component: VehiculoPermisosComponent
      },
      {
        path: 'ConductorCrear',
        component: ConductorCrearComponent
      },
      {
        path: 'ConductorConfirmar',
        component: ConductorConfirmarComponent
      },
      {
        path: 'ConductorPresentacion',
        component: ConductorPresentacionComponent
      },
      {
        path: 'AbonoPago',
        component: AbonoPagoComponent
      },
      {
        path: 'AbonoResultado',
        component: AbonoResultadoComponent
      },
      {
        path: 'CreditoPago',
        component: CreditoPagoComponent
      },
      {
        path: 'CreditoResultado',
        component: CreditoResultadoComponent
      },
      {
        path: 'ConfigurarPerfil',
        component: ConfigurarPerfilComponent
      },
      {
        path: 'ConfigurarEmpresa',
        component: ConfigurarEmpresaComponent
      },
      {
        path: 'ConfigurarEmpresa/:idEmpresa',
        component: ConfigurarEmpresaComponent
      },
      {
        path: 'Dashboard',
        component: DashboardComponent
      },
      {
        path: 'Estaciones',
        component: EstacionesComponent
      }
    ]
  },
  {
    path: 'HomeMuevo',
    component: HomeMuevoComponent,
    children: [
      
      {
        path: 'MisAdministradores',
        component: MisAdministradoresComponent
      },
      {
        path: 'Dashboard',
        component: MuevoDashboardComponent
      },
      {
        path: 'dev',
        component: ComponentsUiComponent
      },
      {
        path: 'Dashboard/:ConductoresVehiculo',
        component: MuevoDashboardComponent
      },
      {
        path: 'CrearConductor',
        component: ConductorCrearInvitacionComponent 
        //MuevoConductorCrearComponent
      },
      {
        path: 'CrearConductor/:origen',
        component: MuevoConductorCrearComponent
      },
      {
        path: 'ConfirmarConductor',
        component: MuevoConductorConfirmarComponent
      },
      {
        path: 'MisConductores',
        component: MuevoConductoresRootComponent//MuevoMisConductoresComponent
      },
      {
        path: 'Cuentas',
        component: CuentasComponent
      },
      {
        path: 'MisInvitaciones',
        component: MuevoMisInvitacionesComponent
      },
      {
        path: 'MisInvitaciones/:idCaller',
        component: MuevoMisInvitacionesComponent
      },
      {
        path: 'ConfirmarVehiculo',
        component: MuevoVehiculoConfirmarComponent
      },
      {
        path: 'MisVehiculos',
        component: MuevoMisVehiculosComponent
      },
      {
        path: 'MiFacturacion',
        component: MuevoMiFacturacionComponent
      },
      {
        path: 'MiPerfil',
        component: PerfilComponent
      },
      {
        path: 'MiPerfil/:idPerfil',
        component: PerfilComponent
      },
      {
        path: 'MisRolesYPerfiles',
        component: RolesPerfilesComponent
      },
      {
        path: 'MisMetodosPago',
        component: MetodosPagoComponent
      },
      {
        path: 'MisMetodosPago/:origen',
        component: MetodosPagoComponent
      },
      {
        path: 'MisMetodosPagoMovimientos',
        component: MetodosPagoMovimientosComponent
      },
      {
        path: 'AbonarBilletera',
        component: AbonarComponent
      },
      {
        path: 'AbonarBilletera/:idTipoAbono',
        component: AbonarComponent
      },
      {
        path: 'AbonarProcesando',
        component: MuevoAbonoProcesandoComponent
      },
      {
        path: 'EquipoSelCuenta',
        component: MuevoEquipoSeleccionaCuentaComponent
      },
      {
        path: 'EquipoNombre',
        component: MuevoEquipoNombreComponent
      },
      {
        path: 'MisTransacciones',
        component: HistorialesComponent //TransaccionesComponent
      },
      {
        path: 'MisTransacciones/:facturaId',
        component: HistorialesComponent
      },
      {
        path: 'MiHistorial',
        component: HistorialComponent
      }
    ]
  },
  {
    path: 'Error',
    component: ErrorComponent,
    children: [
      {
        path: 'errorEscrituraStorage',
        component: ErrorEscrituraStorageComponent
      },
      {
        path: 'errorLecturaDispositivo',
        component: ErrorLecturaDispositivoComponent
      },
      {
        path: 'errorLecturaStorage',
        component: ErrorLecturaStorageComponent
      },
      {
        path: 'errorNOK',
        component: ErrorNOKComponent
      },
      {
        path: 'errorSA',
        component: ErrorSAComponent
      },
      {
        path: 'errorSC',
        component: ErrorSCComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top',
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})


export class AppRoutingModule { }
