import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform {
    
    filterType: FilterType;
    
    transform(list: any[], filter: any): any {
        //console.log('aplicando filtro', filter);
        if (!list || !filter)
        return list;
        
        let result = this.applyFilter(list, filter);
        //console.log(`${result.length} resultados`);
        return result;
    }
    
    applyFilter = (list: Array<any>, filter: any): Array<any> => {
        this.filterType = typeof filter === 'string' ? FilterType.String : FilterType.Object;
        
        return list.filter(x => {
            return this.itemHaveValue(x, filter);
        });
    }
    
    itemHaveValue = (item, filter): boolean => {
        if(filter == null) return false;
        
        let itemComplex = this.isComplexObject(item);
        let filterComplex = this.isComplexObject(filter);
        
        if (itemComplex) {
            //recorrer propiedades y buscar
            let encontrado: boolean = true;
            if (filterComplex) {
                for (let filterProp in filter) {
                    let value = item[filterProp];
                    //si no esta definido dere retornar false
                    if (!value) {
                        encontrado = false;
                        break;
                    }
                    else {
                        let res = this.itemHaveValue(value, filter[filterProp])
                        if (!res)
                        encontrado = false;
                    }
                }
            }
            else {
                for(let p in item){
                    let res = this.itemHaveValue(item[p], filter);
                    if(res)
                    return true;
                }
                
                return false;
            }
            return encontrado;
        }
        else {
            if (!filterComplex)
            return this.propertieValue(item, null, filter);
            else {
                console.log('El filtro no esta configurado correctamente');
                return true;
            }
        }
    }
    
    private isComplexObject = (value: any) => {
        if (typeof value === 'string')
        return false;
        else if (typeof value !== 'string' && !isNaN(Number(value)))
        return false;
        else if (Object.assign({}, value))
        return true;
        else
        return true;
    }
    
    /** Compara los valores */
    private propertieValue = (obj, propName, value: string, options?: FilterPipeOptions) => {
        if (propName != null) {
            if (options != undefined) {
                if (options.exact && !options.exact)
                return obj[propName].toString() == value;
            }
            
            let res = value == undefined || (propName != null && obj[propName].toString().indexOf(value) > -1);
            return res;
        }
        else{
            if(obj != null){
                if(typeof(obj) === 'boolean')
                {
                    return obj.toString().indexOf(value) > -1;
                }
                else{
                    let ind = obj.toString().toLowerCase().indexOf(value.toLowerCase()) > -1;
                    return ind;
                }
                
                
            }
                
            return false;
        }
        
    }
    
}

export class FilterPipeOptions {
    /** Indica si debe buscar por valores exactos */
    exact: boolean;
    caseSensitive: boolean
    
    constructor(usingContains?: boolean, caseSensitive?: boolean) {
        this.exact = usingContains;
        this.caseSensitive = caseSensitive;
    }
}

export enum FilterType {
    Object = 'byObject',
    String = 'byString'
}