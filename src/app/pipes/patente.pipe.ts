import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'patente'
})
export class PatentePipe implements PipeTransform {
    
    transform(value: string, args?: any): any {
        let haveValue = value && value.length > 0;
        let result = "";

        if(haveValue && value.length == 6)
            result = value.substring(0, 2) + ' • ' + value.substring(2, 4) + ' • ' + value.substring(4, 6);
        else if(haveValue)
            result = value;

        return result;

    }
    
}
