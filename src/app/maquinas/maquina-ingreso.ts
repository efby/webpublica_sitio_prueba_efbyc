import { Injectable } from '@angular/core';
import { ResponseTipo, RequestTipo } from '../models/respuestas-tipo';
import { Respuestas } from '../globales/respuestas';
import { IngresoService } from '../servicios/ingreso/ingreso.service';
import { Paginas } from '../globales/paginas';
import { VariablesGenerales, Maestros, Home, Ingreso } from '../globales/variables-generales';
import { AccionTipo, NavegacionTipo, AnimacionTipo } from '../globales/enumeradores';
import { Constantes } from '../globales/constantes';
import { UtilitariosService } from '../servicios/utilitarios.service';

@Injectable({
    providedIn: 'root'
})

export class MaquinaIngreso {

    constructor(private ingresoService: IngresoService, private variablesApi: VariablesGenerales, private constantes: Constantes) {
        if (localStorage.getItem("authorizationToken") !== "undefined" && localStorage.getItem("authorizationToken") !== null) {
            this.variablesApi.authorizationToken = localStorage.getItem("authorizationToken");
        } else {
            this.variablesApi.authorizationToken = "";
        }
        this.variablesApi.equipoSecret = localStorage.getItem("equipoSecret");
        this.variablesApi.ingreso.usuario.usuarioId = localStorage.getItem("usuarioId");
    }

    public async inicioMuevo(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();

        if (request.accion === AccionTipo.ACCION_INICIO) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            let obtenerStorage = await this.ingresoService.obtenerStorage();

            // let tieneDatosEnStorage = obtenerStorage.estado === Respuestas.RESPUESTA_OK;
            // console.log('tiene datos en storage: ', tieneDatosEnStorage);

            if (obtenerStorage.estado == Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorLecturaStorage;
                retorno.paginaRespuesta = Paginas.ErrorLecturaStorage;
            }
            // else if(obtenerStorage.estado == Respuestas.RESPUESTA_NOE){
            //     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            //     this.variablesApi.ingreso.paginaTipo == Paginas.ErrorLecturaStorage
            //     retorno.paginaRespuesta = Paginas.MuevoNuevoAdministrador;
            // }
            else if (obtenerStorage.estado === Respuestas.RESPUESTA_OK) {
                let secret = await this.ingresoService.creaSecretB("");
                if (secret.estado === Respuestas.RESPUESTA_NOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                    retorno.paginaRespuesta = Paginas.ErrorNOK;
                    this.variablesApi.ingreso.errorMessage = secret.data.message;
                } else if (secret.estado === Respuestas.RESPUESTA_SC) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                    this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                    retorno.modalSalida = Paginas.ModalSCComponent;
                } else if (secret.estado === Respuestas.RESPUESTA_SA) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                    retorno.paginaRespuesta = Paginas.ErrorSA;
                }
                else if (secret.estado = Respuestas.RESPUESTA_OK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    retorno.mensajeSalida = { tipoMensaje: Respuestas.toastrSuccess, tituloMensaje: "", textoMensaje: "" };
                    if (secret.data.codigoRespuesta === 2) {
                        retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
                    } else if (secret.data.codigoRespuesta === 3) {
                        // this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                        // retorno.paginaRespuesta = Paginas.HomeMuevo;

                        //OBTENERWEB - cargar informacion global aqui
                        let usuarioObtener = await this.ingresoService.ingresoUsuarioObtener();
                        if (usuarioObtener.estado === Respuestas.RESPUESTA_NOK) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                            this.variablesApi.ingreso.errorMessage = usuarioObtener.data.message;
                            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                            retorno.paginaRespuesta = Paginas.ErrorNOK;
                        } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SC) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                            this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                            retorno.modalSalida = Paginas.ModalSCComponent;
                        } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SA) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                            retorno.paginaRespuesta = Paginas.ErrorSA;
                        } else if (usuarioObtener.estado === Respuestas.RESPUESTA_OK) {

                            await this.cargaVariables(usuarioObtener);
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            if (this.variablesApi.home.fuerzaCambioCuenta) {
                                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: "Hemos cargado tu cuenta personal porque no tienes privilegios para ingresar a tu última cuenta." }
                            }

                            if (this.variablesApi.home.primerIngreso) {
                                this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                                retorno.paginaRespuesta = Paginas.HomeMuevo;
                            }
                            else if (this.variablesApi.home.usuarioLogIn.solicitaTerminos === 1) {
                                this.variablesApi.ingreso.paginaTipo = Paginas.IngresoTerminosCondiciones;
                                retorno.paginaRespuesta = Paginas.IngresoTerminosCondiciones;
                            }
                            else {
                                this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                                retorno.paginaRespuesta = Paginas.HomeMuevo;
                            }
                        }
                    } else if (secret.data.codigoRespuesta === 4) {
                        retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
                        retorno.data = {
                            action: 'token_revalidated'
                        };
                    }
                    this.variablesApi.authorizationToken = secret.data.authorizationToken;
                    this.saveInfoInStorage();
                }
            } else {
                let obtenerInfoDispositivo = await this.ingresoService.obtieneInfoDispositivo();
                if (obtenerInfoDispositivo.estado == Respuestas.RESPUESTA_NOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorLecturaDispositivo;
                    retorno.paginaRespuesta = Paginas.ErrorLecturaDispositivo;
                } else if (obtenerInfoDispositivo.estado == Respuestas.RESPUESTA_OK) {
                    let equipoSecretA = await UtilitariosService.obtenerSecretA("123456789");
                    let secret = await this.ingresoService.creaSecretB(equipoSecretA);

                    if (secret.estado === Respuestas.RESPUESTA_NOK) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                        retorno.paginaRespuesta = Paginas.ErrorNOK;
                        this.variablesApi.ingreso.errorMessage = secret.data.message;
                    } else if (secret.estado === Respuestas.RESPUESTA_SC) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                        this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                        retorno.modalSalida = Paginas.ModalSCComponent;
                    } else if (secret.estado === Respuestas.RESPUESTA_SA) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                        retorno.paginaRespuesta = Paginas.ErrorSA;
                        localStorage.clear();
                    } else if (secret.estado = Respuestas.RESPUESTA_OK) {


                        // if(!tieneDatosEnStorage){ //Respuestas.RESPUESTA_NOE){
                        //     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        //     this.variablesApi.ingreso.paginaTipo = Paginas.ErrorLecturaStorage;
                        //     retorno.paginaRespuesta = Paginas.MuevoNuevoAdministrador;
                        // }
                        // else{
                        //     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        //     retorno.mensajeSalida = { tipoMensaje: Respuestas.toastrSuccess, tituloMensaje: "", textoMensaje: "" };
                        //     retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
                        // }



                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        retorno.mensajeSalida = { tipoMensaje: Respuestas.toastrSuccess, tituloMensaje: "", textoMensaje: "" };
                        retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;

                        this.variablesApi.equipoSecret = equipoSecretA + secret.data.secret;
                        this.variablesApi.authorizationToken = secret.data.authorizationToken;

                        this.saveInfoInStorage();


                    }
                }
            }

        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.IngresoInicioPage
            retorno.paginaRespuesta = Paginas.IngresoInicioPage;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSC
            retorno.paginaRespuesta = Paginas.ErrorSC;

        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
        }
        return retorno;
    }

    private saveInfoInStorage() {
        localStorage.setItem("equipoSecret", (this.variablesApi.equipoSecret));
        localStorage.setItem("authorizationToken", this.variablesApi.authorizationToken);
        localStorage.setItem("tipoToken", this.constantes.TIPOTOKENEQUIPO);
    }

    public async ingresoTelefonoId(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_CLICK) {
            this.variablesApi.ingreso.usuario.telefonoId = request.datos.telefonoId
            let crearTelefono = await this.ingresoService.ingresoTelefonoCrear(request.datos.telefonoId);


            if (crearTelefono.estado == Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = crearTelefono.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.usuario.smsToken = crearTelefono.data.smsToken;
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.paginaRespuesta = Paginas.MuevoNuevoAdministrador
                this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCodigoSms = Paginas.MuevoNuevoAdministrador
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoTelefonoId;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;

        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaTelefonoId;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaTelefonoId;
        }
        return retorno;
    }

    public async ingresoTelefonoLlamada(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_CLICK) {
            this.variablesApi.ingreso.usuario.telefonoId = request.datos.telefonoId
            let crearTelefono = await this.ingresoService.ingresoTelefonoLlamada(request.datos.telefonoId);
            if (crearTelefono.estado == Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = crearTelefono.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.usuario.smsToken = crearTelefono.data.smsToken;
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.paginaRespuesta = Paginas.MuevoNuevoAdministrador
                this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCodigoSms = Paginas.MuevoNuevoAdministrador
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoTelefonoId;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;

        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaTelefonoId;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaTelefonoId;
        }
        return retorno;
    }

    public async ingresoCodigoSMS(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_BOTON_VALIDAR) {
            let codigoSMS = await this.ingresoService.ingresoCodigoSMS(request.datos.telefonoId, request.datos.smsToken, request.datos.smsValor);
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (codigoSMS.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = codigoSMS.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (codigoSMS.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (codigoSMS.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;

            } else if (codigoSMS.estado === Respuestas.RESPUESTA_OK) {
                // if (codigoSMS.data.codigoOk) {
                if (codigoSMS.data.authorizationtokenTelefono != "undefined") {
                    this.variablesApi.authorizationToken = codigoSMS.data.authorizationtokenTelefono;
                    this.variablesApi.ingreso.usuario.nombre = codigoSMS.data.usuario;
                }
                localStorage.setItem("authorizationToken", this.variablesApi.authorizationToken);
                localStorage.setItem("telefonoId", this.variablesApi.ingreso.usuario.telefonoId);
                // if (codigoSMS.data.usuarioRegistrado) {
                //     this.variablesApi.ingreso.usuario.nombre = codigoSMS.data.nombre;
                //     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                //     retorno.paginaRespuesta = Paginas.IngresoContrasena;
                //     this.variablesApi.ingreso.paginaTipo = Paginas.IngresoContrasena;
                //     this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro = Paginas.IngresoContrasena;
                // } else {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                if (codigoSMS.data.codigoRespuesta === 3) {
                    retorno.data = { validado: 0 };
                    retorno.paginaRespuesta = Paginas.MuevoNuevoAdministrador;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoAdministrador;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro = Paginas.IngresoSMSValor;
                } else if (codigoSMS.data.codigoRespuesta === 4) {
                    retorno.data = { validado: 1 };
                    retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoLogin;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro = Paginas.MuevoNuevoLogin;
                }
                // }
                // } else {
                //     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                //     this.variablesApi.ingreso.animacionTipo = AnimacionTipo.CODIGO_SMS_INCORRECTO;
                //     retorno.paginaRespuesta = Paginas.IngresoSMSValor;
                // }
            }
        } else if (request.accion === AccionTipo.ACCION_REENVIAR_CODIGO) {
            let reenviarCodigoSms = await this.ingresoService.reenviarCodigoSMS(request.datos.telefonoId, request.datos.smsToken);
            if (reenviarCodigoSms.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = reenviarCodigoSms.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (reenviarCodigoSms.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (reenviarCodigoSms.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;

            } else if (reenviarCodigoSms.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.paginaRespuesta = Paginas.IngresoSMSValor;
            }
        } else if (request.accion === AccionTipo.ACCION_RECIBIR_LLAMADA) {
            let crearTelefono = await this.ingresoService.ingresoTelefonoLlamada(request.datos.telefonoId);
            if (crearTelefono.estado == Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = crearTelefono.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            }
            else if (crearTelefono.estado == Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.usuario.smsToken = crearTelefono.data.smsToken;
                retorno.estado = Respuestas.RESPUESTA_OK;
                retorno.paginaRespuesta = Paginas.MuevoNuevoAdministrador
                this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCodigoSms = Paginas.MuevoNuevoAdministrador
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoSMSValor;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCodigoSms;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCodigoSms;
        }
        return retorno;
    }

    public async ingresoRutIdRegistro(request: RequestTipo): Promise<ResponseTipo> {
        this.variablesApi.ingreso.usuario.usuarioId = request.datos.usuarioId;
        this.variablesApi.ingreso.usuario.numeroDocumento = request.datos.numeroDocumento;
        let retorno = new ResponseTipo()
        if (request.accion === AccionTipo.ACCION_BOTON_CONSULTAR_NUMERO_SERIE) {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
            this.variablesApi.ingreso.modalTipo = Paginas.ModalNroDocumentoComponent;
            retorno.modalSalida = Paginas.ModalNroDocumentoComponent

        } else if (request.accion === AccionTipo.ACCION_BOTON_SIGUIENTE) {
            let rutIdRegistro = await this.ingresoService.ingresoRutIdRegistro(request.datos.telefonoId, request.datos.usuarioId, request.datos.numeroDocumento);
            retorno.estado = rutIdRegistro.estado;
            if (rutIdRegistro.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = rutIdRegistro.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (rutIdRegistro.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (rutIdRegistro.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (rutIdRegistro.estado === Respuestas.RESPUESTA_OK) {
                retorno.estado = Respuestas.RESPUESTA_OK;
                if (rutIdRegistro.data.codigoRespuesta == 1) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                    retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoRutId
                } else if (rutIdRegistro.data.codigoRespuesta == 2) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                    retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoRutId
                }
                else if (rutIdRegistro.data.codigoRespuesta == 3) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                    retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoRutId
                }
                else if (rutIdRegistro.data.codigoRespuesta == 4) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                    retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoRutId
                } else {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                    this.variablesApi.ingreso.errorMessage = rutIdRegistro.data.message;
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoRutId
            }
        } else if (request.accion === AccionTipo.ACCION_BOTON_NO_PUEDO_ACCEDER_A_MI_CUENTA) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoCorreoRecuperacion;
            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCorreoRecuperacion = Paginas.IngresoRutIdRecuperacion;
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoRutId;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro;
        }
        return retorno;
    }

    public async ingresoRutIdRecuperacion(request: RequestTipo): Promise<ResponseTipo> {
        this.variablesApi.ingreso.usuario.usuarioId = request.datos.usuarioId;
        this.variablesApi.ingreso.usuario.numeroDocumento = request.datos.numeroDocumento;
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_BOTON_SIGUIENTE) {
            let rutIdRecuperacion = await this.ingresoService.ingresoRutIdRecuperacion(request.datos.telefonoId, request.datos.usuarioId, request.datos.numeroDocumento);
            retorno.estado = rutIdRecuperacion.estado;
            if (rutIdRecuperacion.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = rutIdRecuperacion.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (rutIdRecuperacion.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (rutIdRecuperacion.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (rutIdRecuperacion.estado === Respuestas.RESPUESTA_OK) {
                retorno.estado = Respuestas.RESPUESTA_OK;
                if (!rutIdRecuperacion.data.numeroDocumentoOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.NUMERO_DOCUMENTO_INCORRECTO;
                    retorno.paginaRespuesta = Paginas.IngresoRutId;

                } else if (rutIdRecuperacion.data.numeroDocumentoOK && !rutIdRecuperacion.data.usuarioRegistrado) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.IngresoNombre;
                    retorno.paginaRespuesta = Paginas.IngresoNombre;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaNombre = Paginas.IngresoRutIdRecuperacion;

                } else if (rutIdRecuperacion.data.numeroDocumentoOK && rutIdRecuperacion.data.usuarioRegistrado && !rutIdRecuperacion.data.usuarioTelefonoAsociado) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.USUARIO_TELEFONO_NO_ASOCIADO
                    retorno.paginaRespuesta = Paginas.IngresoRutId;

                } else if (rutIdRecuperacion.data.numeroDocumentoOK && rutIdRecuperacion.data.usuarioRegistrado && rutIdRecuperacion.data.usuarioTelefonoAsociado) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.IngresoModificaContrasena;
                    retorno.paginaRespuesta = Paginas.IngresoModificaContrasena;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorModificaContrasena = Paginas.IngresoRutIdRecuperacion

                } else {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                    this.variablesApi.ingreso.errorMessage = rutIdRecuperacion.data.message;
                }
            } else {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = rutIdRecuperacion.data.message;
            }

        } else if (request.accion === AccionTipo.ACCION_BOTON_NO_PUEDO_ACCEDER_A_MI_CUENTA) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoCorreoRecuperacion;
            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCorreoRecuperacion = Paginas.IngresoRutIdRecuperacion;
        } else if (request.accion === AccionTipo.ACCION_BOTON_MODIFICAR_TELEFONO) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoInicioPage;
            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio = Paginas.IngresoRutIdRecuperacion;
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoRutIdRecuperacion;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRecuperacion;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRecuperacion;
        }
        return retorno
    }

    public async ingresoNombre(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo()
        if (request.accion === AccionTipo.ACCION_BOTON_SIGUIENTE) {
            retorno.estado = Respuestas.RESPUESTA_OK;

            this.variablesApi.ingreso.usuario.nombre = request.datos.nombre;
            this.variablesApi.ingreso.usuario.apellido = request.datos.apellido;

            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.IngresoCreaContrasena;
            retorno.paginaRespuesta = Paginas.IngresoCreaContrasena;
            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoNombre

        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoNombre;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaNombre;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaNombre;
        }
        return retorno;
    }

    public async ingresoMail(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo()
        if (request.accion === AccionTipo.ACCION_BOTON_SIGUIENTE) {
            this.variablesApi.ingreso.usuario.nombre = request.datos.nombre;
            this.variablesApi.ingreso.usuario.apellido = request.datos.apellido;
            this.variablesApi.ingreso.usuario.usuarioId = request.datos.usuarioId;
            this.variablesApi.ingreso.usuario.numeroDocumento = request.datos.numeroDocumento;
            this.variablesApi.ingreso.usuario.mail = request.datos.mail;

            let crearMail = await this.ingresoService.ingresoMail(request.datos.mail);
            if (crearMail.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = crearMail.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (crearMail.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (crearMail.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (crearMail.estado === Respuestas.RESPUESTA_OK) {
                if (crearMail.data.codigoRespuesta === 3) {
                    retorno.data = { validado: 1 };
                    this.variablesApi.authorizationToken = crearMail.data.authorizationtokenTelefono;
                    retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: "El mail ingresado ya está asociado a una cuenta. Intenta con otro mail." };
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro = Paginas.MuevoNuevoMail;
                } else {
                    this.variablesApi.authorizationToken = crearMail.data.authorizationtokenTelefono;
                    let rutIdRegistro = await this.ingresoService.ingresoRutIdRegistro(request.datos.telefonoId, request.datos.usuarioId, request.datos.numeroDocumento);
                    if (rutIdRegistro.estado === Respuestas.RESPUESTA_NOK) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                        this.variablesApi.ingreso.errorMessage = rutIdRegistro.data.message;
                        retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                        retorno.paginaRespuesta = Paginas.ErrorNOK;
                    } else if (rutIdRegistro.estado === Respuestas.RESPUESTA_SA) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                        retorno.paginaRespuesta = Paginas.ErrorSA;
                    } else if (rutIdRegistro.estado === Respuestas.RESPUESTA_SC) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                        this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                        retorno.modalSalida = Paginas.ModalSCComponent;
                    } else if (rutIdRegistro.estado === Respuestas.RESPUESTA_OK) {
                        retorno.estado = Respuestas.RESPUESTA_OK;
                        if (rutIdRegistro.data.codigoRespuesta == 1) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                            retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                            this.variablesApi.ingreso.errorMessage = rutIdRegistro.data.message;
                            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoRutId
                        } else if (rutIdRegistro.data.codigoRespuesta == 2) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                            retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                            this.variablesApi.ingreso.errorMessage = rutIdRegistro.data.message;
                            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoRutId
                        }
                        else if (rutIdRegistro.data.codigoRespuesta == 3) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                            retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                            this.variablesApi.ingreso.errorMessage = rutIdRegistro.data.message;
                            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoRutId
                        }
                        else if (rutIdRegistro.data.codigoRespuesta == 4) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoMail;
                            retorno.paginaRespuesta = Paginas.MuevoNuevoMail;
                            this.variablesApi.ingreso.errorMessage = rutIdRegistro.data.message;
                            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena = Paginas.IngresoRutId
                        } else {
                            this.variablesApi.authorizationToken = rutIdRegistro.data.authorizationtokenUsuario
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoContrasena;
                            retorno.paginaRespuesta = Paginas.MuevoNuevoContrasena;
                            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaNombre = Paginas.MuevoNuevoAdministrador;
                        }
                    } else {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                        this.variablesApi.ingreso.errorMessage = rutIdRegistro.data.message;
                    }
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoCreaContrasena
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoEmail;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCorreo;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCorreo;
        }
        return retorno;
    }

    public async ingresoCorreoRecuperacion(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo()
        if (request.accion === AccionTipo.ACCION_BOTON_SIGUIENTE) {
            this.variablesApi.ingreso.usuario.mail = request.datos.mail;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.estado = Respuestas.RESPUESTA_OK;
            retorno.paginaRespuesta = Paginas.IngresoInicioPage;

        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoEmail;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCorreo;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaCorreo;
        }
        return retorno;
    }

    public async ingresoCreaContrasena(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo()
        if (request.accion === AccionTipo.ACCION_BOTON_CREAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            let crearContrasena = await this.ingresoService.ingresoCreaContrasena(request.datos.telefonoId, request.datos.usuarioId, request.datos.nombre, request.datos.apellido, request.datos.mail, "" + request.datos.password, request.datos.numeroDocumento);
            if (crearContrasena.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = crearContrasena.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (crearContrasena.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (crearContrasena.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (crearContrasena.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.authorizationToken = crearContrasena.data.authorizationtokenUsuario;
                localStorage.setItem("authorizationToken", this.variablesApi.authorizationToken);
                localStorage.setItem("tipoToken", this.constantes.TIPOTOKENUSUARIO);
                localStorage.setItem("usuarioId", this.variablesApi.ingreso.usuario.usuarioId);

                let usuarioObtener = await this.ingresoService.ingresoUsuarioObtener(true, true);
                if (usuarioObtener.estado === Respuestas.RESPUESTA_NOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                    this.variablesApi.ingreso.errorMessage = usuarioObtener.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.paginaRespuesta = Paginas.ErrorNOK;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SC) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                    this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                    retorno.modalSalida = Paginas.ModalSCComponent;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SA) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                    retorno.paginaRespuesta = Paginas.ErrorSA;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_OK) {
                    await this.cargaVariables(usuarioObtener);

                    this.variablesApi.home.cuentas = usuarioObtener.data.cuentas;
                    this.variablesApi.home.invitaciones = usuarioObtener.data.invitaciones;
                    this.variablesApi.home.usuarioLogIn = usuarioObtener.data.usuarioLogin;
                    this.variablesApi.home.vehiculos = usuarioObtener.data.vehiculos;
                    this.variablesApi.home.conductores = usuarioObtener.data.conductores;
                    this.variablesApi.home.usuarios = usuarioObtener.data.usuarios;
                    this.variablesApi.home.facturasPendientesPago = usuarioObtener.data.facturasPendiente;
                    this.variablesApi.home.primerIngreso = usuarioObtener.data.primerIngreso;
                    this.variablesApi.home.montoTotalMes = usuarioObtener.data.montoTotalMes;
                    this.variablesApi.home.tipoFormaPago = usuarioObtener.data.tiposFormaPago;
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    if (this.variablesApi.home.primerIngreso) {
                        this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                        retorno.paginaRespuesta = Paginas.HomeMuevo;
                    } else {
                        this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                        retorno.paginaRespuesta = Paginas.HomeMuevo;
                    }
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoCreaContrasena
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoCreaContrasena;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena;
        }
        return retorno;
    }

    public async ingresoModificaContrasena(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo()
        if (request.accion === AccionTipo.ACCION_BOTON_CREAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            let modificarContrasena = await this.ingresoService.ingresoModificaContrasena(request.datos.telefonoId, request.datos.usuarioId, request.datos.password, request.datos.numeroDocumento);
            if (modificarContrasena.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = modificarContrasena.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (modificarContrasena.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (modificarContrasena.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (modificarContrasena.estado === Respuestas.RESPUESTA_OK) {
                let validarContrasena = await this.ingresoService.ingresoUsuarioValidar(request.datos.telefonoId, request.datos.usuarioId, request.datos.password);
                if (validarContrasena.estado === Respuestas.RESPUESTA_NOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                    this.variablesApi.ingreso.errorMessage = validarContrasena.data.message;
                    retorno.paginaRespuesta = Paginas.ErrorNOK;
                } else if (validarContrasena.estado === Respuestas.RESPUESTA_SC) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                    this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                    retorno.modalSalida = Paginas.ModalSCComponent;
                } else if (validarContrasena.estado === Respuestas.RESPUESTA_SA) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                    retorno.paginaRespuesta = Paginas.ErrorSA;
                } else if (validarContrasena.estado === Respuestas.RESPUESTA_OK) {
                    this.variablesApi.authorizationToken = validarContrasena.data.authorizationtokenUsuario;
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.IngresoPinValor;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaPin = "";
                    retorno.paginaRespuesta = Paginas.IngresoPinValor;
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoCreaContrasena
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoCreaContrasena;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorCreaContrasena;
        }
        return retorno;
    }

    public async ingresoContrasena(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_BOTON_OLVIDE_MI_CONTRASENA) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoRutIdRecuperacion;
            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRecuperacion = Paginas.IngresoContrasena;

        } else if (request.accion === AccionTipo.ACCION_BOTON_CREAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoRutId;
            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro = Paginas.IngresoContrasena;

        } else if (request.accion === AccionTipo.ACCION_BOTON_VALIDAR) {
            let validarContrasena = await this.ingresoService.ingresoUsuarioValidarLogin(request.datos.telefonoId, request.datos.password);
            if (validarContrasena.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = validarContrasena.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (validarContrasena.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (validarContrasena.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (validarContrasena.estado === Respuestas.RESPUESTA_OK) {
                localStorage.setItem("authorizationToken", validarContrasena.data.authorizationtokenUsuario);
                localStorage.setItem("tipoToken", this.constantes.TIPOTOKENUSUARIO);
                localStorage.setItem("usuarioId", this.variablesApi.ingreso.usuario.usuarioId);
                this.variablesApi.authorizationToken = validarContrasena.data.authorizationtokenUsuario;
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.IngresoPinValor;
                this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaPin = Paginas.IngresoContrasena;
                retorno.paginaRespuesta = Paginas.IngresoPinValor;
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoRutId;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaContrasena;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaContrasena;
        }
        return retorno;
    }

    public async ingresoPin(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo()
        if (request.accion === AccionTipo.ACCION_BOTON_SIGUIENTE) {
            this.variablesApi.ingreso.usuario.pin1 = request.datos.pinValor;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;

            retorno.estado = Respuestas.RESPUESTA_OK;
            retorno.paginaRespuesta = Paginas.IngresoConfirmaPin;
            this.variablesApi.ingreso.paginaAnterior.paginaAnteriorConfirmaPin = Paginas.IngresoPinValor

        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoPinValor;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        }
        return retorno;
    }

    public async ingresoConfimaPin(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo()
        this.variablesApi.ingreso.usuario.pin2 = request.datos.pinValor;
        if (request.accion === AccionTipo.ACCION_BOTON_CREAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (this.variablesApi.ingreso.usuario.pin1 === this.variablesApi.ingreso.usuario.pin2) {
                let pinCrear = await this.ingresoService.ingresoCrearPin(request.datos.telefonoId, request.datos.usuarioId, request.datos.pinValor);
                if (pinCrear.estado === Respuestas.RESPUESTA_NOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                    this.variablesApi.ingreso.errorMessage = pinCrear.data.message;
                    retorno.paginaRespuesta = Paginas.ErrorNOK;
                } else if (pinCrear.estado === Respuestas.RESPUESTA_SC) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                    this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                    retorno.modalSalida = Paginas.ModalSCComponent;
                } else if (pinCrear.estado === Respuestas.RESPUESTA_SA) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                    retorno.paginaRespuesta = Paginas.ErrorSA;
                } else if (pinCrear.estado === Respuestas.RESPUESTA_OK) {
                    let pinValidar = await this.ingresoService.ingresoValidarPin(request.datos.telefonoId, request.datos.usuarioId, request.datos.pinValor);
                    if (pinValidar.estado === Respuestas.RESPUESTA_NOK) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                        this.variablesApi.ingreso.errorMessage = pinValidar.data.message;
                        retorno.paginaRespuesta = Paginas.ErrorNOK;
                    } else if (pinValidar.estado === Respuestas.RESPUESTA_SC) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                        this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                        retorno.modalSalida = Paginas.ModalSCComponent;
                    } else if (pinValidar.estado === Respuestas.RESPUESTA_SA) {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                        retorno.paginaRespuesta = Paginas.ErrorSA;
                    } else if (pinValidar.estado === Respuestas.RESPUESTA_OK) {

                        localStorage.setItem("authorizationToken", this.variablesApi.authorizationToken);
                        localStorage.setItem("tipoToken", this.constantes.TIPOTOKENUSUARIO);
                        localStorage.setItem("usuarioId", this.variablesApi.ingreso.usuario.usuarioId);
                        this.variablesApi.authorizationToken = pinValidar.data.authorizationTokenPin;

                        let usuarioObtener = await this.ingresoService.ingresoUsuarioObtener();
                        if (usuarioObtener.estado === Respuestas.RESPUESTA_NOK) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                            this.variablesApi.ingreso.errorMessage = usuarioObtener.data.message;
                            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                            retorno.paginaRespuesta = Paginas.ErrorNOK;
                        } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SC) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                            this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                            retorno.modalSalida = Paginas.ModalSCComponent;
                        } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SA) {
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                            retorno.paginaRespuesta = Paginas.ErrorSA;
                        } else if (usuarioObtener.estado === Respuestas.RESPUESTA_OK) {
                            this.variablesApi.home.cuentas = usuarioObtener.data.cuentas;
                            this.variablesApi.home.invitaciones = usuarioObtener.data.invitaciones;
                            this.variablesApi.home.usuarioLogIn = usuarioObtener.data.usuarioLogin;
                            this.variablesApi.home.vehiculos = usuarioObtener.data.vehiculos;
                            this.variablesApi.home.conductores = usuarioObtener.data.conductores;
                            this.variablesApi.home.usuarios = usuarioObtener.data.usuarios;
                            this.variablesApi.home.facturasPendientesPago = usuarioObtener.data.facturasPendiente;
                            this.variablesApi.home.primerIngreso = usuarioObtener.data.primerIngreso;
                            this.variablesApi.home.montoTotalMes = usuarioObtener.data.montoTotalMes;
                            this.variablesApi.home.tipoFormaPago = usuarioObtener.data.tiposFormaPago;
                            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                            if (this.variablesApi.home.primerIngreso) {
                                this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                                retorno.paginaRespuesta = Paginas.HomeMuevo;
                            } else {
                                this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                                retorno.paginaRespuesta = Paginas.HomeMuevo;
                            }
                        }
                    }
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoPinValor
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoConfirmaPin;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorConfirmaPin;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorConfirmaPin;
        }
        return retorno;
    }

    public async ingresoValidaPin(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_BOTON_VALIDAR) {
            let pinValidar = await this.ingresoService.ingresoValidarPin(request.datos.telefonoId, request.datos.usuarioId, request.datos.pinValor);
            if (pinValidar.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = pinValidar.data.message;
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (pinValidar.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (pinValidar.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (pinValidar.estado === Respuestas.RESPUESTA_OK) {

                localStorage.setItem("authorizationToken", this.variablesApi.authorizationToken);
                localStorage.setItem("tipoToken", this.constantes.TIPOTOKENUSUARIO);
                localStorage.setItem("usuarioId", this.variablesApi.ingreso.usuario.usuarioId);
                this.variablesApi.authorizationToken = pinValidar.data.authorizationTokenPin;

                let usuarioObtener = await this.ingresoService.ingresoUsuarioObtener();
                if (usuarioObtener.estado === Respuestas.RESPUESTA_NOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                    this.variablesApi.ingreso.errorMessage = usuarioObtener.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.paginaRespuesta = Paginas.ErrorNOK;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SC) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                    this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                    retorno.modalSalida = Paginas.ModalSCComponent;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SA) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                    retorno.paginaRespuesta = Paginas.ErrorSA;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_OK) {
                    this.variablesApi.home.cuentas = usuarioObtener.data.cuentas;
                    this.variablesApi.home.invitaciones = usuarioObtener.data.invitaciones;
                    this.variablesApi.home.usuarioLogIn = usuarioObtener.data.usuarioLogin;
                    this.variablesApi.home.vehiculos = usuarioObtener.data.vehiculos;
                    this.variablesApi.home.conductores = usuarioObtener.data.conductores;
                    this.variablesApi.home.usuarios = usuarioObtener.data.usuarios;
                    this.variablesApi.home.facturasPendientesPago = usuarioObtener.data.facturasPendiente;
                    this.variablesApi.home.primerIngreso = usuarioObtener.data.primerIngreso;
                    this.variablesApi.home.montoTotalMes = usuarioObtener.data.montoTotalMes;
                    this.variablesApi.home.tipoFormaPago = usuarioObtener.data.tiposFormaPago;
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    if (this.variablesApi.home.primerIngreso) {
                        this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                        retorno.paginaRespuesta = Paginas.HomeMuevo;
                    } else {
                        this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                        retorno.paginaRespuesta = Paginas.HomeMuevo;
                    }
                }
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.IngresoValidaPin;

        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorValidaPin;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorValidaPin;
        }
        return retorno;
    }

    public async IngresoTokenPost(request: RequestTipo, aceptarTerminos: boolean = false): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_CLICK) {
            let tokenPost = await this.ingresoService.IngresoTokenPost("" + request.datos.password);
            if (tokenPost.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = tokenPost.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (tokenPost.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (tokenPost.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (tokenPost.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.authorizationToken = tokenPost.data.authorizationtokenTelefono;
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                localStorage.setItem("authorizationToken", this.variablesApi.authorizationToken);
                localStorage.setItem("tipoToken", this.constantes.TIPOTOKENUSUARIO);
                localStorage.setItem("usuarioId", this.variablesApi.ingreso.usuario.usuarioId);

                let usuarioObtener = await this.ingresoService.ingresoUsuarioObtener(false, false);
                if (usuarioObtener.estado === Respuestas.RESPUESTA_NOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                    this.variablesApi.ingreso.errorMessage = usuarioObtener.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.paginaRespuesta = Paginas.ErrorNOK;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SC) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                    this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                    retorno.modalSalida = Paginas.ModalSCComponent;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SA) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                    retorno.paginaRespuesta = Paginas.ErrorSA;
                } else if (usuarioObtener.estado === Respuestas.RESPUESTA_OK) {

                    await this.cargaVariables(usuarioObtener);
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    if (this.variablesApi.home.fuerzaCambioCuenta) {
                        retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: "Hemos cargado tu cuenta personal porque no tienes privilegios para ingresar a tu última cuenta." }
                    }
                    if (this.variablesApi.home.primerIngreso) {
                        this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                        retorno.paginaRespuesta = Paginas.HomeMuevo;
                    } else {

                        if (usuarioObtener.data.usuarioLogin.solicitaTerminos) {
                            this.variablesApi.ingreso.paginaTipo = Paginas.IngresoTerminosCondiciones;
                            retorno.paginaRespuesta = Paginas.IngresoTerminosCondiciones;
                        }
                        else {
                            this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                            retorno.paginaRespuesta = Paginas.HomeMuevo;
                        }
                    }
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoCreaContrasena
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
        }

        return retorno
    }

    public async IngresoTokenMailPost(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_CLICK) {
            let tokenMailPost = await this.ingresoService.IngresoTokenMailPost();
            if (tokenMailPost.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = tokenMailPost.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (tokenMailPost.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (tokenMailPost.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (tokenMailPost.estado === Respuestas.RESPUESTA_OK) {
                retorno.estado = Respuestas.RESPUESTA_OK;
                // this.variablesApi.authorizationToken = tokenMailPost.data.authorizationtokenTelefono;
                this.variablesApi.ingreso.usuario.nombre = tokenMailPost.data.usuario;
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                if (tokenMailPost.data.codigoRespuesta === 2) {
                    retorno.paginaRespuesta = Paginas.RecuperaPinMuevo;
                    this.variablesApi.ingreso.usuario.mail = tokenMailPost.data.mail;
                    this.variablesApi.ingreso.paginaTipo = Paginas.RecuperaPinMuevo;
                } else {
                    retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoLogin;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro = Paginas.MuevoNuevoLogin;
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoCreaContrasena
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
        }

        return retorno
    }

    public async IngresoTokenTelefonoPost(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_CLICK) {
            let tokenTelefonoPost = await this.ingresoService.IngresoTokenTelefonoPost(request.datos.rut, request.datos.numeroSerieRut);
            if (tokenTelefonoPost.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = tokenTelefonoPost.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (tokenTelefonoPost.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (tokenTelefonoPost.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (tokenTelefonoPost.estado === Respuestas.RESPUESTA_OK) {
                retorno.estado = Respuestas.RESPUESTA_OK;
                // this.variablesApi.authorizationToken = tokenMailPost.data.authorizationtokenTelefono;
                this.variablesApi.ingreso.usuario.nombre = tokenTelefonoPost.data.usuario;
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                if (tokenTelefonoPost.data.codigoRespuesta === 6) {
                    retorno.paginaRespuesta = Paginas.RecuperaPinMuevo;
                    this.variablesApi.ingreso.usuario.telefonoCorto = tokenTelefonoPost.data.telefono;
                    this.variablesApi.ingreso.paginaTipo = Paginas.RecuperaPinMuevo;
                } else {
                    retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
                    this.variablesApi.ingreso.paginaTipo = Paginas.MuevoNuevoLogin;
                    this.variablesApi.ingreso.paginaAnterior.paginaAnteriorIngresaRutIdRegistro = Paginas.MuevoNuevoLogin;
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoCreaContrasena
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
        }

        return retorno
    }

    public async IngresoTokenPassPost(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion === AccionTipo.ACCION_CLICK) {
            let tokenMailPost = await this.ingresoService.IngresoTokenPassPost(request.datos.codigo, "" + request.datos.password);
            if (tokenMailPost.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = tokenMailPost.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (tokenMailPost.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (tokenMailPost.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (tokenMailPost.estado === Respuestas.RESPUESTA_OK) {
                retorno.estado = Respuestas.RESPUESTA_OK;
                if (tokenMailPost.data.codigoRespuesta === 4) {
                    retorno = await this.IngresoTokenPost(request);
                }
            } else {
                retorno.estado = Respuestas.RESPUESTA_NOK;
                retorno.paginaRespuesta = Paginas.IngresoCreaContrasena
            }
        } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.MuevoNuevoLogin;
        } else if (request.accion === AccionTipo.ACCION_SALIR) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            retorno.paginaRespuesta = Paginas.ErrorSC;
        } else if (request.accion === AccionTipo.ACCION_ATRAS) {
            retorno.estado = Respuestas.RESPUESTA_OK;
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
            retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorInicio;
        }

        return retorno
    }

    public CloseSession() {
        localStorage.clear();
        this.variablesApi.authorizationToken = null;
        this.variablesApi.equipoSecret = null;
        this.variablesApi.home = new Home();
        this.variablesApi.ingreso = new Ingreso()
    }

    //utilitario Reintentar

    public async reintentarIngreso(request: RequestTipo, origen): Promise<ResponseTipo> {
        switch (origen) {
            case Paginas.IngresoTelefonoId:
                return await this.ingresoTelefonoId(request);
                break;
            case Paginas.IngresoSMSValor:
                return await this.ingresoCodigoSMS(request);
                break;
            case Paginas.IngresoNombre:
                return await this.ingresoNombre(request);
                break;
            default:
                break;
        }
    }


    private async cargaVariables(usuarioObtener: ResponseTipo) {
        this.variablesApi.home.cuentaSeleccionada = usuarioObtener.data.cuentaSeleccionada;
        this.variablesApi.home.cuentas = usuarioObtener.data.cuentas;
        let getMaestros = await this.ingresoService.ingresoObtenerMaestros(this.variablesApi.home.cuentaSeleccionada.cuentaId);
        this.variablesApi.home.maestros = getMaestros.data.maestros;
        this.variablesApi.home.tipoFormaPago = usuarioObtener.data.tiposFormaPago;
        this.variablesApi.home.saldo = usuarioObtener.data.saldo;
        this.variablesApi.home.menu = usuarioObtener.data.menu;
        this.variablesApi.home.usuarioLogIn = usuarioObtener.data.usuarioLogin
        this.variablesApi.home.rolPrivilegiosFront = usuarioObtener.data.rolPrivilegiosFront


        // this.variablesApi.home.invitaciones = usuarioObtener.data.invitaciones;
        // this.variablesApi.home.usuarioLogIn = usuarioObtener.data.usuarioLogin;
        // this.variablesApi.home.vehiculos = usuarioObtener.data.vehiculos;
        // this.variablesApi.home.flotas = usuarioObtener.data.flotas;
        // this.variablesApi.home.conductores = usuarioObtener.data.conductores;
        // this.variablesApi.home.usuarios = usuarioObtener.data.usuarios;
        // this.variablesApi.home.grupos = usuarioObtener.data.grupos;
        // this.variablesApi.home.facturasPendientesPago = usuarioObtener.data.facturasPendiente;
        // this.variablesApi.home.primerIngreso = usuarioObtener.data.primerIngreso;
        // this.variablesApi.home.montoTotalMes = usuarioObtener.data.montoTotalMes;
        // this.variablesApi.home.tipoFormaPago = usuarioObtener.data.tiposFormaPago;
        // this.variablesApi.home.menu = usuarioObtener.data.menu;

        // this.variablesApi.home.mediosPago = usuarioObtener.data.mediosPago;
        // this.variablesApi.home.datosSaldo = usuarioObtener.data.datosSaldo;
        // this.variablesApi.home.tipoDocumento = usuarioObtener.data.tipoDocumento;
        // this.variablesApi.home.cuentaDatosDte = usuarioObtener.data.cuentaDatosDte;
        // this.variablesApi.home.notificaciones = usuarioObtener.data.notificaciones;
        // this.variablesApi.home.fuerzaCambioCuenta = usuarioObtener.data.fuerzaCambioCuenta;


        // if (!this.variablesApi.home.fuerzaCambioCuenta) {
        //let idComponent = this.variablesApi.home.cuentas.findIndex(r => r.cuentaDefault === true);



        // this.variablesApi.home.conductores.forEach(element => {
        //     element.rolNombre = this.variablesApi.home.maestros.roles.filter(r => r.rolId == element.rolId)[0].rolNombre;
        // });
    }

    //Plantilla Funcion Maquina
    // public async funcion(request: RequestTipo): Promise<ResponseTipo> {
    //     let retorno = new ResponseTipo();
    //     if (request.accion === AccionTipo.ACCION_CLICK) {
    //         //TODO:DO SOMETHING
    //     } else if (request.accion === AccionTipo.ACCION_REINTENTAR) {
    //         retorno.estado = Respuestas.RESPUESTA_OK;
    //         this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //         retorno.paginaRespuesta = Paginas.EllaMisma;

    //     } else if (request.accion === AccionTipo.ACCION_SALIR) {
    //         retorno.estado = Respuestas.RESPUESTA_OK;
    //         this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //         retorno.paginaRespuesta = Paginas.ErrorSC;
    //     } else if (request.accion === AccionTipo.ACCION_ATRAS) {
    //         retorno.estado = Respuestas.RESPUESTA_OK;
    //         this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //         this.variablesApi.ingreso.paginaTipo = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorEllaMisma;
    //         retorno.paginaRespuesta = this.variablesApi.ingreso.paginaAnterior.paginaAnteriorEllaMisma;
    //     }

    //     return retorno
    // }

} //Fin Clase
