import { Injectable } from '@angular/core';
import { ResponseTipo, RequestTipo } from '../models/respuestas-tipo';
import { Respuestas } from '../globales/respuestas';
import { HomeService } from "../servicios/home/home.service";
import { Paginas } from '../globales/paginas';
import { VariablesGenerales } from '../globales/variables-generales';
import { AccionTipo, NavegacionTipo, AnimacionTipo } from '../globales/enumeradores';
import { UtilitariosService } from '../servicios/utilitarios.service';
import { Conductor } from '../models/conductores';

@Injectable({
    providedIn: 'root'
})

export class MaquinaHome {
    constructor(private homeService: HomeService, private variablesApi: VariablesGenerales) { }
    
    public async homeInicio(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_INICIO) {
            let usuarioObtener = await this.homeService.homeUsuarioObtener();
            if (usuarioObtener.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = usuarioObtener.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (usuarioObtener.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
                this.variablesApi.ingreso.errorMessage = usuarioObtener.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: "No Tiene Autorización para esta Accion" };
            } else if (usuarioObtener.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.home.cuentaSeleccionada = usuarioObtener.data.cuentaSeleccionada;
                this.variablesApi.home.saldo = usuarioObtener.data.saldo;
                //console.log('cuenta seleccionada: ', this.variablesApi.home.cuentaSeleccionada.cuentaNombre)
                this.variablesApi.home.invitaciones = usuarioObtener.data.invitaciones;
                this.variablesApi.home.usuarioLogIn = usuarioObtener.data.usuarioLogin;
                this.variablesApi.home.vehiculos = usuarioObtener.data.vehiculos;
                this.variablesApi.home.conductores = usuarioObtener.data.conductores;
                this.variablesApi.home.usuarios = usuarioObtener.data.usuarios;
                this.variablesApi.home.facturasPendientesPago = usuarioObtener.data.facturasPendiente;
                this.variablesApi.home.primerIngreso = usuarioObtener.data.primerIngreso;
                this.variablesApi.home.montoTotalMes = usuarioObtener.data.montoTotalMes;
                this.variablesApi.home.tipoFormaPago = usuarioObtener.data.tiposFormaPago;
                this.variablesApi.home.menu = usuarioObtener.data.menu;
                this.variablesApi.home.cuentas = usuarioObtener.data.cuentas;
                this.variablesApi.home.mediosPago = usuarioObtener.data.mediosPago;
                this.variablesApi.home.datosSaldo = usuarioObtener.data.datosSaldo;
                this.variablesApi.home.tipoDocumento = usuarioObtener.data.tipoDocumento;
                this.variablesApi.home.cuentaDatosDte = usuarioObtener.data.cuentaDatosDte;
                this.variablesApi.home.notificaciones = usuarioObtener.data.notificaciones;
                this.variablesApi.home.fuerzaCambioCuenta = usuarioObtener.data.fuerzaCambioCuenta;
                this.variablesApi.home.rolPrivilegiosFront= usuarioObtener.data.rolPrivilegiosFront;

                console.log("RolPrivilegiosFront")
                console.log(this.variablesApi.home.rolPrivilegiosFront)
                
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                if (this.variablesApi.home.fuerzaCambioCuenta) {
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: "Hemos cargado tu cuenta personal porque no tienes privilegios para ingresar a tu última cuenta." }
                }
                let getMaestros = await this.homeService.homeObtenerMaestros(this.variablesApi.home.cuentaSeleccionada.cuentaId);
                this.variablesApi.home.maestros = getMaestros.data.maestros;

                if(this.variablesApi.home.conductores && this.variablesApi.home.conductores.length > 0)
                {
                    this.variablesApi.home.conductores.forEach(element => {
                        element.rolNombre = this.variablesApi.home.maestros.roles.filter(r => r.rolId == element.rolId)[0].rolNombre;
                    });
                }
                
                if (this.variablesApi.home.primerIngreso) {
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                    retorno.paginaRespuesta = Paginas.HomeMuevo;
                } else {
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                    retorno.paginaRespuesta = Paginas.HomeMuevo;
                }
            }
        }
        
        return retorno;
    }
    
    public async homeCrearCuentaCorriente(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let crearCuentaCorriente = await this.homeService.homeCrearCuentaCorriente(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.cuentaCorrienteNumero), request.datos.cuentaCorrienteRut, request.datos.cuentaCorrienteNombreTitular, request.datos.cuentaCorrienteEmailTitular, request.datos.cuentaCorrienteNombre, this.convertNumber(request.datos.cuentaCorrienteTipo), this.convertNumber(request.datos.bancoId));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (crearCuentaCorriente.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = crearCuentaCorriente.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (crearCuentaCorriente.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (crearCuentaCorriente.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (crearCuentaCorriente.estado === Respuestas.RESPUESTA_OK) {
                if (typeof crearCuentaCorriente.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = crearCuentaCorriente.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = crearCuentaCorriente.data.message;
                } else { }
            }
            
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeCrearCuentaEmpresa(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let crearCuentaEmpresa = await this.homeService.homeCrearCuentaEmpresa(request.datos.cuentaNombre);
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = crearCuentaEmpresa.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_OK) {
                if (typeof crearCuentaEmpresa.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = crearCuentaEmpresa.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = crearCuentaEmpresa.data.message;
                } else {
                    retorno.data = crearCuentaEmpresa.data
                }
                
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeModificarCuentaEmpresa(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let crearCuentaEmpresa = await this.homeService.homeModificarCuentaEmpresa(request.datos.cuentaId, request.datos.cuentaNombre);
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = crearCuentaEmpresa.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_OK) {
                if (typeof crearCuentaEmpresa.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = crearCuentaEmpresa.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = crearCuentaEmpresa.data.message;
                } else { }
                
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    // public async homeCrearDatosFacturacion(request: RequestTipo): Promise<ResponseTipo> {
    //     let retorno = new ResponseTipo();
    //     if (request.accion == AccionTipo.ACCION_CLICK) {
    //         let crearCuentaEmpresa = await this.homeService.homeCrearDatosFacturacion(this.convertNumber(request.datos.cuentaId), request.datos.datosFacturacionRut, request.datos.datosFacturacionEmail, request.datos.datosFacturacionGiro, request.datos.datosFacturacionDistrito, request.datos.datosFacturacionDireccion, request.datos.datosFacturacionRazonSocial, request.datos.datosFacturacionCodigoPostal, request.datos.datosFacturacionNombreFantasia, request.datos.datosFacturacionActividadEconomica, request.datos.datosFacturacionPeriodoFacturacion, request.datos.restringeEleccionDte, request.datos.dtePuedeFacturar);
    //         retorno.estado = Respuestas.RESPUESTA_OK;
    //         if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_NOK) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
    //             this.variablesApi.ingreso.errorMessage = crearCuentaEmpresa.data.message;
    //             retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //             retorno.paginaRespuesta = Paginas.ErrorNOK;
    //         } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_SA) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
    //             retorno.paginaRespuesta = Paginas.ErrorSA;
    //         } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_SC) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
    //             this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
    //             retorno.modalSalida = Paginas.ModalSCComponent;
                
    //         } else if (crearCuentaEmpresa.estado === Respuestas.RESPUESTA_OK) {
    //             if (typeof crearCuentaEmpresa.data.message !== 'undefined') {
    //                 this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
    //                 this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
    //                 this.variablesApi.ingreso.errorMessage = crearCuentaEmpresa.data.message;
    //                 retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //                 retorno.mensajeSalida = crearCuentaEmpresa.data.message;
    //             } else { }
                
    //         }
    //     } else {
    //         this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //         this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
    //         this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
    //         retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //         retorno.paginaRespuesta = Paginas.ErrorNOK;
    //     }
    //     return retorno;
    // }
    
    public async homeConsultarDatosFacturacion(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarDatosFacturacion = await this.homeService.homeConsultarDatosFacturacion(this.convertNumber(request.datos.cuentaId));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (consultarDatosFacturacion.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = consultarDatosFacturacion.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarDatosFacturacion.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarDatosFacturacion.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (consultarDatosFacturacion.estado === Respuestas.RESPUESTA_OK) {
                if (typeof consultarDatosFacturacion.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = consultarDatosFacturacion.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = consultarDatosFacturacion.data.message;
                } else {
                    retorno.data = consultarDatosFacturacion.data;
                    retorno.paginaRespuesta = Paginas.HomeConductorConfirmar;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeCambiarCuentaSeleccionada(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let cambiarCuenta = await this.homeService.homeCambiarCuentaSeleccionada(request.datos.cuentaId);
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (cambiarCuenta.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = cambiarCuenta.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (cambiarCuenta.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (cambiarCuenta.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (cambiarCuenta.estado === Respuestas.RESPUESTA_OK) {
                let req: RequestTipo = new RequestTipo();
                req.accion = AccionTipo.ACCION_INICIO;
                req.datos = { telefonoId: "", usuarioId: "" };
                let inicio = await this.homeInicio(req);
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                retorno.paginaRespuesta = Paginas.HomeMuevo;
                
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeUsuarioInvitar(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            for (const data of request.datos) {
                alert('obsoleto')
                // let usuarioInvitar = await this.homeService.homeUsuarioInvitar(this.convertNumber(data.cuentaId), this.convertNumber(data.rolId), data.invitacionTelefonoId, data.invitacionNombre, data.invitacionApellido, this.convertNumber(data.estado), this.convertNumber(data.montoMaximoDiaValor), this.convertNumber(data.montoMaximoMesValor), data.usuarioCuentaConduce);
                // retorno.estado = Respuestas.RESPUESTA_OK;
                // if (usuarioInvitar.estado === Respuestas.RESPUESTA_NOK) {
                //     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                //     this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                //     this.variablesApi.ingreso.errorMessage = usuarioInvitar.data.message;
                //     retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                //     retorno.paginaRespuesta = Paginas.ErrorNOK;
                // } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_SA) {
                //     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                //     this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                //     retorno.paginaRespuesta = Paginas.ErrorSA;
                // } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_SC) {
                //     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                //     this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                //     retorno.modalSalida = Paginas.ModalSCComponent;
                    
                // } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_OK) {
                //     if (typeof usuarioInvitar.data.message !== 'undefined') {
                //         this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                //         this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                //         this.variablesApi.ingreso.errorMessage = usuarioInvitar.data.message;
                //         retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                //         retorno.mensajeSalida = usuarioInvitar.data.message;
                //     } else {
                //         this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                //         this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevoConfirmarConductor;
                //         retorno.paginaRespuesta = Paginas.HomeMuevoConfirmarConductor;
                //     }
                // }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeUsuarioAceptaInvitacion(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let usuarioInvitar = await this.homeService.homeUsuarioAceptaInvitacion(this.convertNumber(request.datos.invitacionId), this.convertNumber(request.datos.invitacionAceptada));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (usuarioInvitar.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = usuarioInvitar.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_OK) {
                if (typeof usuarioInvitar.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = usuarioInvitar.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = usuarioInvitar.data.message;
                } else {
                    request.accion = AccionTipo.ACCION_INICIO;
                    request.datos.telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
                    let inicio = await this.homeInicio(request);
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                    retorno.paginaRespuesta = Paginas.HomeMuevo;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeUsuarioCancelaInvitacion(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let usuarioInvitar = await this.homeService.homeUsuarioCancelaInvitacion(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.invitacionId));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (usuarioInvitar.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = usuarioInvitar.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_OK) {
                if (typeof usuarioInvitar.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = usuarioInvitar.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = usuarioInvitar.data.message;
                } else {
                    request.accion = AccionTipo.ACCION_INICIO;
                    request.datos.telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
                    let inicio = await this.homeInicio(request);
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                    retorno.paginaRespuesta = Paginas.HomeMuevo;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeUsuarioAutorizaInvitacion(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let usuarioInvitar = await this.homeService.homeUsuarioAutorizaInvitacion(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.invitacionId), this.convertNumber(request.datos.invitacionAutorizada), this.convertNumber(request.datos.combustibleEstado), this.convertNumber(request.datos.combustibleMontoMaximoDiaValor), this.convertNumber(request.datos.combustibleMontoMaximoMesValor));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (usuarioInvitar.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = usuarioInvitar.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (usuarioInvitar.estado === Respuestas.RESPUESTA_OK) {
                if (typeof usuarioInvitar.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = usuarioInvitar.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = usuarioInvitar.data.message;
                } else {
                    request.accion = AccionTipo.ACCION_INICIO;
                    request.datos.telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
                    let inicio = await this.homeInicio(request);
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevoConfirmarConductor;
                    retorno.paginaRespuesta = Paginas.HomeMuevoConfirmarConductor;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeUsuarioConsultarInvitacion(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarInvitacion = await this.homeService.homeUsuarioConsultarUsuarios(this.convertNumber(request.datos.cuentaId));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (consultarInvitacion.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = consultarInvitacion.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarInvitacion.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarInvitacion.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (consultarInvitacion.estado === Respuestas.RESPUESTA_OK) {
                if (typeof consultarInvitacion.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = consultarInvitacion.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = consultarInvitacion.data.message;
                } else {
                    this.variablesApi.home.invitados = consultarInvitacion.data.invitados;
                    this.variablesApi.home.usuarios = consultarInvitacion.data.usuarios;
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeConductorConfirmar;
                    retorno.paginaRespuesta = Paginas.HomeConductorConfirmar;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeUsuarioConsultarUsuarios(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarInvitacion = await this.homeService.homeUsuarioConsultarUsuarios(this.convertNumber(request.datos.cuentaId));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (consultarInvitacion.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = consultarInvitacion.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarInvitacion.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarInvitacion.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (consultarInvitacion.estado === Respuestas.RESPUESTA_OK) {
                if (typeof consultarInvitacion.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = consultarInvitacion.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = consultarInvitacion.data.message;
                } else {
                    this.variablesApi.home.invitados = consultarInvitacion.data.invitados;
                    this.variablesApi.home.usuarios = consultarInvitacion.data.usuarios;
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeConductorConfirmar;
                    retorno.paginaRespuesta = Paginas.HomeConductorConfirmar;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeUsuarioModificar(request: RequestTipo, tipo: number = 0): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let usuarioModificar = await this.homeService.homeUsuarioModificar(this.convertNumber(request.datos.cuentaId), request.datos.usuarioId, this.convertNumber(request.datos.grupoCuentaId), this.convertNumber(request.datos.usuarioCuentaEstadoId), this.convertNumber(request.datos.rolId), this.convertNumber(request.datos.usuarioCuentaConduce), this.convertNumber(request.datos.usuarioCuentaRindeGasto), this.convertNumber(request.datos.usuarioCuentaCreaVehiculo), this.convertNumber(request.datos.usuarioCuentaEligeDTE), this.convertNumber(request.datos.usuarioCuentaSeleccionado), request.datos.vehiculosAsignados, request.datos.estadoCombustible, this.convertNumber(request.datos.montoMaximoDiaValor), this.convertNumber(request.datos.montoMaximoMesValor));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (usuarioModificar.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = usuarioModificar.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (usuarioModificar.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (usuarioModificar.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (usuarioModificar.estado === Respuestas.RESPUESTA_OK) {
                if (typeof usuarioModificar.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = usuarioModificar.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = usuarioModificar.data.message;
                } else {
                    let inicio
                    if (tipo == 0) {
                        request.accion = AccionTipo.ACCION_INICIO;
                        request.datos.telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
                        inicio = await this.homeInicio(request);
                    } else {
                        request.accion = AccionTipo.ACCION_CLICK;
                        inicio = await this.homeUsuarioConsultarUsuarios(request);
                    }
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeConductorConfirmar;
                    retorno.paginaRespuesta = Paginas.HomeConductorConfirmar;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeUsuarioModificarMasivo(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            for (const data of request.datos) {
                let usuarioModificar = await this.homeService.homeUsuarioModificar(this.convertNumber(data.cuentaId), data.usuarioId, this.convertNumber(data.grupoCuentaId), this.convertNumber(data.usuarioCuentaEstadoId), this.convertNumber(data.rolId), this.convertNumber(data.usuarioCuentaConduce), this.convertNumber(data.usuarioCuentaRindeGasto), this.convertNumber(data.usuarioCuentaCreaVehiculo), this.convertNumber(data.usuarioCuentaEligeDTE), this.convertNumber(data.usuarioCuentaSeleccionado), data.vehiculosAsignados, data.estadoCombustible, this.convertNumber(data.montoMaximoDiaValor), this.convertNumber(data.montoMaximoMesValor));
                retorno.estado = Respuestas.RESPUESTA_OK;
                if (usuarioModificar.estado === Respuestas.RESPUESTA_NOK) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                    this.variablesApi.ingreso.errorMessage = usuarioModificar.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.paginaRespuesta = Paginas.ErrorNOK;
                } else if (usuarioModificar.estado === Respuestas.RESPUESTA_SA) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                    retorno.paginaRespuesta = Paginas.ErrorSA;
                } else if (usuarioModificar.estado === Respuestas.RESPUESTA_SC) {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                    this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                    retorno.modalSalida = Paginas.ModalSCComponent;
                    
                } else if (usuarioModificar.estado === Respuestas.RESPUESTA_OK) {
                    if (typeof usuarioModificar.data.message !== 'undefined') {
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                        this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                        this.variablesApi.ingreso.errorMessage = usuarioModificar.data.message;
                        retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                        retorno.mensajeSalida = usuarioModificar.data.message;
                    } else {
                        // request.accion = AccionTipo.ACCION_INICIO;
                        // request.datos.telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
                        // let inicio = await this.homeInicio(request);
                        this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                        this.variablesApi.ingreso.paginaTipo = Paginas.HomeConductorConfirmar;
                        retorno.paginaRespuesta = Paginas.HomeConductorConfirmar;
                    }
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    // public async homeVehiculoCrear(request: RequestTipo): Promise<ResponseTipo> {
    //     let retorno = new ResponseTipo();
    //     if (request.accion == AccionTipo.ACCION_CLICK) {
    //         for (const data of request.datos) {
    //             let creaVehiculo = await this.homeService.homeVehiculoCrear(this.convertNumber(data.cuentaId), this.convertNumber(data.flotaCuentaId), data.vehiculoCuentaPatente, data.vehiculoCuentaModelo, data.vehiculoCuentaMarca, this.convertNumber(data.vehiculoCuentaAno), this.convertNumber(data.vehiculoCuentaTanque), data.vehiculoCuentaAlias, this.convertNumber(data.vehiculoCuentaTipo));
    //             retorno.estado = Respuestas.RESPUESTA_OK;
    //             if (creaVehiculo.estado === Respuestas.RESPUESTA_NOK) {
    //                 this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
    //                 this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
    //                 this.variablesApi.ingreso.errorMessage = creaVehiculo.data.message;
    //                 retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //                 retorno.paginaRespuesta = Paginas.ErrorNOK;
    //             } else if (creaVehiculo.estado === Respuestas.RESPUESTA_SA) {
    //                 this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //                 this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
    //                 retorno.paginaRespuesta = Paginas.ErrorSA;
    //             } else if (creaVehiculo.estado === Respuestas.RESPUESTA_SC) {
    //                 this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
    //                 this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
    //                 retorno.modalSalida = Paginas.ModalSCComponent;
                    
    //             } else if (creaVehiculo.estado === Respuestas.RESPUESTA_OK) {
    //                 if (typeof creaVehiculo.data.message !== 'undefined') {
    //                     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
    //                     this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
    //                     this.variablesApi.ingreso.errorMessage = creaVehiculo.data.message;
    //                     retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //                     retorno.mensajeSalida = creaVehiculo.data.message;
    //                 } else {
    //                     this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //                     this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevoConfirmarVehiculo;
    //                     retorno.paginaRespuesta = Paginas.HomeMuevoConfirmarVehiculo;
    //                 }
    //             }
    //         }
    //     } else {
    //         this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //         this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
    //         this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
    //         retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //         retorno.paginaRespuesta = Paginas.ErrorNOK;
    //     }
    //     return retorno;
    // }
    
    // public async homeVehiculoModificar(request: RequestTipo): Promise<ResponseTipo> {
    //     let retorno = new ResponseTipo();
    //     if (request.accion == AccionTipo.ACCION_CLICK) {
    //         let modificaVehiculo = await this.homeService.homeVehiculoModificar(this.convertNumber(request.datos.vehiculoCuentaId), this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.flotaCuentaId), request.datos.vehiculoCuentaPatente, request.datos.vehiculoCuentaModelo, request.datos.vehiculoCuentaMarca, this.convertNumber(request.datos.vehiculoCuentaAno), this.convertNumber(request.datos.vehiculoCuentaTanque), request.datos.vehiculoCuentaAlias, request.datos.vehiculoCuentaTipo, this.convertNumber(request.datos.vehiculoCuentaValidado), this.convertNumber(request.datos.vehiculoCuentaEstado));
    //         retorno.estado = Respuestas.RESPUESTA_OK;
    //         if (modificaVehiculo.estado === Respuestas.RESPUESTA_NOK) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
    //             this.variablesApi.ingreso.errorMessage = modificaVehiculo.data.message;
    //             retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //             retorno.paginaRespuesta = Paginas.ErrorNOK;
    //         } else if (modificaVehiculo.estado === Respuestas.RESPUESTA_SA) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
    //             retorno.paginaRespuesta = Paginas.ErrorSA;
    //         } else if (modificaVehiculo.estado === Respuestas.RESPUESTA_SC) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
    //             this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
    //             retorno.modalSalida = Paginas.ModalSCComponent;
                
    //         } else if (modificaVehiculo.estado === Respuestas.RESPUESTA_OK) {
    //             if (typeof modificaVehiculo.data.message !== 'undefined') {
    //                 this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
    //                 this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
    //                 this.variablesApi.ingreso.errorMessage = modificaVehiculo.data.message;
    //                 retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //                 retorno.mensajeSalida = modificaVehiculo.data.message;
    //             } else {
    //                 request.accion = AccionTipo.ACCION_INICIO;
    //                 request.datos.telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
    //                 let inicio = await this.homeInicio(request);
    //                 this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //                 this.variablesApi.ingreso.paginaTipo = Paginas.HomeVehiculoPermisos;
    //                 retorno.paginaRespuesta = Paginas.HomeVehiculoPermisos;
    //             }
    //         }
    //     } else {
    //         this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //         this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
    //         this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
    //         retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //         retorno.paginaRespuesta = Paginas.ErrorNOK;
    //     }
    //     return retorno;
    // }
    
    public async homeFlotaCrear(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let crearFlota = await this.homeService.homeFlotaCrear(this.convertNumber(request.datos.cuentaId), request.datos.flotaCuentaNombre);
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (crearFlota.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = crearFlota.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (crearFlota.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (crearFlota.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (crearFlota.estado === Respuestas.RESPUESTA_OK) {
                if (typeof crearFlota.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = crearFlota.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = crearFlota.data.message;
                } else {
                    let req: RequestTipo = new RequestTipo();
                    req.accion = AccionTipo.ACCION_INICIO;
                    req.datos = { telefonoId: "", usuarioId: "" };
                    let inicio = await this.homeInicio(req);
                    
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeVehiculoCrearFlota;
                    retorno.paginaRespuesta = Paginas.HomeVehiculoCrearFlota;
                }
            }
            
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeFlotaModificar(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let modificarFlota = await this.homeService.homeFlotaModificar(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.flotaCuentaId), request.datos.flotaCuentaNombre, this.convertNumber(request.datos.flotaCuentaEstado));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (modificarFlota.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = modificarFlota.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (modificarFlota.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (modificarFlota.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (modificarFlota.estado === Respuestas.RESPUESTA_OK) {
                if (typeof modificarFlota.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = modificarFlota.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = modificarFlota.data.message;
                } else {
                    request.accion = AccionTipo.ACCION_INICIO;
                    request.datos.telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
                    let inicio = await this.homeInicio(request);
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeConductorCrearGrupo;
                    retorno.paginaRespuesta = Paginas.HomeConductorCrearGrupo;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeGrupoCrear(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let crearGrupo = await this.homeService.homeGrupoCrear(this.convertNumber(request.datos.cuentaId), request.datos.grupoCuentaNombre);
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (crearGrupo.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = crearGrupo.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (crearGrupo.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (crearGrupo.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (crearGrupo.estado === Respuestas.RESPUESTA_OK) {
                if (typeof crearGrupo.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = crearGrupo.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = crearGrupo.data.message;
                } else {
                    let req: RequestTipo = new RequestTipo();
                    req.accion = AccionTipo.ACCION_INICIO;
                    req.datos = { telefonoId: "", usuarioId: "" };
                    let inicio = await this.homeInicio(req);
                    
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeConductorCrearGrupo;
                    retorno.paginaRespuesta = Paginas.HomeConductorCrearGrupo;
                }
            }
            
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    public async homeGrupoModificar(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let modificarGrupo = await this.homeService.homeGrupoModificar(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.grupoCuentaId), request.datos.grupoCuentaNombre, this.convertNumber(request.datos.grupoCuentaEstado));
            retorno.estado = Respuestas.RESPUESTA_OK;
            if (modificarGrupo.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
                this.variablesApi.ingreso.errorMessage = modificarGrupo.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (modificarGrupo.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA;
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (modificarGrupo.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
                
            } else if (modificarGrupo.estado === Respuestas.RESPUESTA_OK) {
                if (typeof modificarGrupo.data.message !== 'undefined') {
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                    this.variablesApi.ingreso.animacionTipo = AnimacionTipo.OBJETO_EXISTENTE
                    this.variablesApi.ingreso.errorMessage = modificarGrupo.data.message;
                    retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                    retorno.mensajeSalida = modificarGrupo.data.message;
                } else {
                    let req: RequestTipo = new RequestTipo();
                    req.accion = AccionTipo.ACCION_INICIO;
                    req.datos = { telefonoId: "", usuarioId: "" };
                    let inicio = await this.homeInicio(req);
                    this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                    this.variablesApi.ingreso.paginaTipo = Paginas.HomeConductorCrearGrupo;
                    retorno.paginaRespuesta = Paginas.HomeConductorCrearGrupo;
                }
            }
        } else {
            this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
            this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK;
            this.variablesApi.ingreso.errorMessage = "Opcion no Existente";
            retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
            retorno.paginaRespuesta = Paginas.ErrorNOK;
        }
        return retorno;
    }
    
    // public async homeUsuarioConsultarFacturas(request: RequestTipo): Promise<ResponseTipo> {
    //     let retorno = new ResponseTipo();
    //     if (request.accion == AccionTipo.ACCION_CLICK) {
    //         let consultarFacturas = await this.homeService.homeUsuarioConsultarFacturas(this.convertNumber(request.datos.cuentaId), request.datos.fechaConsulta);
    //         if (consultarFacturas.estado === Respuestas.RESPUESTA_NOK) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
    //             this.variablesApi.ingreso.errorMessage = consultarFacturas.data.message;
    //             retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //             retorno.paginaRespuesta = Paginas.ErrorNOK;
    //         } else if (consultarFacturas.estado === Respuestas.RESPUESTA_SC) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
    //             this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
    //             retorno.modalSalida = Paginas.ModalSCComponent;
    //         } else if (consultarFacturas.estado === Respuestas.RESPUESTA_SA) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
    //             retorno.paginaRespuesta = Paginas.ErrorSA;
    //         } else if (consultarFacturas.estado === Respuestas.RESPUESTA_OK) {
    //             this.variablesApi.home.facturas = consultarFacturas.data.facturas;
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.HomeFacturas;
    //             retorno.paginaRespuesta = Paginas.HomeFacturas;
    //         }
    //     }
    //     return retorno;
    // }
    
    public async homeUsuarioConsultarDetalleFacturas(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarDetalleFacturas = await this.homeService.homeUsuarioConsultarDetalleFacturas(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.facturaId));
            if (consultarDetalleFacturas.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = consultarDetalleFacturas.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarDetalleFacturas.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (consultarDetalleFacturas.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarDetalleFacturas.estado === Respuestas.RESPUESTA_OK) {
                retorno.data = consultarDetalleFacturas.data;
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.HomeFacturas;
                retorno.paginaRespuesta = Paginas.HomeFacturas;
            }
        }
        return retorno;
    }
    
    public async homeUsuarioConsultarTransacciones(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarTransacciones = await this.homeService.homeUsuarioConsultarTransacciones(this.convertNumber(request.datos.cuentaId), null, request.datos.fechaConsulta, 0);
            if (consultarTransacciones.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = consultarTransacciones.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarTransacciones.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (consultarTransacciones.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarTransacciones.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.home.guias = consultarTransacciones.data.guias;
                this.variablesApi.home.boletas = consultarTransacciones.data.boletas;
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.HomeFacturas;
                retorno.paginaRespuesta = Paginas.HomeFacturas;
            }
        }
        return retorno;
    }
    
    public async homeUsuarioConsultarMovimientos(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarMovimientos = await this.homeService.homeUsuarioConsultarMovimientos(this.convertNumber(request.datos.cuentaId), request.datos.fechaConsulta);
            if (consultarMovimientos.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = consultarMovimientos.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarMovimientos.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (consultarMovimientos.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarMovimientos.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.home.movimientos = consultarMovimientos.data.movimientos;
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.HomeFacturas;
                retorno.paginaRespuesta = Paginas.HomeFacturas;
            }
        }
        return retorno;
    }
    
    public async homeUsuarioConsultarDetalleTransacciones(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarDetalleTransacciones = await this.homeService.homeUsuarioConsultarDetalleTransacciones(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.ventaId));
            if (consultarDetalleTransacciones.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = consultarDetalleTransacciones.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarDetalleTransacciones.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (consultarDetalleTransacciones.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarDetalleTransacciones.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = consultarDetalleTransacciones.data;
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.HomeFacturas;
                retorno.paginaRespuesta = Paginas.HomeFacturas;
            }
        }
        return retorno;
    }
    
    public async homeCuentaConsultarMedioPago(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarMedioPago = await this.homeService.homeCuentaConsultarMedioPago(this.convertNumber(request.datos.cuentaId));
            if (consultarMedioPago.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = consultarMedioPago.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarMedioPago.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (consultarMedioPago.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarMedioPago.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = consultarMedioPago.data;
                this.variablesApi.home.mediosPago = consultarMedioPago.data.mediosPago;
                this.variablesApi.home.cuentasCorrientes = consultarMedioPago.data.cuentasCorrientes;
                retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
            }
        }
        return retorno;
    }
    
    /** (Obsoleto): traer solo datos de quien llame a esta funciona */
    public async homeCuentaNuevoConsultarMedioPago(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let consultarMedioPago = await this.homeService.homeCuentaNuevoConsultarMedioPago(this.convertNumber(request.datos.cuentaId));
            if (consultarMedioPago.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = consultarMedioPago.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (consultarMedioPago.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (consultarMedioPago.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (consultarMedioPago.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = consultarMedioPago.data;
                this.variablesApi.home.mediosPago = consultarMedioPago.data.mediosPago;
                this.variablesApi.home.cuentasCorrientes = consultarMedioPago.data.cuentasCorrientes;
                this.variablesApi.home.tarjetas = consultarMedioPago.data.tarjetas;
                this.variablesApi.home.tarjetaCompartidaId = consultarMedioPago.data.tarjetaCompartidaId;
                retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
            }
        }
        return retorno;
    }
    
    public async homeCuentaNuevoPostMedioPago(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let postMedioPago = await this.homeService.homeCuentaNuevoPostMedioPago(this.convertNumber(request.datos.cuentaId));
            if (postMedioPago.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = postMedioPago.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (postMedioPago.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (postMedioPago.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (postMedioPago.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = postMedioPago.data;
                retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
            }
        }
        return retorno;
    }
    
    public async homeCuentaModificarMedioPago(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let modificarMedioPago = await this.homeService.homeCuentaModificarMedioPago(this.convertNumber(request.datos.formaPagoSaldo), this.convertNumber(request.datos.formaPagoEstacion), this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.formaPagoOneclick));
            if (modificarMedioPago.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = modificarMedioPago.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (modificarMedioPago.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (modificarMedioPago.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (modificarMedioPago.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = modificarMedioPago.data;
                retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
            }
        }
        return retorno;
    }
    
    /** Obsolete, utilizar rx */
    public async homeCuentaNuevoModificarMedioPago(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let modificarMedioPago = await this.homeService.homeCuentaNuevoModificarMedioPago(this.convertNumber(request.datos.formaPagoSaldo), this.convertNumber(request.datos.formaPagoEstacion), this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.formaPagoOneclick), request.datos.tarjetaCompartidaId, request.datos.tarjetaCompartidaUltimos4Digitos, request.datos.tarjetaCompartidaTipo);
            if (modificarMedioPago.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = modificarMedioPago.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (modificarMedioPago.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (modificarMedioPago.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (modificarMedioPago.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = modificarMedioPago.data;
                retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
            }
        }
        return retorno;
    }
    
    public async homeCuentaNuevoEliminarMedioPago(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let eliminarMedioPago = await this.homeService.homeCuentaNuevoEliminarMedioPago(this.convertNumber(request.datos.cuentaId), request.datos.idInscripcion);
            if (eliminarMedioPago.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = eliminarMedioPago.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (eliminarMedioPago.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (eliminarMedioPago.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (eliminarMedioPago.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = eliminarMedioPago.data;
                retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
            }
        }
        return retorno;
    }
    
    // public async homeCuentaAsociarCuentaCorriente(request: RequestTipo): Promise<ResponseTipo> {
    //     let retorno = new ResponseTipo();
    //     if (request.accion == AccionTipo.ACCION_CLICK) {
    //         let asociarCuentaCorriente = await this.homeService.homeCuentaAsociarCuentaCorriente(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.cuentaCorrienteNumero), request.datos.cuentaCorrienteRut, request.datos.cuentaCorrienteNombreTitular, this.convertNumber(request.datos.tipoCuentaBancariaId), this.convertNumber(request.datos.bancoId));
    //         if (asociarCuentaCorriente.estado === Respuestas.RESPUESTA_NOK) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
    //             this.variablesApi.ingreso.errorMessage = asociarCuentaCorriente.data.message;
    //             retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
    //             retorno.paginaRespuesta = Paginas.ErrorNOK;
    //         } else if (asociarCuentaCorriente.estado === Respuestas.RESPUESTA_SC) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
    //             this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
    //             retorno.modalSalida = Paginas.ModalSCComponent;
    //         } else if (asociarCuentaCorriente.estado === Respuestas.RESPUESTA_SA) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //             this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
    //             retorno.paginaRespuesta = Paginas.ErrorSA;
    //         } else if (asociarCuentaCorriente.estado === Respuestas.RESPUESTA_OK) {
    //             this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
    //             retorno.data = asociarCuentaCorriente.data;
    //             retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
    //         }
    //     }
    //     return retorno;
    // }
    
    public async homeUsuarioCuentaObtener(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let usuarioCuentaObtener = await this.homeService.homeUsuarioCuentaObtener(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.usuarioObtenerId));
            if (usuarioCuentaObtener.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = usuarioCuentaObtener.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (usuarioCuentaObtener.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (usuarioCuentaObtener.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (usuarioCuentaObtener.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = usuarioCuentaObtener.data;
                retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
            }
        }
        return retorno;
    }
    
    public async homeVehiculoCuentaObtener(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let vehiculoObtener = await this.homeService.homeVehiculoCuentaObtener(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.vehiculoCuentaId));
            if (vehiculoObtener.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = vehiculoObtener.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (vehiculoObtener.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (vehiculoObtener.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (vehiculoObtener.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = vehiculoObtener.data;
                retorno.paginaRespuesta = Paginas.HomeMuevoMisMetodosPago;
            }
        }
        return retorno;
    }
    
    public async homeUsuarioCuentaModificarRestricciones(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let usuarioCuentaModificarRestricciones = await this.homeService.homeUsuarioCuentaModificarRestricciones(this.convertNumber(request.datos.cuentaId), this.convertNumber(request.datos.usuarioId), this.convertNumber(request.datos.diaNoHabilitadoEstado), request.datos.diaNoHabilitadoValor, this.convertNumber(request.datos.horarioNoHabilitadoEstado), request.datos.horarioNoHabilitadoValor, this.convertNumber(request.datos.estacionNoHabilitadaEstado), request.datos.estacionNoHabilitadaValor);
            if (usuarioCuentaModificarRestricciones.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = usuarioCuentaModificarRestricciones.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (usuarioCuentaModificarRestricciones.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (usuarioCuentaModificarRestricciones.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (usuarioCuentaModificarRestricciones.estado === Respuestas.RESPUESTA_OK) {
                request.accion = AccionTipo.ACCION_INICIO;
                request.datos.telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
                let inicio = await this.homeInicio(request);
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.HomeMuevo;
                retorno.paginaRespuesta = Paginas.HomeMuevo;
            }
        }
        return retorno;
    }
    
    public async homeUsuarioCuentaAbonoPost(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let abonoPost = await this.homeService.homeUsuarioCuentaAbonoPost(this.convertNumber(request.datos.cuentaId), request.datos.idInscripcion, this.convertNumber(request.datos.monto));
            if (abonoPost.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = abonoPost.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (abonoPost.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (abonoPost.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (abonoPost.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = abonoPost.data;
                retorno.paginaRespuesta = Paginas.HomeMuevoAbonarBilletera;
            }
        }
        return retorno;
    }
    
    public async homeUsuarioCuentaAbonoGet(request: RequestTipo): Promise<ResponseTipo> {
        let retorno = new ResponseTipo();
        if (request.accion == AccionTipo.ACCION_CLICK) {
            let abonoGet = await this.homeService.homeUsuarioCuentaAbonoGet(this.convertNumber(request.datos.cuentaId), request.datos.numeroProceso);
            if (abonoGet.estado === Respuestas.RESPUESTA_NOK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.ANIMACION;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorNOK
                this.variablesApi.ingreso.errorMessage = abonoGet.data.message;
                retorno.mensajeSalida = { tipoMensaje: 'alert', tituloMensaje: '', textoMensaje: this.variablesApi.ingreso.errorMessage };
                retorno.paginaRespuesta = Paginas.ErrorNOK;
            } else if (abonoGet.estado === Respuestas.RESPUESTA_SC) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.MODAL;
                this.variablesApi.ingreso.modalTipo = Paginas.ModalSCComponent;
                retorno.modalSalida = Paginas.ModalSCComponent;
            } else if (abonoGet.estado === Respuestas.RESPUESTA_SA) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                this.variablesApi.ingreso.paginaTipo = Paginas.ErrorSA
                retorno.paginaRespuesta = Paginas.ErrorSA;
            } else if (abonoGet.estado === Respuestas.RESPUESTA_OK) {
                this.variablesApi.ingreso.navegacionTipo = NavegacionTipo.PAGINA;
                retorno.data = abonoGet.data;
                retorno.paginaRespuesta = Paginas.HomeMuevoAbonarBilletera;
            }
        }
        return retorno;
    }
    
    /* FUNCIONES */
    
    private convertNumber(valor: string): number {
        let val = parseInt(valor);
        if (!isNaN(val)) {
            return val;
        } else {
            return -1;
        }
    }
}
