import { AccionTipo } from '../globales/enumeradores';

export class ResponseTipo {
    estado: string;
    paginaRespuesta: string;
    mensajeSalida: {
        tipoMensaje: string;
        tituloMensaje: string;
        textoMensaje: string;
    };
    modalSalida: string;
    data: any;
}

export class RequestTipo {
    accion: AccionTipo;
    datos?: any;
    funcion?: any;
}