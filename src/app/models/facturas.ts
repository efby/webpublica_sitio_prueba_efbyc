export class Facturas {
    facturaDatosFacturacion: DatosFacturacion
    facturaFechaCase: string;
    facturaEstadoId: string;
    facturaPagada: boolean;
    facturaPagadaString: string;
    facturaNroDocumento: string;
    facturaFechaEmision: string;
    facturaFechaVencimiento: string;
    facturaFechaEmisionString: string;
    facturaFechaVencimientoString: string;
    facturaId: number;
    facturaMontoTotal: number;
    facturaMontoTotalString: string;
    facturaUrl: string;
    tipoOperacionId: string;
    detalleFactura?: DetalleFactura[];
    collapsed: boolean;
    tieneDocumento: boolean;

    constructor() {
        
        this.facturaEstadoId = "";
        this.facturaFechaCase = "";
        this.facturaNroDocumento = "";
        this.facturaFechaEmision = "";
        this.facturaFechaVencimiento = "";
        this.facturaFechaEmisionString = "";
        this.facturaFechaVencimientoString = "";
        this.facturaId = 0;
        this.facturaMontoTotal = 0;
        this.facturaMontoTotalString = "";
        this.facturaUrl = "";
        this.tipoOperacionId = "";
        this.collapsed = true;
        this.tieneDocumento = false;

        this.facturaDatosFacturacion = new DatosFacturacion();
        this.detalleFactura = [];
    }
}

export class DatosFacturacion {
    datosFacturacionActividadEconomica: number;
    datosFacturacionCodigoPostal: number;
    datosFacturacionDestinatarioFacturaSap: number;
    datosFacturacionDeudorSap: number;
    datosFacturacionDiasCredito: number;
    datosFacturacionDireccion: string;
    datosFacturacionDistrito: string;
    datosFacturacionEmail: string;
    datosFacturacionFechaUpdate: Date;
    datosFacturacionGiro: string;
    datosFacturacionNombreFantasia: string;
    datosFacturacionPeriodoFacturacion: string;
    datosFacturacionRazonSocial: string;
    datosFacturacionRut: string;

    constructor() {
        this.datosFacturacionActividadEconomica = 0;
        this.datosFacturacionCodigoPostal = 0;
        this.datosFacturacionDestinatarioFacturaSap = 0;
        this.datosFacturacionDeudorSap = 0;
        this.datosFacturacionDiasCredito = 0;
        this.datosFacturacionDireccion = "";
        this.datosFacturacionDistrito = "";
        this.datosFacturacionEmail = "";
        this.datosFacturacionFechaUpdate = new Date();
        this.datosFacturacionGiro = "";
        this.datosFacturacionNombreFantasia = "";
        this.datosFacturacionPeriodoFacturacion = "";
        this.datosFacturacionRazonSocial = "";
        this.datosFacturacionRut = "";
    }
}

export class DetalleFactura {
    ventaId: number;
    documentoUrl: string;
    ventaPagoTotal: number;
    vehiculoPatente: string;
    documentoEntrega: string;
    ventaFechaCreacion: Date;
    ventaDocumentoFolio: string;

    constructor() {
        this.ventaId = 0;
        this.documentoUrl = "";
        this.ventaPagoTotal = 0;
        this.vehiculoPatente = "";
        this.documentoEntrega = "";
        this.ventaFechaCreacion = new Date();
        this.ventaDocumentoFolio = "";
    }
}
