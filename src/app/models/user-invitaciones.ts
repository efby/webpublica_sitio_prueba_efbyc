export class UserInvitaciones {
    nombreInvitado: string;
    apellidosInvitado: string;
    rutInvitado: string;
    correoInvitado: string;
    telefonoInvitado: string;
    rolInvitado: number;


    constructor() {
        this.nombreInvitado = "";
        this.apellidosInvitado = "";
        this.rutInvitado = "";
        this.correoInvitado = "";
        this.telefonoInvitado = "";
        this.rolInvitado = 0;
    }
}
