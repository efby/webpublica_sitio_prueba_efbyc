export class Boletas {
    comercioId: number;
    documentoUrl: string;
    tieneDocumento: boolean;
    siiMedioPagoId: string;
    tipoDocumentoId: number;
    vehiculoPatente: string;
    ventaAtendedorNombre: string;
    ventaCalificacionComentario: string;
    ventaCalificacionNota: number;
    ventaDescuentoTotal: number;
    ventaDocumentoFolio: string;
    ventaFechaCreacion: string;
    ventaFechaCreacionString: string;
    ventaId: number;
    ventaMontoTotal: number;
    ventaNumeroTrxComercio: number;
    ventaPagoTotal: number;
    ventaPagoTotalString: string;
    ventaSitioNombre: string;
    formaPagoTipo: number;
    usuarioNombreApellido: string;
    usuarioRut: string;
    cuentaNombre: string;
    vehiculoOdometro: number;
    ventaEstacion: string;
    ventaEstadoId: string;
    ventaFecha: string;
    ventaHora: string;
    ventaNumeroProceso: number;
    detalleBoletas?: DetalleBoleta[];
    collapsed: boolean;

    constructor() {
        this.comercioId = 0;
        this.documentoUrl = "";
        this.tieneDocumento = false;
        this.siiMedioPagoId = "";
        this.tipoDocumentoId = 0;
        this.vehiculoPatente = "";
        this.ventaAtendedorNombre = "";
        this.ventaCalificacionComentario = "";
        this.ventaCalificacionNota = 0;
        this.ventaDescuentoTotal = 0;
        this.ventaDocumentoFolio = ""
        this.ventaFechaCreacion = "";
        this.ventaFechaCreacionString = "";
        this.ventaId = 0;
        this.ventaMontoTotal = 0;
        this.ventaNumeroTrxComercio = 0;
        this.ventaPagoTotal = 0;
        this.ventaPagoTotalString = "";
        this.ventaSitioNombre = "";
        this.detalleBoletas = [];
        this.formaPagoTipo = 0;
        this.usuarioNombreApellido = "";
        this.usuarioRut = "";
        this.cuentaNombre = "";
        this.vehiculoOdometro = 0;
        this.ventaEstacion = "";
        this.ventaEstadoId = "";
        this.ventaFecha = "";
        this.ventaHora = "";
        this.ventaNumeroProceso = 0;
        this.collapsed = true;
    }
}

export class DetalleBoleta {
    lineaId: number;
    ventaId: number;
    productoId: number;
    productoNombre: string;
    lineaCantidad: number;
    lineaPagoTotal: number;
    lineaMontoTotal: number;
    lineaUnidadMedida: string;
    lineaDescuentoTotal: number;
    lineaPrecioUnitario: number;
    lineaDescuentoUnitario: number;

    constructor() {
        this.lineaId = 0;
        this.ventaId = 0;
        this.productoId = 0;
        this.productoNombre = "";
        this.lineaCantidad = 0;
        this.lineaPagoTotal = 0;
        this.lineaMontoTotal = 0;
        this.lineaUnidadMedida = "";
        this.lineaDescuentoTotal = 0;
        this.lineaPrecioUnitario = 0;
        this.lineaDescuentoUnitario = 0;
    }
}