export class Marcadores {
    lat: number;
    lng: number;
    draggable: boolean;
    infoWindow: string;
    email: string;
    telefono: string;
    jefeEDS: string;
    servicios?: any[];
    tiempoViaje?: string;
    desviacion?: string;
    constructor() {
        this.lat = 0;
        this.lng = 0;
        this.draggable = false;
        this.infoWindow = "";
        this.email = "";
        this.telefono = "";
        this.jefeEDS = "";
        this.tiempoViaje = "";
        this.desviacion = "";

    }
}
