import { DeviceOrientation } from './../globales/enumeradores';

export interface DeviceInfo{
    orientation: DeviceOrientation;
    isMobile: boolean;
    heigth: number;
    width: number;
}

export interface OptionsModalDefault{
    title: string;
    messages: string[];

    okText?: string;
    cancelText?: string;
}