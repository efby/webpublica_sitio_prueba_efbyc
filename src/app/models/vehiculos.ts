import { Conductor } from './conductores';

export class Vehiculos {
    cuentaId: number;
    vehiculoId: number;
    patente: string;
    tipo: number;
    marca: string;
    modelo: string;
    alias: string;
    ano?: number;
    conductores?: Conductor[];
    gastoDiario: number;
    gastoMensual: number;
    vehiculoDefault: boolean;
    estado: boolean;
    validado: boolean;
    flotaId?: number;
    tanque: number;
    editable: boolean;
    fechaCreacion: Date;
    fechaCreacionString: string;


    constructor() {
        this.patente = "";
        this.marca = "";
        this.modelo = "";
        this.alias = "";
        this.gastoDiario = 0;
        this.gastoMensual = 0;
        this.tipo = 0;
        this.validado = true;
        this.tanque = 0;
        this.conductores = [];
        this.editable = false;
        this.fechaCreacion = new Date();
        this.fechaCreacionString = "";
        this.ano = 0;
    }
}
