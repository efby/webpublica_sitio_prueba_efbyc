

export class Conductor{
    rolId: number;
    rolNombre: string;
    usuarioId: number;
    rut: string;
    email: string;
    nombre: string;
    nombreCompleto: string;
    apellido: string;
    telefonoId: string;
    colorFlagCombustible: string;
    conduce: boolean;
    estado: boolean;

    primerVehiculoAsignado: string;
    vehiculosAsignadosCantidad: number;
    vehiculosAsignados: Vehiculo[];
    //permiteComprar: boolean;
    //permiteComprarString: string;

    //montoMaximoDia: number;
    //montoMaximoMes: number;
    restriccionCombustible: RestriccionCombustible;

    seleccionDiaHorario: string;
    diasHabilitados: number[];
    horariosHabilitados: number[];
    estacionesHabilitadas: number[];

    constructor(){
        this.rolId = 0;
        this.rolNombre = "";
        this.usuarioId= 0;
        this.rut = "";
        this.email = "";
        this.nombre = "";
        this.nombreCompleto = "";
        this.apellido = "";
        this.telefonoId = "";
        this.colorFlagCombustible = "";
        

        this.conduce = false;
        this.estado = false;
    
    
        this.primerVehiculoAsignado = "";
        this.vehiculosAsignadosCantidad = 0;

        this.restriccionCombustible = new RestriccionCombustible();
        this.vehiculosAsignados = [];
    

        this.seleccionDiaHorario = '';
        this.diasHabilitados = [];
        this.horariosHabilitados = [];
        this.estacionesHabilitadas = [];

    }

    static crearObjetoConductor(element:any, instruccionesDibujo:any): Conductor{

        let conductor: Conductor = new Conductor();
        conductor.apellido = element.usuarioApellido;
        
        conductor.email = element.usuarioEmail;
        conductor.estado = element.usuarioCuentaEstadoId === 1;
        conductor.nombre = element.usuarioNombre;
        conductor.nombreCompleto = element.usuarioNombre + " " + element.usuarioApellido;
        conductor.rolId = element.rolId;
        conductor.rut = element.usuarioRut;
        conductor.telefonoId = element.usuarioTelefonoId;
        conductor.usuarioId = element.usuarioId;
        conductor.vehiculosAsignadosCantidad = element.cantidadVehiculosAsignados;

        conductor.seleccionDiaHorario = instruccionesDibujo != null ? instruccionesDibujo.seleccionDiaHorario || '' : '';
        conductor.colorFlagCombustible = element.colorFlagCombustible;
        conductor.conduce = element.usuarioCuentaConduce == 1;

        if(element.vehiculosAsignados){
            element.vehiculosAsignados.map(x => {
                let v: Vehiculo = new Vehiculo();
                v.vehiculoId = x.vehiculoCuentaId;
                v.alias = x.vehiculoCuentaAlias;
                v.estado = x.vehiculoCuentaEstado == 1;
                v.patente = x.vehiculoPatente;
                v.tipo = x.vehiculoCuentaTipo;

                conductor.vehiculosAsignados.push(v);
            });

        }

        conductor.vehiculosAsignadosCantidad = element.cantidadVehiculosAsignados; //conductor.vehiculosAsignados.length;

        
        if(element.restriccionGeneral){
            conductor.diasHabilitados = element.restriccionGeneral.diaHabilitado;
            conductor.horariosHabilitados = element.restriccionGeneral.horarioHabilitado;
        }

        return conductor;
    }

    static cargarRestriccionesDiariasMensuales(c:Conductor, restricciones:any[]){

        const permiteComprar:any[] = restricciones.filter(r => r.tipoRestriccionId === 7);
        const montoMaximoDia:any[] = restricciones.filter(r => r.tipoRestriccionId === 1);
        const montoMaximoMes:any[] = restricciones.filter(r => r.tipoRestriccionId === 2);

        c.restriccionCombustible = {
            permiteComprar: permiteComprar.length > 0 ? permiteComprar[0].restriccionUsuarioValor == 1 : false,

            montoMaximoDia: montoMaximoDia.length > 0 && montoMaximoDia[0].restriccionUsuarioValor.length > 0 ? montoMaximoDia[0].restriccionUsuarioValor[0] : null,
            montoMaximoMes: montoMaximoMes.length > 0 && montoMaximoMes[0].restriccionUsuarioValor.length > 0 ? montoMaximoMes[0].restriccionUsuarioValor[0] : null,
            permiteComprarString: '',
            
            //no cargamos esta info por que no la tenemos
            traeMontosDisponibles: false,
            restriccionAcumuladoDia: null,
            restriccionAcumuladoMes: null,
            montoDisponibleDia: null,
            montoDisponibleMes: null
        }
        c.restriccionCombustible.permiteComprarString = c.restriccionCombustible.permiteComprar ? 'Puede' : 'No puede';

        // c.restriccionCombustible = new RestriccionCombustible();

        // //ESTADO
        // if(configuracionCombustible.montosMaximosEstado){
        //     let ce = configuracionCombustible.montosMaximosEstado != undefined ? configuracionCombustible.montosMaximosEstado == 1 : false;
        //     c.restriccionCombustible.montoMaximosEstado = ce;
        // }

        // //DIARIO
        // if(configuracionCombustible.usuarioCuentaMontoMaximoDia){
        //     let c1 = configuracionCombustible.usuarioCuentaMontoMaximoDia.filter(x => x.usuarioId === c.usuarioId);
        //     if(c1 && c1.length > 0)
        //         c.restriccionCombustible.montoMaximoDia = c1[0].montoMaximoDia && c1[0].montoMaximoDia.length > 0 ? c1[0].montoMaximoDia[0] : 0;
        //     else
        //         c.restriccionCombustible.montoMaximoDia = 0
        // }

        // //MENSUAL
        // if(configuracionCombustible.usuarioCuentaMontoMaximoMes){
        //     let c2 = configuracionCombustible.usuarioCuentaMontoMaximoMes.filter(x => x.usuarioId === c.usuarioId);
        //     if(c2 && c2.length > 0)
        //         c.restriccionCombustible.montoMaximoMes = c2[0].montoMaximoMes && c2[0].montoMaximoMes.length > 0 ? c2[0].montoMaximoMes[0] : 0;
        //     else
        //         c.restriccionCombustible.montoMaximoMes = 0
        // }

        // //COMPRAS
        // if(configuracionCombustible.usuarioCuentaPermiteComprar){
        //     let c3 = configuracionCombustible.usuarioCuentaPermiteComprar.filter(x => x.usuarioId === c.usuarioId);
        //     if(c3 && c3.length > 0){
        //         c.restriccionCombustible.permiteComprar = c3[0].permiteComprar && c3[0].permiteComprar.length > 0 ? c3[0].permiteComprar[0] === 1 : false;
        //         c.restriccionCombustible.permiteComprarString = c.restriccionCombustible.permiteComprar ? "Puede" : "No puede";
        //         c.vehiculosAsignadosCantidad = c3[0].cantidadVehiculosAsignados || 0;
        //         c.primerVehiculoAsignado = c3[0].primerVehiculoAsignado || '';
        //     }
        //     else
        //         c.restriccionCombustible.permiteComprar = false;
        // }

        return c;
        
    }

    static cargarRestriccionesDiariasMensualesFromDetail(c:Conductor, restriccionCombustible:any){

        c.restriccionCombustible = new RestriccionCombustible();

        c.restriccionCombustible.montoMaximosEstado = restriccionCombustible.montosMaximosEstado == 1;
        c.restriccionCombustible.montoMaximoDia = restriccionCombustible.montoMaximoDiaValor && restriccionCombustible.montoMaximoDiaValor.length > 0 ? restriccionCombustible.montoMaximoDiaValor[0] : 0;
        c.restriccionCombustible.montoMaximoMes = restriccionCombustible.montoMaximoMesValor && restriccionCombustible.montoMaximoMesValor.length > 0 ? restriccionCombustible.montoMaximoMesValor[0] : 0;
        c.restriccionCombustible.permiteComprar = restriccionCombustible.permiteComprarValor && restriccionCombustible.permiteComprarValor.length > 0 ? restriccionCombustible.permiteComprarValor[0] == 1 : false;
        c.restriccionCombustible.permiteComprarString = c.restriccionCombustible.permiteComprar ? "Puede" : "No puede";

        c.restriccionCombustible.traeMontosDisponibles = true;
        c.restriccionCombustible.montoDisponibleDia = restriccionCombustible.montoDisponibleDia;
        c.restriccionCombustible.montoDisponibleMes = restriccionCombustible.montoDisponibleMes;
        
        c.restriccionCombustible.restriccionAcumuladoDia = restriccionCombustible.restriccionCombustibleMontoAcumuladoDia;
        c.restriccionCombustible.restriccionAcumuladoMes = restriccionCombustible.restriccionCombustibleMontoAcumuladoMes;
        
        return c;
    }

    
}


export class RestriccionCombustible{
    permiteComprar = false;
    permiteComprarString = "No puede";

    montoMaximosEstado?: boolean;
    montoMaximoDia: number;
    montoMaximoMes: number;

    traeMontosDisponibles: boolean;
    montoDisponibleDia: number;
    montoDisponibleMes: number;  

    restriccionAcumuladoDia: number;
    restriccionAcumuladoMes: number; 

    constructor(){
        this.permiteComprar = false;
        this.permiteComprarString = "";

        this.montoMaximoDia = 0;
        this.montoMaximoMes = 0;

        this.traeMontosDisponibles = false;
        this.montoDisponibleDia = 0;
        this.montoDisponibleMes = 0;  
    }
}

export interface IModificarConductor{
    cuentaId:number, 
    usuarioId:number, 
    estado:number, 
    rolId:number, 

    configuracionCombustible?: IConfiguracionCombustible,
    restriccionesCombustible?: IRestriccionesCombustible
}

export interface IConfiguracionCombustible{
    estadoCombustible:boolean, 
    montoMaximoDiaValor: number, 
    montoMaximoMesValor: number,
    permiteComprar?: boolean;
}

export interface IRestriccionesCombustible{


    diasHabilitar:boolean, 
    diasHabilitados: number[],

    horarioHabilitar: boolean, 
    horariosHabilitados : number[],

    estacionHabilitar: boolean, 
    estacionesHabilitadas :number[]
}

export class Vehiculo{
    alias: string;
    estado: boolean;
    vehiculoId: number;
    tipo: number;
    patente: string;

    constructor(){
        this.alias = '';
        this.estado = false;
        this.vehiculoId = 0;
        this.tipo = 0;
        this.patente = '';
    }
}


// import { TipoConductorEstado } from '../globales/enumeradores';

// export class Conductor {
//     idConductor: number;
//     grupoId?: number;
//     nombreConductor: string;
//     apellidosConductor: string;
//     rutConductor: string
//     telefonoConductor?: string;
//     mailConductor: string;
//     estadoConductor: boolean;
//     restriccionConductor: RestriccionConsumoCombustible;
//     restriccionGeneral: RestriccionGeneral;
//     nombreCompleto: string;
//     rolId: number;
//     rolNombre: string;
//     usuarioCuentaConduce: number;
//     usuarioCuentaRindeGasto: number;
//     usuarioCuentaCreaVehiculo: number;
//     usuarioCuentaEligeDTE: number;
//     usuarioCuentaSeleccionado: number;
//     usuarioCuentaConsumeCombustible: number;
//     valInvitacionLicencia: boolean;
//     valInvitacionSelfie: boolean;
//     estadoCombustible: boolean;
//     limiteDiarioCombustible: number;
//     limiteMensualCombustible: number;
//     limiteDiarioCombustibleString: string;
//     limiteMensualCombustibleString: string;
//     maximoVentaMes: number;
//     porcentajeVentaMes: number;
//     sumaVentaMes: number;
//     maximoVentaDia: number;
//     porcentajeVentaDia: number;
//     sumaVentaDia: number;
//     disponibleDiaString: string;
//     disponibleMesString: string;
//     collapse: boolean;
//     editable: boolean;
//     fecha: Date;
//     fechaString: string;
//     nroDiasHabilitado: number;
//     nroEstacionesHabilitado: number;
//     vehiculosAsignados: VehiculoAsignado[];

//     constructor() {
//         this.grupoId = null;
//         this.idConductor = 0;
//         this.nombreConductor = "";
//         this.apellidosConductor = "";
//         this.rutConductor = "";
//         this.telefonoConductor = "";
//         this.estadoConductor = true;
//         this.restriccionConductor = new RestriccionConsumoCombustible();
//         this.restriccionGeneral = new RestriccionGeneral();
//         this.nombreCompleto = "";
//         this.rolId = 0;
//         this.usuarioCuentaConduce = 1;
//         this.usuarioCuentaRindeGasto = 1;
//         this.usuarioCuentaCreaVehiculo = 1;
//         this.usuarioCuentaEligeDTE = 1;
//         this.usuarioCuentaSeleccionado = 1;
//         this.valInvitacionLicencia = false;
//         this.valInvitacionSelfie = false;
//         this.estadoCombustible = true;
//         this.collapse = true;
//         this.editable = false;
//         this.fecha = new Date();
//         this.fecha.setDate(this.fecha.getDate() + 2);
//         this.fechaString = "";
//         this.maximoVentaMes = 0;
//         this.porcentajeVentaMes = 0;
//         this.sumaVentaMes = 0;
//         this.maximoVentaDia = 0;
//         this.porcentajeVentaDia = 0;
//         this.sumaVentaDia = 0;
//         this.disponibleDiaString = "";
//         this.disponibleMesString = "";
//         this.nroDiasHabilitado = 0;
//         this.nroEstacionesHabilitado = 0;
//         this.vehiculosAsignados = [];
//     }
// }

// export class RestriccionGeneral {
//     diaHabilitado: any[];
//     horarioHabilitado: any[];
//     estacionHabilitada: any[];
//     bloqueadoPorDiaNoHabilitado: number;
//     bloqueadoPorHorarioNoHabilitado: number;
//     bloqueadoPorEstacionNoHabilitada: number;


//     constructor() {
//         this.diaHabilitado = [];
//         this.horarioHabilitado = [];
//         this.estacionHabilitada = [];
//         this.bloqueadoPorDiaNoHabilitado = 0;
//         this.bloqueadoPorHorarioNoHabilitado = 0;
//         this.bloqueadoPorEstacionNoHabilitada = 0;
//     }
// }

// export class RestriccionConsumoCombustible {
//     estado: boolean;
//     montoMaximoDiaEstado: number;
//     montoMaximoDiaValor: number;
//     montoMaximoMesEstado: number
//     montoMaximoMesValor: number;


//     constructor() {
//         this.estado = true;
//         this.montoMaximoDiaEstado = 0
//         this.montoMaximoDiaValor = 0
//         this.montoMaximoMesEstado = 0
//         this.montoMaximoMesValor = 0
//     }
// }

// export class VehiculoAsignado{
//     vehiculoPatente: string;
//     vehiculoCuentaId: number;
//     vehiculoCuentaTipo: number;
//     vehiculoCuentaAlias: string;
//     vehiculoCuentaEstado: number;

//     constructor(){
//         this.vehiculoCuentaAlias = '';
//         this.vehiculoCuentaEstado = 0;
//         this.vehiculoCuentaId = 0;
//         this.vehiculoCuentaTipo = 0;
//         this.vehiculoPatente = '';
//     }
// }