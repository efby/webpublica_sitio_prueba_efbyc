import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'menu-editar',
  host: { 'class': 'full-screen-sidebar' },
  templateUrl: './menu-editar-component.component.html',
  styleUrls: ['./menu-editar-component.component.scss']
})
export class MenuEditarComponentComponent implements OnInit, OnChanges {

  @Input() titulo: string;
  @Input() subTitulo: string;
  @Input() isColor: boolean = true
  @Input() isVerificado: boolean = false
  @Input() isBadget: boolean = false
  @Input() estadoSwitch: boolean = false;

  @Output() fnClick = new EventEmitter<any>();
  @Output() fnSwitch = new EventEmitter<any>();


  isFnClick: boolean
  isfnSwitch: boolean = false;

  constructor() { }

  ngOnInit() {
    this.isFnClick = this.fnClick.observers.length > 0
    this.isfnSwitch = this.fnSwitch.observers.length > 0;
  }
  ngOnChanges(event) {

  }

}
