import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'btn-flotante',
  templateUrl: './btn-flotante.component.html',
  styleUrls: ['./btn-flotante.component.scss']
})
export class BtnFlotanteComponent implements OnInit, OnChanges {
  @Input() sizeTotal:number;
  @Input() sizeComparar:number;
  @Input() isLoad:boolean=false;
  @Output() next = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes:SimpleChanges){
    this.isLoad=changes.isLoad.currentValue || false
  }

}
