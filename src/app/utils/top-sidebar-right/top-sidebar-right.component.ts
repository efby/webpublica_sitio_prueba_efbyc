import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'top-sidebar-right',
  templateUrl: './top-sidebar-right.component.html',
  styleUrls: ['./top-sidebar-right.component.scss']
})
export class TopSidebarRightComponent implements OnInit {

  @Input() titulo:string;
  @Input() descripcion:string;
  @Input() subtitulo:string;
  @Output() fnClick= new EventEmitter<any>();
  @Output() fnClickBack= new EventEmitter<any>();

  isFnClickBack

  constructor() { }

  ngOnInit() {
    this.isFnClickBack = this.fnClickBack.observers.length > 0
  }

}
