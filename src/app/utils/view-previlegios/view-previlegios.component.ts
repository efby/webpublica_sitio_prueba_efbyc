import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'view-previlegios',
  templateUrl: './view-previlegios.component.html',
  styleUrls: ['./view-previlegios.component.scss']
})
export class ViewPrevilegiosComponent implements OnInit, OnChanges {

  @Input() roles: [any];
  @Input() rolId: any;

  rol: any
  constructor() { 
    console.log(this.rolId)
  }

  ngOnInit() {
    
    this.rol = this.roles.filter(a => { return a.rolId == this.rolId })[0]
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
    this.rolId = changes.rolId.currentValue
    this.rol = this.roles.filter(a => { return a.rolId == this.rolId })[0]
  }

}
