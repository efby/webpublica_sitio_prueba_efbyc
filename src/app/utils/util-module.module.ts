import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilModuleComponent } from './util-module.component';
import { MenuEditarComponentComponent } from './menu-editar-component/menu-editar-component.component';
import { TopSidebarRightComponent } from './top-sidebar-right/top-sidebar-right.component';
import { BtnFlotanteComponent } from './btn-flotante/btn-flotante.component';
import { BtnPlusComponent } from './btn-plus/btn-plus.component';
import { ViewPrevilegiosComponent } from './view-previlegios/view-previlegios.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import {MatBadgeModule} from '@angular/material/badge';


@NgModule({
  imports: [
    CommonModule,
    UiSwitchModule,
    MatBadgeModule

  ],
  declarations: [UtilModuleComponent,MenuEditarComponentComponent,TopSidebarRightComponent,BtnFlotanteComponent,BtnPlusComponent,ViewPrevilegiosComponent],
  exports: [MenuEditarComponentComponent,TopSidebarRightComponent,BtnFlotanteComponent,BtnPlusComponent,ViewPrevilegiosComponent],
  entryComponents:[MenuEditarComponentComponent,TopSidebarRightComponent,BtnFlotanteComponent,BtnPlusComponent,ViewPrevilegiosComponent]
  
})
export class UtilModuleModule { }
