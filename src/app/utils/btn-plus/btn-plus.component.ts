import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'btn-plus',
  templateUrl: './btn-plus.component.html',
  host: { 'class': 'full-screen-sidebar' },
  styleUrls: ['./btn-plus.component.scss']
})
export class BtnPlusComponent implements OnInit {
  @Input() titulo: string;
  @Input() isLineaDivisora:boolean=false;

  @Output() fnClick = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

}
