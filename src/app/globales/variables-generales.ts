import { NavegacionTipo, AnimacionTipo, TipoMedioPago, TipoUsuarioCuentaEstado, TipoMedioPagoEstado } from './enumeradores';
import { Vehiculos } from '../models/vehiculos';
import { Conductor } from '../models/conductores';
import { Facturas } from '../models/facturas';
import { Guias } from '../models/guias';
import { Boletas } from '../models/boletas';
import { Cuenta, Menu, TipoFormaPago, Saldo, CuentaCorriente, MedioPago, Tarjeta, UsuarioLogin, PeriodosFacturacion, Invitacion, CuentaSeleccionada } from './business-objects';

export class VariablesGenerales {
    authorizationToken: string = "";
    equipoSecret: string = "";
    esMobile: boolean = false;
    device: string;
    ingreso: Ingreso;
    home: Home;
    ultimaPagina: string = "";

    constructor() {
        this.ingreso = new Ingreso();
        this.home = new Home();
    }
}
//#region Ingreso

export class Ingreso {
    navegacionTipo: NavegacionTipo;
    paginaTipo: string = "";
    modalTipo: string = "";
    animacionTipo: AnimacionTipo;
    errorMessage: string = "";
    paginaAnterior: PaginaAnterior
    usuario: DatosUsuario;
    esFlujoCreacion: boolean;
    constructor() {
        this.paginaAnterior = new PaginaAnterior();
        this.usuario = new DatosUsuario();
        this.esFlujoCreacion = false;
    }
}

export class DatosUsuario {
    usuarioId: string = "";
    nombre: string = "";
    apellido: string = "";
    mail: string = "";
    telefonoId: string = "";
    equipoId: string = "";
    smsToken: string = "";
    smsValor: string = "";
    numeroDocumento: string = "";
    contrasena: string = "";
    pin1: string = "";
    pin2: string = "";
    telefonoCorto: string = "";
    aceptaTerminos: boolean = false;
}

export class PaginaAnterior {

    paginaAnteriorInicio: string = "";
    paginaAnteriorIngresaTelefonoId: string = "";
    paginaAnteriorIngresaCodigoSms: string = "";
    paginaAnteriorIngresaContrasena: string = "";
    paginaAnteriorIngresaRutIdRecuperacion: string = "";
    paginaAnteriorIngresaRutIdRegistro: string = "";
    paginaAnteriorIngresaNombre: string = "";
    paginaAnteriorCreaContrasena: string = "";
    paginaAnteriorModificaContrasena: string = "";
    paginaAnteriorIngresaPin: string = "";
    paginaAnteriorConfirmaPin: string = "";
    paginaAnteriorIngresoValidaPin: string = "";
    paginaAnteriorIngresaCorreoRecuperacion: string = "";
    paginaAnteriorIngresaCorreo: string = "";
    paginaAnteriorValidaPin
}
//#endregion

//#region Home
export class Home {
    cuentaSeleccionada: CuentaSeleccionada;
    //cuentas: Cuentas[];
    cuentas: Cuenta[];
    menu: Menu;
    maestros: Maestros;
    saldo: Saldo;

    rolPrivilegiosFront?: RolPrivilegiosFront;
    invitaciones?: Invitacion[];
    invitados?: Invitacion[];
    usuarioLogIn: UsuarioLogin;
    vehiculos?: Vehiculos[];
    //flotas?: Flotas[];
    conductores?: Conductor[];
    usuarios?: Conductor[];
    conductoresInvitados?: Conductor[];
    //grupos?: Grupos[];
    mediosPago?: MedioPago[];
    cuentasCorrientes?: CuentaCorriente[];
    tarjetas?: Tarjeta[];
    tarjetaCompartidaId?: string;
    facturasPendientesPago: number;
    facturas?: Facturas[];
    guias?: Guias[];
    boletas?: Boletas[]
    primerIngreso: boolean;

    montoTotalMes: number;
    tipoFormaPago?: TipoFormaPago[];
    nvoVehiculo?: Vehiculos[];
    equipos: Equipos;


    cuentaDatosDte: CuentaDatosDte;
    datosSaldo: TipoSaldo;
    tipoDocumento: TipoDocumento;
    notificaciones: Notificaciones[];
    movimientos?: Movimiento[];
    fuerzaCambioCuenta: boolean;


    constructor() {
        this.cuentaSeleccionada = new CuentaSeleccionada();
        this.cuentas = [];
        this.invitaciones = [];
        this.invitados = [];
        this.usuarioLogIn = new UsuarioLogin();
        this.saldo = new Saldo();

        this.vehiculos = [];
        this.nvoVehiculo = [];
        this.conductores = [];
        this.mediosPago = [];
        this.cuentasCorrientes = [];
        this.tarjetas = [];
        this.tarjetaCompartidaId = "";
        this.facturasPendientesPago = 0;
        this.facturas = [];
        this.guias = [];
        this.boletas = [];
        this.notificaciones = [];
        this.primerIngreso = false;
        this.montoTotalMes = 0;
        this.tipoFormaPago = [];
        this.equipos = new Equipos();
        this.maestros = new Maestros();
        this.menu = new Menu();
        this.cuentaDatosDte = new CuentaDatosDte();
        //this.datosSaldo = new TipoSaldo();
        this.tipoDocumento = new TipoDocumento();
        this.movimientos = [];
        this.fuerzaCambioCuenta = false;
    }
}

export class Movimiento {
    movimientoFechaCreacion: string;
    movimientoFechaCreacionString: string;
    movimientoId: number;
    movimientoMonto: number
    movimientoMontoString: string
    movimientoSaldoFinal: number;
    movimientoSaldoFinalString: string;
    tipoMovimientoNombre: string;

    constructor() {
        this.movimientoFechaCreacion = "";
        this.movimientoId = 0;
        this.movimientoMonto = 0;
        this.movimientoMontoString = "";
        this.movimientoSaldoFinal = 0;
        this.movimientoSaldoFinalString = "";
        this.tipoMovimientoNombre = "";
        this.movimientoFechaCreacionString = "";
    }
}

export class Notificaciones {
    tipoNotificacionId: number;
    tipoNotificacionNombre: string;
    tipoNotificacionDetalle: string;
    tipoNotificacionEstado: boolean;

    constructor() {
        this.tipoNotificacionId = 0;
        this.tipoNotificacionNombre = "";
        this.tipoNotificacionDetalle = "";
        this.tipoNotificacionEstado = false;
    }
}

export class CuentaDatosDte {
    dteActividadEconomica: string;
    dteDireccion: string;
    dteDistrito: string;
    dteEmail: string;
    dteFechaUpdate: Date;
    dteGiro: string;
    dteNombreFantasia: string;
    dteRazonSocial: string;
    dteRut: string;
    dteRestringeEleccion: string;
    dtePuedeFacturar: boolean;

    constructor() {
        this.dteActividadEconomica = "";
        this.dteDireccion = "";
        this.dteDistrito = "";
        this.dteEmail = "";
        this.dteFechaUpdate = new Date();
        this.dteGiro = "";
        this.dteNombreFantasia = "";
        this.dteRazonSocial = "";
        this.dteRut = "";
        this.dteRestringeEleccion = "";
        this.dtePuedeFacturar = false;
    }
}

export class Maestros {
    bancos: Bancos[];
    tipoSaldo: TipoSaldo[];
    tipoVehiculo: TipoVehiculo[];
    tipoDocumento: TipoDocumento[];
    tipoRestriccion: TipoRestriccion[];
    tipoCuentaBancaria: TipoCuentaBancaria[];
    periodosFacturacion: PeriodosFacturacion[];
    tiposNotificaciones: TiposNotificaciones[]
    tipoFormaPago: MedioPago[];
    roles: Roles[];

    constructor() {
        this.bancos = [];
        this.tipoSaldo = [];
        this.tipoVehiculo = [];
        this.tipoDocumento = [];
        this.tipoRestriccion = [];
        this.tipoCuentaBancaria = [];
        this.periodosFacturacion = [];
        this.roles = [];
        this.tipoFormaPago = [];
        this.tiposNotificaciones = [];
    }

}
export class TiposNotificaciones {
    tipoNotificacionDescripcion: String
    tipoNotificacionId: number
    tipoNotificacionNombre: String
    tipoNotificacionSeccion: String
}

export class VerEditar {
    verEditar: number;
    abonarYretirar: number
    constructor() {
        this.verEditar = 0;
        this.abonarYretirar = null;
    }
}
export class RolPrivilegiosFront {
    administradores: VerEditar
    conductores: VerEditar
    facturacion: VerEditar
    formasDePago: VerEditar
    vehiculos: VerEditar
    constructor() {
        this.administradores = new VerEditar()
        this.conductores = new VerEditar()
        this.facturacion = new VerEditar()
        this.formasDePago = new VerEditar()
        this.vehiculos = new VerEditar()
    }
}
export class Roles {
    rolId: number;
    rolNombre: string;
    rolDescripcion: string;
    rolPrivilegiosFront: RolPrivilegiosFront

    constructor() {
        this.rolId = 0;
        this.rolNombre = "";
        this.rolDescripcion = "";
        this.rolPrivilegiosFront = new RolPrivilegiosFront();
    }
}

export class Equipos {
    nuevaCuenta: boolean;
    nombreEquipo: string;
    constructor() {
        this.nuevaCuenta = false;
        this.nombreEquipo = "";
    }
}

export class Cuentas {
    cuentaId: number;
    cuentaEstado: boolean;
    cuentaNombre: string;
    cuentaDefault: boolean;
    tipoDocumento: number;
    cuentaFechaCreacion: Date;
    cuentaFechaCreacionString: string;
    rolId: number;

    constructor() {
        this.cuentaId = 0;
        this.cuentaEstado = false;
        this.cuentaNombre = "";
        this.cuentaDefault = true;
        this.tipoDocumento = 0;
        this.cuentaFechaCreacion = new Date();
        this.rolId = 0;
        this.cuentaFechaCreacionString = "";
    }
}


export class Flotas {
    flotaId: number;
    flotaCuentaId: number;
    flotaCuentaNombre: string;
    alertaActiva: number;
    cantidadVehiculos: number;
    estado: boolean;
    seleccionado: boolean;
    editable: boolean;
    vehiculosFlota?: Vehiculos[];

    constructor() {
        this.flotaId = 0;
        this.flotaCuentaId = 0;
        this.flotaCuentaNombre = "";
        this.alertaActiva = 0;
        this.cantidadVehiculos = 0;
        this.estado = true;
        this.seleccionado = false;
        this.vehiculosFlota = [];
        this.editable = false;
    }
}

export class Grupos {
    grupoId: number;
    grupoCuentaId: number;
    grupoCuentaNombre: string;
    alertaActiva: number;
    cantidadConductores: number;
    estado: boolean;
    seleccionado: boolean;
    editable: boolean;
    conductoresGrupo?: Conductor[];

    constructor() {
        this.grupoId = 0;
        this.grupoCuentaId = 0;
        this.grupoCuentaNombre = "";
        this.alertaActiva = 0;
        this.cantidadConductores = 0;
        this.estado = true;
        this.seleccionado = false;
        this.conductoresGrupo = [];
        this.editable = false;
    }
}

// export class MedioPago {
//     formaPagoTipo: string;
//     formaPagoId: number;
//     formaPagoActiva: boolean;
//     formaPagoDescripcion: string;

//     constructor() {
//         this.formaPagoTipo = "";
//         this.formaPagoId = 0;
//         this.formaPagoActiva = false;
//         this.formaPagoDescripcion = "";
//     }
// }

// export class Tarjetas {
//     idInscripcion: string;
//     tipoTarjetaCredito: string;
//     ultimos4Digitos: string;

//     constructor() {
//         this.idInscripcion = "";
//         this.tipoTarjetaCredito = "";
//         this.ultimos4Digitos = "";
//     }

// }

//migrated
// export class CuentaCorriente {
//     bancoId: string;
//     bancoNombre: string;
//     cuentaCorrienteRut: string;
//     tipoCuentaBancariaId: number;
//     cuentaCorrienteNumero: number;
//     tipoCuentaBancariaNombre: string;
//     cuentaCorrienteNombreTitular: string;

//     constructor() {
//         this.bancoId = "";
//         this.bancoNombre = "";
//         this.cuentaCorrienteRut = "";
//         this.tipoCuentaBancariaId = 0;
//         this.cuentaCorrienteNumero = 0;
//         this.tipoCuentaBancariaNombre = "";
//         this.cuentaCorrienteNombreTitular = "";
//     }
// }

//migrated
// export class Menu {
//     titulo: string;
//     subTitulo: string;
//     pagarEstado: number;
//     abonarEstado: number;
//     misCargasEstado: number;
//     datosDeFacturacionEstado: number;
//     formasDePagoEstado: number;
//     misVehiculosEstado: number;
//     misDocumentosEstado: number;
//     misConductoresEstado: number;
//     invitarConductoresEstado: number;

//     constructor() {
//         this.titulo = "";
//         this.subTitulo = "";
//         this.pagarEstado = 0;
//         this.abonarEstado = 0;
//         this.misCargasEstado = 0;
//         this.datosDeFacturacionEstado = 0;
//         this.formasDePagoEstado = 0;
//         this.misVehiculosEstado = 0;
//         this.misDocumentosEstado = 0;
//         this.misConductoresEstado = 0;
//         this.invitarConductoresEstado = 0;
//     }
// }

export class Bancos {
    bancoId: number;
    bancoNombre: string;

    constructor() {
        this.bancoId = 0;
        this.bancoNombre = "";
    }
}

// export class TipoFormaPago {
//     tipoFormaPagoId: number;
//     tipoFormaPagoNombre: string;
//     tipoFormaPagoDescripcion: string;

//     constructor() {
//         this.tipoFormaPagoId = 0;
//         this.tipoFormaPagoNombre = "";
//         this.tipoFormaPagoDescripcion = "";
//     }
// }

export class TipoSaldo {
    tipoSaldoId: number;
    tipoSaldoNombre: string;
    lineaCredito: number;
    lineaCreditoString: string;
    saldo: number;
    saldoString: string;
    saldoFinal: number;
    saldoFinalString: string;


    constructor() {
        this.tipoSaldoId = 0;
        this.tipoSaldoNombre = "";
        this.lineaCredito = 0;
        this.lineaCreditoString = "";
        this.saldo = 0;
        this.saldoString = "";
        this.saldoFinal = 0;
        this.saldoFinalString = "";
    }
}

export class TipoVehiculo {
    tipoVehiculoId: number;
    tipoVehiculoNombre: string;

    constructor() {
        this.tipoVehiculoId = 0;
        this.tipoVehiculoNombre = "";
    }
}

export class TipoDocumento {
    tipoDocumentoId: number;
    tipoDocumentoNombre: string;

    constructor() {
        this.tipoDocumentoId = 0;
        this.tipoDocumentoNombre = "";
    }
}

export class TipoRestriccion {
    tipoRestriccionId: number;
    tipoDocumentoNombre: string;

    constructor() {
        this.tipoRestriccionId = 0;
        this.tipoDocumentoNombre = "";
    }
}

export class TipoCuentaBancaria {
    tipoCuentaBancariaId: number;
    tipoCuentaBancariaNombre: string;

    constructor() {
        this.tipoCuentaBancariaId = 0;
        this.tipoCuentaBancariaNombre = "";
    }
}

//#endregion
