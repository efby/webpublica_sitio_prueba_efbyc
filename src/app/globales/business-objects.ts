import { TipoUsuarioCuentaEstado } from './enumeradores';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';

export interface IPagination{
    currentPage: number,
    pages: number,
    totalRows: number,

    positionFinal: number;
    pagesSize: number; 

    rows: number;
}

export class Cuenta {
    
    rolId: number;
    rolNombre: string;
    cuentaId: number;
    cuentaNombre: string;
    usuarioCuentaEstado: boolean;
    usuarioCuentaFechaCreacion: string;
    gastoTotalMes: number;
    cuentaDefault:any
    
    constructor() {
        this.rolId = 0;
        this.cuentaId = 0;
        this.cuentaNombre = "";
        this.usuarioCuentaEstado = false;
        this.usuarioCuentaFechaCreacion = "";
    }
}

//root
export class CuentaSeleccionada extends Cuenta{
    //gastoTotalMes: number;
    //usuarioCuentaRolId: number;
    
    tiposFormaPago: TipoFormaPago[];
    saldo: Saldo;
    indicadores:Indicadores;
    
    constructor(){
        super();

        this.indicadores = new Indicadores();
    }
}

export class Indicadores{
    gastoTotalMes:number;
    porcentajeAumentoGastoMes:number;
    gastoTotalMesAnterior:number;

    constructor(){
        this.gastoTotalMes = 0;
        this.porcentajeAumentoGastoMes = 0;
        this.gastoTotalMesAnterior = 0;
    }
}



export class UsuarioLogin {
    usuarioId: string;
    usuarioNombre: string;
    usuarioApellido: string;
    usuarioRut: string;
    usuarioTelefono: string;
    usuarioEmail: string;
    usuarioCuentaEstado: TipoUsuarioCuentaEstado;
    rolId: number;
    rolNombre: string;
    invitaciones:InvitacionRecibida[];
    solicitaTerminos: number;
    terminosMensaje: string;
    
    constructor() {
        this.usuarioId = "";
        this.usuarioNombre = "";
        this.usuarioApellido = "";
        this.usuarioRut = "";
        this.usuarioTelefono = "";
        this.usuarioEmail = "";
        this.usuarioCuentaEstado = TipoUsuarioCuentaEstado.ACTIVO;
        this.rolId = 1;
        this.rolNombre = "";
        this.invitaciones = [];
        this.solicitaTerminos = 0;
        this.terminosMensaje = '';
    }
}


export class Menu{
    pagarEstado: boolean;
    abonarEstado: boolean;
    historialEstado: boolean;
    vehiculosEstado: boolean;
    documentosEstado: boolean;
    conductoresEstado: boolean;
    facturacionEstado: boolean;
    formasDePagoEstado: boolean;
    notificacionesEstado: boolean;
    tieneInvitaciones:boolean;
    tieneNotificiones:boolean
    
    constructor(){
        this.pagarEstado =  false;
        this.abonarEstado = false;
        this.historialEstado = false;
        this.vehiculosEstado = false;
        this.documentosEstado = false;
        this.conductoresEstado = false;
        this.facturacionEstado = false;
        this.formasDePagoEstado = false;
        this.notificacionesEstado = false;
        this.tieneInvitaciones=false;
        this.tieneNotificiones=false;
        
    }
}

export class Saldo{
    saldo: number;
    saldoFinal: number;
    lineaCredito: number;
    
    constructor(){
        this.saldo = 0;
        this.saldoFinal = 0;
        this.lineaCredito = 0;
    }
}

export class TipoFormaPago {
    tipoFormaPagoId: number;
    tipoFormaPagoNombre: string;
    tipoFormaPagoDescripcion: string;
    
    constructor() {
        this.tipoFormaPagoId = 0;
        this.tipoFormaPagoNombre = "";
        this.tipoFormaPagoDescripcion = "";
    }
}


export class MedioPago {
    formaPagoTipo: string;
    formaPagoId: number;
    formaPagoActiva: boolean;
    formaPagoDescripcion: string;
    
    constructor() {
        this.formaPagoTipo = "";
        this.formaPagoId = 0;
        this.formaPagoActiva = false;
        this.formaPagoDescripcion = "";
    }
}

export class Tarjeta {
    idInscripcion: string;
    tipoTarjetaCredito: string;
    ultimos4Digitos: string;
    
    constructor() {
        this.idInscripcion = "";
        this.tipoTarjetaCredito = "";
        this.ultimos4Digitos = "";
    }
    
}

export class CuentaCorriente {
    inscripcionId: number;
    bancoId: string;
    bancoNombre: string;
    cuentaCorrienteRut: string;
    tipoCuentaBancariaId: number;
    cuentaCorrienteNumero: number;
    tipoCuentaBancariaNombre: string;
    cuentaCorrienteNombreTitular: string;
    
    constructor() {
        this.bancoId = "";
        this.bancoNombre = "";
        this.cuentaCorrienteRut = "";
        this.tipoCuentaBancariaId = 0;
        this.cuentaCorrienteNumero = 0;
        this.tipoCuentaBancariaNombre = "";
        this.cuentaCorrienteNombreTitular = "";
    }
}

export class DatosFacturacion{
    dteRut: string;
    dteGiro: string;
    dteEmail: string;
    dteDistrito: string;
    comuna:string
    dteDireccion: string;
    dteFechaUpdate: string;
    dteRazonSocial:string;
    dtePuedeFacturar: boolean;
    dteNombreFantasia: string;
    dteRestringeEleccion: number;
    dteActividadEconomica: string;
    dtePeriodoFacturacion: number;
    dteNumeroSerie: string;
    dteDeclaracion: boolean;
    
    constructor(){
        this.dteActividadEconomica = "";
        this.dteDireccion = "";
        this.dteDistrito = "";
        this.dteEmail = "";
        this.dteFechaUpdate = null;
        this.dteGiro = "";
        this.dteNombreFantasia = "";
        this.dtePuedeFacturar = false;
        this.dteRazonSocial = "";
        this.comuna="";
        
        this.dteRut = "";

        this.dteDeclaracion = false,
        this.dteNumeroSerie = '';

        this.dtePeriodoFacturacion = null;
        this.dteRestringeEleccion = null;
        
    }
}

export class PeriodosFacturacion {
    periodoFacturacionId: number;
    periodoFacturacionNombre: string;
    
    constructor() {
        this.periodoFacturacionId = 0;
        this.periodoFacturacionNombre = "";
    }
}

export class Perfil{
    nombre:string;
    apellido:string;
    rut:string;
    celular:string;
    email:string;
}


export class InvitacionValidaciones {
    invitacionNombre: string;
    invitacionApellido: string;
    invitacionSolicitaFoto: number;
    aceptaFoto: boolean;
    invitacionSolicitaLicencia: number
    aceptaLicencia: boolean;
    
    constructor() {
        this.invitacionNombre = "";
        this.invitacionApellido = "";
        this.invitacionSolicitaFoto = 0;
        this.aceptaFoto = false;
        this.invitacionSolicitaLicencia = 0;
        this.aceptaLicencia = false;
    }
}

export class Invitacion {
    invitacionId: number;
    invitacionEstado: string;
    invitacionActivate: boolean;
    invitacionNombreCuenta: string;
    invitacionCuentaId: number;
    invitacionFechaCaducidad?: string;
    invitacionTelefonoInvitadoId?: string;
    invitacionValidaciones?: InvitacionValidaciones;
    invitacionEstadoInvitado?: string;
    invitacionUsuarioCreadorNombre?: string;
    invitacionRol?: string;
    invitacionRolId: number;

    activaCombustible: boolean;
    montosMaximosEstado: boolean;
    combustibleMontoDiario?: number;
    combustibleMontoMensual?: number;

    activaAlimentacion: boolean;
    alimentacionMontoDiario: number;
    alimentacionMontoMensual: number;
    vehiculosAsignados: number[]
    
    constructor() {
        this.invitacionId = 0;
        this.invitacionEstado = "";
        this.invitacionActivate = false;
        this.invitacionNombreCuenta = "";
        this.invitacionCuentaId = 0;
        this.invitacionFechaCaducidad = "";
        this.invitacionTelefonoInvitadoId = "";
        this.invitacionEstadoInvitado = "";
        this.invitacionUsuarioCreadorNombre = "";
        this.invitacionRol = "";
        this.invitacionRolId = 0;
        this.activaCombustible = false;
        this.combustibleMontoDiario;
        this.combustibleMontoMensual;
        this.activaAlimentacion = false;
        this.alimentacionMontoDiario = 0;
        this.alimentacionMontoMensual = 0;
        this.invitacionValidaciones = new InvitacionValidaciones();
        
    }
}

export class InvitacionRecibida{
    cuentaId: number;
    invitacionId: number;
    invitacionEstadoId: string;
    invitacionCuentaNombre: string;
    invitacionFechaCaducidad: string;
    invitacionTelefonoInvitadoId: number;
    invitacionUsuarioCreadorNombre: string;
    
    constructor(){
        this.cuentaId = 0;
        this.invitacionCuentaNombre ="";
        this.invitacionEstadoId = "";
        this.invitacionFechaCaducidad = "";
        this.invitacionId = 0;
        this.invitacionTelefonoInvitadoId = 0;
        this.invitacionUsuarioCreadorNombre = "";
    }
}

export class Transaccion{
    comercioId: number;
    cuentaId: number;
    cuentaNombre: string;
    documentoUrl: string;
    medioDePago: string;
    movimientoId: string;
    tipoDocumentoId: number;
    tipoDocumentoNombre: string;
    tipoFormaPagoId: number;
    tipoFormaPagoNombre: string;
    usuarioNombreApellido: string;
    usuarioRut: string;
    vehiculoPatente: string;
    ventaDocumentoFolio: string;
    ventaFechaCreacion: string;
    ventaHoraCreacion: string;
    ventaId: number;
    ventaPagoTotal: number;
    ventaSitioDireccion: string;
    ventaSitioNombre: string;
    tieneDocumento: boolean;
    ventaDocumentoId: number;

    siiMedioPagoId: string;
    ventaAtendedorNombre: string;
    ventaCalificacionComentario: string;
    ventaCalificacionNota: number;
    ventaDescuentoTotal: number;
    ventaMontoTotal: number;
    ventaNumeroTrxComercio: number;
    vehiculoOdometro: number;
    ventaEstacion: string;
    ventaEstadoId: string;
    ventaFecha: string;
    ventaHora: string;
    ventaNumeroProceso: number;
    facturaNumeroDocumento: number;

    detalle: TransaccionDetalle[];

    constructor(){
        this.comercioId = 0;
        this.cuentaId = 0;
        this.cuentaNombre = "";
        this.documentoUrl = "";
        this.medioDePago = "";
        this.movimientoId = "";
        this.tipoDocumentoId = 0;
        this.tipoDocumentoNombre = "";
        this.tipoFormaPagoId = 0;
        this.tipoFormaPagoNombre = "";
        this.usuarioNombreApellido = "";
        this.usuarioRut = "";
        this.vehiculoPatente = "";
        this.ventaDocumentoFolio = "";
        this.ventaFechaCreacion = "";
        this.ventaHoraCreacion = "";
        this.ventaId = 0;
        this.ventaPagoTotal = 0;
        this.ventaSitioDireccion = "";
        this.ventaSitioNombre = "";
        this.tieneDocumento = false;
        this.ventaDocumentoId = 0;
        this.facturaNumeroDocumento = 0;

        this.siiMedioPagoId = "";
        this.ventaAtendedorNombre = "";
        this.ventaCalificacionComentario = "";
        this.ventaCalificacionNota = 0;
        this.ventaDescuentoTotal = 0;
        this.ventaMontoTotal = 0;
        this.ventaNumeroTrxComercio = 0;
        this.vehiculoOdometro = 0;
        this.ventaEstacion = '';
        this.ventaEstadoId = '';
        this.ventaFecha = '';
        this.ventaHora = '';
        this.ventaNumeroProceso = 0;
        this.detalle = [];
        
    }
}

export class TransaccionDetalle{

    lineaId:string;
    productoId:string;
    productoNombre:string;
    lineaCantidad:string;
    lineaUnidadMedida:string;
    lineaPrecioUnitario:string;

    constructor(){
        this.lineaCantidad = '';
        this.lineaId = '';
        this.lineaPrecioUnitario = '';
        this.lineaUnidadMedida = '';
        this.productoId = '';
        this.productoNombre = '';

    }
}

export interface TableDataCsv{
    header: string[],
    rows: any[][]
}

export class MedioPagoMovimiento{
    movimientoId: number;
    movimientoMonto: number;
    movimientoSaldoFinal: number;
    tipoMovimientoNombre: string;
    movimientoFechaCreacion: string;

    constructor(){
        this.movimientoFechaCreacion = "";
        this.movimientoId = 0;
        this.movimientoMonto = 0;
        this.movimientoSaldoFinal = 0;
        this.tipoMovimientoNombre = "";
    }
}


export class Vehiculo{
    vehiculoId: number;
    anio: string;
    tipo: number;
    tipoString: string;
    alias: string;
    marca: string;
    estado: boolean;
    modelo: string;
    patente: string;
    fechaCreacion?: string;

    requireOdometro: boolean;
    frecuenciaOdometro: number;

    constructor(){
        this.vehiculoId = 0;
        this.anio = '';
        this.tipo = null;
        this.alias = '';
        this.marca = '';
        this.modelo = '';
        this.patente = '';
        this.fechaCreacion = '';
        this.requireOdometro = false;
        this.frecuenciaOdometro = 0;
    }
}


export interface IInvitacionConductor{
    id:string;
    nombre:string;
    apellido:string;
    telefono:string;
    combustible:IConfiguracionLimites;
    alimentacion:IConfiguracionLimites;
    vehiculos: IVehiculosInvitacion[]
}

export interface IVehiculosInvitacion{
    vehiculoId:number;
    patente:string;
    alias: string;
}

export interface IConfiguracionLimites{
    title: string;
    applyChanges: boolean;
    permiteComprar: boolean;
    diario:string;
    mensual:string;
}

export interface IAbonoPost{
    cuentaId: number;
    idTarjeta: string;
    monto: number;
}

            
