export class Constantes {
    UrlCopec = "https://api.copecempresas.com/";
    // UrlCopec = "https://api.sitescopec.com/";
    UrlCopecIngreso = "https://api.copecseguridad.com/EMP1/";
    Ambiente = "QA";
    authorizationtokenInit: string = "13476833cad30bf9c07fc32e0c3a6ba797f284b6";
    secretA = "1234567890123456";

    ICON_COMBUSTIBLE = "fa-gas-pump";
    ICON_ALIMENTACION = "fa-utensils";

    TIPOTOKENEQUIPO = "TIPOTOKEN_EQUIPO";
    TIPOTOKENPIN = "TIPOTOKEN_PIN";
    TIPOTOKENUSUARIO = "TIPOTOKENUSUARIO";

    //#region Api Ingreso
    COPEC_INGRESO_EQUIPO_CREAR_NUEVO_QA = "QA/ingreso/equipo";
    COPEC_INGRESO_EQUIPO_CREAR_NUEVO_PR = "PR/ingreso/equipo";

    COPEC_INGRESO_EQUIPO_CREAR_QA = "IN1/QA/ingreso/equipo/crear";
    COPEC_INGRESO_EQUIPO_CREAR_PR = "IN1/PR/ingreso/equipo/crear";

    COPEC_INGRESO_TELEFONO_CONSULTAR_ESTADO_QA = "IN1/QA/ingreso/telefono/consultarestado";
    COPEC_INGRESO_TELEFONO_CONSULTAR_ESTADO_PR = "IN1/PR/ingreso/telefono/consultarestado";

    COPEC_INGRESO_TELEFONO_CREAR_NUEVO_QA = "QA/ingreso/telefono";
    COPEC_INGRESO_TELEFONO_CREAR_NUEVO_PR = "PR/ingreso/telefono";

    COPEC_INGRESO_TELEFONO_CREAR_QA = "IN1/QA/ingreso/telefono/crear";
    COPEC_INGRESO_TELEFONO_CREAR_PR = "IN1/PR/ingreso/telefono/crear";

    COPEC_INGRESO_TELEFONO_REENVIAR_QA = "IN1/QA/ingreso/telefono/reenviar";
    COPEC_INGRESO_TELEFONO_REENVIAR_PR = "IN1/PR/ingreso/telefono/reenviar";

    COPEC_INGRESO_TELEFONO_VALIDAR_NUEVO_QA = "QA/ingreso/smstelefono";
    COPEC_INGRESO_TELEFONO_VALIDAR_NUEVO_PR = "PR/ingreso/smstelefono";

    COPEC_INGRESO_MAIL_NUEVO_QA = "QA/ingreso/mail";
    COPEC_INGRESO_MAIL_NUEVO_PR = "PR/ingreso/mail";

    COPEC_INGRESO_TELEFONO_VALIDAR_QA = "IN1/QA/ingreso/telefono/validar";
    COPEC_INGRESO_TELEFONO_VALIDAR_PR = "IN1/PR/ingreso/telefono/validar";

    COPEC_INGRESO_USUARIO_CONSULTAR_ESTADO_QA = "IN1/QA/ingreso/usuario/consultarestado";
    COPEC_INGRESO_USUARIO_CONSULTAR_ESTADO_PR = "IN1/PR/ingreso/usuario/consultarestado";

    COPEC_INGRESO_USUARIO_CONSULTAR_ESTADO_NUEVO_QA = "QA/ingreso/rut";
    COPEC_INGRESO_USUARIO_CONSULTAR_ESTADO_NUEVO_PR = "PR/ingreso/rut";

    COPEC_INGRESO_USUARIO_CREAR_QA = "IN1/QA/ingreso/usuario/crear";
    COPEC_INGRESO_USUARIO_CREAR_PR = "IN1/PR/ingreso/usuario/crear";

    COPEC_INGRESO_USUARIO_CREAR_NUEVO_QA = "QA/ingreso/usuario";
    COPEC_INGRESO_USUARIO_CREAR_NUEVO_PR = "PR/ingreso/usuario";


    COPEC_INGRESO_USUARIO_MODIFICAR_QA = "IN1/QA/ingreso/usuario/modificar";
    COPEC_INGRESO_USUARIO_MODIFICAR_PR = "IN1/PR/ingreso/usuario/modificar";

    COPEC_INGRESO_USUARIO_VALIDAR_QA = "IN1/QA/ingreso/usuario/validar";
    COPEC_INGRESO_USUARIO_VALIDAR_PR = "IN1/PR/ingreso/usuario/validar";

    COPEC_INGRESO_USUARIO_VALIDAR_LOGIN_QA = "IN1/QA/ingreso/usuario/validarlogin";
    COPEC_INGRESO_USUARIO_VALIDAR_LOGIN_PR = "IN1/PR/ingreso/usuario/validarlogin";

    COPEC_INGRESO_PIN_CREAR_QA = "QA/ingreso/pin/crear";
    COPEC_INGRESO_PIN_CREAR_PR = "PR/ingreso/pin/crear";

    COPEC_INGRESO_PIN_VALIDAR_QA = "QA/ingreso/pin/validar";
    COPEC_INGRESO_PIN_VALIDAR_PR = "PR/ingreso/pin/validar";

    COPEC_INGRESO_TOKEN_POST_QA = "QA/ingreso/token";
    COPEC_INGRESO_TOKEN_POST_PR = "PR/ingreso/token"

    COPEC_INGRESO_TOKENMAIL_POST_QA = "QA/ingreso/tokenmail";
    COPEC_INGRESO_TOKENMAIL_POST_PR = "PR/ingreso/tokenmail";

    COPEC_INGRESO_TOKENTELEFONO_POST_QA = "QA/ingreso/tokentelefono";
    COPEC_INGRESO_TOKENTELEFONO_POST_PR = "PR/ingreso/tokentelefono";

    COPEC_INGRESO_TOKENPASS_POST_QA = "QA/ingreso/contrasena";
    COPEC_INGRESO_TOKENPASS_POST_PR = "PR/ingreso/contrasena";

    
    //#endregion

    //#region Api Home

    COPEC_EMPRESAS_ORGANIZACION_CREAR_QA = "EM1/QA/empresas/organizacion/crear";
    COPEC_EMPRESAS_ORGANIZACION_CREAR_PR = "EM1/PR/empresas/organizacion/crear";

    COPEC_EMPRESAS_USUARIO_OBTENER_QA = "EM1/QA/empresas/usuario/obtenerweb";
    COPEC_EMPRESAS_USUARIO_OBTENER_PR = "EM1/PR/empresas/usuario/obtenerweb";

    COPEC_EMPRESAS_VENTA_ESCANEA_QR_QA = "EM1/QA/empresas/venta/escaneaqr";
    COPEC_EMPRESAS_VENTA_ESCANEA_QR_PR = "EM1/PR/empresas/venta/escaneaqr";

    COPEC_EMPRESAS_VENTA_DEFINE_FORMA_PAGO_QA = "EM1/QA/empresas/venta/defineformapago";
    COPEC_EMPRESAS_VENTA_DEFINE_FORMA_PAGO_PR = "EM1/PR/empresas/venta/defineformapago";

    COPEC_EMPRESAS_VENTA_CONSULTA_TELEFONO_QA = "EM1/QA/empresas/venta/consultatelefono";
    COPEC_EMPRESAS_VENTA_CONSULTA_TELEFONO_PR = "EM1/PR/empresas/venta/consultatelefono";

    COPEC_EMPRESAS_CREA_CUENTA_CORRIENTE_QA = "EM1/QA/empresas/cuenta/asociarcuentacorriente";
    COPEC_EMPRESAS_CREA_CUENTA_CORRIENTE_PR = "EM1/PR/empresas/cuenta/asociarcuentacorriente";

    COPEC_EMPRESAS_CREAR_CUENTA_QA = "EM1/QA/empresas/cuenta/crear";
    COPEC_EMPRESAS_CREAR_CUENTA_PR = "EM1/PR/empresas/cuenta/crear";

    COPEC_EMPRESAS_MODIFICAR_CUENTA_QA = "EM1/QA/empresas/cuenta/modificar";
    COPEC_EMPRESAS_MODIFICAR_CUENTA_PR = "EM1/PR/empresas/cuenta/modificar";

    COPEC_EMPRESAS_CREAR_DATOS_FACTURACION_QA = "EM1/QA/empresas/cuenta/creardatosfacturacion";
    COPEC_EMPRESAS_CREAR_DATOS_FACTURACION_PR = "EM1/PR/empresas/cuenta/creardatosfacturacion";

    COPEC_EMPRESAS_CONSULTAR_DATOS_FACTURACION_QA = "EM1/QA/empresas/cuenta/consultardatosfacturacion";
    COPEC_EMPRESAS_CONSULTAR_DATOS_FACTURACION_PR = "EM1/PR/empresas/cuenta/consultardatosfacturacion";

    COPEC_EMPRESAS_CONSULTAR_INVITACIONES_QA = "EM1/QA/empresas/cuenta/consultarinvitaciones";
    COPEC_EMPRESAS_CONSULTAR_INVITACIONES_PR = "EM1/PR/empresas/cuenta/consultarinvitaciones";

    COPEC_EMPRESAS_CUENTA_MODIFICAR_QA = "EM1/QA/empresas/usuario/cambiarcuenta";
    COPEC_EMPRESAS_CUENTA_MODIFICAR_PR = "EM1/PR/empresas/usuario/cambiarcuenta";

    COPEC_EMPRESAS_CUENTA_INVITAR_QA = "EM1/QA/empresas/cuenta/crearinvitaciones";
    COPEC_EMPRESAS_CUENTA_INVITAR_PR = "EM1/PR/empresas/cuenta/crearinvitaciones";

    COPEC_EMPRESAS_USUARIOCUENTA_OBTENER_QA = "EM1/QA/empresas/cuenta/consultardetalleusuariocuenta";
    COPEC_EMPRESAS_USUARIOCUENTA_OBTENER_PR = "EM1/PR/empresas/cuenta/consultardetalleusuariocuenta";

    COPEC_EMPRESAS_USUARIO_INVITACIONES_OBTENER_QA = "EM1/QA/empresas/cuenta/consultarusuarios";
    COPEC_EMPRESAS_USUARIO_INVITACIONES_OBTENER_PR = "EM1/PR/empresas/cuenta/consultarusuarios";

    COPEC_EMPRESAS_USUARIO_CONDUCTORES_OBTENER_QA = "EM1/QA/empresas/cuenta/consultarusuariosweb";
    COPEC_EMPRESAS_USUARIO_CONDUCTORES_OBTENER_PR = "EM1/PR/empresas/cuenta/consultarusuariosweb";

    COPEC_EMPRESAS_USUARIO_RESTRICCIONES_OBTENER_QA = "EM1/QA/empresas/cuenta/consultarrestriccionesusuarios";
    COPEC_EMPRESAS_USUARIO_RESTRICCIONES_OBTENER_PR = "EM1/PR/empresas/cuenta/consultarrestriccionesusuarios";

    COPEC_EMPRESAS_USUARIO_VALIDA_INVITACION_QA = "EM1/QA/empresas/usuariocuenta/aceptarinvitacion";
    COPEC_EMPRESAS_USUARIO_VALIDA_INVITACION_PR = "EM1/PR/empresas/usuariocuenta/aceptarinvitacion";

    COPEC_EMPRESAS_USUARIO_CANCELA_INVITACION_QA = "EM1/QA/empresas/usuariocuenta/cancelarinvitacion";
    COPEC_EMPRESAS_USUARIO_CANCELA_INVITACION_PR = "EM1/PR/empresas/usuariocuenta/cancelarinvitacion";

    COPEC_EMPRESAS_USUARIO_AUTORIZA_INVITACION_QA = "EM1/QA/empresas/usuariocuenta/autorizarinvitacion";
    COPEC_EMPRESAS_USUARIO_AUTORIZA_INVITACION_PR = "EM1/PR/empresas/usuariocuenta/autorizarinvitacion";

    COPEC_EMPRESAS_USUARIO_MODIFICAR_QA = "EM1/QA/empresas/cuenta/modificarusuariocuenta";
    COPEC_EMPRESAS_USUARIO_MODIFICAR_PR = "EM1/PR/empresas/cuenta/modificarusuariocuenta";

    COPEC_EMPRESAS_USUARIO_CONSULTAR_FACTURAS_QA = "EM1/QA/empresas/cuenta/consultarfacturas";
    COPEC_EMPRESAS_USUARIO_CONSULTAR_FACTURAS_PR = "EM1/PR/empresas/cuenta/consultarfacturas";

    COPEC_EMPRESAS_USUARIO_CONSULTAR_DETALLE_FACTURAS_QA = "EM1/QA/empresas/cuenta/consultardetallefactura";
    COPEC_EMPRESAS_USUARIO_CONSULTAR_DETALLE_FACTURAS_PR = "EM1/PR/empresas/cuenta/consultardetallefactura";

    COPEC_EMPRESAS_USUARIO_CONSULTAR_TRANSACCIONES_QA = "EM1/QA/empresas/cuenta/consultartransacciones";
    COPEC_EMPRESAS_USUARIO_CONSULTAR_TRANSACCIONES_PR = "EM1/PR/empresas/cuenta/consultartransacciones";

    COPEC_EMPRESAS_USUARIO_CONSULTAR_DETALLE_TRANSACCIONES_QA = "EM1/QA/empresas/cuenta/consultardetalletransaccion";
    COPEC_EMPRESAS_USUARIO_CONSULTAR_DETALLE_TRANSACCIONES_PR = "EM1/PR/empresas/cuenta/consultardetalletransaccion";

    COPEC_EMPRESAS_USUARIO_CONSULTAR_MOVIMIENTOS_QA = "EM1/QA/empresas/cuenta/consultarmovimientos";
    COPEC_EMPRESAS_USUARIO_CONSULTAR_MOVIMIENTOS_PR = "EM1/PR/empresas/cuenta/consultarmovimientos";

    COPEC_EMPRESAS_VEHICULO_CREAR_QA = "EM1/QA/empresas/vehiculocuenta/crear";
    COPEC_EMPRESAS_VEHICULO_CREAR_PR = "EM1/PR/empresas/vehiculocuenta/crear";

    COPEC_EMPRESAS_VEHICULO_MODIFICAR_QA = "EM1/QA/empresas/cuenta/modificarvehiculocuenta";
    COPEC_EMPRESAS_VEHICULO_MODIFICAR_PR = "EM1/PR/empresas/cuenta/modificarvehiculocuenta";

    COPEC_EMPRESAS_VEHICULO_ASIGNAR_QA = "EM1/QA/empresas/cuenta/asignarvehiculos"
    COPEC_EMPRESAS_VEHICULO_ASIGNAR_PR = "EM1/PR/empresas/cuenta/asignarvehiculos"

    COPEC_EMPRESAS_VEHICULO_DESASIGNAR_QA = "EM1/QA/empresas/cuenta/desasignarvehiculos"
    COPEC_EMPRESAS_VEHICULO_DESASIGNAR_PR = "EM1/PR/empresas/cuenta/desasignarvehiculos"

    COPEC_EMPRESAS_VEHICULO_OBTENER_QA = "EM1/QA/empresas/cuenta/consultardetallevehiculocuenta";
    COPEC_EMPRESAS_VEHICULO_OBTENER_PR = "EM1/PR/empresas/cuenta/consultardetallevehiculocuenta";

    COPEC_EMPRESAS_CUENTA_OBTENER_VEHICULOS_QA = "EM1/QA/empresas/cuenta/consultarvehiculos";
    COPEC_EMPRESAS_CUENTA_OBTENER_VEHICULOS_PR = "EM1/PR/empresas/cuenta/consultarvehiculos";

    COPEC_EMPRESAS_FLOTACUENTA_CREAR_QA = "EM1/QA/empresas/flotacuenta/crear";
    COPEC_EMPRESAS_FLOTACUENTA_CREAR_PR = "EM1/PR/empresas/flotacuenta/crear";

    COPEC_EMPRESAS_FLOTACUENTA_MODIFICAR_QA = "EM1/QA/empresas/flotacuenta/modificar";
    COPEC_EMPRESAS_FLOTACUENTA_MODIFICAR_PR = "EM1/PR/empresas/flotacuenta/modificar";

    COPEC_EMPRESAS_GRUPOCUENTA_CREAR_QA = "EM1/QA/empresas/grupocuenta/crear";
    COPEC_EMPRESAS_GRUPOCUENTA_CREAR_PR = "EM1/PR/empresas/grupocuenta/crear";

    COPEC_EMPRESAS_GRUPOCUENTA_MODIFICAR_QA = "EM1/QA/empresas/grupocuenta/modificar";
    COPEC_EMPRESAS_GRUPOCUENTA_MODIFICAR_PR = "EM1/PR/empresas/grupocuenta/modificar";

    COPEC_EMPRESAS_CONSULTAR_MEDIO_PAGO_QA = "EM1/QA/empresas/cuenta/consultarformadepago";
    COPEC_EMPRESAS_CONSULTAR_MEDIO_PAGO_PR = "EM1/PR/empresas/cuenta/consultarformadepago";

    COPEC_EMPRESAS_NUEVO_GET_MEDIO_PAGO_QA = "EM1/QA/empresas/formadepago/get";
    COPEC_EMPRESAS_NUEVO_GET_MEDIO_PAGO_PR = "EM1/PR/empresas/formadepago/get";

    COPEC_EMPRESAS_NUEVO_GET_MEDIO_PAGO_MOVIMIENTOS_QA = "EM1/QA/empresas/cuenta/consultarmovimientos";
    COPEC_EMPRESAS_NUEVO_GET_MEDIO_PAGO_MOVIMIENTOS_PR = "EM1/PR/empresas/cuenta/consultarmovimientos";

    COPEC_EMPRESAS_NUEVO_POST_MEDIO_PAGO_QA = "EM1/QA/empresas/formadepago/post";
    COPEC_EMPRESAS_NUEVO_POST_MEDIO_PAGO_PR = "EM1/PR/empresas/formadepago/post";

    COPEC_EMPRESAS_NUEVO_ELIMINAR_MEDIO_PAGO_QA = "EM1/QA/empresas/formadepago/eliminar";
    COPEC_EMPRESAS_NUEVO_ELIMINAR_MEDIO_PAGO_PR = "EM1/PR/empresas/formadepago/eliminar";


    COPEC_EMPRESAS_MODIFICAR_MEDIO_PAGO_QA = "EM1/QA/empresas/cuenta/modificarformadepago";
    COPEC_EMPRESAS_MODIFICAR_MEDIO_PAGO_PR = "EM1/PR/empresas/cuenta/modificarformadepago";

    COPEC_EMPRESAS_NUEVO_MODIFICAR_MEDIO_PAGO_QA = "EM1/QA/empresas/formadepago/modificar";
    COPEC_EMPRESAS_NUEVO_MODIFICAR_MEDIO_PAGO_PR = "EM1/PR/empresas/formadepago/modificar";

    COPEC_EMPRESAS_ASOCIAR_CUENTA_CORRIENTE_QA = "EM1/QA/empresas/cuenta/asociarcuentacorriente";
    COPEC_EMPRESAS_ASOCIAR_CUENTA_CORRIENTE_PR = "EM1/PR/empresas/cuenta/asociarcuentacorriente";

    COPEC_EMPRESAS_DESASOCIAR_CUENTA_CORRIENTE_QA = "EM1/QA/empresas/cuenta/desasociarcuentacorriente";
    COPEC_EMPRESAS_DESASOCIAR_CUENTA_CORRIENTE_PR = "EM1/PR/empresas/cuenta/desasociarcuentacorriente";

    COPEC_EMPRESAS_OBTENER_MAESTROS_QA = "EM1/QA/empresas/cuenta/consultarmaestros";
    COPEC_EMPRESAS_OBTENER_MAESTROS_PR = "EM1/PR/empresas/cuenta/consultarmaestros";

    COPEC_EMPRESAS_ABONO_POST_QA = "EM1/QA/empresas/abono/post";
    COPEC_EMPRESAS_ABONO_POST_PR = "EM1/PR/empresas/abono/post";

    COPEC_EMPRESAS_ABONO_GET_QA = "EM1/QA/empresas/abono/get";
    COPEC_EMPRESAS_ABONO_GET_PR = "EM1/PR/empresas/abono/get";

    COPEC_EMPRESAS_USUARIO_MODIFICAR_DATOS_QA = "QA/ingreso/usuario";
    COPEC_EMPRESAS_USUARIO_MODIFICAR_DATOS_PR = "PR/ingreso/usuario";

    // nuevas URL
    CAMBIAR_MAIL_ENDPOINT:string     = 'ingreso/cambiarmail';
    USUARIO_MODIFICAR_DATOS          = "ingreso/usuario";
    CUENTA_CONSULTAR_ABONOS          = "empresas/cuenta/consultarabonos";
    FORMA_DE_PAGO_GET                = "empresas/formadepago/get"
    RETIRAR_FONDO_BILLETERA          = "empresas/cuenta/retirarfondosbilletera";

    DESASOCIAR_CUENTA_CORRIENTE      = "empresas/cuenta/desasociarcuentacorriente";
    CONSULTAR_USUARIO_ADMIN          = "empresas/cuenta/consultarusuariosadmin";

    DETALLE_USUARIO_ADMIN            = "empresas/cuenta/consultardetalleusuariocuenta";

    kcuentaModificarUsuarioCuentaRol = "empresas/cuenta/modificarusuariocuentarol";

    kCuentaConsultarnotificacionesEnviadas = "empresas/cuenta/consultarinvitaciones";
    kcuentaCrearInvitacionUsuarioAdmin = "empresas/cuenta/crearinvitacionusuarioadmin";
    kCancelarInvitacionEndPoint = "empresas/usuariocuenta/cancelarinvitacion";
    kConsultarCondiguracionNotificaciones ="empresas/usuario/consultarconfiguracionnotificaciones";
    kCuentaConsultarnotificacionesRecibidas = "empresas/usuario/consultarinvitacionesrecibidas";
    kAceptarInvitacionEndPoint = "empresas/usuariocuenta/aceptarinvitacion";
    kModificarCondiguracionNotificaciones ="empresas/usuario/modificarconfiguracionnotificaciones";
    kUsuarioConsultarNotificaciones = "empresas/usuario/consultarnotificaciones";
    kModificarVehiculoEndPoint = "empresas/cuenta/modificarvehiculocuenta";



    //closeThis()empresas/usuario/consultarconfiguracionnotificaciones














    //#endregion

    //#region Google
    GOOGLE_KEY = "AIzaSyAqR5c23DyLVCnaiWsWkmQZrc-rqNvZK_Y";
    GOOGLE_AUTOCOMPLETE = "https://maps.googleapis.com/maps/api/place/autocomplete/xml?input=#input&types=establishment&location=#location&radius=1000&strictbounds&key=#key";
    //#endregion
}
