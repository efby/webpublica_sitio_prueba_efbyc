export class Paginas {
    //Paginas Internas

    public static sinPagina = "/";

    //Ingreso
    public static IngresoInicioPage = "Ingreso/Cargando";
    public static IngresoTelefonoId = "Ingreso/TelefonoId";
    public static IngresoContrasena = "Ingreso/ValidarContrasena";
    public static IngresoCreaContrasena = "Ingreso/CrearContrasena";
    public static IngresoModificaContrasena = "Ingreso/ModificarContrasena";
    public static IngresoEmail = "Ingreso/Email";
    public static IngresoNombre = "Ingreso/Nombre";
    public static IngresoPinValor = "Ingreso/PinValor";
    public static IngresoRutId = "Ingreso/RutId";
    public static IngresoRutIdRecuperacion = "Ingreso/RecuperaRutId";
    public static IngresoSMSValor = "Ingreso/SMSValor";
    public static IngresoConfirmaPin = "Ingreso/ConfirmaPinValor";
    public static IngresoValidaPin = "Ingreso/ValidaPin";
    public static IngresoCorreoRecuperacion = "Ingreso/RecuperarContrasena";
    public static IngresoTerminosCondiciones = "Ingreso/TerminosCondiciones"

    public static IniciaSesion = "/Login";

    //Home
    //public static Home = "Home/Dashboard";
    public static HomeFacturas = "Home/Facturas";
    public static HomeGuiasDespacho = "Home/GuiasDespacho";
    public static HomeBoletas = "Home/Boletas";
    public static HomeVehiculoPermisos = "Home/VehiculoPermisos";
    public static HomeVehiculoCrear = "Home/VehiculoCrear";
    public static HomeVehiculoCrearFlota = "Home/VehiculoCrearGrupo";
    public static HomeConductorCrear = "Home/ConductorCrear";
    public static HomeConductorConfirmar = "Home/ConductorConfirmar";
    public static HomeConductorCrearGrupo = "Home/ConductorCrearGrupo";
    public static HomeConductorPresentacion = "Home/ConductorPresentacion";
    public static HomeConductorDetalle = "Home/ConductorDetalle";
    public static HomeAbonoPago = "Home/AbonoPago";
    public static HomeAbonoResultado = "Home/AbonoResultado";
    public static HomeCreditoPago = "Home/CreditoPago";
    public static HomeCreditoResultado = "Home/CreditoResultado";
    public static HomeConfigurarPerfil = "Home/ConfigurarPerfil";
    public static HomeConfigurarEmpresa = "Home/ConfigurarEmpresa";
    public static HomeEstaciones = "Home/Estaciones";
    public static HomeFlotasVehiculo = "Home/FlotasVehiculos";
    public static HomeGruposConductores = "Home/GruposConductores";



    //Muevo
    public static MuevoNuevoAdministrador = "Ingreso/AdministradorMuevo";
    public static MuevoNuevoContrasena = "Ingreso/ContrasenaMuevo";
    public static MuevoNuevoMail = "Ingreso/MailMuevo";
    public static MuevoNuevoLogin = "Ingreso/LogInMuevo";
    public static MuevoNuevoSoporte = "Ingreso/Soporte";
    public static MuevoLogInPers = "Ingreso/LogInPersMuevo";
    public static RecuperaPinMuevo = "Ingreso/RecuperaPinMuevo";

    public static HomeMuevo = "HomeMuevo/Dashboard";
    public static HomeMuevoCrearConductor = "HomeMuevo/CrearConductor";
    public static HomeMuevoConfirmarConductor = "HomeMuevo/ConfirmarConductor";
    public static HomeMuevoMisConductores = "HomeMuevo/MisConductores";
    public static HomeMuevoMiGrupo = "HomeMuevo/MiGrupo";
    public static HomeMuevoMisInvitaciones = "HomeMuevo/MisInvitaciones";
    public static HomeMuevoCrearVehiculo = "HomeMuevo/CrearVehiculo";
    public static HomeMuevoConfirmarVehiculo = "HomeMuevo/ConfirmarVehiculo";
    public static HomeMuevoMisVehiculos = "HomeMuevo/MisVehiculos";
    public static HomeMuevoMiFlota = "HomeMuevo/MiFlota";
    public static HomeMuevoMiFacturacion = "HomeMuevo/MiFacturacion";
    public static HomeMuevoMiPerfil = "HomeMuevo/MiPerfil";
    public static HomeMuevoMisMetodosPago = "HomeMuevo/MisMetodosPago";
    public static HomeMuevoMisMetodosPagoMovimientos = "HomeMuevo/MisMetodosPagoMovimientos";
    public static HomeMuevoEquipoSelCuenta = "HomeMuevo/EquipoSelCuenta";
    public static HomeMuevoEquipoNombre = "HomeMuevo/EquipoNombre";
    public static HomeMuevoMisTransacciones = "HomeMuevo/MisTransacciones";
    public static HomeMuevoMiHistorial = "HomeMuevo/MiHistorial";
    public static HomeMuevoAbonarBilletera = "HomeMuevo/AbonarBilletera";
    public static HomeMuevoAbonarProcesando = "HomeMuevo/AbonarProcesando";
    public static HomeMuevoMisRolesYPerfiles = "HomeMuevo/MisRolesYPerfiles";
    public static HomeMuevoCuentas = "HomeMuevo/Cuentas";
    public static HomeMuevoDev = "HomeMuevo/Dev";



    //Error
    public static ErrorGenerico = "Error";
    public static ErrorEscrituraStorage = "Error/errorEscrituraStorage";
    public static ErrorLecturaDispositivo = "Error/errorLecturaDispositivo";
    public static ErrorLecturaStorage = "Error/errorLecturaStorage";
    public static ErrorNOK = "Error/errorNOK";
    public static ErrorSA = "Error/errorSA";
    public static ErrorSC = "Error/errorSC";


    //Modales
    public static ModalSCComponent = "ModalSCComponent";
    public static ModalNroDocumentoComponent = "NroDocumentoComponent";
    public static ModalSubirArchivoComponent = "ModalSubirArchivoComponent";
    public static ModalAgregarEmpresaComponent = "ModalAgregaEmpresaComponent";
    public static ModalAgregarDatosFacturacionComponent = "ModalAgregaDatosFacturacionComponent";
    public static ModalInvitarAdminComponent = "ModalInvitarAdminComponent";
    public static ModalAceptarInvitacionComponent = "ModalAceptarInvitacionComponent";
    public static ModalAceptarCancelarComponent = "ModalAceptarCancelarComponent";
    public static ModalAceptar = "ModalAceptarComponent";

    public static ModalConductorDetalle = "ModalConductorDetalle"
    public static ModalConductorAjusteDiarioMensual = "ModalConductorAjusteDiarioMensual";
    public static ModalVehiculosAsignados = "ModalVehiculosAsignados";
    public static ModalVehiculosAsignacion = "ModalVehiculosAsignacion";
    public static ModalVehiculoCrear = "ModalVehiculoCrear";
    public static ModalVehiculoDetalle = "ModalVehiculoDetalle";
    public static ModalCuentaDetalle = "ModalCuentaDetalle";
    public static ModalMediosPagoCuentasBancarias = "ModalMediosPagoCuentasBancarias";
    public static ModalMediosPagoCrearCuentasBancarias = "ModalMediosPagoCrearCuentasBancarias";
    public static ModalMediosPagoSeleccionMedio = "ModalMediosPagoSeleccionMedio";

    public static ModalAbonoExito = "ModalAbonoExito";
    public static ModalTarjetaAbonoAbonar = "ModalTarjetaAbonoAbonar";
    //public static Modal

    public static ModalOneClickComponent = "ModalOneClickComponent";
    public static ModalMuevo = "MuevoGeneric"

}
