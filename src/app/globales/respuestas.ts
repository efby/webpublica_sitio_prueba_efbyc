export class Respuestas {

    public static RESPUESTA_NOK: string = "NOK";
    public static RESPUESTA_NOE: string = "NOE";
    public static RESPUESTA_OK: string = "OK";
    public static RESPUESTA_SC: string = "SC";
    public static RESPUESTA_SA: string = "SA";

    public static InicioSAPage: "No es posible ingresar a la aplicación en este momento. Por favor intente nuevamente más tarde."
    public static SAModal: "Su credenciales ya no son validas. Por favor ingrese nuevamente."
    public static SCPage: "No se ha podido establecer una conexión. Por favor intente nuevamente más tarde."
    public static SCModal: "No se ha podido establecer una conexión. ¿Desea reintentar?"
    public static NOKPage: "Se ha presentado un error inesperado en la aplicación."
    public static ErrorDispositivoLecturaPage: "No es posible leer la información de su dispositivo."
    public static ErrorEscrituraStoragePage: "No es posible almacenar las credenciales de acceso en su equipo."
    public static ErrorLecturaStoragePage: "No es posible leer los datos almacenados en su equipo."
    public static ErrorBorradoStoragePage: "No es posible limpiar la memoria de su equipo para iniciar un nuevo registro."


    public static toastrError: string = "error";
    public static toastrInfo: string = "info";
    public static toastrWarning: string = "warning";
    public static toastrSuccess: string = "success";

}
