import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { v4 as uuidv4 } from 'uuid';

@Injectable({ providedIn: 'root' })
export class SidebarService {

    constructor(private modal: NgbModal,) { }
    public closeAll(reason?) {
        var list = Array.from(document.getElementsByClassName("right-muevo"));
        document.body.setAttribute("style","")
        for (let item of list) {
            item.classList.remove("in")
        }
        setTimeout(() => {
            if (reason != null) {
                this.modal.dismissAll(reason);
            } else {
                this.modal.dismissAll();
            }

            this.sidebars = [];
        }, 470);
    }
    public open(componente: any, options?: SidebarModeloOptions): SidebarModelo {
        let _options: SidebarModeloOptions = new SidebarModeloOptions();
        _options.id = uuidv4()
        if (options != null) {
            _options.id = options.id || uuidv4();

            _options.miBackdrop = options.miBackdrop == null ? "static" : options.miBackdrop;
        }
        //var _options = options || {id:uuidv4(),};

        var refModal = this.modal.open(componente, { windowClass: "right-muevo " + _options.id, backdrop: _options.miBackdrop })
        setTimeout(() => {
            document.getElementsByClassName(_options.id)[0].classList.add("in")
        }, 10)
        var configuracionModal = {
            id: _options.id,
            refModal: refModal,
        }
        refModal.componentInstance.id = _options.id

        this.add(configuracionModal);
        return configuracionModal;
    }
    public close(id: string, result?:any): void {
        var listaSidebar = this.sidebars.filter(x => x.id == id)
        if (listaSidebar.length > 0) {
            var selectedSidebar = listaSidebar[0];
            document.getElementsByClassName(id)[0].classList.remove("in")
            setTimeout(() => {
                if(result){
                    selectedSidebar.refModal.close(result)
                   
                }else{
                    selectedSidebar.refModal.close()
                }
                this.remove(id);
               
            }, 470);
        } else {
            console.error("ERROR: " + id + " no existe!!!")
        }

    }
    public sidebars: Array<SidebarModelo> = [];
    public add(sidebar: any): void {
        this.sidebars.push(sidebar);
    }
    public remove(id: string): void {
        this.sidebars = this.sidebars.filter(x => x.id !== id);
    }

}
export class SidebarModelo {
    id: string
    refModal: NgbModalRef
}
export class SidebarModeloOptions {
    id?: string;
    miBackdrop?: boolean | 'static' = "static";
}
