import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorSAComponent } from './error-sa.component';

describe('ErrorSAComponent', () => {
  let component: ErrorSAComponent;
  let fixture: ComponentFixture<ErrorSAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorSAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorSAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
