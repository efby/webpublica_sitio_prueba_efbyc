import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales, Ingreso, Home } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-error-sa',
  templateUrl: './error-sa.component.html',
  styleUrls: ['./error-sa.component.css']
})
export class ErrorSAComponent implements OnInit {
  paginaVolver = "/";
  constructor(private variablesApi: VariablesGenerales) {
  }

  ngOnInit() {
    console.log('SA init..');
    // localStorage.clear();
    // this.variablesApi.authorizationToken = null;
    // this.variablesApi.equipoSecret = null;
    // this.variablesApi.home = new Home();
    // this.variablesApi.ingreso = new Ingreso()
    // this.variablesApi = new VariablesGenerales();
  }

}
