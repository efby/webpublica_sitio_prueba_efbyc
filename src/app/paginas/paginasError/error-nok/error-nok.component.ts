import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-error-nok',
  templateUrl: './error-nok.component.html',
  styleUrls: ['./error-nok.component.css']
})
export class ErrorNOKComponent implements OnInit {
  paginaVolver = "/";
  errorMessage: string;
  constructor(private variablesApi: VariablesGenerales) {
    this.errorMessage = this.variablesApi.ingreso.errorMessage;
  }

  ngOnInit() {
  }

}
