import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorNOKComponent } from './error-nok.component';

describe('ErrorNOKComponent', () => {
  let component: ErrorNOKComponent;
  let fixture: ComponentFixture<ErrorNOKComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorNOKComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorNOKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
