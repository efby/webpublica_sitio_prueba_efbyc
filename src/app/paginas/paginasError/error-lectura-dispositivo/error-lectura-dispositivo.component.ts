import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-error-lectura-dispositivo',
  templateUrl: './error-lectura-dispositivo.component.html',
  styleUrls: ['./error-lectura-dispositivo.component.css']
})
export class ErrorLecturaDispositivoComponent implements OnInit {
  paginaVolver = "/";
  constructor() { }

  ngOnInit() {
  }

}
