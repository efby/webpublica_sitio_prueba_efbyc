import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorLecturaDispositivoComponent } from './error-lectura-dispositivo.component';

describe('ErrorLecturaDispositivoComponent', () => {
  let component: ErrorLecturaDispositivoComponent;
  let fixture: ComponentFixture<ErrorLecturaDispositivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorLecturaDispositivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorLecturaDispositivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
