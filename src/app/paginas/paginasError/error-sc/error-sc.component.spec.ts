import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorSCComponent } from './error-sc.component';

describe('ErrorSCComponent', () => {
  let component: ErrorSCComponent;
  let fixture: ComponentFixture<ErrorSCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorSCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorSCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
