import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-error-sc',
  templateUrl: './error-sc.component.html',
  styleUrls: ['./error-sc.component.css']
})
export class ErrorSCComponent implements OnInit {
  paginaVolver = "/";
  constructor() { }

  ngOnInit() {
  }

}
