import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorLecturaStorageComponent } from './error-lectura-storage.component';

describe('ErrorLecturaStorageComponent', () => {
  let component: ErrorLecturaStorageComponent;
  let fixture: ComponentFixture<ErrorLecturaStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorLecturaStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorLecturaStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
