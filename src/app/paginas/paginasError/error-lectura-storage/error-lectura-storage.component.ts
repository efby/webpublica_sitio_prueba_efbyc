import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-error-lectura-storage',
  templateUrl: './error-lectura-storage.component.html',
  styleUrls: ['./error-lectura-storage.component.css']
})
export class ErrorLecturaStorageComponent implements OnInit {
  paginaVolver = "/";
  constructor() { }

  ngOnInit() {
  }

}
