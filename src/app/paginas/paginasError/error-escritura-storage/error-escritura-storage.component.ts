import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-error-escritura-storage',
  templateUrl: './error-escritura-storage.component.html',
  styleUrls: ['./error-escritura-storage.component.css']
})
export class ErrorEscrituraStorageComponent implements OnInit {

  constructor() { }
  paginaVolver = "/";

  ngOnInit() {
  }

}
