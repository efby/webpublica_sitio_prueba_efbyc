import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorEscrituraStorageComponent } from './error-escritura-storage.component';

describe('ErrorEscrituraStorageComponent', () => {
  let component: ErrorEscrituraStorageComponent;
  let fixture: ComponentFixture<ErrorEscrituraStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorEscrituraStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorEscrituraStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
