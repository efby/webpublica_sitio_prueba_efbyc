import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { slideInAnimation } from 'src/app/route-animation';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css'],
  animations: [slideInAnimation]
})
export class ErrorComponent implements OnInit {

  interval: any;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private ref: ChangeDetectorRef) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges()
      this.detectmob();
    }, 0);
    this.detectmob();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  detectmob() {
    let info = UtilitariosService.getPlatformInfo();
    this.isMobile = info.isMobile;
    this.variablesApi.esMobile = this.isMobile;
  }

}
