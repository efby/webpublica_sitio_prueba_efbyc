import { Component, OnInit, ChangeDetectorRef, HostListener, ViewChild, EventEmitter } from '@angular/core';
import { Cuentas, Grupos, Flotas, VariablesGenerales, Maestros, Bancos, Home, Ingreso, TipoVehiculo, TipoCuentaBancaria, Roles } from 'src/app/globales/variables-generales';
import { Conductor } from 'src/app/models/conductores';
import { BreadCrumbsService, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { Vehiculos } from 'src/app/models/vehiculos';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { fadeOutToRight } from './../../animations/fade-right';
import { Cuenta, Menu, MedioPago, UsuarioLogin, Tarjeta, PeriodosFacturacion, Invitacion, InvitacionRecibida, Transaccion } from 'src/app/globales/business-objects';
import { RxService } from 'src/app/servicios/rx.service';
import { PageBase } from './pageBase';
import { ComprasDetalleComponent } from './historiales/compras/compras-detalle/compras-detalle.component';
import { FacturasDetalleComponent } from './historiales/facturas/facturas-detalle/facturas-detalle.component';
import { Facturas } from 'src/app/models/facturas';
import { ConductorDetalleComponent } from './conductores/conductor-detalle/conductor-detalle.component';
import { RestriccionesDiasHorariosComponent } from './conductores/restricciones-dias-horarios/restricciones-dias-horarios.component';
import { ConductorAjustesCombustibleComponent } from './conductores/conductor-ajustes-combustible/conductor-ajustes-combustible.component';
import { VehiculosAsignadosComponent } from './vehiculos/vehiculos-asignados/vehiculos-asignados.component';
import { VehiculosAsignacionComponent } from './vehiculos/vehiculos-asignacion/vehiculos-asignacion.component';
import { VehiculoCrearComponent } from './vehiculos/vehiculo-crear/vehiculo-crear.component';
import { VehiculoDetalleComponent } from './vehiculos/vehiculo-detalle/vehiculo-detalle.component';
import { CuentaDetalleComponent } from './cuentas/cuenta-detalle/cuenta-detalle.component';
import { MuevoInvitacionesConductoresComponent } from './conductores/muevo-invitaciones-conductores/muevo-invitaciones-conductores.component';
import { SeleccionMedioComponent } from './metodos-pago/abono/seleccion-medio/seleccion-medio.component';
import { CuentasBancariasComponent } from './metodos-pago/abono/cuentas-bancarias/cuentas-bancarias.component';
import { RutPipe } from 'ng2-rut';
import { TarjetaAbonoComponent } from './metodos-pago/abono/tarjeta-abono/tarjeta-abono.component';
import { TarjetaAbonoAbonarComponent } from './metodos-pago/abono/tarjeta-abono-abonar/tarjeta-abono-abonar.component';
import { SidebarService } from 'src/app/services/sidebar-services';
import { Sidebar } from 'ng-sidebar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditarPerfilComponent } from './perfil/editar-perfil/editar-perfil.component';
import { MiPerfilSidebarComponent } from './perfil/mi-perfil-sidebar/mi-perfil-sidebar.component';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';
import { Constantes } from 'src/app/globales/constantes';

@Component({
    selector: 'app-home-muevo',
    templateUrl: './home-muevo.component.html',
    styleUrls: ['./home-muevo.component.scss'],
    providers: [RutPipe],
    animations: [fadeOutToRight], // make fade in animation available to this component                
    host: { '[@fadeOutToRight]': '' }
})
export class HomeMuevoComponent extends PageBase implements OnInit {

    sidebar = {
        animated: false,
        opened: true,
        position: 'left',
        mode: 'push', // 'over|push'
        closeOnClickOutside: false,
        enabled: true
    }


    @ViewChild('sidebarPerfil')
    public sidebarPerfilText: Sidebar;

    @ViewChild('sideBarConductores')
    sidebarConductores: any;

    @ViewChild('sideBarSeleccionTarjeta')
    sidebarSeleccionTarjeta: any;

    @ViewChild('sideBarConfiguracionFacturacion')
    sideBarConfiguracionFacturacion: any;

    @ViewChild('sideBarRestriccionesConductor')
    sideBarRestriccionesConductor: any;

    @ViewChild('sideBarConductorAjusteDiarioMensual')
    sideBarConductorAjusteDiarioMensual: any;

    @ViewChild('sideBarCondutorEstaciones')
    sideBarCondutorEstaciones: any;

    @ViewChild('sideBarVehiculosAsignados')
    sideBarVehiculosAsignados: any;

    @ViewChild('sideBarVehiculosAsignacion')
    sideBarVehiculosAsignacion: any;

    @ViewChild('sideBarVehiculoCrear')
    sideBarVehiculoCrear: any;

    @ViewChild('sideBarVehiculoDetalle')
    sideBarVehiculoDetalle: any;

    @ViewChild('sideBarDetalleCuenta')
    sideBarDetalleCuenta: any;

    @ViewChild('sideBarCuentaCorriente')
    sideBarCuentaCorriente: any;

    @ViewChild('sideBarTarjetaAbonoAbonar')
    sideBarTarjetaAbonoAbonar: any;

    @ViewChild('sideBarAbonoCuentasBancarias')
    sideBarAbonoCuentasBancarias: any;

    @ViewChild('sideBarAbonoTarjeta')
    sideBarAbonoTarjeta: any;

    @ViewChild('sideBarAbonoSeleccionMedio')
    sideBarAbonoSeleccionMedio: any;

    //COMPONENTS

    @ViewChild(ConductorAjustesCombustibleComponent)
    conductorAjustesCombustibleComponent: ConductorAjustesCombustibleComponent;

    @ViewChild(ConductorDetalleComponent)
    conductorDetalleComponent: ConductorDetalleComponent;

    @ViewChild(ComprasDetalleComponent)
    comprasDetalleComponent: ComprasDetalleComponent;

    @ViewChild(FacturasDetalleComponent)
    facturaDetalleComponent: FacturasDetalleComponent;

    @ViewChild(RestriccionesDiasHorariosComponent)
    restriccionesDiasHorariosComponent: RestriccionesDiasHorariosComponent;

    @ViewChild(VehiculosAsignadosComponent)
    vehiculosAsignadosComponent: VehiculosAsignadosComponent;

    @ViewChild(VehiculosAsignacionComponent)
    vehiculosAsignacionComponent: VehiculosAsignacionComponent;

    @ViewChild(VehiculoCrearComponent)
    vehiculoCrearComponent: VehiculoCrearComponent;

    @ViewChild(VehiculoDetalleComponent)
    vehiculoDetalleComponent: VehiculoDetalleComponent;

    @ViewChild(CuentaDetalleComponent)
    cuentaDetalleComponent: CuentaDetalleComponent;

    @ViewChild(MuevoInvitacionesConductoresComponent)
    muevoInvitacionesConductoresComponent: MuevoInvitacionesConductoresComponent;

    @ViewChild(SeleccionMedioComponent)
    seleccionMedioComponent: SeleccionMedioComponent;

    @ViewChild(CuentasBancariasComponent)
    cuentasBancariasComponent: CuentasBancariasComponent;

    @ViewChild(TarjetaAbonoComponent)
    tarjetaAbonoComponent: TarjetaAbonoComponent;

    @ViewChild(TarjetaAbonoAbonarComponent)
    tarjetaAbonoAbonarComponent: TarjetaAbonoAbonarComponent;

    cuentaSeleccionada: Cuenta;

    _false: boolean = false;
    _true: boolean = true;
    _confServicios: boolean = false;
    _isConfiguracion: boolean = false;
    empresaActual: string = "";
    _dropEmpresa: boolean = true;
    breadCrumbs: any;
    muestraBreadcrumbs: boolean;
    muestraBanner: boolean;
    _seleccionTarjeta = {
        tarjetas: [],
        seleccionada: null,
        opened: false,
        flags: {
            _billetera: false,
            _oneClick: false,
            _atendedor: false
        }
    }


    _invitaciones: boolean = false;
    _enableSaveInvitaciones: boolean = false;
    _estaciones: boolean = false;

    _invitados: boolean = false;
    _diaHorario: boolean = false;
    _diarioMensual: boolean = false;
    _cuentaCorriente: boolean = false;
    _inviteUsuario: boolean = false;
    _edicion: boolean = false;
    _diaHorarioPersonalizado: boolean = true;
    time = { hour: 8, minute: 0 };
    collapseComuna: boolean = false;
    _activo1: boolean = true;
    _activo2: boolean = false;

    _activaMenuInternalOptions: boolean = false;
    _ajusteDiarioMensual = {
        selecteds: [],
        limiteDiarioCombustible: 0,
        limiteMensualCombustible: 0
    }
    _asignacionVehiculos: any = {
        open: false
    };
    _configuracionFacturacion: {
        open: boolean,
        periodos: PeriodosFacturacion[],
        actividades: any[],
        distritos: any[],
        declarationSuccess: boolean,
        data: any
    };

    _configuracionInvitacionesEmpresas: {
        open: boolean,
        invitaciones: InvitacionRecibida[]
    }


    _configuracionFacturasDetalle: {
        open: boolean,
        cuentaId: number,
        facturaId: number,
        item: Facturas
    }

    sidebars = {
        vehiculoDetalleOpen: false,
        vehiculoCrearOpen: false,
        vehiculosAsignadosOpen: false,
        conductorEstacionesOpen: false,
        conductorAjusteDiarioMensualOpen: false,
        conductorRestriccionesDHOpen: false,
        conductorDetalleOpen: false,
        compraDetalleOpen: false,
        cuentaCrearOpen: false,
        cuentaDetalleOpen: false,
        invitacionesOpen: false,

        seleccionMedioOpen: false,
        cuentasBancariasOpen: false,
        cuentaCorriente: false,
        cuentasBancariasInfoBancariaOpen: false,
        tarjetasRegistradasOpen: false,
        tarjetasAbonarOpen: false,
    }

    btnGuardarSidebar = true;
    esDashboard: boolean = false;
    isCollapsed: boolean = true;
    isMobile: boolean = false;
    generalSidebarLeft: string = "generalSidebarLeft";
    generalSidebarRight: string = "sidebar-right";

    _horarioDia: number = 0;

    dashDisplay: boolean = true;

    idRol: number = 0;
    titleInvitacion: string = "";
    invite_nombre: string = "";
    invite_apellido: string = "";
    invite_telefono: string = "";
    invite_restriccion_DiaValor: string = "";
    invite_restriccion_MesValor: string = "";
    invite_conduce: boolean = false;
    invite_restriccion_combustible: boolean = false;

    ctacte: {
        muestraMenu: "M"
        cuentaId: number,
        banco: string,
        tipoCuenta: string,//titular: string,
        rut: string,
        nroCuenta: string,
        //email: string
    }
    cuentacorrienteNuevaOrigin: string = '';

    // cuentaCorriente_muestraMenu: string = "M";
    // cuentaCorriente_cuentaId: number;
    // cuentaCorriente_nombre: string;
    // cuentaCorriente_banco: string = "";
    // cuentaCorriente_Titular: string;
    // cuentaCorriente_rut: string;
    // cuentaCorriente_nroCuenta: number;
    // cuentaCorriente_email: string;
    inputNvaGrupo: string = "";
    inputNvaFlota: string = "";
    //idConductor: number;
    idVehiculo: number;
    idInvitado: number;

    _diaNoHabilitadoEstado: number = 0;
    _diaNoHabilitadoValor: number[] = [];
    _horarioNoHabilitadoEstado: number = 0;
    _horarioNoHabilitadoValor: number[] = [];
    _estacionNoHabilitadaEstado: number = 0;
    _estacionNoHabilitadaValor: number[] = [];

    //_selDiaHorario: number = 0;

    empresas: Cuenta[] = [];
    medioPago: MedioPago;
    invitaciones: Invitacion[] = [];
    grupos: Grupos[] = [];
    conductores: Conductor[] = [];
    vehiculos: Vehiculos[] = [];
    flotas: Flotas[] = [];
    maestros: Maestros;
    menu: Menu;
    bancos: Bancos[] = [];
    tiposCuentaBancaria: TipoCuentaBancaria[];
    invitado: Invitacion = new Invitacion();
    usuarioLogin: UsuarioLogin = new UsuarioLogin();
    //_cuentaNombre: string = "";
    _conductorEdit: Conductor;
    _vehiculoEdit: Vehiculos;
    private unsubscribe = new Subject();

  
    nombre: UsuarioLogin = new UsuarioLogin();
    anoSelect: number[] = [];
    tipoSelect: TipoVehiculo[] = [];
    diasDesde: number;
    diasHasta: number;
    interval: any;

    device = {
        xs: false,
        sm: false,
        md: false,
        lg: false,
        xl: false
    }

    ui = {
        showBigFooter: false
    }

    currentYear:number;
    


    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.handlerScreen(event.target.innerWidth);
    }

    constructor(
        private modal: NgbModal,
        private constantes: Constantes,
        private modalSidebar: SidebarService,
        private rx: RxService, private emiter: BreadCrumbsService, private maquinaHome: MaquinaHome, public utils: UtilitariosService,
        public variablesApi: VariablesGenerales, public router: Router, public ref: ChangeDetectorRef, public rutPipe: RutPipe) {
        super(ref, utils, router);

        this.ctacte = {
            muestraMenu: "M",
            banco: "",
            tipoCuenta: "",
            rut: "",
            //email: "",
            nroCuenta: "",
            cuentaId: 0
        }

        this._configuracionFacturacion = {
            open: false,
            periodos: [],
            actividades: [],
            distritos: [],
            declarationSuccess: false,
            data: {
            }
        }


        this._configuracionInvitacionesEmpresas = {
            open: false,
            invitaciones: []
        }


        this._configuracionFacturasDetalle = {
            open: false,
            cuentaId: 0,
            facturaId: 0,
            item: new Facturas()
        }


        //this.closeAllSidebars();

        this.isMobile = false;
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        // ref.detach();
        // this.interval = setInterval(() => {
        //     this.ref.detectChanges()
        //     this.detectmob();
        // }, 0);
        this.muestraBreadcrumbs = true;
        this.detectmob();

        let info = UtilitariosService.getPlatformInfo();
        this.handlerScreen(info.width);
    }

    ngOnInit() {
        this.currentYear=new Date().getFullYear();
        this.cuentaSeleccionada = this.variablesApi.home.cuentaSeleccionada;
        this.cuentaSeleccionada.rolNombre = this.variablesApi.home.maestros.roles.filter((x: Roles) => x.rolId == this.cuentaSeleccionada.rolId)[0].rolNombre;
        console.log('cuenta seleccionada: ', this.cuentaSeleccionada)
        let anoLimite = (new Date()).getFullYear() + 1;
        for (let index = anoLimite; index > 1900; index--) {
            this.anoSelect.push(index);
        }
        //this.restriccionDiaHorarioPersonalizado(0);
        this._conductorEdit = new Conductor();
        this._vehiculoEdit = new Vehiculos();
        this.empresas = this.variablesApi.home.cuentas.filter(r => r.rolId != 4).map((c: Cuenta) => {
            c.rolNombre = this.variablesApi.home.maestros.roles.filter((x: Roles) => x.rolId == c.rolId)[0].rolNombre;
            return c;
        });
        //this.variablesApi.home.cuentas.filter(r => r.rolId === 1);
        this.invitaciones = this.variablesApi.home.invitaciones;
        console.log("invitaciones: ")
        console.log(this.variablesApi.home.invitaciones)
        this.conductores = this.variablesApi.home.conductores;
        this.vehiculos = this.variablesApi.home.vehiculos;
        this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
        //this._cuentaNombre = this.variablesApi.home.cuentas[idComponente].cuentaNombre
        //this._conductores = false;
        this.maestros = this.variablesApi.home.maestros;
        this.menu = this.variablesApi.home.menu;
        this.bancos = this.variablesApi.home.maestros.bancos;
        this.tiposCuentaBancaria = this.variablesApi.home.maestros.tipoCuentaBancaria;
        this.tipoSelect = this.variablesApi.home.maestros.tipoVehiculo;
        this.nombre = this.variablesApi.home.usuarioLogIn;

        this.emiter.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.internalListener(data);
        });

        //this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandlerCallback);


        this._invitaciones = this.variablesApi.home.invitaciones && this.variablesApi.home.invitaciones.length > 0;


        // this.accionInicio();
        this.setSelectedEmpresa();
        this.enableSaveInvitaciones();
        setTimeout(() => {
            this.sidebar.animated = true;
        }, 10);
        setTimeout(() => {
            this.sidebar.enabled = true;
        }, 10);

    }

    closeAllSidebars() {
        //cierra todos los sidebars
        for (var i in this.sidebars) {
            this.sidebars[i] = false;
        }
    }

    handlerScreen(width) {

        for (let media in this.device)
            this.device[media] = false;


        if (width >= 1200) {
            this.device.xl = true;
            setTimeout(() => {
                this.sidebar.opened = true;
            }, 100)
        }
        else if (width >= 992) {
            this.device.lg = true;
            setTimeout(() => {
                this.sidebar.opened = true;
            }, 100)
        }
        else if (width >= 768) {
            this.device.md = true;
            setTimeout(() => {
                this.sidebar.opened = true;
            }, 100)

        }
        else if (width >= 576) {
            this.device.sm = true;
            setTimeout(() => {
                this.sidebar.opened = false;
            }, 100)
        }
        else {
            this.device.xs = true;
            setTimeout(() => {
                this.sidebar.opened = false;
            }, 100)
        }


        let res = '';
        for (let media in this.device) {
            if (this.device[media]) {
                res = media;
                this.variablesApi.device = media;
                break;
            }
        }

        if (this.device.xs || this.device.sm) {
            this.sidebar.mode = 'over';
            this.sidebar.closeOnClickOutside = true;
        }
        else {
            this.sidebar.mode = 'push';
            this.sidebar.closeOnClickOutside = false;
        }


        this.detectmob();

        console.log(res)
    }

    /** Cierra el flujo de sidebars anidados */
    closeAllFlowSidebars = (currentSidebar, otherSidebars: any[]) => {
        const speed = 100;
        currentSidebar.close();
        if (!otherSidebars || otherSidebars.length == 0)
            return;

        otherSidebars.map(x => {
            switch (x) {
                case 'sideBarConductores':
                    setTimeout(() => this.sidebarConductores.close(), speed);
                    break;
            }
        })
    }

    detectmob() {
        let info = UtilitariosService.getPlatformInfo();
        this.isMobile = info.isMobile;

        if (this.variablesApi.esMobile != this.isMobile) {
            if (this.isMobile) {
                this.isCollapsed = !this.sidebar.opened;
                this.sidebar.opened = false;
            } else {
                this.sidebar.opened = !this.isCollapsed;
                this.isCollapsed = false;
            }
        }
        this.variablesApi.esMobile = this.isMobile;
    }

    toggleMenu() {
        this.isCollapsed = !this.isCollapsed;
    }

    _toggleSidebar() {
        this.sidebar.opened = !this.sidebar.opened;
        this.dashDisplay = true;

    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
        clearInterval(this.interval);
    }

    date2Human(date) {
        let format = 'dd/MM/yyyy';
        return this.utils.date2Human(date, format, this.router.url);
    }


    getRolText(rolId) {
        return this.variablesApi.home.maestros.roles.filter(r => r.rolId === rolId)[0].rolNombre;
    }

    navegaDashboard() {
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_INICIO,
            datos: {
                telefonoId: '',
                usuarioId: ''
            }
        };
        this.utils.showSpinner("Interno");
        this.maquinaHome.homeInicio(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.hideSpinner("Interno");
                if (this.variablesApi.home.fuerzaCambioCuenta) {
                    this.utils.msgPlain(respuesta.mensajeSalida);
                    // this.ngOnInit();
                }
                if (respuesta.paginaRespuesta == Paginas.ErrorNOK) {
                    this.router.navigate([respuesta.paginaRespuesta]);
                } else {
                    this.router.navigate([Paginas.HomeMuevo])
                }

            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                this.utils.hideSpinner("Interno");
                this.utils.msgPlain(respuesta.mensajeSalida);
                this.cerrarSesion();
            }

        });
    }

    eliminaConductorAjustes(item: Conductor) {
        const index = this.utils.arrayIndexOf(this._ajusteDiarioMensual.selecteds, 'idConductor', item.usuarioId);
        if (index >= 0) {
            this._ajusteDiarioMensual.selecteds[index].dropped_adjustments = true;
            setTimeout(() => this._ajusteDiarioMensual.selecteds.splice(index, 1), 400);
        }
    }

    saveAjustesPresupuesto = (popover: any) => {
        if (this._ajusteDiarioMensual.selecteds.length == 0) {
            this.utils.msgPlainModalAccept('error', ['Debe seleccionar al menos un conductor para aplicar el límite de gastos diarios']).then((result: OpcionesModal) => { });
            return;
        }

        let modifieds = 0;
        for (let index = 0; index < this._ajusteDiarioMensual.selecteds.length; index++) {
            let i = this.utils.arrayIndexOf(this.conductores, 'idConductor', this._ajusteDiarioMensual.selecteds[index].idConductor);
            if (i >= 0) {
                this.conductores[i].restriccionCombustible.montoMaximoDia = this._ajusteDiarioMensual.limiteDiarioCombustible;
                this.conductores[i].restriccionCombustible.montoMaximoMes = this._ajusteDiarioMensual.limiteMensualCombustible;
                modifieds++;
            }
        }

        if (modifieds > 0)
            this.modificarMasivoConductor();
    }

    private modificarMasivoConductor() {
        this.utils.showSpinner("Interno");
        let data = [];
        this.conductores.forEach(conductor => {
            let datos = {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                usuarioId: conductor.usuarioId,
                //grupoCuentaId: (conductor.grupoId == null) ? 0 : conductor.grupoId,
                usuarioCuentaEstadoId: conductor.estado ? 1 : 0,
                rolId: conductor.rolId,
                usuarioCuentaConduce: conductor.conduce,
                // usuarioCuentaRindeGasto: conductor.usuarioCuentaRindeGasto,
                // usuarioCuentaCreaVehiculo: conductor.usuarioCuentaCreaVehiculo,
                // usuarioCuentaEligeDTE: conductor.usuarioCuentaEligeDTE,
                // usuarioCuentaSeleccionado: conductor.usuarioCuentaSeleccionado,
                vehiculosAsignados: [],
                estadoCombustible: 1,
                montoMaximoDiaValor: conductor.restriccionCombustible.montoMaximoDia,
                montoMaximoMesValor: conductor.restriccionCombustible.montoMaximoMes
            }
            data.push(datos);
        });

        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: data
        }

        this.maquinaHome.homeUsuarioModificarMasivo(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.hideSpinner("Interno");
                for (let index = 0; index < this.conductores.length; index++) {
                    //this.conductores[index].editable = false;
                }
                this.ngOnInit();
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                this.utils.hideSpinner("Interno");
                this.utils.openModal(respuesta.modalSalida).then(response => {
                    console.log(response);
                });
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                this.utils.hideSpinner("Interno");
                this.utils.msgPlain(respuesta.mensajeSalida);

            }
        });
    }

    private internalListener(data) {
        if (data.migajas != null) {
            this.breadCrumbs = data.migajas;
            this.muestraBreadcrumbs = data.muestraBreadcrumbs;
            this._isConfiguracion = false;
            if (this.breadCrumbs.length <= 1) {
                this.esDashboard = true;
            } else {
                this.esDashboard = false;
            }
            // this.menu = this.variablesApi.home.menu;
        } else {
            console.log('Emmit: ', data.tipo, data)
            switch (data.tipo) {
                case "SeleccionTarjeta":
                    this._seleccionTarjeta.tarjetas = data.tarjetas;
                    this._seleccionTarjeta.flags._atendedor = data.atendedor;
                    this._seleccionTarjeta.flags._billetera = data.billetera;
                    this._seleccionTarjeta.flags._oneClick = data.oneClick;
                    this._seleccionTarjeta.opened = true;
                    break;
                case "Estacion":
                    this._estaciones = data.estacion;
                    this.btnGuardarSidebar = true
                    break;
                case "DiaHorario":
                    this._diaHorario = data.diaHorario;
                    this.btnGuardarSidebar = true
                    break;
                case "Conductor":
                    if (data.action == 'OPEN')
                        this.sidebars.conductorDetalleOpen = true;
                    if (data.action == 'GET_DATA')
                        this.conductorDetalleComponent.setData(data.usuarioId);
                    if (data.action == "CLOSE MODAL CONDUCTOR")
                        this.closeAllFlowSidebars(this.sidebarConductores, []);
                    break;
                case "Restricciones Conductor":
                    if (data.action == 'OPEN') {
                        this.sidebars.conductorRestriccionesDHOpen = true;
                        this.restriccionesDiasHorariosComponent.setData(data.data);
                    }
                    break;
                case "VehiculoDetalle":
                    this.sidebars.vehiculoDetalleOpen = true;
                    this.vehiculoDetalleComponent.setData(data.vehiculoId)
                    //this.getVehiculo()
                    break;
                case "Invitaciones":
                    this._invitados = data.Invitaciones;
                    this.idInvitado = data.idInvitacion;
                    this.invitado = this.variablesApi.home.invitados.filter(r => r.invitacionId === data.idInvitacion)[0];
                    this.btnGuardarSidebar = true
                    break;
                case "CuentaCorriente":
                    this.ctacte = {
                        muestraMenu: data.muestraMenu,
                        banco: "",
                        tipoCuenta: "",
                        rut: "",
                        //email: "",
                        nroCuenta: "",
                        cuentaId: 0
                    }
                    this.cuentacorrienteNuevaOrigin = data.data.origin;
                    this.sidebars.cuentaCorriente = true;
                    this.btnGuardarSidebar = true;

                    break;
                case "InvitaUsuario":
                    this.invite_telefono = "";
                    this.invite_nombre = "";
                    this.invite_apellido = "";
                    this.invite_restriccion_DiaValor = "";
                    this.invite_restriccion_MesValor = "";
                    this.invite_conduce = false;
                    this.invite_restriccion_combustible = false;
                    this.idRol = data.idRol;
                    this._inviteUsuario = data.inviteUsuario;
                    this.utils.pintaValidacion(document.getElementById("dv_invite_nombre"), "123");
                    this.utils.pintaValidacion(document.getElementById("dv_invite_apellido"), "123");
                    this.utils.pintaValidacion(document.getElementById("dv_invite_telefono"), "123");
                    this.titleInvitacion = this.variablesApi.home.maestros.roles.filter(r => r.rolId == data.idRol)[0].rolNombre;
                    this.btnGuardarSidebar = true
                    break;
                case "Configuracion":
                    this._isConfiguracion = data._isConfiguracion;
                    this.btnGuardarSidebar = true
                    break;
                case "AjusteDiarioMensual":
                    this._diarioMensual = true;
                    Object.assign(this._ajusteDiarioMensual.selecteds, data.conductores);
                    this._ajusteDiarioMensual.selecteds.map(x => x.dropped = false);
                    break;

                case "ConfiguracionFacturacion":

                    this._configuracionFacturacion = {
                        open: true,
                        data: data.item,
                        periodos: data.periodos,
                        actividades: data.actividades,
                        distritos: data.distritos,
                        declarationSuccess: data.item.dteDeclaracion
                    }
                    break;
                case "ConfiguracionInvitaciones":
                    this.sidebars.invitacionesOpen = true;
                    break;
                case "InvitacionesConductores":
                    this.sidebars.invitacionesOpen = !this.sidebars.invitacionesOpen;
                    this.muevoInvitacionesConductoresComponent.cargarInvitaciones();
                    break;
                case "InvitacionesEmpresas":
                    this._configuracionInvitacionesEmpresas = {
                        invitaciones: data.invitaciones,
                        open: true
                    }
                    break;
                case "NuevaCuenta":
                    this.sidebars.cuentaCrearOpen = true
                    break;
                case "CuentaDetalle":
                    this.sidebars.cuentaDetalleOpen = true;
                    this.cuentaDetalleComponent.setData(data.data.item);
                    break;
                case "CompraDetalle":
                    this.sidebars.compraDetalleOpen = true;
                    this.comprasDetalleComponent.getData(data.item);
                    break;
                case "FacturaDetalle":
                    this._configuracionFacturasDetalle = {
                        open: true,
                        cuentaId: data.cuentaId,
                        facturaId: data.facturaId,
                        item: data.item
                    }

                    this.facturaDetalleComponent.setData(this._configuracionFacturasDetalle.cuentaId, this._configuracionFacturasDetalle.facturaId, this._configuracionFacturasDetalle.item);
                    break;
                case "Conductor Gasto DiarioMensual":
                    this.sidebars.conductorAjusteDiarioMensualOpen = true;
                    let conductor_gastos = new Conductor();
                    Object.assign(conductor_gastos, data.data);
                    this.conductorAjustesCombustibleComponent.setData(conductor_gastos)
                    break;
                case "Vehiculos Asignados":
                    this.sidebars.vehiculosAsignadosOpen = true;
                    let conductor_asignados = this.utils.clone(data.data.conductor)

                    this.vehiculosAsignadosComponent.setData(data.data.origin, conductor_asignados, data.data.conductorKey)
                    break;
                case "VehiculosAsignacion":
                    this._asignacionVehiculos.open = true;
                    this.vehiculosAsignacionComponent.setData(data.data.origin, data.data.conductor, data.data.vehiculosDisponibles, data.data.vehiculosAsignados)
                    break;
                case "VehiculoCrear":
                    this.sidebars.vehiculoCrearOpen = true;
                    this.vehiculoCrearComponent.setData(data.data.origin);
                    break;
                case "Conductor Estaciones":
                    this.sidebars.conductorEstacionesOpen = true;
                    break;

                case "MetodosPago_Abono_SeleccionMedio":
                    this.sidebars.seleccionMedioOpen = true;
                    this.seleccionMedioComponent.main(data.data.cuentasCorrientes);
                    break;
                case "MetodosPago_Abono_CuentasBancarias":
                    this.sidebars.cuentasBancariasOpen = true;
                    this.cuentasBancariasComponent.main(data.data.selectionMode, data.action);
                    break;
                case "MetodosPago_Abono_InfoBancaria":
                    this.sidebars.cuentasBancariasInfoBancariaOpen = true;
                    break;

                //seleccionar tarjeta para abonar
                case "MetodosPago_Abono_Tarjeta":
                    this.sidebars.tarjetasRegistradasOpen = true;
                    this.tarjetaAbonoComponent.main(data.data.selectionMode, data.action);
                    break;
                case "MetodosPago_Abono_Tarjeta_Abonar":
                    this.sidebars.tarjetasAbonarOpen = true;
                    this.tarjetaAbonoAbonarComponent.main(data.data.tarjeta, data.data.saldo);
                    break;

                case "CERRAR MODAL REMOTO":
                    this.closeSidebarsRemoteHandler(data.data.sidebars)
                    break;

                default:
                    break;
            }
        }
    }

    private closeSidebarsRemoteHandler(sidebars: string[]) {
        const speed = 500;
        sidebars.forEach(sb => {

            switch (sb) {
                case 'sideBarConductores':
                    this.closeAllFlowSidebars(this.sidebarConductores, [])
                    break;
                case 'sideBarRestriccionesConductor':
                    this.closeAllFlowSidebars(this.sideBarRestriccionesConductor, [])
                    break;
                case 'sideBarConductorAjusteDiarioMensual':
                    this.closeAllFlowSidebars(this.sideBarConductorAjusteDiarioMensual, [])
                    break;
                case 'sideBarConductorAjusteDiarioMensual':
                    this.closeAllFlowSidebars(this.sideBarConductorAjusteDiarioMensual, [])
                    break;
                case 'sideBarCondutorEstaciones':
                    this.closeAllFlowSidebars(this.sideBarCondutorEstaciones, []);
                    break;
                case 'sideBarCondutorEstaciones':
                    this.closeAllFlowSidebars(this.sideBarCondutorEstaciones, []);
                    break;

                case 'sideBarVehiculosAsignados':
                    this.closeAllFlowSidebars(this.sideBarVehiculosAsignados, []);
                    break;
                case 'sideBarVehiculosAsignacion':
                    this.closeAllFlowSidebars(this.sideBarVehiculosAsignacion, []);
                    break;
                case 'sideBarVehiculoCrear':
                    this.closeAllFlowSidebars(this.sideBarVehiculoCrear, []);
                    break;
                case 'sideBarVehiculoDetalle':
                    this.closeAllFlowSidebars(this.sideBarVehiculoDetalle, [])
                    break;
                case 'sideBarDetalleCuenta':
                    this.closeAllFlowSidebars(this.sideBarDetalleCuenta, [])
                    break;
                case 'sideBarCuentaCorriente':
                    this.closeAllFlowSidebars(this.sideBarCuentaCorriente, [])
                    break;
                case 'sideBarTarjetaAbonoAbonar':
                    this.closeAllFlowSidebars(this.sideBarTarjetaAbonoAbonar, []);
                    break;
                case 'sideBarAbonoCuentasBancarias':
                    this.closeAllFlowSidebars(this.sideBarAbonoCuentasBancarias, []);
                    break;
                case 'sideBarAbonoTarjeta':
                    this.closeAllFlowSidebars(this.sideBarAbonoTarjeta, []);
                    break;
                case 'sideBarAbonoSeleccionMedio':
                    this.closeAllFlowSidebars(this.sideBarAbonoSeleccionMedio, []);
                    break;

            }

            //setTimeout(() => this.sidebarConductores.close(), speed);
            //this.closeAllFlowSidebars(sb, [])
        });
    }

    /** Cierra un modal de manera remota */
    // private closeSidebarRemoteHandler(key:string){
    //     console.log('Try close sidebar: ' + key)
    //     switch(key){
    //         case 'sideBarRestriccionesConductor':
    //             this.closeAllFlowSidebars(this.sideBarRestriccionesConductor, [])
    //             break;
    //         case 'sideBarRestriccionesConductor && sideBarConductores':
    //             this.closeAllFlowSidebars(this.sideBarRestriccionesConductor, ['sideBarConductores'])
    //             break;
    //         case 'sideBarConductorAjusteDiarioMensual':
    //             this.closeAllFlowSidebars(this.sideBarConductorAjusteDiarioMensual, [])
    //             break;
    //         case 'sideBarConductorAjusteDiarioMensual && sideBarConductores':
    //             this.closeAllFlowSidebars(this.sideBarConductorAjusteDiarioMensual, ['sideBarConductores'])
    //             break;
    //         case 'sideBarCondutorEstaciones':
    //             this.closeAllFlowSidebars(this.sideBarCondutorEstaciones, []);
    //             break;
    //         case 'sideBarCondutorEstaciones && sideBarConductores':
    //             this.closeAllFlowSidebars(this.sideBarCondutorEstaciones, ['sideBarConductores']);
    //             break;

    //         case 'sideBarVehiculosAsignados':
    //             this.closeAllFlowSidebars(this.sideBarVehiculosAsignados, []);
    //             break;
    //         case 'sideBarVehiculosAsignacion':
    //             this.closeAllFlowSidebars(this.sideBarVehiculosAsignacion, []);
    //             break;
    //         case 'sideBarVehiculosAsignacion && sideBarVehiculosAsignados':
    //             this.closeAllFlowSidebars(this.sideBarVehiculosAsignados, ['sideBarVehiculosAsignacion'])
    //             break;
    //         case 'sideBarVehiculoCrear':
    //             this.closeAllFlowSidebars(this.sideBarVehiculoCrear, []);
    //             break;

    //         case 'sideBarVehiculosAsignados && sideBarConductores':
    //             this.closeAllFlowSidebars(this.sideBarVehiculosAsignados, ['sideBarConductores']);
    //             break;

    //         case 'sideBarVehiculoDetalle':
    //             this.closeAllFlowSidebars(this.sideBarVehiculoDetalle, [])
    //             break;

    //         case 'sideBarCuentaCorriente':
    //             this.closeAllFlowSidebars(this.sideBarCuentaCorriente, []);
    //             break;

    //     }
    // }



    //FACTURACION ACTIONS

    guardarDatosFacturacion() {
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: cuentaId,
                datosFacturacionRut: this._configuracionFacturacion.data.dteRut,
                datosFacturacionEmail: this._configuracionFacturacion.data.dteEmail,
                datosFacturacionGiro: this._configuracionFacturacion.actividades.filter(x => x.codigo == this._configuracionFacturacion.data.dteActividadEconomica)[0].descripcion,
                datosFacturacionDistrito: this._configuracionFacturacion.data.dteDistrito,
                datosFacturacionDireccion: this._configuracionFacturacion.data.dteDireccion,
                datosFacturacionRazonSocial: this._configuracionFacturacion.data.dteRazonSocial,
                datosFacturacionCodigoPostal: '',
                datosFacturacionNombreFantasia: this._configuracionFacturacion.data.dteNombreFantasia,
                datosFacturacionActividadEconomica: this._configuracionFacturacion.data.dteActividadEconomica,
                datosFacturacionPeriodoFacturacion: this._configuracionFacturacion.data.dtePeriodoFacturacion,

                datosFacturacionNserie: this._configuracionFacturacion.data.dteNumeroSerie,
                datosFacturacionDeclaracion: this._configuracionFacturacion.data.declarationSuccess,

                restringeEleccionDte: this._configuracionFacturacion.data.dteRestringeEleccion == 'si' ? 1 : 0,
                dtePuedeFacturar: 1

            }
        };

        this.utils.showSpinner();
        this.rx.facturacionCrearDatos(action).then(result => {
            this.closeAllFlowSidebars(this.sideBarConfiguracionFacturacion, []);
            this.emiter.emitCallback({
                from: Paginas.HomeMuevoMiFacturacion,
                action: 'refresh'
            });
            this.utils.hideSpinner();
        }, this.errorCallback);
    }

    private setSelectedEmpresa() {
        if (this.empresas.length > 1) {
            this._dropEmpresa = true;
            // this.empresas.forEach(element => {
            //     if (element.cuentaDefault) {
            //         this.empresaActual = element.cuentaNombre;
            //         this.ctacte.cuentaId = element.cuentaId;
            //     }
            // });
        } else if (this.empresas.length > 0) {
            this._dropEmpresa = false;
            this.empresaActual = this.empresas[0].cuentaNombre;
            this.ctacte.cuentaId = this.empresas[0].cuentaId;
        } else {
            this._dropEmpresa = false;
            this.empresaActual = "Sin Cuenta";
        }
    }

    cerrarSesion() {
        localStorage.clear();
        this.variablesApi.authorizationToken = null;
        this.variablesApi.equipoSecret = null;
        this.variablesApi.home = new Home();
        this.variablesApi.ingreso = new Ingreso()
        this.router.navigate([Paginas.IngresoInicioPage]);
    }

    cambiarCuenta(cuenta: Cuenta) {
        this.rx.cuentasCambiarCuentaSeleccionada(cuenta.cuentaId).then(result => {
            this.router.navigate([Paginas.IngresoInicioPage])
        }, this.errorCallback)
    }

    changeEmpresa(id) {
        // this.empresas.forEach(element => {
        //     element.cuentaDefault = false
        // });

        //this.empresas[idComponente].cuentaDefault = true;
        this.setSelectedEmpresa();
        this.accionCambiarEmpresaSeleccionada(id);
    }

    accionCambiarEmpresaSeleccionada(cuentaId: number) {
        this.utils.showSpinner("Interno");
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: cuentaId,
                telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
                usuarioId: this.variablesApi.home.usuarioLogIn.usuarioId
            }
        };

        this.maquinaHome.homeCambiarCuentaSeleccionada(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.hideSpinner("Interno");
                this.router.navigate([respuesta.paginaRespuesta]);
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                this.utils.hideSpinner("Interno");
                this.utils.msgPlain(respuesta.mensajeSalida);
            }
        });

    }

    activaDesactiva(id: number) {
        let value = this._conductorEdit.estado;
        this.maquinaModificarConductor(value ? 0 : 1, true);
    }

    activateGastoDiario(conductor: Conductor) {
        let data: any = {
            tipo: 'ConductorGastoDiario',
            conductor: conductor
        };
        this.internalListener(data);
    }

    activateVehicles(conductor: Conductor) {
        let data: any = {
            tipo: 'VehiculosAsignacion',
            conductor: conductor
        };
        this.internalListener(data);
    }

    activateDiasHorarios() {
        let data: any = {
            tipo: 'DiaHorario',
            diaHorario: true
        };
        this.utils.pintaValidacion(document.getElementById("spDias"), "123", 'sidebar', 1);
        this.internalListener(data);
    }

    activateEstacionesBloqueadas() {
        let data: any = {
            tipo: 'Estacion',
            estacion: true
        };

        this.internalListener(data);
    }

    apagaRestriccionInvite() {
        if (this.invite_conduce) {
            this.invite_restriccion_combustible = false;
        }
    }

    accionInviteUsuario() {

        let ingresa: boolean = true;

        let actualUrl = this.router.url.substr(1);

        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: [{
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                rolId: this.idRol,
                estado: this.invite_restriccion_combustible ? 1 : 0,
                montoMaximoDiaValor: this.invite_restriccion_DiaValor.replace(/\./g, ""),
                montoMaximoMesValor: this.invite_restriccion_MesValor.replace(/\./g, ""),
                usuarioCuentaConduce: this.invite_conduce,
                invitacionTelefonoId: "569" + this.invite_telefono.replace(/\+/g, ""),
                invitacionNombre: this.invite_nombre,
                invitacionApellido: this.invite_apellido

            }]
        }
        if (this.invite_telefono.length == 8 && this.invite_nombre != "" && this.invite_apellido != "") {
            ingresa = true;
            this.utils.pintaValidacion(document.getElementById("dv_invite_telefono"), this.invite_telefono.replace(/\+/g, ""));
            this.utils.pintaValidacion(document.getElementById("dv_invite_nombre"), this.invite_nombre);
            this.utils.pintaValidacion(document.getElementById("dv_invite_apellido"), this.invite_apellido);
            if (this.invite_conduce) {
                if (this.invite_restriccion_DiaValor == "" || this.invite_restriccion_MesValor == "") {
                    ingresa = false;
                }
                this.utils.pintaValidacion(document.getElementById("dv_invite_restriccion_DiaValor"), this.invite_restriccion_DiaValor);
                this.utils.pintaValidacion(document.getElementById("dv_invite_restriccion_MesValor"), this.invite_restriccion_MesValor);
            }
        } else {
            ingresa = false;
            if (this.invite_telefono.length != 8) {
                this.utils.pintaValidacion(document.getElementById("dv_invite_telefono"), "");
            } else {
                this.utils.pintaValidacion(document.getElementById("dv_invite_telefono"), this.invite_telefono.replace(/\+/g, ""));
            }
            this.utils.pintaValidacion(document.getElementById("dv_invite_nombre"), this.invite_nombre);
            this.utils.pintaValidacion(document.getElementById("dv_invite_apellido"), this.invite_apellido);
            if (this.invite_conduce) {
                this.utils.pintaValidacion(document.getElementById("dv_invite_restriccion_DiaValor"), this.invite_restriccion_DiaValor);
                this.utils.pintaValidacion(document.getElementById("dv_invite_restriccion_MesValor"), this.invite_restriccion_MesValor);
            }
        }

        if (ingresa) {
            this.utils.pintaValidacion(document.getElementById("dv_invite_telefono"), this.invite_telefono.replace(/\+/g, ""));
            this.utils.pintaValidacion(document.getElementById("dv_invite_nombre"), this.invite_nombre);
            this.utils.pintaValidacion(document.getElementById("dv_invite_apellido"), this.invite_apellido);
            if (this.invite_conduce) {
                this.utils.pintaValidacion(document.getElementById("dv_invite_restriccion_DiaValor"), this.invite_restriccion_DiaValor);
                this.utils.pintaValidacion(document.getElementById("dv_invite_restriccion_MesValor"), this.invite_restriccion_MesValor);
            }
            this.utils.showSpinner("Interno");
            this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
                this.utils.hideSpinner("Interno");
                if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                    if (respuesta.paginaRespuesta == Paginas.ErrorNOK) {

                        let confirma: boolean = false;
                        confirma = this.utils.msgPlain({ tipoMensaje: "confirm", tituloMensaje: "Error de Ingreso", textoMensaje: "Este usuario ya existe en su cuenta, \n ¿Desea Modificar su rol a " + this.titleInvitacion + "?" })

                        if (confirma) {
                            let conductores = this.variablesApi.home.conductores.filter(r => r.telefonoId == ("569" + this.invite_telefono));
                            let otrosUsuarios = this.variablesApi.home.usuarios.filter(r => r.telefonoId == ("569" + this.invite_telefono));
                            let usuario: Conductor;
                            if (conductores.length > 0) {
                                usuario = conductores[0];
                            } else if (otrosUsuarios.length > 0) {
                                usuario = otrosUsuarios[0];
                            }
                            usuario.restriccionCombustible.montoMaximoDia = parseInt(this.invite_restriccion_DiaValor.replace(/\./g, ""));
                            usuario.restriccionCombustible.montoMaximoMes = parseInt(this.invite_restriccion_MesValor.replace(/\./g, ""));
                            this.maquinaModificarUsuarioInvitado(usuario, 1)
                        } else {
                            this.router.navigate([Paginas.HomeMuevoMisRolesYPerfiles])
                        }
                    } else {
                        this.router.navigate([Paginas.HomeMuevoMisRolesYPerfiles]);
                    }
                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                    this.utils.hideSpinner("Interno");
                    let confirma: boolean = false;
                    confirma = this.utils.msgPlain({ tipoMensaje: "confirm", tituloMensaje: "Error de Ingreso", textoMensaje: "Este usuario ya existe en su cuenta, \n ¿Desea Modificar su rol a " + this.titleInvitacion + "?" })

                    if (confirma) {
                        let conductores = this.variablesApi.home.conductores.filter(r => r.telefonoId == ("569" + this.invite_telefono));
                        let otrosUsuarios = this.variablesApi.home.usuarios.filter(r => r.telefonoId == ("569" + this.invite_telefono));
                        let usuario: Conductor;
                        if (conductores.length > 0) {
                            usuario = conductores[0];
                        } else if (otrosUsuarios.length > 0) {
                            usuario = otrosUsuarios[0];
                        }
                        usuario.restriccionCombustible.montoMaximoDia = parseInt(this.invite_restriccion_DiaValor.replace(/\./g, ""));
                        usuario.restriccionCombustible.montoMaximoMes = parseInt(this.invite_restriccion_MesValor.replace(/\./g, ""));
                        this.maquinaModificarUsuarioInvitado(usuario, 1)
                    } else {
                        this.router.navigate([Paginas.HomeMuevoMisRolesYPerfiles])
                    }
                    // this.utils.msgPlain(respuesta.mensajeSalida);
                }
            });
        }
    }

    maquinaModificarUsuarioInvitado(usuario: Conductor, estado: number) {
        this.utils.showSpinner("Interno");
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                usuarioId: usuario.usuarioId,
                //grupoCuentaId: (usuario.grupoId == null) ? 0 : usuario.grupoId,
                usuarioCuentaEstadoId: usuario.estado ? 1 : 0,
                rolId: this.idRol,
                usuarioCuentaConduce: estado,
                // usuarioCuentaRindeGasto: usuario.usuarioCuentaRindeGasto,
                // usuarioCuentaCreaVehiculo: usuario.usuarioCuentaCreaVehiculo,
                // usuarioCuentaEligeDTE: usuario.usuarioCuentaEligeDTE,
                // usuarioCuentaSeleccionado: usuario.usuarioCuentaSeleccionado,
                vehiculosAsignados: [],
                estadoCombustible: usuario.estado,
                montoMaximoDiaValor: usuario.restriccionCombustible.montoMaximoDia,
                montoMaximoMesValor: usuario.restriccionCombustible.montoMaximoMes
            }
        }
        if (usuario.restriccionCombustible.montoMaximoDia > 0 && usuario.restriccionCombustible.montoMaximoMes[0] > 0) {
            this.maquinaHome.homeUsuarioModificar(action).then(respuesta => {
                if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                    this.utils.hideSpinner("Interno");
                    this.router.navigate([Paginas.HomeMuevoMisRolesYPerfiles]);
                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                    this.utils.hideSpinner("Interno");
                    this.utils.openModal(respuesta.modalSalida).then(response => {
                        console.log(response);
                    });
                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                    this.utils.hideSpinner("Interno");
                    this.utils.msgPlain(respuesta.mensajeSalida);
                }
            });
        } else {
            this.utils.msgPlain({ tipoMensaje: "warning", tituloMensaje: "", textoMensaje: "No se puede modificar el conductor, si las restricciones estan activas, estas son Obligatorias" });
        }

    }

    accionCuentaCorriente() {
        this.ctacte.cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.ctacte.cuentaId,
                cuentaCorrienteNumero: parseInt(this.ctacte.nroCuenta),
                cuentaCorrienteRut: this.ctacte.rut.replace(/\./g, '').replace(/\-/g, ''),
                //cuentaCorrienteNombreTitular: this.ctacte.titular,
                //cuentaCorrienteEmailTitular: this.ctacte.email,
                tipoCuentaBancariaId: this.ctacte.tipoCuenta,
                bancoId: this.ctacte.banco
            }
        };

        if (this.ctacte.cuentaId > 0 && !isNaN(parseInt("" + this.ctacte.nroCuenta)) && this.ctacte.rut.replace(/\./g, '').replace(/\-/g, '') != "" && this.ctacte.banco) {

            this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_nroCuenta"), "" + this.ctacte.nroCuenta);
            this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_rut"), this.ctacte.rut.replace(/\./g, '').replace(/\-/g, ''));
            this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_tipoCuenta"), this.ctacte.tipoCuenta);
            //this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_email"), this.ctacte.email);
            this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_banco"), this.ctacte.banco);


            let modalData = {
                title: '¿Estás seguro que tus datos son correctos?',
                messages: [
                    this.bancos.filter(x => x.bancoId.toString() == this.ctacte.banco)[0].bancoNombre,
                    this.tiposCuentaBancaria.filter(x => x.tipoCuentaBancariaId.toString() == this.ctacte.tipoCuenta)[0].tipoCuentaBancariaNombre,
                    'Nº ' + this.ctacte.nroCuenta,
                    'RUT: ' + this.rutPipe.transform(this.ctacte.rut),
                    '',
                    'Si tus datos son incorrectos lamentablemente no podremos asignar este dinero a tu cuenta.'
                ],

                ok: 'Estoy seguro',
                cancel: 'Revisar'
            }
            let modalRef = this.utils.openModal(Paginas.ModalMuevo, modalData).then(result => {
                if (result == OpcionesModal.ACEPTAR) {
                    this.utils.showSpinner("Interno");
                    this.rx.mediosPagoAsociarCtaCte(action).then(result => {
                        this.utils.hideSpinner("Interno");

                        if (this.utils.isNullOrWithSpace(this.cuentacorrienteNuevaOrigin))
                            this.router.navigate([Paginas.HomeMuevoMisMetodosPago, this.ctacte.muestraMenu]);
                        else if (this.cuentacorrienteNuevaOrigin == Paginas.ModalMediosPagoCuentasBancarias) {
                            //refresca el listado
                            this.emiter.emitCallback({
                                from: Paginas.ModalMediosPagoCrearCuentasBancarias,
                                action: 'refresh'
                            });

                            //cierra el modal actual
                            let config: IEmmitModel = {
                                tipo: 'CERRAR MODAL REMOTO',
                                action: 'CLOSE',
                                data: {
                                    sidebars: ['sideBarCuentaCorriente'],
                                }
                            }

                            this.emiter.emmitChange(config);
                        } else {
                            //cierra el modal actual
                            let config: IEmmitModel = {
                                tipo: 'CERRAR MODAL REMOTO',
                                action: 'CLOSE',
                                data: {
                                    sidebars: ['sideBarCuentaCorriente'],
                                }
                            }

                            this.emiter.emmitChange(config);
                        }
                    }, this.errorCallback)

                }
            })

        } else {
            this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_nroCuenta"), isNaN(parseInt("" + this.ctacte.nroCuenta)) ? "" : "" + this.ctacte.nroCuenta);
            this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_rut"), this.ctacte.rut.replace(/\./g, '').replace(/\-/g, ''));
            this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_tipoCuenta"), this.ctacte.tipoCuenta);
            //this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_email"), this.ctacte.email);
            this.utils.pintaValidacion(document.getElementById("dv_cuentaCorriente_banco"), this.ctacte.banco);
        }
    }

    accionInicio() {
        this.utils.showSpinner("Interno");
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_INICIO,
            datos: {}
        };

        this.maquinaHome.homeInicio(action).then(respuesta => {

            this.utils.hideSpinner("Interno");
        });

    }

    goToInvitacion() {
        this.router.navigate([Paginas.HomeMuevoMiPerfil, 2]);
    }

    private maquinaCrearGrupo() {
        if (this.inputNvaGrupo !== "") {
            this.utils.showSpinner("Interno");
            let actualUrl = this.router.url.substr(1);
            let action: RequestTipo = {
                accion: AccionTipo.ACCION_CLICK,
                datos: {
                    cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                    grupoCuentaNombre: this.inputNvaGrupo
                }
            }


            this.maquinaHome.homeGrupoCrear(action).then(respuesta => {
                if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                    this.utils.hideSpinner("Interno");
                    this.ngOnInit();
                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                    this.utils.hideSpinner("Interno");
                    this.utils.openModal(respuesta.modalSalida).then(response => {

                    });
                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                    this.utils.hideSpinner("Interno");
                    this.utils.msgPlain(respuesta.mensajeSalida);
                }
            });
        }

    }


    async maquinaModificarConductor(estado: number = 1, edit: boolean) {
        let conductor: Conductor = new Conductor();
        if (edit)
            conductor = this._conductorEdit;

        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                //usuarioId: this.idConductor,
                //grupoCuentaId: (grupo == null) ? 0 : grupo.grupoId,
                usuarioCuentaEstadoId: estado,
                rolId: conductor.rolId,
                usuarioCuentaConduce: conductor.conduce,
                // usuarioCuentaRindeGasto: conductor.usuarioCuentaRindeGasto,
                // usuarioCuentaCreaVehiculo: conductor.usuarioCuentaCreaVehiculo,
                // usuarioCuentaEligeDTE: conductor.usuarioCuentaEligeDTE,
                // usuarioCuentaSeleccionado: conductor.usuarioCuentaSeleccionado,
                vehiculosAsignados: [],
                //estadoCombustible: conductor.restriccionConductor.estado,
                montoMaximoDiaValor: parseInt(("" + conductor.restriccionCombustible.montoMaximoDia).replace(/\./g, "")),
                montoMaximoMesValor: parseInt(("" + conductor.restriccionCombustible.montoMaximoMes).replace(/\./g, ""))

            }
        }
        let confirma: boolean = false;
        if (estado == 2) {
            await this.utils.msgPlainModal('Eliminar', ['Va a eliminar un conductor de manera definitiva', '¿Está seguro que desea eliminarlo?']).then(result => {
                if (result == OpcionesModal.ACEPTAR)
                    confirma = true
                console.log(confirma);
            })
        }
        else
            confirma = true

        if (confirma) {
            this.utils.showSpinner("Interno");
            this.maquinaHome.homeUsuarioModificar(action).then(respuesta => {
                if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                    this.utils.hideSpinner("Interno");
                    // this.router.navigate([Paginas.HomeMuevo]);
                    //this._conductores = false;

                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                    this.utils.hideSpinner("Interno");
                    this.utils.openModal(respuesta.modalSalida).then(response => {

                    });
                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                    this.utils.hideSpinner("Interno");
                    this.utils.msgPlain(respuesta.mensajeSalida);
                }
            });
        } else if (conductor.restriccionCombustible.montoMaximoDia > 0 && conductor.restriccionCombustible.montoMaximoMes > 0 && confirma) {
            // let maximoDiario = parseInt(("" + conductor.restriccionConductor.montoMaximoDiaValor[0]).replace(/\./g, ""));
            // let maximoMensual = parseInt(("" + conductor.restriccionConductor.montoMaximoMesValor[0]).replace(/\./g, ""));
            // if (maximoMensual > maximoDiario) {
            this.utils.showSpinner("Interno");
            this.utils.pintaValidacion(document.getElementById("sidebarConductorLimiteDiario"), '123', 'sidebar');
            this.utils.pintaValidacion(document.getElementById("sidebarConductorLimiteMensual"), '123', 'sidebar');
            this.maquinaHome.homeUsuarioModificar(action).then(respuesta => {
                if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                    this.utils.hideSpinner("Interno");
                    // this.router.navigate([Paginas.HomeMuevo]);
                    //this._conductores = false;

                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                    this.utils.hideSpinner("Interno");
                    this.utils.openModal(respuesta.modalSalida).then(response => {

                    });
                } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                    this.utils.hideSpinner("Interno");
                    this.utils.msgPlain(respuesta.mensajeSalida);
                }
            });
            // } else {
            //   this.utils.pintaValidacion(document.getElementById("sidebarConductorLimiteDiario"), "", 'sidebar');
            //   this.utils.pintaValidacion(document.getElementById("sidebarConductorLimiteMensual"), "", 'sidebar');
            //   this.utils.msgPlain({ tipoMensaje: "alert", textoMensaje: "El monto diario no puede ser mayor que el monto mensual" });
            // }
        } else {
            this.utils.pintaValidacion(document.getElementById("sidebarConductorLimiteDiario"), conductor.restriccionCombustible.montoMaximoDia.toString(), 'sidebar');
            this.utils.pintaValidacion(document.getElementById("sidebarConductorLimiteMensual"), conductor.restriccionCombustible.montoMaximoMes.toString(), 'sidebar');
        }
    }

    activaCombustible() {
        this.invitado.activaCombustible = !this.invitado.activaCombustible;
        this.enableSaveInvitaciones();
    }

    activaAlimentacion() {
        this.invitado.activaAlimentacion = !this.invitado.activaAlimentacion;
        this.enableSaveInvitaciones();
    }

    enableSaveInvitaciones() {
        let _foto: boolean = false;
        let _licencia: boolean = false;
        let _combustible: boolean = false;
        let _alimentacion: boolean = false;

        if (this.invitado.invitacionValidaciones.invitacionSolicitaFoto == 1) {
            if (this.invitado.invitacionValidaciones.aceptaFoto) {
                _foto = true;
            }
        } else {
            _foto = true;
        }

        if (this.invitado.invitacionValidaciones.invitacionSolicitaLicencia == 1) {
            if (this.invitado.invitacionValidaciones.aceptaLicencia) {
                _licencia = true;
            }
        } else {
            _licencia = true;
        }

        if (this.invitado.activaCombustible) {
            if (this.invitado.combustibleMontoDiario > 0 && this.invitado.combustibleMontoMensual > 0) {
                _combustible = true;
            }
        } else {
            _combustible = true;
        }

        if (this.invitado.activaAlimentacion) {
            if (this.invitado.alimentacionMontoDiario > 0 && this.invitado.alimentacionMontoMensual > 0) {
                _alimentacion = true;
            }
        } else {
            _alimentacion = true;
        }

        if (_foto && _licencia && _combustible && _alimentacion) {
            this._enableSaveInvitaciones = true;
        } else {
            this._enableSaveInvitaciones = false;
        }

    }

    autorizarInvitacion(invitacionId: number, autorizacion: number) {
        this.utils.showSpinner("Interno");
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                invitacionId: invitacionId,
                invitacionAutorizada: autorizacion,
                combustibleEstado: this.invitado.activaCombustible ? 1 : 0,
                combustibleMontoMaximoDiaValor: (this.invitado.activaCombustible) ? this.invitado.combustibleMontoDiario : 0,
                combustibleMontoMaximoMesValor: (this.invitado.activaCombustible) ? this.invitado.combustibleMontoMensual : 0
            }
        }

        this.maquinaHome.homeUsuarioAutorizaInvitacion(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.hideSpinner("Interno");
                this.router.navigate([Paginas.HomeMuevoMisConductores]);
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                this.utils.openModal(respuesta.modalSalida).then(response => {
                    this.utils.hideSpinner("Interno");

                });
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                this.utils.hideSpinner("Interno");
                this.utils.msgPlain(respuesta.mensajeSalida);
            }
        });
    }

    saveRestriccionEstaciones() {
        this.utils.showSpinner("Interno");
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                //usuarioId: this.idConductor,
                diaNoHabilitadoEstado: 0,
                diaNoHabilitadoValor: [],
                horarioNoHabilitadoEstado: 0,
                horarioNoHabilitadoValor: [],
                estacionNoHabilitadaEstado: this._estacionNoHabilitadaEstado,
                estacionNoHabilitadaValor: this._estacionNoHabilitadaValor,
            }
        }

        this.maquinaHome.homeUsuarioCuentaModificarRestricciones(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.hideSpinner("Interno");
                // this.router.navigate([Paginas.HomeMuevoMisConductores]);
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                this.utils.openModal(respuesta.modalSalida).then(response => {
                    this.utils.hideSpinner("Interno");

                });
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                this.utils.hideSpinner("Interno");
                this.utils.msgPlain(respuesta.mensajeSalida);
            }
        });
    }

    closeSidebarSeleccionTarjeta() {
        this.emiter.emitCallback({
            from: Paginas.HomeMuevoMisMetodosPago,
            action: 'refresh'
        });
        this.closeAllFlowSidebars(this.sidebarSeleccionTarjeta, []);
    }

    seleccionaTarjeta(i) {
        let compartida = this._seleccionTarjeta.seleccionada.idInscripcion;

        let saldo: number = this._seleccionTarjeta.flags._billetera ? 1 : 0;
        let estacion: number = this._seleccionTarjeta.flags._atendedor ? 1 : 0;
        let oneClick: number = 1; //this._seleccionTarjeta.flags._oneClick ? 1 : 0;

        this.homeCuentaModificarMedioPago(saldo, estacion, oneClick, result => {
            this.closeAllFlowSidebars(this.sidebarSeleccionTarjeta, []);
            this.emiter.emitCallback({
                from: Paginas.HomeMuevoMisMetodosPago,
                action: 'refresh'
            });
            this.utils.hideSpinner("Interno");
        });

    }

    nvaTarjeta() {
        this.utils.showSpinner("Interno");

        this.rx.mediosPagoNuevaTarjeta(this.variablesApi.home.cuentaSeleccionada.cuentaId).then((result: any) => {
            window.location.href = result.data.url;
        }, this.errorCallback);
    }

    homeCuentaModificarMedioPago(saldo: number, estacion: number, oneClick: number, fxSuccess) {
        let compartida: Tarjeta = this._seleccionTarjeta.seleccionada;

        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                formaPagoSaldo: saldo,
                formaPagoEstacion: estacion,
                formaPagoOneclick: oneClick,
                tarjetaCompartidaId: compartida.idInscripcion || "",
                tarjetaCompartidaUltimos4Digitos: compartida.ultimos4Digitos || "",
                tarjetaCompartidaTipo: compartida.tipoTarjetaCredito || "",
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            }
        };

        this.utils.showSpinner("Interno");
        this.rx.mediosPagoModificarMedioPago(action).then(fxSuccess, this.errorCallback);
    }

    validaInput(ingreso: any) {
        let item = ingreso.srcElement;
        if (item.maxLength > 0) {
            item.value = this.utils.formatMaxInput(item.value, item.maxLength);
        }
        item.value = this.utils.formatNumbers(item.value);
        item.value = this.utils.formatCurrency("" + item.value);
    }

    currency(val) {
        return this.utils.formatCurrency("" + val);
    }

    formatRut(val: string) {
        let dv = val.slice(-1)
        let body = val.substr(0, (val.length - 1));
        return body + '-' + dv;
    }

    validaMaxInputRut(ingreso: any) {
        let item = ingreso.srcElement;
        item.value = this.utils.formatMaxInput(item.value, item.maxLength);
        item.value = item.value.replace(!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/g, '').replace(/(\..*)\./g, '$1');
    }

    validaMaxInputText(ingreso: any) {
        this.utils.validaMaxInputText(ingreso);
    }

    validaInputTextEmail(ingreso: any) {
        this.utils.validaMaxInputMail(ingreso);
    }

    onlyNumber(ingreso: any) {
        this.utils.validaMaxInputNumber(ingreso);
        // let item = ingreso.srcElement;
        // if (item.maxLength > 0) {
        //     item.value = this.utils.formatMaxInput(item.value, item.maxLength);
        // }
        // item.value = this.utils.formatNumbers(item.value);
    }

    validaAnoSelect(ingreso: any) {
        let item = ingreso.srcElement;

        if (parseInt(item.value) == 0) {
            this.utils.pintaValidacion(document.getElementById("aliasAnoSidebar"), "");
        } else {
            this.utils.pintaValidacion(document.getElementById("aliasAnoSidebar"), item.value);
        }
    }

    validaTipoSelect(ingreso: any) {
        let item = ingreso.srcElement;
        if (parseInt(item.value) == 0) {
            this.utils.pintaValidacion(document.getElementById("aliasTipoSidebar"), "");
        } else {
            this.utils.pintaValidacion(document.getElementById("aliasTipoSidebar"), item.value);
        }
    }

    activaBtnGuardar() {
        this.btnGuardarSidebar = false;
    }


    levantarModalPerfil() {
        var mod = this.modalSidebar.open(MiPerfilSidebarComponent,);
        mod.refModal.result.then(() => {
            //this.updateData()
        }, () => {
            // this.updateData()
        })
    }

    levantarModalNotificaciones() {
        var body = {
            "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
        };

        body["tipoConsulta"] = "POR_LEER";
        this.utils.showSpinner("Interno")
        this.rx.llamarPOSTHome(body, this.constantes.kUsuarioConsultarNotificaciones).then(result => {

            console.log(result.data)
            var mod = this.modalSidebar.open(NotificacionesComponent);
            mod.refModal.componentInstance.notificaciones = result.data.data.notificaciones
            mod.refModal.componentInstance.posicionFinal = result.data.data.posicionFinal
            mod.refModal.componentInstance.cantidadRegistrosPorConsulta = result.data.data.cantidadRegistrosPorConsulta

            mod.refModal.result.then(() => {
                this.obtenerVariable()
            }, () => {
                this.obtenerVariable()
            })

            this.utils.hideSpinner("Interno");
        }, this.errorCallback)


    }


    obtenerVariable(){
        this.rx.obtenerWeb().then((result: any) => {
            this.variablesApi.home.cuentaSeleccionada = result.data.cuentaSeleccionada;
            this.variablesApi.home.saldo = result.data.saldo;
            //console.log('cuenta seleccionada: ', this.variablesApi.home.cuentaSeleccionada.cuentaNombre)
            this.variablesApi.home.invitaciones = result.data.invitaciones;
            this.variablesApi.home.usuarioLogIn = result.data.usuarioLogin;
            this.variablesApi.home.vehiculos = result.data.vehiculos;
            this.variablesApi.home.conductores = result.data.conductores;
            this.variablesApi.home.usuarios = result.data.usuarios;
            this.variablesApi.home.facturasPendientesPago = result.data.facturasPendiente;
            this.variablesApi.home.primerIngreso = result.data.primerIngreso;
            this.variablesApi.home.montoTotalMes = result.data.montoTotalMes;
            this.variablesApi.home.tipoFormaPago = result.data.tiposFormaPago;
            this.variablesApi.home.menu = result.data.menu;
            this.variablesApi.home.cuentas = result.data.cuentas;
            this.variablesApi.home.mediosPago = result.data.mediosPago;
            this.variablesApi.home.datosSaldo = result.data.datosSaldo;
            this.variablesApi.home.tipoDocumento = result.data.tipoDocumento;
            this.variablesApi.home.cuentaDatosDte = result.data.cuentaDatosDte;
            this.variablesApi.home.notificaciones = result.data.notificaciones;
            this.variablesApi.home.fuerzaCambioCuenta = result.data.fuerzaCambioCuenta;
            this.variablesApi.home.rolPrivilegiosFront = result.data.rolPrivilegiosFront;
        })
    }


}
