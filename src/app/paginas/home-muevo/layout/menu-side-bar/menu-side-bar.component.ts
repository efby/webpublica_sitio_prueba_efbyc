import { Component, OnInit, Input } from '@angular/core';
import { RolPrivilegiosFront, VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
    selector: 'app-menu-side-bar',
    templateUrl: './menu-side-bar.component.html',
    styleUrls: ['./menu-side-bar.component.scss']
})
export class MenuSideBarComponent implements OnInit {

    @Input()
    isOpened: boolean;

    @Input()
    isMobile: boolean;

    menuItems: any[];

    constructor(private variablesApi: VariablesGenerales,) { }

    ngOnInit() {
        this.createMenuItems();
        console.log("RolPrivilegiosFront")
        console.log(this.variablesApi.home.rolPrivilegiosFront)
    }

    createMenuItems() {
        this.menuItems = []
        this.menuItems.push({
            title: '',
            img: './assets/css/cromo-blue/logo_muevo.png',
            url: '/HomeMuevo/Dashboard'
        })

        if (this.variablesApi.home.rolPrivilegiosFront.conductores.verEditar > 0) {
            this.menuItems.push({
                title: 'Conductores',
                img: './assets/img/conductor-white-blue@3x.png',
                url: '/HomeMuevo/MisConductores'
            })
        }
        if (this.variablesApi.home.rolPrivilegiosFront.vehiculos.verEditar > 0) {
            this.menuItems.push({
                title: 'Vehículos',
                img: './assets/css/cromo-blue/vehiculos.png',
                url: '/HomeMuevo/MisVehiculos'
            })
        }

        if (this.variablesApi.home.rolPrivilegiosFront.administradores.verEditar > 0) {
            this.menuItems.push({
                title: 'Administradores',
                img: './assets/css/cromo-blue/conductores.png',
                url: '/HomeMuevo/MisAdministradores'
            })
        }

        this.menuItems.push({
            title: 'Historial',
            img: './assets/css/cromo-blue/documentos.png',
            url: '/HomeMuevo/MisTransacciones'
        })

        if (this.variablesApi.home.rolPrivilegiosFront.formasDePago.verEditar > 0) {
            this.menuItems.push({
                title: 'Formas de Pago',
                img: './assets/css/cromo-blue/metpago.png',
                url: '/HomeMuevo/MisMetodosPago/M'
            })
        }


        if (this.variablesApi.home.rolPrivilegiosFront.facturacion.verEditar > 0) {
            this.menuItems.push({
                title: 'Facturación',
                img: './assets/css/cromo-blue/facturacion.png',
                url: '/HomeMuevo/MiFacturacion'
            })
        }




    }

}
