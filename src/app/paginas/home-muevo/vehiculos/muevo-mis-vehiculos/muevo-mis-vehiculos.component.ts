import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { Vehiculos } from "src/app/models/vehiculos";
import { VariablesGenerales } from "src/app/globales/variables-generales";
import {
  BreadCrumbsService,
  IEmmitCallbackModel,
  IEmmitModel,
} from "src/app/servicios/bread-crumbs.service";
import { Router } from "@angular/router";
import { UtilitariosService } from "src/app/servicios/utilitarios.service";
import { MaquinaHome } from "src/app/maquinas/maquina-home";
import { Paginas } from "src/app/globales/paginas";
import { PageBase } from "../../pageBase";
import { MatTableDataSource, MatPaginator, MatSort } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";
import { fadeOutToRight } from "src/app/animations/fade-right";
import { RxService } from "src/app/servicios/rx.service";
import {
  Vehiculo,
  IPagination,
  TableDataCsv,
} from "src/app/globales/business-objects";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ExportDataService } from "src/app/servicios/export-data.service";

import * as moment from "moment";
import "moment/min/locales";

@Component({
  selector: "app-muevo-mis-vehiculos",
  templateUrl: "./muevo-mis-vehiculos.component.html",
  styleUrls: ["./muevo-mis-vehiculos.component.scss"],
  // animations: [fadeOutToRight], // make fade in animation available to this component
  // host: { '[@fadeOutToRight]': '' }
})
export class MuevoMisVehiculosComponent extends PageBase implements OnInit {
  CURRENT_PAGE = Paginas.HomeMuevoMisVehiculos;

  migajas = [
    { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Mis Vehículos", class: "active", url: "" },
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private unsubscribe = new Subject();
  callbackSubscripcion: any;

  displayedColumns = [
    "patente",
    "alias",
    "tipoString",
    "marca",
    "modelo",
    "ano",
    "estado",
  ];
  dataSource: MatTableDataSource<Vehiculo> = new MatTableDataSource<Vehiculo>();
  selection = new SelectionModel<Vehiculo>(true, []);

  vehiculos: Vehiculo[] = [];

  pagination: IPagination;

  constructor(
    private rx: RxService,
    public variablesApi: VariablesGenerales,
    private emiter: BreadCrumbsService,
    public router: Router,
    public utils: UtilitariosService,
    private maquinaHome: MaquinaHome,
    private ref: ChangeDetectorRef
  ) {
    super(ref, utils, router);

    this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });

    this.pagination = {
      currentPage: 0,
      pages: 0,
      totalRows: 0,
      positionFinal: 0,
      pagesSize: 0,
      rows: 0,
    };
  }

  ngOnInit() {
    this.callbackSubscripcion = this.emiter.callbackEmmited$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(this.emitterHandler);
    this.setPage(1);
  }

  next() {
    this.cargarVehiculos(this.pagination);
  }

  cargarVehiculos(paginacion: IPagination) {
    let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

    //this.utils.showSpinner();
    this._isLoading = true;
    this.rx
      .vehiculosGet(cuentaId, this.pagination.positionFinal)
      .then((result: any) => {
        //this.utils.hideSpinner();
        this._isLoading = false;

        this.vehiculos = this.vehiculos.concat(result.data.vehiculos);
        //this.vehiculos = result.data.vehiculos;

        this.dataSource = new MatTableDataSource(this.vehiculos);
        this._configureFunctionOfMatTable(
          this.dataSource,
          this.paginator,
          this.sort
        );

        this.paginationHandler(result.data.pagination);
      }, this.errorCallback);
  }

  mostrarVehiculo(vehiculo: Vehiculo) {
    event.preventDefault();
    this.emiter.emitChange({
      tipo: "VehiculoDetalle",
      Vehiculos: true,
      vehiculoId: vehiculo.vehiculoId,
    });
  }

  nuevoVehiculo() {
    event.preventDefault();
    event.stopPropagation();

    let model: IEmmitModel = {
      tipo: "VehiculoCrear",
      action: "OPEN",
      data: {
        origin: this.CURRENT_PAGE,
      },
    };
    this.emiter.emmitChange(model);
  }

  exportCsv() {
    let data: TableDataCsv = {
      header: ["PATENTE", "ALIAS", "TIPO", "MARCA", "MODELO", "AÑO"],
      rows: this.dataSource.data.map((x: Vehiculo) => {
        return [x.patente, x.alias, x.tipoString, x.marca, x.modelo, x.anio];
      }),
    };

    ExportDataService.ExportCsv(
      this.generateFilenameFormat(
        "vehiculos_",
        this.variablesApi.home.cuentaSeleccionada.cuentaId.toString()
      ),
      data.rows,
      data.header,
      ";",
      ",",
      "Vehiculos MUEVO EMPRESAS"
    );
  }

  private generateFilenameFormat(prefix: string, key: string) {
    return prefix + `${key}_${moment().format("YYYYMMDD_HHmm")}`;
  }

  // desactivarVehiculo(id) {
  //     let element = this.variablesApi.home.vehiculos.filter(r => r.vehiculoId === id)[0];
  //     if (element.estado) {
  //         this.maquinaModificarVehiculo(id, 0);
  //     } else {
  //         this.maquinaModificarVehiculo(id, 1);
  //     }
  // }

  // maquinaModificarVehiculo(id, estado) {
  //     let element = this.variablesApi.home.vehiculos.filter(r => r.vehiculoId === id)[0];
  //     let actualUrl = this.router.url.substr(1);
  //     let action: RequestTipo = {
  //         accion: AccionTipo.ACCION_CLICK,
  //         datos: {
  //             cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
  //             flotaCuentaId: (element.flotaId == null) ? 0 : element.flotaId,
  //             vehiculoCuentaId: element.vehiculoId,
  //             vehiculoCuentaPatente: element.patente,
  //             vehiculoCuentaModelo: element.modelo,
  //             vehiculoCuentaMarca: element.marca,
  //             vehiculoCuentaAno: (element.ano == null) ? 0 : element.ano,
  //             vehiculoCuentaTanque: element.tanque,
  //             vehiculoCuentaAlias: element.alias,
  //             vehiculoCuentaTipo: element.tipo,
  //             vehiculoCuentaValidado: 1,
  //             vehiculoCuentaEstado: estado
  //         }
  //     }
  //     let confirma: boolean = false;
  //     if (estado == 2) {
  //         confirma = this.utils.msgPlain({ tipoMensaje: "confirm", tituloMensaje: "Confirmacion", textoMensaje: "Va a eliminar un vehiculo de manera definitiva \n\n ¿Está seguro que desea eliminarlo?" });
  //     } else {
  //         confirma = true
  //     }

  //     if (confirma) {
  //         this.utils.showSpinner("Interno");
  //         this.maquinaHome.homeVehiculoModificar(action).then(this.callApiCallbackHandler);
  //     }
  // }

  onlyNumber(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = this.utils.formatNumbers(item.value);
  }

  maxInput(ingreso: any) {
    let item = ingreso.srcElement;
    item.value = this.utils.formatMaxInput(item.value, item.maxLength);
  }

  private emitterHandler = (data: IEmmitCallbackModel) => {
    if (
      data.from == Paginas.ModalVehiculoDetalle ||
      data.from == Paginas.ModalVehiculoCrear
    ) {
      this.pagination.positionFinal = 0;
      this.vehiculos = [];
      this.next();
    }
  };

  private paginationHandler(configResponse: any) {
    this.pagination.positionFinal = configResponse.posicionFinal;
    this.pagination.totalRows = configResponse.totalRegistros;
    this.pagination.pagesSize = configResponse.registrosPorPagina;
    this.pagination.rows = configResponse.registrosObtenidos;

    let mod = this.pagination.totalRows % configResponse.registrosPorPagina;
    this.pagination.pages =
      this.pagination.totalRows / configResponse.registrosPorPagina;

    if (mod > 0) this.pagination.pages += 1;

    this.pagination.pages = parseInt(this.pagination.pages.toString());

    this.pagination.currentPage =
      this.pagination.positionFinal / configResponse.registrosPorPagina;
    this.pagination.currentPage = parseInt(
      this.pagination.currentPage.toString()
    );
  }

  private setPage(page) {
    if (page == 0) return;
    // else if(page == 1 && this.pagination.currentPage == 1)
    //     return;
    else if (this.pagination.pages > 0 && page > this.pagination.pages) return;

    if (page >= 1) this.pagination.currentPage = page;
    this.pagination.positionFinal =
      page * this.pagination.pagesSize - this.pagination.pagesSize;

    this.cargarVehiculos(this.pagination);
  }

  ngOnDestroy() {
    if (
      this.callbackSubscripcion != undefined &&
      !this.callbackSubscripcion.closed
    )
      this.callbackSubscripcion.unsubscribe();
  }
}
