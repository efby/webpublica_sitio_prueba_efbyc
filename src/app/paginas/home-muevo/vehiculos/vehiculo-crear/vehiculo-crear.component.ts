import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { IEmmitModel, BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Vehiculo } from 'src/app/globales/business-objects';
import { PageBase } from '../../pageBase';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { FormControl, NgForm } from '@angular/forms';
import { VariablesGenerales, TipoVehiculo } from 'src/app/globales/variables-generales';
import { RxService } from 'src/app/servicios/rx.service';
import { Paginas } from 'src/app/globales/paginas';
import { NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-vehiculo-crear-modal',
    templateUrl: './vehiculo-crear.component.html',
    styleUrls: ['./vehiculo-crear.component.scss']
})
export class VehiculoCrearComponent extends PageBase implements OnInit {
    @ViewChild('vehiculoForm', { read: NgForm }) form: any;
    
    origin: string;
    declarationSuccess: boolean;

    tooltipMsg = 'Si tu objetivo es conocer el odómetro de tu vehículo para mantenciones y conocer su rendimiento, la mejor opción es solicitar un registro mensual. Esto, ya que mientras más información de cargas tengamos, más confiable será el cálculo del odómetro.';


    current:Vehiculo;
    tiposVehiculo: TipoVehiculo[];

    showCloseThis: boolean;

    constructor(private tooltip: NgbTooltipConfig, private variablesApi:VariablesGenerales, private emiter:BreadCrumbsService, public ref: ChangeDetectorRef, public utils:UtilitariosService, public router:Router, private rx:RxService) { 
        super(ref, utils, router);

        this.declarationSuccess = false;
        this.tooltip.tooltipClass = "local-tooltip";

        this.origin = "";
        this.current = new Vehiculo();
        this.showCloseThis = false;
    }
    
    ngOnInit() {
    }

    declarationChange(){
        console.log(this.declarationSuccess);
    }

    setData(origin:string){
        this.declarationSuccess = false;
        this.origin = origin;
        this.current = new Vehiculo();
        this.tiposVehiculo = this.variablesApi.home.maestros.tipoVehiculo;

        this.showCloseThis = origin != Paginas.HomeMuevoMisVehiculos;

        this.form.reset();
    }

    closeThis(){
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarVehiculoCrear']
            }
        }

        this.emiter.emmitChange(config);
    }

    closeAll(){
        event.preventDefault();
        event.stopPropagation();

        // let toClose = [];
        // if(this.origin == Paginas.HomeMuevoMisVehiculos) 
        //     toClose.push('sideBarVehiculoCrear');
        // else if(this.origin == Paginas.ModalVehiculosAsignacion)
        //     toClose.push('sideBarVehiculoCrear');
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarVehiculoCrear']
            }
        }

        if(this.origin == Paginas.ModalVehiculosAsignacion){
            config.data.sidebars.push('sideBarVehiculosAsignacion');
            config.data.sidebars.push('sideBarVehiculosAsignados');
            config.data.sidebars.push('sideBarConductores');
        }

        this.emiter.emmitChange(config);
    }

    creaVehiculo(item:Vehiculo){
        event.preventDefault();
        event.stopPropagation();

        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

        this.utils.showSpinner();
        this.rx.vehiculosCrear(cuentaId, item).then(result => {
            this.utils.hideSpinner();

            this.solitarActualizacionDesdeOrigen();
            this.closeThis();
        }, this.errorCallback);
    }

    solitarActualizacionDesdeOrigen(){
        this.emiter.emitCallback({
            from:  Paginas.ModalVehiculoCrear,
            action: 'refresh'
        });
    }

    //cambia el estado del odometro para la validacion del formulario
    toggleOdometro(){
        //this.odometroTouch = true;
        this.current.requireOdometro = !this.current.requireOdometro;
        this.current.frecuenciaOdometro = 1;
    }

    inputValidatorPatente(input:any){
        this.utils.validatePatente(input);
    }

    validaMaxInputText(input:any){
        this.utils.validaMaxInputText(input);
    }
}
