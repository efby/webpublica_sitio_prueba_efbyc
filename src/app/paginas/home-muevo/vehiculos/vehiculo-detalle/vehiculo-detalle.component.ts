import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Vehiculo } from 'src/app/globales/business-objects';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales, TipoVehiculo } from 'src/app/globales/variables-generales';
import { PageBase } from '../../pageBase';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { NgForm } from '@angular/forms';
import { BreadCrumbsService, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';
import { Constantes } from 'src/app/globales/constantes';
import { OpcionesModal } from 'src/app/globales/enumeradores';

@Component({
    selector: 'app-vehiculo-detalle',
    templateUrl: './vehiculo-detalle.component.html',
    styleUrls: ['./vehiculo-detalle.component.scss'],
    providers: [NgbTooltipConfig]
})
export class VehiculoDetalleComponent extends PageBase implements OnInit {
    @ViewChild('mainForm', { read: NgForm }) form: any;

    CURRENT_PAGE = Paginas.ModalVehiculoDetalle;

    tooltipMsg = 'Si tu objetivo es conocer el odómetro de tu vehículo para mantenciones y conocer su rendimiento, la mejor opción es solicitar un registro mensual. Esto, ya que mientras más información de cargas tengamos, más confiable será el cálculo del odómetro.';

    current: Vehiculo;
    tiposVehiculo: TipoVehiculo[];

    estadoTouch: boolean = false;
    odometroTouch: boolean = false;

    constructor(
        private tooltip: NgbTooltipConfig,
        private emiter: BreadCrumbsService,
        private constantes: Constantes,
        public ref: ChangeDetectorRef, public router: Router, public utils: UtilitariosService, private rx: RxService, public variablesApi: VariablesGenerales) {
        super(ref, utils, router);

        this.tooltip.tooltipClass = "local-tooltip";
    }

    ngOnInit() {
        this.reset();
        this.fechaMax = new Date().getFullYear() + 1;
    }

    reset() {
        this.form.reset();
        this.current = new Vehiculo();

        this.estadoTouch = false;
        this.odometroTouch = false;
    }
    fechaMax:number

    setData(vehiculoId: number) {
        this.reset();
        this.tiposVehiculo = this.variablesApi.home.maestros.tipoVehiculo;

        this.obtenerVehiculo(vehiculoId);
    }

    obtenerVehiculo(vehiculoId: number) {
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

        this.utils.showSpinner();
        this.rx.vehiculoGetVehiculo(cuentaId, vehiculoId).then((result: any) => {
            this.utils.hideSpinner();
            this.current = result.data.vehiculo;
        }, this.errorCallback);
    }

    //cambia el estado del 'estado' para la validacion del formulario
    toggleEstado() {
        this.estadoTouch = true;
        this.current.estado = !this.current.estado;
    }

    //cambia el estado del odometro para la validacion del formulario
    toggleOdometro() {
        this.odometroTouch = true;
        this.current.requireOdometro = !this.current.requireOdometro;
    }

    errorNumero:string

    validarRangoFechas(input){
        
        let item = input.srcElement;
        if( item.value <1900 || item.value >this.fechaMax){
            this.errorNumero="Fecha no válida para vehículo."
        }
        else{
            this.errorNumero=null
        }
    }

    onlyNumber(input: any) {
        let item = input.srcElement;
        if (item.maxLength > 0){
            item.value = this.utils.formatMaxInput(item.value, item.maxLength);
           
        }
       
           
        item.value = this.utils.formatNumbers(item.value);

    }

    guardarVehiculo() {
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        let estado = this.current.estado ? 1 : 0;

        this.utils.showSpinner();
        this.rx.vehiculosModificar(cuentaId, estado, this.current).then(result => {
            this.utils.hideSpinner();
            this.solitarActualizacionDesdeOrigen();
            this.backToMain();
        }, this.errorCallback);
    }

    solitarActualizacionDesdeOrigen() {
        this.emiter.emitCallback({
            from: this.CURRENT_PAGE,
            action: 'refresh'
        });
    }

    //cierra modal
    backToMain() {
        event.preventDefault();
        event.stopPropagation();

        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarVehiculoDetalle']
            }
        }

        this.emiter.emmitChange(config);
    }
    eliminar(current) {
        console.log(current)
        let msg = {
            title: 'Eliminar vehículo',
            messages: ["¿Estás seguro que deseas eliminar al vehículo  " +
                current
                    .patente +
                " de su cuenta?"],
            okText: "Eliminar"
        }

        this.utils.openModal(Paginas.ModalAceptarCancelarComponent, msg
        ).then(result => {
            switch (result) {
                case OpcionesModal.ACEPTAR:

                    var body = {
                        "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
                        "vehiculoCuentaId": current.vehiculoId,
                        "vehiculoCuentaEstado": 2
                    };

                    this.utils.showSpinner("Interno")
                    this.rx.llamarPOSTHome(body, this.constantes.kModificarVehiculoEndPoint).then(result => {

                        console.log(result.data)
                        this.solitarActualizacionDesdeOrigen();
                        this.backToMain();
                       /*  setTimeout(() => {

                            let msg_2 = {
                                title: 'Eliminar vehículo',
                                messages: [result.data.message],

                            }
                            this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {

                              
                            })
                        }, 400); */

                        this.utils.hideSpinner("Interno");
                    }, this.errorCallback)


                    break;
                case OpcionesModal.CANCELAR:
                    console.log("NOK")
                    // this.closeThis()
                    break;
            }

        });



    }

}
