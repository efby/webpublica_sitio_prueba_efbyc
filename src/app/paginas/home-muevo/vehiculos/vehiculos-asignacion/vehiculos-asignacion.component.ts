import { Component, OnInit } from '@angular/core';
import { Conductor, Vehiculo } from 'src/app/models/conductores';
import { IEmmitModel, BreadCrumbsService, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RxService } from 'src/app/servicios/rx.service';
import { IPagination } from 'src/app/globales/business-objects';

@Component({
    selector: 'app-vehiculos-asignacion',
    templateUrl: './vehiculos-asignacion.component.html',
    styleUrls: ['./vehiculos-asignacion.component.scss']
})
export class VehiculosAsignacionComponent implements OnInit {
    CURRENT_PAGE = Paginas.ModalVehiculosAsignacion;

    origin: string;
    conductor: Conductor;
    vehiculosDisponibles: Vehiculo[];
    buscarVehiculosFilter: string;

    vehiculosAsignados: Vehiculo[];

    private unsubscribe = new Subject();
    callbackSubscripcion:any;

    pagination:IPagination;

    _isLoading: boolean;

    lastPosicionFinal:number;
    
    constructor(private rx:RxService, private emiter: BreadCrumbsService, private utils:UtilitariosService, private variablesApi:VariablesGenerales) { 
        this.reset();
    }
    
    ngOnInit() {
        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);
    }

    setData(origin:string, conductor:Conductor, vehiculosDisponibles: Vehiculo[], vehiculosAsignados: Vehiculo[]){
        this.reset();
        
        this.obtenerVehiculosDisponibles();

        //*this.vehiculosDisponibles.map((x:any)=> x.selected = false)

        this.origin = origin;
        this.conductor = conductor;

        this.vehiculosAsignados = vehiculosAsignados;

        //56955222227 mail 7
        //*this.vehiculosDisponibles = this.obtenerVehiculosNoAsignados(vehiculosDisponibles, this.vehiculosAsignados);
    }

    reset(){
        this.conductor = new Conductor();
        this.vehiculosDisponibles = [];
        this.buscarVehiculosFilter = '';
        this.origin = '';
        this._isLoading = false;
        this.lastPosicionFinal = 0;

        this.pagination = {
            currentPage: 0,
            pages: 0,
            totalRows: 0, 
            positionFinal: 0,
            pagesSize: 0,
            rows: 0
        }

    }

    obtenerVehiculosNoAsignados(vehiculosDisponibles:Vehiculo[], vehiculosAsignados: Vehiculo[]){
        const asignados = vehiculosAsignados.map(x => x.vehiculoId);

        const res = vehiculosDisponibles.filter(x => asignados.indexOf(x.vehiculoId) == -1);
        return res;
    }

    mostrarCreacionVehiculo(){
        event.preventDefault();
        event.stopPropagation();

        let model: IEmmitModel = {
            tipo: 'VehiculoCrear',
            action: 'OPEN',
            data: {
                origin: Paginas.ModalVehiculosAsignacion,
            }
        }
        this.emiter.emmitChange(model);
    }

    closeAll(){
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarVehiculosAsignacion', 'sideBarVehiculosAsignados']
            }
        }

        this.emiter.emmitChange(config);
    }

    backToMain(){
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarVehiculosAsignacion']
            }
        }

        this.emiter.emmitChange(config);
    }

    asignarVehiculos(){
        
        this.emiter.emitCallback({
            from: this.CURRENT_PAGE,
            action: this.origin,
            data: this.vehiculosDisponibles.filter((x:any) => x.selected)
        });
        this.backToMain();
    }

    private emitterHandler = (data:IEmmitCallbackModel) => {
        if(data.from == Paginas.ModalVehiculoCrear){
            //si llega de este modal es por que creo un vehiculo
            this.obtenerVehiculosDisponibles();
        }
    }

    private obtenerVehiculosDisponibles(){
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

        this._isLoading = true;
        this.lastPosicionFinal = this.pagination.positionFinal;
        this.rx.vehiculosGet(cuentaId, this.pagination.positionFinal).then((result:any) => {
            //this.utils.hideSpinner();
            this._isLoading = false;
            let idsSeleccionados = this.vehiculosDisponibles.filter((x:any) => x.selected).map(x => x.vehiculoId);
  
            let nuevosNoExistentes = result.data.vehiculos.filter(x => this.utils.arrayIndexOf(this.vehiculosDisponibles, 'vehiculoId', x.vehiculoId) == -1 );

            this.vehiculosDisponibles = this.vehiculosDisponibles.concat(nuevosNoExistentes);
            this.vehiculosDisponibles = this.obtenerVehiculosNoAsignados(this.vehiculosDisponibles, this.vehiculosAsignados);
            //this.vehiculosDisponibles = result.data.vehiculos;

            this.vehiculosDisponibles.map(x => {
                if(idsSeleccionados.indexOf(x.vehiculoId) > -1)
                    x['selected'] = true;
            })

            this.paginationHandler(result.data.pagination);
        });
    }

    paginationHandler(configResponse:any){
        this.pagination.rows = configResponse.registrosObtenidos;
        this.pagination.totalRows = configResponse.totalRegistros;
        this.pagination.pagesSize = configResponse.registrosPorPagina;

        if(this.pagination.rows < this.pagination.pagesSize){
            //this.lastPosicionFinal = configResponse.posicionFinal;
            this.pagination.positionFinal = this.lastPosicionFinal; 
        }
        else{
            this.lastPosicionFinal = configResponse.posicionFinal;
            this.pagination.positionFinal = configResponse.posicionFinal;
        }

        
        
    }

    next(){
        this.obtenerVehiculosDisponibles()
    }

    ngOnDestroy() {
        if(this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }
    
}
