import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoVehiculoConfirmarComponent } from './muevo-vehiculo-confirmar.component';

describe('MuevoVehiculoConfirmarComponent', () => {
  let component: MuevoVehiculoConfirmarComponent;
  let fixture: ComponentFixture<MuevoVehiculoConfirmarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoVehiculoConfirmarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoVehiculoConfirmarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
