import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Conductor } from 'src/app/models/conductores';
import { Vehiculos } from 'src/app/models/vehiculos';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-muevo-vehiculo-confirmar',
  templateUrl: './muevo-vehiculo-confirmar.component.html',
  styleUrls: ['./muevo-vehiculo-confirmar.component.css']
})
export class MuevoVehiculoConfirmarComponent implements OnInit {

  conductores: Conductor[];
  vehiculos: Vehiculos[];
  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Crear Vehículos", class: "", url: "/" + Paginas.HomeMuevoCrearVehiculo },
    { title: "Confirmación de Vehículos", class: "active", url: "" }
  ];

  interval: any;
  isMobile: boolean = false;

  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private maquinaHome: MaquinaHome, private router: Router, private utils: UtilitariosService, private route: ActivatedRoute, private ref: ChangeDetectorRef) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.conductores = this.variablesApi.home.conductores;
    this.vehiculos = this.variablesApi.home.nvoVehiculo;
  }

  muestraDiaHorario() {
    this.emiter.emitChange({
      tipo: 'DiaHorario',
      diaHorario: true
    });
  }
  muestraEstaciones() {
    this.emiter.emitChange({
      tipo: 'Estacion',
      estacion: true
    });
  }

  // eliminarVehiculo(idVehiculo) {
  //   let idComponente = this.vehiculos.findIndex(r => r.vehiculoId === idVehiculo);
    
  //   let datos = {
  //     vehiculoCuentaId: this.vehiculos[idComponente].vehiculoId,
  //     cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
  //     flotaCuentaId: this.vehiculos[idComponente].flotaId,
  //     vehiculoCuentaPatente: this.vehiculos[idComponente].patente,
  //     vehiculoCuentaModelo: this.vehiculos[idComponente].modelo,
  //     vehiculoCuentaMarca: this.vehiculos[idComponente].marca,
  //     vehiculoCuentaAno: this.vehiculos[idComponente].ano,
  //     vehiculoCuentaTanque: this.vehiculos[idComponente].tanque,
  //     vehiculoCuentaAlias: this.vehiculos[idComponente].alias,
  //     vehiculoCuentaTipo: this.vehiculos[idComponente].tipo,
  //     vehiculoCuentaValidado: (this.vehiculos[idComponente].validado) ? 1 : 0,
  //     vehiculoCuentaEstado: 2
  //   }

  //   this.utils.showSpinner("Interno");
  //   let actualUrl = this.router.url.substr(1);
  //   let action: RequestTipo = {
  //     accion: AccionTipo.ACCION_CLICK,
  //     datos: datos
  //   }

  //   this.maquinaHome.homeVehiculoModificar(action).then(respuesta => {
  //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
  //       this.utils.hideSpinner("Interno");
  //       if (actualUrl !== respuesta.paginaRespuesta) {
  //         this.router.navigate([respuesta.paginaRespuesta]);
  //       } else {
  //         this.vehiculos.splice(idComponente, 1);
  //         this.ngOnInit();
  //       }
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
  //       this.utils.hideSpinner("Interno");
  //       this.utils.openModal(respuesta.modalSalida).then(response => {
  //         console.log(response);
  //       });
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
  //       this.utils.hideSpinner("Interno");
  //       this.utils.msgPlain(respuesta.mensajeSalida);
  //     }
  //   });

  // }

}
