import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Conductor, Vehiculo } from 'src/app/models/conductores';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { PageBase } from '../../pageBase';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { IEmmitModel, BreadCrumbsService, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-vehiculos-asignados',
    templateUrl: './vehiculos-asignados.component.html',
    styleUrls: ['./vehiculos-asignados.component.scss']
})
export class VehiculosAsignadosComponent extends PageBase implements OnInit {

    CURRENT_PAGE = Paginas.ModalVehiculosAsignados;

    origen:string;
    conductorKey: any;
    conductor: Conductor;
    vehiculosDisponibles: Vehiculo[];
    cantidadVehiculosDisponiblesParaAsignar: number;

    vehiculosDesasignados:number[];

    haveChanges:boolean = false;

    buscarVehiculosFilter: string;
    

    private unsubscribe = new Subject();
    callbackSubscripcion:any;

    get origenIsConductoresDetalle(): boolean{
        return this.origen == Paginas.ModalConductorDetalle;
    }
    
    constructor(private emiter: BreadCrumbsService, public ref:ChangeDetectorRef, public utils: UtilitariosService, public router: Router, private rx: RxService, public variablesApi: VariablesGenerales) { 
        super(ref, utils, router)
        
        
        this.reset();
    }
    
    ngOnInit() {
        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);
    }

    remove(item:Vehiculo){
        let index = this.utils.arrayIndexOf(this.conductor.vehiculosAsignados, 'vehiculoId', item.vehiculoId);
        if(index > -1){
            this.conductor.vehiculosAsignados.splice(index, 1);
            this.vehiculosDesasignados.push(item.vehiculoId);
            this.haveChanges = true;

            this.cantidadVehiculosDisponiblesParaAsignar = this.obtenerVehiculosNoAsignados(this.vehiculosDisponibles, this.conductor.vehiculosAsignados).length;
        }
    }

    setData(origin:string, conductor: Conductor, key:any){
        this.reset();
        this.origen = origin;
        this.conductorKey = key;
        this.conductor = conductor;

        //obtener asignados
        this.obtenerVehiculosDisponibles();
    }

    obtenerVehiculosNoAsignados(vehiculosDisponibles:Vehiculo[], vehiculosAsignados: Vehiculo[]){
        const asignados = vehiculosAsignados.map(x => x.vehiculoId);

        const res = vehiculosDisponibles.filter(x => asignados.indexOf(x.vehiculoId) == -1);
        return res;
    }

    obtenerVehiculosDisponibles(){
        
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

        this.utils.showSpinner();
        this.rx.vehiculosGet(cuentaId, 0).then((result:any) => {
            this.utils.hideSpinner();
            this.vehiculosDisponibles = result.data.vehiculos;
            
            this.cantidadVehiculosDisponiblesParaAsignar = this.obtenerVehiculosNoAsignados(this.vehiculosDisponibles, this.conductor.vehiculosAsignados).length;
        });
    }

    mostrarAsignacionVehiculos(){
        let model: IEmmitModel = {
            tipo: 'VehiculosAsignacion',
            action: 'OPEN',
            data: {
                origin: Paginas.ModalVehiculosAsignados,
                conductor: this.conductor,
                vehiculosDisponibles: this.vehiculosDisponibles,
                vehiculosAsignados: this.conductor.vehiculosAsignados
            }
        }
        this.emiter.emmitChange(model);
    }

    asignarVehiculos(){

        if(this.origen == Paginas.ModalConductorDetalle){
            //asignar vehiculos
            const cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
            const vehiculosAsignados = this.conductor.vehiculosAsignados.map(x => x.vehiculoId);
            

            this.utils.showSpinner();
            this.rx.vehiculosAsignar(cuentaId, this.conductor.usuarioId, vehiculosAsignados).then(result => {
                this.utils.hideSpinner();

                if(this.vehiculosDesasignados.length > 0){
                    console.log(this.vehiculosDesasignados);
                    this.rx. vehiculosDesasignar(cuentaId, this.conductor.usuarioId, this.vehiculosDesasignados).then(result => {
                        
                        this.retornarVehiculosAlOrigen();
                        this.closeModal();

                    }, this.errorCallback);
                }
                else{
                    this.retornarVehiculosAlOrigen();
                    this.closeModal();
                }

                
            }, this.errorCallback);
        }
        else{
            this.retornarVehiculosAlOrigen();
            this.closeModal();
        }
        
    }

    retornarVehiculosAlOrigen(){
        //devuelve los vehiculos asignados al origen para que guarde los cambios (invitaciones)
        this.emiter.emitCallback({
            from: this.CURRENT_PAGE,
            action: this.origen,
            data: {
                conductorKey: this.conductorKey,
                vehiculosAsignados: this.conductor.vehiculosAsignados
            }
        });
    }

    closeModal(){

        //si origen es conductores cerrar ambos sino solo este
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarVehiculosAsignados']
            }
        }

        if(this.origen == Paginas.ModalConductorDetalle)
            config.data.sidebars.push('sideBarConductores')

        this.emiter.emmitChange(config);
    }

    backToMain(){

        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarVehiculosAsignados'],
            }
        }

        this.emiter.emmitChange(config);
    }

    private emitterHandler = (data:IEmmitCallbackModel) => {
        if(data.from == Paginas.ModalVehiculosAsignacion){
            data.data.forEach((element:Vehiculo) => {
                //si no lo tiene lo agrega
                let index = this.utils.arrayIndexOf(this.conductor.vehiculosAsignados, 'vehiculoId', element.vehiculoId);
                if(index == -1){
                    this.conductor.vehiculosAsignados.push(element);

                    //si se agrega uno se quita de los desasignados si es que existe
                    let idx = this.vehiculosDesasignados.indexOf(element.vehiculoId);
                    if(idx > -1)
                        this.vehiculosDesasignados.splice(idx, 1);

                }
            });
            this.haveChanges = true;
        }
    }

    private reset(){
        this.conductor = new Conductor();
        this.vehiculosDisponibles = [];
        this.haveChanges = false;
        this.cantidadVehiculosDisponiblesParaAsignar = 0;
        this.vehiculosDesasignados = []; 
    }
    
    ngOnDestroy() {
        if(this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }
}
