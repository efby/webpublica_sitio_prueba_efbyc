import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../../pageBase';

@Component({
  selector: 'app-envia-invitacion',
  templateUrl: './envia-invitacion.component.html',
  styleUrls: ['./envia-invitacion.component.scss']
})
export class EnviaInvitacionComponent extends PageBase implements OnInit {
  @Input() id: string;
  @Input() current: any

  email: any
  constructor(private rx: RxService,
    public modalSidebar: SidebarService,
    public variablesApi: VariablesGenerales,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router) {
    super(ref, utils, router);
  }

  ngOnInit() {
    console.log(this.current)
  }

  closeThis() {
    this.modalSidebar.close(this.id);
  }
  EnviarInvitacion() {
    /**
     * 
     * current = {
    nombre: null,
    apellido: null,
    telefono: null,
    rolId: null,
    enviarSMS:true,
    enviarMail:false
  }
     */

    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
      "rolId": this.current.rolId,
      "invitacionTelefonoInvitadoId": "569" + this.current.telefono,
      "invitacionNombre": this.current.nombre,
      "invitacionApellido": this.current.apellido,
    }

    if (this.email != null && this.email != "") {
      body["invitacionMail"] = this.email;
    }

    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kcuentaCrearInvitacionUsuarioAdmin).then(result => {

      setTimeout(() => {
        let msg_2 = {
          title: 'Invitación enviada',
          messages: [result.data.message],
        }
        this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {
          this.modalSidebar.closeAll(true)
        })
      }, 400);

      this.utils.hideSpinner("Interno");
    }, this.errorCallback)
  }
  changeSMS() {

  }
  changeMAIL() {

    this.current.enviarMail = !this.current.enviarMail;
    if (!this.current.enviarMail) {
      this.email = null
    }
  }



}
