import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';
import { DatosAdministradorComponentextends } from './datos-administrador/datos-administrador.component';

@Component({
  selector: 'app-crear-invitacion',
  templateUrl: './crear-invitacion.component.html',
  styleUrls: ['./crear-invitacion.component.scss']
})
export class CrearInvitacionComponent extends PageBase implements OnInit {
  @Input() id: string;
  rolId: number
  roles = [];

  constructor(private rx: RxService,
    public modalSidebar: SidebarService,
    public variablesApi: VariablesGenerales,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router) {
    super(ref, utils, router);
  }

  ngOnInit() {

    this.roles = this.variablesApi.home.maestros.roles.filter(a => { return a.rolId != 4 && a.rolId != 1 })
    this.rolId = null
  }

  closeThis() {
    this.modalSidebar.close(this.id);
  }

  avanzar() {
    var modal = this.modalSidebar.open(DatosAdministradorComponentextends, { miBackdrop: false })
    modal.refModal.componentInstance.rolId = this.rolId;

  }

}
