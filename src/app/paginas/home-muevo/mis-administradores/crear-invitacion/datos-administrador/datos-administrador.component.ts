import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RevisaEnviaRetiroComponent } from '../../../abonar/revisa-envia-retiro/revisa-envia-retiro.component';
import { PageBase } from '../../../pageBase';
import { EnviaInvitacionComponent } from '../envia-invitacion/envia-invitacion.component';

@Component({
  selector: 'app-datos-administrador',
  templateUrl: './datos-administrador.component.html',
  styleUrls: ['./datos-administrador.component.scss']
})
export class DatosAdministradorComponentextends extends PageBase implements OnInit {
  @Input() id: string;
  @Input() rolId: number
  current = {
    nombre: null,
    apellido: null,
    telefono: null,
    rolId: null,
    enviarSMS:true,
    enviarMail:false
  }


  constructor(private rx: RxService,
    public modalSidebar: SidebarService,
    public variablesApi: VariablesGenerales,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router) {
    super(ref, utils, router);
  }

  ngOnInit() {
    this.current.rolId = this.rolId
  }

  closeThis() {
    this.modalSidebar.close(this.id);
  }
  avanzar() {

    var modal = this.modalSidebar.open(EnviaInvitacionComponent, { miBackdrop: false })
    modal.refModal.componentInstance.current = this.current;
  }

  validaMaxInputText(event) {
    this.utils.validaMaxInputText(event)
  }
  onlyNumber(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = this.utils.formatNumbers(item.value);
  }
}
