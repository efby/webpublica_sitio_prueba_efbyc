import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';

@Component({
  selector: 'app-detalle-administrador',
  templateUrl: './detalle-administrador.component.html',
  styleUrls: ['./detalle-administrador.component.scss']
})
export class DetalleAdministradorComponent extends PageBase implements OnInit {
  @Input() id: string;
  @Input() usuarioObtenerId: any;
  @Input() cuentaId: any;
  estadoTouch: boolean
  @Input() detalleUsuario: any;



  roles = [];


  constructor(private rx: RxService,

    public modalSidebar: SidebarService,
    public variablesApi: VariablesGenerales,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router) {
    super(ref, utils, router);
  }

  ngOnInit() {
    this.detalleUsuario.estado = this.detalleUsuario.usuarioCuentaEstadoId == 1
    if (this.detalleUsuario.rolId == 1) {
      this.roles = this.variablesApi.home.maestros.roles.filter(a => { return a.rolId != 4 })
    } else {
      this.roles = this.variablesApi.home.maestros.roles.filter(a => { return a.rolId != 4 && a.rolId != 1 })
    }

  }

  obtenerDetalleAdministrador() {

  }
  bloquearEditar() {
    return this.detalleUsuario.usuarioId == this.variablesApi.home.usuarioLogIn.usuarioId || this.detalleUsuario.rolId == 1 || this.variablesApi.home.rolPrivilegiosFront.administradores.verEditar != 2
  }

  closeThis() {
    this.modalSidebar.close(this.id);
  }

  guardarUSuario() {
    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
      "usuarioId": this.detalleUsuario.usuarioId,
      "rolId": this.detalleUsuario.rolId,
    };

    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kcuentaModificarUsuarioCuentaRol).then(result => {
      this.utils.hideSpinner("Interno");
      this.closeThis()
      /* setTimeout(() => {
        let msg_2 = {
          title: 'Correcto',
          messages: [result.data.message],
        }
        this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {
          this.closeThis()
        })
      }, 400); */
    }, this.errorCallback)

  }

  desactivarUsuarioAdmin() {
    this.estadoTouch = true;
    this.detalleUsuario.estado = !this.detalleUsuario.estado;
    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
      "usuarioId": this.detalleUsuario.usuarioId,
      "usuarioCuentaEstadoId": this.detalleUsuario.estado ? 1 : 0
    };
    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kcuentaModificarUsuarioCuentaRol).then(result => {
      this.utils.hideSpinner("Interno");
    }, this.errorCallback)
  }

  eliminarUsuario() {
    let msg = {
      title: 'Eliminar usuario',
      messages: ["¿Estás seguro que deseas eliminar a " +
        this.detalleUsuario.usuarioNombre + ' ' + this.detalleUsuario.usuarioApellido +
        " de tu cuenta?"],
      okText: "Eliminar"
    }
    this.utils.openModal(Paginas.ModalAceptarCancelarComponent, msg
    ).then(result => {
      switch (result) {
        case OpcionesModal.ACEPTAR:
          var body = {
            "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
            "usuarioId": this.detalleUsuario.usuarioId,
            "usuarioCuentaEstadoId": 2
          };
          this.utils.showSpinner("Interno");
          this.rx.llamarPOSTHome(body, this.constantes.kcuentaModificarUsuarioCuentaRol).then(result => {
            this.utils.hideSpinner("Interno");
            this.closeThis()
           /*  setTimeout(() => {
              let msg_2 = {
                title: 'Usuario Eliminado',
                messages: [result.data.message],
              }
              this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {
                this.closeThis()
              })
            }, 400); */
          }, this.errorCallback)
          break;
        case OpcionesModal.CANCELAR:
          console.log("NOK")
          // this.closeThis()
          break;
      }
    });
  }
}
