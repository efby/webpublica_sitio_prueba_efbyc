import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';

@Component({
  selector: 'app-invitaciones-administrador',
  templateUrl: './invitaciones-administrador.component.html',
  styleUrls: ['./invitaciones-administrador.component.scss']
})
export class InvitacionesAdministradorComponent extends PageBase implements OnInit {
  @Input() id: string;
  @Input() posicionFinal: number
  @Input() conductoresInvitados = []
  @Input() cantidadRegistrosPorConsulta: number

  invitaciones = []

  constructor(private rx: RxService,
    public modalSidebar: SidebarService,
    public variablesApi: VariablesGenerales,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router) {
    super(ref, utils, router);

  }

  ngOnInit() {
    this.invitaciones = this.conductoresInvitados.filter(a => { return a.rolId != 4 })
  }

  closeThis() {
    this.modalSidebar.close(this.id);
  }

  reenviarInvitacion(invitacion) {
    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
      "rolId": invitacion.rolId,
      "invitacionTelefonoInvitadoId": invitacion.invitacionTelefonoInvitadoId,
      "invitacionNombre": invitacion.invitacionDatos.invitacionNombre.trim(),
    };
    if (invitacion.invitacionDatos.invitacionApellido != null &&
      invitacion.invitacionDatos.invitacionApellido != "") {
      body["invitacionApellido"] = invitacion.invitacionDatos.invitacionApellido.trim();
    }
    if (invitacion.invitacionMail != null) {
      body["invitacionMail"] = invitacion.invitacionMail;
    }

    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kcuentaCrearInvitacionUsuarioAdmin).then(result => {

      setTimeout(() => {
        let msg_2 = {
          title: 'Invitación',
          messages: [result.data.message],
        }
        this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {
          this.closeThis()
        })
      }, 400);

      this.utils.hideSpinner("Interno");
    }, this.errorCallback)
  }

  eliminar(invitacion) {

    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
      "invitacionId": invitacion.invitacionId
    };

    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kCancelarInvitacionEndPoint).then(result => {

      /* setTimeout(() => {
        let msg_2 = {
          title: 'Invitación',
          messages: [result.data.message],
        }
        this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {
          this.closeThis()
        })
      }, 400); */
      this.utils.hideSpinner("Interno");
      this.closeThis()
      
    }, this.errorCallback)
  }



  next() {

    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId
    }

    if (this.posicionFinal != null) {
      body["posicionInicial"] = this.posicionFinal;
    }

    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kCuentaConsultarnotificacionesEnviadas).then(result => {

      this.invitaciones = this.invitaciones.concat(result.data.data.conductoresInvitados)

      this.utils.hideSpinner("Interno");
    }, this.errorCallback)

  }

}
