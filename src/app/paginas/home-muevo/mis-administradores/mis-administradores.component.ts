import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { IPagination, TableDataCsv } from 'src/app/globales/business-objects';
import { Constantes } from 'src/app/globales/constantes';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Conductor } from 'src/app/models/conductores';
import { SidebarService } from 'src/app/services/sidebar-services';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { ExportDataService } from 'src/app/servicios/export-data.service';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../pageBase';
import { CrearInvitacionComponent } from './crear-invitacion/crear-invitacion.component';
import { DetalleAdministradorComponent } from './detalle-administrador/detalle-administrador.component';
import { InvitacionesAdministradorComponent } from './invitaciones-administrador/invitaciones-administrador.component';
import * as moment from "moment";
import "moment/min/locales";

@Component({
  selector: 'app-mis-administradores',
  templateUrl: './mis-administradores.component.html',
  styleUrls: ['./mis-administradores.component.scss']
})
export class MisAdministradoresComponent extends PageBase implements OnInit {

  navegacion = [
    { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Mis Administradores", class: "active", url: "" }
  ];
  private administradores: any[] = [];
  pagination: IPagination;


  posicionFinal = 0;
  tieneInvitaciones = 0;


  displayedColumns = ['rut', 'nombreCompleto', 'telefonoId', 'email', 'rolNombre', 'actions']
  dataSource: MatTableDataSource<Conductor> = new MatTableDataSource<Conductor>();
  selection = new SelectionModel<Conductor>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public variablesApi: VariablesGenerales,
    public modalSidebar: SidebarService,
    private rx: RxService,
    private constantes: Constantes,
    public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router, private emiter: BreadCrumbsService,) {

    super(ref, utils, router);
    this.emiter.emitChange({ migajas: this.navegacion, muestraBreadcrumbs: true });
    this.pagination = {
      currentPage: 0,
      pages: 0,
      totalRows: 0,
      positionFinal: 0,
      pagesSize: 0,
      rows: 0
    }
  }

  ngOnInit() {
    this.obtenerAdministradores();
  }

  obtenerAdministradores(posicionInicial?) {
    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId
    }
    if (posicionInicial != null) {
      body["posicionInicial"] = posicionInicial;
    }
    this._isLoading = true;
    this.rx.llamarPOSTHome(body, this.constantes.CONSULTAR_USUARIO_ADMIN).then((result: any) => {
      console.log(result.data);
      this.posicionFinal = result.data.data.posicionFinal;
      this.tieneInvitaciones = result.data.data.tieneInvitaciones;
      this.administradores = this.administradores.concat(result.data.data.administradores);
      this.administradores.forEach(element => {
        element.email = element.usuarioEmail;
        element.estado = element.usuarioCuentaEstadoId === 1;
        element.nombre = element.usuarioNombre;
        element.nombreCompleto = element.usuarioNombre + " " + element.usuarioApellido;
        element.rolId = element.rolId;
        element.rut = element.usuarioRut;
        element.telefonoId = element.usuarioTelefonoId;
        element.usuarioId = element.usuarioId;
        element.apellido = element.usuarioApellido;
        element.rolNombre = this.obtenerValor(element.rolId, 'rolId', this.variablesApi.home.maestros.roles, 'rolNombre')
      })

      this.administradores.sort((a, b) => a.apellido.localeCompare(b.apellido));
      this.pagination.positionFinal = result.data.data.posicionFinal
      this.pagination.totalRows = this.administradores.length
      this.pagination.pagesSize = result.data.data.cantidadRegistrosPorConsulta
      this.pagination.rows = result.data.data.administradores.length
      this.dataSource = new MatTableDataSource(this.administradores);
      this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);
      this._isLoading = false;

      this.utils.hideSpinner("Interno");

    }, this.errorCallback);
  }

  exportCsv() {
    let data: TableDataCsv = {
      //'rut', 'nombreCompleto', 'telefonoId', 'email', 'rolNombre'
      header: ["Rut", "Nombre Completo", "Teléfono", "Email", "Rol"],
      rows: this.dataSource.data.map((x: any) => {
        return [x.rut, x.nombreCompleto, x.telefonoId, x.email, x.rolNombre,];
      }),
    };

    ExportDataService.ExportCsv(
      'Administradores_' + `${this.variablesApi.home.cuentaSeleccionada.cuentaId.toString()}_${moment().format("YYYYMMDD_HHmm")}`,
      data.rows,
      data.header,
      ";",
      ",",
      "Administradores MUEVO EMPRESAS"
    );
  }
  toogleEnable() { }
  mostrarConductor(row, event) {

    var body = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
      usuarioObtenerId: row.usuarioId //usuarioObtenerId
    }


    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.DETALLE_USUARIO_ADMIN).then(result => {

      var modal = this.modalSidebar.open(DetalleAdministradorComponent, /* { miBackdrop: false } */)
      modal.refModal.componentInstance.usuarioObtenerId = row.usuarioId;
      modal.refModal.componentInstance.cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
      modal.refModal.componentInstance.detalleUsuario = result.data.data.usuarioCuenta
      if (row.rolId != 1 && this.variablesApi.home.rolPrivilegiosFront.administradores.verEditar == 2) {
        modal.refModal.result.then(() => {
          this.administradores = [];
          this.utils.showSpinner("Interno");
          this.obtenerAdministradores();
        })
      }


      this.utils.hideSpinner("Interno");
    }, this.errorCallback)

  }



  next() {
    this.obtenerAdministradores(this.posicionFinal);
  }

  abrirInvitaciones(posicionInicial) {

    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId
    }

    if (posicionInicial != null) {
      body["posicionInicial"] = posicionInicial;
    }

    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kCuentaConsultarnotificacionesEnviadas).then(result => {

      var modal = this.modalSidebar.open(InvitacionesAdministradorComponent, /* { miBackdrop: false } */)


      modal.refModal.componentInstance.posicionFinal = result.data.data.posicionFinal;
      modal.refModal.componentInstance.conductoresInvitados = result.data.data.conductoresInvitados
      modal.refModal.componentInstance.cantidadRegistrosPorConsulta = result.data.data.cantidadRegistrosPorConsulta
      /*  
       modal.refModal.componentInstance.cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
       modal.refModal.componentInstance.detalleUsuario = result.data.data.usuarioCuenta
       if (row.rolId != 1) {
         modal.refModal.result.then(() => {
           this.administradores = [];
           this.utils.showSpinner("Interno");
           this.obtenerAdministradores();
         })
       } */
      modal.refModal.result.then(() => {
        this.administradores = [];
        this.utils.showSpinner("Interno");
        this.obtenerAdministradores();
      }, (reason) => {
        if (reason) {
          this.administradores = [];
          this.utils.showSpinner("Interno");
          this.obtenerAdministradores();
        }
      })
      this.utils.hideSpinner("Interno");
    }, this.errorCallback)

  }
  invitar() {
    var modal = this.modalSidebar.open(CrearInvitacionComponent, /* { miBackdrop: false } */)
    modal.refModal.result.then((result) => {
      if (result) {
        this.administradores = [];
        this.utils.showSpinner("Interno");
        this.obtenerAdministradores();
      }
    }, (reason) => {
      if (reason) {
        this.administradores = [];
        this.utils.showSpinner("Interno");
        this.obtenerAdministradores();
      }
    })


  }
}
