import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { RxService } from 'src/app/servicios/rx.service';
import { PageBase } from '../../pageBase';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DatosFacturacion } from 'src/app/globales/business-objects';

@Component({
    selector: 'app-muevo-mi-facturacion',
    templateUrl: './muevo-mi-facturacion.component.html',
    styleUrls: ['./muevo-mi-facturacion.component.css']
})
export class MuevoMiFacturacionComponent extends PageBase implements OnInit {
    current: DatosFacturacion;
    haveFacturation: boolean;

    private unsubscribe = new Subject();
    callbackSubscripcion:any;

    migajas = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Mi Facturacion", class: "active", url: "" }
    ];
    _estadoFacturacion: boolean = false;
    actividadesEconomicas: any[] = [];
    periodosFacturacion: any[] = [];
    distritos: any[] = [];
    
    btnGuardar: boolean = true;
    
    NombreUsuario: string;
    
    nvaDatosFacturacionRut: string = "";
    nvaDatosFacturacionEmail: string = "";
    nvaDatosFacturacionGiro: string = "";
    nvaDatosFacturacionDistrito: string = "";
    nvaDatosFacturacionDireccion: string = "";
    nvaDatosFacturacionRazonSocial: string = "";
    nvaDatosFacturacionCodigoPostal: string = "";
    nvaDatosFacturacionNombreFantasia: string = "";
    nvaDatosFacturacionActividadEconomica: string = "";
    nvaDatosFacturacionPeriodoFacturacion: string = "";
    nvaOpcionesEmision: string = "0";
    
    constructor(private rx: RxService, public router: Router, public ref:ChangeDetectorRef, public utils: UtilitariosService, public variablesApi: VariablesGenerales, private maquinaHome: MaquinaHome, private emiter: BreadCrumbsService) {
        super(ref, utils, router);

        this.current = new DatosFacturacion();

        // {
        //     rutEmpresa: '',
        //     alias: '',
        //     email: '',
        //     periodoFacturacion: 0,
        //     distrito: '0',
        //     emitir: -1,
        //     actividades: '0'
        // }
        this.haveFacturation = false;
    }
    
    ngOnInit() {
        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);

        this.cargaDatosFacturacion();
        
        setTimeout(() => this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true }));

        console.log(this.current)
        //this.setDatosCuenta(this.variablesApi.home.cuentaDatosDte);
    }

    openDetails(){
        let copy = this.utils.clone(this.current)
        copy.dteRestringeEleccion = copy.dteRestringeEleccion == 1 ? 'si' : 'no';

        this.emiter.emitChange({
            tipo: 'ConfiguracionFacturacion',
            item: copy,
            periodos: this.variablesApi.home.maestros.periodosFacturacion,
            actividades: this.actividadesEconomicas,
            distritos: this.distritos
        });
    }
    
    cargaDatosFacturacion() {
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        
        this.utils.showSpinner("Interno");
        this.rx.facturacionGet(cuentaId).then((result:any) => {
            this._pageReady = true;
            this.actividadesEconomicas = result.data.actividadesEconomicas;
            this.periodosFacturacion = result.data.periodosFacturacion;
            this.distritos = result.data.distritos;

            
            this.setDatosCuenta(result.data.datosCuenta);
            this.utils.hideSpinner("Interno");
        }, this.errorCallback);
    }
    
    guardar() {

        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        if (this.nvaDatosFacturacionRut != null) {
            this.utils.showSpinner("Interno");
            let action: RequestTipo = {
                accion: AccionTipo.ACCION_CLICK,
                datos: {
                    cuentaId: cuentaId,
                    datosFacturacionRut: this.current.dteRut,
                    datosFacturacionEmail: this.current.dteEmail,
                    datosFacturacionGiro: this.current.dteGiro,
                    datosFacturacionDistrito: this.current.dteDistrito,
                    datosFacturacionDireccion: this.current.dteDistrito,
                    datosFacturacionRazonSocial: this.current.dteRazonSocial,
                    datosFacturacionNombreFantasia: this.current.dteNombreFantasia,
                    datosFacturacionActividadEconomica: this.current.dteActividadEconomica,
                    datosFacturacionPeriodoFacturacion: this.current.dtePeriodoFacturacion,
                    restringeEleccionDte: this.current.dteRestringeEleccion,
                    dtePuedeFacturar: !this._estadoFacturacion
                }
            };
            
            this.rx.facturacionCrearDatos(action).then(result => {
                this.utils.hideSpinner("Interno");
            }, this.errorCallback);
        
        }
    }
    
    validaGuardar(): boolean {
        let retorno: boolean = false;
        if (this.nvaDatosFacturacionRut !== "" && this.nvaDatosFacturacionGiro !== "" && this.nvaDatosFacturacionDistrito !== "" && this.nvaDatosFacturacionDireccion !== "" && this.nvaDatosFacturacionRazonSocial !== "" && this.nvaDatosFacturacionNombreFantasia !== "" && this.nvaDatosFacturacionActividadEconomica !== "" && this.nvaDatosFacturacionPeriodoFacturacion !== "") {
            retorno = true;
        }
        
        if (document.getElementById("dvDatosFacturacionRut") != null) {
            this.utils.pintaValidacion(document.getElementById("dvDatosFacturacionRut"), this.nvaDatosFacturacionRut, 'facturacion');
            this.utils.pintaValidacion(document.getElementById("dvDatosFacturacionGiro"), this.nvaDatosFacturacionGiro, 'facturacion');
            this.utils.pintaValidacion(document.getElementById("dvDatosFacturacionDistrito"), this.nvaDatosFacturacionDistrito, 'facturacion');
            this.utils.pintaValidacion(document.getElementById("dvDatosFacturacionDireccion"), this.nvaDatosFacturacionDireccion, 'facturacion');
            this.utils.pintaValidacion(document.getElementById("dvDatosFacturacionRazonSocial"), this.nvaDatosFacturacionRazonSocial, 'facturacion');
            this.utils.pintaValidacion(document.getElementById("dvDatosFacturacionNombreFantasia"), this.nvaDatosFacturacionNombreFantasia, 'facturacion');
            this.utils.pintaValidacion(document.getElementById("dvDatosFacturacionActividadEconomica"), this.nvaDatosFacturacionActividadEconomica, 'facturacion');
            this.utils.pintaValidacion(document.getElementById("dvDatosFacturacionPeriodoFacturacion"), this.nvaDatosFacturacionPeriodoFacturacion, 'facturacion');
        }
        return retorno;
    }
    
    setDatosCuenta(dte: any) {
        console.log(dte);

        if (typeof dte.dteActividadEconomica !== 'undefined') {
            this.haveFacturation = true;

            this.current.dteActividadEconomica = dte.dteActividadEconomica;
            this.current.dteDireccion = dte.dteDireccion;
            this.current.dteDistrito = dte.dteDistrito == null ? "" : dte.dteDistrito;
            this.current.dteEmail = dte.dteEmail;
            this.current.dteFechaUpdate = dte.dteFechaUpdate;
            this.current.dteGiro = dte.dteGiro == null ? "" : dte.dteGiro;
            this.current.dteNombreFantasia = dte.dteNombreFantasia;
            this.current.dtePeriodoFacturacion = dte.dtePeriodoFacturacion == null ? "" : dte.dtePeriodoFacturacion;
            this.current.dtePuedeFacturar = dte.dtePuedeFacturar === 1;
            this.current.dteRazonSocial = dte.dteRazonSocial;
            this.current.dteRestringeEleccion = (dte.dteRestringeEleccion == null) ? "0" : (dte.dteRestringeEleccion == "") ? "0" : dte.dteRestringeEleccion;
            this.current.dteRut = dte.dteRut;
            this._estadoFacturacion = dte.dtePuedeFacturar === 1;
            this.current.dteNumeroSerie = dte.dteNumeroSerie;
            this.current.dteDeclaracion = dte.dteDeclaracion;

            this.current.comuna=this.distritos.filter(item=>{return item.codigo==this.current.dteDistrito})[0].descripcion
        } else {
            this._estadoFacturacion = false;
        }
        
    }
    
    
    desactivaFacturacion() {
        this.guardar();
    }
    

    private emitterHandler = (data:IEmmitCallbackModel) => {
        if(data.from == Paginas.HomeMuevoMiFacturacion)
            this.cargaDatosFacturacion();
    }

    ngOnDestroy() {
        if(this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }
    
}
