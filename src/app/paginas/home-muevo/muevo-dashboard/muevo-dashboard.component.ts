import { Component, OnInit, Input, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Cuentas, VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { UsuarioLogin, Cuenta, Saldo } from 'src/app/globales/business-objects';
import { RxService } from 'src/app/servicios/rx.service';
import { PageBase } from '../pageBase';
import { MuevoFormasdePagoComponent } from './muevo-formasde-pago/muevo-formasde-pago.component';
import { MuevoFormaPagoTodosComponent } from './muevo-formasde-pago/muevo-forma-pago-todos/muevo-forma-pago-todos.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-muevo-dashboard',
    templateUrl: './muevo-dashboard.component.html',
    styleUrls: ['./muevo-dashboard.component.css']
})
export class MuevoDashboardComponent extends PageBase implements OnInit {
    
    migajas = [
        { title: "Inicio", class: "active", url: "" }
    ];
    
    nombre: UsuarioLogin = this.variablesApi.home.usuarioLogIn;
    empresas: Cuenta[] = this.variablesApi.home.cuentas;
    empresaActual: string;
    ConductoresVehiculo: number = 1;
    _receive: number = 0;
    _false: boolean = false;
    _true: boolean = true;
    isMobile: boolean = false;
    
    searchInComponent: string = "";
    
    activateFormaPago: boolean = false;
    interval: any;

    // private unsubscribe = new Subject();
    // callbackSubscripcion:any;

    @ViewChild(MuevoFormasdePagoComponent)
    muevoFormasdePagoComponent: MuevoFormasdePagoComponent

    @ViewChild(MuevoFormaPagoTodosComponent)
    muevoFormaPagoTodosComponent: MuevoFormaPagoTodosComponent;

    constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private maquinaHome: MaquinaHome, public router: Router, public utils: UtilitariosService, private route: ActivatedRoute, public ref: ChangeDetectorRef, private rx:RxService) {
        super(ref, utils, router);
        
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this._receive = 0
        this.variablesApi.ultimaPagina = Paginas.HomeMuevo;
        //ref.detach();
        // this.interval = setInterval(() => {
        //     this.ref.detectChanges();
        //     this.isMobile = this.variablesApi.esMobile;
        // }, 100);
    }
    ngOnDestroy() {
        clearInterval(this.interval);

        // if(this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
        //     this.callbackSubscripcion.unsubscribe();
    }
    
    ngOnInit() {
        console.log('dashboard init..')

        //this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);
        this.obtenerDatosWeb();
        
        
        // this._receive = parseInt(this.route.snapshot.paramMap.get('ConductoresVehiculo'));
        // if (isNaN(this._receive)) {
        //   this._receive = 1;
        //   document.getElementById("selConductores").focus();
        // }
        // this.ConductoresVehiculo = this._receive;
        // if (this._receive == 1) {
        //   document.getElementById("selConductores").focus();
        // } else if (this._receive == 2) {
        //   document.getElementById("selVehiculos").focus();
        // }
        
        setTimeout(() => {
            this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
        });
        
        // this.cargaDatos();
    }
    
    obtenerDatosWeb(){
        const request:RequestTipo = {
            accion: AccionTipo.ACCION_INICIO
        };
        
        this.utils.showSpinner();
        this.maquinaHome.homeInicio(request).then(result => {
            this.activateFormaPago = this.variablesApi.home.menu.formasDePagoEstado;
            //this.saldo = this.variablesApi.home.saldo;

            this.muevoFormaPagoTodosComponent.main();

            
            //this.homeCuentaConsultarMedioPago();
            this.nombre = this.variablesApi.home.usuarioLogIn;
            this.empresas = this.variablesApi.home.cuentas;
            this.setSelectedEmpresa();

            this.ConductoresVehiculo = this._receive;
            if (this._receive == 1) 
                document.getElementById("selConductores").focus();
            else if (this._receive == 2)
                document.getElementById("selVehiculos").focus();
            
                this.utils.hideSpinner();
            
        }, this.errorCallback)
    }
    
    
    private setSelectedEmpresa() {
        if (this.empresas.length > 1) {
            // this.empresas.forEach(element => {
            //   if (element.cuentaDefault) {
            //     this.empresaActual = element.cuentaNombre;
            //   }
            // });
        } else if (this.empresas.length > 0) {
            this.empresaActual = this.empresas[0].cuentaNombre;
        } else {
            this.empresaActual = "Sin Cuenta";
        }
    }
    
    cambiaConductorVehiculo(valor: number) {
        this.ConductoresVehiculo = valor;
        this.searchInComponent = "";
    }

    // private emitterHandler = (data:IEmmitCallbackModel) => {
    //    if(data.from == Paginas.ModalTarjetaAbonoAbonar){
    //         //si llega aqui es por que relizo un abono y requiere cargar los datos
    //         this.obtenerDatosWeb();
    //     }
    // }
}
