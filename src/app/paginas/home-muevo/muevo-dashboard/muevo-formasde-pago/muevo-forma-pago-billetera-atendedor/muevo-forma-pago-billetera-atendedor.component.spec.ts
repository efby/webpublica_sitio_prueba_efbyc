import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoFormaPagoBilleteraAtendedorComponent } from './muevo-forma-pago-billetera-atendedor.component';

describe('MuevoFormaPagoBilleteraAtendedorComponent', () => {
  let component: MuevoFormaPagoBilleteraAtendedorComponent;
  let fixture: ComponentFixture<MuevoFormaPagoBilleteraAtendedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoFormaPagoBilleteraAtendedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoFormaPagoBilleteraAtendedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
