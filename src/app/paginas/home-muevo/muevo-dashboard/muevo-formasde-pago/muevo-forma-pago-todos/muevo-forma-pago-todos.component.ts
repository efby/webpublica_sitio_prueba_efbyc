import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { TipoSaldo, VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, TipoMedioPago } from 'src/app/globales/enumeradores';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { TipoFormaPago, Saldo, CuentaCorriente, MedioPago, Tarjeta, Indicadores } from 'src/app/globales/business-objects';
import { HomeService } from 'src/app/servicios/home/home.service';
import { RxService } from 'src/app/servicios/rx.service';
import { Respuestas } from 'src/app/globales/respuestas';
import { Paginas } from 'src/app/globales/paginas';
import { Router } from '@angular/router';
import { PageBase } from '../../../pageBase';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Constantes } from 'src/app/globales/constantes';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RetirarAbonoComponent } from '../../../abonar/retirar-abono/retirar-abono.component';

@Component({
    selector: 'app-muevo-forma-pago-todos',
    templateUrl: './muevo-forma-pago-todos.component.html',
    styleUrls: ['./muevo-forma-pago-todos.component.scss']
})
export class MuevoFormaPagoTodosComponent extends PageBase implements OnInit {
    // @Input()
    // saldo:Saldo;

    medios: TipoMedioPago[] = [];

    current: {
        tarjetaCompartidaId: string
        cuentasCorrientes: CuentaCorriente[],
        mediosPago: MedioPago[],
        tarjetas: Tarjeta[],
        tarjeta?: Tarjeta,
        saldo: Saldo,
        tipoTarjetaCompartida: MedioPago
    }

    indicadores: Indicadores;
    
    montoTotalMes: string;
    tipoTarjetaCompartida: TipoFormaPago = new TipoFormaPago();
   
    config: {
        _billetera: boolean;
        _tarjeta: boolean;
        _atendedor: boolean;
        _activateFormaPago: boolean;
    }
    
    isMobile: boolean = false;
    
    
    
    constructor(
        private constantes:Constantes,
        private modalSidebar:SidebarService,
        private emiter: BreadCrumbsService, public router: Router, private rx: RxService, private homeService: HomeService, public variablesApi: VariablesGenerales, public utils: UtilitariosService, private ref: ChangeDetectorRef, private maquinaHome: MaquinaHome) {
        super(ref, utils, router)

        this.current = {
            tarjetaCompartidaId: '',
            cuentasCorrientes: [],
            mediosPago: [],
            tarjetas: [],
            saldo: null,
            tarjeta: null,
            tipoTarjetaCompartida: null
        }

        this.config = {
            _billetera: false,
            _atendedor: false,
            _tarjeta: false,
            _activateFormaPago: this.variablesApi.home.menu.formasDePagoEstado
        }

        this.indicadores = new Indicadores();

        //ref.detach();
        //this.interval = setInterval(() => {
            //this.ref.detectChanges();
            //this.saldo = this.variablesApi.home.saldo;
            // this.saldo.lineaCreditoString = this.currency(this.saldo.lineaCredito);
            // this.saldo.saldoString = this.currency(this.saldo.saldo);
            // this.saldo.saldoFinalString = this.currency(this.saldo.saldoFinal);
            //this.montoTotalMes = this.currency(this.variablesApi.home.montoTotalMes);
            //this.current.tarjetaCompartidaId = this.variablesApi.home.tarjetaCompartidaId;
            //this.tarjeta = this.variablesApi.home.tarjetas.filter(r => r.idInscripcion == this.tarjetaCompartida)[0];
            // if (this.medios.length !== this.variablesApi.home.tipoFormaPago.length) {
            //     this.cargaDataInicial(this.variablesApi.home.tipoFormaPago);
            // }
            
            //this.isMobile = this.variablesApi.esMobile;
        //}, 10);
    }
    
    ngOnDestroy() {
        //clearInterval(this.interval);
    }
    
    ngOnInit() {
        
    }

    main(){
        console.log('formas de pago todos main..')
        this.indicadores = this.variablesApi.home.cuentaSeleccionada.indicadores;
        this.current.saldo = this.variablesApi.home.saldo;  
        this.current.mediosPago = this.variablesApi.home.mediosPago;
      
        if (this.medios.length !== this.variablesApi.home.tipoFormaPago.length)
            this.cargaDataInicial(this.variablesApi.home.tipoFormaPago);
    }
    
    obtenerInformacion() {
        // this._isLoading = true;
        
        // this.rx.mediosPagoGet(this.variablesApi.home.cuentaSeleccionada.cuentaId).then((result:any) => {

        //     this.current.tarjetaCompartidaId = result.data.tarjetaCompartidaId;
        //     this.current.cuentasCorrientes = result.data.cuentasCorrientes;
        //     this.current.mediosPago = result.data.mediosPago;
        //     this.current.tarjetas = result.data.tarjetas;
        //     this.current.saldo = result.data.saldo;

        //     this.current.tarjeta = this.current.tarjetas.filter(r => r.idInscripcion == this.current.tarjetaCompartidaId)[0];
        //     let index = this.variablesApi.home.tipoFormaPago.findIndex(r => r.tipoFormaPagoId == 3);
        //     if (index > -1) 
        //         this.tipoTarjetaCompartida = this.variablesApi.home.tipoFormaPago[index];

        //     if (this.medios.length !== this.variablesApi.home.tipoFormaPago.length)
        //         this.cargaDataInicial(this.variablesApi.home.tipoFormaPago);
            
        //     this._isLoading = false;
        // }, this.errorCallback)

        

        // let idComponente = this.variablesApi.home.cuentas.findIndex(r => r.cuentaDefault === true);
        // let action: RequestTipo = {
        //     accion: AccionTipo.ACCION_CLICK,
        //     datos: {
        //         cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        //     }
        // };
        // this.utils.showSpinner("Interno");
        //this.maquinaHome.homeCuentaNuevoConsultarMedioPago(action).then(respuesta => {
        //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        //         let formaPago = this.variablesApi.home.tipoFormaPago;
        //         //this.activateFormaPago = formaPago.length > 0;
        //         this.utils.hideSpinner("Interno");
        //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        //         this.utils.msgPlain(respuesta.mensajeSalida);
        //         this.utils.hideSpinner("Interno");
        //     }
        // });
    }
    
    cargaDataInicial(mediosPago) {
        this.medios = mediosPago;
        
        mediosPago.forEach(mp => {
            switch (mp.tipoFormaPagoId) {
                case 1:
                    this.config._atendedor = true;
                    break;
                case 2:
                    this.config._billetera = true;
                    break;
                case 3:
                    this.config._tarjeta = true;
                    break;
                default:
                    break;
            }
        });
        this.config._activateFormaPago = this.config._atendedor || this.config._billetera; 
    }
    
    currency(val) {
        return this.utils.formatCurrency("" + val);
    }
    
    seleccionaTarjeta() {
        let compartida: Tarjeta = new Tarjeta();
        this.utils.showSpinner("Interno");
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                formaPagoSaldo: this.config._billetera ? 1 : 0,
                formaPagoEstacion: this.config._atendedor ? 1 : 0,
                formaPagoOneclick: this.config._tarjeta ? 1 : 0,
                tarjetaCompartidaId: (this.current.tarjetaCompartidaId == "") ? "" : compartida.idInscripcion,
                tarjetaCompartidaUltimos4Digitos: compartida.ultimos4Digitos,
                tarjetaCompartidaTipo: compartida.tipoTarjetaCredito,
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            }
        };
        
        this.rx.mediosPagoModificarMedioPago(action).then(result => {

        }, this.errorCallback);

        // this.maquinaHome.homeCuentaNuevoModificarMedioPago(action).then(respuesta => {
        //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        //         this.utils.hideSpinner("Interno");
        //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        //         this.utils.hideSpinner("Interno");
        //         this.utils.msgPlain(respuesta.mensajeSalida);
        //     }
        // });
        
    }

    openAbonoSeleccion(){
        
        this.emiter.emmitChange({
            tipo: 'MetodosPago_Abono_SeleccionMedio',
            action: 'OPEN',
            data: {
                cuentasCorrientes: this.utils.clone(this.current.cuentasCorrientes)
            }
        });
    }
    retirarAbono(){
       
            var body = { "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId };
           
            this.utils.showSpinner("Interno");
            this.rx.llamarPOSTHome(body, this.constantes.CUENTA_CONSULTAR_ABONOS).then(result => {
    
                var modal = this.modalSidebar.open(RetirarAbonoComponent)
                modal.refModal.componentInstance.abonos = result.data.data
                this.utils.hideSpinner("Interno");
            }, this.errorCallback)
    
        
    }
    
}
