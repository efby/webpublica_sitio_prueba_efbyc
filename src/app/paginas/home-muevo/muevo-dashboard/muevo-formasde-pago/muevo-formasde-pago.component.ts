import { Component, OnInit, ChangeDetectorRef, ViewChild, Input } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { TipoFormaPago, Saldo } from 'src/app/globales/business-objects';
import { MuevoFormaPagoTodosComponent } from './muevo-forma-pago-todos/muevo-forma-pago-todos.component';

@Component({
    selector: 'app-muevo-formasde-pago',
    templateUrl: './muevo-formasde-pago.component.html',
    styleUrls: ['./muevo-formasde-pago.component.css']
})
export class MuevoFormasdePagoComponent implements OnInit {
    
    @ViewChild(MuevoFormaPagoTodosComponent)
    muevoFormaPagoTodosComponent: MuevoFormaPagoTodosComponent;

    //activateFormaPago: boolean = false;
    //interval: any;
    //formaPago: TipoFormaPago[] = [];
    //_cargaDashboardReady: boolean = false;

    //saldo: Saldo;
    
    constructor(private ref: ChangeDetectorRef, private variablesApi: VariablesGenerales) {
        // ref.detach();
        // this.interval = setInterval(() => {
        //     this.ref.detectChanges();
        //     this.ngOnInit()
        // }, 10);
        //this.activateFormaPago = false;
    }
    ngOnDestroy() {
        //clearInterval(this.interval);
    }
    
    ngOnInit() {
        console.log('formas de pago init..')
    }
    
    main(){
        console.log('formas de pago main..')
        //this._cargaDashboardReady = true;
        //this.formaPago = this.variablesApi.home.tipoFormaPago;
        //this.saldo = this.variablesApi.home.saldo;

        //this.activateFormaPago = this.formaPago.length > 0;
        
        this.muevoFormaPagoTodosComponent.main()
;        //this.muevoFormaPagoTodosComponent.main()
    }
    
}
