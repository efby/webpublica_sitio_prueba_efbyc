import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoFormaPagoSoloBilleteraComponent } from './muevo-forma-pago-solo-billetera.component';

describe('MuevoFormaPagoSoloBilleteraComponent', () => {
  let component: MuevoFormaPagoSoloBilleteraComponent;
  let fixture: ComponentFixture<MuevoFormaPagoSoloBilleteraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoFormaPagoSoloBilleteraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoFormaPagoSoloBilleteraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
