import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';

@Component({
  selector: 'app-muevo-forma-pago-solo-billetera',
  templateUrl: './muevo-forma-pago-solo-billetera.component.html',
  styleUrls: ['./muevo-forma-pago-solo-billetera.component.css']
})
export class MuevoFormaPagoSoloBilleteraComponent implements OnInit {

  montoTotalMes: string;
  interval: any;


  constructor(private variablesApi: VariablesGenerales, private utils: UtilitariosService, private ref: ChangeDetectorRef, private maquinaHome: MaquinaHome) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.montoTotalMes = this.currency(this.variablesApi.home.cuentaSeleccionada.gastoTotalMes);
    }, 10);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

}
