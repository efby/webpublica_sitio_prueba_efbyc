import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoFormasdePagoComponent } from './muevo-formasde-pago.component';

describe('MuevoFormasdePagoComponent', () => {
  let component: MuevoFormasdePagoComponent;
  let fixture: ComponentFixture<MuevoFormasdePagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoFormasdePagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoFormasdePagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
