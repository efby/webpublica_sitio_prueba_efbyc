import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoCupoDisponibleComponent } from './muevo-cupo-disponible.component';

describe('MuevoCupoDisponibleComponent', () => {
  let component: MuevoCupoDisponibleComponent;
  let fixture: ComponentFixture<MuevoCupoDisponibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoCupoDisponibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoCupoDisponibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
