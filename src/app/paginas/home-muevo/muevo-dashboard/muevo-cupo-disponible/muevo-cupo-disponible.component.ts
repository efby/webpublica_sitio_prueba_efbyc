import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { VariablesGenerales, TipoSaldo } from 'src/app/globales/variables-generales';
import { TipoMedioPago } from 'src/app/globales/enumeradores';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-muevo-cupo-disponible',
  templateUrl: './muevo-cupo-disponible.component.html',
  styleUrls: ['./muevo-cupo-disponible.component.css']
})
export class MuevoCupoDisponibleComponent implements OnInit {

  _abono: boolean = false;
  _credito: boolean = true;

  saldo: TipoSaldo;
  interval: any;


  constructor(private variablesApi: VariablesGenerales, private utils: UtilitariosService, private ref: ChangeDetectorRef) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.saldo = this.variablesApi.home.datosSaldo;
    }, 500);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.saldo = this.variablesApi.home.datosSaldo;
    this.saldo.saldoFinalString = this.currency(this.saldo.saldoFinal);
    this.saldo.saldoString = this.currency(this.saldo.saldo);
    this.saldo.lineaCreditoString = this.currency(this.saldo.lineaCredito);
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

}
