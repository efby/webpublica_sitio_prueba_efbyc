import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import * as shape from 'd3-shape';
import * as d3 from 'd3';

@Component({
  selector: 'app-muevo-grafico-gasto',
  templateUrl: './muevo-grafico-gasto.component.html',
  styleUrls: ['./muevo-grafico-gasto.component.css']
})
export class MuevoGraficoGastoComponent implements OnInit {
  curve: any = shape.curveBasis;
  single: any = [
    {
      "name": "Empresa",
      "series": []
    }
  ];
  multi: any[];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Dias';
  showYAxisLabel = false;
  yAxisLabel = 'Gastos';

  colorScheme = {
    domain: ['#FFFFFF']
  };

  mesActual: string = "";

  constructor(private variablesApi: VariablesGenerales) {
    Object.assign(this, this.single)
  }

  onSelect(event) {
  }

  ngOnInit() {
    let series = [];

    for (let index = 30; index > 0; index--) {
      let d = new Date();
      d.setDate(d.getDate() - index);
      let val = Math.round((Math.random() * 1000000));
      series.push({ name: d.toDateString(), value: val });
    }
    this.single[0].series = series;

    let d = new Date();
    this.setMonthName(d.getMonth());
  }

  setMonthName(m: number) {
    switch (m) {
      case 0:
        this.mesActual = "Enero";
        break;
      case 1:
        this.mesActual = "Febrero";
        break;
      case 2:
        this.mesActual = "Marzo";
        break;
      case 3:
        this.mesActual = "Abril";
        break;
      case 4:
        this.mesActual = "Mayo";
        break;
      case 5:
        this.mesActual = "Junio";
        break;
      case 6:
        this.mesActual = "Julio";
        break;
      case 7:
        this.mesActual = "Agosto";
        break;
      case 8:
        this.mesActual = "Septiembre";
        break;
      case 9:
        this.mesActual = "Octubre";
        break;
      case 10:
        this.mesActual = "Noviembre";
        break;
      case 11:
        this.mesActual = "Diciembre";
        break;
      default:
        this.mesActual = "";
        break;
    }
  }

}
