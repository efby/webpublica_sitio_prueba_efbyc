import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoGraficoGastoComponent } from './muevo-grafico-gasto.component';

describe('MuevoGraficoGastoComponent', () => {
  let component: MuevoGraficoGastoComponent;
  let fixture: ComponentFixture<MuevoGraficoGastoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoGraficoGastoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoGraficoGastoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
