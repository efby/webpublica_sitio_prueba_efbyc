import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoVehiculosComponent } from './muevo-vehiculos.component';

describe('MuevoVehiculosComponent', () => {
  let component: MuevoVehiculosComponent;
  let fixture: ComponentFixture<MuevoVehiculosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoVehiculosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoVehiculosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
