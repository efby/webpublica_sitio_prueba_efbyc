import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Vehiculos } from 'src/app/models/vehiculos';
import { Flotas, VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-muevo-vehiculos',
  templateUrl: './muevo-vehiculos.component.html',
  styleUrls: ['./muevo-vehiculos.component.css']
})
export class MuevoVehiculosComponent implements OnInit {

  @Output() emitFlota: EventEmitter<any> = new EventEmitter();
  vehiculos: Vehiculos[] = [];
  flotas: Flotas[] = [];
  interval: any;
  filter: boolean = false;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, private router: Router, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private ref: ChangeDetectorRef) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      if (!this.filter) {
        this.ngOnInit();
      }
    }, 10);

    this.vehiculos = this.variablesApi.home.vehiculos;
    this.cargaVehiculosFlotas();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.isMobile = this.variablesApi.esMobile;
    setTimeout(() => {
      this.vehiculos = this.variablesApi.home.vehiculos;
      this.cargaVehiculosFlotas();
    });
  }

  cargaVehiculosFlotas() {

    this.flotas.forEach(fl => {
      fl.vehiculosFlota = this.vehiculos.filter(r => r.flotaId === fl.flotaId);
      fl.cantidadVehiculos = fl.vehiculosFlota.length;
    });
    this.vehiculos = this.vehiculos.filter(r => r.flotaId === null);
  }

  // desactivarVehiculo(id) {
  //   let element = this.variablesApi.home.vehiculos.filter(r => r.vehiculoId === id)[0];
  //   if (element.estado) {
  //     this.maquinaModificarVehiculo(id, 0);
  //   } else {
  //     this.maquinaModificarVehiculo(id, 1);
  //   }
  // }

  mostrarVehiculoDashboard(id) {
    event.preventDefault();
    this.emiter.emitChange({
      tipo: 'Vehiculos',
      Vehiculos: true,
      idVehiculo: id
    });
  }

  // maquinaModificarVehiculo(id, estado) {
  //   this.utils.showSpinner("Interno");
  //   let element = this.variablesApi.home.vehiculos.filter(r => r.vehiculoId === id)[0];
  //   let actualUrl = this.router.url.substr(1);
  //   let action: RequestTipo = {
  //     accion: AccionTipo.ACCION_CLICK,
  //     datos: {
  //       cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
  //       flotaCuentaId: (element.flotaId == null) ? 0 : element.flotaId,
  //       vehiculoCuentaId: element.vehiculoId,
  //       vehiculoCuentaPatente: element.patente,
  //       vehiculoCuentaModelo: element.modelo,
  //       vehiculoCuentaMarca: element.marca,
  //       vehiculoCuentaAno: (element.ano == null) ? 0 : element.ano,
  //       vehiculoCuentaTanque: element.tanque,
  //       vehiculoCuentaAlias: element.alias,
  //       vehiculoCuentaTipo: element.tipo,
  //       vehiculoCuentaValidado: 1,
  //       vehiculoCuentaEstado: estado
  //     }
  //   }
  //   this.maquinaHome.homeVehiculoModificar(action).then(respuesta => {
  //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
  //       this.utils.hideSpinner("Interno");
  //       // if (actualUrl !== respuesta.paginaRespuesta) {
  //       //   this.router.navigate([respuesta.paginaRespuesta]);
  //       // } else {
  //       this.ngOnInit();
  //       // }
  //       // this.router.navigate([Paginas.HomeFlotasVehiculo, element.flotaId]);
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
  //       this.utils.hideSpinner("Interno");
  //       this.utils.openModal(respuesta.modalSalida).then(response => {
  //         console.log(response);
  //       });
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
  //       this.utils.hideSpinner("Interno");
  //       this.utils.msgPlain(respuesta.mensajeSalida);
  //     }
  //   });
  // }

  searchComponent(searchValue: string) {
    if (searchValue === "") {
      this.filter = false;
    } else {
      this.vehiculos = this.variablesApi.home.vehiculos.filter(function (f) {
        return (
          f.patente.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase().match(searchValue.toUpperCase()) ||
          f.alias.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase().match(searchValue.toUpperCase()) ||
          f.marca.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase().match(searchValue.toUpperCase()) ||
          f.modelo.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase().match(searchValue.toUpperCase()) ||
          f.ano.toString().match(searchValue.toUpperCase())
        );
      });
      this.filter = true;
    }
  }

}
