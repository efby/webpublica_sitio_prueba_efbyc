import { Component, OnInit, Input, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MuevoVehiculosComponent } from './muevo-vehiculos/muevo-vehiculos.component';
import { MuevoConductoresComponent } from './muevo-conductores/muevo-conductores.component';

@Component({
  selector: 'app-muevo-gastos',
  templateUrl: './muevo-gastos.component.html',
  styleUrls: ['./muevo-gastos.component.css']
})
export class MuevoGastosComponent implements OnInit {

  @ViewChild(MuevoVehiculosComponent) private vehiculos: MuevoVehiculosComponent;
  @ViewChild(MuevoConductoresComponent) private conductores: MuevoConductoresComponent;

  @Input() activateConductorVehiculo: number = 0;
  @Input() searchString: string = "";
  oldSearch: string = "";
  interval: any;

  constructor(private ref: ChangeDetectorRef) {
    ref.detach();
    
    this.activateConductorVehiculo = 0;
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
  }


}
