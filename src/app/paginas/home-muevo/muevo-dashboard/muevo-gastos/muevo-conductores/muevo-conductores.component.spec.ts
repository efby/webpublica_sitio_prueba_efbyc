import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoConductoresComponent } from './muevo-conductores.component';

describe('MuevoConductoresComponent', () => {
  let component: MuevoConductoresComponent;
  let fixture: ComponentFixture<MuevoConductoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoConductoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoConductoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
