import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Conductor } from 'src/app/models/conductores';
import { Grupos, VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-muevo-conductores',
  templateUrl: './muevo-conductores.component.html',
  styleUrls: ['./muevo-conductores.component.css']
})
export class MuevoConductoresComponent implements OnInit {

  @Output() emitGrupo: EventEmitter<any> = new EventEmitter();
  conductores: Conductor[];
  grupos: Grupos[];
  interval: any;
  filter: boolean = false;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, private router: Router, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private ref: ChangeDetectorRef) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      if (!this.filter) {
        this.iniciaVariablesGlobales();
      }
    }, 100);

    this.iniciaVariablesGlobales();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {

  }

  iniciaVariablesGlobales() {
    this.isMobile = this.variablesApi.esMobile;
    //this.modCurrency();
    this.conductores = this.variablesApi.home.conductores;
  }

  // modCurrency() {
  //   if(this.variablesApi.home.conductores == undefined)
  //     return;

  //   this.variablesApi.home.conductores.forEach(element => {
  //     element.disponibleDiaString = this.currency((element.maximoVentaDia - element.sumaVentaDia));
  //     element.disponibleMesString = this.currency((element.maximoVentaMes - element.sumaVentaMes))
  //   });
  // }

  // date2Human(date) {
  //   let format = 'dd/MM/yyyy';
  //   return this.utils.date2Human(date, format, this.router.url);
  // }

  // cargaConductoresGrupos() {
  //   this.grupos.forEach(gr => {
  //     gr.conductoresGrupo = this.conductores.filter(r => r.grupoId === gr.grupoId);
  //     gr.cantidadConductores = gr.conductoresGrupo.length;
  //   });
  //   this.conductores = this.conductores.filter(r => r.grupoId === null);
  // }

  activarEdicion(id) {
    let idComponente = this.grupos.findIndex(r => r.grupoId === id);
    for (let index = 0; index < this.grupos.length; index++) {
      this.grupos[index].editable = false;
    }
    this.grupos[idComponente].editable = true;
  }

  activaDesactiva(id: number) {
    let conductor = this.conductores.filter(r => r.usuarioId === id)[0];
    if (conductor.estado) {
      this.maquinaModificarConductor(id, 0);
    } else {
      this.maquinaModificarConductor(id, 1);
    }
  }

  maquinaModificarConductor(id: number, estado: number) {
    let conductor = this.conductores.filter(r => r.usuarioId === id)[0];
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        usuarioId: id,
        //grupoCuentaId: (conductor.grupoId == null) ? 0 : conductor.grupoId,
        usuarioCuentaEstadoId: estado,
        rolId: conductor.rolId,
        usuarioCuentaConduce: conductor.conduce,
        // usuarioCuentaRindeGasto: conductor.usuarioCuentaRindeGasto,
        // usuarioCuentaCreaVehiculo: conductor.usuarioCuentaCreaVehiculo,
        // usuarioCuentaEligeDTE: conductor.usuarioCuentaEligeDTE,
        // usuarioCuentaSeleccionado: conductor.usuarioCuentaSeleccionado,
        vehiculosAsignados: []
      }
    }

    this.maquinaHome.homeUsuarioModificar(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        this.ngOnInit();
        // }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }
 
  mostrarConductorDashboard(id, event) {
    event.preventDefault();
    event.stopPropagation();
    this.emiter.emitChange({
      tipo: 'Conductores',
      Conductores: true,
      idConductor: id
    });
  }


  muestraDiaHorario() {
    this.emiter.emitChange({
      tipo: 'DiaHorario',
      diaHorario: true
    });
  }
  muestraEstaciones() {
    this.emiter.emitChange({
      tipo: 'Estacion',
      estacion: true
    });
  }

  formatRut(rut: string) {
    let actual = rut.replace(/^0+/, "");
    let rutPuntos = "";
    if (actual != '' && actual.length > 1) {
      let sinPuntos = actual.replace(/\./g, "");
      let actualLimpio = sinPuntos.replace(/-/g, "");
      let inicio = actualLimpio.substring(0, actualLimpio.length - 1);
      let i = 0;
      let j = 1;
      for (i = inicio.length - 1; i >= 0; i--) {
        let letra = inicio.charAt(i);
        rutPuntos = letra + rutPuntos;
        if (j % 3 == 0 && j <= inicio.length - 1) {
          rutPuntos = "." + rutPuntos;
        }
        j++;
      }
      let dv = actualLimpio.substring(actualLimpio.length - 1);
      rutPuntos = rutPuntos + "-" + dv;
    }
    return rutPuntos;
  }
  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

}
