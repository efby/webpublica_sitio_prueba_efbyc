import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoGastosComponent } from './muevo-gastos.component';

describe('MuevoGastosComponent', () => {
  let component: MuevoGastosComponent;
  let fixture: ComponentFixture<MuevoGastosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoGastosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoGastosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
