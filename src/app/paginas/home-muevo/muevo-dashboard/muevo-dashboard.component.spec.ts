import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoDashboardComponent } from './muevo-dashboard.component';

describe('MuevoDashboardComponent', () => {
  let component: MuevoDashboardComponent;
  let fixture: ComponentFixture<MuevoDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
