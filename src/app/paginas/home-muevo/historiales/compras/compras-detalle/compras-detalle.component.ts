import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Transaccion } from 'src/app/globales/business-objects';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RxService } from 'src/app/servicios/rx.service';
import { PageBase } from '../../../pageBase';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
    selector: 'app-compras-detalle',
    templateUrl: './compras-detalle.component.html',
    styleUrls: ['./compras-detalle.component.scss']
})
export class ComprasDetalleComponent extends PageBase implements OnInit {
    // @Input('item')
    current: Transaccion;
    DOC_NO_DISPONIBLE = "documento no disponible";
    activaDescarga = false;

    constructor(public ref:ChangeDetectorRef, public router:Router, public utils:UtilitariosService, private rx:RxService, private variablesApi:VariablesGenerales) { 
        super(ref, utils, router)

        this.reset();
    }
    
    ngOnInit() {
        // console.log(this.variablesApi.home.maestros.tipo)
    }

    reset(){
        this.current = new Transaccion();
        this.activaDescarga = false;
    }

    getData(item: Transaccion){
        this.reset();

        this.utils.showSpinner("Interno")
        this.rx.transaccionesDetalleGet(item.cuentaId, item.ventaId).then((result:any) => {
            this.current = result.data;
            
            const aux = this.current.documentoUrl != undefined && this.current.documentoUrl.toLowerCase().indexOf(this.DOC_NO_DISPONIBLE) >= 0;
            this.activaDescarga = !aux;

            this.utils.hideSpinner("Interno")
        }, this.errorCallback);
    }

    verDocumento(){
        const ventana = window.open(this.current.documentoUrl,"_blank");
    }

    
}
