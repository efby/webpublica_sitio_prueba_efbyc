import { Component, OnInit, ChangeDetectorRef, ViewChild, Input } from '@angular/core';

import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { PageBase } from '../../pageBase';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Transaccion, TableDataCsv, IPagination } from 'src/app/globales/business-objects';


import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { ExportDataService } from 'src/app/servicios/export-data.service';
import { DatePipe } from '@angular/common';
//import { matRangeDatepickerRangeValue, matRangeDatepickerInputEvent } from 'mat-range-datepicker';

import * as moment from 'moment';
import 'moment/min/locales';
import { MuevoCalendarComponent } from 'src/app/components/muevo-calendar/muevo-calendar.component';
import { window } from 'rxjs/operators';
moment.locale('es')

@Component({
    selector: 'app-compras',
    templateUrl: './compras.component.html',
    styleUrls: ['./compras.component.scss']
})
export class ComprasComponent extends PageBase implements OnInit {

    @Input() input: {
        facturaId: number,
        facturaNroDocumento: number
    };
    showSearchAdvanced: boolean = false;

    filterFacturaApply: boolean;
    filterDateApplycated: boolean;
    transacciones: Transaccion[];

    displayedColumns = [
        'ventaFechaCreacion',
        'tipoDocumentoNombre',
        'usuarioNombreApellido',
        'vehiculoPatente',
        'tipoFormaPagoNombre',
        'ventaPagoTotal',
        'ventaDocumentoFolio',
        'actions']
    dataSource: MatTableDataSource<Transaccion> = new MatTableDataSource<Transaccion>();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @ViewChild(MuevoCalendarComponent)
    muevoCalendarComponent: MuevoCalendarComponent;

    maxDate: Date;

    conductores: any[];
    facturas: any[];
    vehiculos: any[];

    filters: {
        vehiculo: any,
        usuario: any,
        documento: any,
        fecha: any
    }

    pagination: IPagination;

    defaultDates: {
        start: Date,
        end: Date
    }

    constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private rx: RxService, public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router) {
        super(ref, utils, router);

        this.transacciones = [];
        this.maxDate = moment().endOf('month').toDate();

        this.filters = {
            vehiculo: null,
            usuario: null,
            documento: null,
            fecha: null
        };

        this.vehiculos = [
            { id: 1, name: 'XXCR01 - auto test 01' },
            { id: 2, name: 'XXCR02 - auto test 02' },
            { id: 3, name: 'XXCR03 - auto test 03' },
            { id: 4, name: 'XXCR04 - auto test 04' }
        ];

        this.conductores = [
            { id: 1, name: 'Cristian Rojas Caceres - 16.122.944-k' },
            { id: 2, name: 'Daniela Rossi Sanchez - 17.040.705-9' }
        ]

        this.facturas = [
            { id: 1000000, name: '1000000 - Emitida 07/07/2020' },
            { id: 1000001, name: '1000001 - Emitida 07/07/2020' }
        ];

        this.pagination = {
            currentPage: 0,
            pages: 0,
            totalRows: 0,
            positionFinal: 0,
            pagesSize: 0,
            rows: 0
        }

        this.setDefaultDates();

    }

    ngOnInit() {
        // let start = moment(this.defaultDates.start).format('YYYY-MM-DD');
        // let end = moment(this.defaultDates.end).format('YYYY-MM-DD');

        this.setPage(1);
        //this.obtenerTransacciones(start, end, this.pagination);
        //this.obtenerTransacciones(moment().format('YYYY-MM-DD'), this.pagination);
    }



    generateFilterArray() {
        let result: any[] = [];
        for (let f in this.filters) {
            result.push(f);
        }
        //console.log(result);
        return result;
        //return Object.keys(obj).map((key)=>{ return obj[key]});
    }

    removeFilter(f: any) {

        this.filters[f] = null;
        this.transacciones = [];

        if (f == 'fecha') {
            this.setDefaultDates();
            const start = this.defaultDates.start;
            const end = this.defaultDates.end;
            this.muevoCalendarComponent.SetDates(start, end);
        }

        this.setPage(1);
    }

    private setDefaultDates() {
        this.defaultDates = {
            start: moment().startOf('month').toDate(),
            end: moment().endOf('month').toDate()
        }
    }

    filterByDates(e) {
        this.filterDateApplycated = true;

        let start = moment(e.start).add(23, 'h').add(59, 'm').add(59, 's');
        let end = moment(e.end).add(1, 'day')

     

        // let dataFiltred = this.transacciones.filter((x:Transaccion) => {
        //     return start <= moment(x.ventaFechaCreacion) && end >= moment(x.ventaFechaCreacion)
        // });
        // this.bindTableWithtOriginalData(dataFiltred);

        /**-------------- */
        this.defaultDates.start = start.toDate();
        this.defaultDates.end = end.toDate();

        this.pagination.positionFinal = 0;
        this.filters.fecha = { id: '', name: `Registros entre el ${start.format('DD-MM-YYYY')} y el ${end.format('DD-MM-YYYY')}` };
        this.transacciones = [];
        this.obtenerTransacciones(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'), this.pagination);

    }

    bindTableWithtOriginalData(data: Transaccion[]) {
        this.dataSource = new MatTableDataSource(data);
        this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);
    }

    viewDetail(item: Transaccion) {
        this.emiter.emitChange({
            tipo: 'CompraDetalle',
            item: item
        });
    }

    applyFilterByFactura(facturaId: number) {
        this.filterFacturaApply = true;
        let dataByFactura = this.transacciones.filter((x: Transaccion) => {
            return x.ventaDocumentoId == facturaId
        });
        this.bindTableWithtOriginalData(dataByFactura);
    }

    refreshHistory() {
        this.router.navigate([Paginas.HomeMuevoMisTransacciones])
    }

    exportCsv() {

        let data: TableDataCsv = {
            header: ['FECHA', 'TIPO DOCUMENTO', 'CONDUCTOR', 'PATENTE', 'FORMA DE PAGO', 'DIRECCIÓN', 'MONTO', 'Nº FOLIO', 'URL', 'FACTURA'],
            rows: this.dataSource.data.map((x: Transaccion) => {
                return [
                    moment(x.ventaFechaCreacion).format('DD-MM-YYYY HH:mm'),
                    x.tipoDocumentoNombre || '',
                    x.usuarioNombreApellido || '',
                    x.vehiculoPatente || '',
                    x.tipoFormaPagoNombre || '',
                    x.ventaSitioDireccion || '', 
                    x.ventaPagoTotal,
                    x.ventaDocumentoFolio || '', 
                    x.documentoUrl || '', 
                    x.ventaDocumentoFolio || 0,
                    
                ]
            })
        };

        ExportDataService.ExportCsv(this.generateFilenameFormat('compras_', this.variablesApi.home.cuentaSeleccionada.cuentaId.toString()), data.rows, data.header, ';', ',', 'Compras MUEVO EMPRESAS')
    }

    private generateFilenameFormat(prefix: string, key: string) {
        return prefix + `${key}_${moment().format('YYYYMMDD_HHmm')}`;
    }

    private obtenerTransacciones(fechaInicio, fechaFin: string, paginacion: IPagination) {
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

        //this.utils.showSpinner("Interno");
        this._isLoading = true;
        this.rx.transaccionesGet(cuentaId, fechaInicio, fechaFin, paginacion.positionFinal).then((result: any) => {
            this._isLoading = false;
            this.transacciones = this.transacciones.concat(result.data.transacciones);

            if (this.input.facturaId > 0)
                this.applyFilterByFactura(this.input.facturaId);
            else {
                this.bindTableWithtOriginalData(this.transacciones);
                //this.utils.hideSpinner("Interno");
            }

            this.paginationHandler(result.data.pagination);

        },(err)=>{
            this.errorCallback(err)
            this.filters.fecha=null
        } )
    }

    paginationHandler(configResponse: any) {
        this.pagination.positionFinal = configResponse.posicionFinal;
        this.pagination.totalRows = configResponse.totalRegistros;
        this.pagination.pagesSize = configResponse.registrosPorPagina;
        this.pagination.rows = configResponse.registrosObtenidos;


        // this.pagination.positionFinal = configResponse.posicionFinal;
        // this.pagination.totalRows = configResponse.totalRegistros;
        // this.pagination.pagesSize = configResponse.registrosPorPagina;
        // this.pagination.rows = configResponse.registrosObtenidos;

        // let mod = this.pagination.totalRows % configResponse.registrosPorPagina;
        // this.pagination.pages = this.pagination.totalRows / configResponse.registrosPorPagina;

        // if(mod > 0)
        //     this.pagination.pages += 1;

        // this.pagination.pages = parseInt(this.pagination.pages.toString());


        // this.pagination.currentPage = this.pagination.positionFinal / configResponse.registrosPorPagina;
        // this.pagination.currentPage = parseInt(this.pagination.currentPage.toString())

    }

    setPage(page) {
        if (page == 0)
            return;
        // else if(page == 1 && this.pagination.currentPage == 1)
        //     return;
        else if (this.pagination.pages > 0 && page > this.pagination.pages)
            return


        if (page >= 1)
            this.pagination.currentPage = page;
        this.pagination.positionFinal = (page * this.pagination.pagesSize) - this.pagination.pagesSize;

        let start = moment(this.defaultDates.start).format('YYYY-MM-DD');
        let end = moment(this.defaultDates.end).add(1, 'day').format('YYYY-MM-DD');

        this.obtenerTransacciones(start, end, this.pagination);
    }

    next() {
        let start = moment(this.defaultDates.start).format('YYYY-MM-DD');
        let end = moment(this.defaultDates.end).add(1, 'day').format('YYYY-MM-DD');

        this.obtenerTransacciones(start, end, this.pagination)
    }

}