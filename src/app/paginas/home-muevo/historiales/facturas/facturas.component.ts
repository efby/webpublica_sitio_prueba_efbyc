import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { PageBase } from '../../pageBase';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { Facturas } from 'src/app/models/facturas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';

import * as moment from 'moment';
import 'moment/min/locales';
import { ExportDataService } from 'src/app/servicios/export-data.service';
import { TableDataCsv, IPagination } from 'src/app/globales/business-objects';
import { MuevoCalendarComponent } from 'src/app/components/muevo-calendar/muevo-calendar.component';

moment.locale('es')

@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.component.html',
  styleUrls: ['./facturas.component.scss']
})
export class FacturasComponent extends PageBase implements OnInit {

    filterDateApplycated: boolean;
    facturas: Facturas[];

    displayedColumns = [ 'facturaFechaEmision', 'facturaNroDocumento', 'facturaMontoTotal', 'facturaFechaVencimiento', 'facturaPagadaString', 'actions']
    dataSource: MatTableDataSource<Facturas> = new MatTableDataSource<Facturas>();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @ViewChild(MuevoCalendarComponent) 
    muevoCalendarComponent: MuevoCalendarComponent;

    maxDate: Date;

    filters: {
        vehiculo: any,
        usuario: any,
        documento: any,
        fecha: any
    }

    pagination: IPagination;

    defaultDates: {
        start:Date,
        end: Date
    }

    constructor(private emiter: BreadCrumbsService, private variablesApi:VariablesGenerales, private rx: RxService, public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router) { 
        super(ref, utils, router);
 
        this.facturas = [];
        this.maxDate = moment().endOf('month').toDate()

        this.filters = {
            vehiculo: null,
            usuario: null,
            documento: null,
            fecha: null
        };

        this.pagination = {
            currentPage: 0,
            pages: 0,
            totalRows: 0, 
            positionFinal: 0,
            pagesSize: 0,
            rows: 0
        }

        this.setDefaultDates();
    }
    
    ngOnInit() {
        //this.obtenerFacturas(moment().format('YYYY-MM-DD'));
        this.setPage(1);
    }

    generateFilterArray(){
        let result:any[] = [];
        for(let f in this.filters){
            result.push(f);
        }
        //console.log(result);
        return result;
        //return Object.keys(obj).map((key)=>{ return obj[key]});
    }

    removeFilter(f:any){

        this.filters[f] = null;
        this.facturas = [];

        if(f == 'fecha'){
            this.setDefaultDates();
            const start = this.defaultDates.start;
            const end = this.defaultDates.end;
            this.muevoCalendarComponent.SetDates(start, end);
        }
            
        this.setPage(1);
    }

    private setDefaultDates(){
        this.defaultDates = {
            start: moment().startOf('month').toDate(),
            end: moment().endOf('month').toDate()
        }
    }

    filterByDates(e){
        this.filterDateApplycated = true;

        let start = moment(e.start).add(23, 'h').add(59, 'm').add(59, 's')
        let end = moment(e.end).add(1,'day').add(23, 'h').add(59, 'm').add(59, 's');

        // let dataFiltred = this.facturas.filter((x:Facturas) => {
        //     return start <= moment(x.facturaFechaEmision) && end >= moment(x.facturaFechaEmision)
        // });
        // this.bindTableWithtOriginalData(dataFiltred)

        /**-------------- */
        this.defaultDates.start = start.toDate();
        this.defaultDates.end = end.toDate();

        this.pagination.positionFinal = 0;
        this.filters.fecha = { id: '', name: `Registros entre el ${start.format('DD-MM-YYYY')} y el ${end.format('DD-MM-YYYY')}` };
        this.facturas = [];
        this.obtenerFacturas(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'), this.pagination);
        
    }

    bindTableWithtOriginalData(data:Facturas[]){
        this.dataSource = new MatTableDataSource(data);
        this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);
    }

    viewDetail(item:Facturas){
        this.emiter.emitChange({
            tipo: 'FacturaDetalle',
            cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            facturaId: item.facturaId,
            item: item
        });
    }


    exportCsv(){
        
        let data: TableDataCsv = {
            header:  ['FACTURA', 'FECHA', 'Nº DOCUMENTO', 'MONTO', 'FECHA DE VENCIMIENTO', 'ESTADO','URL' ],
            rows: this.dataSource.data.map((x:Facturas) => { 
                return [
                    x.facturaId,
                    moment(x.facturaFechaEmision).format('DD-MM-YYYY HH:mm'),
                    x.facturaNroDocumento,
                    x.facturaMontoTotal,
                    moment(x.facturaFechaVencimiento).format('DD-MM-YYYY HH:mm'),
                    x.facturaPagadaString || '',
                    x.facturaUrl || '',
                ]
            })
        };

        ExportDataService.ExportCsv(this.generateFilenameFormat('compras_', this.variablesApi.home.cuentaSeleccionada.cuentaId.toString()), data.rows, data.header, ';', ',', 'Facturas MUEVO EMPRESAS')
    }

    private generateFilenameFormat(prefix:string, key:string){
        return prefix + `${key}_${moment().format('YYYYMMDD_HHmm')}`;
    }

    private obtenerFacturas(fechaInicio, fechaFin:string, paginacion:IPagination){
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

        //this.utils.showSpinner("Interno");
        this._isLoading = true;
        this.rx.facturasGet(cuentaId, fechaInicio, fechaFin, paginacion.positionFinal).then((result:any) => {
            this._isLoading = false;
            this.facturas = this.facturas.concat(result.data.facturas);
            
            this.bindTableWithtOriginalData(this.facturas);
            //this.utils.hideSpinner("Interno");

            this.paginationHandler(result.data.pagination);

        }, (err)=>{
            this.errorCallback(err)
            this.filters.fecha=null
        } )
    }

    paginationHandler(configResponse:any){
        this.pagination.positionFinal = configResponse.posicionFinal;
        this.pagination.totalRows = configResponse.totalRegistros;
        this.pagination.pagesSize = configResponse.registrosPorPagina;
        this.pagination.rows = configResponse.registrosObtenidos;
    }

    setPage(page){
        if(page == 0)
            return;
        // else if(page == 1 && this.pagination.currentPage == 1)
        //     return;
        else if(this.pagination.pages > 0 && page > this.pagination.pages)
            return


        if(page >= 1)
            this.pagination.currentPage = page;
        this.pagination.positionFinal = (page * this.pagination.pagesSize) - this.pagination.pagesSize;

        let start = moment(this.defaultDates.start).format('YYYY-MM-DD');
        let end = moment(this.defaultDates.end).add(1,'day').format('YYYY-MM-DD');

        this.obtenerFacturas(start, end, this.pagination);
    }

    next(){
        let start = moment(this.defaultDates.start).format('YYYY-MM-DD');
        let end = moment(this.defaultDates.end).add(1,'day').format('YYYY-MM-DD');

        this.obtenerFacturas(start, end, this.pagination)
    }
}
