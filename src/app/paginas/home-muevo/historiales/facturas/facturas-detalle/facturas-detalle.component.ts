import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PageBase } from '../../../pageBase';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Transaccion } from 'src/app/globales/business-objects';
import { Facturas } from 'src/app/models/facturas';
import { Paginas } from 'src/app/globales/paginas';

@Component({
    selector: 'app-facturas-detalle',
    templateUrl: './facturas-detalle.component.html',
    styleUrls: ['./facturas-detalle.component.scss']
})
export class FacturasDetalleComponent extends PageBase implements OnInit {
    
    current: Facturas;

    constructor(public ref:ChangeDetectorRef, public router:Router, public utils:UtilitariosService, private rx:RxService, private variablesApi:VariablesGenerales) { 
        super(ref, utils, router)
        
        this.current = new Facturas();
    }
    
    ngOnInit() {

    }
    
    setData(cuentaId:number, facturaId:number, item:Facturas){
        this.utils.showSpinner("Interno")
        this.rx.facturasDetalleGet(cuentaId, facturaId).then((result:any) => {
            this.current = result.data.factura;
            this._pageReady = true;
            this.utils.hideSpinner("Interno")
        }, this.errorCallback);
    }

    verFactura(){
        const ventana = window.open(this.current.facturaUrl,"_blank");
    }

    verDetalleCompras(facturaId:number, facturaNroDocumento:number){
        this.router.navigate([Paginas.HomeMuevoMisTransacciones, { facturaId: facturaId, facturaNroDocumento: facturaNroDocumento }],  );
    }
    
}
