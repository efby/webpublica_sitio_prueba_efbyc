import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { Subject } from 'rxjs';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { PageBase } from '../pageBase';
import { ComprasComponent } from './compras/compras.component';

@Component({
    selector: 'app-historiales',
    templateUrl: './historiales.component.html',
    styleUrls: ['./historiales.component.scss']
})
export class HistorialesComponent extends PageBase implements OnInit {

    comprasParams: {
        facturaId:number;
        facturaNroDocumento: number;
    }

    navegacion = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Historial", class: "active", url: "" }
    ];
    
    private unsubscribe = new Subject();

    ui: {
        switchData: string
    }
    
    constructor(private route: ActivatedRoute, private rx: RxService, public utils: UtilitariosService, public ref: ChangeDetectorRef, public router: Router, private emiter: BreadCrumbsService, private variablesApi:VariablesGenerales) { 
        super(ref, utils, router);

        this.emiter.emitChange({ migajas: this.navegacion, muestraBreadcrumbs: true });

        this.ui = {
            switchData: 'compras'
        }

        this.comprasParams = {
            facturaId: this.route.snapshot.paramMap.get('facturaId') == null ? 0 : +this.route.snapshot.paramMap.get('facturaId'),
            facturaNroDocumento: this.route.snapshot.paramMap.get('facturaNroDocumento') == null ? 0 : +this.route.snapshot.paramMap.get('facturaNroDocumento')
        } 
    }
    
    ngOnInit() {
        
    }

    toggleSwitchData(tabName:string){
        this.ui.switchData = tabName;
    }
    
}
