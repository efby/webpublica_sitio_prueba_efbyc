import { ChangeDetectorRef } from "@angular/core";
import { MatPaginator, MatTableDataSource, MatSort } from "@angular/material";
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Respuestas } from 'src/app/globales/respuestas';
import { Paginas } from 'src/app/globales/paginas';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { VariablesGenerales } from "src/app/globales/variables-generales";
import * as moment from 'moment';
import 'moment/min/locales';

moment.locale('es')


export class PageBase {
    _pageReady: boolean;
    _showResult: boolean;
    _isLoading: boolean;
    _operationPrincipalIsSuccess: boolean;

    _pattern_FormatoPatente = /^([a-zA-Z]{2}[a-zA-Z0-9]{2}[0-9]{1,2}$)/
    _pattern_typeNumero =/[0-9]*/
    _pattern_typePatente = /^[a-zA-Z0-9]*/
    _pattern_typeMail = /^[0-9A-Za-z@\.\_\-]*/
    _pattern_FormatoMAil = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    _pattern_FormatoDefault = /^[0-9A-Za-zÑñÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïö&\[^¨\]]{1}[0-9A-Za-zÑñÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïö&\\s]*/;

    _TypeDefault(event){
        let match = /^[0-9A-Za-zÑñÁÉÍÓÚáéíóúÀÈÌÒÙàèìòù&\\s ]*/.exec(event.key)
        if (match != null && match[0] == "") {
            event.preventDefault();
            console.log('cancel key: ', event.key)
        }
    }

    _TypeMail(event) {
        
        let match = this._pattern_typeMail.exec(event.key)
        if (match != null && match[0] == "") {
            event.preventDefault();
            console.log('cancel key: ', event.key)
        }
    }

    _TypePatente(event: any) {
        let match =this._pattern_typePatente.exec(event.key);

        if (match != null && match[0] == "") {
            event.preventDefault();
            console.log('cancel key: ', event.key)
        }
    }



    _alertMessage: {
        showAlert: boolean,
        message: string,
        type: string
    }

    constructor(
        public cdr: ChangeDetectorRef,
        public utils: UtilitariosService,
        public router: Router) {

        this._alertMessage = {
            showAlert: false,
            message: '',
            type: ''
        }
        this._pageReady = false;
        this._isLoading = false;
        this._operationPrincipalIsSuccess = false;
    }

    _errorCallback = (error) => {
        this._showResult = false;
        let msg = this._proccessErrorMesssage(error);
        this._manageAlert('danger', msg || 'Problemas al ejecutar la operación', true);
    }

    _proccessErrorMesssage = (result): string => {
        let msg = undefined;
        if (result.hasOwnProperty('error') && result.error != undefined) {
            if (result.error.hasOwnProperty('error') && result.error.error) {
                if (result.error.error.hasOwnProperty('userMessage') && result.error.error.userMessage)
                    msg = result.error.error.userMessage;
            }
        }
        return msg;
    }

    _manageAlert(type: string, message: string, show: boolean) {
        this._alertMessage.showAlert = show;
        this._alertMessage.message = message;
        this._alertMessage.type = type;
        this.cdr.detectChanges();
    }

    _sorting(item, property) {
        if (property.includes('.')) {
            return property.split('.')
                .reduce((object, key) => object[key], item);
        }
        return item[property];
    }

    public _haveRows = (list: any[]) => {
        return list != undefined && list.length > 0;
    }

    _configureFunctionOfMatTable(dataSource: MatTableDataSource<any>, paginator: MatPaginator, sort: MatSort) {
        //Configura el ordenamiento
        if (sort) {
            dataSource.sort = sort;
            dataSource.sortingDataAccessor = this._sorting;
        }

        //Configura la paginacion
        if (paginator) {
            dataSource.paginator = paginator;
            this._translatePaginator(paginator);
        }

        //Configura los filtros de busqueda
        dataSource.filterPredicate = (data: any, filter) => {
            const dataStr = JSON.stringify(data).toLowerCase();
            return dataStr.indexOf(filter) != -1;
        }
        this.cdr.detectChanges();
    }

    _applyFilter(dataSource: any, filterValue: string) {
        dataSource.filter = filterValue.trim().toLowerCase();
    }

    /** CHECKBOX FUNCTIONS */
    /** Whether the number of selected elements matches the total number of rows. */
    _checkToggle(tableSelection, row, e) {
        let res = e ? tableSelection.toggle(row) : null;
        this.cdr.detectChanges();
    }

    _isAllSelected(selection, datasource) {
        const numSelected = selection.selected.length;
        const numRows = datasource.data.length;
        return numSelected === numRows;
    }

    /** The label for the checkbox on the passed row */
    _checkboxLabel(selection: any, datasource, row?: any): string {
        if (!row)
            return `${this._isAllSelected(selection, datasource) ? 'select' : 'deselect'} all`;
        return `${selection.isSelected(row) ? 'deselect' : 'select'} row`;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    _masterToggle(selection, datasource) {
        this._isAllSelected(selection, datasource) ?
            selection.clear() :
            datasource.data.forEach(row => selection.select(row));
        this.cdr.detectChanges();
    }

    _validaInput(ingreso: any) {
        let item = ingreso.srcElement;
        if (item.maxLength > 0) {
            item.value = this.utils.formatMaxInput(item.value, item.maxLength);
        }
        item.value = this.utils.formatNumbers(item.value);
        item.value = this.utils.formatCurrency("" + item.value);
    }

    _showControlError = (control: FormControl) => {
        //fg, fieldname: string
        let res = this.utils.showControlError(control);
        return res;
    }


    errorCallback = (result: any) => {

        console.log(result);
        this._isLoading = false;
        this.utils.hideSpinner("Interno");

        if (result.estado == Respuestas.RESPUESTA_NOK || result.estado == Respuestas.RESPUESTA_SC) {
            //ANIMACION
            let message = result.data != undefined ? result.data.message : 'Problemas al ejecutar el servicio, Reintente!'
            let msg = {
                title: 'Error',
                messages: [message]
            }
            this.utils.openModal(Paginas.ModalAceptar, msg);
        }
        // else if(result.estado == Respuestas.RESPUESTA_SC){
        //     //MODAL
        //     this.utils.openModal(Paginas.ModalSCComponent).then(response => {
        //         console.log(response);
        //     });
        // }
        else if (result.estado == Respuestas.RESPUESTA_SA) {
            this.router.navigate([Paginas.ErrorSA]);
        }
    }


    private _translatePaginator(paginator: MatPaginator) {
        if (paginator == undefined || paginator._intl == undefined)
            return;
        paginator._intl.itemsPerPageLabel = 'Registros por página';
        paginator._intl.nextPageLabel = 'Siguiente página';
        paginator._intl.lastPageLabel = 'Última página';
        paginator._intl.previousPageLabel = 'Página anterior';
        paginator._intl.firstPageLabel = 'Primera página';
    }

    currency(val) {
        return this.utils.formatCurrency("" + val);
    }
    fechatToString(val) {

        let fecha = val.substring(0, 10);
        let listaMeses: Array<string> = [
            "enero",
            "febrero",
            "marzo",
            "abril",
            "mayo",
            "junio",
            "julio",
            "agosto",
            "septiembre",
            "octubre",
            "noviembre",
            "diciembre",
        ];
        let mes = listaMeses[parseInt(fecha.substring(5, 7)) - 1];
        return fecha.substring(8, 10) + " de " + mes + " del " + fecha.substring(0, 4);

    }

    obtenerValor(value: any, key: string, array: Array<any>, campo: string) {

        let elementoFiltrado = array.filter(x => x[key] == value);
        if (elementoFiltrado.length > 0) {
            return array.filter(x => x[key] == value)[0][campo];
        }
        return value;

    }

    formatearRut(rut) {
        // XX.XXX.XXX-X
        let newRut = rut.replace(/\./g, '').replace(/\-/g, '').trim().toLowerCase();
        let lastDigit = newRut.substr(-1, 1);
        const rutDigit = newRut.substr(0, newRut.length - 1)
        let format = '';
        for (let i = rutDigit.length; i > 0; i--) {
            const e = rutDigit.charAt(i - 1);
            format = e.concat(format);
            if (i % 3 === 0) {
                format = '.'.concat(format);
            }
        }
        return format.concat('-').concat(lastDigit);
    }

    convertDateToISOString(fecha: string): string {
        let d = moment(fecha, "YYYY-MM-DD HH:mm:ss")
        //console.log(fecha, '->', d.toISOString());
        return d.toISOString();
    }

    _validaMaxInputMail(mail) {


        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(mail)) {
            return true;
        }
        return false;
    }



}