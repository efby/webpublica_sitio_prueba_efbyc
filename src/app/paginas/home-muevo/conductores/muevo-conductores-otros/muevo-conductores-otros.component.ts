import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Conductor } from 'src/app/models/conductores';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { PageBase } from '../../pageBase';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { ExportDataService } from 'src/app/servicios/export-data.service';
import { TableDataCsv } from 'src/app/globales/business-objects';

import * as moment from 'moment';
import 'moment/min/locales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';

@Component({
    selector: 'app-muevo-conductores-otros',
    templateUrl: './muevo-conductores-otros.component.html',
    styleUrls: ['./muevo-conductores-otros.component.scss']
})
export class MuevoConductoresOtrosComponent extends PageBase implements OnInit {
    
    private conductores: Conductor[];
   
    //'select',
    displayedColumns = [ 'rut', 'nombreCompleto', 'diasHabilitados', 'horarioHabilitado', 'actions' ]
    dataSource: MatTableDataSource<Conductor> = new MatTableDataSource<Conductor>();
    selection = new SelectionModel<Conductor>(true, []);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    
    days = [
        { id: 0, name: 'L', fullname: 'Lunes' },
        { id: 1, name: 'Ma', fullname: 'Martes'  },
        { id: 2, name: 'Mi', fullname: 'Míercoles'  },
        { id: 3, name: 'J', fullname: 'Jueves'  },
        { id: 4, name: 'V', fullname: 'Viernes'  },
        { id: 5, name: 'S', fullname: 'Sabado'  },
        { id: 6, name: 'D', fullname: 'Domingo'  }
    ]

    constructor(public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router, private rx:RxService, private variablesApi: VariablesGenerales,private emiter: BreadCrumbsService) { 
        super(ref, utils, router)
    }
    
    ngOnInit() {
        //this.days = this.variablesApi.home.maestros.
    }

    loadData(conductores:Conductor[]){
        this.conductores = conductores;

        console.warn(conductores);

        this.dataSource = new MatTableDataSource(this.conductores);
        this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);

    }

    containDay(conductor:Conductor, d:any){
        return conductor.diasHabilitados.indexOf(d.id) > -1;
    }

    displayHorario(item:Conductor){
        let getDesc = (hora) => {
            return hora < 12 ? 'am': 'pm'
        }

        let result = '';

        if(item.horariosHabilitados && item.horariosHabilitados.length > 0){
            result += `${item.horariosHabilitados[0]}${getDesc(item.horariosHabilitados[0])} - ${item.horariosHabilitados[item.horariosHabilitados.length - 1]}${getDesc(item.horariosHabilitados[item.horariosHabilitados.length - 1])}`;
        }
        return result;
    }

    exportCsv(){
        
        let data: TableDataCsv = {
            header:  [ 'RUT', 'NOMBRE', 'DIAS HABILITADOS', 'HORARIO' ],
            rows: this.dataSource.data.map((x:Conductor) => { 
                return [
                    x.rut,
                    x.nombreCompleto,
                    this.getDias(x.diasHabilitados),
                    this.displayHorario(x)
                ]
            })
        };

        ExportDataService.ExportCsv(this.generateFilenameFormat('restricciones_', this.variablesApi.home.cuentaSeleccionada.cuentaId.toString()), data.rows, data.header, ';', ',', 'Restricciones MUEVO EMPRESAS')
    }

    mostrarConductor(item:Conductor, event) {
        event.preventDefault();
        event.stopPropagation();
        this.emiter.emitChange({
            tipo: 'Conductor',
            action: 'GET_DATA',
            usuarioId: item.usuarioId
        });
    }

    private generateFilenameFormat(prefix:string, key:string){
        return prefix + `${key}_${moment().format('YYYYMMDD_HHmm')}`;
    }

    /** Obtiee los dias en string */
    private getDias(dias:number[]){
        let result = '';
        let index = 0;
        dias.map(x => {
            let res = this.days.filter(f => f.id === x);
            if(res.length > 0)
                result += res[0].name + ', ';
        });

        let chart_index = result.lastIndexOf(',');
        if(chart_index > -1)
            result = result.substr(0, chart_index);


        return result;
    }
    
}
