import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef, ViewChild, Input } from '@angular/core';
import { Conductor, Vehiculo, IModificarConductor } from 'src/app/models/conductores';
import { TableDataCsv } from 'src/app/globales/business-objects';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { PageBase } from '../../pageBase';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { ExportDataService } from 'src/app/servicios/export-data.service';

import * as moment from 'moment';
import 'moment/min/locales';
import { OpcionesModal } from 'src/app/globales/enumeradores';

@Component({
    selector: 'app-muevo-conductores-list',
    templateUrl: './muevo-conductores-list.component.html',
    styleUrls: ['./muevo-conductores-list.component.scss']
})
export class MuevoConductoresListComponent extends PageBase implements OnInit {
    //@Output() emitGrupo: EventEmitter<any> = new EventEmitter();
    

    private conductores: Conductor[];
   
    //'select',
    displayedColumns = [  'rut', 'nombreCompleto', 'restriccionCombustible.permiteComprarString', 'restriccionCombustible.montoMaximoDia', 'restriccionCombustible.montoMaximoMes', 'actions' ]
    dataSource: MatTableDataSource<Conductor> = new MatTableDataSource<Conductor>();
    selection = new SelectionModel<Conductor>(true, []);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    enableMasivo: boolean = false;
    
    constructor(public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router, private rx:RxService, public variablesApi: VariablesGenerales
        ,private emiter: BreadCrumbsService) { 
        super(ref, utils, router)
    }
    
    ngOnInit() {
        
    }

    loadData(conductores:Conductor[]){
        this.conductores = conductores;

        this.dataSource = new MatTableDataSource(this.conductores);
        this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);

    }

    mostrarConductor(item:Conductor, event) {
        event.preventDefault();
        event.stopPropagation();
        this.emiter.emitChange({
            tipo: 'Conductor',
            action: 'GET_DATA',
            usuarioId: item.usuarioId
        });
    }

    exportCsv(){
        
        let data: TableDataCsv = {
            header:  [ 'RUT', 'NOMBRE', 'COMPRA DE COMBUSTIBLE', 'PRESUPUESTO DIARIO', 'PRESUPUESTO MENSUAL' ],
            rows: this.dataSource.data.map((x:Conductor) => { 
                return [
                    x.rut,
                    x.nombreCompleto,
                    x.restriccionCombustible.permiteComprarString,
                    x.restriccionCombustible.montoMaximoDia,
                    x.restriccionCombustible.montoMaximoMes
                ]
            })
        };

        ExportDataService.ExportCsv(this.generateFilenameFormat('conductores_', this.variablesApi.home.cuentaSeleccionada.cuentaId.toString()), data.rows, data.header, ';', ',', 'Conductores MUEVO EMPRESAS')
    }

    toogleEnable(){
        if(this.enableMasivo){
            this.utils.msgPlainModal('Deshabilitar Todos', ['¿Está seguro que desea deshabilitar todos los conductores?']).then((result:OpcionesModal) => {
                if(result == OpcionesModal.ACEPTAR){
                    this.enableMasivo = false;
                    this.dataSource.data.forEach(c => c.estado = this.enableMasivo)
                    //actualizar
                }
            })
            
        }
        else{
            this.enableMasivo = !this.enableMasivo;
            this.dataSource.data.forEach(c => c.estado = this.enableMasivo)
            //actualizar
        }
    }

    // private modificarConductor(c:Conductor, estado:number){
    //     let m:IModificarConductor = {
    //         cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
    //         usuarioId: c.usuarioId,
    //         estado: estado,
    //         rolId: c.rolId
    //     }

    //     this.rx.conductoresModificaConductor(x)
    // }

    private generateFilenameFormat(prefix:string, key:string){
        return prefix + `${key}_${moment().format('YYYYMMDD_HHmm')}`;
    }

    private obtenerVehiculosAsignadosString(vehiculosAsignados:Vehiculo[]){
        //No se utuliza esta funcion ya que los conductores no traen los vehiuclos
        let result = '';
        vehiculosAsignados.map(x => {
            result += x.patente + ', ';
        });

        let chart_index = result.lastIndexOf(',');
        if(chart_index > -1)
            result = result.substr(0, chart_index);
        
        return result;

    }
    
}
