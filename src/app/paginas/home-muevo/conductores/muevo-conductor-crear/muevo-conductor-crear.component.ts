import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Conductor } from 'src/app/models/conductores';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-muevo-conductor-crear',
  templateUrl: './muevo-conductor-crear.component.html',
  styleUrls: ['./muevo-conductor-crear.component.css']
})
export class MuevoConductorCrearComponent implements OnInit {

  // idGrupoConductor: number = 0;
  tipoMenu: string;
  conductores: Conductor[] = [];
  valInvitacionLicencia: boolean = false;
  valInvitacionSelfie: boolean = false;
  limiteDiarioCombustible: string;
  limiteMensualCombustible: string;
  disBtnGuardar: boolean = true;

  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Invitar Conductor", class: "active", url: "" }
  ];

  interval: any;
  isMobile: boolean = false;

  constructor(private emiter: BreadCrumbsService, private maquinaHome: MaquinaHome, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private router: Router, private route: ActivatedRoute, private ref: ChangeDetectorRef) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }


  ngOnInit() {
    this.tipoMenu = this.route.snapshot.paramMap.get('origen');
    if (this.tipoMenu === 'N') {
      setTimeout(() => {
        this.emiter.emitChange({ tipo: "Configuracion", _isConfiguracion: true });
      });
    } else {
      setTimeout(() => {
        this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
      });
    }


    // this.idGrupoConductor = parseInt(this.route.snapshot.paramMap.get('id'));
    //this.conductores = this.variablesApi.home.conductores;
  }

  addMeFirst() {
    this.conductores[0].nombre = this.variablesApi.home.usuarioLogIn.usuarioNombre;
    this.conductores[0].apellido = this.variablesApi.home.usuarioLogIn.usuarioApellido;
    this.conductores[0].telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
  }

  addConductor() {
    let conductor = new Conductor();
    if (this.conductores.length > 0) {
      conductor.usuarioId = this.conductores[this.conductores.length - 1].usuarioId + 1;
    } else {
      conductor.usuarioId = 1;
    }
    let idComponente = this.conductores.findIndex(r => r.telefonoId === "");
    if (idComponente >= 0) {
      this.utils.pintaValidacion(document.getElementById("card_"), "")
    } else {
      this.conductores.forEach(element => {
        if (element.telefonoId !== "") {
          this.utils.pintaValidacion(document.getElementById("card_" + element.telefonoId), "123");
        }
      });

      if (this.isMobile) {
        this.conductores.unshift(conductor)
      } else {
        this.conductores.push(conductor);
      }
      this.disBtnGuardar = false;
    }
  }

  removeConductor(id) {
    let idComponente = this.conductores.findIndex(r => r.usuarioId === id);
    this.conductores.splice(idComponente, 1);
    if (this.conductores.length == 0) {
      this.disBtnGuardar = true;
    }
  }

  uploadExcel() {
    this.utils.openModal(Paginas.ModalSubirArchivoComponent).then(response => {
      let lastIndex = this.conductores.length - 1;
      if (this.conductores[lastIndex].telefonoId === "") {
        this.removeConductor("");
      }
      var range = XLSX.utils.decode_range(response['!ref']);
      for (let rowNum = 6; rowNum <= range.e.r; rowNum++) {
        let conductor = new Conductor();
        // Example: Get second cell in each row, i.e. Column "B"
        conductor.nombre = response[XLSX.utils.encode_cell({ r: rowNum, c: 0 })].v;
        conductor.apellido = response[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v;
        conductor.telefonoId = response[XLSX.utils.encode_cell({ r: rowNum, c: 2 })].v;
        // conductor.grupoId = this.idGrupoConductor;
        this.conductores.push(conductor);
      }
    });
  }

  homeUsuarioInvitar() {
    let datos = [];

    this.conductores.forEach(element => {
      let data = {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        rolId: 4,
        usuarioCuentaConduce: element.conduce,
        invitacionTelefonoId: "569" + element.telefonoId.replace(/\+/g, ""),
        invitacionNombre: element.nombre,
        invitacionApellido: element.apellido,
        estado: 1,
        montoMaximoDiaValor: ("" + element.restriccionCombustible.montoMaximoDia).replace(/\./g, ""),
        montoMaximoMesValor: ("" + element.restriccionCombustible.montoMaximoMes).replace(/\./g, "")

      }
      if (data.invitacionTelefonoId !== "" && element.telefonoId.length == 8 && data.invitacionNombre !== "" && data.invitacionApellido !== "" && (data.montoMaximoDiaValor !== "undefined" && data.montoMaximoDiaValor !== "") && (data.montoMaximoMesValor !== "undefined" && data.montoMaximoMesValor !== "")) {
        // let maximoDiario = parseInt(("" + element.limiteDiarioCombustible).replace(/\./g, ""));
        // let maximoMensual = parseInt(("" + element.limiteMensualCombustible).replace(/\./g, ""));
        // if (maximoMensual > maximoDiario) {
        datos.push(data);
        this.setBorderOk(element.telefonoId.replace(/\+/g, ""));
        // } else {
        //   this.setBorderError(element.telefonoConductor.replace(/\+/g, ""));
        // }
      } else {
        this.setBorderError(element.telefonoId.replace(/\+/g, ""));
      }
    });

    if (datos.length < 1) {
      // this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "No hay Datos", textoMensaje: "Debe Ingresar un Conductor valido, el Nombre, Apellidos, Numero de telefono y Restricciones son Obligatorias" });
    } else if (datos.length < this.conductores.length) {
      // this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "", textoMensaje: "No se pueden cargar todos los Conductores, el Nombre, Apellidos, Numero de telefono y Restricciones son Obligatorias, recuerde que el monto diario no puede ser mayor que el monto mensual" });
    } else {

      this.conductores.forEach(element => {
        element.telefonoId = "569" + element.telefonoId.replace(/\+/g, "");
      });
      this.variablesApi.home.conductoresInvitados = this.conductores;

      this.utils.showSpinner("Interno");
      let actualUrl = this.router.url.substr(1);
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_CLICK,
        datos: datos
      }

      this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          } else {
            this.ngOnInit();
          }
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            console.log(response);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.utils.msgPlain(respuesta.mensajeSalida);
        }
      });
    }
  }

  chInvitacionSelfie() {
    // this.valInvitacionSelfie = !this.valInvitacionSelfie;
    // for (let index = 0; index < this.conductores.length; index++) {
    //   this.conductores[index].valInvitacionSelfie = this.valInvitacionSelfie;
    // }
  }

  chInvitacionLicencia() {
    // this.valInvitacionLicencia = !this.valInvitacionLicencia;
    // for (let index = 0; index < this.conductores.length; index++) {
    //   this.conductores[index].valInvitacionLicencia = this.valInvitacionLicencia;
    // }
  }

  validaInput(ingreso: any) {
    let item = ingreso.srcElement;
    item.value = this.utils.formatMaxInput(item.value, item.maxLength);
    item.value = this.utils.formatNumbers(item.value);
    item.value = this.currency(item.value);
  }

  maxInput(ingreso: any) {
    let item = ingreso.srcElement;
    item.value = this.utils.formatMaxInput(item.value, item.maxLength);
  }

  onlyNumber(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = this.utils.formatNumbers(item.value);
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

  setBorderError(telefono) {
    if (telefono !== "") {
      let cond = this.conductores.filter(r => r.telefonoId = telefono)[0];
      // let maximoDiario = parseInt(("" + cond.limiteDiarioCombustible).replace(/\./g, ""));
      // let maximoMensual = parseInt(("" + cond.limiteMensualCombustible).replace(/\./g, ""));


      this.utils.pintaValidacion(document.getElementById("nombre_" + telefono), cond.nombre);
      this.utils.pintaValidacion(document.getElementById("apellido_" + telefono), cond.apellido);
      if (telefono.length == 8) {
        this.utils.pintaValidacion(document.getElementById("telefono_" + telefono), telefono);
      } else {
        this.utils.pintaValidacion(document.getElementById("telefono_" + telefono), "");
      }
      // if (maximoMensual > maximoDiario) {
      if (typeof cond.restriccionCombustible.montoMaximoDia == 'undefined') {
        this.utils.pintaValidacion(document.getElementById("limiteDiario_" + telefono), "");
      } else {
        this.utils.pintaValidacion(document.getElementById("limiteDiario_" + telefono), "" + cond.restriccionCombustible.montoMaximoDia);
      }

      if (typeof cond.restriccionCombustible.montoMaximoMes == 'undefined') {
        this.utils.pintaValidacion(document.getElementById("limiteMensual_" + telefono), "");
      } else {
        this.utils.pintaValidacion(document.getElementById("limiteMensual_" + telefono), "" + cond.restriccionCombustible.montoMaximoMes);
      }
      // } else {
      //   this.utils.pintaValidacion(document.getElementById("limiteDiario_" + telefono), "");
      //   this.utils.pintaValidacion(document.getElementById("limiteMensual_" + telefono), "");
      // }
    } else {
      let cond = this.conductores.filter(r => r.telefonoId == telefono)[0];


      this.utils.pintaValidacion(document.getElementById("nombre_" + telefono), cond.nombre);
      this.utils.pintaValidacion(document.getElementById("apellido_" + telefono), cond.apellido);
      this.utils.pintaValidacion(document.getElementById("telefono_" + telefono), telefono);
      if (typeof cond.restriccionCombustible.montoMaximoDia == 'undefined') {
        this.utils.pintaValidacion(document.getElementById("limiteDiario_" + telefono), "");
      } else {
        this.utils.pintaValidacion(document.getElementById("limiteDiario_" + telefono), "" + cond.restriccionCombustible.montoMaximoDia);
      }

      if (typeof cond.restriccionCombustible.montoMaximoMes == 'undefined') {
        this.utils.pintaValidacion(document.getElementById("limiteMensual_" + telefono), "");
      } else {
        this.utils.pintaValidacion(document.getElementById("limiteMensual_" + telefono), "" + cond.restriccionCombustible.montoMaximoMes);
      }
    }

  }

  setBorderOk(telefono) {
    let cond = this.conductores.filter(r => r.telefonoId = telefono)[0];
    this.utils.pintaValidacion(document.getElementById("card_" + telefono), telefono)

    this.utils.pintaValidacion(document.getElementById("nombre_" + telefono), cond.nombre);
    this.utils.pintaValidacion(document.getElementById("apellido_" + telefono), cond.apellido);
    this.utils.pintaValidacion(document.getElementById("telefono_" + telefono), telefono);
    this.utils.pintaValidacion(document.getElementById("limiteDiario_" + telefono), "" + cond.restriccionCombustible.montoMaximoDia);
    this.utils.pintaValidacion(document.getElementById("limiteMensual_" + telefono), "" + cond.restriccionCombustible.montoMaximoMes);
  }

  editLimiteDiarioGeneral() {
    for (let index = 0; index < this.conductores.length; index++) {
      this.conductores[index].restriccionCombustible.montoMaximoDia = parseInt(this.limiteDiarioCombustible.replace(/\./g, ""));
    }
  }

  editLimiteMensualGeneral() {
    for (let index = 0; index < this.conductores.length; index++) {
      this.conductores[index].restriccionCombustible.montoMaximoMes = parseInt(this.limiteMensualCombustible.replace(/\./g, ""));
    }
  }

}
