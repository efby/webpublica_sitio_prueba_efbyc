import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoConductorCrearComponent } from './muevo-conductor-crear.component';

describe('MuevoConductorCrearComponent', () => {
  let component: MuevoConductorCrearComponent;
  let fixture: ComponentFixture<MuevoConductorCrearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoConductorCrearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoConductorCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
