import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BreadCrumbsService, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { Conductor } from 'src/app/models/conductores';
import { Invitacion, IPagination } from 'src/app/globales/business-objects';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RxService } from 'src/app/servicios/rx.service';
import { PageBase } from '../../pageBase';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MuevoConductoresListComponent } from '../muevo-conductores-list/muevo-conductores-list.component';
import { MuevoConductoresOtrosComponent } from '../muevo-conductores-otros/muevo-conductores-otros.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MuevoConductoresGeneralesComponent } from '../muevo-conductores-generales/muevo-conductores-generales.component';

@Component({
    selector: 'app-muevo-conductores-root',
    templateUrl: './muevo-conductores-root.component.html',
    styleUrls: ['./muevo-conductores-root.component.scss']
})
export class MuevoConductoresRootComponent extends PageBase implements OnInit {

    @ViewChild(MuevoConductoresGeneralesComponent) conductoresGeneralesComponent: MuevoConductoresGeneralesComponent;
    @ViewChild(MuevoConductoresListComponent) conductoresListComponent: MuevoConductoresListComponent;
    @ViewChild(MuevoConductoresOtrosComponent) conductoresOtrosComponent: MuevoConductoresOtrosComponent;
    
    navegacion = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Mis Conductores", class: "active", url: "" }
    ];

    private unsubscribe = new Subject();
    callbackSubscripcion:any;
    
    ui: {
        mostrarInvitados: boolean,
        mostrarBotonIrInvitaciones: boolean,
        switchData: string
    }

    conductores: Conductor[];
    invitados?: Invitacion[];

    pagination:IPagination;
    
    constructor(public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router, private rx: RxService, private emiter: BreadCrumbsService, public variablesApi: VariablesGenerales) {
        super(ref, utils, router);

        this.emiter.emitChange({ migajas: this.navegacion, muestraBreadcrumbs: true });
     
        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);


        this.ui = {
            mostrarInvitados: false,
            mostrarBotonIrInvitaciones: false,
            switchData: 'generales'
        }

        this.pagination = {
            currentPage: 0,
            pages: 0,
            totalRows: 0, 
            positionFinal: 0,
            pagesSize: 0,
            rows: 0
        }

        this.invitados = [];
        this.conductores = [];
    }
    
    ngOnInit() {
        this.obtenerConductores();
    }

    mapearDatosRestriccionesConductor(result){
        this.conductores.map(c => {
            let restriccionesConductor = result.data.restricciones.filter(r => r.usuarioId === c.usuarioId);

            const permiteComprar:any[] = restriccionesConductor.filter(r => r.tipoRestriccionId === 7);
            const montoMaximoDia:any[] = restriccionesConductor.filter(r => r.tipoRestriccionId === 1);
            const montoMaximoMes:any[] = restriccionesConductor.filter(r => r.tipoRestriccionId === 2);

            c.restriccionCombustible = {
                permiteComprar: permiteComprar.length > 0 ? permiteComprar[0].restriccionValor == 1 : false,

                montoMaximoDia: montoMaximoDia.length > 0 && montoMaximoDia[0].restriccionValor.length > 0 ? montoMaximoDia[0].restriccionValor[0] : null,
                montoMaximoMes: montoMaximoMes.length > 0 && montoMaximoMes[0].restriccionValor.length > 0 ? montoMaximoMes[0].restriccionValor[0] : null,
                permiteComprarString: '',
                
                //no cargamos esta info por que no la tenemos
                traeMontosDisponibles: false,
                restriccionAcumuladoDia: null,
                restriccionAcumuladoMes: null,
                montoDisponibleDia: null,
                montoDisponibleMes: null
            }

            c.restriccionCombustible.permiteComprarString = c.restriccionCombustible.permiteComprar ? 'Puede' : 'No puede';
        })
    }

    obtenerConductores(){
        const cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        
        this._isLoading = true;
        this.rx.conductoresGet(cuentaId, this.pagination.positionFinal).then((result:any) => {
            console.log(result.data);
            this.ui.mostrarBotonIrInvitaciones = result.data.tieneInvitaciones;

            this.conductores = this.conductores.concat(result.data.conductores);

            this.conductoresGeneralesComponent.loadData(this.conductores);
            this.conductoresListComponent.loadData(this.conductores);
            this.conductoresOtrosComponent.loadData(this.conductores);

            //this.obtenerRestricciones();
            this.paginationHandler(result.data.pagination);
            this._isLoading = false;

        }, this.errorCallback);
    }

    abrirInvitaciones(){
        this.emiter.emitChange({
            tipo: 'InvitacionesConductores',
        });
    }

    toggleSwitchData(tabName:string){
        this.ui.switchData = tabName;
    }

    next(){
        this.obtenerConductores();
    }

    private emitterHandler = (data:IEmmitCallbackModel) => {
        if(data.from == Paginas.HomeMuevoMisConductores){
            this.pagination.positionFinal = 0;
            this.conductores = [];
            this.obtenerConductores();
        }
    }

    private paginationHandler(configResponse:any){

        this.pagination.positionFinal = configResponse.posicionFinal;
        this.pagination.totalRows = configResponse.totalRegistros;
        this.pagination.pagesSize = configResponse.registrosPorPagina;
        this.pagination.rows = configResponse.registrosObtenidos;
    }

    ngOnDestroy() {
        if(this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }
    
}
