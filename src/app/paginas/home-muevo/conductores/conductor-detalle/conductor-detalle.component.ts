import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Conductor, IModificarConductor } from "src/app/models/conductores";
import { PageBase } from "../../pageBase";
import { UtilitariosService } from "src/app/servicios/utilitarios.service";
import { Router } from "@angular/router";
import { RequestTipo } from "src/app/models/respuestas-tipo";
import { VariablesGenerales } from "src/app/globales/variables-generales";
import { RxService } from "src/app/servicios/rx.service";
import {
  BreadCrumbsService,
  IEmmitModel,
  IEmmitCallbackModel,
} from "src/app/servicios/bread-crumbs.service";
import { Paginas } from "src/app/globales/paginas";
import { OpcionesModal } from "src/app/globales/enumeradores";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-conductor-detalle",
  templateUrl: "./conductor-detalle.component.html",
  styleUrls: ["./conductor-detalle.component.scss"],
})
export class ConductorDetalleComponent extends PageBase implements OnInit {
  CURRENT_PAGE = Paginas.ModalConductorDetalle;
  current: Conductor;

  unsubscribe = new Subject();
  callbackSubscripcion: any;

  constructor(
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    public router: Router,
    public variablesApi: VariablesGenerales,
    private rx: RxService,
    private emiter: BreadCrumbsService
  ) {
    super(ref, utils, router);

    this.current = new Conductor();
  }

  ngOnInit() {
    this.callbackSubscripcion = this.emiter.callbackEmmited$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(this.emitterHandler);
  }

  setData(idUsuario: number) {
    this.getConductor(idUsuario);
  }

  eliminar(item: Conductor) {
    this.utils
      .msgPlainModal("Eliminar Conductor", [
        "¿Está seguro que desea eliminar este conductor?",
      ])
      .then((result) => {
        if (result == OpcionesModal.ACEPTAR)
          this.maquinaModificarConductor(2, item);
      });
  }

  mostrarRestriccionesConductor() {
    event.preventDefault();
    event.stopPropagation();

    let config: IEmmitModel = {
      tipo: "Restricciones Conductor",
      action: "OPEN",
      data: this.current,
    };

    this.emiter.emmitChange(config);
  }

  mostrarAjustesCombustible() {
    event.preventDefault();
    event.stopPropagation();

    let conductor_readonly = new Conductor();
    Object.assign(conductor_readonly, this.current);

    let config: IEmmitModel = {
      tipo: "Conductor Gasto DiarioMensual",
      action: "OPEN",
      data: conductor_readonly,
    };

    this.emiter.emmitChange(config);
  }

  private maquinaModificarConductor(estado: number, item: Conductor) {
    let model: IModificarConductor = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
      usuarioId: item.usuarioId,
      estado: estado,
      rolId: item.rolId,
    };

    this.utils.showSpinner();
    this.rx.conductoresModificaConductor(model).then((result: any) => {
      this.utils.hideSpinner();
      this.emiter.emmitChange({
        tipo: "Conductor",
        action: "CLOSE MODAL CONDUCTOR",
      });

      this.actualizarListadoConductores();
    }, this.errorCallback);
  }

  private actualizarListadoConductores() {
    this.emiter.emitCallback({
      from: Paginas.HomeMuevoMisConductores,
      action: "refresh",
    });
  }

  private getConductor(idUsuario: number) {
    this.utils.showSpinner("Interno");
    let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

    this.rx.conductoresGetConductor(idUsuario, cuentaId).then((result: any) => {
      this.current = result.data.conductor;
      this.current.rolNombre = this.variablesApi.home.maestros.roles.filter(
        (x) => x.rolId == this.current.rolId
      )[0].rolNombre;
      this.mostrarConductor();
      this.utils.hideSpinner("Interno");
    }, this.errorCallback);
  }

  private mostrarConductor() {
    event.preventDefault();
    event.stopPropagation();
    this.emiter.emitChange({
      tipo: "Conductor",
      action: "OPEN",
    });
  }

  mostrarVehiculosAsignados(conductor: Conductor) {
    let model: IEmmitModel = {
      tipo: "Vehiculos Asignados",
      action: "OPEN",
      data: {
        origin: Paginas.ModalConductorDetalle,
        conductor: conductor,
        conductorKey: conductor.usuarioId,
      },
    };
    this.emiter.emmitChange(model);
  }

  mostrarEstacionesMap() {
    event.preventDefault();
    event.stopPropagation();
    this.emiter.emitChange({
      tipo: "Conductor Estaciones",
      action: "OPEN",
    });
  }

  activaDesactiva(id: number) {
    let value = this.current.estado;
    this.maquinaModificarConductor(value ? 0 : 1, this.current);
  }

  private emitterHandler = (data: IEmmitCallbackModel) => {
    if (
      data.from == Paginas.ModalVehiculosAsignados &&
      data.action == this.CURRENT_PAGE
    ) {
      this.current.vehiculosAsignados = data.data.vehiculosAsignados;
      this.actualizarListadoConductores();
    }
  };

  ngOnDestroy() {
    if (
      this.callbackSubscripcion != undefined &&
      !this.callbackSubscripcion.closed
    )
      this.callbackSubscripcion.unsubscribe();
  }
}
