import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Invitacion } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-muevo-mis-invitaciones',
  templateUrl: './muevo-mis-invitaciones.component.html',
  styleUrls: ['./muevo-mis-invitaciones.component.css']
})
export class MuevoMisInvitacionesComponent implements OnInit {

  invitados?: Invitacion[];

  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Mis Conductores", class: "", url: "/" + Paginas.HomeMuevoMisConductores },
    { title: "Mis Invitaciones", class: "active", url: "" }
  ];

  interval: any;
  isMobile: boolean = false;
  idCaller: string = "";

  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private router: Router, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private ref: ChangeDetectorRef, private route: ActivatedRoute) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    this.idCaller = this.route.snapshot.paramMap.get('idCaller');
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
    this.cargaInvitaciones();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
  }

  cargaFecha() {
    this.variablesApi.home.invitados.forEach(element => {
      element.invitacionFechaCaducidad = this.date2Human(element.invitacionFechaCaducidad);
    });
  }

  iniciaVariablesGlobales() {
    this.cargaFecha();
    if (this.idCaller == "C") {
      this.invitados = this.variablesApi.home.invitados.filter(r => r.invitacionRolId === 4 && r.invitacionEstadoInvitado !== "ACEPTADA" && r.invitacionEstadoInvitado !== "NO_AUTORIZADA" && r.invitacionEstadoInvitado !== "INVALIDA" && r.invitacionEstadoInvitado !== "CANCELADA");
    } else {
      this.invitados = this.variablesApi.home.invitados.filter(r => r.invitacionEstadoInvitado !== "ACEPTADA" && r.invitacionEstadoInvitado !== "NO_AUTORIZADA" && r.invitacionEstadoInvitado !== "INVALIDA" && r.invitacionEstadoInvitado !== "CANCELADA");
    }
  }

  cargaInvitaciones() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarUsuarios(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //this.ngOnInit();
        // }
        this.iniciaVariablesGlobales();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  date2Human(date) {
    let format = 'dd/MM/yyyy HH:mm';
    return this.utils.date2Human(date, format, this.router.url);
  }

  cancelarInvitacion(invitacionId: number, invitacionAceptada: number) {
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        invitacionId: invitacionId
      }
    }

    this.maquinaHome.homeUsuarioCancelaInvitacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "", textoMensaje: "Se ha cancelado la Invitacion" });
        this.cargaInvitaciones();
        // }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  reenviarInvitacion(invitacionId: number, autorizacion: number) {
    let datos = [];
    let invitado = this.invitados.filter(r => r.invitacionId == invitacionId)[0];

    let actualUrl = this.router.url.substr(1);
    let data = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
      rolId: 4,
      usuarioCuentaConduce: 1,
      invitacionTelefonoId: invitado.invitacionTelefonoInvitadoId,
      invitacionNombre: invitado.invitacionValidaciones.invitacionNombre,
      invitacionApellido: invitado.invitacionValidaciones.invitacionApellido,
      estado: 1,
      montoMaximoDiaValor: invitado.combustibleMontoDiario,
      montoMaximoMesValor: invitado.combustibleMontoMensual
    }
    datos.push(data);

    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: datos
    }

    this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "", textoMensaje: "Se ha reenviado la Invitación" });
        this.cargaInvitaciones();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  getRolText(rolId) {
    return this.variablesApi.home.maestros.roles.filter(r => r.rolId === rolId)[0].rolNombre;
  }

}
