import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoMisInvitacionesComponent } from './muevo-mis-invitaciones.component';

describe('MuevoMisInvitacionesComponent', () => {
  let component: MuevoMisInvitacionesComponent;
  let fixture: ComponentFixture<MuevoMisInvitacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoMisInvitacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoMisInvitacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
