import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Conductor } from 'src/app/models/conductores';
import { Invitacion } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-muevo-conductor-confirmar',
  templateUrl: './muevo-conductor-confirmar.component.html',
  styleUrls: ['./muevo-conductor-confirmar.component.css']
})
export class MuevoConductorConfirmarComponent implements OnInit {

  invitados?: Conductor[];
  invitadosInter?: Invitacion[];

  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Invitar Conductores", class: "", url: "/" + Paginas.HomeMuevoCrearConductor },
    { title: "Envio Exitoso", class: "active", url: "" }
  ];

  interval: any;
  isMobile: boolean = false;

  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private router: Router, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private route: ActivatedRoute, private ref: ChangeDetectorRef) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.cargaInvitaciones();
    this.cargaFecha();
    this.invitados = this.variablesApi.home.conductoresInvitados;
  }

  cargaFecha() {
    // this.variablesApi.home.conductoresInvitados.forEach(element => {
    //   element.fechaString = this.date2Human(element.fecha);
    // });

  }

  cargaInvitaciones() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarUsuarios(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");

        this.invitadosInter = this.variablesApi.home.invitados.filter(r => r.invitacionRolId === 4 && r.invitacionEstadoInvitado === "CREADA");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  date2Human(date) {
    let format = 'dd/MM/yyyy HH:mm';
    return this.utils.date2Human(date, format, this.router.url);
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

  cancelarInvitacion(invitacionTelefonoId: string, invitacionAceptada: number) {
    let invitacionId = this.invitadosInter.filter(r => r.invitacionTelefonoInvitadoId == invitacionTelefonoId)[0].invitacionId;
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        invitacionId: invitacionId
      }
    }

    this.maquinaHome.homeUsuarioCancelaInvitacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        this.variablesApi.home.conductoresInvitados.splice(this.variablesApi.home.conductoresInvitados.findIndex(r => invitacionTelefonoId == invitacionTelefonoId), 1);
        this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "", textoMensaje: "Se ha cancelado la Invitacion" });
        this.ngOnInit();
        // }
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
          this.utils.hideSpinner("Interno");
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

}
