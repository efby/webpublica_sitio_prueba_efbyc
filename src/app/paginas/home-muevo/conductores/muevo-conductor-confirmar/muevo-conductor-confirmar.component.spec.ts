import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoConductorConfirmarComponent } from './muevo-conductor-confirmar.component';

describe('MuevoConductorConfirmarComponent', () => {
  let component: MuevoConductorConfirmarComponent;
  let fixture: ComponentFixture<MuevoConductorConfirmarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoConductorConfirmarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoConductorConfirmarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
