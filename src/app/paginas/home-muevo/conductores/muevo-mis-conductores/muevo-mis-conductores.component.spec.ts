import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoMisConductoresComponent } from './muevo-mis-conductores.component';

describe('MuevoMisConductoresComponent', () => {
  let component: MuevoMisConductoresComponent;
  let fixture: ComponentFixture<MuevoMisConductoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoMisConductoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoMisConductoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
