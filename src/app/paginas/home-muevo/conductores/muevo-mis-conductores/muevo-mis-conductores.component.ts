import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef, QueryList, ViewChildren, ViewChild } from '@angular/core';
import { Conductor } from 'src/app/models/conductores';
import { Grupos, VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { MatTableDataSource, MatPaginator, MatSort, MatTable, Sort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { PageBase } from '../../pageBase';
import { fadeOutToRight } from 'src/app/animations/fade-right';
import { HomeService } from 'src/app/servicios/home/home.service';
import { RxService } from 'src/app/servicios/rx.service';
import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
import { Invitacion } from 'src/app/globales/business-objects';

@Component({
    selector: 'app-muevo-mis-conductores',
    templateUrl: './muevo-mis-conductores.component.html',
    styleUrls: ['./muevo-mis-conductores.component.scss'],
    animations: [fadeOutToRight], // make fade in animation available to this component                
    host: { '[@fadeOutToRight]': '' } 
})
export class MuevoMisConductoresComponent extends PageBase implements OnInit {
    @Output() emitGrupo: EventEmitter<any> = new EventEmitter();
    conductores: Conductor[];
    invitados?: Invitacion[];

    ui: {
        mostrarInvitados: boolean,
        mostrarBotonIrInvitaciones: boolean,
        switchData: string
    }

    grupos: Grupos[];
    _invitadosPendientes: boolean = false;
    _navInvitados: boolean = false;
    limiteDiarioCombustible: string;
    limiteMensualCombustible: string;
    
    searchInConductor: string = "";
    interval: any;
    isMobile: boolean = false;
    filter: boolean = false;
    ESTADO_ELIMINADO: number = 2;

    displayedColumnsConductores = [ 'select', 'rutConductor', 'nombreConductor', 'puedeComprarString', 'limiteDiarioCombustible', 'limiteMensualCombustible', 'vehiculosAsignados', 'estadoConductor' ]
    dataSource: MatTableDataSource<Conductor> = new MatTableDataSource<Conductor>();
    selection = new SelectionModel<Conductor>(true, []);

    migajas = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Mis Conductores", class: "active", url: "" }
    ];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    
    constructor(private rx: RxService, private variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, public router: Router, public utils: UtilitariosService, private maquinaHome: MaquinaHome, public ref: ChangeDetectorRef) {
        super(ref, utils, router);

        this.invitados = []; 
        this.ui = {
            mostrarInvitados: false,
            mostrarBotonIrInvitaciones: false,
            switchData: 'combustible'
        }

        //comentados crojas
        // this.router.routeReuseStrategy.shouldReuseRoute = function () {
        //     return false;
        // };
        
        setTimeout(() => {
            this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
        });
        //TODO: Cargar las invitaciones
        //this.cargaInvitaciones();
    }
    
    ngOnDestroy() {
        clearInterval(this.interval);
    }
    
    ngOnInit() {
        this.obtenerConductores();
        
        this.iniciaVariablesGlobales();
    }

    obtenerConductores(){
        // this.rx.conductoresGet(this.variablesApi.home.cuentaSeleccionada.cuentaId).then((result:any) => {
        //     console.log(result.data.conductores);
        //     this.conductores = result.data.conductores.map(c => {
        //         //TODO: añadir informacion desde api (pendiente)
        //         c.puedeComprar = true;
        //         c.puedeComprarString = 'Puede';

        //         c.vehiculosAsignados = ['VB0310', 'BTDZ32', 'CF5697'];
        //         return c;
        //     });
        //     this.invitados = result.data.invitados;

        //     this.dataSource = new MatTableDataSource(this.conductores);
        //     this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);


        //     this.ui.mostrarInvitados = this.invitados.length > 0;
        //     this.ui.mostrarBotonIrInvitaciones = this.invitados.length > 3;

        //     if(this.ui.mostrarInvitados)
        //         this.invitados = this.invitados.splice(0, 2);

        // }, this.errorCallback);

        // let idComponente = this.variablesApi.home.cuentas.findIndex(r => r.cuentaDefault === true);
        // this.utils.showSpinner("Interno");
        // let action: RequestTipo = {
        //     accion: AccionTipo.ACCION_CLICK,
        //     datos: {
        //         cuentaId: this.variablesApi.home.cuentas[idComponente].cuentaId
        //     }
        // }
        
        // this.maquinaHome.homeUsuarioConsultarUsuarios(action).then(respuesta => {
        //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        //         this.utils.hideSpinner("Interno");
        //         this.iniciaVariablesGlobales();
        //     } 
        //     else
        //         this.serviceCallback(respuesta);
        // });
    }

    toggleSwitchData(tabName:string){
        this.ui.switchData = tabName;
    }

    iniciaVariablesGlobales() {
        this.isMobile = this.variablesApi.esMobile;
        //crojas this.modCurrency();
        //this.conductores = this.variablesApi.home.conductores;
 
        //this.grupos = this.variablesApi.home.grupos;
        // this.invitados = this.variablesApi.home.invitados.filter(r => r.invitacionRolId === 4 && r.invitacionEstadoInvitado !== "ACEPTADA" && r.invitacionEstadoInvitado !== "NO_AUTORIZADA" && r.invitacionEstadoInvitado !== "INVALIDA" && r.invitacionEstadoInvitado !== "CANCELADA");
        // if (this.invitados.length > 0) {
        //     this._invitadosPendientes = true;
        //     if (this.invitados.length > 3) {
        //         this._navInvitados = true;
        //         this.invitados = this.invitados.splice(0, 2);
        //     } else {
        //         this._navInvitados = false;
        //     }
        // } else {
        //     this._invitadosPendientes = false;
        //     this._navInvitados = false;
        // }
        // this.cargaConductoresGrupos();
    }
    
    modCurrency() {
        this.variablesApi.home.usuarios.forEach(element => {
            element.restriccionCombustible.montoMaximoDia = element.restriccionCombustible.montoMaximoDia;
            element.restriccionCombustible.montoMaximoMes = element.restriccionCombustible.montoMaximoMes;
        });
    }
    
    date2Human(date) {
        let format = 'dd/MM/yyyy';
        return this.utils.date2Human(date, format, this.router.url);
    }
    
    // cargaInvitaciones() {
    //     let idComponente = this.variablesApi.home.cuentas.findIndex(r => r.cuentaDefault === true);
    //     this.utils.showSpinner("Interno");
    //     let action: RequestTipo = {
    //         accion: AccionTipo.ACCION_CLICK,
    //         datos: {
    //             cuentaId: this.variablesApi.home.cuentas[idComponente].cuentaId
    //         }
    //     }
        
    //     this.maquinaHome.homeUsuarioConsultarUsuarios(action).then(respuesta => {
    //         if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
    //             this.utils.hideSpinner("Interno");
    //             this.iniciaVariablesGlobales();
    //         } 
    //         this.serviceCallback(respuesta);
    //     });
    // }

    openAdjustment(){
        event.preventDefault();
        event.stopPropagation();
        this.emiter.emitChange({
            tipo: 'AjusteDiarioMensual',
            conductores: this.selection.selected
        });
    }
    
    cargaConductoresGrupos() {
        // this.grupos.forEach(gr => {
        //     gr.conductoresGrupo = this.conductores.filter(r => r.grupoId === gr.grupoId);
        //     gr.cantidadConductores = gr.conductoresGrupo.length;
        // });
        // this.conductores = this.conductores.filter(r => r.grupoId === null);// && r.usuarioCuentaEstadoId != ESTADO_ELIMINADO );

        // this.dataSource = new MatTableDataSource(this.conductores);
        // this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);
        
        // // let elements = document.getElementsByClassName('cdk-visually-hidden')
        // // Array.from(elements).forEach(function (element) { 
        // //     element['hidden'] = true;
        // // }); 
    }

    
    
    activarEdicion(id) {
        let idComponente = this.grupos.findIndex(r => r.grupoId === id);
        for (let index = 0; index < this.grupos.length; index++) {
            this.grupos[index].editable = false;
        }
        this.grupos[idComponente].editable = true;
    }
    
    editarConductor(id) {
        let idComponente = this.conductores.findIndex(r => r.usuarioId === id);
        //this.conductores[idComponente].editable = true;
    }
    
    activaDesactiva(id: number) {
        let conductor = this.conductores.filter(r => r.usuarioId === id)[0];
        if (conductor.estado) {
            this.maquinaModificarConductor(id, 0);
        } else {
            this.maquinaModificarConductor(id, 1);
        }
    }
    
    // editLimiteDiarioGeneral =  (popover: any) => {
    //     if(this.selection.selected.length == 0){
    //         this.utils.msgPlainModalAccept('error', '', ['Debe seleccionar al menos un conductor para aplicar el límite de gastos diarios']).then((result: OpcionesModal) => {});
    //         return;
    //     }
    //     let modifieds = 0;
    //     for (let index = 0; index < this.selection.selected.length; index++) {
    //         let i = this.utils.arrayIndexOf(this.conductores, 'idConductor', this.selection.selected[index].idConductor);
    //         if(i >= 0){
    //             this.conductores[i].limiteDiarioCombustible = parseInt(this.limiteDiarioCombustible.replace(/\./g, ""));
    //             modifieds++;
    //         }
    //     }

    //     if(modifieds > 0)
    //         this.modificarMasivoConductor();
    //     popover.close();
    // }

        // let confirma: boolean = false;
        // confirma = this.utils.msgPlain({ tipoMensaje: "confirm", tituloMensaje: "Confirmacion", textoMensaje: "Esta accion Cambiara el limite Diario para todos los usuarios, \n ¿Esta seguro de querer hacer este cambio?" })
        
        // if (confirma) {
        //     for (let index = 0; index < this.conductores.length; index++) {
        //         this.conductores[index].limiteDiarioCombustible = parseInt(this.limiteDiarioCombustible.replace(/\./g, ""));
        //     }
        //     this.modificarMasivoConductor();
        // }
        // document.getElementById("idCondu").click();
    
    
    // editLimiteMensualGeneral(popover:any) {
    //     if(this.selection.selected.length == 0){
    //         this.utils.msgPlainModalAccept('error', '', ['Debe seleccionar al menos un conductor para aplicar el límite de gastos mensual']).then((result: OpcionesModal) => {});
    //         return;
    //     }
    //     let modifieds = 0;
    //     for (let index = 0; index < this.selection.selected.length; index++) {
    //         let i = this.utils.arrayIndexOf(this.conductores, 'idConductor', this.selection.selected[index].idConductor);
    //         if(i >= 0){
    //             this.conductores[i].limiteMensualCombustible = parseInt(this.limiteMensualCombustible.replace(/\./g, ""));
    //             modifieds++;
    //         }
    //     }

    //     if(modifieds > 0)
    //         this.modificarMasivoConductor();
    //     popover.close();

    //     // let confirma: boolean = false;
    //     // confirma = this.utils.msgPlain({ tipoMensaje: "confirm", tituloMensaje: "Confirmacion", textoMensaje: "Esta accion Cambiara el limite Mensual para todos los usuarios, \n ¿Esta seguro de querer hacer este cambio?" })
        
    //     // if (confirma) {
    //     //     for (let index = 0; index < this.conductores.length; index++) {
    //     //         this.conductores[index].limiteMensualCombustible = parseInt(this.limiteMensualCombustible.replace(/\./g, ""));
    //     //     }
    //     //     this.modificarMasivoConductor();
    //     // }
    //     // document.getElementById("idCondu").click();
    // }
    
    modificarMasivoConductor() {
        let actualUrl = this.router.url.substr(1);
        this.utils.showSpinner("Interno");
        let data = [];
        this.conductores.forEach(conductor => {
            let datos = {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                usuarioId: conductor.usuarioId,
                //grupoCuentaId: (conductor.grupoId == null) ? 0 : conductor.grupoId,
                usuarioCuentaEstadoId: conductor.estado ? 1 : 0,
                rolId: conductor.rolId,
                usuarioCuentaConduce: conductor.conduce,
                //usuarioCuentaRindeGasto: conductor.usuarioCuentaRindeGasto,
                //usuarioCuentaCreaVehiculo: conductor.usuarioCuentaCreaVehiculo,
                //usuarioCuentaEligeDTE: conductor.usuarioCuentaEligeDTE,
                //usuarioCuentaSeleccionado: conductor.usuarioCuentaSeleccionado,
                vehiculosAsignados: [],
                estadoCombustible: 1,
                montoMaximoDiaValor: conductor.restriccionCombustible.montoMaximoDia,
                montoMaximoMesValor: conductor.restriccionCombustible.montoMaximoMes
            }
            data.push(datos);
        });
        
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: data
        }
        
        this.maquinaHome.homeUsuarioModificarMasivo(action).then(this.callApiCallbackHandler);
        
    }


    mostrarConductor(id, event) {
        event.preventDefault();
        event.stopPropagation();
        this.emiter.emitChange({
            tipo: 'Conductores',
            Conductores: true,
            idConductor: id
        });
    }

    async maquinaModificarConductoresSeleccionados(estado:number){
        if(this.selection.selected.length < 0)
        {
            this.utils.msgPlainModal('', ['Debe selecionar al menos un conductor para realizar esta accion']);
            return;
        }

        let confirma = false
        if(estado == 2){
            let result = await this.utils.msgPlainModal('Eliminar Conductores', ['¿Está seguro que desea eliminar los conductores seleccionados?']).then(result => {
                confirma = result == OpcionesModal.ACEPTAR;
                console.log(OpcionesModal.ACEPTAR)
            });
        }
        else
            confirma = true;

        if(confirma){
            let data = [];
            this.selection.selected.map((conductor: Conductor) => {
                const index = this.utils.arrayIndexOf(this.conductores, 'usuarioId', conductor.usuarioId);
                if(index >= 0){
                    let datos =  this.updateDriver(conductor, estado);
                    data.push(datos);
                }
            });

            let action: RequestTipo = {
                accion: AccionTipo.ACCION_CLICK,
                datos: data
            };

            await this.maquinaHome.homeUsuarioModificarMasivo(action).then(respuesta => {
                //quita los eliminados de las fuentes de datos
                if(estado == this.ESTADO_ELIMINADO){
                    action.datos.map(d => {
                        this.utils.removeOfList(this.variablesApi.home.conductores, 'usuarioId', d.usuarioId);
                        let index = this.utils.arrayIndexOf(this.conductores, 'usuarioId', d.usuarioId)
                        if(index > -1)
                            this.removeFromList(this.conductores[index]);
                    });
                } 
                this.callApiCallbackHandler(respuesta);
            });
        }
    }

    removeFromList(conductor:any){
        conductor.dropped = true;
        setTimeout(() => {
            this.utils.removeOfList(this.conductores, 'usuarioId', conductor.usuarioId)
        }, 300);
    }

    updateDriver(conductor:Conductor, estado:number){
        let data = {
            cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            usuarioId: conductor.usuarioId,
            //grupoCuentaId: (conductor.grupoId == null) ? 0 : conductor.grupoId,
            usuarioCuentaEstadoId: estado,
            rolId: conductor.rolId,
            //usuarioCuentaConduce: conductor.usuarioCuentaConduce,
            //usuarioCuentaRindeGasto: conductor.usuarioCuentaRindeGasto,
            //usuarioCuentaCreaVehiculo: conductor.usuarioCuentaCreaVehiculo,
            //usuarioCuentaEligeDTE: conductor.usuarioCuentaEligeDTE,
            //usuarioCuentaSeleccionado: conductor.usuarioCuentaSeleccionado,
            vehiculosAsignados: []
        }
        return data;
    }
    
    maquinaModificarConductor(id: number, estado: number) {
        let conductor = this.conductores.filter(r => r.usuarioId === id)[0];
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                usuarioId: id,
                //grupoCuentaId: (conductor.grupoId == null) ? 0 : conductor.grupoId,
                usuarioCuentaEstadoId: estado,
                rolId: conductor.rolId,
                usuarioCuentaConduce: conductor.conduce,
                // usuarioCuentaRindeGasto: conductor.usuarioCuentaRindeGasto,
                // usuarioCuentaCreaVehiculo: conductor.usuarioCuentaCreaVehiculo,
                // usuarioCuentaEligeDTE: conductor.usuarioCuentaEligeDTE,
                // usuarioCuentaSeleccionado: conductor.usuarioCuentaSeleccionado,
                vehiculosAsignados: []
            }
        }
        let confirma: boolean = false;
        if (estado == 2) {
            confirma = this.utils.msgPlain({ tipoMensaje: "confirm", tituloMensaje: "Confirmacion", textoMensaje: "Va a eliminar un conductor de manera definitiva \n\n ¿Está seguro que desea eliminarlo?" });
        } else {
            confirma = true
        }
        
        if (confirma) {
            this.utils.showSpinner("Interno");
            this.maquinaHome.homeUsuarioModificar(action).then(this.callApiCallbackHandler);
        }
    }

    muestraDiaHorario() {
        this.emiter.emitChange({
            tipo: 'DiaHorario',
            diaHorario: true
        });
    }
    muestraEstaciones() {
        this.emiter.emitChange({
            tipo: 'Estacion',
            estacion: true
        });
    }
    
    
    muestraAprobacionConductor(id) {
        this.emiter.emitChange({
            tipo: 'Invitaciones',
            Invitaciones: true,
            idInvitacion: id
        });
    }
    
    fnConviertEstadoInvitacion(estado: string) {
        let retorno: string = "";
        switch (estado) {
            case "CREADA":
            retorno = "Enviada";
            break;
            case "RECHAZADA":
            retorno = "Rechazada por Conductor";
            break;
            case "ACEPTADA":
            retorno = "Aceptada por Conductor";
            break;
            default:
            break;
        }
        return retorno;
    }
    
    cancelarInvitacion(invitacionId: number, invitacionAceptada: number) {
        this.utils.showSpinner("Interno");
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                invitacionId: invitacionId
            }
        }
        
        this.maquinaHome.homeUsuarioCancelaInvitacion(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "", textoMensaje: "Se ha cancelado la Invitacion" });
                this.utils.hideSpinner("Interno");
                this.ngOnInit();
            } else 
                this.serviceCallback(respuesta);
        });
    }

    abrirInvitaciones(){
        this.emiter.emitChange({
            tipo: 'InvitacionesConductores',
        });
    }
    
    autorizarInvitacion(invitacionId: number, autorizacion: number) {
        this.utils.showSpinner("Interno");
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                invitacionId: invitacionId,
                invitacionAutorizada: autorizacion
            }
        }
        
        this.maquinaHome.homeUsuarioAutorizaInvitacion(action).then(this.callApiCallbackHandler);
    }
    
    reenviarInvitacion(invitacionId: number) {
        let datos = [];
        this.utils.showSpinner("Interno");
        let idInvitacion = this.invitados.findIndex(r => r.invitacionId === invitacionId);
        let actualUrl = this.router.url.substr(1);

        let data = {
            cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            rolId: 4,
            usuarioCuentaConduce: true,
            invitacionTelefonoId: this.invitados[idInvitacion].invitacionTelefonoInvitadoId,
            invitacionNombre: this.invitados[idInvitacion].invitacionValidaciones.invitacionNombre,
            invitacionApellido: this.invitados[idInvitacion].invitacionValidaciones.invitacionApellido,
            invitacionSolicitaFoto: this.invitados[idInvitacion].invitacionValidaciones.invitacionSolicitaFoto,
            invitacionSolicitaLicencia: this.invitados[idInvitacion].invitacionValidaciones.invitacionSolicitaLicencia,
            estado: 1,
            montoMaximoDiaValor: this.invitados[idInvitacion].combustibleMontoDiario,
            montoMaximoMesValor: this.invitados[idInvitacion].combustibleMontoMensual
        }
        datos.push(data);
        
        
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: datos
        }
        
        this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "", textoMensaje: "Se ha reenviado la Invitación" });
                this.ngOnInit();
                this.utils.hideSpinner("Interno");
            } 
            this.serviceCallback(respuesta);
        });
    }
    
    formatRut(rut: string) {
        let actual = rut.replace(/^0+/, "");
        let rutPuntos = "";
        if (actual != '' && actual.length > 1) {
            let sinPuntos = actual.replace(/\./g, "");
            let actualLimpio = sinPuntos.replace(/-/g, "");
            let inicio = actualLimpio.substring(0, actualLimpio.length - 1);
            let i = 0;
            let j = 1;
            for (i = inicio.length - 1; i >= 0; i--) {
                let letra = inicio.charAt(i);
                rutPuntos = letra + rutPuntos;
                if (j % 3 == 0 && j <= inicio.length - 1) {
                    rutPuntos = "." + rutPuntos;
                }
                j++;
            }
            let dv = actualLimpio.substring(actualLimpio.length - 1);
            rutPuntos = rutPuntos + "-" + dv;
        }
        return rutPuntos;
    }
    
    validaInput(ingreso: any) {
        let item = ingreso.srcElement;
        item.value = this.utils.formatNumbers(item.value);
        item.value = this.utils.formatCurrency("" + item.value);
    }
    
    currency(val) {
        return this.utils.formatCurrency("" + val);
    }
    
    onlyNumbers(val: string) {
        return this.utils.formatNumbers(val);
    }
    
    // searchComponent() {
    //     let searchValue = this.searchInConductor.toString().toUpperCase();
    //     if (searchValue === "") {
    //         this.iniciaVariablesGlobales();
    //         this.filter = false;
    //     } else {
    //         this.conductores = this.variablesApi.home.conductores.filter(function (f) {
    //             return (f.nombreCompleto.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase().match(searchValue) ||
    //             f.rutConductor.toUpperCase().match(searchValue) ||
    //             f.rolNombre.toUpperCase().match(searchValue) ||
    //             f.limiteMensualCombustible.toString().match(searchValue) ||
    //             (f.porcentajeVentaMes.toString() + '%').match(searchValue)
    //             );
                
    //         });
    //         this.filter = true;
    //     }
    // }

    openPopoverOptions(popover, conductor:Conductor) {
        console.log(conductor)
        if(popover.isOpen())
            popover.close();
        else
            popover.open({conductor})
    }

    /** Evalua la respuesta y recarga la informacion de inicio */
    private callApiCallbackHandler = (respuesta) => {
        this.utils.hideSpinner("Interno");
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA)
            this.ngOnInit();
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL)
            this.utils.openModal(Paginas.ModalAceptar, respuesta.modalSalida).then(response => {
                console.log(response);
            });
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) 
            this.utils.openModal(Paginas.ModalAceptar, respuesta.mensajeSalida);
        
    }

    private serviceCallback(respuesta){
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
            this.utils.hideSpinner("Interno");
            this.utils.openModal(respuesta.modalSalida).then(response => {
                console.log(response);
            });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
            this.utils.hideSpinner("Interno");
            this.utils.msgPlain(respuesta.mensajeSalida);
        }
    }
}
