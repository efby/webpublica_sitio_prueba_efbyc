import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { FormControl } from '@angular/forms';
import { IConfiguracionLimites, IInvitacionConductor } from 'src/app/globales/business-objects';

@Component({
    selector: 'app-limite-gastos',
    templateUrl: './limite-gastos.component.html',
    styleUrls: ['./limite-gastos.component.scss']
})
export class LimiteGastosComponent implements OnInit {
    @ViewChild('p1') popover: any;

    @Input()
    config: IConfiguracionLimites;

    limits: {
        daily: string,
        monthly: string
    }

    
    
    constructor(private utils: UtilitariosService) {
        this.limits = {
            daily: '',
            monthly: ''
        }
    }
    
    ngOnInit() {
        this.limits = {
            daily: this.utils.formatCurrency(this.config.diario),
            monthly: this.utils.formatCurrency(this.config.mensual)
        }
    }

    setCheckValuePermiteComprar(row:IConfiguracionLimites, popover:any){
        row.permiteComprar = !row.permiteComprar;

        this.limits = {
            daily: this.utils.formatCurrency(row.diario),
            monthly: this.utils.formatCurrency(row.mensual)
        }
        
        if(row.permiteComprar)
            popover.open({row, popover});
        else
            popover.close();
    }

    popoverClose(item:IConfiguracionLimites, popover){
        if(!item.applyChanges)
            item.permiteComprar = false;
        popover.close();
    }


    applyChanges(item:IConfiguracionLimites, popover){
        item.applyChanges = true;
        item.permiteComprar = true;
        item.diario = this.limits.daily.replace(/\./g, "");
        item.mensual = this.limits.monthly.replace(/\./g, "");
        popover.close();
    }


    transformAmount(element: any) {
        let item = element.srcElement;
        item.value = this.utils.formatCurrency(item.value);
    }

    edit(row:IInvitacionConductor, popover){
        this.limits = {
            daily: this.utils.formatCurrency(this.limits.daily),
            monthly: this.utils.formatCurrency(this.limits.monthly)
        }
        popover.open({row, popover});
    }

    _showControlError(control:FormControl, force?:boolean){
        this.utils.showControlError(control, force);
    }
    errorDiario: string;
    errorMensual: string
    mensajeGastoDiario() {
        var _sinPuntos = this.limits.daily.replace(/\./g, "")
        
        var _strMensualLimpio = this.limits.monthly!=null?this.limits.monthly.replace(/\./g, ""):"0";
        
        if (_sinPuntos != "") {
            let valorDiario = parseInt(_sinPuntos);
            if (valorDiario <= 0) {
                this.errorDiario = "El límite diario, debe ser <br>superior a 0";
            } else if (_strMensualLimpio != "") {
                let valoMensual = parseInt(_strMensualLimpio);
                if (valorDiario > valoMensual) {
                    this.errorDiario = "El límite diario, es superior <br>al gasto mensual";
                } else {
                    this.errorMensual = valoMensual > 0 ? null : this.errorMensual;
                    this.errorDiario = null;
                }
            }
            else{
                this.errorDiario =null;
            }
        }
    }

    mensajeGastoMensual() {
        
        let _sinPuntos = this.limits.monthly.replace(/\./g, "")
        let _strDiarioLimpio = this.limits.daily!=null?this.limits.daily.replace(/\./g, ""):"0"


        if (_sinPuntos != "") {
            let valorMensual = parseInt(_sinPuntos);
            if (valorMensual <= 0) {
                this.errorMensual = "El límite mensual, debe ser<br>superior a 0";
            } else if (_strDiarioLimpio != "") {
                let valorDiario = parseInt(_strDiarioLimpio);
                if (valorDiario > valorMensual) {
                    this.errorMensual = "El límite mensual, es inferior<br>al gasto diario";
                } else {
                    this.errorMensual = null;
                    this.errorDiario = valorDiario > 0 ? null : this.errorDiario;
                }
            } else {
                this.errorMensual = null;
            }

            //formatearNumero(_sinPuntos, limiteGastoMensualController);
        }
    }
}
