import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService, IEmmitModel, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { PageBase } from '../../pageBase';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, throwMatDuplicatedDrawerError } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { IConfiguracionCombustible, Conductor } from 'src/app/models/conductores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

import * as moment from 'moment';
import 'moment/min/locales';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Vehiculo, IInvitacionConductor, IConfiguracionLimites, IVehiculosInvitacion } from 'src/app/globales/business-objects';
import { RxService } from 'src/app/servicios/rx.service';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { Action } from 'rxjs/internal/scheduler/Action';
moment.locale('es')

@Component({
    selector: 'app-conductor-crear-invitacion',
    templateUrl: './conductor-crear-invitacion.component.html',
    styleUrls: ['./conductor-crear-invitacion.component.scss']
})
export class ConductorCrearInvitacionComponent extends PageBase implements OnInit {
    CURRENT_PAGE = Paginas.HomeMuevoCrearConductor;
    
    @ViewChild('popoverMasive') popoverMasive: any;
    
    navegacion = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Mis Conductores", url: "/" + Paginas.HomeMuevoMisConductores },
        { title: "Invitar Conductores", class: "active", url: "" }
    ];
    
    displayedColumns = [ 'nombre', 'apellido', 'telefono', 'configuracion', 'vehiculos', 'actions' ]
    dataSource: MatTableDataSource<IInvitacionConductor> = new MatTableDataSource<IInvitacionConductor>();
    selection = new SelectionModel<IInvitacionConductor>(true, []);
    
    invitaciones: IInvitacionConductor[];
    
    private unsubscribe = new Subject();
    callbackSubscripcion:any;
    
    get device(): string{
        return this.variablesApi.device;
    }
    
    //@ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    
    multiple:{
        combustible: IConfiguracionLimites,
        alimentacion: IConfiguracionLimites
    }
    
    limits:{
        daily: string,
        monthly: string
    }
    
    PERMITE_ALIMENTACION: boolean = false;
    
    constructor(private rx:RxService, private emiter:BreadCrumbsService, public ref: ChangeDetectorRef, public utils:UtilitariosService, public router:Router, private variablesApi:VariablesGenerales) { 
        super(ref, utils, router);
        this.emiter.emitChange({ migajas: this.navegacion, muestraBreadcrumbs: true });
        
        this.invitaciones = [];
        
        this.multiple = {
            combustible: {
                title: 'combustible',
                applyChanges: false,
                permiteComprar: false,
                diario: '',
                mensual: ''
            },
            alimentacion: {
                title: 'alimentación',
                applyChanges: false,
                permiteComprar: false,
                diario: '',
                mensual: ''
            }
        }
        
        
    }
    
    ngOnInit() {
        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);
        this.bindTableWithtOriginalData(this.invitaciones)
        
    }
    
    toNumber(element:any){
        this.utils.validaMaxInputNumber(element);
        
        // let item = element.srcElement;
        // item.value = this.utils.formatNumbers(item.value);
    }
    
    invitacionValid(item:IInvitacionConductor):boolean{
        let result = true;
        
        if(this.utils.isNullOrWithSpace(item.nombre))
        result = false;
        else if(this.utils.isNullOrWithSpace(item.apellido))
        result = false;
        else if(this.utils.isNullOrWithSpace(item.telefono) || (!this.utils.isNullOrWithSpace(item.telefono) && item.telefono.length != 8))
        result = false;
        
        return result;
    }
    
    invitacionesValid(){
        let result = true;
        
        this.invitaciones.map(i =>{
            if(!this.invitacionValid(i))
            result = false;
        });
        
        return result;
    }
    
    aplicarMasivo(popover){
        this.invitaciones = this.invitaciones.map(i => {
            i.combustible = {
                title: 'combustible',
                applyChanges: true,
                permiteComprar: this.multiple.combustible.permiteComprar,
                diario: this.multiple.combustible.diario.replace(/\./g, ""),
                mensual: this.multiple.combustible.mensual.replace(/\./g, "")
            }
            
            if(this.PERMITE_ALIMENTACION){
                i.alimentacion = {
                    title: 'alimentación',
                    applyChanges: true,
                    permiteComprar:  this.multiple.alimentacion.permiteComprar,
                    diario: this.multiple.alimentacion.diario.replace(/\./g, ""),
                    mensual: this.multiple.alimentacion.mensual.replace(/\./g, "")
                }
            }
            
            return i;
        });
        
        this.bindTableWithtOriginalData(this.invitaciones);
        this.popoverMasive.close();
    }
    
    agregarNuevaFila(){
        
        if(this.invitaciones.length >= 20){
            console.log(this.invitaciones.length);
            this.utils.msgPlainModalAccept('Error', [ 'No puedes enviar mas de 20 invitaciones simultáneamente.' ]).then(result => {
                
            });
            return;
        }

        
        let invitacion:IInvitacionConductor = {
            id: moment().toISOString(),
            nombre: '',
            apellido: '',
            telefono: '',
            combustible: {
                title: 'combustible',
                applyChanges: false,
                permiteComprar:false,
                diario: '',
                mensual: ''
            },
            alimentacion: {
                title: 'alimentación',
                applyChanges: false,
                permiteComprar:false,
                diario: '',
                mensual: ''
            },
            vehiculos: []
        }
        this.invitaciones.push(invitacion);
        this.bindTableWithtOriginalData(this.invitaciones)
        
    }
    
    openMasiveAdjusts(popover){
        let multiple = this.multiple;
        multiple.combustible.diario = this.utils.formatCurrency(multiple.combustible.diario.replace(/\./g, ""));
        multiple.combustible.mensual = this.utils.formatCurrency(multiple.combustible.mensual.replace(/\./g, ""));
        popover.open({multiple, popover});
    }
    
    toggleEnableCombustible(combustible:IConfiguracionLimites, popover:any){
        combustible.permiteComprar = !combustible.permiteComprar;
    }
    
    
    remove(item:IInvitacionConductor){
        
        let index = this.utils.arrayIndexOf(this.invitaciones, 'id', item.id);
        if(index > -1){
            this.invitaciones.splice(index, 1);
            this.bindTableWithtOriginalData(this.invitaciones);
        }
    }
    
    enviarInvitaciones(){
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        
        this.rx.invitacionCrearInvitaciones(cuentaId, this.invitaciones).then((response:any) => {
            console.log(response)
            this.utils.msgPlainModalAccept('Invitación enviada', [response.data.userMessage]).then(result => {
                if(result == OpcionesModal.ACEPTAR)
                this.router.navigate([Paginas.HomeMuevoMisConductores])
            })
            
        }, this.errorCallback)
    }
    
    mostrarVehiculosAsignados(invitacion:IInvitacionConductor){
        let c:Conductor = new Conductor();
        
        c.nombre = invitacion.nombre;
        c.apellido = invitacion.apellido;
        c.telefonoId = invitacion.telefono;
        c.vehiculosAsignados = invitacion.vehiculos.map((x:IVehiculosInvitacion) => {
            let v:Vehiculo = {
                vehiculoId: x.vehiculoId,
                patente: x.patente,
                alias: x.alias,
                
                estado: true,
                tipo: 1,
                anio: '',
                marca: '',
                modelo: '',
                tipoString: '',
                
                requireOdometro: false,
                frecuenciaOdometro: 0
            }
            return v;
        });
        
        
        let model: IEmmitModel = {
            tipo: 'Vehiculos Asignados',
            action: 'OPEN',
            data: {
                origin: Paginas.HomeMuevoCrearConductor,
                conductor: c,
                conductorKey: invitacion.id
            }
        }
        this.emiter.emmitChange(model);
    }
    
    transformAmount(element: any) {
        let item = element.srcElement;
        item.value = this.utils.formatCurrency(item.value);
    }
    
    private bindTableWithtOriginalData(data:IInvitacionConductor[]){
        this.dataSource = new MatTableDataSource(data);
        this._configureFunctionOfMatTable(this.dataSource, null, this.sort);
    }
    
    private emitterHandler = (data:IEmmitCallbackModel) => {
        if(data.from == Paginas.ModalVehiculosAsignados && data.action == this.CURRENT_PAGE){
            
            let index = this.utils.arrayIndexOf(this.invitaciones, 'id', data.data.conductorKey);
            if(index > -1){
                let nuevos = data.data.vehiculosAsignados.map(x => {
                    let v:IVehiculosInvitacion = {
                        vehiculoId: x.vehiculoId,
                        patente: x.patente,
                        alias: x.alias
                    }
                    return v;
                });
                this.invitaciones[index].vehiculos = nuevos;
                this.bindTableWithtOriginalData(this.invitaciones);
            }
        }
    }
    
    public validaMaxInputText(e:any){
        this.utils.validaMaxInputText(e);
    }

    ngOnDestroy() {
        if(this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }
}

