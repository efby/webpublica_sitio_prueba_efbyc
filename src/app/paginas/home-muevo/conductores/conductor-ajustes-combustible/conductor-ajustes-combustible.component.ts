import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Conductor, IModificarConductor } from 'src/app/models/conductores';
import { IEmmitModel, BreadCrumbsService, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { PageBase } from '../../pageBase';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { NgForm } from '@angular/forms';
import { Paginas } from 'src/app/globales/paginas';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-conductor-ajustes-combustible',
    templateUrl: './conductor-ajustes-combustible.component.html',
    styleUrls: ['./conductor-ajustes-combustible.component.scss']
})
export class ConductorAjustesCombustibleComponent extends PageBase implements OnInit {
    CURRENT_PAGE = Paginas.ModalConductorAjusteDiarioMensual;

    @ViewChild('diarioMensualForm', { read: NgForm }) form: any;

    current: Conductor;
    limits: {
        daily: string;
        mouthly: string;
    }
    vehiculosModificados: boolean

    private unsubscribe = new Subject();
    callbackSubscripcion: any;

    constructor(public variablesApi: VariablesGenerales, private rx: RxService, public ref: ChangeDetectorRef, public router: Router, public utils: UtilitariosService, private emiter: BreadCrumbsService) {
        super(ref, utils, router);

        this.reset();
    }

    ngOnInit() {
        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);
    }

    applyChanges() {


        let model: IModificarConductor = {
            cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            usuarioId: this.current.usuarioId,
            rolId: this.current.rolId,
            estado: this.current.rolId == 1 ? 1 : (this.current.estado ? 1 : 0),
            configuracionCombustible: {
                permiteComprar: this.current.rolId == 1 ? true : this.current.restriccionCombustible.permiteComprar,
                estadoCombustible: this.current.rolId == 1 ? this.current.restriccionCombustible.permiteComprar : true,
                montoMaximoDiaValor: this.current.restriccionCombustible.permiteComprar ? parseInt(this.limits.daily.replace(/\./g, "")) : 0,
                montoMaximoMesValor: this.current.restriccionCombustible.permiteComprar ? parseInt(this.limits.mouthly.replace(/\./g, "")) : 0
            }
        };

        this.utils.showSpinner();
        this.rx.conductoresModificaConductor(model).then((result: any) => {
            this.utils.hideSpinner();
            this.emiter.emitCallback({
                from: Paginas.HomeMuevoMisConductores,
                action: 'refresh'
            });
            this.closeAll();
        });
    }

    setCheckValuePermiteComprar() {
        this.current.restriccionCombustible.permiteComprar = !this.current.restriccionCombustible.permiteComprar;
        this.ref.detectChanges();
    }

    setData(conductor: Conductor) {
        this.reset();
        this.current = conductor;

        if (this.current.rolId == 1)
            this.current.restriccionCombustible.permiteComprar = this.current.restriccionCombustible.montoMaximosEstado;
    }

    closeAll() {
        event.preventDefault();
        event.stopPropagation();

        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarConductorAjusteDiarioMensual', 'sideBarConductores'],
            }
        }

        this.emiter.emmitChange(config);
    }

    backToMain() {
        event.preventDefault();
        event.stopPropagation();

        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarConductorAjusteDiarioMensual']
            }
        }

        this.emiter.emmitChange(config);
    }

    transformAmount(event: any) {
        this.utils.validaMaxInputCurrency(event);
    }

    maskCurrency(event: any) {
        let item = event.srcElement;
        item.value = this.utils.formatCurrency(item.value);
    }
    errorDiario: string;
    errorMensual: string
    mensajeGastoDiario() {
        var _sinPuntos = this.limits.daily.replace(/\./g, "")
        var _strMensualLimpio = this.limits.mouthly!=null?this.limits.mouthly.replace(/\./g, ""):"0"
        if (_sinPuntos != "") {
            let valorDiario = parseInt(_sinPuntos);
            if (valorDiario <= 0) {
                this.errorDiario = "El límite diario, debe ser superior a 0";
            } else if (_strMensualLimpio != "") {
                let valoMensual = parseInt(_strMensualLimpio);
                if (valorDiario > valoMensual) {
                    this.errorDiario = "El límite diario, es superior al gasto mensual";
                } else {
                    this.errorMensual = valoMensual > 0 ? null : this.errorMensual;
                    this.errorDiario = null;
                }
            }
            else{
                this.errorDiario =null;
            }
        }
    }

    mensajeGastoMensual() {
        let _sinPuntos = this.limits.mouthly.replace(/\./g, "")
        let _strDiarioLimpio = this.limits.daily!=null?this.limits.daily.replace(/\./g, ""):"0"


        if (_sinPuntos != "") {
            let valorMensual = parseInt(_sinPuntos);
            if (valorMensual <= 0) {
                this.errorMensual = "El límite mensual, debe ser superior a 0";
            } else if (_strDiarioLimpio != "") {
                let valorDiario = parseInt(_strDiarioLimpio);
                if (valorDiario > valorMensual) {
                    this.errorMensual = "El límite mensual, es inferior al gasto diario";
                } else {
                    this.errorMensual = null;
                    this.errorDiario = valorDiario > 0 ? null : this.errorDiario;
                }
            }  else {
                this.errorMensual = null;
            }

            //formatearNumero(_sinPuntos, limiteGastoMensualController);
        }
    }

    reset() {
        if (this.form != undefined)
            this.form.reset();

        this.current = new Conductor();
        this.limits = {
            daily: '',
            mouthly: ''
        }
        this.vehiculosModificados = false;
    }



    private emitterHandler = (data: IEmmitCallbackModel) => {
        // if(data.from == Paginas.ModalVehiculosAsignados && data.action == this.CURRENT_PAGE)
        // {
        //     this.current.vehiculosAsignados = data.data.vehiculosAsignados;
        //     this.vehiculosModificados = true;
        // }
    }

    ngOnDestroy() {
        if (this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }

}
