import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Conductor } from 'src/app/models/conductores';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { PageBase } from '../../pageBase';
import { TableDataCsv } from 'src/app/globales/business-objects';
import { ExportDataService } from 'src/app/servicios/export-data.service';

import * as moment from 'moment';
import 'moment/min/locales';

@Component({
    selector: 'app-muevo-conductores-generales',
    templateUrl: './muevo-conductores-generales.component.html',
    styleUrls: ['./muevo-conductores-generales.component.scss']
})
export class MuevoConductoresGeneralesComponent extends PageBase implements OnInit {
    
    private conductores: Conductor[];
   
    displayedColumns = [  'rut', 'nombreCompleto', 'telefonoId', 'email', 'primerVehiculoAsignado', 'actions' ]
    dataSource: MatTableDataSource<Conductor> = new MatTableDataSource<Conductor>();
    selection = new SelectionModel<Conductor>(true, []);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router, private rx:RxService, 
        public variablesApi: VariablesGenerales,
        private emiter: BreadCrumbsService) { 
        super(ref, utils, router)
    }
    
    ngOnInit() {

    }

    loadData(conductores:Conductor[], ){
        this.conductores = conductores;

        this.dataSource = new MatTableDataSource(this.conductores);
        this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);
    }

    mostrarConductor(item:Conductor, event) {
        event.preventDefault();
        event.stopPropagation();
        this.emiter.emitChange({
            tipo: 'Conductor',
            action: 'GET_DATA',
            usuarioId: item.usuarioId
        });
    }

    exportCsv(){
        
        let data: TableDataCsv = {
            header:  [ 'RUT', 'NOMBRE', 'TELÉFONO', 'EMAIL', 'VEHICULOS ASIGNADOS' ],
            rows: this.dataSource.data.map((x:Conductor) => { 
                return [
                    x.rut,
                    x.nombreCompleto,
                    x.telefonoId,
                    x.email,
                    x.vehiculosAsignadosCantidad
                ]
            })
        };

        ExportDataService.ExportCsv(this.generateFilenameFormat('conductores_', this.variablesApi.home.cuentaSeleccionada.cuentaId.toString()), data.rows, data.header, ';', ',', 'Conductores MUEVO EMPRESAS')
    }

    private generateFilenameFormat(prefix:string, key:string){
        return prefix + `${key}_${moment().format('YYYYMMDD_HHmm')}`;
    }

    
    toogleEnable(){}

    
}
