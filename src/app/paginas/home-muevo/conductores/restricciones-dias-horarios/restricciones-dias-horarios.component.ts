import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Conductor, IModificarConductor } from 'src/app/models/conductores';
import { BreadCrumbsService, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { RxService } from 'src/app/servicios/rx.service';
import { PageBase } from '../../pageBase';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Paginas } from 'src/app/globales/paginas';

@Component({
    selector: 'app-restricciones-dias-horarios',
    templateUrl: './restricciones-dias-horarios.component.html',
    styleUrls: ['./restricciones-dias-horarios.component.scss']
})
export class RestriccionesDiasHorariosComponent extends PageBase implements OnInit {
    
    conductor: Conductor;
    current: {
        configSeleccionHorario:number,

        configSeleccion: number,
        personalizado: boolean;

        estadoDias: boolean,
        estadoHorarios: boolean,

        diasHabilitados: number[],
        horariosHabilitados: number[],

        //configSeleccionHora: number,
        horarioPersonlizado: boolean,
        horaDesde: number,
        horaHasta: number,

        errorPersonalizadoSinDias: boolean,
        //errorPersonalizadoSinHoras: boolean
    }
    
    dias = [{ id: 0, nombre: "Lunes", selected: false }, { id: 1, nombre: "Martes", selected: false }, { id: 2, nombre: "Miércoles", selected: false }, { id: 3, nombre: "Jueves", selected: false }, { id: 4, nombre: "Viernes", selected: false }, { id: 5, nombre: "Sabado", selected: false }, { id: 6, nombre: "Domingo", selected: false }];
    horas = [{ id: 0, nombre: "00" }, { id: 1, nombre: "01" }, { id: 2, nombre: "02" }, { id: 3, nombre: "03" }, { id: 4, nombre: "04" }, { id: 5, nombre: "05" }, { id: 6, nombre: "06" }, { id: 7, nombre: "07" }, { id: 8, nombre: "08" }, { id: 9, nombre: "09" }, { id: 10, nombre: "10" }, { id: 11, nombre: "11" }, { id: 12, nombre: "12" }, { id: 13, nombre: "13" }, { id: 14, nombre: "14" }, { id: 15, nombre: "15" }, { id: 16, nombre: "16" }, { id: 17, nombre: "17" }, { id: 18, nombre: "18" }, { id: 19, nombre: "19" }, { id: 20, nombre: "20" }, { id: 21, nombre: "21" }, { id: 22, nombre: "22" }, { id: 23, nombre: "23" }];
    


    constructor(public variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, private rx:RxService, public utils: UtilitariosService, public router: Router, public ref: ChangeDetectorRef) {
        super(ref, utils, router);

        this.resetearSeleccion();
    }
    
    ngOnInit() {

    }
    
    setData(conductor: Conductor){
        this.dias.forEach(element => element.selected = false);
        this.resetearSeleccion();

        this.conductor = conductor;

        this.current.diasHabilitados = this.conductor.diasHabilitados;
        this.current.horariosHabilitados = this.conductor.horariosHabilitados;

        this.dias.map(x => {
            if(this.conductor.diasHabilitados.indexOf(x.id) > -1)
                x.selected = true;
        })
        this.current.configSeleccionHorario = this.current.horariosHabilitados.length == 24 ? 0 : 1;
        this.seleccionaHorario(this.current.configSeleccionHorario, false);
        //let dias = this.conductor.restriccionCombustible.

        // switch(this.conductor.seleccionDiaHorario){
        //     case TipoRestriccion.TODO_DIA_TODO_HORARIO: 
        //         this.seleccionaDiaHorario(0, false);
        //         break;
        //     case TipoRestriccion.LUNES_VIERNES_TODO_HORARIO:
        //         this.seleccionaDiaHorario(1, false);
        //         break;
        //     case TipoRestriccion.LUNES_VIERNES_06_20:
        //         this.seleccionaDiaHorario(2, false);
        //         break;
        //     case TipoRestriccion.PERSONALIZADO:
        //         this.seleccionaDiaHorario(3, false);
        //         break;
        // }

        
    }

    closeAll(){
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarRestriccionesConductor', 'sideBarConductores']
            }
        }

        this.emiter.emmitChange(config);
    }

    backToMain(){
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarRestriccionesConductor']
            }
        }

        this.emiter.emmitChange(config);
    }

    seleccionaHorario(value: number, seleccionManual:boolean){
        this.current.configSeleccionHorario = value;

        switch(this.current.configSeleccionHorario){
            case 0:
                this.current.estadoHorarios = true;
                this.current.horariosHabilitados = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
                break;
            case 1:
                this.current.horaDesde = null;
                this.current.horaHasta = null;

                this.current.horaDesde = this.conductor.horariosHabilitados[0];
                this.current.horaHasta = this.conductor.horariosHabilitados[this.conductor.horariosHabilitados.length - 1] + 1;
                break;
        }
    }

    // seleccionaDiaHorario(value: number, seleccionManual:boolean) {
    //     event.preventDefault();
    //     event.stopPropagation();

    //     console.log(value)
        
    //     switch(value){
    //         case 0: 
    //             this.current.personalizado = false;
    //             this.current.configSeleccion = 0;
    //             this.current.estadoDias = true;
    //             this.current.diasHabilitados = [0,1,2,3,4,5,6];

    //             this.current.estadoHorarios = true;
    //             this.current.horariosHabilitados = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
    //             break;
    //         case 1:
    //             this.current.personalizado = false;
    //             this.current.configSeleccion = 1;
    //             this.current.estadoDias = true;
    //             this.current.diasHabilitados = [0,1,2,3,4];

    //             this.current.estadoHorarios = true;
    //             this.current.horariosHabilitados = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
    //             break;
    //         case 2:
    //             this.current.personalizado = false;
    //             this.current.configSeleccion = 2;
    //             this.current.estadoDias = true;
    //             this.current.diasHabilitados = [0,1,2,3,4];

    //             this.current.estadoHorarios = true;
    //             this.current.horariosHabilitados = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
    //             break;
    //         case 3:
    //             this.current.personalizado = true;
    //             this.current.configSeleccion = 3;
    //             this.current.estadoDias = true;

    //             this.current.estadoHorarios = true;
                
    //             this.dias.forEach(element => element.selected = false);
    //             this.current.horaDesde = null;
    //             this.current.horaHasta = null;
                
    //             if(!seleccionManual){
    //                 if(this.conductor.diasHabilitados.length > 0)
    //                     this.dias.map(x => {
    //                         if(this.conductor.diasHabilitados.indexOf(x.id) > -1)
    //                             x.selected = true;
    //                     })

    //                 this.current.horaDesde = this.conductor.horariosHabilitados[0];
    //                 this.current.horaHasta = this.conductor.horariosHabilitados[this.conductor.horariosHabilitados.length -1 ];
    //             }
    //             break;
    //     }
    //     this.ref.detectChanges();
    // }

    seleccionDiaPersonalizado(val: number) {
        let index = this.dias.findIndex(r => r.id == val);
        this.dias[index].selected = !this.dias[index].selected;

        this.current.diasHabilitados = this.dias.filter(x => x.selected).map(x => x.id);
    }

    // seleccionHorarioPersonalizado(val: number) {
    //     event.preventDefault();
    //     event.stopPropagation();

    //     //this.current.configSeleccion = val;
    //     if (this.current.configSeleccion != 3) {
    //         this.current.horaDesde = null;
    //         this.current.horaHasta = null;
    //     }
    // }

    guardar(){
        if(!this.validarSeleccion())
            return;

        let model: IModificarConductor = {
            cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            usuarioId: this.conductor.usuarioId,
            estado: 1,
            rolId: this.conductor.rolId,
            restriccionesCombustible: {
                diasHabilitar: true,
                diasHabilitados: this.current.diasHabilitados,

                horarioHabilitar: true,
                horariosHabilitados: this.current.configSeleccionHorario == 1 ? this.obtenerHorariosPersonalizados() : this.current.horariosHabilitados,

                estacionHabilitar: false,
                estacionesHabilitadas: []
            }
        }

        this.utils.showSpinner();
        this.rx.conductoresModificaConductor(model).then((result:any) => {
            this.utils.hideSpinner();
            this.emiter.emitCallback({
                from: Paginas.HomeMuevoMisConductores,
                action: 'refresh'
            });
            
            this.closeAll();
        }, this.errorCallback)
    }

    private resetearSeleccion(){
        this.conductor = new Conductor();
        this.current = {
            configSeleccionHorario: 0,

            configSeleccion: 0,
            personalizado: false,
            estadoDias: false,
            estadoHorarios: false,
            diasHabilitados: [],
            horariosHabilitados: [],

            //configSeleccionHora: 0,
            horarioPersonlizado: false,
            horaDesde: 0,
            horaHasta: 0,

            errorPersonalizadoSinDias: false,
            //errorPersonalizadoSinHoras: false
        }

        //this.current.errorPersonalizadoSinDias = false;
        //this.current.errorPersonalizadoSinHoras = false
    }

    private loadData(c:Conductor){
        if(c.diasHabilitados.length == 7 && c.horariosHabilitados.length == 24)
            this.current.configSeleccion = 0;
    }

    private obtenerHorariosPersonalizados(): number[]{
        let res:number[] = [];
        for(let i = parseInt(this.current.horaDesde.toString()); i <= this.current.horaHasta - 1; i++){
            res.push(i);
        }
        
        let arr = 
        console.log(res)
        return res;
    }

    private validarSeleccion(): boolean{
        let res = false;
        //valida los dias seleccionados
        if(this.current.configSeleccion == 3)
        {
            let hayDiasSeleccionados = this.dias.filter(x => x.selected).length > 0;
            this.current.errorPersonalizadoSinDias = !hayDiasSeleccionados;
        }
        else
            this.current.errorPersonalizadoSinDias = false
        

        //valida que tenga horarios
        // if(this.current.configSeleccionHora == 1){
        //     let seleccionaHoras = this.current.horaDesde != null && this.current.horaHasta != null;
        //     this.current.errorPersonalizadoSinHoras = !seleccionaHoras;
        // }
        // else
        //     this.current.errorPersonalizadoSinHoras = false;
        
        res = !this.current.errorPersonalizadoSinDias;// && !this.current.errorPersonalizadoSinHoras;
        return res;
    }
    
}

export enum TipoRestriccion{
    TODO_DIA_TODO_HORARIO = 'TODO_DIA_TODO_HORARIO',
    LUNES_VIERNES_TODO_HORARIO = 'LUNES_VIERNES_TODO_HORARIO',
    LUNES_VIERNES_06_20 = 'LUNES_VIERNES_06_20',
    PERSONALIZADO = 'PERSONALIZADO'
}
