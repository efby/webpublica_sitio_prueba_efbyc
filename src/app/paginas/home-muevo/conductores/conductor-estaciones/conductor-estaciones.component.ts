import { Component, OnInit } from '@angular/core';
import { IEmmitModel, BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';

@Component({
    selector: 'app-conductor-estaciones',
    templateUrl: './conductor-estaciones.component.html',
    styleUrls: ['./conductor-estaciones.component.scss']
})
export class ConductorEstacionesComponent implements OnInit {
    
    constructor(private emiter:BreadCrumbsService) { }
    
    ngOnInit() {
    }
    
    closeAll(){
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarCondutorEstaciones', 'sideBarConductores'],
            }
        }
        
        this.emiter.emmitChange(config);
    }
    
    backToMain(){
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarCondutorEstaciones']
            }
        }
        
        this.emiter.emmitChange(config);
    }
    
}
