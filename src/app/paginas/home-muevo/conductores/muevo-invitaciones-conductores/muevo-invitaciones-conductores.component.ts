import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Invitacion, IInvitacionConductor, IPagination } from 'src/app/globales/business-objects';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo } from 'src/app/globales/enumeradores';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';
import { Router } from '@angular/router';

@Component({
    selector: 'app-muevo-invitaciones-conductores',
    templateUrl: './muevo-invitaciones-conductores.component.html',
    styleUrls: ['./muevo-invitaciones-conductores.component.scss']
})
export class MuevoInvitacionesConductoresComponent extends PageBase implements OnInit {
    
    invitaciones: Invitacion[];

    pagination: IPagination;
    

    constructor(public utils:UtilitariosService, private rx:RxService, private variablesApi:VariablesGenerales, public ref: ChangeDetectorRef, public router: Router) {
        super(ref, utils, router);
        
     }
    
    ngOnInit() {
        this.reset();
    }

    reset(){
        this.invitaciones = [];

        this.pagination = {
            currentPage: 0,
            pages: 0,
            totalRows: 0, 
            positionFinal: 0,
            pagesSize: 0,
            rows: 0
        }
    }
    
    public cargarInvitaciones(){
        

        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        this._isLoading = true;
        this.rx.invitacionesGet(cuentaId, this.pagination.positionFinal).then((result:any) => {
            this._isLoading = false;
            
            this.invitaciones = this.invitaciones.concat(result.data.invitados);
            this.invitaciones = this.invitaciones.map(x => {
                x.invitacionEstadoInvitado = x.invitacionEstadoInvitado.replace('_', ' ');
                return x;
            })

            this.paginationHandler(result.data.pagination);
        }, this.errorCallback);

    }

    reenviarInvitacion(item:Invitacion){

        let i:IInvitacionConductor = {
            id: '',
            nombre: item.invitacionValidaciones.invitacionNombre,
            apellido: item.invitacionValidaciones.invitacionApellido,
            telefono: item.invitacionTelefonoInvitadoId.indexOf('569') == 0 ? item.invitacionTelefonoInvitadoId.replace('569', '') : item.invitacionTelefonoInvitadoId,
            combustible: {
                title: '',
                applyChanges: false,
                permiteComprar: item.activaCombustible,
                diario: item.combustibleMontoDiario.toString(),
                mensual: item.combustibleMontoMensual.toString()
            },
            alimentacion: {
                title: '',
                applyChanges: false,
                permiteComprar: false,
                diario: '0',
                mensual: '0'
            },
            vehiculos: item.vehiculosAsignados.map(x => {
                return {
                    vehiculoId: x,
                    patente: '',
                    alias: '',
                }
            })
        }
        
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

        this.utils.showSpinner("Interno");
        this.rx.invitacionCrearInvitaciones(cuentaId, [i]).then(result => {
            this.utils.hideSpinner("Interno");
            this.utils.msgPlainModalAccept('Correcto', ['Se ha reenviado la Invitación']);
            
            this.pagination.positionFinal = 0;
            this.invitaciones = [];
            this.cargarInvitaciones();
        }, this.errorCallback);
    }

    eliminar(item:Invitacion){
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        let invitacionId = item.invitacionId;

        this.utils.showSpinner("Interno");
        this.rx.invitacionCancelarInvitacionConductor(cuentaId, invitacionId).then(result => {
            this.utils.hideSpinner("Interno");
            this.pagination.positionFinal = 0;
            this.invitaciones = [];

            this.cargarInvitaciones();
            
        }, this.errorCallback);
    }

    next(){
        this.cargarInvitaciones();
    }

    private paginationHandler(configResponse:any){
        this.pagination.positionFinal = configResponse.posicionFinal;
        this.pagination.totalRows = configResponse.totalRegistros;
        this.pagination.pagesSize = configResponse.registrosPorPagina;
        this.pagination.rows = configResponse.registrosObtenidos;
    }
    
}
