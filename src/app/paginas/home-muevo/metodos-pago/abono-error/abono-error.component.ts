import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-abono-error',
  templateUrl: './abono-error.component.html',
  styleUrls: ['./abono-error.component.scss']
})
export class AbonoErrorComponent implements OnInit {

  constructor(private modal: NgbActiveModal) { }
  
  @Input()
  public errorMessage:string;

  ngOnInit() {
  }

  close(){
      this.modal.close();
  }


}
