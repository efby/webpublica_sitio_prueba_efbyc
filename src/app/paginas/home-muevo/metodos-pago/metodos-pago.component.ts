import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService, IEmmitCallbackModel, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Router, ActivatedRoute } from '@angular/router';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, OpcionesModal, NavegacionTipo } from 'src/app/globales/enumeradores';
import { MedioPago, CuentaCorriente, Tarjeta, Saldo, IAbonoPost } from 'src/app/globales/business-objects';
import { RxService } from 'src/app/servicios/rx.service';
import { PageBase } from '../pageBase';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbonoProcesarComponent } from './abono-procesar/abono-procesar.component';
import { AbonoExitoComponent } from './abono-exito/abono-exito.component';
import { DecimalPipe } from '@angular/common';
import { AbonoErrorComponent } from './abono-error/abono-error.component';
import { Respuestas } from 'src/app/globales/respuestas';
import { Constantes } from 'src/app/globales/constantes';
import { SidebarModule } from 'ng-sidebar';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RetirarAbonoComponent } from '../abonar/retirar-abono/retirar-abono.component';

@Component({
    selector: 'app-metodos-pago',
    templateUrl: './metodos-pago.component.html',
    styleUrls: ['./metodos-pago.component.scss'],
    providers: [DecimalPipe]
})
export class MetodosPagoComponent extends PageBase implements OnInit {
    CURRENT_PAGE = Paginas.HomeMuevoMisMetodosPago;
    ui: {
        selection: string
        billeteraSelection: string
    }
    saldo: Saldo;
    tarjeta: Tarjeta;
    tarjetas: Tarjeta[];
    cuentasCorrientes?: CuentaCorriente[];

    device: string;

    abonoBilletera: {
        tarjeta: Tarjeta,
        monto: string
    }

    _atendedor: boolean = false;
    _billetera: boolean = false;
    _tarjeta: boolean = false;
    _oneClick: boolean = false;

    private unsubscribe = new Subject();
    callbackSubscripcion: any;

    tipoMenu: string;
    migajas = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Formas de Pago", class: "active", url: "" }
    ];


    _btnSiguiente: boolean = false;
    _visibleTab: boolean = false;
    mediosPago?: MedioPago[];

    //tarjetas?: Tarjeta[];
    disTarjetas: boolean = true;
    opened: any;
    idCompartido: string = "";

    interval: any;
    isMobile: boolean = false;

    constructor(
        private constantes: Constantes,
        private modalSidebar: SidebarService,
        private numberPipe: DecimalPipe, private modalService: NgbModal, private rx: RxService, private emiter: BreadCrumbsService, public variablesApi: VariablesGenerales, public router: Router, private route: ActivatedRoute, private maquinaHome: MaquinaHome, public utils: UtilitariosService, public ref: ChangeDetectorRef) {
        super(ref, utils, router)

        this.variablesApi.ultimaPagina = Paginas.HomeMuevoMisMetodosPago;
        this.ui = {
            selection: FormasPagoTab.Billetera,
            billeteraSelection: 'tarjeta' || 'ctacte'
        }

        this.abonoBilletera = {
            monto: null,
            tarjeta: null
        }

        this.cuentasCorrientes = [];



        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        ref.detach();
        this.interval = setInterval(() => {
            this.ref.detectChanges();
            this.isMobile = this.variablesApi.esMobile;
        }, 10);

    }

    ngOnDestroy() {
        clearInterval(this.interval);


        if (this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();

    }

    ngOnInit() {
        this.device = this.variablesApi.device;
        this._pageReady = true;

        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);

        this.homeCuentaConsultarMedioPago();


        this.tipoMenu = this.route.snapshot.paramMap.get('origen');
        this._visibleTab = false;
        if (this.tipoMenu === 'N') {
            setTimeout(() => {
                this.emiter.emitChange({ tipo: "Configuracion", _isConfiguracion: true });
            });
            this._btnSiguiente = true;
        } else {
            setTimeout(() => {
                this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
            });
            this._btnSiguiente = false;
        }
    }

    private emitterHandler = (data: IEmmitCallbackModel) => {
        if (data.from == Paginas.HomeMuevoMisMetodosPago) {
            this._oneClick = false;
            this._atendedor = false;
            this._billetera = false;
            this.homeCuentaConsultarMedioPago();
        }
    }

    homeCuentaConsultarMedioPago() {

        this.utils.showSpinner("Interno");
        this.rx.mediosPagoGet(this.variablesApi.home.cuentaSeleccionada.cuentaId).then((result: any) => {
            this.idCompartido = result.data.tarjetaCompartidaId;
            this.saldo = result.data.saldo;
            this.tarjetas = result.data.tarjetas;
            this.tarjeta = this.tarjetas.filter(x => x.idInscripcion == this.idCompartido)[0];

            this.cuentasCorrientes = result.data.cuentasCorrientes;
            this.mediosPago = result.data.mediosPago;

            this.cargaDataInicial();
            this.utils.hideSpinner("Interno");
        }, this.errorCallback);
    }

    cargaDataInicial() {

        if (this.tarjetas.length > 0)
            this.disTarjetas = false;
        else
            this.disTarjetas = true;

        this.mediosPago.forEach(mp => {
            switch (mp.formaPagoId) {
                case 1:
                    this._atendedor = mp.formaPagoActiva;
                    break;
                case 2:
                    this._billetera = mp.formaPagoActiva;
                    break;
                case 3:
                    this._oneClick = mp.formaPagoActiva;
                    break;
                default:
                    break;
            }
        });
    }

    nvaCtaCte() {
        this.emiter.emitChange({
            tipo: 'CuentaCorriente',
            cuentaCorriente: true,
            edicion: false,
            muestraMenu: this.tipoMenu
        });
    }

    irAConductores() {
        this.router.navigate([Paginas.HomeMuevoCrearConductor, this.tipoMenu]);
    }

    modificarSaldo() {
        let saldo: number = this._billetera ? 0 : 1;
        let estacion: number = this._atendedor ? 1 : 0;
        let oneClick: number = this._oneClick ? 1 : 0;
        this.homeCuentaModificarMedioPago(saldo, estacion, oneClick);
    }

    modificarEstacion() {
        let saldo: number = this._billetera ? 1 : 0;
        let estacion: number = this._atendedor ? 0 : 1;
        let oneClick: number = this._oneClick ? 1 : 0;
        this.homeCuentaModificarMedioPago(saldo, estacion, oneClick);
    }

    modificarOneClick() {

        //en este context _oneClick aun no tiene su valor actualizado
        if (!this._oneClick)
            this.muestraSeleccionTarjeta();
        else {
            let saldo: number = this._billetera ? 1 : 0;
            let estacion: number = this._atendedor ? 1 : 0;
            let oneClick: number = this._oneClick ? 0 : 1;
            this.homeCuentaModificarMedioPago(saldo, estacion, oneClick);
        }

        // let saldo: number = this._billetera ? 1 : 0;
        // let estacion: number = this._atendedor ? 1 : 0;
        // let oneClick: number = this._oneClick ? 0 : 1;


        // this.homeCuentaModificarMedioPago(saldo, estacion, oneClick);
        // // if (oneClick !== 1) 
        // //     this.homeCuentaModificarMedioPago(saldo, estacion, oneClick);
        // // else
        // //     this.idCompartido = "";
    }

    homeCuentaModificarMedioPago(saldo: number, estacion: number, oneClick: number) {
        let compartida: Tarjeta = new Tarjeta();
        if (this.idCompartido !== "")
            compartida = this.tarjetas.filter(r => r.idInscripcion === this.idCompartido)[0];
        else
            compartida = this.tarjetas.length > 0 ? this.tarjetas[0] : null;

        this.utils.showSpinner("Interno");
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                formaPagoSaldo: saldo,
                formaPagoEstacion: estacion,
                formaPagoOneclick: oneClick,
                tarjetaCompartidaId: compartida != null ? compartida.idInscripcion : '',
                tarjetaCompartidaUltimos4Digitos: compartida != undefined ? compartida.ultimos4Digitos : '',
                tarjetaCompartidaTipo: compartida != undefined ? compartida.tipoTarjetaCredito : '',
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            }
        };


        this.rx.mediosPagoModificarMedioPago(action).then(result => {
            this.utils.hideSpinner("Interno");
            this.ngOnInit();
            //this.tarjeta = this.tarjetas.filter(r => r.idInscripcion === this.idCompartido)[0];
        }, this.errorCallback);
    }

    nvaTarjeta() {
        this.utils.showSpinner("Interno");

        this.rx.mediosPagoNuevaTarjeta(this.variablesApi.home.cuentaSeleccionada.cuentaId).then((result: any) => {
            window.location.href = result.data.url;
        }, this.errorCallback);
    }

    // seleccionaTarjeta() {
    //     // this.idCompartido = idInscripcion;
    //     if (this.idCompartido !== "") {
    //         let saldo: number = this._billetera ? 1 : 0;
    //         let estacion: number = this._atendedor ? 1 : 0;
    //         let oneClick: number = this._oneClick ? 1 : 0;
    //         this.homeCuentaModificarMedioPago(saldo, estacion, oneClick);
    //     }
    // }

    // seleccionaTarjeta(item:Tarjeta){
    //     this.idCompartido  = item.idInscripcion;
    //     let saldo: number = this._billetera ? 1 : 0;
    //     let estacion: number = this._atendedor ? 1 : 0;
    //     let oneClick: number = this._oneClick ? 1 : 0;
    //     this.homeCuentaModificarMedioPago(saldo, estacion, oneClick);     
    // }

    muestraSeleccionTarjeta() {
        this.emiter.emitChange({
            tipo: 'SeleccionTarjeta',
            tarjetas: this.tarjetas,
            atendedor: this._atendedor,
            billetera: this._billetera,
            oneClick: this._oneClick

        });
    }

    eliminaTarjeta(idInscripcion) {
        let tarjeta = this.tarjetas.filter(r => r.idInscripcion == idInscripcion)[0];
        this.utils.msgPlainModal('Eliminar Tarjeta', ['¿Esta seguro que desea eliminar esta tarjeta?', tarjeta.tipoTarjetaCredito + " - XXXX XXXX XXXX " + tarjeta.ultimos4Digitos]).then(result => {
            if (result == OpcionesModal.ACEPTAR)
                this.maquinaEliminarMedioPago(idInscripcion)
        })
    }

    abonar() {
        if (this.abonoBilletera.monto === null || this.abonoBilletera.tarjeta === null)
            this.utils.msgPlainModalAccept('Datos Faltantes', ['Para Abonar necesita seleccionar una tarjeta e indicar un monto']);
        else {
            let monto = this.abonoBilletera.monto.replace(/\./g, "");
            this.utils.msgPlainModal('Confirmar transferencia', [`¿Está seguro que desea transferir $${this.numberPipe.transform(monto, '1.0-0')} desde la tarjeta **** ${this.abonoBilletera.tarjeta.ultimos4Digitos} a tu billetera Muevo?`]).then((result: OpcionesModal) => {
                if (result == OpcionesModal.ACEPTAR)
                    this.registrarAbonoPost(this.abonoBilletera.tarjeta.idInscripcion, parseInt(monto))
            });

            //this.router.navigate([Paginas.HomeMuevoAbonarProcesando, { valorMonto: parseInt(this.abonoBilletera.monto.replace(/\./g, "")), idInscripcion: this.abonoBilletera.tarjeta.idInscripcion }]);

        }
    }

    registrarAbonoPost(idTarjeta: string, monto: number) {

        let model: IAbonoPost = {
            cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            idTarjeta: idTarjeta,
            monto: monto,
        }

        let modalProcess = this.modalService.open(AbonoProcesarComponent, { backdrop: 'static', centered: true, size: 'sm' });

        this.rx.mediosPagoAbonoPost(model).then((result) => {
            modalProcess.close();

            this.abonoBilletera = {
                monto: null,
                tarjeta: null
            }

            let modalSuccess = this.modalService.open(AbonoExitoComponent, { backdrop: 'static', centered: true, size: 'sm' });
            modalSuccess.result.then((result) => {
                this.ngOnInit();
            });

        }, error => {
            modalProcess.close();

            let defaultMsg = 'Error al procesar el abono';
            let message = '';
            if (error.estado != undefined && (error.estado == Respuestas.RESPUESTA_NOK || error.estado == Respuestas.RESPUESTA_SC))
                message = error.data != undefined ? error.data.message || defaultMsg : 'Problemas al ejecutar el servicio, Reintente!'
            else
                message = defaultMsg


            let modalError = this.modalService.open(AbonoErrorComponent, { backdrop: 'static', centered: true, size: 'sm' });
            modalError.componentInstance.errorMessage = message;
        });

    }

    maquinaEliminarMedioPago(idInscripcion) {
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        let compartida = this.tarjeta;

        this.utils.showSpinner("Interno");
        this.rx.mediosPagoEliminarMedioPago(cuentaId, idInscripcion).then(result => {
            this.utils.hideSpinner("Interno");
            this.ngOnInit();
        }, this.errorCallback)
    }

    desasociarCtaCte(cc: CuentaCorriente) {
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
                inscripcionId: cc.inscripcionId
            }
        };

        this.rx.mediosPagoDesasociarCtaCte(action).then(result => {
            this.ngOnInit();
        }, this.errorCallback)
    }

    openAbonoSeleccion() {

        this.emiter.emmitChange({
            tipo: 'MetodosPago_Abono_SeleccionMedio',
            action: 'OPEN',
            data: {
                cuentasCorrientes: this.utils.clone(this.cuentasCorrientes)
            }
        });
    }

    private mostrarCuentasBancarias() {
        let config: IEmmitModel = {
            tipo: 'MetodosPago_Abono_CuentasBancarias',
            action: this.CURRENT_PAGE,
            data: {
                selectionMode: false
            }
        }

        this.emiter.emitChange(config);
    }

    private mostrarTarjetasBancarias() {
        let config: IEmmitModel = {
            tipo: 'MetodosPago_Abono_Tarjeta',
            action: this.CURRENT_PAGE,
            data: {
                selectionMode: false
            }
        }

        this.emiter.emitChange(config);
    }

    obtenerAbonos(posicionInicial: number) {
        var body = { "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId };
        if (posicionInicial != null) {
            body["posicionInicial"] = posicionInicial;
        }
        this.utils.showSpinner("Interno");
        this.rx.llamarPOSTHome(body, this.constantes.CUENTA_CONSULTAR_ABONOS).then(result => {

            var modal = this.modalSidebar.open(RetirarAbonoComponent)
            modal.refModal.componentInstance.abonos = result.data.data
            this.utils.hideSpinner("Interno");
        }, this.errorCallback)

    }
}

export enum FormasPagoTab {
    Billetera = 'billetera',
    Atendedor = 'atendedor',
    Tarjetas = 'tarjetas'
}
