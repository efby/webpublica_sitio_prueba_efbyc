import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { CuentaCorriente } from 'src/app/globales/business-objects';
import { Paginas } from 'src/app/globales/paginas';

@Component({
    selector: 'app-seleccion-medio',
    templateUrl: './seleccion-medio.component.html',
    styleUrls: ['./seleccion-medio.component.scss']
})
export class SeleccionMedioComponent implements OnInit {
    
    CURRENT_PAGE = Paginas.ModalMediosPagoSeleccionMedio;
    cuentasCorrientes: CuentaCorriente[];

    constructor(private emiter: BreadCrumbsService) { }
    
    ngOnInit() {
        this.reset();
    }

    private reset(){
        this.cuentasCorrientes = [];
    }

    main(cuentasCorrientes:CuentaCorriente[]){
        this.reset();

        this.cuentasCorrientes = cuentasCorrientes;
    }
    
    abonoPorCtaBancaria(){
        this.emiter.emmitChange({
            tipo: 'MetodosPago_Abono_CuentasBancarias',
            action: this.CURRENT_PAGE,
            data: {
                selectionMode: true
            }
        });
    }
    
    abonoPorTarjeta(){
        this.emiter.emmitChange({
            tipo: 'MetodosPago_Abono_Tarjeta',
            action: this.CURRENT_PAGE,
            data: {
                selectionMode: true
            }
        });
    }
}
