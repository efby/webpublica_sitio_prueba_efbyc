import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { CuentaCorriente } from 'src/app/globales/business-objects';
import { BreadCrumbsService, IEmmitModel, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../../pageBase';
import { Router } from '@angular/router';
import { Paginas } from 'src/app/globales/paginas';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SidebarService } from 'src/app/services/sidebar-services';
import { Constantes } from 'src/app/globales/constantes';
import { OpcionesModal } from 'src/app/globales/enumeradores';

@Component({
    selector: 'app-cuentas-bancarias',
    templateUrl: './cuentas-bancarias.component.html',
    styleUrls: ['./cuentas-bancarias.component.scss']
})
export class CuentasBancariasComponent extends PageBase implements OnInit {

    @Input() id: string;
    CURRENT_PAGE: string = Paginas.ModalMediosPagoCuentasBancarias;
    cuentasCorrientes: CuentaCorriente[];

    modoAbono: boolean;
    origin: string;

    private unsubscribe = new Subject();
    callbackSubscripcion: any;

    isRoot: boolean;



    constructor(
        private modalSidebar: SidebarService,
        private constantes: Constantes,
        public ref: ChangeDetectorRef, public router: Router, public utils: UtilitariosService, private emiter: BreadCrumbsService, public rx: RxService, public variablesApi: VariablesGenerales) {
        super(ref, utils, router);
    }

    ngOnInit() {
        this.reset();
        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);
    }

    main(modoAbono: boolean, origin: string) {
        this.reset();


        this.modoAbono = modoAbono;
        this.origin = origin;

        this.isRoot = this.origin === Paginas.HomeMuevoMisMetodosPago;

        this.obtenerCuentasBancarias();
    }

    private emitterHandler = (data: IEmmitCallbackModel) => {
        if (data.from == Paginas.ModalMediosPagoCrearCuentasBancarias) {
            this.obtenerCuentasBancarias();
        }
    }

    private obtenerCuentasBancarias() {
        this.utils.showSpinner("Interno");
        this._isLoading = true;
        this.rx.mediosPagoGet(this.variablesApi.home.cuentaSeleccionada.cuentaId).then((result: any) => {
            this.cuentasCorrientes = result.data.cuentasCorrientes;

            this._isLoading = false;
            this.utils.hideSpinner("Interno");
        }, this.errorCallback);
    }

    nvaCta() {
        let config: IEmmitModel = {
            tipo: 'CuentaCorriente',
            action: 'OPEN',
            data: {
                origin: this.CURRENT_PAGE
            }
        }
        this.emiter.emmitChange(config);
    }

    private mostrarInfoBancaria() {
        this.emiter.emmitChange({
            tipo: 'MetodosPago_Abono_InfoBancaria',
            action: 'OPEN',

        });
    }


    private reset() {
        this.cuentasCorrientes = [];
        this.modoAbono = true;
        this.origin = '';
        this.isRoot = false;
    }

    closeModals() {
        event.preventDefault();
        event.stopPropagation();

        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarAbonoCuentasBancarias', 'sideBarAbonoSeleccionMedio'],
            }
        }

        this.emiter.emmitChange(config);
    }

    ngOnDestroy() {
        if (this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }


    closeThis() {
        this.modalSidebar.close(this.id);
    }

    desasociarCtaCte(c: CuentaCorriente) {

        let msg = {
            title: 'Desasociar cuenta bancaria',
            messages: ["Ya no podremos reconocer los abonos que realices a tu billetera desde esta cuenta bancaria. ¿Estás seguro que deseas eliminarla?."],
            okText: "Eliminar"
        }

        this.utils.openModal(Paginas.ModalAceptarCancelarComponent, msg
        ).then(result => {
            switch (result) {
                case OpcionesModal.ACEPTAR:
                    var body = {
                        "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
                        "inscripcionId": c.inscripcionId,
                    };
                    this.utils.showSpinner("Interno");
                    this.rx.llamarPOSTHome(body, this.constantes.DESASOCIAR_CUENTA_CORRIENTE).then(result => {
                        this.obtenerCuentasBancarias()
                    }, this.errorCallback)
                    break;
                case OpcionesModal.CANCELAR:
                    console.log("NOK")
                    // this.closeThis()
                    break;
            }

        });


    }


}
