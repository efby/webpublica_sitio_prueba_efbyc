import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PageBase } from '../../../pageBase';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RxService } from 'src/app/servicios/rx.service';
import { Tarjeta } from 'src/app/globales/business-objects';
import { BreadCrumbsService, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';

@Component({
    selector: 'app-tarjeta-abono',
    templateUrl: './tarjeta-abono.component.html',
    styleUrls: ['./tarjeta-abono.component.scss']
})
export class TarjetaAbonoComponent extends PageBase implements OnInit {
    
    tarjetas: Tarjeta[];
    tarjetaSeleccionada: Tarjeta;
    saldo: number;
    modoAbono:boolean;
    origin:string;
    isRoot:boolean;

    constructor(public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router, public variablesApi:VariablesGenerales, private rx:RxService, private emiter:BreadCrumbsService) {
        super(ref, utils, router);
    }
    
    ngOnInit() {
        this.reset();
    }

    main(modoAbono:boolean, origin:string){
        this.reset();

        this.modoAbono = modoAbono;
        this.origin = origin;

        this.isRoot = this.origin === Paginas.HomeMuevoMisMetodosPago;

        this.obtenerTarjetas();
    }

    obtenerTarjetas() {
        this.utils.showSpinner("Interno");
        this._isLoading = true;
        this.rx.mediosPagoGet(this.variablesApi.home.cuentaSeleccionada.cuentaId).then((result: any) => {

            this.tarjetas = result.data.tarjetas;
            this.saldo = result.data.saldo.saldoFinal;

            this._isLoading = false;
            this.utils.hideSpinner("Interno");
        }, this.errorCallback);
    }
    
    private reset(){
        this.tarjetaSeleccionada = undefined;
        this.tarjetas = [];
        this.saldo = 0;
        this.modoAbono = true;
    }

    private mostrarPantallaAbono(){
        let config: IEmmitModel = {
            tipo: 'MetodosPago_Abono_Tarjeta_Abonar',
            action: 'OPEN',
            data: {
                tarjeta: this.tarjetaSeleccionada,
                saldo: this.saldo
            }
        };

        this.emiter.emmitChange(config);
    }

    nvaTarjeta(){
        this.utils.showSpinner("Interno");

        this.rx.mediosPagoNuevaTarjeta(this.variablesApi.home.cuentaSeleccionada.cuentaId).then((result:any) => {
            window.location.href = result.data.url;
        }, this.errorCallback);
    }

    private eliminaTarjeta(idInscripcion) {
        let tarjeta = this.tarjetas.filter(r => r.idInscripcion == idInscripcion)[0];
        this.utils.msgPlainModal('Eliminar Tarjeta', ['¿Está seguro que desea eliminar esta tarjeta?', tarjeta.tipoTarjetaCredito + " - XXXX XXXX XXXX " + tarjeta.ultimos4Digitos]).then(result => {
            if(result == OpcionesModal.ACEPTAR)
                this.maquinaEliminarMedioPago(idInscripcion)
        })
    }

    private maquinaEliminarMedioPago(idInscripcion) {
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;

        this.utils.showSpinner("Interno");
        this.rx.mediosPagoEliminarMedioPago(cuentaId, idInscripcion).then(result => {
            this.utils.hideSpinner("Interno");
            this.obtenerTarjetas();
        }, this.errorCallback)
    }


    closeModals(){
        event.preventDefault();
        event.stopPropagation();
        
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarAbonoTarjeta', 'sideBarAbonoSeleccionMedio'],
            }
        }

        this.emiter.emmitChange(config);
    }

}
