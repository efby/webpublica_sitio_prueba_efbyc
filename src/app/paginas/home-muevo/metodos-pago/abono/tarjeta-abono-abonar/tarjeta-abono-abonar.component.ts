import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Tarjeta, IAbonoPost } from 'src/app/globales/business-objects';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { PageBase } from '../../../pageBase';
import { DecimalPipe } from '@angular/common';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Respuestas } from 'src/app/globales/respuestas';
import { AbonoErrorComponent } from '../../abono-error/abono-error.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbonoProcesarComponent } from '../../abono-procesar/abono-procesar.component';
import { RxService } from 'src/app/servicios/rx.service';
import { AbonoExitoComponent } from '../../abono-exito/abono-exito.component';
import { BreadCrumbsService, IEmmitCallbackModel, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { takeUntil } from 'rxjs/operators';
import { Paginas } from 'src/app/globales/paginas';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-tarjeta-abono-abonar',
    templateUrl: './tarjeta-abono-abonar.component.html',
    styleUrls: ['./tarjeta-abono-abonar.component.scss'],
    providers: [DecimalPipe]
})
export class TarjetaAbonoAbonarComponent extends PageBase implements OnInit {

    CURRENT_PAGE = Paginas.ModalTarjetaAbonoAbonar;

    current: {
        amount: number
    }

    tarjeta: Tarjeta;
    saldo: number;

    unsubscribe = new Subject();
    callbackSubscripcion: any;

    constructor(private emitter: BreadCrumbsService, public ref: ChangeDetectorRef, public utils: UtilitariosService, public router: Router, private numberPipe: DecimalPipe, private variablesApi: VariablesGenerales, private modalService: NgbModal, private rx: RxService, private emmiter: BreadCrumbsService) {
        super(ref, utils, router);
    }

    ngOnInit() {
        this.reset();
    }

    private reset() {
        this.tarjeta = undefined;
        this.saldo = 0;
        this.current = {
            amount: 0
        }
    }

    main(tarjeta: Tarjeta, saldo: number) {
        this.callbackSubscripcion = this.emmiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);

        this.reset();

        this.saldo = saldo;
        this.tarjeta = tarjeta || undefined;
    }

    transformAmount(event: any) {
        this.utils.validaMaxInputCurrency(event);
    }

    maskCurrency(event: any) {
        let item = event.srcElement;
        item.value = this.utils.formatCurrency(item.value);
    }

    abonar() {
            debugger
        if (this.current.amount === null || this.tarjeta === null)
            this.utils.msgPlainModalAccept('Datos Faltantes', ['Para Abonar necesita seleccionar una tarjeta e indicar un monto']);
        else if (this.current.amount !== null && this.current.amount == 0)
            this.utils.msgPlainModalAccept('Datos Faltantes', ['Ingrese un monto válido']);
        else {
            let monto = this.current.amount.toString().replace(/\./g, "");
            this.utils.msgPlainModal('Confirmar transferencia', [`¿Está seguro que desea transferir $${this.numberPipe.transform(monto, '1.0-0')} desde la tarjeta **** ${this.tarjeta.ultimos4Digitos} a tu billetera Muevo?`]).then((result: OpcionesModal) => {
                if (result == OpcionesModal.ACEPTAR)
                    this.registrarAbonoPost(this.tarjeta.idInscripcion, parseInt(monto))
            });
        }
    }

    private registrarAbonoPost(idTarjeta: string, monto: number) {

        let model: IAbonoPost = {
            cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
            idTarjeta: idTarjeta,
            monto: monto,
        }

        let modalProcess = this.modalService.open(AbonoProcesarComponent, { backdrop: 'static', centered: true, size: 'sm' });

        this.rx.mediosPagoAbonoPost(model).then((result: any) => {
            modalProcess.close();

            this.current = {
                amount: 0
            }

            console.log(result)

            if (result.data.estadoCierre == "OK") {
                let modalSuccess = this.modalService.open(AbonoExitoComponent, { backdrop: 'static', centered: true, size: 'sm' });

                modalSuccess.result.then((result) => {
                    this.ngOnInit();
                });
            } else {



                let modalError = this.modalService.open(AbonoErrorComponent, { backdrop: 'static', centered: true, size: 'sm' });
                modalError.componentInstance.errorMessage = result.data.userMessage;
            }



        }, error => {
            modalProcess.close();

            let defaultMsg = 'Error al procesar el abono';
            let message = '';
            if (error.estado != undefined && (error.estado == Respuestas.RESPUESTA_NOK || error.estado == Respuestas.RESPUESTA_SC))
                message = error.data != undefined ? error.data.message || defaultMsg : 'Problemas al ejecutar el servicio, Reintente!'
            else
                message = defaultMsg


            let modalError = this.modalService.open(AbonoErrorComponent, { backdrop: 'static', centered: true, size: 'sm' });
            modalError.componentInstance.errorMessage = message;
        });

    }

    private emitterHandler = (data: IEmmitCallbackModel) => {
        if (data.from == Paginas.ModalAbonoExito && data.action == this.CURRENT_PAGE && this.variablesApi.ultimaPagina == Paginas.HomeMuevoMisMetodosPago) {
            this.closeThis();
            this.router.navigate([Paginas.HomeMuevoMisMetodosPago]);
        }
        else if (data.from == Paginas.ModalAbonoExito && data.action == this.CURRENT_PAGE && this.variablesApi.ultimaPagina == Paginas.HomeMuevo) {
            this.closeThis();
            this.router.navigate([Paginas.HomeMuevo]);

            // let config:IEmmitCallbackModel = {
            //     from: this.CURRENT_PAGE,
            //     action: 'REFRESH',
            // };
            // this.emitter.emitCallback(config);
        }
    }

    ngOnDestroy() {
        this.unsubscribeEvents();
    }

    unsubscribeEvents() {
        if (this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }

    closeThis() {
        this.unsubscribeEvents();

        //cierra modal
        let config: IEmmitModel = {
            tipo: 'CERRAR MODAL REMOTO',
            action: 'CLOSE',
            data: {
                sidebars: ['sideBarTarjetaAbonoAbonar']
            }
        }
        this.emmiter.emitChange(config);
    }

}
