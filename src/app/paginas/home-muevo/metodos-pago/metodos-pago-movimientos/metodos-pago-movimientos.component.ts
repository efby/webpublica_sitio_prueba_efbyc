import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { PageBase } from '../../pageBase';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MedioPagoMovimiento, TableDataCsv, IPagination } from 'src/app/globales/business-objects';
import { ExportDataService } from 'src/app/servicios/export-data.service';

import * as moment from 'moment';
import 'moment/min/locales';
moment.locale('es')

@Component({
    selector: 'app-metodos-pago-movimientos',
    templateUrl: './metodos-pago-movimientos.component.html',
    styleUrls: ['./metodos-pago-movimientos.component.scss']
})
export class MetodosPagoMovimientosComponent extends PageBase implements OnInit {
    
    movimientos: MedioPagoMovimiento[];

    displayedColumns = [ 'movimientoFechaCreacion', 'tipoMovimientoNombre', 'movimientoMonto', 'movimientoSaldoFinal' ]
    dataSource: MatTableDataSource<MedioPagoMovimiento> = new MatTableDataSource<MedioPagoMovimiento>();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    migajas = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Formas de Pago", class: "active", url: '/' + Paginas.HomeMuevoMisMetodosPago + '/M' },
        { title: "Movimientos", class: "active", url: '' }
    ];

    pagination: IPagination;
    
    constructor(public ref: ChangeDetectorRef, public router:Router, public utils: UtilitariosService, private emiter: BreadCrumbsService, private rx: RxService, private variablesApi:VariablesGenerales) {
        super(ref, utils, router); 

        this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
        this.movimientos = [];

        this.pagination = {
            currentPage: 0,
            pages: 0,
            totalRows: 0, 
            positionFinal: 0,
            pagesSize: 0,
            rows: 0
        }
    }
    
    ngOnInit() {
        this.obtenerMovimientos();
    }

    exportCsv(){
        
        let data: TableDataCsv = {
            header:  [ 'FECHA', 'TIPO', 'MONTO', 'DETALLE', 'SALDO FINAL' ],
            rows: this.dataSource.data.map((x:MedioPagoMovimiento) => { 
                return [
                    moment(x.movimientoFechaCreacion).format('DD-MM-YYYY HH:mm'),
                    x.tipoMovimientoNombre || '',
                    x.movimientoMonto || 0,
                    'pendiente',
                    x.movimientoSaldoFinal || 0
                ]
            })
        };

        ExportDataService.ExportCsv(this.generateFilenameFormat('movimientos_', this.variablesApi.home.cuentaSeleccionada.cuentaId.toString()), data.rows, data.header, ';', ',', 'Movimientos MUEVO EMPRESAS')
    }

    private obtenerMovimientos(){
        let cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
        this._isLoading = true;
        //this.utils.showSpinner("Interno");
        this.rx.mediosPagoMovimientosGet(cuentaId, this.pagination.positionFinal).then((result:any) => {

            result

            this.movimientos = this.movimientos.concat(result.data.movimientos);
            this.movimientos.forEach(a=>{
                a.movimientoFechaCreacion=this.convertDateToISOString( a.movimientoFechaCreacion)
            })
            this.bindTableWithtData(this.movimientos);
            this._isLoading = false;

            //this.utils.hideSpinner("Interno");
            this.paginationHandler(result.data.pagination)
        }, this.errorCallback);
    }

    private bindTableWithtData(data:MedioPagoMovimiento[]){
        this.dataSource = new MatTableDataSource(data);
        this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);
    }

    private generateFilenameFormat(prefix:string, key:string){
        return prefix + `${key}_${moment().format('YYYYMMDD_HHmm')}`;
    }

    private paginationHandler(configResponse:any){

        this.pagination.positionFinal = configResponse.posicionFinal;
        this.pagination.totalRows = configResponse.totalRegistros;
        this.pagination.pagesSize = configResponse.registrosPorPagina;
        this.pagination.rows = configResponse.registrosObtenidos;

    }
    

    next(){
        this.obtenerMovimientos();
    }
}
