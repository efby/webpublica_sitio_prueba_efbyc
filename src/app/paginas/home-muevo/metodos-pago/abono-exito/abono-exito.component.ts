import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BreadCrumbsService, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
    selector: 'app-abono-exito',
    templateUrl: './abono-exito.component.html',
    styleUrls: ['./abono-exito.component.scss']
})
export class AbonoExitoComponent implements OnInit {
    
    CURRENT_PAGE = Paginas.ModalAbonoExito;

    constructor(
        private rx:RxService,
        private variablesApi:VariablesGenerales,
        private modal: NgbActiveModal, private emiiter:BreadCrumbsService) { }
    
    ngOnInit() {
    }
    
    close(){
        this.modal.close();
        
        let config:IEmmitCallbackModel = {
            from: this.CURRENT_PAGE,
            action: Paginas.ModalTarjetaAbonoAbonar,
        };
        this.emiiter.emitCallback(config);

        this.rx.obtenerWeb().then((result: any) => {
            this.variablesApi.home.cuentaSeleccionada = result.data.cuentaSeleccionada;
            this.variablesApi.home.saldo = result.data.saldo;
            //console.log('cuenta seleccionada: ', this.variablesApi.home.cuentaSeleccionada.cuentaNombre)
            this.variablesApi.home.invitaciones = result.data.invitaciones;
            this.variablesApi.home.usuarioLogIn = result.data.usuarioLogin;
            this.variablesApi.home.vehiculos = result.data.vehiculos;
            this.variablesApi.home.conductores = result.data.conductores;
            this.variablesApi.home.usuarios = result.data.usuarios;
            this.variablesApi.home.facturasPendientesPago = result.data.facturasPendiente;
            this.variablesApi.home.primerIngreso = result.data.primerIngreso;
            this.variablesApi.home.montoTotalMes = result.data.montoTotalMes;
            this.variablesApi.home.tipoFormaPago = result.data.tiposFormaPago;
            this.variablesApi.home.menu = result.data.menu;
            this.variablesApi.home.cuentas = result.data.cuentas;
            this.variablesApi.home.mediosPago = result.data.mediosPago;
            this.variablesApi.home.datosSaldo = result.data.datosSaldo;
            this.variablesApi.home.tipoDocumento = result.data.tipoDocumento;
            this.variablesApi.home.cuentaDatosDte = result.data.cuentaDatosDte;
            this.variablesApi.home.notificaciones = result.data.notificaciones;
            this.variablesApi.home.fuerzaCambioCuenta = result.data.fuerzaCambioCuenta;
            this.variablesApi.home.rolPrivilegiosFront = result.data.rolPrivilegiosFront;
        })
    }
    
}
