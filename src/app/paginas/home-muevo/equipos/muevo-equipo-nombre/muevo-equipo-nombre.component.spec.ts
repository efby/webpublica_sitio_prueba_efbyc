import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoEquipoNombreComponent } from './muevo-equipo-nombre.component';

describe('MuevoEquipoNombreComponent', () => {
  let component: MuevoEquipoNombreComponent;
  let fixture: ComponentFixture<MuevoEquipoNombreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoEquipoNombreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoEquipoNombreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
