import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales, Equipos } from 'src/app/globales/variables-generales';
import { Router } from '@angular/router';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, OpcionesModal, NavegacionTipo } from 'src/app/globales/enumeradores';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-muevo-equipo-nombre',
  templateUrl: './muevo-equipo-nombre.component.html',
  styleUrls: ['./muevo-equipo-nombre.component.css']
})
export class MuevoEquipoNombreComponent implements OnInit {

  equipo: Equipos;
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private router: Router, private maquinaHome: MaquinaHome, private utils: UtilitariosService) { }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ tipo: "Configuracion", _isConfiguracion: true });
    });
    this.equipo = this.variablesApi.home.equipos;
    if (!this.equipo.nuevaCuenta) {
      this.equipo.nombreEquipo = this.variablesApi.home.cuentaSeleccionada.cuentaNombre
    }
  }

  configurarEquipo() {
    if (this.equipo.nuevaCuenta) {
      this.homeMaquinaCrearCuenta();
    } else {
      this.homeMaquinaModificarNombre();
    }
  }

  homeMaquinaCrearCuenta() {
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaNombre: this.equipo.nombreEquipo
      }
    };
    this.maquinaHome.homeCrearCuentaEmpresa(action).then(respuesta => {

      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.seleccionarCuenta(respuesta.data.cuentaId);
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  seleccionarCuenta(id: number) {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: id,
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        usuarioId: this.variablesApi.home.usuarioLogIn.usuarioId
      }
    };

    this.maquinaHome.homeCambiarCuentaSeleccionada(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.usuarioObtener();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }

    });

  }

  homeMaquinaModificarNombre() {
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        cuentaNombre: this.equipo.nombreEquipo
      }
    };
    this.maquinaHome.homeModificarCuentaEmpresa(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.usuarioObtener();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  usuarioObtener() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_INICIO,
      datos: {
        telefonoId: '',
        usuarioId: ''
      }
    };

    this.maquinaHome.homeInicio(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.router.navigate([Paginas.HomeMuevoMisMetodosPago, 'N']);
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });

  }

  maxInput(ingreso: any) {
    let item = ingreso.srcElement;
    item.value = this.utils.formatMaxInput(item.value, item.maxLength);
  }

}
