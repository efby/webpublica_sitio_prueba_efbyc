import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoEquipoSeleccionaCuentaComponent } from './muevo-equipo-selecciona-cuenta.component';

describe('MuevoEquipoSeleccionaCuentaComponent', () => {
  let component: MuevoEquipoSeleccionaCuentaComponent;
  let fixture: ComponentFixture<MuevoEquipoSeleccionaCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoEquipoSeleccionaCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoEquipoSeleccionaCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
