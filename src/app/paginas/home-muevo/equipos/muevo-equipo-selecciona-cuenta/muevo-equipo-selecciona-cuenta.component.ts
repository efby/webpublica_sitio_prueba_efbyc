import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales, Equipos } from 'src/app/globales/variables-generales';
import { Router } from '@angular/router';
import { Paginas } from 'src/app/globales/paginas';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-muevo-equipo-selecciona-cuenta',
  templateUrl: './muevo-equipo-selecciona-cuenta.component.html',
  styleUrls: ['./muevo-equipo-selecciona-cuenta.component.css']
})
export class MuevoEquipoSeleccionaCuentaComponent implements OnInit {

  nvaCuenta: boolean = false;
  equipo: Equipos;
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private router: Router, private utils: UtilitariosService) { }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ tipo: "Configuracion", _isConfiguracion: true });
    });
    this.equipo = this.variablesApi.home.equipos;
  }

  btnAvanzar() {
    this.variablesApi.home.equipos = this.equipo;
    this.router.navigate([Paginas.HomeMuevoEquipoNombre]);
  }
  maxInput(ingreso: any) {
    let item = ingreso.srcElement;
    item.value = this.utils.formatMaxInput(item.value, item.maxLength);
  }
}
