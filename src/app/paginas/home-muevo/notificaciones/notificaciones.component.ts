import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Constantes } from 'src/app/globales/constantes';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../pageBase';
import { ConfigNotificacionesComponent } from '../perfil/config-notificaciones/config-notificaciones.component';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.component.html',
  styleUrls: ['./notificaciones.component.scss']
})
export class NotificacionesComponent extends PageBase implements OnInit {


  @Input() id: string;
  posicionFinal = 0
  cantidadRegistrosPorConsulta = 0;
  isLeidaNotificaciones: boolean = false;
  notificaciones = new Array<any>();
  cuentaIdSelecionada = null;

  constructor(public activeModal: NgbActiveModal,
    public modalSidebar: SidebarService,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    public router: Router,
    public variablesApi: VariablesGenerales,
    private modalService: NgbModal,
    private rx: RxService,
    private constantes: Constantes,
  ) {
    super(ref, utils, router);
  }

  ngOnInit() {
    //this.leerNotificaciones()
    this.cuentaIdSelecionada = this.variablesApi.home.cuentaSeleccionada.cuentaId;
    this._isLoading = false
  }

  closeThis() {
    this.modalSidebar.close(this.id);

  }
  changeCuentaID(cuentaID: number) {
    this.cuentaIdSelecionada = cuentaID;
    this.posicionFinal = 0;
    this.isLeidaNotificaciones = false;
    this.leerNotificaciones();
  }

  leerNotificaciones(notificacionesLeidas?) {
    var body = {
      "cuentaId": this.cuentaIdSelecionada
    };
    if (this.posicionFinal != null) {
      body["posicionInicial"] = this.posicionFinal;
    }
    body["tipoConsulta"] = this.isLeidaNotificaciones ? "LEIDA" : "POR_LEER";

    if (notificacionesLeidas != null) {
      body["notificacionesLeidas"] = notificacionesLeidas;
    }
    this.utils.showSpinner("Interno")
    this.rx.llamarPOSTHome(body, this.constantes.kUsuarioConsultarNotificaciones).then(result => {
      this.notificaciones = result.data.data.notificaciones
      this.posicionFinal = result.data.data.posicionFinal
      this.cantidadRegistrosPorConsulta = result.data.data.cantidadRegistrosPorConsulta
      console.log(result.data)
      this.utils.hideSpinner("Interno");
    }, this.errorCallback)
  }
  next() {
    var body = {
      "cuentaId": this.cuentaIdSelecionada
    };
    if (this.posicionFinal != null) {
      body["posicionInicial"] = this.posicionFinal;
    }
    body["tipoConsulta"] = this.isLeidaNotificaciones ? "LEIDA" : "POR_LEER";


    this.utils.showSpinner("Interno")
    this.rx.llamarPOSTHome(body, this.constantes.kUsuarioConsultarNotificaciones).then(result => {
      this.notificaciones = this.notificaciones.concat(result.data.data.notificaciones)
      this.posicionFinal = result.data.data.posicionFinal
      this.cantidadRegistrosPorConsulta = result.data.data.cantidadRegistrosPorConsulta
      console.log(result.data)
      this.utils.hideSpinner("Interno");
    }, this.errorCallback)
  }
  mostrarLeidas() {
    this.notificaciones = []
    this.isLeidaNotificaciones = !this.isLeidaNotificaciones
    this.posicionFinal = 0
    this.leerNotificaciones();
  }
  marcarComoLeidas() {
    let limiteNotificaciones = 999;

    var notificacionesLeidas = Array<number>();
    let contador = 0
    this.notificaciones.forEach((element) => {
      contador++;
      if (element.notificacionId != null &&
        element.notificacionEstado == "POR_LEER" &&
        contador <= limiteNotificaciones) {
        notificacionesLeidas
          .push(element.notificacionId);
      }
    });
    if (notificacionesLeidas.length >
      0) {
      this.posicionFinal = 0;
      this.leerNotificaciones(notificacionesLeidas);
    }


  }
  marcarComoLeida(item) {

    console.log(item)
    var notificacionesLeidas = Array<number>();
    notificacionesLeidas
      .push(item.notificacionId);
    if (notificacionesLeidas.length >
      0) {
      this.posicionFinal = 0;
      this.leerNotificaciones(notificacionesLeidas);
    }


  }

  configurarNotificaciones() {

    var body = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
    }


    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kConsultarCondiguracionNotificaciones).then(result => {

      var mod = this.modalSidebar.open(ConfigNotificacionesComponent, { miBackdrop: false });
      mod.refModal.componentInstance.configuracionNotificaciones = result.data.data.configuracionNotificaciones
      mod.refModal.result.then(() => {
        //this.updateData()
      }, () => {
        //this.updateData()
      })
      this.utils.hideSpinner("Interno");
    }, this.errorCallback)


  }


}
