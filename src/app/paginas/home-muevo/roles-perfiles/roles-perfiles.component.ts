import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Conductor } from 'src/app/models/conductores';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Invitacion } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-roles-perfiles',
  templateUrl: './roles-perfiles.component.html',
  styleUrls: ['./roles-perfiles.component.css']
})
export class RolesPerfilesComponent implements OnInit {
  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Mis Roles y Perfiles", class: "active", url: "" }
  ];

  SuperAdmins?: any[] = [];
  Administradores?: any[] = [];
  Analistas?: any[] = [];
  invitados?: Invitacion[] = [];
  _invitadosPendientes: boolean = false;
  _navInvitados: boolean = false;

  interval: any;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, private router: Router, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private ref: ChangeDetectorRef) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });

    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.isMobile = this.variablesApi.esMobile;
      this.iniciaVariablesGlobales()
    }, 10);
    this.cargaInvitaciones();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.iniciaVariablesGlobales();
  }

  iniciaVariablesGlobales() {
    this.SuperAdmins = this.variablesApi.home.usuarios.filter(r => r.rolId === 1);
    this.Administradores = this.variablesApi.home.usuarios.filter(r => r.rolId === 2);
    this.Analistas = this.variablesApi.home.usuarios.filter(r => r.rolId === 3);

    this.invitados = this.variablesApi.home.invitados.filter(r => r.invitacionEstadoInvitado !== "ACEPTADA" && r.invitacionEstadoInvitado !== "NO_AUTORIZADA" && r.invitacionEstadoInvitado !== "INVALIDA" && r.invitacionEstadoInvitado !== "CANCELADA");
    if (this.invitados.length > 0) {
      this._invitadosPendientes = true;
      if (this.invitados.length > 3) {
        this._navInvitados = true;
        this.invitados = this.invitados.splice(0, 2);
      } else {
        this._navInvitados = false;
      }
    } else {
      this._invitadosPendientes = false;
      this._navInvitados = false;
    }
  }

  date2Human(date) {
    let format = 'dd/MM/yyyy';
    return this.utils.date2Human(date, format, this.router.url);
  }

  formatRut(rut: string) {
    let actual = rut.replace(/^0+/, "");
    let rutPuntos = "";
    if (actual != '' && actual.length > 1) {
      let sinPuntos = actual.replace(/\./g, "");
      let actualLimpio = sinPuntos.replace(/-/g, "");
      let inicio = actualLimpio.substring(0, actualLimpio.length - 1);
      let i = 0;
      let j = 1;
      for (i = inicio.length - 1; i >= 0; i--) {
        let letra = inicio.charAt(i);
        rutPuntos = letra + rutPuntos;
        if (j % 3 == 0 && j <= inicio.length - 1) {
          rutPuntos = "." + rutPuntos;
        }
        j++;
      }
      let dv = actualLimpio.substring(actualLimpio.length - 1);
      rutPuntos = rutPuntos + "-" + dv;
    }
    return rutPuntos;
  }

  traduceRol(idRol) {
    return this.variablesApi.home.maestros.roles.filter(r => r.rolId == idRol)[0].rolNombre;
  }

  cargaInvitaciones() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarUsuarios(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //this.ngOnInit();
        // }
        this.iniciaVariablesGlobales();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  cancelarInvitacion(invitacionId: number, invitacionAceptada: number) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        invitacionId: invitacionId
      }
    }

    this.maquinaHome.homeUsuarioCancelaInvitacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "", textoMensaje: "Se ha cancelado la Invitacion" });
        this.cargaInvitaciones();

        // }
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
          this.utils.hideSpinner("Interno");
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  reenviarInvitacion(invitacionId: number) {
    let datos = [];
    this.utils.showSpinner("Interno");
    let idInvitacion = this.invitados.findIndex(r => r.invitacionId === invitacionId);
    let actualUrl = this.router.url.substr(1);

    let data = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
      rolId: this.invitados[idInvitacion].invitacionRolId,
      invitacionTelefonoId: this.invitados[idInvitacion].invitacionTelefonoInvitadoId,
      invitacionNombre: this.invitados[idInvitacion].invitacionValidaciones.invitacionNombre,
      invitacionApellido: this.invitados[idInvitacion].invitacionValidaciones.invitacionApellido,
      invitacionSolicitaFoto: this.invitados[idInvitacion].invitacionValidaciones.invitacionSolicitaFoto,
      invitacionSolicitaLicencia: this.invitados[idInvitacion].invitacionValidaciones.invitacionSolicitaLicencia,
      estado: 1,
      montoMaximoDiaValor: this.invitados[idInvitacion].combustibleMontoDiario,
      montoMaximoMesValor: this.invitados[idInvitacion].combustibleMontoMensual
    }
    datos.push(data);


    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: datos
    }

    this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
        this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "", textoMensaje: "Se ha reenviado la Invitación" });
        this.cargaInvitaciones();
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.utils.hideSpinner("Interno");
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  mostrarUsuario(id, event) {
    event.preventDefault();
    event.stopPropagation();
    this.emiter.emitChange({
      tipo: 'Conductores',
      Conductores: true,
      idConductor: id
    });
  }

  modificaSuperAdmin(id, estado) {
    let usuario = this.SuperAdmins.filter(r => r.usuarioId === id)[0];
    this.maquinaModificarConductor(usuario, estado);
  }
  modificaAdministrador(id, estado) {
    let usuario = this.Administradores.filter(r => r.usuarioId === id)[0];
    this.maquinaModificarConductor(usuario, estado);
  }
  modificaAnalista(id, estado) {
    let usuario = this.Analistas.filter(r => r.usuarioId === id)[0];
    this.maquinaModificarConductor(usuario, estado);
  }

  maquinaModificarConductor(usuario: Conductor, estado: number) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        usuarioId: usuario.usuarioId,
        //grupoCuentaId: (usuario.grupoId == null) ? 0 : usuario.grupoId,
        usuarioCuentaEstadoId: usuario.estado ? 1 : 0,
        rolId: usuario.rolId,
        usuarioCuentaConduce: estado,
        //usuarioCuentaRindeGasto: usuario.usuarioCuentaRindeGasto,
        // usuarioCuentaCreaVehiculo: usuario.usuarioCuentaCreaVehiculo,
        // usuarioCuentaEligeDTE: usuario.usuarioCuentaEligeDTE,
        // usuarioCuentaSeleccionado: usuario.usuarioCuentaSeleccionado,
        vehiculosAsignados: [],
        estadoCombustible: usuario.estado
      }
    }

    this.maquinaHome.homeUsuarioModificar(action, 1).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  inviteSuperAdmin() {
    this.emiter.emitChange({
      tipo: 'InvitaUsuario',
      inviteUsuario: true,
      idRol: 1
    });
  }
  inviteAdmin() {
    this.emiter.emitChange({
      tipo: 'InvitaUsuario',
      inviteUsuario: true,
      idRol: 2
    });
  }
  inviteAnalist() {
    this.emiter.emitChange({
      tipo: 'InvitaUsuario',
      inviteUsuario: true,
      idRol: 3
    });
  }

  activaDesactivaAdmin(id: number) {
    let usuario = this.Administradores.filter(r => r.usuarioId === id)[0];
    if (usuario.estado) {
      this.maquinaModificarEstadoConductor(usuario, 0);
    } else {
      this.maquinaModificarEstadoConductor(usuario, 1);
    }
  }

  activaDesactivaAnalista(id: number) {
    let usuario = this.Analistas.filter(r => r.usuarioId === id)[0];
    if (usuario.estado) {
      this.maquinaModificarEstadoConductor(usuario, 0);
    } else {
      this.maquinaModificarEstadoConductor(usuario, 1);
    }
  }

  maquinaModificarEstadoConductor(usuario: Conductor, estado: number) {
    let conductor = usuario;
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        usuarioId: conductor.usuarioId,
        //grupoCuentaId: (conductor.grupoId == null) ? 0 : conductor.grupoId,
        usuarioCuentaEstadoId: estado,
        rolId: conductor.rolId,
        usuarioCuentaConduce: conductor.conduce,
        //usuarioCuentaRindeGasto: conductor.usuarioCuentaRindeGasto,
        //usuarioCuentaCreaVehiculo: conductor.usuarioCuentaCreaVehiculo,
        //usuarioCuentaEligeDTE: conductor.usuarioCuentaEligeDTE,
        //usuarioCuentaSeleccionado: conductor.usuarioCuentaSeleccionado,
        vehiculosAsignados: []
      }
    }
    let confirma: boolean = false;
    if (estado == 2) {
      confirma = this.utils.msgPlain({ tipoMensaje: "confirm", tituloMensaje: "Confirmacion", textoMensaje: "Va a eliminar un conductor de manera definitiva \n\n ¿Está seguro que desea eliminarlo?" });
    } else {
      confirma = true
    }

    if (confirma) {
      this.utils.showSpinner("Interno");
      this.maquinaHome.homeUsuarioModificar(action, 1).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          this.ngOnInit();
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            console.log(response);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.utils.msgPlain(respuesta.mensajeSalida);

        }
      });
    }
  }

}
