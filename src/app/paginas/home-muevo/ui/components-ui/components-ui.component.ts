import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';

@Component({
  selector: 'app-components-ui',
  templateUrl: './components-ui.component.html',
  styleUrls: ['./components-ui.component.scss']
})
export class ComponentsUiComponent implements OnInit {

  migajas = [
    { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Dev", class: "active", url: "" }
  ];

  current: {
    estado: boolean
  }

  constructor(private emiter:BreadCrumbsService) { }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });

    this.current = {
      estado: true
    }
  }

}
