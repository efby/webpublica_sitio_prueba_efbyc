import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService, IEmmitCallbackModel, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';
import { takeUntil } from 'rxjs/operators';
import { SidebarService } from 'src/app/services/sidebar-services';
import { v4 as uuidv4 } from 'uuid';
import { fadeOutToRight } from 'src/app/animations/fade-right';
import { Sidebar } from 'ng-sidebar';
import { Perfil, UsuarioLogin } from 'src/app/globales/business-objects';
import { ModificarNombreComponent } from './modificar-nombre/modificar-nombre.component';
import { ModificarMailComponent } from './modificar-mail/modificar-mail.component';


@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.css'],
  //entryComponents:[ModificarNombreComponent]

})
export class EditarPerfilComponent extends PageBase implements OnInit, OnChanges {


  @Input() id: string;

  usuarioLogin: UsuarioLogin;
  current: Perfil;

  sidebar = {
    animated: false,
    opened: false,
    position: 'left',
    mode: 'push', // 'over|push'
    closeOnClickOutside: false,
    enabled: true
  }
  constructor(

    public activeModal: NgbActiveModal,
    public modalSidebar: SidebarService,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    public router: Router,
    private variablesApi: VariablesGenerales,
    private modalService: NgbModal,
    private rx: RxService
  ) {
    super(ref, utils, router);
    this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
    this.current = {
      nombre: this.usuarioLogin.usuarioNombre,
      apellido: this.usuarioLogin.usuarioApellido,
      email: this.usuarioLogin.usuarioEmail,
      celular: this.usuarioLogin.usuarioTelefono,
      rut: this.usuarioLogin.usuarioRut
    }

  }
  ngOnChanges(changes: SimpleChanges) {
    console.log("Cambios Parent")

  }

  ngOnInit() {
    var sideBar = {
      id: EditarPerfilComponent.name,
      fnClose: () => {
        this.closeThis();
      }
    };
    this.modalSidebar.add(sideBar)
  }

  pruebaFN() {
    console.log(this.current)
  }
  fnNombre() {
    console.log(this.current.nombre)
  }
  fnMail() { console.log(this.current.email) }


  closeThis() {
    this.modalSidebar.close(this.id);
  }


  modificarNombre() {
    var mod = this.modalSidebar.open(ModificarNombreComponent, { miBackdrop: false })
   
  }
  modificarMail() {
    this.modalSidebar.open(ModificarMailComponent, { miBackdrop: false })
  }

}
