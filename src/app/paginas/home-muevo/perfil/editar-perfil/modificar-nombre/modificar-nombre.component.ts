import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService, IEmmitCallbackModel, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { takeUntil } from 'rxjs/operators';
import { SidebarService } from 'src/app/services/sidebar-services';
import { v4 as uuidv4 } from 'uuid';
import { fadeOutToRight } from 'src/app/animations/fade-right';
import { Sidebar } from 'ng-sidebar';
import { Perfil, UsuarioLogin } from 'src/app/globales/business-objects';
import { PageBase } from '../../../pageBase';
import { EditarPerfilComponent } from '../editar-perfil.component';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo } from 'src/app/globales/enumeradores';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Constantes } from 'src/app/globales/constantes';


@Component({
  selector: 'app-modificar-nombre',
  templateUrl: './modificar-nombre.component.html',
  styleUrls: ['./modificar-nombre.component.css'],

})
export class ModificarNombreComponent extends PageBase implements OnInit, OnChanges {

  @Input() id: string;


  usuarioLogin: UsuarioLogin;
  current: Perfil;
  constructor(
    private modalSidebar: SidebarService,
    private constantes:Constantes,
    public activeModal: NgbActiveModal,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    public router: Router,
    private variablesApi: VariablesGenerales,
    private modalService: NgbModal,
    private rx: RxService,
    private maquinaHome: MaquinaHome,
  ) {
    super(ref, utils, router);
    this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
    this.current = {
      nombre: this.usuarioLogin.usuarioNombre,
      apellido: this.usuarioLogin.usuarioApellido,
      email: this.usuarioLogin.usuarioEmail,
      celular: this.usuarioLogin.usuarioTelefono,
      rut: this.usuarioLogin.usuarioRut
    }

  }
  ngOnChanges(changes: SimpleChanges) {
    console.log("Cambios Parent")

  }
  ngOnInit() {

  }
  closeThis() {
    this.modalSidebar.close(this.id);
  }

  save() {
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        nombre: this.current.nombre,
        apellido: this.current.apellido
      }
    };

    this.utils.showSpinner("Interno")

    var body = {
      nombre: this.current.nombre,
      apellido: this.current.apellido,
      enviarPush: 1,
      enviarRecibos: 1,
    };

    this.rx.llamarPUTIngreso(body, this.constantes.USUARIO_MODIFICAR_DATOS).then(result => {
      this.variablesApi.home.usuarioLogIn.usuarioNombre = this.current.nombre
      this.variablesApi.home.usuarioLogIn.usuarioApellido = this.current.apellido
      this.utils.hideSpinner("Interno")
      setTimeout(() => {
        this.modalSidebar.closeAll()
      }, 400);
    }, this.errorCallback);
  }
  validaMaxInputText(event) {
    this.utils.validaMaxInputText(event)
  }

}
