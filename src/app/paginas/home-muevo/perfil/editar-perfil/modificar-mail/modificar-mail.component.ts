import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Perfil, UsuarioLogin } from 'src/app/globales/business-objects';
import { Constantes } from 'src/app/globales/constantes';
import { AccionTipo } from 'src/app/globales/enumeradores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../../pageBase';

@Component({
  selector: 'app-modificar-mail',
  templateUrl: './modificar-mail.component.html',
  styleUrls: ['./modificar-mail.component.scss']
})
export class ModificarMailComponent extends PageBase implements OnInit {
  @Input() id: string;

  usuarioLogin: UsuarioLogin;
  current: Perfil;

  constructor(
    private rx: RxService,

    private modalSidebar: SidebarService, private variablesApi: VariablesGenerales, public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router,) {
    super(ref, utils, router);

    this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
    this.current = {
      nombre: this.usuarioLogin.usuarioNombre,
      apellido: this.usuarioLogin.usuarioApellido,
      email: this.usuarioLogin.usuarioEmail,
      celular: this.usuarioLogin.usuarioTelefono,
      rut: this.usuarioLogin.usuarioRut
    }

  }

  ngOnInit() {
  }

  validaMaxInputMail(ingreso: any) {
    this.utils.validaMaxInputMail(ingreso);
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(ingreso)){

    }


  }

 


  closeThis() {
    this.modalSidebar.close(this.id);
  }

  save() {

    let body = {mail: this.current.email};
    this.utils.showSpinner("Interno")
    this.rx.llamarPOSTIngreso(body, this.constantes.CAMBIAR_MAIL_ENDPOINT).then(result => {
      this.variablesApi.home.usuarioLogIn.usuarioEmail = this.current.email
     
      this.utils.hideSpinner("Interno")
      setTimeout(() => {
        this.modalSidebar.closeAll()
      }, 400);
    }, this.errorCallback);
  }

}
