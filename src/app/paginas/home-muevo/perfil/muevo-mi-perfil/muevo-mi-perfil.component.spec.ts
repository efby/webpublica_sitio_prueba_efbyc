import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoMiPerfilComponent } from './muevo-mi-perfil.component';

describe('MuevoMiPerfilComponent', () => {
  let component: MuevoMiPerfilComponent;
  let fixture: ComponentFixture<MuevoMiPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoMiPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoMiPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
