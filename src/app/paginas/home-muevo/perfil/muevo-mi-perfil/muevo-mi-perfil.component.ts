import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { UsuarioLogin, Perfil } from 'src/app/globales/business-objects';
import { PageBase } from '../../pageBase';
import { RxService } from 'src/app/servicios/rx.service';
import { EditarPerfilComponent } from '../editar-perfil/editar-perfil.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SidebarService } from 'src/app/services/sidebar-services';

@Component({
    selector: 'app-muevo-mi-perfil',
    templateUrl: './muevo-mi-perfil.component.html',
    styleUrls: ['./muevo-mi-perfil.component.css']
})
export class MuevoMiPerfilComponent extends PageBase implements OnInit {

    current: Perfil;

    _nvoPin: boolean = false;
    _nvoTelefono: boolean = false;
    inputNroTelefono: string = "";
    inputValidaCodigo1: string;
    inputValidaCodigo2: string;
    inputValidaCodigo3: string;
    inputPin: string = "";
    inputIngresoContrasena: string = "";
    inputRepiteContrasena: string = "";
    usuarioLogin: UsuarioLogin;

    constructor(
        private modalSidebar: SidebarService,
        private rx: RxService, private maquinaIngreso: MaquinaIngreso, public router: Router, public utils: UtilitariosService, private variablesApi: VariablesGenerales, public ref: ChangeDetectorRef) {
        super(ref, utils, router);
        this.current = new Perfil();
    }


    public get tieneCorreo(): boolean {
        return this.current.email != null;
    }


    ngOnInit() {
        this.updateData();
    }
    updateData() {
        this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
        this.current = {
            nombre: this.usuarioLogin.usuarioNombre,
            apellido: this.usuarioLogin.usuarioApellido,
            email: this.usuarioLogin.usuarioEmail,
            celular: this.usuarioLogin.usuarioTelefono,
            rut: this.usuarioLogin.usuarioRut
        }
        console.log(this.usuarioLogin)
    }

    save() {
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_CLICK,
            datos: {
                nombre: this.current.nombre,
                apellido: this.current.apellido
            }
        };

        this.utils.showSpinner("Interno")
        this.rx.cuentaUsuarioModificarDatos(this.current.nombre, this.current.apellido).then(result => {
            this.utils.hideSpinner("Interno")
        }, this.errorCallback);
    }

    validaMaxInputText(event) {
        this.utils.validaMaxInputText(event)
    }

    levantarModal() {
        var mod = this.modalSidebar.open(EditarPerfilComponent,);
        mod.refModal.result.then(() => {
            this.updateData()
        }, () => {
            this.updateData()
        })
    }





}
