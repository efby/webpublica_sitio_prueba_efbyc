import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.component.html',
    styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
    
    migajas = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Perfil", class: "active", url: "" }
    ];
    
    _configurarPerfil: boolean = false;
    _configurarNotificaciones: boolean = false;
    _configurarEmpresas: boolean = false;
    _terminosycondiciones: boolean = false;
    _ayuda: boolean = false;
    tipoMenu: number = 0;
    
    interval: any;
    isMobile: boolean = false;
    
    constructor(private variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, private route: ActivatedRoute, private router: Router, private ref: ChangeDetectorRef) {
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        ref.detach();
        this.interval = setInterval(() => {
            this.ref.detectChanges();
            this.isMobile = this.variablesApi.esMobile;
        }, 10);
    }
    
    ngOnDestroy() {
        clearInterval(this.interval);
    }
    
    ngOnInit() {
        this.tipoMenu = parseInt(this.route.snapshot.paramMap.get('idPerfil'));
        setTimeout(() => {
            this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
        });
        
        switch (this.tipoMenu) {
            case 0:
            this.activatePerfil();
            break;
            case 1:
            this.activateNotificaciones();
            break;
            case 2:
            this.activateEmpresas();
            break;
            case 3:
            this.activateTyC();
            break;
            case 4:
            this.activateAyuda();
            break;
            default:
            this.activatePerfil();
            break;
        }
    }
    
    activatePerfil() {
        this._configurarPerfil = true;
        this._configurarNotificaciones = false;
        this._configurarEmpresas = false;
        this._terminosycondiciones = false;
        this._ayuda = false;
    }
    
    activateNotificaciones() {
        this._configurarPerfil = false;
        this._configurarNotificaciones = true;
        this._configurarEmpresas = false;
        this._terminosycondiciones = false;
        this._ayuda = false;
    }
    
    activateEmpresas() {
        this._configurarPerfil = false;
        this._configurarNotificaciones = false;
        this._configurarEmpresas = true;
        this._terminosycondiciones = false;
        this._ayuda = false;
    }
    
    activateTyC() {
        this._configurarPerfil = false;
        this._configurarNotificaciones = false;
        this._configurarEmpresas = false;
        this._terminosycondiciones = true;
        this._ayuda = false;
    }
    
    activateAyuda() {
        this._configurarPerfil = false;
        this._configurarNotificaciones = false;
        this._configurarEmpresas = false;
        this._terminosycondiciones = false;
        this._ayuda = true;
    }
    
}
