import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoTerminosyCondicionesComponent } from './muevo-terminosy-condiciones.component';

describe('MuevoTerminosyCondicionesComponent', () => {
  let component: MuevoTerminosyCondicionesComponent;
  let fixture: ComponentFixture<MuevoTerminosyCondicionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoTerminosyCondicionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoTerminosyCondicionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
