import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoAyudaComponent } from './muevo-ayuda.component';

describe('MuevoAyudaComponent', () => {
  let component: MuevoAyudaComponent;
  let fixture: ComponentFixture<MuevoAyudaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoAyudaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoAyudaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
