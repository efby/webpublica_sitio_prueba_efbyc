import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { RouteConfigLoadStart, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Perfil, UsuarioLogin } from 'src/app/globales/business-objects';
import { Constantes } from 'src/app/globales/constantes';
import { Paginas } from 'src/app/globales/paginas';
import { Home, Ingreso, VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';
import { AyudaComponent } from '../ayuda/ayuda.component';
import { ConfigNotificacionesComponent } from '../config-notificaciones/config-notificaciones.component';
import { EditarPerfilComponent } from '../editar-perfil/editar-perfil.component';

@Component({
  selector: 'app-mi-perfil-sidebar',
  templateUrl: './mi-perfil-sidebar.component.html',
  styleUrls: ['./mi-perfil-sidebar.component.scss']
})
export class MiPerfilSidebarComponent extends PageBase implements OnInit, OnChanges {


  @Input() id: string;

  usuarioLogin: UsuarioLogin;
  current: Perfil;
  constructor(public activeModal: NgbActiveModal,
    public modalSidebar: SidebarService,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    public router: Router,
    public variablesApi: VariablesGenerales,
    private modalService: NgbModal,
    private rx: RxService,
    private constantes: Constantes,
  ) {
    super(ref, utils, router);
    this.updateData();

  }

  updateData() {
    this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
    this.current = {
      nombre: this.usuarioLogin.usuarioNombre,
      apellido: this.usuarioLogin.usuarioApellido,
      email: this.usuarioLogin.usuarioEmail,
      celular: this.usuarioLogin.usuarioTelefono,
      rut: this.usuarioLogin.usuarioRut
    }
    console.log(this.usuarioLogin)
  }
  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("Cambios Parent")

  }
  closeThis() {
    this.modalSidebar.close(this.id);
  }

  levantarModalMisDatos() {
    var mod = this.modalSidebar.open(EditarPerfilComponent,);
    mod.refModal.result.then(() => {
      this.updateData()
    }, () => {
      this.updateData()
    })
  }

  cerrarSesion() {
    localStorage.clear();
    this.variablesApi.authorizationToken = null;
    this.variablesApi.equipoSecret = null;
    this.variablesApi.home = new Home();
    this.variablesApi.ingreso = new Ingreso()
    this.router.navigate([Paginas.IngresoInicioPage]);
  }

  levantarModalAyuda() {
    var mod = this.modalSidebar.open(AyudaComponent,);
    mod.refModal.result.then(() => {
      this.updateData()
    }, () => {
      this.updateData()
    })
  }
  levantarModalConfiguracion() {
    var body = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
    }


    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kConsultarCondiguracionNotificaciones).then(result => {
     
      var mod = this.modalSidebar.open(ConfigNotificacionesComponent,);
      mod.refModal.componentInstance.configuracionNotificaciones=result.data.data.configuracionNotificaciones
      mod.refModal.result.then(() => {
        this.updateData()
      }, () => {
        this.updateData()
      })
      this.utils.hideSpinner("Interno");
    }, this.errorCallback)

  }


}
