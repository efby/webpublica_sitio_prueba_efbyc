import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoMisNotificacionesComponent } from './muevo-mis-notificaciones.component';

describe('MuevoMisNotificacionesComponent', () => {
  let component: MuevoMisNotificacionesComponent;
  let fixture: ComponentFixture<MuevoMisNotificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoMisNotificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoMisNotificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
