import { Component, OnInit } from '@angular/core';
import { Notificaciones, VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-muevo-mis-notificaciones',
  templateUrl: './muevo-mis-notificaciones.component.html',
  styleUrls: ['./muevo-mis-notificaciones.component.css']
})
export class MuevoMisNotificacionesComponent implements OnInit {

  _notificacionCompras: boolean = true;
  _notificacionSaldo: boolean = true;
  _notificacionDocumentacion: boolean = true;
  _notificacionPromociones: boolean = true;
  _notificacionInvitaciones: boolean = true;
  _notificacionRendiciones: boolean = true;

  notiDummy: Notificaciones[] = [
    {
      tipoNotificacionId: 1,
      tipoNotificacionEstado: true,
      tipoNotificacionNombre: "Compras",
      tipoNotificacionDetalle: "Compras realizadas por los conductores de tu equipo"
    },
    {
      tipoNotificacionId: 2,
      tipoNotificacionEstado: true,
      tipoNotificacionNombre: "Saldo",
      tipoNotificacionDetalle: "movimientos en tu saldo"
    },
    {
      tipoNotificacionId: 3,
      tipoNotificacionEstado: true,
      tipoNotificacionNombre: "Usuarios",
      tipoNotificacionDetalle: "Incorporación y eliminación usuarios"
    },
    {
      tipoNotificacionId: 4,
      tipoNotificacionEstado: true,
      tipoNotificacionNombre: "Vehículos",
      tipoNotificacionDetalle: "Creación y eliminación vehículos"
    },
    {
      tipoNotificacionId: 5,
      tipoNotificacionEstado: true,
      tipoNotificacionNombre: "Invitaciones",
      tipoNotificacionDetalle: "Invitaciones recibidas para participar de otras cuentas"
    },
  ]
  notificaciones: Notificaciones[] = [];

  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
    // this.notificaciones = this.variablesApi.home.notificaciones;
    this.notificaciones = this.notiDummy;
  }

  deActivateOptions() {
    for (let index = 0; index < this.notificaciones.length; index++) {
      this.notificaciones[index].tipoNotificacionEstado = false;
    }
  }

}
