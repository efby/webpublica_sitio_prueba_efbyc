import { Component, OnInit } from '@angular/core';
import { VariablesGenerales, Cuentas } from 'src/app/globales/variables-generales';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { UsuarioLogin, Invitacion, Cuenta } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-muevo-mis-empresas',
  templateUrl: './muevo-mis-empresas.component.html',
  styleUrls: ['./muevo-mis-empresas.component.css']
})
export class MuevoMisEmpresasComponent implements OnInit {
  _invitaciones: boolean;
  autorizaInvitacion: boolean = false;
  _saveInvitaciones: boolean = false;
  invitaciones: Invitacion[] = [];
  empresas: Cuenta[] = [];
  cuentas: Cuenta[] = [];
  invitado: Invitacion;
  _muestraInvitaciones: boolean = false;
  usuarioLogin: UsuarioLogin = new UsuarioLogin();
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private maquinaHome: MaquinaHome, private utils: UtilitariosService, private router: Router) {
    this.isMobile = variablesApi.esMobile;
  }

  ngOnInit() {
    this.invitado = new Invitacion();
    this.cargaFechaInvitaciones();
    this.invitaciones = this.variablesApi.home.invitaciones;
    this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
    this.cargaFecha();
    this.empresas = this.variablesApi.home.cuentas.filter(r => r.rolId != 1);
    this.cuentas = this.variablesApi.home.cuentas.filter(r => r.rolId === 1);
    this._invitaciones = false;
    if (this.invitaciones.length > 0) {
      this._muestraInvitaciones = true;
    } else {
      this._muestraInvitaciones = false;
    }
  }

  cargaFecha() {
    // this.variablesApi.home.cuentas.forEach(element => {
    //   element.cuentaFechaCreacionString = this.date2Human(element.cuentaFechaCreacion);
    // });
  }
  cargaFechaInvitaciones() {
    this.variablesApi.home.invitaciones.forEach(element => {
      element.invitacionFechaCaducidad = this.date2Human(element.invitacionFechaCaducidad);
    });
  }

  selInvitacion(id) {
    let idComponent = this.invitaciones.findIndex(r => r.invitacionId === id);
    for (let index = 0; index < this.invitaciones.length; index++) {
      this.invitaciones[index].invitacionActivate = false;
    }

    this.invitaciones[idComponent].invitacionActivate = true;
    this.invitado = this.invitaciones[idComponent];
    this._invitaciones = true;
  }

  enableSaveInvitaciones() {
    if (this.autorizaInvitacion) {
      this._saveInvitaciones = true;
    } else {
      this._saveInvitaciones = false;
    }
  }

  fnConviertEstadoInvitacion(estado: string) {
    let retorno: string = "";
    switch (estado) {
      case "CREADA":
        retorno = "Enviada";
        break;
      case "RECHAZADA":
        retorno = "Rechazada por Conductor";
        break;
      case "ACEPTADA":
        retorno = "Aceptada por Conductor";
        break;
      default:
        break;
    }
    return retorno;
  }

  fnInvitaciones() {
    if (this.autorizaInvitacion) {
      this.autorizarInvitacion(this.invitado.invitacionId, 1);
    } else {
      this.autorizarInvitacion(this.invitado.invitacionId, 0);
    }
  }

  autorizarInvitacion(invitacionId: number, invitacionAceptada: number) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        invitacionId: invitacionId,
        invitacionAceptada: invitacionAceptada
      }
    }

    this.maquinaHome.homeUsuarioAceptaInvitacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.ngOnInit();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
          this.utils.hideSpinner("Interno");
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  date2Human(date) {
    let format = 'dd/MM/yyyy';
    return this.utils.date2Human(date, format, this.router.url);
  }

  changeEmpresa(id) {
    // this.empresas.forEach(element => {
    //   element.cuentaDefault = false
    // });

    let idComponente = this.empresas.findIndex(r => r.cuentaId === id);
    this.accionCambiarEmpresaSeleccionada(id);
  }

  accionCambiarEmpresaSeleccionada(cuentaId: number) {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: cuentaId,
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        usuarioId: this.variablesApi.home.usuarioLogIn.usuarioId
      }
    };

    this.maquinaHome.homeCambiarCuentaSeleccionada(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.ngOnInit();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });

  }

  getRolText(rolId) {
    return this.variablesApi.home.maestros.roles.filter(r => r.rolId === rolId)[0].rolNombre;
  }


  maquinaModificarUsuarioCuenta(cuentaId: number, estado: number) {
    let usuarioCuenta = this.variablesApi.home.usuarioLogIn;
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: cuentaId,
        usuarioId: usuarioCuenta.usuarioId,
        grupoCuentaId: 0,
        usuarioCuentaEstadoId: estado,
        rolId: 4,
        usuarioCuentaConduce: 0,
        usuarioCuentaRindeGasto: 0,
        usuarioCuentaCreaVehiculo: 0,
        usuarioCuentaEligeDTE: 0,
        usuarioCuentaSeleccionado: 0,
        vehiculosAsignados: [],
        estadoCombustible: 1,
        montoMaximoDiaValor: 0,
        montoMaximoMesValor: 0
      }
    }

    this.maquinaHome.homeUsuarioModificar(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.ngOnInit();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

}
