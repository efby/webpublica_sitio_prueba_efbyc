import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoMisEmpresasComponent } from './muevo-mis-empresas.component';

describe('MuevoMisEmpresasComponent', () => {
  let component: MuevoMisEmpresasComponent;
  let fixture: ComponentFixture<MuevoMisEmpresasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoMisEmpresasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoMisEmpresasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
