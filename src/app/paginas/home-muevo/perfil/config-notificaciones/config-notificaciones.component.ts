import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Constantes } from 'src/app/globales/constantes';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';

@Component({
  selector: 'app-config-notificaciones',
  templateUrl: './config-notificaciones.component.html',
  styleUrls: ['./config-notificaciones.component.scss']
})
export class ConfigNotificacionesComponent extends PageBase implements OnInit, OnChanges {


  @Input() id: string;

  @Input() configuracionNotificaciones: Array<any>

  constructor(public activeModal: NgbActiveModal,
    public modalSidebar: SidebarService,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    public router: Router,
    public variablesApi: VariablesGenerales,
    private modalService: NgbModal,
    private rx: RxService,
    private constantes: Constantes,
  ) {
    super(ref, utils, router);
  }

  ngOnInit() {
    this.obtenerValoresNotificacion()
    console.log(this.configuracionNotificaciones);
  }

  obtenerValoresNotificacion() {
    this.configuracionNotificaciones.forEach(elemento => {
      elemento.tipoNotificacionDescripcion = this.obtenerValor(elemento.tipoNotitifacionId, "tipoNotificacionId", this.variablesApi.home.maestros.tiposNotificaciones, "tipoNotificacionDescripcion");
      elemento.tipoNotificacionNombre = this.obtenerValor(elemento.tipoNotitifacionId, "tipoNotificacionId", this.variablesApi.home.maestros.tiposNotificaciones, "tipoNotificacionNombre")
      elemento.tipoNotificacionSeccion = this.obtenerValor(elemento.tipoNotitifacionId, "tipoNotificacionId", this.variablesApi.home.maestros.tiposNotificaciones, "tipoNotificacionSeccion")

    })

    this.configuracionNotificaciones = this.configuracionNotificaciones.filter(item => { return item.tipoNotificacionSeccion == "ADMINISTRADOR" });
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("Cambios Parent")

  }
  closeThis() {
    this.modalSidebar.close(this.id);
  }
  cambiarEstadoNotificacion(item) {
    let listaNotificaciones = [];
    item.tipoNotificacionEstado = item.tipoNotificacionEstado == 0 ? 1 : 0;
    listaNotificaciones
      .push({ "tipoNotitifacionId": item.tipoNotitifacionId, "tipoNotificacionEstado": item.tipoNotificacionEstado });
    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
      "arrayNotificaciones": listaNotificaciones
    };
    this.llamarNotificaciones(body)

  }
  desactivarAllNotificaciones() {
    let listaNotificaciones = [];
    this.configuracionNotificaciones.forEach((element) => {
      listaNotificaciones
        .push({ "tipoNotitifacionId": element.tipoNotitifacionId, "tipoNotificacionEstado": 0 });
    });
    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
      "arrayNotificaciones": listaNotificaciones
    };
    this.llamarNotificaciones(body);

  }
  llamarNotificaciones(body) {
    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kModificarCondiguracionNotificaciones).then(result => {

      this.reloadNotificaciones()
      console.log(result)
      if (body.arrayNotificaciones != null && body.arrayNotificaciones.length > 1) {
        setTimeout(() => {
          let msg_2 = {
            title: 'Notificaciones',
            messages: [result.data.message],
          }
          this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {

          })
        }, 400);
      }


      this.utils.hideSpinner("Interno");
    }, this.errorCallback)
  }

  reloadNotificaciones() {
    var body = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
    }
    this.rx.llamarPOSTHome(body, this.constantes.kConsultarCondiguracionNotificaciones).then(result => {
      this.configuracionNotificaciones = result.data.data.configuracionNotificaciones
      this.obtenerValoresNotificacion()
    }, this.errorCallback)
  }

}
