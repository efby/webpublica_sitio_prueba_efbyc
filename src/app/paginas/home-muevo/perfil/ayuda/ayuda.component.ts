import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Perfil, UsuarioLogin } from 'src/app/globales/business-objects';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.component.html',
  styleUrls: ['./ayuda.component.scss']
})
export class AyudaComponent extends PageBase implements OnInit, OnChanges {


  @Input() id: string;

  usuarioLogin: UsuarioLogin;
  current: Perfil;
  constructor(public activeModal: NgbActiveModal,
    public modalSidebar: SidebarService,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    public router: Router,
    public variablesApi: VariablesGenerales,
    private modalService: NgbModal,
    private rx: RxService
  ) {
    super(ref, utils, router);
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("Cambios Parent")

  }
  closeThis() {
    this.modalSidebar.close(this.id);
  }

}
