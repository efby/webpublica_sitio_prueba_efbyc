import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMuevoComponent } from './home-muevo.component';

describe('HomeMuevoComponent', () => {
  let component: HomeMuevoComponent;
  let fixture: ComponentFixture<HomeMuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
