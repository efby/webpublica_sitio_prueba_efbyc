import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { PageBase } from '../pageBase';
import { Cuenta, InvitacionRecibida } from 'src/app/globales/business-objects';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService, IEmmitCallbackModel, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { VariablesGenerales, Roles } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { Constantes } from 'src/app/globales/constantes';
import { InvitacionesRecibidasComponent } from './invitaciones-recibidas/invitaciones-recibidas.component';

@Component({
    selector: 'app-cuentas',
    templateUrl: './cuentas.component.html',
    styleUrls: ['./cuentas.component.scss']
})
export class CuentasComponent extends PageBase implements OnInit {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    migajas = [
        { title: "Inicio", class: "", url: "/" + Paginas.HomeMuevo },
        { title: "Mis empresas e invitaciones", class: "active", url: "" }
    ];

    private unsubscribe = new Subject();
    callbackSubscripcion: any;

    displayedColumns = ['cuentaNombre', 'rolNombre', 'actions']
    dataSource: MatTableDataSource<Cuenta> = new MatTableDataSource<Cuenta>();

    cuentas: Cuenta[];
    invitaciones: InvitacionRecibida[];

    constructor(private rx: RxService,
        public modalSidebar: SidebarService,
        private constantes: Constantes,
        public utils: UtilitariosService, public ref: ChangeDetectorRef, public router: Router,
        private emiter: BreadCrumbsService, public variablesApi: VariablesGenerales) {
        super(ref, utils, router);
        console.clear();
        setTimeout(() => {
            this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
        });
        this.cuentas = [];
        this.invitaciones = [];

        this.callbackSubscripcion = this.emiter.callbackEmmited$.pipe(takeUntil(this.unsubscribe)).subscribe(this.emitterHandler);

    }

    ngOnInit() {
        this.obtenerCuentas();
    }


    abrirInvitaciones() {
        var body = {
            "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId
        }


        this.utils.showSpinner("Interno");
        this.rx.llamarPOSTHome(body, this.constantes.kCuentaConsultarnotificacionesRecibidas).then(result => {

            var modal = this.modalSidebar.open(InvitacionesRecibidasComponent, /* { miBackdrop: false } */)


            modal.refModal.componentInstance.posicionFinal = result.data.data.posicionFinal;
            modal.refModal.componentInstance.invitacionesRecibidas = result.data.data.invitaciones
            modal.refModal.componentInstance.cantidadRegistrosPorConsulta = result.data.data.cantidadRegistrosPorConsulta
            /*  
             modal.refModal.componentInstance.cuentaId = this.variablesApi.home.cuentaSeleccionada.cuentaId;
             modal.refModal.componentInstance.detalleUsuario = result.data.data.usuarioCuenta
             if (row.rolId != 1) {
               modal.refModal.result.then(() => {
                 this.administradores = [];
                 this.utils.showSpinner("Interno");
                 this.obtenerAdministradores();
               })
             } */
            modal.refModal.result.then((re) => {
                if (re) {
                    this.obtenerCuentas();
                }
                //this.utils.showSpinner("Interno");
                //this.obtenerAdministradores();
            }, (reason) => {
                if (reason) {
                    this.obtenerCuentas();
                    //this.utils.showSpinner("Interno");
                    //this.obtenerAdministradores();
                }
            })
            this.utils.hideSpinner("Interno");
        }, this.errorCallback)



    }

    nuevaCuenta() {
        this.emiter.emitChange({
            tipo: 'NuevaCuenta'
        });
    }

    private obtenerCuentas() {
        this.utils.showSpinner("Interno");
        this.rx.cuentasGet().then((result: any) => {
            console.log("GET CUENTAS")
            console.log(result)
            this.variablesApi.home.rolPrivilegiosFront = result.data.rolPrivilegiosFront
            this.variablesApi.home.menu = result.data.menu
            this.cuentas = result.data.cuentas.map((c: Cuenta) => {
                c.rolNombre = this.variablesApi.home.maestros.roles.filter((x: Roles) => x.rolId == c.rolId)[0].rolNombre;
                return c;
            });
            this.invitaciones = result.data.usuarioLogin ? result.data.usuarioLogin.invitaciones : [];
            this.dataSource = new MatTableDataSource(this.cuentas);
            this._configureFunctionOfMatTable(this.dataSource, this.paginator, this.sort);

            this.utils.hideSpinner("Interno");
            console.log(result);
        }, this.errorCallback)
    }

    private emitterHandler = (data: IEmmitCallbackModel) => {
        if (data.from == Paginas.HomeMuevoCuentas || data.from == Paginas.ModalCuentaDetalle)
            this.obtenerCuentas();
    }

    mostrarConductor(cuenta: Cuenta, event) {


        let config: IEmmitModel = {
            tipo: 'CuentaDetalle',
            action: 'set',
            data: {
                item: cuenta
            }
        }

        this.emiter.emmitChange(config);
    }

    ngOnDestroy() {
        if (this.callbackSubscripcion != undefined && !this.callbackSubscripcion.closed)
            this.callbackSubscripcion.unsubscribe();
    }

}
