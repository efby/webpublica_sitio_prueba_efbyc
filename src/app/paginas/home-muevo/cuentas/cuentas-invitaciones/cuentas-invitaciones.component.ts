import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RxService } from 'src/app/servicios/rx.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Router } from '@angular/router';
import { PageBase } from '../../pageBase';
import { InvitacionRecibida } from 'src/app/globales/business-objects';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';

@Component({
    selector: 'app-cuentas-invitaciones',
    templateUrl: './cuentas-invitaciones.component.html',
    styleUrls: ['./cuentas-invitaciones.component.scss']
})
export class CuentasInvitacionesComponent extends PageBase implements OnInit {
    
    @Input()
    invitaciones: InvitacionRecibida[];

    @Output()
    onFinish = new EventEmitter<string>();
    
    constructor(private emiter:BreadCrumbsService, public utils:UtilitariosService, private rx:RxService, private variablesApi:VariablesGenerales, public ref: ChangeDetectorRef, public router: Router) {
        super(ref, utils, router);
        this.invitaciones = [];
    }
    
    ngOnInit() {
        
    }
    
    aceptar(item: InvitacionRecibida){
        this.cambiarEstadoInvitacion(item, true);
    }
    
    rechazar(item:InvitacionRecibida){
        this.cambiarEstadoInvitacion(item, false);
    }
    
    private cambiarEstadoInvitacion(item:InvitacionRecibida, accept: boolean){
        var model = {
            invitacionId:item.invitacionId, 
            invitacionAceptada: accept ? 1 : 0
        }
        
        this.utils.showSpinner("Interno");
        this.rx.cuentasInvitacionAceptarRechazar(model).then(this.successCallback, this.errorCallback);
    }

    private successCallback = (result) => {
        this.utils.hideSpinner("Interno");
        this.emiter.emitCallback({
            from: Paginas.HomeMuevoCuentas,
            action: 'refresh'
        });
        this.onFinish.emit('close modal');
    }
    
    
    
}
