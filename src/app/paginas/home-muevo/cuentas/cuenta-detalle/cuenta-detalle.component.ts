import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Cuenta } from 'src/app/globales/business-objects';
import { PageBase } from '../../pageBase';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { RxService } from 'src/app/servicios/rx.service';
import { BreadCrumbsService, IEmmitModel, IEmmitCallbackModel } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
    selector: 'app-cuenta-detalle',
    templateUrl: './cuenta-detalle.component.html',
    styleUrls: ['./cuenta-detalle.component.scss']
})
export class CuentaDetalleComponent extends PageBase implements OnInit {
    
    CURRENT_PAGE = Paginas.ModalCuentaDetalle;
    current: Cuenta;

    cuentaSeleccionada: Cuenta;

    constructor(private variablesApi: VariablesGenerales, private emiter:BreadCrumbsService, private rx:RxService, public utils: UtilitariosService, public router: Router, public ref: ChangeDetectorRef) { 
        super(ref, utils, router);

        this.current = new Cuenta();
        this.cuentaSeleccionada = new Cuenta();
    }
    
    ngOnInit() {
    }
    
    setData(cuenta:Cuenta){
        this.cuentaSeleccionada = this.variablesApi.home.cuentaSeleccionada;
        this.current = this.utils.clone(cuenta);
    }

    private successCallback = (result) => {
        this.utils.hideSpinner();
            
            let config:IEmmitCallbackModel = {
                from: this.CURRENT_PAGE,
                action: 'refresh'
            }
            this.emiter.emitChange({
                tipo: "CERRAR MODAL REMOTO",
                action: 'CLOSE',
                data: {
                    sidebars: ['sideBarDetalleCuenta']
                }
            })

            this.emiter.emitCallback(config);
    }

    guardar(item:Cuenta){
        this.modificarCuenta(item, 1);
    }

    eliminar(item:Cuenta){
        this.utils.msgPlainModal('Eliminar Cuenta', ['¿Está seguro que desea eliminar esta cuenta?', item.cuentaNombre]).then(result => {
            if(result == OpcionesModal.ACEPTAR)
                this.modificarCuenta(item, 2);
        })
    }

    private modificarCuenta(item:Cuenta, estado:number){
        this.utils.showSpinner();
        this.rx.cuentaUsuarioModificar(item, estado).then(this.successCallback, this.errorCallback)
    }
    
}
