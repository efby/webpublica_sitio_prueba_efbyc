import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';
import { RxService } from 'src/app/servicios/rx.service';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';

@Component({
    selector: 'app-cuentas-crear',
    templateUrl: './cuentas-crear.component.html',
    styleUrls: ['./cuentas-crear.component.scss']
})
export class CuentasCrearComponent extends PageBase implements OnInit {
    @Output()
    onFinish = new EventEmitter<string>();

    current: {
        nombre: string;
    }
    constructor(private emiter: BreadCrumbsService, private rx:RxService, public router: Router, public ref: ChangeDetectorRef, public utils: UtilitariosService) {
        super(ref, utils, router);

        this.current = {
            nombre: ''
        }
    }
    
    ngOnInit() {

    }

    guardar(item:any){
        this.utils.showSpinner("Interno");
        this.rx.cuentasCrear(item.nombre).then((result:any) => {
            this.utils.hideSpinner("Interno");

            this.emiter.emitCallback({
                from: Paginas.HomeMuevoCuentas,
                action: 'refresh'
            });
            this.onFinish.emit('close modal');
        }, this.errorCallback);
    }

    validateInputMaxText(e){
        this.utils.validaMaxInputText(e);
    }
    
}
