import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';
import { DatalleInvitacionRecibidaComponent } from './datalle-invitacion-recibida/datalle-invitacion-recibida.component';

@Component({
  selector: 'app-invitaciones-recibidas',
  templateUrl: './invitaciones-recibidas.component.html',
  styleUrls: ['./invitaciones-recibidas.component.scss']
})
export class InvitacionesRecibidasComponent extends PageBase implements OnInit {
  @Input() id: string;
  @Input() posicionFinal: number
  @Input() invitacionesRecibidas = []
  @Input() cantidadRegistrosPorConsulta: number

  invitaciones = []

  constructor(private rx: RxService,
    public modalSidebar: SidebarService,
    public variablesApi: VariablesGenerales,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router) {
    super(ref, utils, router);

  }

  ngOnInit() {
    this.distintasA4()
  }

  distintasA4() {
    this.invitaciones = this.invitacionesRecibidas.filter(a => { return a.invitacionRolId != 4 })
  }

  closeThis() {
    this.modalSidebar.close(this.id);
  }

 
  ver(i){
    var modal = this.modalSidebar.open(DatalleInvitacionRecibidaComponent,  { miBackdrop: false } )
    modal.refModal.componentInstance.invitacion =i
    modal.refModal.componentInstance.cantidadInvitiaciones=this.invitacionesRecibidas.length

    modal.refModal.result.then((result) => {
      if(result){
       
        this.cargarinvitaciones()
      }
     
    }, () => {
     
    })
      
  }

  cargarinvitaciones(){
    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId
    }

    if (this.posicionFinal != null) {
      body["posicionInicial"] = this.posicionFinal;
    }

    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kCuentaConsultarnotificacionesRecibidas).then(result => {

      this.invitaciones = result.data.data.invitaciones
      this.posicionFinal=result.data.data.posicionFinal;
      if(this.invitaciones.length==0){
        this.modalSidebar.close(this.id,true);
      }
      this.distintasA4();

      this.utils.hideSpinner("Interno");
    }, this.errorCallback)
  }

  next() {

    var body = {
      "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId
    }

    if (this.posicionFinal != null) {
      body["posicionInicial"] = this.posicionFinal;
    }

    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kCuentaConsultarnotificacionesRecibidas).then(result => {

      this.invitaciones = this.invitaciones.concat(result.data.data.invitaciones)
      this.posicionFinal=result.data.data.posicionFinal;
      this.distintasA4();

      this.utils.hideSpinner("Interno");
    }, this.errorCallback)

  }

}
