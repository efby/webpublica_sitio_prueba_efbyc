import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../../pageBase';

@Component({
  selector: 'app-datalle-invitacion-recibida',
  templateUrl: './datalle-invitacion-recibida.component.html',
  styleUrls: ['./datalle-invitacion-recibida.component.scss']
})
export class DatalleInvitacionRecibidaComponent extends PageBase implements OnInit {
  @Input() id: string;
  @Input() invitacion: any
  cantidadInvitiaciones: number

  constructor(private rx: RxService,
    public modalSidebar: SidebarService,
    public variablesApi: VariablesGenerales,
    public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router) {
    super(ref, utils, router);
  }

  ngOnInit() {
  }
  closeThis() {
    this.modalSidebar.close(this.id);
  }

  rechazar(invitacion) {
    /*message: "",
    descripcion:
        "",*/


    let msg = {
      title: 'Rechazar invitación',
      messages: ["¿Estás seguro que deseas rechazar la invitación?"],
      okText: "Rechazar"
    }

    this.utils.openModal(Paginas.ModalAceptarCancelarComponent, msg
    ).then(result => {
      switch (result) {
        case OpcionesModal.ACEPTAR:
          this.aceptacionInvitacion(invitacion, 0);
          break;
        case OpcionesModal.CANCELAR:
          console.log("NOK")
          // this.closeThis()
          break;
      }

    });

  }

  aceptacionInvitacion(invitacion, invitacionAceptada: number) {

    var body = { "invitacionId": invitacion.invitacionId, "invitacionAceptada": invitacionAceptada };
    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.kAceptarInvitacionEndPoint).then(result => {
      this.utils.hideSpinner("Interno")
      setTimeout(() => {
        let msg_2 = {
          title: 'Invitación',
          messages: [result.data.message],
        }
        this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {
          if (invitacionAceptada == 1) {
            this.utils.showSpinner("Interno");
            this.rx.cuentasCambiarCuentaSeleccionada(invitacion.invitacionCuentaId).then(result => {

              this.modalSidebar.closeAll()
              this.utils.hideSpinner("Interno")
              this.router.navigate([Paginas.IngresoInicioPage])
            }, this.errorCallback)
          } else {
            if (this.cantidadInvitiaciones == 1) {
              this.modalSidebar.closeAll(true);
            } else {
              this.modalSidebar.close(this.id, true,)
            }

          }



        })
      }, 400);
    }
    );
  }



}
