import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Facturas } from 'src/app/models/facturas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Guias } from 'src/app/models/guias';
import { Boletas } from 'src/app/models/boletas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-muevo-facturas',
  templateUrl: './muevo-facturas.component.html',
  styleUrls: ['./muevo-facturas.component.css']
})
export class MuevoFacturasComponent implements OnInit {

  facturas: Facturas[];
  facturaDetalle: Facturas;
  guias: Guias[];
  boletas: Boletas[];

  @Input() searchMonth: number = 0;
  oldMonth: number = 0;
  interval: any;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private router: Router, private ref: ChangeDetectorRef) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      if (this.searchMonth !== this.oldMonth) {
        this.filterByDate(this.searchMonth);
        this.oldMonth = this.searchMonth;
      }
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
    this.facturas = [];
    //this.cargarFacturas();
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.resetCollapsed();
  }

  cargaFecha() {
    this.variablesApi.home.facturas.forEach(element => {
      element.facturaFechaEmisionString = this.date2Human(element.facturaFechaEmision);
      element.facturaFechaVencimientoString = this.date2Human(element.facturaFechaVencimiento);
    });
  }

  // cargarFacturas() {
  //   let actualUrl = this.router.url.substr(1);
  //   let action: RequestTipo = {
  //     accion: AccionTipo.ACCION_CLICK,
  //     datos: {
  //       cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
  //       fechaConsulta: new Date()
  //     }
  //   }
  //   this.utils.showSpinner("Interno");
  //   this.maquinaHome.homeUsuarioConsultarFacturas(action).then(respuesta => {
  //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
  //       this.cargaFecha();
  //       this.modCurrency();
  //       this.facturas = this.variablesApi.home.facturas;
  //       this.filterByDate(this.searchMonth);
  //       this.utils.hideSpinner("Interno");
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
  //       this.utils.hideSpinner("Interno");
  //       this.utils.openModal(respuesta.modalSalida).then(response => {
  //         console.log(response);
  //       });
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
  //       this.utils.hideSpinner("Interno");
  //       this.utils.msgPlain(respuesta.mensajeSalida);
  //     }
  //   });
  // }

  date2Human(date) {
    let format = "dd/MM/yyyy HH:mm";
    return this.utils.date2Human(date, format, this.router.url);
  }

  modCurrency() {
    this.variablesApi.home.facturas.forEach(element => {
      element.facturaMontoTotalString = this.currency(element.facturaMontoTotal);
      let dv = element.facturaDatosFacturacion.datosFacturacionRut.substr(element.facturaDatosFacturacion.datosFacturacionRut.length - 1, 1);
      let rut = element.facturaDatosFacturacion.datosFacturacionRut.substr(0, element.facturaDatosFacturacion.datosFacturacionRut.length - 1) + "-" + dv;
      element.facturaDatosFacturacion.datosFacturacionRut = rut;
    });
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

  consultarDetalleFactura(idFactura) {
    // let idComFactura = this.facturas.findIndex(r => r.facturaId === idFactura);
    // if (!this.facturas[idComFactura].collapsed) {
    //   this.facturas[idComFactura].collapsed = true;
    // } else {
    //   this.utils.showSpinner("Interno");
    //   let idComponente = this.variablesApi.home.cuentas.findIndex(r => r.cuentaDefault === true);

    //   let actualUrl = this.router.url.substr(1);
    //   let action: RequestTipo = {
    //     accion: AccionTipo.ACCION_CLICK,
    //     datos: {
    //       cuentaId: this.variablesApi.home.cuentas[idComponente].cuentaId,
    //       facturaId: idFactura
    //     }
    //   }

    //   this.maquinaHome.homeUsuarioConsultarDetalleFacturas(action).then(respuesta => {
    //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
    //       // if (actualUrl !== respuesta.paginaRespuesta) {
    //       //   this.router.navigate([respuesta.paginaRespuesta]);
    //       // } else {
    //       //   this.ngOnInit();
    //       // }
    //       console.log(respuesta.data);
    //       this.facturaDetalle = respuesta.data.factura;
    //       this.facturas[idComFactura].collapsed = false;
    //       this.utils.hideSpinner("Interno");
    //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
    //       this.utils.hideSpinner("Interno");
    //       this.utils.openModal(respuesta.modalSalida).then(response => {
    //         console.log(response);
    //       });
    //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
    //       this.utils.hideSpinner("Interno");
    //       this.utils.msgPlain(respuesta.mensajeSalida);
    //     }
    //   });
    // }
  }

  resetCollapsed() {
    for (let index = 0; index < this.facturas.length; index++) {
      this.facturas[index].collapsed = true;

    }
  }

  filterByDate(month: number) {
    if (month > 0) {
      month--;
      this.facturas = this.variablesApi.home.facturas.filter(function (r) {
        let fecha = new Date(r.facturaFechaEmision.replace(/-/g, "/").substring(0, 19));
        return (fecha.getMonth() == month);
      });
    } else {
      this.facturas = this.variablesApi.home.facturas.filter(function (r) {
        let fecha = (new Date(r.facturaFechaEmision.replace(/-/g, "/").substring(0, 19))).getTime();
        let now = (new Date()).getTime();
        let _now = now - (30 * (86400 * 1000));
        return (fecha > _now && fecha <= now);
      });;
    }
  }

}
