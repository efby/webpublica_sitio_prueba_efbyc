import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoFacturasComponent } from './muevo-facturas.component';

describe('MuevoFacturasComponent', () => {
  let component: MuevoFacturasComponent;
  let fixture: ComponentFixture<MuevoFacturasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoFacturasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoFacturasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
