import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoBoletasComponent } from './muevo-boletas.component';

describe('MuevoBoletasComponent', () => {
  let component: MuevoBoletasComponent;
  let fixture: ComponentFixture<MuevoBoletasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoBoletasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoBoletasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
