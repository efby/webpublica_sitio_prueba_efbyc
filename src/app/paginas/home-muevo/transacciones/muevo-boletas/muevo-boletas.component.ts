import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Boletas } from 'src/app/models/boletas';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Router } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { id } from '@swimlane/ngx-charts/release/utils';

@Component({
  selector: 'app-muevo-boletas',
  templateUrl: './muevo-boletas.component.html',
  styleUrls: ['./muevo-boletas.component.css']
})
export class MuevoBoletasComponent implements OnInit {

  boletas: Boletas[];
  boletaDetalle: Boletas;
  @Input() searchMonth: number = 0;
  oldMonth: number = 0;
  interval: any;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private router: Router, private ref: ChangeDetectorRef) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      if (this.searchMonth !== this.oldMonth) {
        this.filterByDate(this.searchMonth);
        this.oldMonth = this.searchMonth;
      }
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
    this.boletas = [];
    this.cargarDatosTransacciones();
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.resetCollapsed();
  }

  cargaFecha() {
    this.variablesApi.home.boletas.forEach(element => {
      element.ventaFechaCreacionString = this.date2Human(element.ventaFechaCreacion);
    });
  }

  modCurrency() {
    this.variablesApi.home.boletas.forEach(element => {
      element.ventaPagoTotalString = this.currency(element.ventaPagoTotal);
    });
  }

  cargarDatosTransacciones() {
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        fechaConsulta: new Date()
      }
    }
    this.utils.showSpinner("Interno");
    this.maquinaHome.homeUsuarioConsultarTransacciones(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.cargaFecha();
        this.modCurrency();
        this.boletas = this.variablesApi.home.boletas;
        this.filterByDate(this.searchMonth);
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  date2Human(date) {
    let format = "dd/MM/yyyy HH:mm";
    return this.utils.date2Human(date, format, this.router.url);
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

  getMaestro(id) {
    return this.variablesApi.home.maestros.tipoFormaPago.filter(r => r.formaPagoId = id)[0].formaPagoTipo;
  }

  consultarDetalleBoleta(ventaId) {
    this.utils.showSpinner("Interno");
    this.resetCollapsed();
    let idComFactura = this.boletas.findIndex(r => r.ventaId === ventaId);
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        ventaId: ventaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarDetalleTransacciones(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
        this.boletaDetalle = respuesta.data.boletas;
        this.boletas[idComFactura].collapsed = false;
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  resetCollapsed() {
    for (let index = 0; index < this.boletas.length; index++) {
      this.boletas[index].collapsed = true;

    }
  }

  filterByDate(month: number) {
    if (month > 0) {
      month--;
      this.boletas = this.variablesApi.home.boletas.filter(function (r) {
        let fecha = new Date(r.ventaFechaCreacion.replace(/-/g, "/").substring(0, 19));
        return (fecha.getMonth() == month);
      });
    } else {
      this.boletas = this.variablesApi.home.boletas.filter(function (r) {
        let fecha = (new Date(r.ventaFechaCreacion.replace(/-/g, "/").substring(0, 19))).getTime();
        let now = (new Date()).getTime();
        let _now = now - (30 * (86400 * 1000));
        return (fecha > _now && fecha <= now);
      });
    }
  }

}
