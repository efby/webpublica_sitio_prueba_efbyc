import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoGuiasComponent } from './muevo-guias.component';

describe('MuevoGuiasComponent', () => {
  let component: MuevoGuiasComponent;
  let fixture: ComponentFixture<MuevoGuiasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoGuiasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoGuiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
