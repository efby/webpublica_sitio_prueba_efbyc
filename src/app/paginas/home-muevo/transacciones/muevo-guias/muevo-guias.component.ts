import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Guias } from 'src/app/models/guias';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Router } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-muevo-guias',
  templateUrl: './muevo-guias.component.html',
  styleUrls: ['./muevo-guias.component.css']
})
export class MuevoGuiasComponent implements OnInit {

  guias: Guias[];
  guiaDetalle: Guias;

  @Input() searchMonth: number = 0;
  oldMonth: number = 0;
  interval: any;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private router: Router, private ref: ChangeDetectorRef) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      if (this.searchMonth !== this.oldMonth) {
        this.filterByDate(this.searchMonth);
        this.oldMonth = this.searchMonth;
      }
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
    this.cargaFecha();
    this.modCurrency();
    this.guias = this.variablesApi.home.guias;
    this.filterByDate(this.searchMonth);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }
  ngOnInit() {
    this.resetCollapsed();
  }

  cargaFecha() {
    this.variablesApi.home.guias.forEach(element => {
      element.ventaFecha = this.date2Human(element.ventaFechaCreacion);
    });
  }

  modCurrency() {
    this.variablesApi.home.guias.forEach(element => {
      element.ventaPagoTotalString = this.currency(element.ventaPagoTotal);
    });
  }

  date2Human(date) {
    let format = "dd/MM/yyyy HH:mm";
    return this.utils.date2Human(date, format, this.router.url);
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

  getMaestro(id) {
    return this.variablesApi.home.maestros.tipoFormaPago.filter(r => r.formaPagoId = id)[0].formaPagoTipo;
  }

  consultarDetalleGuia(ventaId) {
    this.utils.showSpinner("Interno");
    this.resetCollapsed();
    let idComFactura = this.guias.findIndex(r => r.ventaId === ventaId);
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        ventaId: ventaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarDetalleTransacciones(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
        this.guiaDetalle = respuesta.data.boletas;
        this.guias[idComFactura].collapsed = false;
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  resetCollapsed() {
    for (let index = 0; index < this.guias.length; index++) {
      this.guias[index].collapsed = true;

    }
  }

  filterByDate(month: number) {
    if (month > 0) {
      month--;
      this.guias = this.variablesApi.home.guias.filter(function (r) {
        let fecha = new Date(r.ventaFechaCreacion.replace(/-/g, "/").substring(0, 19));
        return (fecha.getMonth() == month);
      });
    } else {
      this.guias = this.variablesApi.home.guias.filter(function (r) {
        let fecha = (new Date(r.ventaFechaCreacion.replace(/-/g, "/").substring(0, 19))).getTime();
        let now = (new Date()).getTime();
        let _now = now - (30 * (86400 * 1000));
        return (fecha > _now && fecha <= now);
      });
    }
  }

}
