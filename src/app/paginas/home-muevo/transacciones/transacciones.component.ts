import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-transacciones',
  templateUrl: './transacciones.component.html',
  styleUrls: ['./transacciones.component.css']
})
export class TransaccionesComponent implements OnInit {
  selTransacciones: number = 3;

  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Mis Documentos", class: "active", url: "" }
  ];
  idMes: number = 0;

  meses = []

  constructor(private emiter: BreadCrumbsService, private utils: UtilitariosService) {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    let month = new Date().getMonth() + 1;
    this.meses = utils.getMeses(month);
  }

  ngOnInit() {
    document.getElementById("selBoletas").focus();
  }

}
