import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';

@Component({
  selector: 'app-revisa-envia-retiro',
  templateUrl: './revisa-envia-retiro.component.html',
  styleUrls: ['./revisa-envia-retiro.component.scss']
})
export class RevisaEnviaRetiroComponent extends PageBase implements OnInit {
  @Input() id: string;
  @Input() cuenta: any;
  @Input() abono: any;

  constructor(private rx: RxService,

    public modalSidebar: SidebarService, public variablesApi: VariablesGenerales, public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    private emiter: BreadCrumbsService,
    public router: Router,) {
    super(ref, utils, router);
  }

  ngOnInit() {

    console.log(this.cuenta)
    console.log(this.abono)
  }

  closeThis() {
    this.modalSidebar.close(this.id);
  }
  validarDesAbono() {


    let msg = {
      title: 'Información importante',
      messages: ["Las solicitudes de retiro pueden tardar hasta 4 dias hábiles. En este periodo no podrás hacer uso de este dinero en la APP."],
      okText: "Confirmar Retiro"
    }

    this.utils.openModal(Paginas.ModalAceptarCancelarComponent, msg
    ).then(result => {
      switch (result) {
        case OpcionesModal.ACEPTAR:
          var body = {
            "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId,
            "abonoId": this.abono.abonoId,
            "monto": this.abono.abonoMontoDisponible,
            "inscripcionId": this.cuenta.inscripcionId
          };
          this.utils.showSpinner("Interno");
          this.rx.llamarPOSTHome(body, this.constantes.RETIRAR_FONDO_BILLETERA).then(result => {


            
            this.utils.hideSpinner("Interno");

            setTimeout(() => {
              this.rx.obtenerWeb().then((result: any) => {
                this.variablesApi.home.cuentaSeleccionada = result.data.cuentaSeleccionada;
                this.variablesApi.home.saldo = result.data.saldo;
                //console.log('cuenta seleccionada: ', this.variablesApi.home.cuentaSeleccionada.cuentaNombre)
                this.variablesApi.home.invitaciones = result.data.invitaciones;
                this.variablesApi.home.usuarioLogIn = result.data.usuarioLogin;
                this.variablesApi.home.vehiculos = result.data.vehiculos;
                this.variablesApi.home.conductores = result.data.conductores;
                this.variablesApi.home.usuarios = result.data.usuarios;
                this.variablesApi.home.facturasPendientesPago = result.data.facturasPendiente;
                this.variablesApi.home.primerIngreso = result.data.primerIngreso;
                this.variablesApi.home.montoTotalMes = result.data.montoTotalMes;
                this.variablesApi.home.tipoFormaPago = result.data.tiposFormaPago;
                this.variablesApi.home.menu = result.data.menu;
                this.variablesApi.home.cuentas = result.data.cuentas;
                this.variablesApi.home.mediosPago = result.data.mediosPago;
                this.variablesApi.home.datosSaldo = result.data.datosSaldo;
                this.variablesApi.home.tipoDocumento = result.data.tipoDocumento;
                this.variablesApi.home.cuentaDatosDte = result.data.cuentaDatosDte;
                this.variablesApi.home.notificaciones = result.data.notificaciones;
                this.variablesApi.home.fuerzaCambioCuenta = result.data.fuerzaCambioCuenta;
                this.variablesApi.home.rolPrivilegiosFront = result.data.rolPrivilegiosFront;
            })
              let msg_2 = {
                title: 'Retiro exitoso',
                messages: [result.data.message],

              }
              this.utils.openModal(Paginas.ModalAceptar, msg_2).then(a => {
                this.modalSidebar.closeAll()
              })
            }, 400);

          }, this.errorCallback)
          break;
        case OpcionesModal.CANCELAR:
          console.log("NOK")
          // this.closeThis()
          break;
      }

    });
  }

}
