import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoAbonoProcesandoComponent } from './muevo-abono-procesando.component';

describe('MuevoAbonoProcesandoComponent', () => {
  let component: MuevoAbonoProcesandoComponent;
  let fixture: ComponentFixture<MuevoAbonoProcesandoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoAbonoProcesandoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoAbonoProcesandoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
