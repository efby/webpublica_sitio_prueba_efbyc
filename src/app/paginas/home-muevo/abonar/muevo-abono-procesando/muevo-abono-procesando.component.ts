import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { Cuenta, Tarjeta } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-muevo-abono-procesando',
  templateUrl: './muevo-abono-procesando.component.html',
  styleUrls: ['./muevo-abono-procesando.component.css']
})
export class MuevoAbonoProcesandoComponent implements OnInit {

  cuentaSeleccionada: Cuenta;
  tarjeta: Tarjeta;
  idTarjeta: string = "";
  montoAbonar: string = "";
  abonoProgress: number = 180;
  intervalo: any;
  fechaActual: string = "";
  procesando: boolean = false;
  error: boolean = false;

  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Abonar a Billetera Muevo", class: "", url: "/" + Paginas.HomeMuevoAbonarBilletera },
    { title: "Procesando", class: "active", url: "" }
  ];
  interval: any;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, private router: Router, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private ref: ChangeDetectorRef, private route: ActivatedRoute) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.montoAbonar = this.route.snapshot.paramMap.get('valorMonto');
    this.idTarjeta = this.route.snapshot.paramMap.get('idInscripcion');
    this.cuentaSeleccionada = this.variablesApi.home.cuentaSeleccionada;
    this.tarjeta = this.variablesApi.home.tarjetas.filter(r => r.idInscripcion == this.idTarjeta)[0];
    this.fechaActual = this.date2Human(new Date());
    this.homeCuentaAbonoPost();
  }

  homeCuentaAbonoPost() {
    this.startTimer();
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.cuentaSeleccionada.cuentaId,
        monto: parseInt(this.montoAbonar.replace(/\./g, "")),
        idInscripcion: this.idTarjeta
      }
    };

    this.maquinaHome.homeUsuarioCuentaAbonoPost(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.closeTimer();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.procesando = false;
        this.error = true;
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  date2Human(date) {
    let format = 'dd/MM/yyyy   HH:mm';
    return this.utils.date2Human(date, format, this.router.url);
  }

  startTimer() {
    this.intervalo = setInterval(() => {
      if (this.abonoProgress > 0) {
        this.abonoProgress--;
      } else {
        // this.procesando = false;
        clearInterval(this.intervalo);
      }
    }, 1000);
  }

  closeTimer() {
    this.procesando = true;
    this.abonoProgress = 0;
  }

}
