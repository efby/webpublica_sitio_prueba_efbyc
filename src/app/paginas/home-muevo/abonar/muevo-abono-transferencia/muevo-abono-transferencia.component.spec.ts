import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoAbonoTransferenciaComponent } from './muevo-abono-transferencia.component';

describe('MuevoAbonoTransferenciaComponent', () => {
  let component: MuevoAbonoTransferenciaComponent;
  let fixture: ComponentFixture<MuevoAbonoTransferenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoAbonoTransferenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoAbonoTransferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
