import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constantes } from 'src/app/globales/constantes';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../pageBase';
import { SeleccionarCuentaBancariaRetiroComponent } from '../seleccionar-cuenta-bancaria-retiro/seleccionar-cuenta-bancaria-retiro.component';

@Component({
  selector: 'app-retirar-abono',
  templateUrl: './retirar-abono.component.html',
  styleUrls: ['./retirar-abono.component.scss']
})
export class RetirarAbonoComponent extends PageBase implements OnInit {
  @Input() id: string;
  @Input() abonos: RetirarAbono;

  constructor(private rx: RxService,

    private modalSidebar: SidebarService, private variablesApi: VariablesGenerales, public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    public router: Router,) {
    super(ref, utils, router);
  }

  ngOnInit() {

    console.log(this.abonos)
  }
  closeThis() {
    this.modalSidebar.close(this.id);
  }

  next() {
    var body = { "cuentaId": this.variablesApi.home.cuentaSeleccionada.cuentaId };

    body["posicionInicial"] = this.abonos.posicionFinal;

    this._isLoading = true
    this.rx.llamarPOSTHome(body, this.constantes.CUENTA_CONSULTAR_ABONOS).then(result => {
      this.abonos.cantidadRegistrosPorConsulta = result.data.data.cantidadRegistrosPorConsulta;
      this.abonos.posicionFinal = result.data.data.posicionFinal;
      this.abonos.abonos = this.abonos.abonos.concat(result.data.data.abonos)
      setTimeout(() => {
        this._isLoading = false
        this.ref.detectChanges()
        console.log(this.abonos.abonos)
      }, 40);



    }, this.errorCallback)
  }

  seleccionarCuentaBancaria(abono) {
    var body = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId
    }
    this.utils.showSpinner("Interno");
    this.rx.llamarPOSTHome(body, this.constantes.FORMA_DE_PAGO_GET).then(result => {
     
      var modal = this.modalSidebar.open(SeleccionarCuentaBancariaRetiroComponent, { miBackdrop: false })
      modal.refModal.componentInstance.abono = abono;
      
      modal.refModal.componentInstance.cuentasBancarias=result.data.data.cuentasCorriente
      this.utils.hideSpinner("Interno");
    }, this.errorCallback)

  }

}

export interface RetirarAbono {
  abonos: Array<Abono>;
  cantidadRegistrosPorConsulta: number;
  posicionFinal: number
}
export interface Abono {
  abonoFechaCreacion: string
  abonoId: number
  abonoMontoDisponible: number
  abonoMontoTotal: number
}
