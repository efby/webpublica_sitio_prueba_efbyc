import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales, TipoSaldo } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { ActivatedRoute } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-abonar',
  templateUrl: './abonar.component.html',
  styleUrls: ['./abonar.component.css']
})
export class AbonarComponent implements OnInit {

  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Formas de Pago", class: "", url: "/" + Paginas.HomeMuevoMisMetodosPago },
    { title: "Abonar a Billetera Muevo", class: "active", url: "" }
  ];

  _abonarTarjetas: boolean = false;
  _abonarTransferencia: boolean = false;
  tipoAbono: number = 0;
  saldo: TipoSaldo;

  constructor(private variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, private route: ActivatedRoute, private utils: UtilitariosService) { }

  ngOnInit() {
    this.modCurrency();
    this.saldo = this.variablesApi.home.datosSaldo;
    this.tipoAbono = parseInt(this.route.snapshot.paramMap.get('idTipoAbono'));
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    switch (this.tipoAbono) {
      case 0:
        this.activateTarjetas();
        break;
      case 1:
        this.activateTransferencia();
        break;
      default:
        this.activateTarjetas();
        break;
    }
  }

  modCurrency() {
    // this.variablesApi.home.datosSaldo.saldoString = this.utils.formatCurrency("" + this.variablesApi.home.saldo.saldo);
    // this.variablesApi.home.datosSaldo.saldoFinalString = this.utils.formatCurrency("" + this.variablesApi.home.saldo.saldoFinal);
  }

  activateTarjetas() {
    this._abonarTarjetas = true;
    this._abonarTransferencia = false;
  }

  activateTransferencia() {
    this._abonarTarjetas = false;
    this._abonarTransferencia = true;
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

}
