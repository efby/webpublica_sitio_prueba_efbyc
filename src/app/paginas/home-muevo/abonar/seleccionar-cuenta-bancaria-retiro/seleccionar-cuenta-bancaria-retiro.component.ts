import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CuentaCorriente } from 'src/app/globales/business-objects';
import { Constantes } from 'src/app/globales/constantes';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { SidebarService } from 'src/app/services/sidebar-services';
import { BreadCrumbsService, IEmmitModel } from 'src/app/servicios/bread-crumbs.service';
import { RxService } from 'src/app/servicios/rx.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { CuentasBancariasComponent } from '../../metodos-pago/abono/cuentas-bancarias/cuentas-bancarias.component';
import { PageBase } from '../../pageBase';
import { RevisaEnviaRetiroComponent } from '../revisa-envia-retiro/revisa-envia-retiro.component';

@Component({
  selector: 'app-seleccionar-cuenta-bancaria-retiro',
  templateUrl: './seleccionar-cuenta-bancaria-retiro.component.html',
  styleUrls: ['./seleccionar-cuenta-bancaria-retiro.component.scss']
})
export class SeleccionarCuentaBancariaRetiroComponent extends PageBase implements OnInit {
  CURRENT_PAGE: string = Paginas.ModalMediosPagoCuentasBancarias;

  @Input() id: string;
  @Input() cuentasBancarias: any[];
  @Input() abono: any;

  constructor(private rx: RxService,

    public modalSidebar: SidebarService, public variablesApi: VariablesGenerales, public ref: ChangeDetectorRef,
    public utils: UtilitariosService,
    private constantes: Constantes,
    private emiter: BreadCrumbsService,
    public router: Router,) {
    super(ref, utils, router);

  }

  ngOnInit() {
    console.log(this.cuentasBancarias)

  }

  closeThis() {
    this.modalSidebar.close(this.id);
  }

  revisaEnviaRetiro(cuenta) {
    var modal = this.modalSidebar.open(RevisaEnviaRetiroComponent, { miBackdrop: false })
    modal.refModal.componentInstance.cuenta = cuenta;
    modal.refModal.componentInstance.abono = this.abono;
  }
  inscribirCuentaBancaria() {
    this.modalSidebar.closeAll();
    setTimeout(() => {
      let config: IEmmitModel = {
        tipo: 'CuentaCorriente',
        action: 'OPEN',
        data: {
          origin: "this.CURRENT_PAGE"
        }
      }
      this.emiter.emmitChange(config);
    }, 500);

  }

}
