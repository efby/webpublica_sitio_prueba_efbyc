import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales, Cuentas } from 'src/app/globales/variables-generales';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { Cuenta, Tarjeta } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-muevo-abono-tarjetas',
  templateUrl: './muevo-abono-tarjetas.component.html',
  styleUrls: ['./muevo-abono-tarjetas.component.css']
})
export class MuevoAbonoTarjetasComponent implements OnInit {
  cuentaSeleccionada: Cuenta;
  //empresa: Cuentas;
  tarjetas?: Tarjeta[];
  idTarjeta: string = "";
  montoAbonar: string = "";
  interval: any;
  isMobile: boolean = false;

  constructor(private emiter: BreadCrumbsService, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private router: Router, private ref: ChangeDetectorRef, private route: ActivatedRoute) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.isMobile = this.variablesApi.esMobile;
    }, 10);
    this.cuentaSeleccionada = this.variablesApi.home.cuentaSeleccionada;
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.cargaDataInicial();
  }


  homeCuentaAbonoPost() {
    if (this.montoAbonar !== "" && this.idTarjeta !== "") {
      this.router.navigate([Paginas.HomeMuevoAbonarProcesando, { valorMonto: parseInt(this.montoAbonar.replace(/\./g, "")), idInscripcion: this.idTarjeta }]);
    } else {
      this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "Datos Faltantes", textoMensaje: "Para Abonar necesita seleccionar una tarjeta e indicar un monto" });
    }
  }
  //, 

  cargaDataInicial() {
    this.tarjetas = this.variablesApi.home.tarjetas;
    this.idTarjeta = this.variablesApi.home.tarjetaCompartidaId;
  }

  validaInput(ingreso: any) {
    let item = ingreso.srcElement;
    item.value = this.utils.formatNumbers(item.value);
    item.value = this.utils.formatCurrency("" + item.value);
  }

}
