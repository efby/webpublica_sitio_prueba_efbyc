import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoAbonoTarjetasComponent } from './muevo-abono-tarjetas.component';

describe('MuevoAbonoTarjetasComponent', () => {
  let component: MuevoAbonoTarjetasComponent;
  let fixture: ComponentFixture<MuevoAbonoTarjetasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoAbonoTarjetasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoAbonoTarjetasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
