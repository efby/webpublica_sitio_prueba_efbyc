import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Movimiento, VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Router } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-historial-todos',
  templateUrl: './historial-todos.component.html',
  styleUrls: ['./historial-todos.component.css']
})
export class HistorialTodosComponent implements OnInit {

  movimientos: Movimiento[];
  interval: any;
  @Input() searchMonth: number = 0;
  oldMonth: number = 0;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private router: Router, private ref: ChangeDetectorRef) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      if (this.searchMonth !== this.oldMonth) {
        this.filterByDate(this.searchMonth);
        this.oldMonth = this.searchMonth;
      }
      this.isMobile = this.variablesApi.esMobile
    }, 0);
    this.cargarDatosMovimientos();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
  }

  modCurrency() {
    this.variablesApi.home.movimientos.forEach(element => {
      element.movimientoMontoString = this.currency(element.movimientoMonto);
      element.movimientoSaldoFinalString = this.currency(element.movimientoSaldoFinal);
    });
  }

  cargaFecha() {
    this.variablesApi.home.movimientos.forEach(element => {
      element.movimientoFechaCreacionString = this.date2Human(element.movimientoFechaCreacion);
    });
  }

  cargarDatosMovimientos() {
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        fechaConsulta: new Date()
      }
    }
    this.utils.showSpinner("Interno");
    this.maquinaHome.homeUsuarioConsultarMovimientos(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.cargaFecha();
        this.modCurrency();
        this.movimientos = this.variablesApi.home.movimientos;
        this.filterByDate(this.searchMonth);
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  date2Human(date) {
    let format = "dd/MM/yyyy HH:mm";
    return this.utils.date2Human(date, format, this.router.url);
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

  filterByDate(month: number) {
    if (month > 0) {
      month--;
      this.movimientos = this.variablesApi.home.movimientos.filter(function (r) {
        let fecha = new Date(r.movimientoFechaCreacion.replace(/-/g, "/").substring(0, 19));
        return (fecha.getMonth() == month);
      });
    } else {
      this.movimientos = this.variablesApi.home.movimientos.filter(function (r) {
        let fecha = (new Date(r.movimientoFechaCreacion.replace(/-/g, "/").substring(0, 19))).getTime();
        let now = (new Date()).getTime();
        let _now = now - (30 * (86400 * 1000));
        return (fecha > _now && fecha <= now);
      });
    }
  }

}
