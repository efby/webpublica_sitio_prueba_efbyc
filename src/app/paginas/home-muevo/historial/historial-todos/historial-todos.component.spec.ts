import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialTodosComponent } from './historial-todos.component';

describe('HistorialTodosComponent', () => {
  let component: HistorialTodosComponent;
  let fixture: ComponentFixture<HistorialTodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialTodosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialTodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
