import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales, Movimiento } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {
  migajas = [
    { title: "Dashboard", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Mis Movimientos", class: "active", url: "" }
  ];
  idMes: number = 0;

  meses = []

  selTipoMovimiento: number = 0;

  constructor(private emiter: BreadCrumbsService, private variableApi: VariablesGenerales, private utils: UtilitariosService, private ref: ChangeDetectorRef) {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    let month = new Date().getMonth() + 1;
    this.meses = utils.getMeses(month);
  }

  ngOnInit() {
    document.getElementById("historialTodos").focus()
  }

}
