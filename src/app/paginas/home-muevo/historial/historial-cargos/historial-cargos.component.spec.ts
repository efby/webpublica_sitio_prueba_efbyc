import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialCargosComponent } from './historial-cargos.component';

describe('HistorialCargosComponent', () => {
  let component: HistorialCargosComponent;
  let fixture: ComponentFixture<HistorialCargosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialCargosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialCargosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
