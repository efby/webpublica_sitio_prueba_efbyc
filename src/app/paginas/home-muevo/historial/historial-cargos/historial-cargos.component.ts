import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Movimiento, VariablesGenerales } from 'src/app/globales/variables-generales';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-historial-cargos',
  templateUrl: './historial-cargos.component.html',
  styleUrls: ['./historial-cargos.component.css']
})
export class HistorialCargosComponent implements OnInit {

  movimientos: Movimiento[];
  interval: any;
  @Input() searchMonth: number = 0;
  oldMonth: number = 0;
  isMobile: boolean = false;

  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private utils: UtilitariosService, private ref: ChangeDetectorRef, private router: Router) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      if (this.searchMonth !== this.oldMonth) {
        this.filterByDate(this.searchMonth);
        this.oldMonth = this.searchMonth;
      }
      this.isMobile = this.variablesApi.esMobile
    }, 0);
    this.movimientos = this.variablesApi.home.movimientos.filter(r => r.tipoMovimientoNombre == 'CARGO');
    this.filterByDate(this.searchMonth);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {

  }

  date2Human(date) {
    let format = "dd/MM/yyyy HH:mm";
    return this.utils.date2Human(date, format, this.router.url);
  }

  currency(val) {
    return this.utils.formatCurrency("" + val);
  }

  filterByDate(month: number) {
    let cargos = this.variablesApi.home.movimientos.filter(r => r.tipoMovimientoNombre == 'CARGO');
    if (month > 0) {
      month--;
      this.movimientos = cargos.filter(function (r) {
        let fecha = new Date(r.movimientoFechaCreacion.replace(/-/g, "/").substring(0, 19));
        return (fecha.getMonth() == month);
      });
    } else {
      this.movimientos = cargos.filter(function (r) {
        let fecha = (new Date(r.movimientoFechaCreacion.replace(/-/g, "/").substring(0, 19))).getTime();
        let now = (new Date()).getTime();
        let _now = now - (30 * (86400 * 1000));
        return (fecha > _now && fecha <= now);
      });
    }
  }

}
