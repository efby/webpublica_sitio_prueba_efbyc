import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { VariablesGenerales, Cuentas, Grupos, Flotas } from 'src/app/globales/variables-generales';
import { Router } from '@angular/router';
import { Paginas } from 'src/app/globales/paginas';
import { Conductor } from 'src/app/models/conductores';
import { MedioPago, Invitacion, Cuenta } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  _false: boolean = false;
  _true: boolean = true;
  _confServicios: boolean = false;
  _isConfiguracion: boolean = false;
  empresaActual: string = "";
  _dropEmpresa: boolean = true;
  breadCrumbs: any;
  muestraBreadcrumbs: boolean;
  muestraBanner: boolean;
  _invitaciones: boolean = false;
  _enableSaveInvitaciones: boolean = false;
  _opened: boolean = false;
  _estaciones: boolean = false;
  _conductores: boolean = false;
  _invitados: boolean = false;
  _diaHorario: boolean = false;
  _cuentaCorriente: boolean = false;
  _edicion: boolean = false;
  _estacionesPersonalizado: boolean = true;
  time = { hour: 8, minute: 0 };
  collapseComuna: boolean = false;
  _activo1: boolean = true;
  _activo2: boolean = false;

  cuentaCorriente_cuentaId: number;
  cuentaCorriente_nombre: string;
  cuentaCorriente_banco: string;
  cuentaCorriente_Titular: string;
  cuentaCorriente_rut: string;
  cuentaCorriente_nroCuenta: number;
  cuentaCorriente_email: string;
  inputNvaGrupo: string = "";
  inputNvaFlota: string = "";
  idConductor: number;
  idVehiculo: number;
  idInvitado: number;

  empresas: Cuenta[] = [];
  medioPago: MedioPago;
  invitaciones: Invitacion[] = [];
  grupos: Grupos[] = [];
  conductores: Conductor[] = [];
  flotas: Flotas[] = [];
  invitado: Invitacion = new Invitacion();


  constructor(private emiter: BreadCrumbsService, private maquinaHome: MaquinaHome, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private router: Router) {
    // override the route reuse strategy
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    this.empresas = this.variablesApi.home.cuentas;
    this.invitaciones = this.variablesApi.home.invitaciones;
    this.conductores = this.variablesApi.home.conductores;

    this.emiter.changeEmitted$.subscribe(data => {
      this.internalListener(data);
    });



    if (this.variablesApi.home.invitaciones.length > 0) {
      this._invitaciones = true;
    } else {
      this._invitaciones = false;
    }
    // this.accionInicio();
    this.setSelectedEmpresa();

  }

  private internalListener(data) {
    if (data.migajas != null) {
      this.breadCrumbs = data.migajas;
      this.muestraBreadcrumbs = data.muestraBreadcrumbs;
      this._isConfiguracion = false;
    } else {
      switch (data.tipo) {
        case "Estacion":
          this._estaciones = data.estacion;
          break;
        case "DiaHorario":
          this._diaHorario = data.diaHorario;
          break;
        case "Conductores":
          this._conductores = data.Conductores;
          this.idConductor = data.idConductor;
          break;
        case "Invitaciones":
          this._invitados = data.Invitaciones;
          this.idInvitado = data.idInvitacion;
          this.invitado = this.variablesApi.home.invitados.filter(r => r.invitacionId === data.idInvitacion)[0];
          break;
        case "CuentaCorriente":
          this._cuentaCorriente = data.cuentaCorriente
          this._edicion = data.edicion;
          this.cuentaCorriente_nombre = "";
          this.cuentaCorriente_banco = "";
          this.cuentaCorriente_Titular = "";
          this.cuentaCorriente_rut = "";
          this.cuentaCorriente_nroCuenta = 0;
          this.cuentaCorriente_email = "";
          break;
        case "Configuracion":
          this._isConfiguracion = data._isConfiguracion;
          break;
        default:
          break;
      }
    }
  }

  private setSelectedEmpresa() {
    if (this.empresas.length > 1) {
      this._dropEmpresa = true;
      // this.empresas.forEach(element => {
      //   if (element.cuentaDefault) {
      //     this.empresaActual = element.cuentaNombre;
      //     this.cuentaCorriente_cuentaId = element.cuentaId;
      //   }
      // });
    } else if (this.empresas.length > 0) {
      this._dropEmpresa = false;
      this.empresaActual = this.empresas[0].cuentaNombre;
      this.cuentaCorriente_cuentaId = this.empresas[0].cuentaId;
    } else {
      this._dropEmpresa = false;
      this.empresaActual = "Sin Cuenta";
    }
  }

  _toggleSidebar() {
    this._opened = !this._opened;
  }

  restriccionEstacionPersonalizado(val) {
    if (val == 1) {
      this._estacionesPersonalizado = false;
    } else {
      this._estacionesPersonalizado = true;
    }
  }

  changeEmpresa(id) {
    // this.empresas.forEach(element => {
    //   element.cuentaDefault = false
    // });


    //this.empresas[idComponente].cuentaDefault = true;
    this.setSelectedEmpresa();
    this.accionCambiarEmpresaSeleccionada(id);
  }

  accionCambiarEmpresaSeleccionada(cuentaId: number) {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: cuentaId,
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        usuarioId: this.variablesApi.home.usuarioLogIn.usuarioId
      }
    };

    this.maquinaHome.homeCambiarCuentaSeleccionada(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.router.navigate([respuesta.paginaRespuesta]);
      }
    });

  }

  accionCuentaCorriente() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.cuentaCorriente_cuentaId,
        cuentaCorrienteNumero: this.cuentaCorriente_nroCuenta,
        cuentaCorrienteRut: this.cuentaCorriente_rut,
        cuentaCorrienteNombreTitular: this.cuentaCorriente_Titular,
        cuentaCorrienteEmailTitular: this.cuentaCorriente_email,
        cuentaCorrienteNombre: this.cuentaCorriente_nombre,
        cuentaCorrienteTipo: 1,
        bancoId: this.cuentaCorriente_banco
      }
    };

    this.maquinaHome.homeCrearCuentaCorriente(action).then(respuesta => {
      console.log(respuesta);
      this.utils.hideSpinner("Interno");
    });

  }

  accionInicio() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_INICIO,
      datos: {}
    };

    this.maquinaHome.homeInicio(action).then(respuesta => {
      console.log(respuesta);
      this.utils.hideSpinner("Interno");
    });

  }

  fnInvitaciones(id) {
    let idComponente = this.variablesApi.home.invitaciones.findIndex(r => r.invitacionId = id);
    let data = {
      idInvitacion: id,
      nombreInvitado: (this.variablesApi.home.usuarioLogIn.usuarioNombre + " " + this.variablesApi.home.usuarioLogIn.usuarioApellido),
      rolInvitado: "Conductor",
      vigenciaInvitacion: "vigencia",
      nombreEmpresa: this.variablesApi.home.invitaciones[idComponente].invitacionNombreCuenta,
    };

    this.utils.openModal(Paginas.ModalAceptarInvitacionComponent, data).then(response => {
      console.log(response);
    });
  }

  private maquinaModificarConductor() {
    // console.log(this.idConductor);
    // let conductor = this.conductores.filter(r => r.usuarioId === this.idConductor)[0];
    // let grupo = this.grupos.filter(r => r.seleccionado === true)[0];

    // this.utils.showSpinner("Interno");
    // let actualUrl = this.router.url.substr(1);

    // let action: RequestTipo = {
    //   accion: AccionTipo.ACCION_CLICK,
    //   datos: {
    //     cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
    //     usuarioId: this.idConductor,
    //     grupoCuentaId: grupo.grupoId,
    //     usuarioCuentaEstadoId: 1,
    //     rolId: 1,
    //     usuarioCuentaConduce: conductor.conduce,
    //     usuarioCuentaRindeGasto: conductor.usuarioCuentaRindeGasto,
    //     usuarioCuentaCreaVehiculo: conductor.usuarioCuentaCreaVehiculo,
    //     usuarioCuentaEligeDTE: conductor.usuarioCuentaEligeDTE,
    //     usuarioCuentaSeleccionado: conductor.usuarioCuentaSeleccionado,
    //     vehiculosAsignados: [],
    //     estadoCombustible: 1,
    //     montoMaximoDiaValor: conductor.limiteDiarioCombustible[0],
    //     montoMaximoMesValor: conductor.limiteMensualCombustible[0]
    //   }
    // }
    // console.log(action);
    // this.maquinaHome.homeUsuarioModificar(action).then(respuesta => {
    //   if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
    //     this.utils.hideSpinner("Interno");
    //     // if (actualUrl !== respuesta.paginaRespuesta) {
    //     //   this.router.navigate([respuesta.paginaRespuesta]);
    //     // } else {
    //     // this.ngOnInit();
    //     // }
    //     this.ngOnInit();
    //     this.router.navigate([Paginas.HomeGruposConductores, conductor.grupoId]);
    //   } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
    //     this.utils.hideSpinner("Interno");
    //     this.utils.openModal(respuesta.modalSalida).then(response => {
    //       console.log(response);
    //     });
    //   } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
    //     this.utils.hideSpinner("Interno");
    //     this.utils.msgPlain(respuesta.mensajeSalida);

    //   }
    // });
  }

  selFlota(id) {
    for (let index = 0; index < this.flotas.length; index++) {
      this.flotas[index].seleccionado = false;

    }
    let idComponent = this.flotas.findIndex(r => r.flotaId === id);
    this.flotas[idComponent].seleccionado = true;
  }

  // nvaFlota() {
  //   if (this.inputNvaFlota !== "") {
  //     this.maquinaAgregarFlota();
  //   } else {
  //     this.maquinaEditarVehiculo();
  //   }
  // }

  private maquinaAgregarFlota() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        flotaCuentaNombre: this.inputNvaFlota
      }
    }

    this.maquinaHome.homeFlotaCrear(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {

        this.ngOnInit();

        // }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  // private maquinaEditarVehiculo() {
  //   this.utils.showSpinner("Interno");

  //   let element = this.variablesApi.home.vehiculos.filter(r => r.vehiculoId === this.idVehiculo)[0];
  //   let actualUrl = this.router.url.substr(1);
  //   let action: RequestTipo = {
  //     accion: AccionTipo.ACCION_CLICK,
  //     datos: {
  //       cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
  //       vehiculoCuentaId: this.idVehiculo,
  //       vehiculoCuentaPatente: element.patente,
  //       vehiculoCuentaModelo: element.modelo,
  //       vehiculoCuentaMarca: element.marca,
  //       vehiculoCuentaAno: (element.ano == null) ? 0 : element.ano,
  //       vehiculoCuentaTanque: element.tanque,
  //       vehiculoCuentaAlias: element.alias,
  //       vehiculoCuentaTipo: element.tipo,
  //       vehiculoCuentaValidado: 1,
  //       vehiculoCuentaEstado: 1
  //     }
  //   }

  //   this.maquinaHome.homeVehiculoModificar(action).then(respuesta => {
  //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
  //       this.utils.hideSpinner("Interno");
  //       // if (actualUrl !== respuesta.paginaRespuesta) {
  //       //   this.router.navigate([respuesta.paginaRespuesta]);
  //       // } else {
  //       // this.ngOnInit();
  //       // }
  //       this.router.navigate([Paginas.HomeFlotasVehiculo, element.flotaId]);
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
  //       this.utils.hideSpinner("Interno");
  //       this.utils.openModal(respuesta.modalSalida).then(response => {
  //         console.log(response);
  //       });
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
  //       this.utils.hideSpinner("Interno");
  //       this.utils.msgPlain(respuesta.mensajeSalida);

  //     }
  //   });
  // }

  activaCombustible() {
    this.invitado.activaCombustible = !this.invitado.activaCombustible;
    this.enableSaveInvitaciones();
  }

  activaAlimentacion() {
    this.invitado.activaAlimentacion = !this.invitado.activaAlimentacion;
    this.enableSaveInvitaciones();
  }

  enableSaveInvitaciones() {
    let _foto: boolean = false;
    let _licencia: boolean = false;
    let _combustible: boolean = false;
    let _alimentacion: boolean = false;
    console.log(this.invitado.invitacionValidaciones.invitacionSolicitaFoto, this.invitado.invitacionValidaciones.invitacionSolicitaLicencia, this.invitado.activaCombustible, this.invitado.activaAlimentacion);

    if (this.invitado.invitacionValidaciones.invitacionSolicitaFoto == 1) {
      if (this.invitado.invitacionValidaciones.aceptaFoto) {
        _foto = true;
      }
    } else {
      _foto = true;
    }

    if (this.invitado.invitacionValidaciones.invitacionSolicitaLicencia == 1) {
      if (this.invitado.invitacionValidaciones.aceptaLicencia) {
        _licencia = true;
      }
    } else {
      _licencia = true;
    }

    if (this.invitado.activaCombustible) {
      if (this.invitado.combustibleMontoDiario > 0 && this.invitado.combustibleMontoMensual > 0) {
        _combustible = true;
      }
    } else {
      _combustible = true;
    }

    if (this.invitado.activaAlimentacion) {
      if (this.invitado.alimentacionMontoDiario > 0 && this.invitado.alimentacionMontoMensual > 0) {
        _alimentacion = true;
      }
    } else {
      _alimentacion = true;
    }

    if (_foto && _licencia && _combustible && _alimentacion) {
      this._enableSaveInvitaciones = true;
    } else {
      this._enableSaveInvitaciones = false;
    }

  }

  autorizarInvitacion(invitacionId: number, autorizacion: number) {
    this.utils.showSpinner("Interno");

    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        invitacionId: invitacionId,
        invitacionAutorizada: autorizacion,
        combustibleEstado: this.invitado.activaCombustible ? 1 : 0,
        combustibleMontoMaximoDiaValor: this.invitado.combustibleMontoDiario,
        combustibleMontoMaximoMesValor: this.invitado.combustibleMontoMensual
      }
      //{"tipoSaldoId":1, "montoMaximoDiaValor":50000, "montoMaximoMesValor":100000}
    }
    console.log(action);
    this.maquinaHome.homeUsuarioAutorizaInvitacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
        this.utils.hideSpinner("Interno");
        this.router.navigate([Paginas.HomeGruposConductores]);
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

}
