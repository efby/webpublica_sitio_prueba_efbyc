import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-detalle-gastos',
  templateUrl: './detalle-gastos.component.html',
  styleUrls: ['./detalle-gastos.component.css']
})
export class DetalleGastosComponent implements OnInit {
  selBtnDetalleGasto = 0;
  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

}
