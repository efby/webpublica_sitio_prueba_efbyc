import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-detalle-vehiculos',
  templateUrl: './detalle-vehiculos.component.html',
  styleUrls: ['./detalle-vehiculos.component.css']
})
export class DetalleVehiculosComponent implements OnInit {
  vehiculos = this.variablesApi.home.vehiculos;

  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
    this.vehiculos = this.variablesApi.home.vehiculos;
  }

}