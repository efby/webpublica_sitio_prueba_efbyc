import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleVehiculosComponent } from './detalle-vehiculos.component';

describe('DetalleVehiculosComponent', () => {
  let component: DetalleVehiculosComponent;
  let fixture: ComponentFixture<DetalleVehiculosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleVehiculosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleVehiculosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
