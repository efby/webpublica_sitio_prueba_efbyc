import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleGastosComponent } from './detalle-gastos.component';

describe('DetalleGastosComponent', () => {
  let component: DetalleGastosComponent;
  let fixture: ComponentFixture<DetalleGastosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleGastosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleGastosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
