import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficoGastoGeneralComponent } from './grafico-gasto-general.component';

describe('GraficoGastoGeneralComponent', () => {
  let component: GraficoGastoGeneralComponent;
  let fixture: ComponentFixture<GraficoGastoGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficoGastoGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficoGastoGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
