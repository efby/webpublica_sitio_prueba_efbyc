import { Component, OnInit } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-grafico-gasto-general',
  templateUrl: './grafico-gasto-general.component.html',
  styleUrls: ['./grafico-gasto-general.component.css']
})
export class GraficoGastoGeneralComponent implements OnInit {

  single: any = [
    {
      "name": "Empresa",
      "series": []
    }
  ]
  multi: any[];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Dias';
  showYAxisLabel = true;
  yAxisLabel = 'Gastos';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private variablesApi: VariablesGenerales) {
    Object.assign(this, this.single)
  }

  onSelect(event) {
    console.log(event);
  }
  ngOnInit() {
    let series = [];

    for (let index = 30; index > 0; index--) {
      let d = new Date();
      d.setDate(d.getDate() - index);
      let val = Math.round((Math.random() * 1000000));
      series.push({ name: d.toDateString(), value: val });
    }
    this.single[0].series = series;
  }

}
