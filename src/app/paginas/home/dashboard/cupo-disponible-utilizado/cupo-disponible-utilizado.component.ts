import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { TipoMedioPago } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-cupo-disponible-utilizado',
  templateUrl: './cupo-disponible-utilizado.component.html',
  styleUrls: ['./cupo-disponible-utilizado.component.css']
})
export class CupoDisponibleUtilizadoComponent implements OnInit {
  _abono: boolean = false;
  _credito: boolean = true;
  cupoCredito: any = {
    total: "10.000.000",
    utilizado: "8.300.000",
    disponible: "1.700.000",
    porcentaje: "+25%"
  };
  saldo: any = {
    montoSaldo: 0
  };


  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {

  }

}
