import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CupoDisponibleUtilizadoComponent } from './cupo-disponible-utilizado.component';

describe('CupoDisponibleUtilizadoComponent', () => {
  let component: CupoDisponibleUtilizadoComponent;
  let fixture: ComponentFixture<CupoDisponibleUtilizadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CupoDisponibleUtilizadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CupoDisponibleUtilizadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
