import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-solicitudes-permisos',
  templateUrl: './solicitudes-permisos.component.html',
  styleUrls: ['./solicitudes-permisos.component.css']
})
export class SolicitudesPermisosComponent implements OnInit {

  solicitudes: any = [
    {
      tiempo: "1 min atrás",
      nombreConductor: "Juan Aravena",
      patente: "HHYY20",
      accion: "Desbloquear estación Pedro de Valdivia #22038, Providencia.↗"
    },
    {
      tiempo: "10 min atrás",
      nombreConductor: "Rodrigo Zamorano",
      patente: "AHEI90",
      accion: "Desbloquear estación Av Tobalaba #19384.↗"
    },
    {
      tiempo: "10 min atrás",
      nombreConductor: "Rodrigo Zamorano",
      patente: "AHEI90",
      accion: "Aumento de monto máximo diario.↗"
    }
  ]
  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

}
