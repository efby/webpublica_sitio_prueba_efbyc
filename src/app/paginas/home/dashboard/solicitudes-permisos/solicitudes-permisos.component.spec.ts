import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesPermisosComponent } from './solicitudes-permisos.component';

describe('SolicitudesPermisosComponent', () => {
  let component: SolicitudesPermisosComponent;
  let fixture: ComponentFixture<SolicitudesPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
