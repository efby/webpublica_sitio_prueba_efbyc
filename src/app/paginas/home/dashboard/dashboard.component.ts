import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales, Cuentas } from 'src/app/globales/variables-generales';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { id } from '@swimlane/ngx-charts/release/utils';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UsuarioLogin, Cuenta } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  migajas = [
    { title: "Dashboard", class: "active", url: "" }
  ];

  nombre: UsuarioLogin = this.variablesApi.home.usuarioLogIn;
  empresas: Cuenta[] = this.variablesApi.home.cuentas;
  empresaActual: string;

  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private maquinaHome: MaquinaHome, private router: Router, private utils: UtilitariosService) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    this.setSelectedEmpresa();
    this.nombre = this.variablesApi.home.usuarioLogIn;
    this.empresas = this.variablesApi.home.cuentas;
    //this.cargarFacturas();
    this.cargarDatosTransacciones();
  }

  private setSelectedEmpresa() {
    if (this.empresas.length > 1) {
      // this.empresas.forEach(element => {
      //   if (element.cuentaDefault) {
      //     this.empresaActual = element.cuentaNombre;
      //   }
      // });
    } else if (this.empresas.length > 0) {
      this.empresaActual = this.empresas[0].cuentaNombre;
    } else {
      this.empresaActual = "Sin Cuenta";
    }
  }

  cargarDatosTransacciones() {
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        fechaConsulta: new Date()
      }
    }

    this.maquinaHome.homeUsuarioConsultarTransacciones(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  // cargarFacturas() {
  //   let actualUrl = this.router.url.substr(1);
  //   let action: RequestTipo = {
  //     accion: AccionTipo.ACCION_CLICK,
  //     datos: {
  //       cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
  //       fechaConsulta: new Date()
  //     }
  //   }

  //   this.maquinaHome.homeUsuarioConsultarFacturas(action).then(respuesta => {
  //     if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
  //       // if (actualUrl !== respuesta.paginaRespuesta) {
  //       //   this.router.navigate([respuesta.paginaRespuesta]);
  //       // } else {
  //       //   this.ngOnInit();
  //       // }
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
  //       this.utils.openModal(respuesta.modalSalida).then(response => {
  //         console.log(response);
  //       });
  //     } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
  //       this.utils.msgPlain(respuesta.mensajeSalida);

  //     }
  //   });
  // }

}
