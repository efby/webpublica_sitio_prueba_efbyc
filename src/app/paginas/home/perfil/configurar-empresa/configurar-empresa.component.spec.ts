import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurarEmpresaComponent } from './configurar-empresa.component';

describe('ConfigurarEmpresaComponent', () => {
  let component: ConfigurarEmpresaComponent;
  let fixture: ComponentFixture<ConfigurarEmpresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurarEmpresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurarEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
