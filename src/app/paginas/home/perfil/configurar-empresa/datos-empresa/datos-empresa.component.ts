import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-datos-empresa',
  templateUrl: './datos-empresa.component.html',
  styleUrls: ['./datos-empresa.component.css']
})
export class DatosEmpresaComponent implements OnInit {
  closeResult: string;
  _editable: boolean = false;

  superAdmins = [
    {
      idSuperAdmin: 0,
      nombre: "Maximiliano",
      apellido: "Perez",
      email: "mperez@correo.cl",
      telefono: "+56 9 12345678",
      editable: false
    }
  ];
  empresa = {
    rutEmpresa: "1111111 - 1",
    nombreFantasia: "Nombre Fantasía",
    razonSocial: "Razón Social",
    direccion: "Direccion 1",
    giroComercial: "Giro Comercial",
    actividadEconomica: "1"
  };

  constructor(private emiter: BreadCrumbsService, private utils: UtilitariosService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  nvoSuperAdmin() {
    this.utils.openModal(Paginas.ModalInvitarAdminComponent).then(data => {
    });
  }

  edit() {
    if (this._editable) {
      this._editable = false;
    } else {
      this._editable = true;
    }
  }

  editSA(id) {
    let idComponente = this.superAdmins.findIndex(r => r.idSuperAdmin === id);

    if (this.superAdmins[idComponente].editable) {
      this.superAdmins[idComponente].editable = false;
    } else {
      this.superAdmins[idComponente].editable = true;

    }
  }
}
