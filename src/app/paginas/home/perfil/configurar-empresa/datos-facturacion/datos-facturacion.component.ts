import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-datos-facturacion',
  templateUrl: './datos-facturacion.component.html',
  styleUrls: ['./datos-facturacion.component.css']
})
export class DatosFacturacionComponent implements OnInit {
  cuentaCorrientes = [
    {
      idCuenta: 0,
      nombreCuenta: "Cuenta corriente 1",
      nombreBanco: "Banco Santander",
      nombreTitular: "Maximiliano Urrutia",
      rutTitular: "16.XXX.XXX-X",
      nroCuenta: "283.XXX.XXX",
      correoAsociado: "correo@correo.cl",
      editable: false
    }
  ];
  correos = [
    {
      idCorreo: 0,
      txtCorreo: "correo@correo.cl"
    }
  ];
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  nvoCtaCte() {
    this.emiter.emitChange({
      tipo: 'CuentaCorriente',
      cuentaCorriente: true,
      edicion: false
    });
  }

  nvoCorreo() {
    let id = 0
    if (this.correos.length > 0) {
      id = this.correos[this.correos.length - 1].idCorreo + 1;
    }
    let txtCorreo = "";
    this.correos.push({ idCorreo: id, txtCorreo: txtCorreo });

  }

  deleteCorreo(idCorreo) {
    let idComponente = this.correos.findIndex(r => r.idCorreo === idCorreo);
    this.correos.splice(idComponente, 1);
  }

  edit(id) {
    let idComponente = this.cuentaCorrientes.findIndex(r => r.idCuenta === id);

    if (this.cuentaCorrientes[idComponente].editable) {
      this.cuentaCorrientes[idComponente].editable = false;
    } else {
      this.cuentaCorrientes[idComponente].editable = true;

    }
  }

}
