import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoDatosFacturacionComponent } from './nuevo-datos-facturacion.component';

describe('NuevoDatosFacturacionComponent', () => {
  let component: NuevoDatosFacturacionComponent;
  let fixture: ComponentFixture<NuevoDatosFacturacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoDatosFacturacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoDatosFacturacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
