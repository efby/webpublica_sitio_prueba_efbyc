import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { OpcionesModal, AccionTipo } from 'src/app/globales/enumeradores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-nuevo-datos-facturacion',
  templateUrl: './nuevo-datos-facturacion.component.html',
  styleUrls: ['./nuevo-datos-facturacion.component.css']
})
export class NuevoDatosFacturacionComponent implements OnInit {
  @Input() public nvaCuentaId;
  @Input() public actividadesEconomicas;
  @Input() public periodosFacturacion;
  @Output() passEntry: EventEmitter<OpcionesModal> = new EventEmitter();

  NombreUsuario: string;

  nvaDatosFacturacionRut: string = "";
  nvaDatosFacturacionEmail: string = "";
  nvaDatosFacturacionGiro: string = "";
  nvaDatosFacturacionDistrito: string = "";
  nvaDatosFacturacionDireccion: string = "";
  nvaDatosFacturacionRazonSocial: string = "";
  nvaDatosFacturacionCodigoPostal: string = "";
  nvaDatosFacturacionNombreFantasia: string = "";
  nvaDatosFacturacionActividadEconomica: string = "";
  nvaDatosFacturacionPeriodoFacturacion: string = "";


  constructor(private variablesApi: VariablesGenerales, private maquinaHome: MaquinaHome) { }

  ngOnInit() {
    this.NombreUsuario = this.variablesApi.home.usuarioLogIn.usuarioNombre + " " + this.variablesApi.home.usuarioLogIn.usuarioApellido;
  }

  validaMaxInputRut(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/g, '').replace(/(\..*)\./g, '$1');
    if (!this.validateRut(item.value)) {
      this.pintaValidacion(document.getElementById("nvaDatosFacturacionRut"), "");
    } else {
      this.pintaValidacion(document.getElementById("nvaDatosFacturacionRut"), this.nvaDatosFacturacionRut);
    }
  }

  validateRut(rutCliente: string) {
    rutCliente = rutCliente.toUpperCase();
    let rutSinDV = parseInt(rutCliente.slice(0, -1)), loop = 0, resto = 1;
    while (rutSinDV > 0) {
      resto = (resto + rutSinDV % 10 * (9 - loop++ % 6)) % 11;
      rutSinDV = Math.floor(rutSinDV / 10);
    }
    let digitoVerificador = (resto > 0) ? (resto - 1) + '' : 'K';
    return (digitoVerificador === rutCliente.slice(-1));
  }

  pintaValidacion(objeto: any, valor: string) {
    if (valor === "") {
      objeto.style.border = "1px solid #FF0000";
      return true;
    } else if (valor === "0") {
      objeto.style.border = "1px solid #FF0000";
      return true;
    } else {
      objeto.style.border = "1px solid #c5c9df";
      return false;
    }
  }

  response(respuesta: string) {
    switch (respuesta) {
      case "Reintentar":
        this.passEntry.emit(OpcionesModal.REINTENTAR);
        break;
      case "Salir":
        this.passEntry.emit(OpcionesModal.SALIR);
        break;
      case "Guardar":
        this.guardar();
        break;
      default:
        break;
    }
  }

  guardar() {
    this.nvaDatosFacturacionRut = this.nvaDatosFacturacionRut.substr(0, this.nvaDatosFacturacionRut.length - 1) + '-' + this.nvaDatosFacturacionRut.substr(this.nvaDatosFacturacionRut.length - 1)
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.nvaCuentaId,
        datosFacturacionRut: this.nvaDatosFacturacionRut,
        datosFacturacionEmail: this.nvaDatosFacturacionEmail,
        datosFacturacionGiro: this.nvaDatosFacturacionGiro,
        datosFacturacionDistrito: this.nvaDatosFacturacionDistrito,
        datosFacturacionDireccion: this.nvaDatosFacturacionDireccion,
        datosFacturacionRazonSocial: this.nvaDatosFacturacionRazonSocial,
        datosFacturacionCodigoPostal: this.nvaDatosFacturacionCodigoPostal,
        datosFacturacionNombreFantasia: this.nvaDatosFacturacionNombreFantasia,
        datosFacturacionActividadEconomica: this.nvaDatosFacturacionActividadEconomica,
        datosFacturacionPeriodoFacturacion: this.nvaDatosFacturacionPeriodoFacturacion
      }
    };

    alert('reimplement..');
    // this.maquinaHome.homeCrearDatosFacturacion(action).then(respuesta => {
    //   console.log(respuesta);
    //   this.limpiar();
    //   this.passEntry.emit(OpcionesModal.OK);
    // });

  }

  limpiar() {
    this.nvaCuentaId = 0;
    this.nvaDatosFacturacionRut = "";
    this.nvaDatosFacturacionEmail = "";
    this.nvaDatosFacturacionGiro = "";
    this.nvaDatosFacturacionDistrito = "";
    this.nvaDatosFacturacionDireccion = "";
    this.nvaDatosFacturacionRazonSocial = "";
    this.nvaDatosFacturacionCodigoPostal = "";
    this.nvaDatosFacturacionNombreFantasia = "";
    this.nvaDatosFacturacionActividadEconomica = "";
    this.nvaDatosFacturacionPeriodoFacturacion = "";
  }

}
