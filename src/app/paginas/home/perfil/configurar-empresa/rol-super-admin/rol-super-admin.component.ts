import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UserInvitaciones } from 'src/app/models/user-invitaciones';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Router } from '@angular/router';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { Invitacion } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-rol-super-admin',
  templateUrl: './rol-super-admin.component.html',
  styleUrls: ['./rol-super-admin.component.css']
})
export class RolSuperAdminComponent implements OnInit {

  superAdmins = [
    {
      idSuperAdmin: 0,
      nombre: "Maximiliano",
      apellido: "Perez",
      rut: "19",
      email: "mperez@correo.cl",
      telefono: "56912345678",
      detalles: "Habilitado como Conductor, rendidor, aprobador de gastos"
    }
  ];

  invitados: Invitacion[] = [];
  _muestraSA: boolean = false;
  nvoSuperAdmins: UserInvitaciones[];
  constructor(private variablesApi: VariablesGenerales, private emiter: BreadCrumbsService, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private router: Router) { }

  ngOnInit() {
    this.nvoSuperAdmins = [];
    this._muestraSA = false;
  }

  iniciaVariablesGlobales() {
    this.invitados = this.variablesApi.home.invitados.filter(r => r.invitacionRolId === 1 && r.invitacionEstadoInvitado !== "AUTORIZADA" && r.invitacionEstadoInvitado !== "NO_AUTORIZADA" && r.invitacionEstadoInvitado !== "INVALIDA");
  }

  nvoSuperAdmin() {
    let nuevo: UserInvitaciones = new UserInvitaciones();
    this.nvoSuperAdmins.push(nuevo);
    this._muestraSA = true;
  }

  removeSuperAdmin(telefonoInvitado) {
    let idComponente = this.nvoSuperAdmins.findIndex(r => r.telefonoInvitado === telefonoInvitado);
    this.nvoSuperAdmins.splice(idComponente, 1);
    if (this.nvoSuperAdmins.length == 0) {
      this._muestraSA = false;
    }
  }

  homeUsuarioInvitar() {
    let datos = [];

    this.nvoSuperAdmins.forEach(element => {
      if (element.telefonoInvitado !== "" && element.nombreInvitado !== "" && element.apellidosInvitado !== "") {
        let data = {
          cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
          rolId: 1,
          invitacionTelefonoId: element.telefonoInvitado,
          invitacionNombre: element.nombreInvitado,
          invitacionApellido: element.apellidosInvitado,
          invitacionSolicitaFoto: 1,
          invitacionSolicitaLicencia: 0
        }
        datos.push(data);
      }
    });

    if (datos.length > 0) {
      this.utils.showSpinner("Interno");
      let actualUrl = this.router.url.substr(1);
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_CLICK,
        datos: datos
      }

      this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          // if (actualUrl !== respuesta.paginaRespuesta) {
          //   this.router.navigate([respuesta.paginaRespuesta]);
          // } else {
          // this.cargaInvitaciones();
          // this.ngOnInit();
          // }
          this.router.navigate([Paginas.HomeConfigurarEmpresa]);
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            console.log(response);
          });
        }
      });
    }
  }

  cargaInvitaciones() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarUsuarios(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //this.ngOnInit();
        // }
        this.iniciaVariablesGlobales();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  muestraAprobacionConductor(id) {
    this.emiter.emitChange({
      tipo: 'Invitaciones',
      Invitaciones: true,
      idInvitacion: id
    });
  }

  fnConviertEstadoInvitacion(estado: string) {
    let retorno: string = "";
    switch (estado) {
      case "CREADA":
        retorno = "Enviada";
        break;
      case "RECHAZADA":
        retorno = "Rechazada por Conductor";
        break;
      case "ACEPTADA":
        retorno = "Aceptada por Conductor";
        break;
      default:
        retorno = estado;
        break;
    }
    return retorno;
  }


  autorizarInvitacion(invitacionId: number, autorizacion: number) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        invitacionId: invitacionId,
        invitacionAutorizada: autorizacion
      }
    }

    this.maquinaHome.homeUsuarioAutorizaInvitacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
        this.cargaInvitaciones();
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
          this.utils.hideSpinner("Interno");
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

  reenviarInvitacion(invitacionId: number) {
    let datos = [];
    this.utils.showSpinner("Interno");
    let idInvitacion = this.invitados.findIndex(r => r.invitacionId === invitacionId);
    let actualUrl = this.router.url.substr(1);

    let data = {
      cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
      rolId: 1,
      invitacionTelefonoId: this.invitados[idInvitacion].invitacionTelefonoInvitadoId,
      invitacionNombre: this.invitados[idInvitacion].invitacionValidaciones.invitacionNombre,
      invitacionApellido: this.invitados[idInvitacion].invitacionValidaciones.invitacionApellido,
      invitacionSolicitaFoto: this.invitados[idInvitacion].invitacionValidaciones.invitacionSolicitaFoto,
      invitacionSolicitaLicencia: this.invitados[idInvitacion].invitacionValidaciones.invitacionSolicitaLicencia
    }
    datos.push(data);


    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: datos
    }

    this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
        this.cargaInvitaciones();
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.utils.hideSpinner("Interno");
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }
}
