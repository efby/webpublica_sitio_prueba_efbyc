import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolSuperAdminComponent } from './rol-super-admin.component';

describe('RolSuperAdminComponent', () => {
  let component: RolSuperAdminComponent;
  let fixture: ComponentFixture<RolSuperAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolSuperAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolSuperAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
