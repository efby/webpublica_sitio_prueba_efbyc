import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolAnalistaComponent } from './rol-analista.component';

describe('RolAnalistaComponent', () => {
  let component: RolAnalistaComponent;
  let fixture: ComponentFixture<RolAnalistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolAnalistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolAnalistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
