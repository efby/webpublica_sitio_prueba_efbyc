import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-invitar-usuario',
  templateUrl: './invitar-usuario.component.html',
  styleUrls: ['./invitar-usuario.component.css']
})
export class InvitarUsuarioComponent implements OnInit {
  @Output() passEntry: EventEmitter<OpcionesModal> = new EventEmitter();
  _conductor: boolean = false;

  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  response(respuesta: string) {
    switch (respuesta) {
      case "Reintentar":
        this.passEntry.emit(OpcionesModal.REINTENTAR);
        break;
      case "Salir":
        this.passEntry.emit(OpcionesModal.SALIR);
        break;
      case "Enviar":
        this.enviar();
        break;
      default:
        break;
    }
  }

  enviar() {
    this.passEntry.emit(OpcionesModal.OK);
  }


}
