import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { OpcionesModal, AccionTipo } from 'src/app/globales/enumeradores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';

@Component({
  selector: 'app-nueva-empresa',
  templateUrl: './nueva-empresa.component.html',
  styleUrls: ['./nueva-empresa.component.css']
})
export class NuevaEmpresaComponent implements OnInit {
  @Output() passEntry: EventEmitter<OpcionesModal> = new EventEmitter();

  nvaEmpresaCuenta: string = "";
  nvaEmpresaRut: string = "";
  nvaEmpresaNombreFantasia: string = "";
  nvaEmpresaGiroComercial: string = "";
  nvaEmpresaRazonSocial: string = "";
  nvaEmpresaDireccion: string = "";
  nvaEmpresaComuna: string = "";


  constructor(private variablesApi: VariablesGenerales, private maquinaHome: MaquinaHome) { }

  ngOnInit() {
  }

  response(respuesta: string) {
    switch (respuesta) {
      case "Reintentar":
        this.passEntry.emit(OpcionesModal.REINTENTAR);
        break;
      case "Salir":
        this.passEntry.emit(OpcionesModal.SALIR);
        break;
      case "Guardar":
        this.guardar();
        break;
      default:
        break;
    }
  }

  guardar() {
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaNombre: this.nvaEmpresaCuenta,
        rut: this.nvaEmpresaRut,
        razonSocial: this.nvaEmpresaRazonSocial,
        nombreFantasia: this.nvaEmpresaNombreFantasia,
        giroComercial: this.nvaEmpresaGiroComercial,
        direccion: this.nvaEmpresaDireccion,
        comuna: this.nvaEmpresaComuna,
        grupoId: 1,
      }
    };

    this.maquinaHome.homeCrearCuentaEmpresa(action).then(respuesta => {
      console.log(respuesta);
      this.limpiar();
      this.passEntry.emit(OpcionesModal.OK);
    });

  }

  limpiar() {
    this.nvaEmpresaCuenta = "";
    this.nvaEmpresaRut = "";
    this.nvaEmpresaNombreFantasia = "";
    this.nvaEmpresaGiroComercial = "";
    this.nvaEmpresaRazonSocial = "";
    this.nvaEmpresaDireccion = "";
    this.nvaEmpresaComuna = "";
  }

}
