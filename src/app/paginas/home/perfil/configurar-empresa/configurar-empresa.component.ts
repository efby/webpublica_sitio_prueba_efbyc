import { Component, OnInit, ViewChild } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { RolSuperAdminComponent } from './rol-super-admin/rol-super-admin.component';
import { RolAdminComponent } from './rol-admin/rol-admin.component';
import { RolAnalistaComponent } from './rol-analista/rol-analista.component';

@Component({
  selector: 'app-configurar-empresa',
  templateUrl: './configurar-empresa.component.html',
  styleUrls: ['./configurar-empresa.component.css']
})
export class ConfigurarEmpresaComponent implements OnInit {
  @ViewChild(RolSuperAdminComponent) private superAdmin: RolSuperAdminComponent;
  @ViewChild(RolAdminComponent) private admin: RolAdminComponent;
  @ViewChild(RolAnalistaComponent) private analista: RolAnalistaComponent;
  nvaCuentaId: number = 0;
  closeResult: string;
  actividadesEconomicas: any[] = [];
  periodosFacturacion: any[] = [];

  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Configurar Perfil", class: "", url: "/" + Paginas.HomeConfigurarPerfil },
    { title: "Configuracion de Empresa", class: "active", url: "" }
  ];

  constructor(private emiter: BreadCrumbsService, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private maquinaHome: MaquinaHome, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
      this.emiter.emitChange({ tipo: "Configuracion", _isConfiguracion: true });
    });
    this.nvaCuentaId = parseInt(this.route.snapshot.paramMap.get('idEmpresa'));
    this.cargaInvitaciones();
    this.cargaDatosFacturacion();
  }

  nvaEmpresa() {
    this.utils.openModal(Paginas.ModalAgregarEmpresaComponent).then(response => {
    });
  }

  nvaDatosFacturacion() {
    let data = { nvaCuentaId: this.nvaCuentaId, actividadesEconomicas: this.actividadesEconomicas, periodosFacturacion: this.periodosFacturacion };
    this.utils.openModal(Paginas.ModalAgregarDatosFacturacionComponent, data).then(response => {
    });
  }

  cargaInvitacionesHijos() {
    this.superAdmin.iniciaVariablesGlobales();
    this.admin.iniciaVariablesGlobales();
    this.analista.iniciaVariablesGlobales();
  }

  cargaDatosFacturacion() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId
      }
    }

    this.maquinaHome.homeConsultarDatosFacturacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //this.ngOnInit();
        // }
        this.actividadesEconomicas = respuesta.data.actividadesEconomicas;
        this.periodosFacturacion = respuesta.data.periodosFacturacion;

      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  cargaInvitaciones() {
    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarUsuarios(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //this.ngOnInit();
        // }
        this.cargaInvitacionesHijos();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }
}
