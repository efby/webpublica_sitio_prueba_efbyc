import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActividadUsuarioComponent } from './actividad-usuario.component';

describe('ActividadUsuarioComponent', () => {
  let component: ActividadUsuarioComponent;
  let fixture: ComponentFixture<ActividadUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActividadUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
