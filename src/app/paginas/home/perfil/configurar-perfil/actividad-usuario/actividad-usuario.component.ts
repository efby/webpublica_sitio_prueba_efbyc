import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-actividad-usuario',
  templateUrl: './actividad-usuario.component.html',
  styleUrls: ['./actividad-usuario.component.css']
})
export class ActividadUsuarioComponent implements OnInit {
  actividades = [
    {
      idTipoActividad: 0,
      nombreActividad: "Rendicion",
      tituloActividad: "Rendiciones",
      descripcionActividad: "Te informaremos cuando un conductor te envíe una rendicion para tu aprobacion o rechazo",
      estadoActividad: true,
    },
    {
      idTipoActividad: 1,
      nombreActividad: "Consumos",
      tituloActividad: "Consumos",
      descripcionActividad: "Te informaremos cada vez que uno de tus coneuctores realice un consumo",
      estadoActividad: true,
    }
  ];
  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

}
