import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-alertas-usuario',
  templateUrl: './alertas-usuario.component.html',
  styleUrls: ['./alertas-usuario.component.css']
})
export class AlertasUsuarioComponent implements OnInit {
  alertas = [
    {
      idTipoAlerta: 0,
      nombreAlerta: "PermisoEspecial",
      tituloAlerta: "Uso de Permiso especial",
      descripcionAlerta: "Te informaremos cuando un conductor utilice un permiso especial para cargar fuera de día u horario, o para exceder los límites de gasto diario o mensual",
      estadoAlerta: true,
    },
    {
      idTipoAlerta: 1,
      nombreAlerta: "EmpresaSinSaldo",
      tituloAlerta: "Empresa sin Saldo",
      descripcionAlerta: "Te informaremos cuando tu empresa se quede sin saldo",
      estadoAlerta: true,
    },
    {
      idTipoAlerta: 2,
      nombreAlerta: "CuentaBloquead",
      tituloAlerta: "Cuenta Bloqueada",
      descripcionAlerta: "Te informaremos cuando nos veamos forzados a bloquear tu línea de crédito por no pago. Saber mas sobre bloqueo de cuentas",
      estadoAlerta: true,
    }
  ];
  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

}
