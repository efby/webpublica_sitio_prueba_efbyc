import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Paginas } from 'src/app/globales/paginas';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { Router } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Invitacion } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-invitaciones-usuario',
  templateUrl: './invitaciones-usuario.component.html',
  styleUrls: ['./invitaciones-usuario.component.css']
})
export class InvitacionesUsuarioComponent implements OnInit {

  invitaciones?: Invitacion[] = [];;

  constructor(private variablesApi: VariablesGenerales, private utils: UtilitariosService, private maquinaHome: MaquinaHome, private router: Router) {
  }

  ngOnInit() {
    this.invitaciones = this.variablesApi.home.invitaciones;
  }

  fnInvitaciones(id) {
    let idComponente = this.variablesApi.home.invitaciones.findIndex(r => r.invitacionId = id);
    let data = {
      idInvitacion: id,
      nombreInvitado: (this.variablesApi.home.usuarioLogIn.usuarioNombre + " " + this.variablesApi.home.usuarioLogIn.usuarioApellido),
      rolInvitado: "Conductor",
      vigenciaInvitacion: "vigencia",
      nombreEmpresa: this.variablesApi.home.invitaciones[idComponente].invitacionNombreCuenta,
    };

    this.utils.openModal(Paginas.ModalAceptarInvitacionComponent, data).then(response => {
      if (response === OpcionesModal.RECHAZAR) {
        this.autorizarInvitacion(id, 0);
      } else if (response === OpcionesModal.OK) {
        this.autorizarInvitacion(id, 1);
      }
    });
  }

  autorizarInvitacion(invitacionId: number, invitacionAceptada: number) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        invitacionId: invitacionId,
        invitacionAceptada: invitacionAceptada
      }
    }

    this.maquinaHome.homeUsuarioAceptaInvitacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        this.ngOnInit();
        // }
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
          this.utils.hideSpinner("Interno");
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);
      }
    });
  }

}
