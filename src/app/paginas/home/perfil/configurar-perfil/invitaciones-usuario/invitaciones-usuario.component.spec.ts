import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitacionesUsuarioComponent } from './invitaciones-usuario.component';

describe('InvitacionesUsuarioComponent', () => {
  let component: InvitacionesUsuarioComponent;
  let fixture: ComponentFixture<InvitacionesUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitacionesUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitacionesUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
