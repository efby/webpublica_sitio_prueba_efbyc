import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-notificaciones-usuario',
  templateUrl: './notificaciones-usuario.component.html',
  styleUrls: ['./notificaciones-usuario.component.css']
})
export class NotificacionesUsuarioComponent implements OnInit {
  notificaciones = [
    {
      idTipoNotificacion: 0,
      nombreNotificacion: "IncorporacionUsuario",
      tituloNotificacion: "Incorporacion de Usuario a Empresa",
      descripcionNotificacion: "Te informaremos cuando un conductor invitado cargue sus documentos, para que apruebes o rechaces su incorporacion a tu empresa.",
      estadoNotificacion: true,
    },
    {
      idTipoNotificacion: 1,
      nombreNotificacion: "IncorporacionVehiculo",
      tituloNotificacion: "Incorporacion de vehiculo a Empresa",
      descripcionNotificacion: "Te informaremos cuando se cree un nuevo vehiculo en tu empresa.",
      estadoNotificacion: true,
    },
    {
      idTipoNotificacion: 2,
      nombreNotificacion: "AbonosRetiros",
      tituloNotificacion: "Abonos y Retiros",
      descripcionNotificacion: "Te informaremos sobre cualquier abono y retiro realizado",
      estadoNotificacion: true,
    }
  ];
  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

}
