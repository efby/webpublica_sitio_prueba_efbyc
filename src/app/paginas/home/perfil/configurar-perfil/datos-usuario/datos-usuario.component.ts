import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UsuarioLogin } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-datos-usuario',
  templateUrl: './datos-usuario.component.html',
  styleUrls: ['./datos-usuario.component.css']
})
export class DatosUsuarioComponent implements OnInit {

  _editable: boolean = false;
  usuarioLogin: UsuarioLogin = new UsuarioLogin();
  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
    this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
  }

  edit() {
    if (this._editable) {
      // debe guardar la informacion modificada
    }
    this._editable = !this._editable;
  }
}
