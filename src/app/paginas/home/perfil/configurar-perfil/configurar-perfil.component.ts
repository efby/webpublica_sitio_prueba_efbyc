import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UsuarioLogin } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-configurar-perfil',
  templateUrl: './configurar-perfil.component.html',
  styleUrls: ['./configurar-perfil.component.css']
})
export class ConfigurarPerfilComponent implements OnInit {

  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Configuracion de Perfil", class: "active", url: "" }
  ];
  usuarioLogin: UsuarioLogin = new UsuarioLogin();
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
      this.emiter.emitChange({ tipo: "Configuracion", _isConfiguracion: true });
    });
    this.usuarioLogin = this.variablesApi.home.usuarioLogIn;
  }

}
