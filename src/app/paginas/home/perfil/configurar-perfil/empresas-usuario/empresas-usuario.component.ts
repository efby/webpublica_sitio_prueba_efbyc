import { Component, OnInit } from '@angular/core';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales, Cuentas } from 'src/app/globales/variables-generales';
import { Cuenta } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-empresas-usuario',
  templateUrl: './empresas-usuario.component.html',
  styleUrls: ['./empresas-usuario.component.css']
})
export class EmpresasUsuarioComponent implements OnInit {
  closeResult: string;
  cuentas: Cuenta[] = [];

  constructor(private utils: UtilitariosService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
    this.cuentas = this.variablesApi.home.cuentas;
  }

  nvaEmpresa() {
    this.utils.openModal(Paginas.ModalAgregarEmpresaComponent).then(response => {
    });
  }
}
