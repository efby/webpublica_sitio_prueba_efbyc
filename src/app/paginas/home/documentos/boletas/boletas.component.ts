import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Boletas } from 'src/app/models/boletas';
import { Router } from '@angular/router';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-boletas',
  templateUrl: './boletas.component.html',
  styleUrls: ['./boletas.component.scss']
})
export class BoletasComponent implements OnInit {

  boletas: Boletas[];
  boletaDetalle: Boletas;

  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Boletas", class: "active", url: "" }
  ];

  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private router: Router, private maquinaHome: MaquinaHome, private utils: UtilitariosService) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    this.boletas = this.variablesApi.home.boletas;
    this.resetCollapsed();
  }

  consultarDetalleBoleta(ventaId) {
    this.utils.showSpinner("Interno");
    this.resetCollapsed();
    let idComFactura = this.boletas.findIndex(r => r.ventaId === ventaId);
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        ventaId: ventaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarDetalleTransacciones(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
        this.boletaDetalle = respuesta.data.boletas;
        this.boletas[idComFactura].collapsed = false;
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  resetCollapsed() {
    for (let index = 0; index < this.boletas.length; index++) {
      this.boletas[index].collapsed = true;

    }
  }

}
