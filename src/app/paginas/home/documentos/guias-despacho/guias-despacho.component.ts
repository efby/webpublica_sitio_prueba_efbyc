import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Guias } from 'src/app/models/guias';
import { Router } from '@angular/router';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-guias-despacho',
  templateUrl: './guias-despacho.component.html',
  styleUrls: ['./guias-despacho.component.css']
})
export class GuiasDespachoComponent implements OnInit {

  guias: Guias[];
  guiaDetalle: Guias;
  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Guias de Despacho", class: "active", url: "" }
  ];
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private router: Router, private maquinaHome: MaquinaHome, private utils: UtilitariosService) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    this.guias = this.variablesApi.home.guias;
    this.resetCollapsed();
  }


  consultarDetalleGuia(ventaId) {
    this.utils.showSpinner("Interno");
    this.resetCollapsed()
    let idComFactura = this.guias.findIndex(r => r.ventaId === ventaId);
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        ventaId: ventaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarDetalleTransacciones(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
        this.guiaDetalle = respuesta.data.guias;
        this.guias[idComFactura].collapsed = false;
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  resetCollapsed() {
    for (let index = 0; index < this.guias.length; index++) {
      this.guias[index].collapsed = true;

    }
  }

}
