import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-abono-resultado',
  templateUrl: './abono-resultado.component.html',
  styleUrls: ['./abono-resultado.component.css']
})
export class AbonoResultadoComponent implements OnInit {
  procesado: boolean = true;
  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Resultado Abono", class: "active", url: "" }
  ];
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
  }

}
