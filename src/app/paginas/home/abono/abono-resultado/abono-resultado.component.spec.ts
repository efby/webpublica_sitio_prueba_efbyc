import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbonoResultadoComponent } from './abono-resultado.component';

describe('AbonoResultadoComponent', () => {
  let component: AbonoResultadoComponent;
  let fixture: ComponentFixture<AbonoResultadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbonoResultadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbonoResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
