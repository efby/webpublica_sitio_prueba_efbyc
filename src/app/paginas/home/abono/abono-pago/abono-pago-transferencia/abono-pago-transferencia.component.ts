import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-abono-pago-transferencia',
  templateUrl: './abono-pago-transferencia.component.html',
  styleUrls: ['./abono-pago-transferencia.component.css']
})
export class AbonoPagoTransferenciaComponent implements OnInit {
  _step2: boolean = false;
  _step3: boolean = false;
  nombreCuenta: string = "";
  nombreBanco: string = "";
  nombreTitular: string = "";
  rutTitular: string = "";
  nroCuenta: string = "";
  correoAsociado: string = "";

  empresas = [
    {
      idEmpresa: 0,
      nombreEmpresa: "Empresa 1",
      rutEmpresa: "1-9",
      montoAbono: 0,
      activo: true
    },
    {
      idEmpresa: 1,
      nombreEmpresa: "Empresa 2",
      rutEmpresa: "2-7",
      montoAbono: 0,
      activo: true
    },
    {
      idEmpresa: 2,
      nombreEmpresa: "Empresa 3",
      rutEmpresa: "3-5",
      montoAbono: 0,
      activo: true
    }
  ];
  cuentaCorrientes = [
    {
      idCuenta: 0,
      nombreCuenta: "Cuenta corriente 1",
      nombreBanco: "Banco Santander",
      nombreTitular: "Maximiliano Urrutia",
      rutTitular: "16.XXX.XXX-X",
      nroCuenta: "283.XXX.XXX",
      correoAsociado: "correo@correo.cl"
    }
  ];
  constructor(private emiter: BreadCrumbsService, private utils: UtilitariosService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  habilita(id) {
    this.empresas.forEach(element => {
      element.activo = true;
      element.montoAbono = 0;
    });

    let idComponente = this.empresas.findIndex(r => r.idEmpresa === id);
    this.empresas[idComponente].activo = false;
    this.empresas[idComponente].montoAbono = 0;
    this._step2 = true;
  }

  transformAmount(element: any) {
    let item = element.srcElement;
    item.value = this.utils.formatCurrency(item.value);
  }

  nextStep(id) {
    let idComponente = this.cuentaCorrientes.findIndex(r => r.idCuenta === id);
    this.nombreCuenta = this.cuentaCorrientes[idComponente].nombreCuenta;
    this.nombreBanco = this.cuentaCorrientes[idComponente].nombreBanco;
    this.nombreTitular = this.cuentaCorrientes[idComponente].nombreTitular;
    this.rutTitular = this.cuentaCorrientes[idComponente].rutTitular;
    this.nroCuenta = this.cuentaCorrientes[idComponente].nroCuenta;
    this.correoAsociado = this.cuentaCorrientes[idComponente].correoAsociado;
    this._step3 = true;
  }

  nuevaCuenta(edicion: boolean) {
    this.emiter.emitChange({
      tipo: 'CuentaCorriente',
      cuentaCorriente: true,
      edicion: edicion
    });
  }


}
