import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbonoPagoTransferenciaComponent } from './abono-pago-transferencia.component';

describe('AbonoPagoTransferenciaComponent', () => {
  let component: AbonoPagoTransferenciaComponent;
  let fixture: ComponentFixture<AbonoPagoTransferenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbonoPagoTransferenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbonoPagoTransferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
