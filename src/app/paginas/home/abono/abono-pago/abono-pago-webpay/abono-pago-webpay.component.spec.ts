import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbonoPagoWebpayComponent } from './abono-pago-webpay.component';

describe('AbonoPagoWebpayComponent', () => {
  let component: AbonoPagoWebpayComponent;
  let fixture: ComponentFixture<AbonoPagoWebpayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbonoPagoWebpayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbonoPagoWebpayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
