import { Component, OnInit } from '@angular/core';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-abono-pago-webpay',
  templateUrl: './abono-pago-webpay.component.html',
  styleUrls: ['./abono-pago-webpay.component.css']
})
export class AbonoPagoWebpayComponent implements OnInit {
  amount: number = 0.0;
  formattedAmount: string = '';
  _activo: boolean = false;
  empresas = [
    {
      idEmpresa: 0,
      nombreEmpresa: "Empresa 1",
      rutEmpresa: "1-9",
      montoAbono: 0,
      activo: true
    },
    {
      idEmpresa: 1,
      nombreEmpresa: "Empresa 2",
      rutEmpresa: "2-7",
      montoAbono: 0,
      activo: true
    },
    {
      idEmpresa: 2,
      nombreEmpresa: "Empresa 3",
      rutEmpresa: "3-5",
      montoAbono: 0,
      activo: true
    }
  ];
  constructor(private utils: UtilitariosService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  habilita(id) {
    this.empresas.forEach(element => {
      element.activo = true;
      element.montoAbono = 0;
    });

    let idComponente = this.empresas.findIndex(r => r.idEmpresa === id);
    this.empresas[idComponente].activo = false;
    this.empresas[idComponente].montoAbono = 0;
    this._activo = true;
  }

  transformAmount(element: any) {
    let item = element.srcElement;
    item.value = this.utils.formatCurrency(item.value);
  }

}
