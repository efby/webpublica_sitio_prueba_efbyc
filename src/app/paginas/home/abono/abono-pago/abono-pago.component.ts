import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-abono-pago',
  templateUrl: './abono-pago.component.html',
  styleUrls: ['./abono-pago.component.css']
})
export class AbonoPagoComponent implements OnInit {

  _webpay: boolean = false;
  _transferencias: boolean = false;

  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Abono de Presupuesto", class: "active", url: "" }
  ];
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
  }

  beforeChange($event: NgbPanelChangeEvent) {
    switch ($event.panelId) {
      case "webpay":
        if (this._webpay) {
          this._webpay = false;
        } else {
          this._webpay = true;
        }
        this._transferencias = false;
        break;
      case "transferencia":
        if (this._transferencias) {
          this._transferencias = false;
        } else {
          this._transferencias = true;
        }
        this._webpay = false;
        break;
      default:
        break;
    }
  }

}
