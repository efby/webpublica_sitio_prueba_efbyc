import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbonoPagoComponent } from './abono-pago.component';

describe('AbonoPagoComponent', () => {
  let component: AbonoPagoComponent;
  let fixture: ComponentFixture<AbonoPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbonoPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbonoPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
