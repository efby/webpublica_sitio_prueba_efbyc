import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiculoPermisosComponent } from './vehiculo-permisos.component';

describe('VehiculoPermisosComponent', () => {
  let component: VehiculoPermisosComponent;
  let fixture: ComponentFixture<VehiculoPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VehiculoPermisosComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiculoPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
