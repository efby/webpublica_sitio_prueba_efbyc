import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { Conductor } from 'src/app/models/conductores';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import * as XLSX from 'xlsx';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';


@Component({
  selector: 'app-conductor-crear',
  templateUrl: './conductor-crear.component.html',
  styleUrls: ['./conductor-crear.component.css']
})
export class ConductorCrearComponent implements OnInit {
  // idGrupoConductor: number = 0;
  conductores: Conductor[] = [];
  valInvitacionLicencia: number = 0;
  valInvitacionSelfie: number = 0;

  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Invitar Conductor", class: "active", url: "" }
  ];
  constructor(private emiter: BreadCrumbsService, private maquinaHome: MaquinaHome, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    // this.idGrupoConductor = parseInt(this.route.snapshot.paramMap.get('id'));
    //this.conductores = this.variablesApi.home.conductores;
  }

  addMeFirst() {
    this.conductores[0].nombre = this.variablesApi.home.usuarioLogIn.usuarioNombre;
    this.conductores[0].apellido = this.variablesApi.home.usuarioLogIn.usuarioApellido;
    this.conductores[0].telefonoId = this.variablesApi.ingreso.usuario.telefonoId;
  }

  addConductor() {
    this.conductores.push(new Conductor());
  }

  removeConductor(id) {
    let idComponente = this.conductores.findIndex(r => r.usuarioId === id);
    this.conductores.splice(idComponente, 1);
  }

  uploadExcel() {
    this.utils.openModal(Paginas.ModalSubirArchivoComponent).then(response => {
      let lastIndex = this.conductores.length - 1;
      if (this.conductores[lastIndex].telefonoId === "") {
        this.removeConductor("");
      }
      var range = XLSX.utils.decode_range(response['!ref']);
      for (let rowNum = 6; rowNum <= range.e.r; rowNum++) {
        let conductor = new Conductor();
        // Example: Get second cell in each row, i.e. Column "B"
        conductor.nombre = response[XLSX.utils.encode_cell({ r: rowNum, c: 0 })].v;
        conductor.apellido = response[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v;
        conductor.telefonoId = response[XLSX.utils.encode_cell({ r: rowNum, c: 2 })].v;
        // conductor.grupoId = this.idGrupoConductor;
        this.conductores.push(conductor);
      }
    });
  }

  homeUsuarioInvitar() {
    let datos = [];


    this.conductores.forEach(element => {
      let data = {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        rolId: 4,
        invitacionTelefonoId: element.telefonoId,
        invitacionNombre: element.nombre,
        invitacionApellido: element.apellido,
        invitacionSolicitaFoto: this.valInvitacionSelfie ? 1 : 0,
        invitacionSolicitaLicencia: this.valInvitacionLicencia ? 1 : 0
      }
      datos.push(data);
    });

    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: datos
    }

    this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        } else {
          this.ngOnInit();
        }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      }
    });
  }
}
