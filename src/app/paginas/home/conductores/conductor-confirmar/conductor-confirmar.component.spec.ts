import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConductorConfirmarComponent } from './conductor-confirmar.component';

describe('ConfirmarConductorComponent', () => {
  let component: ConductorConfirmarComponent;
  let fixture: ComponentFixture<ConductorConfirmarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConductorConfirmarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConductorConfirmarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
