import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaHome } from 'src/app/maquinas/maquina-home';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo } from 'src/app/globales/enumeradores';
import { Invitacion } from 'src/app/globales/business-objects';

@Component({
  selector: 'app-conductor-confirmar',
  templateUrl: './conductor-confirmar.component.html',
  styleUrls: ['./conductor-confirmar.component.css']
})
export class ConductorConfirmarComponent implements OnInit {

  invitados?: Invitacion[];

  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Confirmar Conductor", class: "active", url: "" }
  ];

  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales, private router: Router, private utils: UtilitariosService, private maquinaHome: MaquinaHome) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
    this.cargaInvitaciones();
  }

  cargaFecha() {
    this.variablesApi.home.invitados.forEach(element => {
      element.invitacionFechaCaducidad = this.date2Human(element.invitacionFechaCaducidad);
    });
  }

  iniciaVariablesGlobales() {
    this.cargaFecha();
    this.invitados = this.variablesApi.home.invitados.filter(r => r.invitacionRolId === 4 && r.invitacionEstadoInvitado === "CREADA");
  }

  cargaInvitaciones() {

    this.utils.showSpinner("Interno");
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId
      }
    }

    this.maquinaHome.homeUsuarioConsultarUsuarios(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //this.ngOnInit();
        // }
        this.iniciaVariablesGlobales();
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  date2Human(date) {
    let format = 'dd/MM/yyyy';
    return this.utils.date2Human(date, format, this.router.url);
  }

  autorizarInvitacion(invitacionId: number, autorizacion: number) {
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        invitacionId: invitacionId,
        invitacionAutorizada: autorizacion
      }
    }

    this.maquinaHome.homeUsuarioAutorizaInvitacion(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  reenviarInvitacion(invitacionId: number, autorizacion: number) {
    let datos = [];

    let actualUrl = this.router.url.substr(1);

    this.invitados.forEach(element => {
      let data = {
        cuentaId: this.variablesApi.home.cuentaSeleccionada.cuentaId,
        rolId: 4,
        invitacionTelefonoId: element.invitacionTelefonoInvitadoId,
        invitacionNombre: element.invitacionValidaciones.invitacionNombre,
        invitacionApellido: element.invitacionValidaciones.invitacionApellido,
        invitacionSolicitaFoto: element.invitacionValidaciones.invitacionSolicitaFoto,
        invitacionSolicitaLicencia: element.invitacionValidaciones.invitacionSolicitaLicencia
      }
      datos.push(data);
    });

    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: datos
    }

    this.maquinaHome.homeUsuarioInvitar(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        // if (actualUrl !== respuesta.paginaRespuesta) {
        //   this.router.navigate([respuesta.paginaRespuesta]);
        // } else {
        //   this.ngOnInit();
        // }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.openModal(respuesta.modalSalida).then(response => {
          console.log(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

}
