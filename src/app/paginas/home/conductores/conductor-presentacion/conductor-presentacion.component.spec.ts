import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConductorPresentacionComponent } from './conductor-presentacion.component';

describe('ConductorPresentacionComponent', () => {
  let component: ConductorPresentacionComponent;
  let fixture: ComponentFixture<ConductorPresentacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConductorPresentacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConductorPresentacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
