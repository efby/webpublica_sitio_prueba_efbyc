import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-conductor-presentacion',
  templateUrl: './conductor-presentacion.component.html',
  styleUrls: ['./conductor-presentacion.component.css']
})
export class ConductorPresentacionComponent implements OnInit {

  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

}
