import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditoResultadoComponent } from './credito-resultado.component';

describe('CreditoResultadoComponent', () => {
  let component: CreditoResultadoComponent;
  let fixture: ComponentFixture<CreditoResultadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditoResultadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditoResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
