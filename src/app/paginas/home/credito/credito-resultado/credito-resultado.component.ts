import { Component, OnInit } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-credito-resultado',
  templateUrl: './credito-resultado.component.html',
  styleUrls: ['./credito-resultado.component.css']
})
export class CreditoResultadoComponent implements OnInit {

  procesado: boolean = false;
  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Resultado Pago de Credito", class: "active", url: "" }
  ];
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
  }

}
