import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditoPagoComponent } from './credito-pago.component';

describe('CreditoPagoComponent', () => {
  let component: CreditoPagoComponent;
  let fixture: ComponentFixture<CreditoPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditoPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditoPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
