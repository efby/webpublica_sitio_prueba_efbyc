import { Component, OnInit } from '@angular/core';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-credito-pago-transferencia',
  templateUrl: './credito-pago-transferencia.component.html',
  styleUrls: ['./credito-pago-transferencia.component.css']
})
export class CreditoPagoTransferenciaComponent implements OnInit {
  _empresaSelect: boolean = false;
  _step2: boolean = false;
  _step3: boolean = false;
  nombreCuenta: string = "";
  nombreBanco: string = "";
  nombreTitular: string = "";
  rutTitular: string = "";
  nroCuenta: string = "";
  correoAsociado: string = "";
  Empresas = [
    {
      idEmpresa: 0,
      nombreEmpresa: "empresa 1",
      rutEmpresa: "1-9",
      montoAbono: 0,
      activo: true
    },
    {
      idEmpresa: 1,
      nombreEmpresa: "empresa 2",
      rutEmpresa: "2-7",
      montoAbono: 0,
      activo: true
    },
    {
      idEmpresa: 2,
      nombreEmpresa: "empresa 3",
      rutEmpresa: "3-5",
      montoAbono: 0,
      activo: true
    }
  ];

  facturas = [
    {
      idFactura: 0,
      fechaFactura: "01/07/2019 08:00",
      nroFactura: "123",
      detalleFactura: "15 Guías",
      montoFactura: "100.000"
    },
    {
      idFactura: 1,
      fechaFactura: "01/07/2019 08:00",
      nroFactura: "124",
      detalleFactura: "15 Guías",
      montoFactura: "100.000"
    },
    {
      idFactura: 2,
      fechaFactura: "01/07/2019 08:00",
      nroFactura: "125",
      detalleFactura: "15 Guías",
      montoFactura: "100.000"
    }
  ];
  cuentas = [
    {
      idCuenta: 0,
      nombreCuenta: "Cuenta corriente 1",
      nombreBanco: "Banco Santander",
      nombreTitular: "Maximiliano Urrutia",
      rutTitular: "16.XXX.XXX-X",
      nroCuenta: "283.XXX.XXX",
      correoAsociado: "correo@correo.cl"
    }
  ];
  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  habilita(id) {
    this.Empresas.forEach(element => {
      element.activo = true;
    });

    let idComponente = this.Empresas.findIndex(r => r.idEmpresa === id);
    this.Empresas[idComponente].activo = false;
    this._empresaSelect = true;
    this._step2 = true;

  }

  nextStep(id) {
    let idComponente = this.cuentas.findIndex(r => r.idCuenta === id);
    this.nombreCuenta = this.cuentas[idComponente].nombreCuenta;
    this.nombreBanco = this.cuentas[idComponente].nombreBanco;
    this.nombreTitular = this.cuentas[idComponente].nombreTitular;
    this.rutTitular = this.cuentas[idComponente].rutTitular;
    this.nroCuenta = this.cuentas[idComponente].nroCuenta;
    this.correoAsociado = this.cuentas[idComponente].correoAsociado;
    this._step3 = true;
  }

  nuevaCuenta(edicion: boolean) {
    this.emiter.emitChange({
      tipo: 'CuentaCorriente',
      cuentaCorriente: true,
      edicion: edicion
    });
  }

}
