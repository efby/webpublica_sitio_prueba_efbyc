import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditoPagoTransferenciaComponent } from './credito-pago-transferencia.component';

describe('CreditoPagoTransferenciaComponent', () => {
  let component: CreditoPagoTransferenciaComponent;
  let fixture: ComponentFixture<CreditoPagoTransferenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditoPagoTransferenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditoPagoTransferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
