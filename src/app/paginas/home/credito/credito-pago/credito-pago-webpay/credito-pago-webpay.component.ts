import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-credito-pago-webpay',
  templateUrl: './credito-pago-webpay.component.html',
  styleUrls: ['./credito-pago-webpay.component.css']
})
export class CreditoPagoWebpayComponent implements OnInit {
  _empresaSelect: boolean = false;
  Empresas = [
    {
      idEmpresa: 0,
      nombreEmpresa: "empresa 1",
      rutEmpresa: "1-9",
      montoAbono: 0,
      activo: true
    },
    {
      idEmpresa: 1,
      nombreEmpresa: "empresa 2",
      rutEmpresa: "2-7",
      montoAbono: 0,
      activo: true
    },
    {
      idEmpresa: 2,
      nombreEmpresa: "empresa 3",
      rutEmpresa: "3-5",
      montoAbono: 0,
      activo: true
    }
  ];

  facturas = [
    {
      idFactura: 0,
      fechaFactura: "01/07/2019 08:00",
      nroFactura: "123",
      detalleFactura: "15 Guías",
      montoFactura: "100.000"
    },
    {
      idFactura: 1,
      fechaFactura: "01/07/2019 08:00",
      nroFactura: "124",
      detalleFactura: "15 Guías",
      montoFactura: "100.000"
    },
    {
      idFactura: 2,
      fechaFactura: "01/07/2019 08:00",
      nroFactura: "125",
      detalleFactura: "15 Guías",
      montoFactura: "100.000"
    }
  ];
  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  habilita(id) {
    this.Empresas.forEach(element => {
      element.activo = true;
    });

    let idComponente = this.Empresas.findIndex(r => r.idEmpresa === id);
    this.Empresas[idComponente].activo = false;
    this._empresaSelect = true;
  }

}
