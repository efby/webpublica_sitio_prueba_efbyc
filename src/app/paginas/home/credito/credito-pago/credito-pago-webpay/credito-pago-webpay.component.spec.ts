import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditoPagoWebpayComponent } from './credito-pago-webpay.component';

describe('CreditoPagoWebpayComponent', () => {
  let component: CreditoPagoWebpayComponent;
  let fixture: ComponentFixture<CreditoPagoWebpayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditoPagoWebpayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditoPagoWebpayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
