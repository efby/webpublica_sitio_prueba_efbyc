export class MarcadoresTest {
    public static markers:any=
    [
        {
            "CodES": "10004",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AV. ANTONIO RENDIC N° 3815",
            "Email": "10004@escopec.cl",
            "Fono": "5699827-0483",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6361582403046",
            "Longitud": "-70.3892576981952",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10005",
            "CodCom": "164",
            "Comuna": "DIEGO DE ALMAGRO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "AV. EL TOFO N° 043",
            "Email": "10005@escopec.cl",
            "Fono": "52 247-5067",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-26.2518331562569",
            "Longitud": "-69.6256222538434",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31",
                    "33"
                ]
            }
        },
        {
            "CodES": "10006",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AVDA PEDRO AGUIRRE CERDA N.10980",
            "Email": "10006@escopec.cl",
            "Fono": "55 221-4281",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.5613338460847",
            "Longitud": "-70.3905855012161",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "28",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "10008",
            "CodCom": "627",
            "Comuna": "CHAÑARAL",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "Panamericana Norte N°800",
            "Email": "10008@escopec.cl",
            "Fono": "",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-26.3366460000000",
            "Longitud": "-70.6045860000000",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "10",
                    "11",
                    "14",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "10009",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AV. MEJILLONES 4950 ESQ. ILLAPEL",
            "Email": "10009@escopec.cl",
            "Fono": "55 226-0185",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6251917194725",
            "Longitud": "-70.3912608038477",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10021",
            "CodCom": "3",
            "Comuna": "ARICA",
            "CodReg": "15",
            "Region": "ARICA Y PARINACOTA",
            "Company": "COPEC",
            "Direccion": "AV. DIEGO PORTALES Nº 1072",
            "Email": "10021@escopec.cl",
            "Fono": "58 224-9901",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-18.4739111702722",
            "Longitud": "-70.3033916304095",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "10040",
            "CodCom": "3",
            "Comuna": "ARICA",
            "CodReg": "15",
            "Region": "ARICA Y PARINACOTA",
            "Company": "COPEC",
            "Direccion": "SAN MARTIN N° 699",
            "Email": "10040@escopec.cl",
            "Fono": "56-58 223-0847",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-18.4798690334705",
            "Longitud": "-70.3133567961392",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "13",
                    "14",
                    "17",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "10056",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "SAN MARTIN ESQ. URIBE",
            "Email": "10056@escopec.cl",
            "Fono": "55 228-6534",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6491868025796",
            "Longitud": "-70.4011811036770",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "23",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "10075",
            "CodCom": "4",
            "Comuna": "TALTAL",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "FRANCISCO BILBAO N° 101",
            "Email": "10075@escopec.cl",
            "Fono": "55 261-1131",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-25.4077896849043",
            "Longitud": "-70.4785097972845",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "10080",
            "CodCom": "6",
            "Comuna": "IQUIQUE",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "VIVAR N° 402",
            "Email": "10080@escopec.cl",
            "Fono": "56-57-426803",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2128419286032",
            "Longitud": "-70.1483817621310",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10095",
            "CodCom": "671",
            "Comuna": "ALTO HOSPICIO",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "RUTA A-16 KM 322 / AVDA SANTA ROSA",
            "Email": "10095@escopec.cl",
            "Fono": "57-495855",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2647991525109",
            "Longitud": "-70.0789934822315",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "10096",
            "CodCom": "5",
            "Comuna": "POZO ALMONTE",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "Panamericana norte km 1810",
            "Email": "10096@escopec.cl",
            "Fono": "",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2425877103796",
            "Longitud": "-69.7868147472275",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "34",
                    "36"
                ]
            }
        },
        {
            "CodES": "10097",
            "CodCom": "671",
            "Comuna": "ALTO HOSPICIO",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "Av. Teniente Arturo Merino Correa 3945",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2685111000000",
            "Longitud": "-70.0953388888889",
            "Servicios": {
                "CodSer": [
                    "2",
                    "6",
                    "12",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10098",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "Av. Edmundo Perez Zujovic N° 10675",
            "Email": "10098@copec.cl",
            "Fono": "",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.5652345720000",
            "Longitud": "-70.3988686900000",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "28",
                    "30",
                    "33",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "10101",
            "CodCom": "7",
            "Comuna": "CALAMA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "GRANADEROS N° 3524",
            "Email": "10101@escopec.cl",
            "Fono": "55 2567561",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-22.4444478499567",
            "Longitud": "-68.9278492667963",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "16",
                    "17",
                    "19",
                    "25",
                    "26",
                    "28",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "10104",
            "CodCom": "3",
            "Comuna": "ARICA",
            "CodReg": "15",
            "Region": "ARICA Y PARINACOTA",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA SUR Nº 2824",
            "Email": "10104@escopec.cl",
            "Fono": "56-58-222025",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-18.4935547270518",
            "Longitud": "-70.2892138207883",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "10120",
            "CodCom": "3",
            "Comuna": "ARICA",
            "CodReg": "15",
            "Region": "ARICA Y PARINACOTA",
            "Company": "COPEC",
            "Direccion": " LUIS VALENTE ROSSI Nº 1990",
            "Email": "10120@escopec.cl",
            "Fono": "58 2260458",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-18.4904552879064",
            "Longitud": "-70.3022220335324",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "19",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10122",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AV. ANGAMOS N° 0633",
            "Email": "10122@escopec.cl",
            "Fono": "55 2893679",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6788069203081",
            "Longitud": "-70.4098457718382",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "19",
                    "23",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10145",
            "CodCom": "627",
            "Comuna": "CHAÑARAL",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "MERINO JARPA, PANAMERICANA NORTE S/N, KM 973",
            "Email": "10145@escopec.cl",
            "Fono": "56-52489692",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-26.3401307814351",
            "Longitud": "-70.6190613946098",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "10194",
            "CodCom": "9",
            "Comuna": "SIERRA GORDA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE S/N",
            "Email": "10194@escopec.cl",
            "Fono": "56-55-487615",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.1840293646029",
            "Longitud": "-69.6345751534424",
            "Servicios": {
                "CodSer": [
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33"
                ]
            }
        },
        {
            "CodES": "10197",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "RUTA 5 NORTE KM 1351",
            "Email": "10197@escopec.cl",
            "Fono": "56-55-486048",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.7853857584240",
            "Longitud": "-70.3166326999454",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33"
                ]
            }
        },
        {
            "CodES": "10223",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AV. ARGENTINA Nº 1103",
            "Email": "10223@escopec.cl",
            "Fono": "55 2241188",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6664725286564",
            "Longitud": "-70.3992164715968",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "20",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "10226",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AVDA. A. RENDIC ESQ. AVDA. P. A. CERDA",
            "Email": "10226@escopec.cl",
            "Fono": "56-55275295",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6037531886273",
            "Longitud": "-70.3888082875401",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "10235",
            "CodCom": "6",
            "Comuna": "IQUIQUE",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "SITIO Nº 54-B, BARRIO INDUSTRIAL",
            "Email": "10235@escopec.cl",
            "Fono": "56-57471593",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2047522830533",
            "Longitud": "-70.1415924319192",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "10238",
            "CodCom": "6",
            "Comuna": "IQUIQUE",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "PEDRO PRADO N° 2345",
            "Email": "10238@escopec.cl",
            "Fono": "57 2532601",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2309486165583",
            "Longitud": "-70.1359143318911",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "24",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "10282",
            "CodCom": "7",
            "Comuna": "CALAMA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "ABAROA ESQ. LATORRE S/N",
            "Email": "10282@escopec.cl",
            "Fono": "55 231-3459",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-22.4675923252462",
            "Longitud": "-68.9256487800441",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "28",
                    "30",
                    "31",
                    "32",
                    "36"
                ]
            }
        },
        {
            "CodES": "10322",
            "CodCom": "10",
            "Comuna": "MEJILLONES",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AVDA. GENERAL SAN MARTIN Nº 525",
            "Email": "10322@escopec.cl",
            "Fono": "56-55621588",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.0992697184524",
            "Longitud": "-70.4509029317257",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "10325",
            "CodCom": "6",
            "Comuna": "IQUIQUE",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "AV. PEDRO PRADO N° 2102",
            "Email": "10325@escopec.cl",
            "Fono": "57 2437070",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2292324267264",
            "Longitud": "-70.1353214968787",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10329",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AV. ARGENTINA N° 3211",
            "Email": "10329@escopec.cl",
            "Fono": "55 2288834",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6442641327104",
            "Longitud": "-70.3929213783546",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "16",
                    "17",
                    "23",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10335",
            "CodCom": "11",
            "Comuna": "SAN PEDRO DE ATACAMA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "TOCONAO S/N",
            "Email": "10335@escopec.cl",
            "Fono": "55 2439812",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-22.9133774748545",
            "Longitud": "-68.1989721995816",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "33"
                ]
            }
        },
        {
            "CodES": "10342",
            "CodCom": "7",
            "Comuna": "CALAMA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "DIEGO DE ALMAGRO Nº 2547",
            "Email": "10342@escopec.cl",
            "Fono": "91298311",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-22.4476276069062",
            "Longitud": "-68.9323937595796",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10347",
            "CodCom": "12",
            "Comuna": "TOCOPILLA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AV. 11 DE SEPTIEMBRE S/N",
            "Email": "10347@escopec.cl",
            "Fono": "55 2810468",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-22.0860656981415",
            "Longitud": "-70.1916702269726",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "10388",
            "CodCom": "3",
            "Comuna": "ARICA",
            "CodReg": "15",
            "Region": "ARICA Y PARINACOTA",
            "Company": "COPEC",
            "Direccion": "18 DE SEPTIEMBRE Nº 2401",
            "Email": "10388@escopec.cl",
            "Fono": "56-58-229392",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-18.4897865503140",
            "Longitud": "-70.2900741954554",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "10397",
            "CodCom": "671",
            "Comuna": "ALTO HOSPICIO",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "ALTO HOSPICIO, RUTA 16 KM 37",
            "Email": "10397@escopec.cl",
            "Fono": "57 2248280",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2739631220730",
            "Longitud": "-70.0955360281595",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "10398",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AVDA. E. PEREZ ZUJOVIC Nº 4256",
            "Email": "10398@escopec.cl",
            "Fono": "55 2934501",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6322535343474",
            "Longitud": "-70.3954858280563",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "10401",
            "CodCom": "7",
            "Comuna": "CALAMA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AVENIDA BALMACEDA N° 3012",
            "Email": "10401@escopec.cl",
            "Fono": "93210707",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-22.4527444900669",
            "Longitud": "-68.9215305447578",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "10405",
            "CodCom": "6",
            "Comuna": "IQUIQUE",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "MANUEL RODRIGUEZ Nº 1404.",
            "Email": "10405@escopec.cl",
            "Fono": "57 2349974",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2233593199890",
            "Longitud": "-70.1422219078402",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10421",
            "CodCom": "6",
            "Comuna": "IQUIQUE",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "ANIBAL PINTO N° 1377",
            "Email": "10421@escopec.cl",
            "Fono": "57 2391884",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2210442804362",
            "Longitud": "-70.1532941976626",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "10427",
            "CodCom": "3",
            "Comuna": "ARICA",
            "CodReg": "15",
            "Region": "ARICA Y PARINACOTA",
            "Company": "COPEC",
            "Direccion": "DIEGO PORTALES N° 1115",
            "Email": "10427@escopec.cl",
            "Fono": "58 2322553",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-18.4740653746175",
            "Longitud": "-70.3023431781722",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10436",
            "CodCom": "225",
            "Comuna": "PICA",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "AV. 27 DE ABRIL S/N",
            "Email": "10436@escopec.cl",
            "Fono": "57 2741653",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.4935672602798",
            "Longitud": "-69.3328261511653",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "10461",
            "CodCom": "2",
            "Comuna": "ANTOFAGASTA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AV. EJERCITO N° 286",
            "Email": "10461@escopec.cl",
            "Fono": "55 2378693",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-23.6712341896955",
            "Longitud": "-70.4075005753545",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10551",
            "CodCom": "6",
            "Comuna": "IQUIQUE",
            "CodReg": "1",
            "Region": "TARAPACÁ",
            "Company": "COPEC",
            "Direccion": "AV. 11 DE SEPTIEMBRE N° 1732",
            "Email": "10551@escopec.cl",
            "Fono": "56-57-438473",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-20.2411893117165",
            "Longitud": "-70.1439818976149",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "28",
                    "30",
                    "32",
                    "33",
                    "36",
                    "38",
                    "39"
                ]
            }
        },
        {
            "CodES": "10570",
            "CodCom": "3",
            "Comuna": "ARICA",
            "CodReg": "15",
            "Region": "ARICA Y PARINACOTA",
            "Company": "COPEC",
            "Direccion": "AV. SANTIAGO ARATA N° 3190",
            "Email": "10570@escopec.cl",
            "Fono": "56-58-213308",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-18.4549546801178",
            "Longitud": "-70.2947246139054",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36",
                    "39"
                ]
            }
        },
        {
            "CodES": "10572",
            "CodCom": "7",
            "Comuna": "CALAMA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "PUNTA DIAMANTE S/N, SALIDA SUR CHUQUICAMATA",
            "Email": "10572@escopec.cl",
            "Fono": "56-56330189",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-22.3295929918594",
            "Longitud": "-68.9274299106150",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "10615",
            "CodCom": "312",
            "Comuna": "MARIA ELENA",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "AVDA. PEDRO MONTT S/N",
            "Email": "10615@escopec.cl",
            "Fono": "56-55-639849",
            "JefeEDS": "Sergio I. Solis",
            "Latitud": "-22.3451991444285",
            "Longitud": "-69.6579623084575",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "10617",
            "CodCom": "164",
            "Comuna": "DIEGO DE ALMAGRO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "ANIBAL PINTO N° 400",
            "Email": "10617@escopec.cl",
            "Fono": "56-52-441111",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-26.3902411229742",
            "Longitud": "-70.0383004260173",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "10619",
            "CodCom": "4",
            "Comuna": "TALTAL",
            "CodReg": "2",
            "Region": "ANTOFAGASTA",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE KM 1144",
            "Email": "10619@escopec.cl",
            "Fono": "56-55-283947",
            "JefeEDS": "Benjamin Bastian",
            "Latitud": "-25.3987948797790",
            "Longitud": "-69.9643184244633",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33"
                ]
            }
        },
        {
            "CodES": "20001",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE Nº 0715",
            "Email": "20001@escopec.cl",
            "Fono": "56-42-430390",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.5903382449821",
            "Longitud": "-72.1028362922027",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20003",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "GABRIELA MISTRAL Nº 1625",
            "Email": "20003@escopec.cl",
            "Fono": "56-43-323256",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4735386547347",
            "Longitud": "-72.3280507588943",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "20005",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "O' HIGGINS Nº 613",
            "Email": "20005@escopec.cl",
            "Fono": "56-42-212517",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6061337256918",
            "Longitud": "-72.1086649792287",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20007",
            "CodCom": "16",
            "Comuna": "TEMUCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR S/N, KM 682",
            "Email": "20007@escopec.cl",
            "Fono": "56-45-334653",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7815648188762",
            "Longitud": "-72.6120400152725",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20011",
            "CodCom": "101",
            "Comuna": "CORONEL",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "SOTOMAYOR, ESQ. VALLE LOTA",
            "Email": "20011@escopec.cl",
            "Fono": "41 2773171",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.0353194009718",
            "Longitud": "-73.1419356750386",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20012",
            "CodCom": "313",
            "Comuna": "SAN RAFAEL",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 237",
            "Email": "20012@escopec.cl",
            "Fono": "56-71-1971523",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.3206555029363",
            "Longitud": "-71.5393566724787",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20013",
            "CodCom": "170",
            "Comuna": "YUMBEL",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CRUZ, ESQ. LAS HERAS",
            "Email": "20013@escopec.cl",
            "Fono": "43 243-9451",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-37.1038894653320",
            "Longitud": "-72.5616683959959",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20014",
            "CodCom": "153",
            "Comuna": "YUNGAY",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "ANGELES Nº 667",
            "Email": "20014@escopec.cl",
            "Fono": "56-42-680052",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-37.1257927237583",
            "Longitud": "-72.0128251532653",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20017",
            "CodCom": "146",
            "Comuna": "SAN CARLOS",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 383,3",
            "Email": "20017@escopec.cl",
            "Fono": "42 243-0872",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.4710529031548",
            "Longitud": "-72.0064013256319",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20018",
            "CodCom": "95",
            "Comuna": "PENCO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "COSMITO, PARCELA Nº 3, LOTE 1, CAMINO A PENCO",
            "Email": "20018@escopec.cl",
            "Fono": "41 238-1560",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7759346883933",
            "Longitud": "-73.0175325992842",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "20019",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "PAICAVI N° 1248",
            "Email": "20019@escopec.cl",
            "Fono": "56-41-2222358",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8152500000000",
            "Longitud": "-73.0462900000000",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20020",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. PEDRO DE VALDIVIA  1120 / LA MOCHITA",
            "Email": "20020@escopec.cl",
            "Fono": "56-41-2339407",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8471792566472",
            "Longitud": "-73.0517558218992",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20024",
            "CodCom": "251",
            "Comuna": "COLBUN",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "ADOLFO NOVOA Nº 120",
            "Email": "20024@escopec.cl",
            "Fono": "73 235-1210",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.6964952373239",
            "Longitud": "-71.4043702499317",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20025",
            "CodCom": "16",
            "Comuna": "TEMUCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "RUDECINDO ORTEGA S/N",
            "Email": "20025@escopec.cl",
            "Fono": "56-45-222656",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7110976992204",
            "Longitud": "-72.5580071232075",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20028",
            "CodCom": "19",
            "Comuna": "OSORNO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "ELEUTERIO RAMIREZ Nº 211",
            "Email": "20028@escopec.cl",
            "Fono": "56-64-252999",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.5730493423815",
            "Longitud": "-73.1448274621930",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "20030",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": " CAMINO INTERNACIONAL S/N, KM 46",
            "Email": "20030@escopec.cl",
            "Fono": "71 222-3048",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.6380962051861",
            "Longitud": "-71.2630497133120",
            "Servicios": {
                "CodSer": [
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20031",
            "CodCom": "13",
            "Comuna": "PINTO",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "J. J. JARPA S/N, SECTOR RECINTO",
            "Email": "20031@escopec.cl",
            "Fono": "56-42-373845",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.8416675759581",
            "Longitud": "-71.6595834927636",
            "Servicios": {
                "CodSer": [
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20032",
            "CodCom": "33",
            "Comuna": "LONCOCHE",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "BALMACEDA Nº 1121",
            "Email": "20032@escopec.cl",
            "Fono": "56-45-471150",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.3724204674548",
            "Longitud": "-72.6386931721131",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20035",
            "CodCom": "98",
            "Comuna": "PUERTO MONTT",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "AV. ANGELMO Nº 1920",
            "Email": "20035@escopec.cl",
            "Fono": "65 243-5965",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4829901590964",
            "Longitud": "-72.9584180769488",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20036",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "AV. LOS PUELCHES Nº 363",
            "Email": "20036@escopec.cl",
            "Fono": "56-42-235661",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6190990779632",
            "Longitud": "-72.0853408696430",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "13",
                    "14",
                    "17",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20043",
            "CodCom": "149",
            "Comuna": "ARAUCO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CONDELL, ESQ. KORNER",
            "Email": "20043@escopec.cl",
            "Fono": "56-46-551175",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.2445251600640",
            "Longitud": "-73.3109628625260",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20046",
            "CodCom": "330",
            "Comuna": "CONTULMO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "IGNACIO CARRERA PINTO N°315",
            "Email": "20046@escopec.cl",
            "Fono": "41 261-8465",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-38.0060734016056",
            "Longitud": "-73.2236629806708",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20049",
            "CodCom": "151",
            "Comuna": "SAN JAVIER",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CHORRILLOS Nº 1398",
            "Email": "20049@escopec.cl",
            "Fono": "56-73-322158",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.5955295301948",
            "Longitud": "-71.7291970648733",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20057",
            "CodCom": "83",
            "Comuna": "LINARES",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. LEON BUSTOS Nº 1051",
            "Email": "20057@escopec.cl",
            "Fono": "73 221-3377",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.8419914951663",
            "Longitud": "-71.6166980496476",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20059",
            "CodCom": "199",
            "Comuna": "LA UNION",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "RUTA 70 KM 4",
            "Email": "20059@escopec.cl",
            "Fono": "56-64-320610",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.3172108059875",
            "Longitud": "-73.0140546363441",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20062",
            "CodCom": "131",
            "Comuna": "FLORIDA",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "GENERAL LAGOS N° 666, (E. RAMIREZ S/N)",
            "Email": "20062@escopec.cl",
            "Fono": "56-41-2645473",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.8234348964671",
            "Longitud": "-72.6591489294097",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20063",
            "CodCom": "95",
            "Comuna": "PENCO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CALLE PENCO Nº 25",
            "Email": "20063@escopec.cl",
            "Fono": "41 245-4105",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7371520936026",
            "Longitud": "-72.9953762946062",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20071",
            "CodCom": "124",
            "Comuna": "MARIQUINA",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "RUTA 5 KM 786 SAN JOSE DE LA MARIQUINA",
            "Email": "20071@escopec.cl",
            "Fono": "63 245-2183",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.5450602886994",
            "Longitud": "-72.8808185646947",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20072",
            "CodCom": "208",
            "Comuna": "CHIGUAYANTE",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CALLE 8 ORIENTE N° 750",
            "Email": "20072@escopec.cl",
            "Fono": "41 236-8149",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8995700961280",
            "Longitud": "-73.0325277596482",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20074",
            "CodCom": "98",
            "Comuna": "PUERTO MONTT",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "CAMINO ALERCE 2000 ESQ. AVDA 4TA",
            "Email": "20074@escopec.cl",
            "Fono": "65 227-9758",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4591887722786",
            "Longitud": "-72.9184372887358",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "21",
                    "24",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20077",
            "CodCom": "208",
            "Comuna": "CHIGUAYANTE",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "O'HIGGINS Nº 2160, CHIGUAYANTE.",
            "Email": "20077@escopec.cl",
            "Fono": "56-41-2361181",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.9105470469664",
            "Longitud": "-73.0294806450308",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20078",
            "CodCom": "36",
            "Comuna": "PUNTA ARENAS",
            "CodReg": "12",
            "Region": "MAGALLANES",
            "Company": "COPEC",
            "Direccion": "SALVADOR ALLENDE N° 0396",
            "Email": "20078@escopec.cl",
            "Fono": "56-9-6400744",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-53.1580697182794",
            "Longitud": "-70.9309157989407",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "22",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20079",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "PRAT Nº 289, SAN MARTIN",
            "Email": "20079@escopec.cl",
            "Fono": "9885-5730",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8323830060078",
            "Longitud": "-73.0576353239824",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "8",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "24",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20080",
            "CodCom": "321",
            "Comuna": "LUMACO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "CAUPOLICAN N° 250",
            "Email": "20080@escopec.cl",
            "Fono": "56-45-753298",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.1912159003670",
            "Longitud": "-72.9905187816872",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20082",
            "CodCom": "98",
            "Comuna": "PUERTO MONTT",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "AV. LOS NOTROS, VOLCAN PUYEHUE",
            "Email": "20082@escopec.cl",
            "Fono": "56-65-435474",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4803531301511",
            "Longitud": "-72.9785000461184",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20084",
            "CodCom": "198",
            "Comuna": "CALBUCO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "ALMIRANTE LA TORRE S/N SECTOR SAN RAFAEL",
            "Email": "20084@escopec.cl",
            "Fono": "65 246-1396",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.7582233430235",
            "Longitud": "-73.1451846667701",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20085",
            "CodCom": "228",
            "Comuna": "CUNCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "AVENIDA SANTA MARIA N° 130",
            "Email": "20085@escopec.cl",
            "Fono": "45 257-3398",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.9320123134661",
            "Longitud": "-72.0361213646093",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20086",
            "CodCom": "106",
            "Comuna": "VALDIVIA",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "AVDA. PEDRO AGUIRRE CERDA N° 670",
            "Email": "20086@escopec.cl",
            "Fono": "63 221-2349",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.8097283019362",
            "Longitud": "-73.2172926667526",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "21",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20087",
            "CodCom": "183",
            "Comuna": "LOS LAGOS",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "QUINCHILCA S/N",
            "Email": "20087@escopec.cl",
            "Fono": "63 246-2402",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.8611538270760",
            "Longitud": "-72.7942712126500",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20088",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "AVDA. VICENTE MENDEZ ESQ. ANDRES BELLO",
            "Email": "20088@escopec.cl",
            "Fono": "42 263-8257",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.5907653302675",
            "Longitud": "-72.0698655869379",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20089",
            "CodCom": "26",
            "Comuna": "PUERTO VARAS",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 1009,4.",
            "Email": "20089@escopec.cl",
            "Fono": "65 228-6664",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.3512155311361",
            "Longitud": "-72.9816969553746",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20090",
            "CodCom": "217",
            "Comuna": "PARRAL",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CAMINO PANAMERICANA SUR KM. 341",
            "Email": "20090@escopec.cl",
            "Fono": "56-73-462120",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.1326495535374",
            "Longitud": "-71.8076071595300",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20091",
            "CodCom": "31",
            "Comuna": "SAN PABLO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM. 904,3 TRAFUN.",
            "Email": "20091@escopec.cl",
            "Fono": "56-64-242323",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.4642387480797",
            "Longitud": "-73.0306210045866",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20092",
            "CodCom": "108",
            "Comuna": "VICTORIA",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "LONGITUDINAL SUR KM 614,2",
            "Email": "20092@escopec.cl",
            "Fono": "45 292-3695",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.2793588171339",
            "Longitud": "-72.3653163261352",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20093",
            "CodCom": "300",
            "Comuna": "MAULLIN",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "GASPAR DEL RIO ESQ. 21 DE MAYO",
            "Email": "20093@escopec.cl",
            "Fono": "65 245-1597",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.6161390274761",
            "Longitud": "-73.5961247451439",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20094",
            "CodCom": "151",
            "Comuna": "SAN JAVIER",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CARRETERA LONGITUDINAL SUR KM 275,3",
            "Email": "20094@escopec.cl",
            "Fono": "73 232-2155",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.6163178904035",
            "Longitud": "-71.7018480371728",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "34",
                    "35",
                    "36"
                ]
            }
        },
        {
            "CodES": "20098",
            "CodCom": "508",
            "Comuna": "FUTALEUFU",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "FUTALEUFU, CARRETERA AUSTRAL CAMINO E LLIMITE S/N",
            "Email": "20098@escopec.cl",
            "Fono": "9643-0867",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-43.1892991563983",
            "Longitud": "-71.8535462232851",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "33"
                ]
            }
        },
        {
            "CodES": "20099",
            "CodCom": "77",
            "Comuna": "COYHAIQUE",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "RIO TRANQUILO S/N, CARRETERA AUSTRAL.",
            "Email": "20099@escopec.cl",
            "Fono": "8433-0043",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-46.6234903583228",
            "Longitud": "-72.6739358359373",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "14",
                    "26",
                    "30",
                    "33",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "20100",
            "CodCom": "661",
            "Comuna": "OHIGGINS",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "CARRETERA AUSTRAL S/N°, LOTE 26-A",
            "Email": "20100@escopec.cl",
            "Fono": "7609-1303",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-48.4634068250508",
            "Longitud": "-72.5611218810082",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20104",
            "CodCom": "146",
            "Comuna": "SAN CARLOS",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "LONGITUDINAL SUR, ESQ. MACKENNA",
            "Email": "20104@escopec.cl",
            "Fono": "56-42-412458",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.4264454313012",
            "Longitud": "-71.9481146897982",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20108",
            "CodCom": "25",
            "Comuna": "CAUQUENES",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "MAIPU, SAN MARTIN",
            "Email": "20108@escopec.cl",
            "Fono": "73-513629",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.9620349339731",
            "Longitud": "-72.3123746650321",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20109",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "ECUADOR, ESQ. O'HIGGINS",
            "Email": "20109@escopec.cl",
            "Fono": "56-42-247905",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.5980117876023",
            "Longitud": "-72.1062259461298",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20110",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "Avda. Colin N°996",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4458015434648",
            "Longitud": "-71.6864107469403",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20111",
            "CodCom": "98",
            "Comuna": "PUERTO MONTT",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "AV. CARDONAL S/N KM 3",
            "Email": "20111@escopec.cl",
            "Fono": "56-9-87683940",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4525984511694",
            "Longitud": "-73.0087307820850",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20115",
            "CodCom": "282",
            "Comuna": "TUCAPEL",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "LAUTARO N°313",
            "Email": "20115@escopec.cl",
            "Fono": "56-2-431975222",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.2926330203790",
            "Longitud": "-71.9514073473223",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20120",
            "CodCom": "138",
            "Comuna": "CASTRO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "GALVARINO RIVEROS N° 1205",
            "Email": "20120@escopec.cl",
            "Fono": "56-65-633007",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-42.4725162692689",
            "Longitud": "-73.7768219116687",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20121",
            "CodCom": "194",
            "Comuna": "QUIRIHUE",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "AV. PRAT Nº 850",
            "Email": "20121@escopec.cl",
            "Fono": "56-42-531245",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.2793482447116",
            "Longitud": "-72.5422791133129",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20123",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "PRAT, ESQ. ROZAS",
            "Email": "20123@escopec.cl",
            "Fono": "56-41-2223280",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8250638888888",
            "Longitud": "-73.0616200154230",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20124",
            "CodCom": "237",
            "Comuna": "SANTA BARBARA",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "PRAT Nº 942,  (DESPACHAR A CASILLA)",
            "Email": "20124@escopec.cl",
            "Fono": "56-43-581275",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.6700557075647",
            "Longitud": "-72.0162469745826",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20127",
            "CodCom": "83",
            "Comuna": "LINARES",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "KURT MOLLER, ESQ. CHACABUCO",
            "Email": "20127@escopec.cl",
            "Fono": "56-73-214927",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.8454675609489",
            "Longitud": "-71.5957572114385",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "26",
                    "30",
                    "33"
                ]
            }
        },
        {
            "CodES": "20128",
            "CodCom": "281",
            "Comuna": "MULCHEN",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "LASTARRIA N° 11",
            "Email": "20128@escopec.cl",
            "Fono": "56-43-562689",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.7132042285876",
            "Longitud": "-72.2474653839944",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20133",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "LONGITUDINAL SUR S/N",
            "Email": "20133@escopec.cl",
            "Fono": "56-43-361895",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4419011454286",
            "Longitud": "-72.3274340189959",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20135",
            "CodCom": "173",
            "Comuna": "COIHUECO",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "PRAT, ESQ. J. M. CARRERA",
            "Email": "20135@escopec.cl",
            "Fono": "56-9-2800151",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6277898879549",
            "Longitud": "-71.8403348311207",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20143",
            "CodCom": "228",
            "Comuna": "CUNCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "LLAIMA Nº 792",
            "Email": "20143@escopec.cl",
            "Fono": "45 257-3398",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.9324750329212",
            "Longitud": "-72.0270784772547",
            "Servicios": {
                "CodSer": [
                    "9",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20144",
            "CodCom": "187",
            "Comuna": "CURACAUTIN",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "MANUEL RODRIGUEZ, ESQ. SERRANO",
            "Email": "20144@escopec.cl",
            "Fono": "45 288-1990",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.4363021422612",
            "Longitud": "-71.8898911421440",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20146",
            "CodCom": "235",
            "Comuna": "GALVARINO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "MATTA, ESQ. MAIPU",
            "Email": "20146@escopec.cl",
            "Fono": "56-45-513006",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.4143690680956",
            "Longitud": "-72.7780748294513",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20148",
            "CodCom": "189",
            "Comuna": "PITRUFQUEN",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "O'HIGGINS Nº 680, ESQ. BALMACEDA",
            "Email": "20148@escopec.cl",
            "Fono": "56-45-393162",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.9838217810885",
            "Longitud": "-72.6408141676571",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20149",
            "CodCom": "26",
            "Comuna": "PUERTO VARAS",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "WALKER MARTINEZ N° 315",
            "Email": "20149@escopec.cl",
            "Fono": "56-991332660",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.3167950000000",
            "Longitud": "-72.9844270000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "20150",
            "CodCom": "16",
            "Comuna": "TEMUCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "GRAL. MACKENNA Nº 712, ESQ. VARAS",
            "Email": "20150@escopec.cl",
            "Fono": "56-45-210202",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7412153250136",
            "Longitud": "-72.5868758127772",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20153",
            "CodCom": "246",
            "Comuna": "LONQUIMAY",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "21 DE MAYO S/N",
            "Email": "20153@escopec.cl",
            "Fono": "56-45-891074",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.4494878722293",
            "Longitud": "-71.3726504071082",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20156",
            "CodCom": "267",
            "Comuna": "PUCON",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "FRESIA 547, ESQ. BRASIL",
            "Email": "20156@escopec.cl",
            "Fono": "45 244-4634",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-39.2779011078319",
            "Longitud": "-71.9754216758761",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20157",
            "CodCom": "16",
            "Comuna": "TEMUCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "RUDECINDO ORTEGA Nº 1471",
            "Email": "20157@escopec.cl",
            "Fono": "56-45-402260",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7193033897121",
            "Longitud": "-72.5675292405983",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20178",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "KM 18 CAMINO ANTUCO",
            "Email": "20178@escopec.cl",
            "Fono": "43 197-1754",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4286102473813",
            "Longitud": "-72.1315220355202",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20182",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "VALDIVIA Nº 100, AV. VICUÑA",
            "Email": "20182@escopec.cl",
            "Fono": "56-43-311047",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4723021625115",
            "Longitud": "-72.3528252737020",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20183",
            "CodCom": "238",
            "Comuna": "CABRERO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "SALIDA CAMINO CABRERO A CONCEPCION",
            "Email": "20183@escopec.cl",
            "Fono": "56-43-411081",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-37.0336498687013",
            "Longitud": "-72.4172043126153",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20188",
            "CodCom": "206",
            "Comuna": "CHAITEN",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "AV. CORCOVADO Nº 453",
            "Email": "20188@escopec.cl",
            "Fono": "56-65-731289",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-42.9170450602066",
            "Longitud": "-72.7126661694076",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "33"
                ]
            }
        },
        {
            "CodES": "20189",
            "CodCom": "98",
            "Comuna": "PUERTO MONTT",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "SAN MARTIN, ESQ. BENAVENTE",
            "Email": "20189@escopec.cl",
            "Fono": "56-65-252417",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4707990369512",
            "Longitud": "-72.9407927883332",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20192",
            "CodCom": "19",
            "Comuna": "OSORNO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "AV. MACKENNA N° 2019.",
            "Email": "20192@escopec.cl",
            "Fono": "56-64-260600",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.5761029436477",
            "Longitud": "-73.1158601212935",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "21",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20193",
            "CodCom": "135",
            "Comuna": "FRUTILLAR",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "ARTURO ALESSANDRI Nº 187",
            "Email": "20193@escopec.cl",
            "Fono": "65 242-1193",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.1220802720488",
            "Longitud": "-73.0597509053927",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20197",
            "CodCom": "299",
            "Comuna": "PUYEHUE",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "MANUEL RODRIGUEZ Nº 69",
            "Email": "20197@escopec.cl",
            "Fono": "56-64-371245",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.6831400005208",
            "Longitud": "-72.6045000001119",
            "Servicios": {
                "CodSer": [
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20199",
            "CodCom": "179",
            "Comuna": "FUTRONO",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "BALMACEDA Nº 100",
            "Email": "20199@escopec.cl",
            "Fono": "56-63-482451",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.1296998747843",
            "Longitud": "-72.3915495152165",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20200",
            "CodCom": "19",
            "Comuna": "OSORNO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "BULNES Nº 544",
            "Email": "20200@escopec.cl",
            "Fono": "9703-0192",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.5723577885181",
            "Longitud": "-73.1384065610029",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20201",
            "CodCom": "218",
            "Comuna": "PANGUIPULLI",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "M. ROZAS Nº 468",
            "Email": "20201@escopec.cl",
            "Fono": "63 231-2430",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.6421784270144",
            "Longitud": "-72.3327529063872",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20202",
            "CodCom": "342",
            "Comuna": "PALENA",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "HURTADO DE MENDOZA S/N",
            "Email": "20202@escopec.cl",
            "Fono": "65 225-8623",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-43.6194989041031",
            "Longitud": "-71.8032272709179",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20203",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CARRETERA LONGITUDINAL SUR km 255",
            "Email": "20203@escopec.cl",
            "Fono": "56-71-242481",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4316354908256",
            "Longitud": "-71.6364901133601",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20204",
            "CodCom": "72",
            "Comuna": "AISEN",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "SARGENTO ALDEA N° 1902",
            "Email": "20204@escopec.cl",
            "Fono": "8375-7473",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-45.3998004426505",
            "Longitud": "-72.6812749424577",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20207",
            "CodCom": "77",
            "Comuna": "COYHAIQUE",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "BALMACEDA Nº 455",
            "Email": "20207@escopec.cl",
            "Fono": "56-67-231362",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-45.5706376184747",
            "Longitud": "-72.0697545441235",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20209",
            "CodCom": "137",
            "Comuna": "CHILE CHICO",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "Q. RODRIGUEZ Nº 1",
            "Email": "20209@escopec.cl",
            "Fono": "56-67-411335",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-46.5363696749279",
            "Longitud": "-71.7312841958229",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "33"
                ]
            }
        },
        {
            "CodES": "20212",
            "CodCom": "251",
            "Comuna": "COLBUN",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "ENTRADA A PUEBLO COLBUN",
            "Email": "20212@escopec.cl",
            "Fono": "73 235-1228",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.7010211264991",
            "Longitud": "-71.4075315480077",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20213",
            "CodCom": "98",
            "Comuna": "PUERTO MONTT",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA Nº 200",
            "Email": "20213@escopec.cl",
            "Fono": "56-65-293838",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4625367865722",
            "Longitud": "-72.9536910371831",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20214",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "2 SUR, ESQ. 1 ORIENTE",
            "Email": "20214@escopec.cl",
            "Fono": "56-71-233188",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4281209425769",
            "Longitud": "-71.6651797844416",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20215",
            "CodCom": "171",
            "Comuna": "ANCUD",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "LIBERTAD Nº 547",
            "Email": "20215@escopec.cl",
            "Fono": "56-9-3590855",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.8669042646864",
            "Longitud": "-73.8292646534085",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20217",
            "CodCom": "26",
            "Comuna": "PUERTO VARAS",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "SANTA  ROSA, ESQ. MARTINEZ",
            "Email": "20217@escopec.cl",
            "Fono": "56-65-237313",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.3166222683052",
            "Longitud": "-72.9833218727551",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "32",
                    "38"
                ]
            }
        },
        {
            "CodES": "20218",
            "CodCom": "301",
            "Comuna": "LOS MUERMOS",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "AV. PRINCIPAL S/N, RIO FRIO",
            "Email": "20218@escopec.cl",
            "Fono": "56-65-707255",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.2890355242160",
            "Longitud": "-73.4401435123939",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20219",
            "CodCom": "106",
            "Comuna": "VALDIVIA",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "PICARTE Nº 895, A. MUÑOZ",
            "Email": "20219@escopec.cl",
            "Fono": "56-63-246890",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.8160591777544",
            "Longitud": "-73.2370286494034",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "17",
                    "23",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20222",
            "CodCom": "183",
            "Comuna": "LOS LAGOS",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "BALMACEDA, ESQ. SAN MARTIN",
            "Email": "20222@escopec.cl",
            "Fono": "56-63-461361",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.8614286654983",
            "Longitud": "-72.8155024594440",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20223",
            "CodCom": "185",
            "Comuna": "SAN CLEMENTE",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "HUAMACHUCO Nº 1261",
            "Email": "20223@escopec.cl",
            "Fono": "71 262-0488",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.5347903605217",
            "Longitud": "-71.4932913134970",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20226",
            "CodCom": "138",
            "Comuna": "CASTRO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "O'HIGGINS Nº 495, SOTOMAYOR",
            "Email": "20226@escopec.cl",
            "Fono": "65 263-8787",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-42.4806358753982",
            "Longitud": "-73.7652171015028",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "33",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20228",
            "CodCom": "205",
            "Comuna": "PURRANQUE",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "COCHRANE Nº 415",
            "Email": "20228@escopec.cl",
            "Fono": "64 235-3353",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-40.9152525407183",
            "Longitud": "-73.1618478893135",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20230",
            "CodCom": "245",
            "Comuna": "PAILLACO",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "COCHRANE Nº 415",
            "Email": "20230@escopec.cl",
            "Fono": "56-63-421054",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.0742903061781",
            "Longitud": "-72.8715738024696",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20232",
            "CodCom": "196",
            "Comuna": "RIO BUENO",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "COMERCIO Nº 972, ESQ. AV. BDO. O'HIGGINS",
            "Email": "20232@escopec.cl",
            "Fono": "56-64-342555",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.3364777862431",
            "Longitud": "-72.9563844235564",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20235",
            "CodCom": "124",
            "Comuna": "MARIQUINA",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "JOSE PUCHI N°127 ESQ. CRISTOBAL COL",
            "Email": "20235@escopec.cl",
            "Fono": "56-2-632452424",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.5398554878286",
            "Longitud": "-72.9671558316598",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20236",
            "CodCom": "151",
            "Comuna": "SAN JAVIER",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "BALMACEDA, ESQ. CHORRILLOS",
            "Email": "20236@escopec.cl",
            "Fono": "56-73-322156",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.5959138812141",
            "Longitud": "-71.7293378427204",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20238",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "6 ORIENTE Nº 1274",
            "Email": "20238@escopec.cl",
            "Fono": "71 222-0415",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4254259085299",
            "Longitud": "-71.6579893311669",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20239",
            "CodCom": "199",
            "Comuna": "LA UNION",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "COMERCIO Nº 557",
            "Email": "20239@escopec.cl",
            "Fono": "64 232-4123",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.2975366359881",
            "Longitud": "-73.0802824957461",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20242",
            "CodCom": "163",
            "Comuna": "CONSTITUCION",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. MAC-IVER, ESQ. SANTA MARIA",
            "Email": "20242@escopec.cl",
            "Fono": "71 267-1283",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.3273358459294",
            "Longitud": "-72.4201751446967",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20272",
            "CodCom": "16",
            "Comuna": "TEMUCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "AV. CAUPOLICAN Nº 0451",
            "Email": "20272@escopec.cl",
            "Fono": "56-45-340273",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7440429681813",
            "Longitud": "-72.6078928808602",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20280",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "LOS CARRERAS Nº 801",
            "Email": "20280@escopec.cl",
            "Fono": "56-41-2229037",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8223011502607",
            "Longitud": "-73.0503943115691",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "20",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20283",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "AV. COLLIN Nº 1101",
            "Email": "20283@escopec.cl",
            "Fono": "42 221-1781",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6166049466336",
            "Longitud": "-72.0975202665249",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "21",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20285",
            "CodCom": "686",
            "Comuna": "HUALPEN",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "COLON Nº 7601",
            "Email": "20285@escopec.cl",
            "Fono": "41 241-7816",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7845132843015",
            "Longitud": "-73.0864540170516",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20286",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CAMINO A BULNES KM  5, PALOMARES",
            "Email": "20286@escopec.cl",
            "Fono": "41 232-3258",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8178711900263",
            "Longitud": "-72.9889311029573",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20288",
            "CodCom": "83",
            "Comuna": "LINARES",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. ANIBAL LEON BUSTOS Nº 047",
            "Email": "20288@escopec.cl",
            "Fono": "56-73-210338",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.8445053884978",
            "Longitud": "-71.6046292739533",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20290",
            "CodCom": "336",
            "Comuna": "TIRUA",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "LOTE 6 B S/N, SECTOR PONOTRO TIRUA",
            "Email": "20290@escopec.cl",
            "Fono": "41 261-4014",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-38.2807664028237",
            "Longitud": "-73.4977726196448",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20316",
            "CodCom": "207",
            "Comuna": "QUELLON",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "LADRILLEROS, ESQ. LA PAZ",
            "Email": "20316@escopec.cl",
            "Fono": "56-65-681230",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-43.1171797841691",
            "Longitud": "-73.6191509131230",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20327",
            "CodCom": "19",
            "Comuna": "OSORNO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "REPUBLICA, ESQ. VICTORIA",
            "Email": "20327@escopec.cl",
            "Fono": "64 248-8615",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.5737157136601",
            "Longitud": "-73.1581223707642",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20336",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "LONGITUDINAL SUR 5 KM. 409",
            "Email": "20336@escopec.cl",
            "Fono": "56-42-262267",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6333003032995",
            "Longitud": "-72.1889835400084",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20341",
            "CodCom": "130",
            "Comuna": "BULNES",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "SARGENTO ALDEA Nº 498",
            "Email": "20341@escopec.cl",
            "Fono": "56-42-631443",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.7489250992294",
            "Longitud": "-72.2869063742258",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20363",
            "CodCom": "240",
            "Comuna": "VILLARRICA",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "PEDRO DE VALDIVIA Nº 302, BALMACEDA",
            "Email": "20363@escopec.cl",
            "Fono": "56-45-412483",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-39.2798317672723",
            "Longitud": "-72.2319603012020",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20373",
            "CodCom": "623",
            "Comuna": "CAÑETE",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. PRESIDENTE FREI Nº 1063",
            "Email": "20373@escopec.cl",
            "Fono": "56-46-611178",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.7903982366545",
            "Longitud": "-73.3888209125084",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20374",
            "CodCom": "240",
            "Comuna": "VILLARRICA",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "GERONIMO DE ALDERETE Nº 857",
            "Email": "20374@escopec.cl",
            "Fono": "56-45-414578",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-39.2845871022328",
            "Longitud": "-72.2306024915833",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "25",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20375",
            "CodCom": "276",
            "Comuna": "LANCO",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA S/N",
            "Email": "20375@escopec.cl",
            "Fono": "56-63-441511",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.4501506449317",
            "Longitud": "-72.7803089081747",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20379",
            "CodCom": "95",
            "Comuna": "PENCO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CAMILO HENRIQUEZ Nº 8, LIRQUEN",
            "Email": "20379@escopec.cl",
            "Fono": "41 238-4089",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7131603385363",
            "Longitud": "-72.9756845054255",
            "Servicios": {
                "CodSer": [
                    "9",
                    "14",
                    "30"
                ]
            }
        },
        {
            "CodES": "20380",
            "CodCom": "163",
            "Comuna": "CONSTITUCION",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CAMINO A CONSTITUCION KM 11",
            "Email": "20380@escopec.cl",
            "Fono": "71 267-6334",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.3939722141921",
            "Longitud": "-72.3716248274320",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20382",
            "CodCom": "208",
            "Comuna": "CHIGUAYANTE",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "MANUEL RODRIGUEZ Nº 725, CHIGUAYANT",
            "Email": "20382@escopec.cl",
            "Fono": "56-41-2337195",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.9277230371333",
            "Longitud": "-73.0239593126009",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20389",
            "CodCom": "100",
            "Comuna": "SAN PEDRO DE LA PAZ",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "P.A.C. Nº 299, SAN PEDRO",
            "Email": "20389@escopec.cl",
            "Fono": "56-41-2370362",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8421797761807",
            "Longitud": "-73.0799379021613",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20393",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "21 DE MAYO N°2750 ESQ. CARLOS OLIVER",
            "Email": "20393@escopec.cl",
            "Fono": "41 279-4039",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8102003000255",
            "Longitud": "-73.0733645559316",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "23",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20396",
            "CodCom": "283",
            "Comuna": "NUEVA IMPERIAL",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "O'HIGGINS Nº 498",
            "Email": "20396@escopec.cl",
            "Fono": "56-45-611011",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7482805221808",
            "Longitud": "-72.9516798217856",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20401",
            "CodCom": "217",
            "Comuna": "PARRAL",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "BUIN Nº 395, ESQ. DIECIOCHO, PARRAL",
            "Email": "20401@escopec.cl",
            "Fono": "73 242-1006",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.1423013220185",
            "Longitud": "-71.8198833617071",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20402",
            "CodCom": "14",
            "Comuna": "TALCAHUANO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "VALDIVIA Nº 60",
            "Email": "20402@escopec.cl",
            "Fono": "56-41-2997734",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7114112534197",
            "Longitud": "-73.1149418809668",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "20407",
            "CodCom": "100",
            "Comuna": "SAN PEDRO DE LA PAZ",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. MICHIMALONCO Nº 1300",
            "Email": "20407@escopec.cl",
            "Fono": "41 228-2116",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8436376309907",
            "Longitud": "-73.0977956691725",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "28",
                    "30",
                    "31",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20417",
            "CodCom": "155",
            "Comuna": "GORBEA",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "O'HIGGINS Nº 290",
            "Email": "20417@escopec.cl",
            "Fono": "56-45-490169",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.1007074518005",
            "Longitud": "-72.6734431969201",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20424",
            "CodCom": "254",
            "Comuna": "CISNES",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "CARDENAL CARO S/N",
            "Email": "20424@escopec.cl",
            "Fono": "91382588",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-44.7314211196910",
            "Longitud": "-72.6813830236768",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20425",
            "CodCom": "254",
            "Comuna": "CISNES",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "AYSEN S/N,  PUYUHUAPI",
            "Email": "20425@escopec.cl",
            "Fono": "67 232-5245",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-44.3261618321306",
            "Longitud": "-72.5661454375663",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20429",
            "CodCom": "14",
            "Comuna": "TALCAHUANO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "PEREZ GACITUA, ESQ. BENAVENTE",
            "Email": "20429@escopec.cl",
            "Fono": "56-41-2541860",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7278238829048",
            "Longitud": "-73.1093472590838",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20430",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "UNO NORTE Nº 2315",
            "Email": "20430@escopec.cl",
            "Fono": "71 261-3530",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4277170455349",
            "Longitud": "-71.6428343074971",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "21",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20431",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "2 Norte N° 2310",
            "Email": "20431@copec.cl",
            "Fono": "",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4269225370000",
            "Longitud": "-71.6428543470000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20432",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "12 ORIENTE Nº 836",
            "Email": "20432@escopec.cl",
            "Fono": "56-71-242793",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4321681725592",
            "Longitud": "-71.6481166096575",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20435",
            "CodCom": "99",
            "Comuna": "LOTA",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "BANNEN, ESQ. SERRANO",
            "Email": "20435@escopec.cl",
            "Fono": "56-41-2876483",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.0885410621121",
            "Longitud": "-73.1578344160157",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "20436",
            "CodCom": "400",
            "Comuna": "RIO CLARO",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "OPAZO Nº 36,  CUMPEO",
            "Email": "20436@escopec.cl",
            "Fono": "56-71-291020",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.2832451552020",
            "Longitud": "-71.2601058440657",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20442",
            "CodCom": "106",
            "Comuna": "VALDIVIA",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "PICARTE, ESQ. SIMPSON",
            "Email": "20442@escopec.cl",
            "Fono": "56-63-216502",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.8286461230617",
            "Longitud": "-73.2220069133204",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20444",
            "CodCom": "329",
            "Comuna": "PEMUCO",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "COLON, ESQ. MIRAFLORES",
            "Email": "20444@escopec.cl",
            "Fono": "56-42-653013",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.9743312451009",
            "Longitud": "-72.0954104812953",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20447",
            "CodCom": "316",
            "Comuna": "COBQUECURA",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "REHUE N° 618",
            "Email": "20447@escopec.cl",
            "Fono": "42 198-3227",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.1347034531919",
            "Longitud": "-72.7964604110889",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20449",
            "CodCom": "240",
            "Comuna": "VILLARRICA",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "CAMINO A COÑARIPE Nº 555, LICAN-RAY",
            "Email": "20449@escopec.cl",
            "Fono": "56-45-431189",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-39.4848991983678",
            "Longitud": "-72.1587454327409",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20455",
            "CodCom": "19",
            "Comuna": "OSORNO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "LOS CARRERAS, ESQ. AV. PRAT",
            "Email": "20455@escopec.cl",
            "Fono": "56-7-6091303",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.5722637996107",
            "Longitud": "-73.1295575415366",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20456",
            "CodCom": "191",
            "Comuna": "LEBU",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "ANDRES BELLO, ESQ. J. A. RIOS",
            "Email": "20456@escopec.cl",
            "Fono": "56-46-511252",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.6096816519716",
            "Longitud": "-73.6588065956335",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20467",
            "CodCom": "391",
            "Comuna": "MAFIL",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "SAN MARTIN Nº 96",
            "Email": "20467@escopec.cl",
            "Fono": "56-63-411205",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.6652814040293",
            "Longitud": "-72.9518736825635",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20475",
            "CodCom": "13",
            "Comuna": "PINTO",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "CAMINO CHILLAN A RECINTO",
            "Email": "20475@escopec.cl",
            "Fono": "56-42-973644",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6963622260770",
            "Longitud": "-71.8990016936013",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20479",
            "CodCom": "14",
            "Comuna": "TALCAHUANO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "PEDRO MONTT S/N, BASE NAVAL",
            "Email": "20479@escopec.cl",
            "Fono": "56-41-2505574",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7007262368131",
            "Longitud": "-73.1099145940518",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "14",
                    "30"
                ]
            }
        },
        {
            "CodES": "20495",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "EJERCITO N° 2001",
            "Email": "20495@escopec.cl",
            "Fono": "56-41-2245899",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8060932459062",
            "Longitud": "-73.0425420152131",
            "Servicios": {
                "CodSer": [
                    "9",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20500",
            "CodCom": "16",
            "Comuna": "TEMUCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "BALMACEDA Nº 1629",
            "Email": "20500@escopec.cl",
            "Fono": "45 221-7570",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7344462261388",
            "Longitud": "-72.5776352608398",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20501",
            "CodCom": "238",
            "Comuna": "CABRERO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "RUTA Q 50, KM 11 DESDE  CABRERO A CONCEPCION",
            "Email": "20501@escopec.cl",
            "Fono": "56-9-57894487",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-37.0205501135402",
            "Longitud": "-72.4721020146483",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "35",
                    "36"
                ]
            }
        },
        {
            "CodES": "20502",
            "CodCom": "30",
            "Comuna": "LLANQUIHUE",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "GABRIELA MISTRAL N° 955, SECTOR ALE",
            "Email": "20502@escopec.cl",
            "Fono": "",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4047519328772",
            "Longitud": "-72.9166299507235",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20505",
            "CodCom": "17",
            "Comuna": "LOS ALAMOS",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. IGNACIO CARRERA PINTO N° 24",
            "Email": "20505@escopec.cl",
            "Fono": "45 261-1178",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.6286230230036",
            "Longitud": "-73.4681954072054",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20507",
            "CodCom": "138",
            "Comuna": "CASTRO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "Panamericana Sur 1170",
            "Email": "20597@escopec.cl",
            "Fono": "",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-42.4672946357952",
            "Longitud": "-73.7661001439617",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "14",
                    "25",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "20509",
            "CodCom": "149",
            "Comuna": "ARAUCO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "S-0509 ARAUCO Horcones",
            "Email": "20509@escopec.cl",
            "Fono": "",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.1949156115799",
            "Longitud": "-73.1999732578043",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "10",
                    "11",
                    "14",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20512",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "Avenida Alemania N° 1.075",
            "Email": "20512@escopec.cl",
            "Fono": "",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4675830000000",
            "Longitud": "-72.3331110000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20513",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "Lircay N° 3070",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4061200000000",
            "Longitud": "-71.6434980000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20517",
            "CodCom": "670",
            "Comuna": "SANTA JUANA",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "Lautaro  Acceso Santa Juana 1262",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.1666908854954",
            "Longitud": "-72.9519762959180",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20523",
            "CodCom": "19",
            "Comuna": "OSORNO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "RENE SORIANO Nº 2335",
            "Email": "20523@escopec.cl",
            "Fono": "56-64-235547",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.5809349526500",
            "Longitud": "-73.1133557280039",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20529",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. VICUÑA MACKENNA Nº 1241",
            "Email": "20529@escopec.cl",
            "Fono": "43 223-2263",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4798617051599",
            "Longitud": "-72.3653447954537",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20530",
            "CodCom": "21",
            "Comuna": "CARAHUE",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "MANUEL MONTT Nº 15",
            "Email": "20530@escopec.cl",
            "Fono": "56-45-651606",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7097761986840",
            "Longitud": "-73.1567640171476",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20531",
            "CodCom": "22",
            "Comuna": "EL CARMEN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "ENTRADA A PUEBLO S/N",
            "Email": "20531@escopec.cl",
            "Fono": "56-42-661065",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.8972942462396",
            "Longitud": "-72.0352125190426",
            "Servicios": {
                "CodSer": [
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20537",
            "CodCom": "23",
            "Comuna": "DALCAHUE",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "RAMON FREIRE PONIENTE N° 595",
            "Email": "20537@escopec.cl",
            "Fono": "65 264-1360",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-42.3803688382026",
            "Longitud": "-73.6547880934913",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20548",
            "CodCom": "24",
            "Comuna": "RETIRO",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "ACC. PRINCIPAL, ESQ. VICUÑA MACKENNA S/N",
            "Email": "20548@escopec.cl",
            "Fono": "56-73-421006",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.0544279899370",
            "Longitud": "-71.7664190453447",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "20549",
            "CodCom": "25",
            "Comuna": "CAUQUENES",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. DOCTOR MEZA S/N, (CAMINO A CHANCO)",
            "Email": "20549@escopec.cl",
            "Fono": "73 251-5413",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.9550373463156",
            "Longitud": "-72.3408646164859",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20551",
            "CodCom": "26",
            "Comuna": "PUERTO VARAS",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "ENSENADA KM 48",
            "Email": "20551@escopec.cl",
            "Fono": "56-65-212001",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.2123446915045",
            "Longitud": "-72.5430565250150",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20561",
            "CodCom": "17",
            "Comuna": "LOS ALAMOS",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. DIEGO PORTALES Nº 1855",
            "Email": "20561@escopec.cl",
            "Fono": "56-46-534032",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.6058456616926",
            "Longitud": "-73.4068942749697",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20563",
            "CodCom": "98",
            "Comuna": "PUERTO MONTT",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "Av. Marathon 1100",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4735100000000",
            "Longitud": "-72.9173540000000",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20565",
            "CodCom": "218",
            "Comuna": "PANGUIPULLI",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "Guido Beck de Ramberga N°1484",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.5744080000000",
            "Longitud": "-71.9995720000000",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14"
                ]
            }
        },
        {
            "CodES": "20600",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "AV. ARGENTINA N° 797",
            "Email": "20600@escopec.cl",
            "Fono": "56-42-219781",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6122166191134",
            "Longitud": "-72.0938136360548",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "19",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20604",
            "CodCom": "30",
            "Comuna": "LLANQUIHUE",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "VICENTE PEREZ ROSALES N° 711",
            "Email": "20604@escopec.cl",
            "Fono": "56-65-242037",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.2636654938262",
            "Longitud": "-73.0019303747355",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20605",
            "CodCom": "31",
            "Comuna": "SAN PABLO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 906",
            "Email": "20605@escopec.cl",
            "Fono": "56-64-381618",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-40.4778807755098",
            "Longitud": "-73.0346744781914",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20606",
            "CodCom": "32",
            "Comuna": "FREIRE",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 698",
            "Email": "20606@escopec.cl",
            "Fono": "45 239-9040",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.9380806943039",
            "Longitud": "-72.6207341822307",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "35",
                    "36"
                ]
            }
        },
        {
            "CodES": "20607",
            "CodCom": "33",
            "Comuna": "LONCOCHE",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA SUR KM 748",
            "Email": "20607@escopec.cl",
            "Fono": "56-45-479983",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.3543076571214",
            "Longitud": "-72.5775520227554",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20609",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 518,5",
            "Email": "20609@escopec.cl",
            "Fono": "56-43-349412",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.5467853666652",
            "Longitud": "-72.3093615615516",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20615",
            "CodCom": "34",
            "Comuna": "LAUTARO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "O'HIGGINS N° 1174",
            "Email": "20615@escopec.cl",
            "Fono": "56-45-534464",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.5351291947720",
            "Longitud": "-72.4363472588243",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20618",
            "CodCom": "35",
            "Comuna": "NATALES",
            "CodReg": "12",
            "Region": "MAGALLANES",
            "Company": "COPEC",
            "Direccion": "BULNES Nº 239",
            "Email": "20618@escopec.cl",
            "Fono": "56-61-411951",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-51.7284449393817",
            "Longitud": "-72.5093827757320",
            "Servicios": {
                "CodSer": [
                    "4",
                    "9",
                    "10",
                    "11",
                    "14",
                    "16",
                    "26",
                    "30",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20619",
            "CodCom": "36",
            "Comuna": "PUNTA ARENAS",
            "CodReg": "12",
            "Region": "MAGALLANES",
            "Company": "COPEC",
            "Direccion": "21 DE MAYO, ESQ. INDEPENDENCIA",
            "Email": "20619@escopec.cl",
            "Fono": "56-61-2242350",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-53.1667153940034",
            "Longitud": "-70.9109258039258",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "24",
                    "26",
                    "30",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20620",
            "CodCom": "36",
            "Comuna": "PUNTA ARENAS",
            "CodReg": "12",
            "Region": "MAGALLANES",
            "Company": "COPEC",
            "Direccion": "AV. BULNES, ESQ. JOSE GONZALEZ",
            "Email": "20620@escopec.cl",
            "Fono": "56-61-214671",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-53.1393082171215",
            "Longitud": "-70.8883499535455",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "33",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20622",
            "CodCom": "36",
            "Comuna": "PUNTA ARENAS",
            "CodReg": "12",
            "Region": "MAGALLANES",
            "Company": "COPEC",
            "Direccion": "AV. BULNES Nº 04486",
            "Email": "20622@escopec.cl",
            "Fono": "61 223-1376",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-53.1269192103775",
            "Longitud": "-70.8727047861487",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "22",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20642",
            "CodCom": "37",
            "Comuna": "PORVENIR",
            "CodReg": "12",
            "Region": "MAGALLANES",
            "Company": "COPEC",
            "Direccion": "SANTA  MARIA Nº 250",
            "Email": "20642@escopec.cl",
            "Fono": "61 258-0350",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-53.3009627515661",
            "Longitud": "-70.3649609894933",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20647",
            "CodCom": "36",
            "Comuna": "PUNTA ARENAS",
            "CodReg": "12",
            "Region": "MAGALLANES",
            "Company": "COPEC",
            "Direccion": "DIAGONAL DON BOSCO, ESQ. SARMIENTO",
            "Email": "20647@escopec.cl",
            "Fono": "56-61-248723",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-53.1567132407990",
            "Longitud": "-70.9014413432077",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "14",
                    "22",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "20648",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 245,  N°6005, PANGUILEMO",
            "Email": "20648@escopec.cl",
            "Fono": "71 223-2193",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.3788862112962",
            "Longitud": "-71.6019299624601",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20674",
            "CodCom": "14",
            "Comuna": "TALCAHUANO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "GRAN BRETANA Nº 5475",
            "Email": "20674@escopec.cl",
            "Fono": "56-41-2410962",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7728200000000",
            "Longitud": "-73.1137999999999",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20677",
            "CodCom": "308",
            "Comuna": "HUALAIHUE",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "CARRETERA AUSTRAL KM 107",
            "Email": "20677@escopec.cl",
            "Fono": "65 221-7215",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.9642869479172",
            "Longitud": "-72.4795436422881",
            "Servicios": {
                "CodSer": [
                    "9",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20696",
            "CodCom": "297",
            "Comuna": "COLLIPULLI",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "FREIRE, ESQ. AMUNATEGUI",
            "Email": "20696@escopec.cl",
            "Fono": "56-45-811024",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.9584198830462",
            "Longitud": "-72.4383175463332",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20703",
            "CodCom": "197",
            "Comuna": "LAJA",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. LOS RIOS Nº 745",
            "Email": "20703@escopec.cl",
            "Fono": "56-43-461215",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-37.2672779376034",
            "Longitud": "-72.7103348779577",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20716",
            "CodCom": "122",
            "Comuna": "COCHRANE",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "AV. PRAT, ESQ. O'HIGGINS",
            "Email": "20716@escopec.cl",
            "Fono": "71 268-4468",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-47.2557519806551",
            "Longitud": "-72.5777073031547",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20718",
            "CodCom": "130",
            "Comuna": "BULNES",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "PRAT, ESQ. ERRAZURIZ",
            "Email": "20718@escopec.cl",
            "Fono": "56-42-631040",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.7449614455640",
            "Longitud": "-72.2973630047587",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20722",
            "CodCom": "279",
            "Comuna": "PUREN",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "IMPERIAL Nº 895, ESQ. QUIROGA",
            "Email": "20722@escopec.cl",
            "Fono": "45 246-4330",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-38.0319742453844",
            "Longitud": "-73.0713278143912",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20756",
            "CodCom": "665",
            "Comuna": "CABO DE HORNOS",
            "CodReg": "12",
            "Region": "MAGALLANES",
            "Company": "COPEC",
            "Direccion": "CALLE NUEVA Nº 154",
            "Email": "20756@escopec.cl",
            "Fono": "56-61-621115",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-54.9333545661413",
            "Longitud": "-67.6042057332432",
            "Servicios": {
                "CodSer": [
                    "9"
                ]
            }
        },
        {
            "CodES": "20768",
            "CodCom": "77",
            "Comuna": "COYHAIQUE",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "CARRETERA AUSTRAL,  LA JUNTA",
            "Email": "20768@escopec.cl",
            "Fono": "67 231-4305",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-43.9701291307352",
            "Longitud": "-72.4000076285365",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20782",
            "CodCom": "34",
            "Comuna": "LAUTARO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 645,7",
            "Email": "20782@escopec.cl",
            "Fono": "56-45-531921",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.5494354446286",
            "Longitud": "-72.4575853292743",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33"
                ]
            }
        },
        {
            "CodES": "20794",
            "CodCom": "214",
            "Comuna": "NACIMIENTO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "LOS CARRERA, ESQ. LAUTARO",
            "Email": "20794@escopec.cl",
            "Fono": "56-43-511266",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.5102404270298",
            "Longitud": "-72.6559916745799",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20802",
            "CodCom": "190",
            "Comuna": "COELEMU",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "CASTELLON, PALAZUELOS",
            "Email": "20802@escopec.cl",
            "Fono": "56-42-511450",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.4869377718067",
            "Longitud": "-72.7055523752338",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20806",
            "CodCom": "72",
            "Comuna": "AISEN",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "CAMINO ACCESO A, PUERTO CHACABUCO",
            "Email": "20806@escopec.cl",
            "Fono": "67 235-1020",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-45.4616852630742",
            "Longitud": "-72.8125794871357",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20825",
            "CodCom": "72",
            "Comuna": "AISEN",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "EUSEBIO IBAR S/N, MANIHUALES",
            "Email": "20825@escopec.cl",
            "Fono": "56-67-431330",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-45.1729961360031",
            "Longitud": "-72.1473689525006",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "33",
                    "39"
                ]
            }
        },
        {
            "CodES": "20830",
            "CodCom": "77",
            "Comuna": "COYHAIQUE",
            "CodReg": "11",
            "Region": "AYSÉN",
            "Company": "COPEC",
            "Direccion": "AV. OGANA N° 1157",
            "Email": "20830@escopec.cl",
            "Fono": "56-67-2230868",
            "JefeEDS": "Mario Cortez",
            "Latitud": "-45.5827972880530",
            "Longitud": "-72.0748199493224",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "33",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20838",
            "CodCom": "16",
            "Comuna": "TEMUCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "AV. ALEMANIA Nº 83",
            "Email": "20838@escopec.cl",
            "Fono": "56-45-233542",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7362606188453",
            "Longitud": "-72.6028965888549",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "30",
                    "32",
                    "33",
                    "36",
                    "38",
                    "39"
                ]
            }
        },
        {
            "CodES": "20840",
            "CodCom": "222",
            "Comuna": "TOME",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. LATORRE Nº 750",
            "Email": "20840@escopec.cl",
            "Fono": "56-41-2655557",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6207468191893",
            "Longitud": "-72.9574045763909",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20843",
            "CodCom": "144",
            "Comuna": "TALCA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. CARLOS SCHORR Nº 83",
            "Email": "20843@escopec.cl",
            "Fono": "71 221-1568",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.4290579234373",
            "Longitud": "-71.6724306567237",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "24",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20852",
            "CodCom": "14",
            "Comuna": "TALCAHUANO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AUTOPISTA CONCEPCION, TALCAHUANO N°7001",
            "Email": "20852@escopec.cl",
            "Fono": "41 242-9234",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7790527850980",
            "Longitud": "-73.0772868466223",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "30",
                    "31",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "20854",
            "CodCom": "301",
            "Comuna": "LOS MUERMOS",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "M. MONTT, ESQ. RODRIGUEZ",
            "Email": "20854@escopec.cl",
            "Fono": "56-65-211423",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.3935123308233",
            "Longitud": "-73.4631739264987",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20859",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "RUTA 5 SUR KM 484, SALIDA NORTE",
            "Email": "20859@escopec.cl",
            "Fono": "56-43-318688",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-37.2832700119968",
            "Longitud": "-72.3545983760671",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20861",
            "CodCom": "233",
            "Comuna": "FRESIA",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "IRARRAZABAL Nº  003, ESQ. BDO. O'HIGGINS",
            "Email": "20861@escopec.cl",
            "Fono": "56-65-441420",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.1526606608944",
            "Longitud": "-73.4178151680243",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20862",
            "CodCom": "730",
            "Comuna": "PERQUENCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "MATTA, ESQ. LASTARRIA",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.4250852800591",
            "Longitud": "-72.3828035863506",
            "Servicios": {
                "CodSer": [
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20864",
            "CodCom": "100",
            "Comuna": "SAN PEDRO DE LA PAZ",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CAMINO CORONEL A CONCEP.Nº 7171, LOMAS COLORADAS",
            "Email": "20864@escopec.cl",
            "Fono": "56-41-2390094",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8863646029906",
            "Longitud": "-73.1396012516964",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20889",
            "CodCom": "178",
            "Comuna": "ANGOL",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "AV. O'HIGGINS, AV. ALEMANIA",
            "Email": "20889@escopec.cl",
            "Fono": "9840-2665",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.8087812908348",
            "Longitud": "-72.6887796261714",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20900",
            "CodCom": "178",
            "Comuna": "ANGOL",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "COLIPI N° 171",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.7963202096565",
            "Longitud": "-72.7082209289074",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20901",
            "CodCom": "95",
            "Comuna": "PENCO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "RUTA 150, KM 3,75",
            "Email": "20901@escopec.cl",
            "Fono": "56-41-2387116",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7798795421813",
            "Longitud": "-73.0186474660113",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20902",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "ANIBAL PINTO Nº 1835",
            "Email": "20902@escopec.cl",
            "Fono": "41 216-2225",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8118670089089",
            "Longitud": "-73.0595279358854",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "21",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20905",
            "CodCom": "242",
            "Comuna": "CHONCHI",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "SARGENTO CANDELARIA, ESQ. CAUPOLICAN",
            "Email": "20905@escopec.cl",
            "Fono": "56-65-671327",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-42.6218826628056",
            "Longitud": "-73.7799094258195",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20906",
            "CodCom": "106",
            "Comuna": "VALDIVIA",
            "CodReg": "14",
            "Region": "LOS RIOS",
            "Company": "COPEC",
            "Direccion": "ANIBAL PINTO N° 2007, ESQ. BULNES",
            "Email": "20906@escopec.cl",
            "Fono": "56-63-251798",
            "JefeEDS": "Pablo Cabezas",
            "Latitud": "-39.8278472457587",
            "Longitud": "-73.2402115005486",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20931",
            "CodCom": "98",
            "Comuna": "PUERTO MONTT",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "AV. PRESIDENTE IBAÑEZ 1118",
            "Email": "20931@escopec.cl",
            "Fono": "56-65-255006",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-41.4685322799714",
            "Longitud": "-72.9609154454484",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20933",
            "CodCom": "297",
            "Comuna": "COLLIPULLI",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "M. RODRIGUEZ, CAMINO MININCO-ESPERANZA",
            "Email": "20933@escopec.cl",
            "Fono": "56-45-813024",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.7892177202534",
            "Longitud": "-72.4744672420995",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20936",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "Maipu N°889",
            "Email": "20936@escopec.cl",
            "Fono": "41 221-1545",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.8230552698114",
            "Longitud": "-73.0490105914994",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "32",
                    "38"
                ]
            }
        },
        {
            "CodES": "20939",
            "CodCom": "101",
            "Comuna": "CORONEL",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "LOS CARRERAS, SERRANO",
            "Email": "20939@escopec.cl",
            "Fono": "56-41-2710947",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.0253703966952",
            "Longitud": "-73.1498337650375",
            "Servicios": {
                "CodSer": [
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20940",
            "CodCom": "14",
            "Comuna": "TALCAHUANO",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AMERICO VESPUCIO Nº 434, (AUTOPISTA)",
            "Email": "20940@escopec.cl",
            "Fono": "56-41-2790669",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7797339011858",
            "Longitud": "-73.0783312657301",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "20",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "20942",
            "CodCom": "222",
            "Comuna": "TOME",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CAMINO ACCESO PROYECTO INMOB. PINGUERAL",
            "Email": "20942@escopec.cl",
            "Fono": "41 295-0497",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.5391393639599",
            "Longitud": "-72.9300690663417",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20944",
            "CodCom": "260",
            "Comuna": "TRAIGUEN",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "CORONEL URRUTIA 581, ESQ. RIVEROS",
            "Email": "20944@escopec.cl",
            "Fono": "56-45-862098",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.2511436913342",
            "Longitud": "-72.6696436783256",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20945",
            "CodCom": "16",
            "Comuna": "TEMUCO",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "AV. SAN MARTIN, ESQ. URUGUAY",
            "Email": "20945@escopec.cl",
            "Fono": "45 221-2514",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.7391589641068",
            "Longitud": "-72.6146972188155",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "20",
                    "24",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20946",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. ERCILLA, ESQ. GALVARINO",
            "Email": "20946@escopec.cl",
            "Fono": "56-43-326422",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4639899092718",
            "Longitud": "-72.3548018240834",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20947",
            "CodCom": "323",
            "Comuna": "VILLA ALEGRE",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. ABATE MOLINA Nº 700",
            "Email": "20947@escopec.cl",
            "Fono": "73 238-2020",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.6780501714046",
            "Longitud": "-71.7409464029338",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20948",
            "CodCom": "221",
            "Comuna": "VILCUN",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "ARTURO PRAT, BLANCO ENCALADA",
            "Email": "20948@escopec.cl",
            "Fono": "45 256-2053",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.6698192418743",
            "Longitud": "-72.2261634677702",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20950",
            "CodCom": "160",
            "Comuna": "CURANILAHUE",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "AV. O'HIGGINS S/N",
            "Email": "20950@escopec.cl",
            "Fono": "56-41-2692893",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4686018799618",
            "Longitud": "-73.3568109502139",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "20951",
            "CodCom": "150",
            "Comuna": "RIO NEGRO",
            "CodReg": "10",
            "Region": "LOS LAGOS",
            "Company": "COPEC",
            "Direccion": "AV. BUSCHMANN Nº 16, BALMACEDA",
            "Email": "20951@escopec.cl",
            "Fono": "64 236-1232",
            "JefeEDS": "Jorge Schiesewitz",
            "Latitud": "-40.7925815874325",
            "Longitud": "-73.2146757723794",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20952",
            "CodCom": "29",
            "Comuna": "CHILLAN",
            "CodReg": "16",
            "Region": "ÑUBLE",
            "Company": "COPEC",
            "Direccion": "RUTA 5 KM 409,4",
            "Email": "20952@escopec.cl",
            "Fono": "56-42-269035",
            "JefeEDS": "Joaquin Traviesa",
            "Latitud": "-36.6334646682868",
            "Longitud": "-72.1940882233116",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "35",
                    "36"
                ]
            }
        },
        {
            "CodES": "20953",
            "CodCom": "20",
            "Comuna": "LOS ANGELES",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "ALMAGRO Nº 1190",
            "Email": "20953@escopec.cl",
            "Fono": "43 236-9467",
            "JefeEDS": "Manuel Canessa",
            "Latitud": "-37.4588286608774",
            "Longitud": "-72.3480560410519",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "20954",
            "CodCom": "290",
            "Comuna": "MAULE",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA SUR KM 265",
            "Email": "20954@escopec.cl",
            "Fono": "56-71-970149",
            "JefeEDS": "Mauricio Soto Marin",
            "Latitud": "-35.5310897335418",
            "Longitud": "-71.6882420202368",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "20955",
            "CodCom": "108",
            "Comuna": "VICTORIA",
            "CodReg": "9",
            "Region": "LA ARAUCANÍA",
            "Company": "COPEC",
            "Direccion": "PRAT N° 1090",
            "Email": "20955@escopec.cl",
            "Fono": "56-45-846489",
            "JefeEDS": "Christian Muñoz",
            "Latitud": "-38.2333278529116",
            "Longitud": "-72.3369118230717",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "20960",
            "CodCom": "15",
            "Comuna": "CONCEPCION",
            "CodReg": "8",
            "Region": "BIOBIO",
            "Company": "COPEC",
            "Direccion": "CALLE B-20 N° 420, LOMAS DE SAN SEBASTIAN.",
            "Email": "20960@escopec.cl",
            "Fono": "56-41-2484030",
            "JefeEDS": "Francisca Sola",
            "Latitud": "-36.7819601967717",
            "Longitud": "-73.0525707862525",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "39"
                ]
            }
        },
        {
            "CodES": "40003",
            "CodCom": "402",
            "Comuna": "CALERA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "LOS CARRERA Nº 1513",
            "Email": "40003@escopec.cl",
            "Fono": "56-33-221820",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.7917267595572",
            "Longitud": "-71.2027898824418",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40004",
            "CodCom": "168",
            "Comuna": "LOS ANDES",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "PASCUAL BABURIZZA, RENE SCHNEIDER",
            "Email": "40004@escopec.cl",
            "Fono": "56-32-2428940",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.8418946080802",
            "Longitud": "-70.5935549980232",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40005",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. LIBERTAD Nº 501",
            "Email": "40005@escopec.cl",
            "Fono": "32 268-4323",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0170171965906",
            "Longitud": "-71.5508065431292",
            "Servicios": {
                "CodSer": [
                    "4",
                    "7",
                    "8",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "23",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40006",
            "CodCom": "113",
            "Comuna": "SAN FELIPE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "RUTA 60-C 4, SECTOR 3 ESQUINAS",
            "Email": "40006@escopec.cl",
            "Fono": "34 2531421",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.7733119801932",
            "Longitud": "-70.7103435480447",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40007",
            "CodCom": "110",
            "Comuna": "MELIPILLA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VICUÑA MACKENNA Nº 420",
            "Email": "40007@escopec.cl",
            "Fono": "56-2-8323501",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.6800158587637",
            "Longitud": "-71.2126575871512",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "26",
                    "30",
                    "32",
                    "38"
                ]
            }
        },
        {
            "CodES": "40008",
            "CodCom": "351",
            "Comuna": "PUCHUNCAVI",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "RUTA F-30-E",
            "Email": "40008@escopec.cl",
            "Fono": "32 279-4664",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.7413227088361",
            "Longitud": "-71.4686247020535",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40010",
            "CodCom": "422",
            "Comuna": "MARIA PINTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LOS RULOS KM 8",
            "Email": "40010@escopec.cl",
            "Fono": "56-2-8361519",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.4660229649336",
            "Longitud": "-71.0842135353220",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "40012",
            "CodCom": "92",
            "Comuna": "SAN ANTONIO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "LAURO BARROS Nº 60",
            "Email": "40012@escopec.cl",
            "Fono": "56-35213314",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.5786978999741",
            "Longitud": "-71.6084515218004",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40013",
            "CodCom": "92",
            "Comuna": "SAN ANTONIO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "LOS AROMOS Nº 346",
            "Email": "40013@escopec.cl",
            "Fono": "56-35-282887",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.6098963483277",
            "Longitud": "-71.6111770407470",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40014",
            "CodCom": "110",
            "Comuna": "MELIPILLA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LAS CASAS CRUCE LA VIRGEN, LOTE 1",
            "Email": "40014@escopec.cl",
            "Fono": "2 28319628",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.5607463224208",
            "Longitud": "-71.2099436090435",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "40018",
            "CodCom": "110",
            "Comuna": "MELIPILLA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VICUÑA MACKENA Nº 1041",
            "Email": "40018@escopec.cl",
            "Fono": "56-2-8323876",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.6799112179964",
            "Longitud": "-71.2209545915074",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40021",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. VALPARAISO Nº 1160",
            "Email": "40021@escopec.cl",
            "Fono": "32 218-6921",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0261531177184",
            "Longitud": "-71.5458506602806",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40022",
            "CodCom": "335",
            "Comuna": "PETORCA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "POBLACION LA ÑIPA S/N",
            "Email": "40022@escopec.cl",
            "Fono": "33 278-1458",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.2808460011210",
            "Longitud": "-70.9734865860020",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40025",
            "CodCom": "176",
            "Comuna": "QUINTERO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "NORMANDIE Nº 2500",
            "Email": "40025@escopec.cl",
            "Fono": "9-8402508",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.7915699700353",
            "Longitud": "-71.5265954139652",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40031",
            "CodCom": "180",
            "Comuna": "LA LIGUA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "PORTALES Nº 1131",
            "Email": "40031@escopec.cl",
            "Fono": "56-33-712089",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.4522802131745",
            "Longitud": "-71.2390109261000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40033",
            "CodCom": "117",
            "Comuna": "VILLA ALEMANA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. VALPARAISO Nº 1999",
            "Email": "40033@escopec.cl",
            "Fono": "56-32-2941544",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0458242596865",
            "Longitud": "-71.3904104971386",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40034",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "AV. COLO COLO Nº 4486",
            "Email": "40034@escopec.cl",
            "Fono": "33 227-2286",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9148032393641",
            "Longitud": "-71.2109262447253",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40035",
            "CodCom": "274",
            "Comuna": "HIJUELAS",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA NORTE KM. 10",
            "Email": "40035@escopec.cl",
            "Fono": "33 227-2758",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.8276163097741",
            "Longitud": "-71.1301749207862",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40036",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "QUILLOTA Nº 724",
            "Email": "40036@escopec.cl",
            "Fono": "33 317-9533",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0158785254898",
            "Longitud": "-71.5446437664225",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "13",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40037",
            "CodCom": "66",
            "Comuna": "COPIAPO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "AV. LOS LOROS Nº 1310",
            "Email": "40037@escopec.cl",
            "Fono": "33 223-5376",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.3635533971479",
            "Longitud": "-70.3173373128524",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40039",
            "CodCom": "201",
            "Comuna": "ALGARROBO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "STA. TERESITA, PEÑABLANCA S/N",
            "Email": "40039@escopec.cl",
            "Fono": "56-35-484072",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.3670560837322",
            "Longitud": "-71.6740051316798",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40040",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AGUA SANTA  Nº 820",
            "Email": "40040@escopec.cl",
            "Fono": "56-32-2775324",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0324128687805",
            "Longitud": "-71.5655164369788",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40043",
            "CodCom": "111",
            "Comuna": "CURACAVI",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "RUTA 68 KM 55",
            "Email": "40043@escopec.cl",
            "Fono": "33 2835-1391",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.4094784018879",
            "Longitud": "-71.1747306916551",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40045",
            "CodCom": "117",
            "Comuna": "VILLA ALEMANA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. VALPARAISO Nº 1215",
            "Email": "40045@escopec.cl",
            "Fono": "56-32-2950311",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0445977230844",
            "Longitud": "-71.3810604525908",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40046",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. EDMUNDO ELUCHANS N° 3100, SECTOR BOSQUES DE MONTEMAR",
            "Email": "40046@escopec.cl",
            "Fono": "33 285-7812",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.9497342926785",
            "Longitud": "-71.5441515545446",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40048",
            "CodCom": "88",
            "Comuna": "SALAMANCA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "CARRETERA D-81, LOTE E, SECTOR SALI",
            "Email": "40048@escopec.cl",
            "Fono": "33 255-2897",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-31.7751167235184",
            "Longitud": "-70.9800207095487",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40049",
            "CodCom": "113",
            "Comuna": "SAN FELIPE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. BERNARDO O'HIGGINS S/N",
            "Email": "40049@escopec.cl",
            "Fono": "56-34-514246",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.7542527558530",
            "Longitud": "-70.7286396960603",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "33",
                    "39"
                ]
            }
        },
        {
            "CodES": "40050",
            "CodCom": "113",
            "Comuna": "SAN FELIPE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "COMBATE LAS COIMAS Nº 1412",
            "Email": "40050@escopec.cl",
            "Fono": "56-34-512550",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.7528078101474",
            "Longitud": "-70.7247206914325",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40051",
            "CodCom": "113",
            "Comuna": "SAN FELIPE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "JOSE MANSO DE VELASCO, ESQ. LAS HERAS 0",
            "Email": "40051@escopec.cl",
            "Fono": "56-9-62722647",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.7566330000000",
            "Longitud": "-70.7346080000000",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40052",
            "CodCom": "168",
            "Comuna": "LOS ANDES",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "ESMERALDA ESQUINA SANTA TERESA",
            "Email": "40052@escopec.cl",
            "Fono": "56-32-2404632...",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.8318129165052",
            "Longitud": "-70.6018743697524",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40053",
            "CodCom": "115",
            "Comuna": "CABILDO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "HUMERES N°1302",
            "Email": "40053@escopec.cl",
            "Fono": "33 276-1501",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.4305312110917",
            "Longitud": "-71.0747020229000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40054",
            "CodCom": "275",
            "Comuna": "LOS VILOS",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE KM 195, PICHIDANGUI",
            "Email": "40054@escopec.cl",
            "Fono": "33 254-1925",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.1529638865920",
            "Longitud": "-71.5113860767336",
            "Servicios": {
                "CodSer": [
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40055",
            "CodCom": "275",
            "Comuna": "LOS VILOS",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE KM. 204 ( UBIC. PONIENTE )",
            "Email": "40055@escopec.cl",
            "Fono": "56-6-6183330",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.0784810055810",
            "Longitud": "-71.5177379439694",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40056",
            "CodCom": "88",
            "Comuna": "SALAMANCA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "O'HIGGINS Nº 490",
            "Email": "40056@escopec.cl",
            "Fono": "33 255-1250",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-31.7801941953019",
            "Longitud": "-70.9638176937404",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "40057",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "1 NORTE Nº 2399",
            "Email": "40057@escopec.cl",
            "Fono": "56-32-2670773",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0323001854362",
            "Longitud": "-71.5297411093663",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40058",
            "CodCom": "119",
            "Comuna": "LIMACHE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. PALMIRA ROMANO NORTE Nº 1",
            "Email": "40058@escopec.cl",
            "Fono": "56-33-411333",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.9817586338604",
            "Longitud": "-71.2759708473364",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40059",
            "CodCom": "351",
            "Comuna": "PUCHUNCAVI",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "RUTA F30E S/N LOTE CC4, MARBELLA",
            "Email": "40059@escopec.cl",
            "Fono": "9-8374095",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.6709366797797",
            "Longitud": "-71.4262650617660",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40060",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AVENIDA EDUARDO FREI N°4025",
            "Email": "40060@escopec.cl",
            "Fono": "33 264-2639",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0234894488638",
            "Longitud": "-71.5085215907503",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40061",
            "CodCom": "112",
            "Comuna": "QUILPUE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "MARGA MARGA 1628",
            "Email": "40061@escopec.cl",
            "Fono": "33 282-8804",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0555517189211",
            "Longitud": "-71.4265567266617",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40062",
            "CodCom": "258",
            "Comuna": "SANTO DOMINGO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "TENIENTE CRUZ MARTINEZ 70",
            "Email": "40062@escopec.cl",
            "Fono": "33 244-3139",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.6460253082045",
            "Longitud": "-71.6241181942375",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40063",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "JORGE MONTT Nº 2300, RECTA LAS SALINAS",
            "Email": "40063@escopec.cl",
            "Fono": "56-32-2971580",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.9998162938464",
            "Longitud": "-71.5482493939045",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "20",
                    "24",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38",
                    "39"
                ]
            }
        },
        {
            "CodES": "40064",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. BRASIL, ESQ. AV. ERRAZURIZ S/N",
            "Email": "40064@escopec.cl",
            "Fono": "56-32-2258223",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0445169809255",
            "Longitud": "-71.6084978369699",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40066",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "SANTOS OSSA Nº 960",
            "Email": "40066@escopec.cl",
            "Fono": "33 237-8265",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0530694891180",
            "Longitud": "-71.6012836562264",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40071",
            "CodCom": "275",
            "Comuna": "LOS VILOS",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA NORTE KM 225",
            "Email": "40071@escopec.cl",
            "Fono": "56-53-543029",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-31.9095338121680",
            "Longitud": "-71.4920269574824",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40078",
            "CodCom": "168",
            "Comuna": "LOS ANDES",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "CAMINO  INTERNACIONAL S/N, RIO BLANCO",
            "Email": "40078@escopec.cl",
            "Fono": "56-32-2428244",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.9076672092938",
            "Longitud": "-70.2938054351941",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "40088",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. COLON Nº 2330",
            "Email": "40088@escopec.cl",
            "Fono": "56-32-2755926",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0499000000000",
            "Longitud": "-71.6126799999999",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40090",
            "CodCom": "162",
            "Comuna": "QUILLOTA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "FREIRE, ECHEVERRIA",
            "Email": "40090@escopec.cl",
            "Fono": "56-33-310104",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.8831904047427",
            "Longitud": "-71.2454203303235",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "31",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40091",
            "CodCom": "112",
            "Comuna": "QUILPUE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "FREIRE, GOMEZ CARREÑO S/N",
            "Email": "40091@escopec.cl",
            "Fono": "56-32-2941602",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0462825462146",
            "Longitud": "-71.4055977739338",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40095",
            "CodCom": "402",
            "Comuna": "CALERA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "J. J. PEREZ, CAUPOLICAN",
            "Email": "40095@escopec.cl",
            "Fono": "56-33-223431",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.7859905216335",
            "Longitud": "-71.1897388322726",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40096",
            "CodCom": "65",
            "Comuna": "CASABLANCA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "CONSTITUCION Nº 550",
            "Email": "40096@escopec.cl",
            "Fono": "33 274-4760",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.3158168519022",
            "Longitud": "-71.4084486574159",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "40098",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "PLACILLA KM 105, RUTA 68, PEÑUELAS",
            "Email": "40098@escopec.cl",
            "Fono": "56-32-2291777",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.1281656112054",
            "Longitud": "-71.5627307701092",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "24",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40099",
            "CodCom": "116",
            "Comuna": "VALLENAR",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE, CRUCE CAMINO HUASCO",
            "Email": "40099@escopec.cl",
            "Fono": "33 261-1183",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-28.5728817999944",
            "Longitud": "-70.7861743607342",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40100",
            "CodCom": "116",
            "Comuna": "VALLENAR",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "BRASIL Nº 1001",
            "Email": "40100@escopec.cl",
            "Fono": "56-54-613888",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-28.5801108673160",
            "Longitud": "-70.7615827109692",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "19",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40101",
            "CodCom": "67",
            "Comuna": "HUASCO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "IGNACIO DE LA CARRERA N°167",
            "Email": "40101@escopec.cl",
            "Fono": "33 253-1004",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-28.4651478540696",
            "Longitud": "-71.2190546576980",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40103",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE, ESQ. FCO. AGUIRRE",
            "Email": "40103@escopec.cl",
            "Fono": "33 225-0425",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9064538918590",
            "Longitud": "-71.2571904923403",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "13",
                    "14",
                    "17",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40104",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "PLAZA COPEC SUR, RUTA Nº 5",
            "Email": "40104@escopec.cl",
            "Fono": "33 221-1522",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9076947345176",
            "Longitud": "-71.2577539569334",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "40105",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "BALMACEDA Nº 1839",
            "Email": "40105@escopec.cl",
            "Fono": "33 221-7004",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9176914262807",
            "Longitud": "-71.2538109769505",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "19",
                    "20",
                    "23",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40107",
            "CodCom": "74",
            "Comuna": "COQUIMBO",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "BAQUEDANO, MERINO JARPA",
            "Email": "40107@escopec.cl",
            "Fono": "33 231-2150",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9609021312413",
            "Longitud": "-71.3348406099888",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40110",
            "CodCom": "210",
            "Comuna": "ANDACOLLO",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "EL SALITRE N° 20",
            "Email": "40110@escopec.cl",
            "Fono": "56-51-431640",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-30.2262705985349",
            "Longitud": "-71.0844083173279",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40111",
            "CodCom": "140",
            "Comuna": "OVALLE",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "ARIZTIA, ESQ. ANTONIO TIRADO",
            "Email": "40111@escopec.cl",
            "Fono": "33 263-9024",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-30.6029041650661",
            "Longitud": "-71.1966018900390",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40112",
            "CodCom": "140",
            "Comuna": "OVALLE",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "BENAVENTE Nº 836",
            "Email": "40112@escopec.cl",
            "Fono": "56-53-629383",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-30.5978957934423",
            "Longitud": "-71.1874972155838",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "15",
                    "19",
                    "30"
                ]
            }
        },
        {
            "CodES": "40113",
            "CodCom": "140",
            "Comuna": "OVALLE",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE KM 370",
            "Email": "40113@escopec.cl",
            "Fono": "33 268-1024",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-30.7188512595565",
            "Longitud": "-71.4954284368563",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40115",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "COLO COLO S/N",
            "Email": "40115@escopec.cl",
            "Fono": "56512560623",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9114600546461",
            "Longitud": "-71.2221803885670",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40117",
            "CodCom": "92",
            "Comuna": "SAN ANTONIO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "BARROS LUCO S/N",
            "Email": "40117@escopec.cl",
            "Fono": "33 221-6546",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.5873173607823",
            "Longitud": "-71.6125048069157",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "15",
                    "17",
                    "30"
                ]
            }
        },
        {
            "CodES": "40120",
            "CodCom": "96",
            "Comuna": "CARTAGENA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. EL PERAL S/N, SAN SEBASTIAN",
            "Email": "40120@escopec.cl",
            "Fono": "56-35-459611",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.5325047672453",
            "Longitud": "-71.5947612735807",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40126",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. BRASIL Nº 1804",
            "Email": "40126@escopec.cl",
            "Fono": "56-32-2469722",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0451610244208",
            "Longitud": "-71.6177742415726",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40128",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "LIMACHE, LUSITANIA Nº 2321",
            "Email": "40128@escopec.cl",
            "Fono": "56-32-2670020",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0335296255500",
            "Longitud": "-71.5320537922635",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40130",
            "CodCom": "112",
            "Comuna": "QUILPUE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "LOS CARRERA Nº 1091",
            "Email": "40130@escopec.cl",
            "Fono": "33 292-6804",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0431227322371",
            "Longitud": "-71.4650190924935",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40131",
            "CodCom": "226",
            "Comuna": "SANTA MARIA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "IRARRAZAVAL, ESQ. OHIGGINS",
            "Email": "40131@escopec.cl",
            "Fono": "56-34-2581048",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.7477674936334",
            "Longitud": "-70.6576579186166",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40132",
            "CodCom": "166",
            "Comuna": "CONCON",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. BORGOÑO N° 25175, CONCON",
            "Email": "40132@escopec.cl",
            "Fono": "32-2815680",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.9202861138809",
            "Longitud": "-71.5088105427194",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40134",
            "CodCom": "257",
            "Comuna": "EL QUISCO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. ISIDORO DUBOURNAIS Nº 733",
            "Email": "40134@escopec.cl",
            "Fono": "33 247-4475",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.4028457913210",
            "Longitud": "-71.6940156228577",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40136",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. PLAYA ANCHA Nº 697",
            "Email": "40136@escopec.cl",
            "Fono": "33 317-1756",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.0264526815263",
            "Longitud": "-71.6397259062012",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "21",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40140",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AVENIDA ALESSANDRI N° 3401",
            "Email": "40140@escopec.cl",
            "Fono": "56-32-2861232",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0016102189353",
            "Longitud": "-71.5174365446472",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "19",
                    "21",
                    "24",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40141",
            "CodCom": "92",
            "Comuna": "SAN ANTONIO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. BARROS LUCO, ESQ. 10 SUR",
            "Email": "40141@escopec.cl",
            "Fono": "56-35-282479",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.6064034517190",
            "Longitud": "-71.6136918948788",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40142",
            "CodCom": "326",
            "Comuna": "PAPUDO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "IRARRAZAVAL Nº 435                      ",
            "Email": "40142@escopec.cl",
            "Fono": "",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.5059955825985",
            "Longitud": "-71.4445169270039",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40143",
            "CodCom": "167",
            "Comuna": "COMBARBALA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "JUAN IGNACIO FLORES N° 576",
            "Email": "40143@escopec.cl",
            "Fono": "56-53-741052",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-31.1753938755692",
            "Longitud": "-71.0062026613082",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40144",
            "CodCom": "74",
            "Comuna": "COQUIMBO",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "FUNDICION NORTE, TOTORAL S/N,TONGOY",
            "Email": "40144@escopec.cl",
            "Fono": "33 239-2076",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-30.2569139275952",
            "Longitud": "-71.4930786569931",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40145",
            "CodCom": "636",
            "Comuna": "VICUÑA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "LAS DELICIAS, SARGENTO ALDEA",
            "Email": "40145@escopec.cl",
            "Fono": "56-51-2412152",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-30.0364095455047",
            "Longitud": "-70.7171924756008",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "33"
                ]
            }
        },
        {
            "CodES": "40146",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "BALMACEDA, PARADERO 10 1/2, LA PAMPA",
            "Email": "40146@escopec.cl",
            "Fono": "56-51-248027",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9438908420384",
            "Longitud": "-71.2647852515958",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "21",
                    "24",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40147",
            "CodCom": "110",
            "Comuna": "MELIPILLA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VICUÑA MACKENNA Nº 820",
            "Email": "40147@escopec.cl",
            "Fono": "33 2831-7814",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.6796297923813",
            "Longitud": "-71.2175851189386",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "27",
                    "30",
                    "31",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "40148",
            "CodCom": "110",
            "Comuna": "MELIPILLA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "HUILCO BAJO S/N",
            "Email": "40148@escopec.cl",
            "Fono": "56-2-8311859",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.7097746917396",
            "Longitud": "-71.2165002411094",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40151",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "CHACABUCO, GENERAL CRUZ",
            "Email": "40151@escopec.cl",
            "Fono": "56-32-2594295",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0462705494855",
            "Longitud": "-71.6133215771322",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40153",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. ALESSANDRI, 21 NORTE",
            "Email": "40153@escopec.cl",
            "Fono": "56-32-2973985",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0031399815170",
            "Longitud": "-71.5443566734484",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "26",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40158",
            "CodCom": "145",
            "Comuna": "RINCONADA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "SAN MARTIN Nº 2510, RINCONADA",
            "Email": "40158@escopec.cl",
            "Fono": "33 248-9990",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.8519262325396",
            "Longitud": "-70.6845913983678",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40161",
            "CodCom": "243",
            "Comuna": "CATEMU",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "EL ARRAYAN S/N",
            "Email": "40161@escopec.cl",
            "Fono": "33 263-2180",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.7894360294185",
            "Longitud": "-70.9587693940303",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40162",
            "CodCom": "74",
            "Comuna": "COQUIMBO",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "TALCA ESQUINA ALESSANDRI",
            "Email": "40162@escopec.cl",
            "Fono": "33 222-8695",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9609886239024",
            "Longitud": "-71.2635966035213",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40163",
            "CodCom": "275",
            "Comuna": "LOS VILOS",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE KM 227",
            "Email": "40163@escopec.cl",
            "Fono": "56-53-2542169",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-31.8885608916665",
            "Longitud": "-71.4922218359267",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40166",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "Camino Internacional N°4690",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.9648750000000",
            "Longitud": "-71.5050470000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40170",
            "CodCom": "162",
            "Comuna": "QUILLOTA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "Av. Rafael Ariztia N°705",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.8773417000000",
            "Longitud": "-71.2331638888889",
            "Servicios": {
                "CodSer": [
                    "2",
                    "12",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40180",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "Av. Matta N° 2114. Cerro Placeres",
            "Email": "",
            "Fono": "",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.0477778000000",
            "Longitud": "-71.5806916666667",
            "Servicios": {
                "CodSer": [
                    "2",
                    "12",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40201",
            "CodCom": "723",
            "Comuna": "TILTIL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "Emilio Valle N°05 / Arturo Prat",
            "Email": "40201@escopec.cl",
            "Fono": "",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0824440000000",
            "Longitud": "-70.9280160000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40368",
            "CodCom": "239",
            "Comuna": "NOGALES",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "JUAN RUSQUE Nº 312",
            "Email": "40368@escopec.cl",
            "Fono": "33 226-3037",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.7329855906666",
            "Longitud": "-71.2048916018607",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40369",
            "CodCom": "119",
            "Comuna": "LIMACHE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. PALMIRA ROMANO SUR Nº 500",
            "Email": "40369@escopec.cl",
            "Fono": "56-33-412040",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0119303882767",
            "Longitud": "-71.2702547490277",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40370",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "G. GONZALEZ VIDELA Nº 1965, LOS MOLLES",
            "Email": "40370@escopec.cl",
            "Fono": "56-51-2228830",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9204726315437",
            "Longitud": "-71.2498797172159",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "39"
                ]
            }
        },
        {
            "CodES": "40371",
            "CodCom": "168",
            "Comuna": "LOS ANDES",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. ARGENTINA , LAS JUNTAS",
            "Email": "40371@escopec.cl",
            "Fono": "33 242-1346",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.8273675385453",
            "Longitud": "-70.6072189719190",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40384",
            "CodCom": "181",
            "Comuna": "LA CRUZ",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "21 DE MAYO  Nº 5490",
            "Email": "40384@escopec.cl",
            "Fono": "56-33-313945",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.8255572497044",
            "Longitud": "-71.2265802132588",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40390",
            "CodCom": "275",
            "Comuna": "LOS VILOS",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA NORTE KM 204 ( UBIC. ORIENTE )",
            "Email": "40390@escopec.cl",
            "Fono": "33 232-0636",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.0797835059345",
            "Longitud": "-71.5152388045035",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "35",
                    "36"
                ]
            }
        },
        {
            "CodES": "40403",
            "CodCom": "74",
            "Comuna": "COQUIMBO",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "AVDA. LA CANTERA N°1935",
            "Email": "40403@escopec.cl",
            "Fono": "33 227-8390",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9737140514896",
            "Longitud": "-71.3022454573440",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "40429",
            "CodCom": "286",
            "Comuna": "CALLE LARGA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "CALLE LARGA Nº 2823",
            "Email": "40429@escopec.cl",
            "Fono": "56-34-461752",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.8528939618793",
            "Longitud": "-70.6241774410551",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40430",
            "CodCom": "74",
            "Comuna": "COQUIMBO",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "AV. ALESSANDRI Nº 1209",
            "Email": "40430@escopec.cl",
            "Fono": "56-51-323483",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9707791760203",
            "Longitud": "-71.3372716203262",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40435",
            "CodCom": "140",
            "Comuna": "OVALLE",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "SOCOS ESQ. VICTORIA",
            "Email": "40435@escopec.cl",
            "Fono": "33 262-5507",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-30.6017465210465",
            "Longitud": "-71.2048558858468",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40468",
            "CodCom": "75",
            "Comuna": "VALPARAISO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. FEDERICO STA. MARIA Nº 2044",
            "Email": "40468@escopec.cl",
            "Fono": "56-32-2283563",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.0458324301244",
            "Longitud": "-71.6505773191946",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40471",
            "CodCom": "115",
            "Comuna": "CABILDO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. HUMERES 2498, ESQ. CALLE 2 (FERROCARRIL A IQUIQUE)",
            "Email": "40471@escopec.cl",
            "Fono": "56-33-761044",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.4380260995259",
            "Longitud": "-71.0849084734224",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40477",
            "CodCom": "162",
            "Comuna": "QUILLOTA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "BLANCO Nº 661, YUNGAY",
            "Email": "40477@escopec.cl",
            "Fono": "9-8729652",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.8849597067137",
            "Longitud": "-71.2440754187183",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "21",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40481",
            "CodCom": "116",
            "Comuna": "VALLENAR",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "JUAN VERDAGER S/N",
            "Email": "40481@escopec.cl",
            "Fono": "56-54-614727",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-28.5758380431334",
            "Longitud": "-70.7558052059156",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40492",
            "CodCom": "125",
            "Comuna": "ILLAPEL",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "TARCICIO VALDERRAMA, EL MIRADOR S/N",
            "Email": "40492@escopec.cl",
            "Fono": "56-53-522847",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-31.6401923288307",
            "Longitud": "-71.1771828011516",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40493",
            "CodCom": "212",
            "Comuna": "ZAPALLAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "JANUARIO OVALLE Nº 128",
            "Email": "40493@escopec.cl",
            "Fono": "33 274-2381",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.5558960679314",
            "Longitud": "-71.4553311112527",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40500",
            "CodCom": "97",
            "Comuna": "CALDERA",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA, C. CALDERILLA",
            "Email": "40500@escopec.cl",
            "Fono": "944306599",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.0903762633547",
            "Longitud": "-70.8011925044956",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33"
                ]
            }
        },
        {
            "CodES": "40501",
            "CodCom": "97",
            "Comuna": "CALDERA",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "MONTT 207",
            "Email": "40501@escopec.cl",
            "Fono": "56-2-522315335",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.0671688133165",
            "Longitud": "-70.8241052068654",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40502",
            "CodCom": "66",
            "Comuna": "COPIAPO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA N° 255, POB.ROSARIO",
            "Email": "40502@escopec.cl",
            "Fono": "52 223-1610",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.3732049881600",
            "Longitud": "-70.3369497730840",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "33",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "40503",
            "CodCom": "66",
            "Comuna": "COPIAPO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "AV. HENRIQUEZ Nº 504",
            "Email": "40503@escopec.cl",
            "Fono": "9789-3877",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.3706477689068",
            "Longitud": "-70.3253010990522",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "40504",
            "CodCom": "66",
            "Comuna": "COPIAPO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "AV. COPAYAPU Nº 2024",
            "Email": "40504@escopec.cl",
            "Fono": "33 222-0355",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.3792267948481",
            "Longitud": "-70.3192690780780",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40506",
            "CodCom": "136",
            "Comuna": "TIERRA AMARILLA",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "AV. MIGUEL LEMEUR Nº 427",
            "Email": "40506@escopec.cl",
            "Fono": "33 232-0602",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.4784286477056",
            "Longitud": "-70.2663286424173",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40508",
            "CodCom": "66",
            "Comuna": "COPIAPO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "CARRETERA  PANAMERICANA  NORTE KM.811",
            "Email": "40508@escopec.cl",
            "Fono": "56-52-2217238",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.3411868500785",
            "Longitud": "-70.3664110425225",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40511",
            "CodCom": "66",
            "Comuna": "COPIAPO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "Copayapu N°4656",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.4008748714288",
            "Longitud": "-70.3008941473019",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "40512",
            "CodCom": "66",
            "Comuna": "COPIAPO",
            "CodReg": "3",
            "Region": "ATACAMA",
            "Company": "COPEC",
            "Direccion": "RUTA 5 NORTE KM 838, COSTADO NORTES N° 2 S/N",
            "Email": "40512@escopec.cl",
            "Fono": "56-965197063",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-27.3392732207572",
            "Longitud": "-70.6065022945404",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "29",
                    "30",
                    "33",
                    "34",
                    "35",
                    "36"
                ]
            }
        },
        {
            "CodES": "40513",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "EL ISLON N° 2561",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.8874902696453",
            "Longitud": "-71.2459275126457",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40514",
            "CodCom": "667",
            "Comuna": "SAN ESTEBAN",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. ALESSANDRI N° 1811",
            "Email": "40514@escopec.cl",
            "Fono": "56-9-73774493",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.8132267576138",
            "Longitud": "-70.5859855136456",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40515",
            "CodCom": "92",
            "Comuna": "SAN ANTONIO",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. EL PARQUE ACERA SUR N° 2545",
            "Email": "",
            "Fono": "56-953701978\r\n",
            "JefeEDS": "José Vargas",
            "Latitud": "-33.6032664570000",
            "Longitud": "-71.6204019200000",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "29",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40516",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "Av. 4 Esquinas N°1550",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9441427986694",
            "Longitud": "-71.2430010821511",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40525",
            "CodCom": "74",
            "Comuna": "COQUIMBO",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA NORTE KM 455",
            "Email": "40525@escopec.cl",
            "Fono": "56-51-261270",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-30.0123351604631",
            "Longitud": "-71.3787586329885",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40526",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "CAMINO INTERNACIONAL N° 5001",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-32.9638810058507",
            "Longitud": "-71.5073815036650",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40527",
            "CodCom": "112",
            "Comuna": "QUILPUE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "Avenida Los Carrera N° 01050",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-33.0426920000000",
            "Longitud": "-71.4642070000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "11",
                    "12",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "40551",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "NICARAGUA Nº 1480",
            "Email": "40551@escopec.cl",
            "Fono": "33 225-3923-9...",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.8885272903351",
            "Longitud": "-71.2426193434297",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "28",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40560",
            "CodCom": "152",
            "Comuna": "LLAILLAY",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA NORTE KM 90",
            "Email": "40560@escopec.cl",
            "Fono": "56-34-611995",
            "JefeEDS": "Jordano Puchi",
            "Latitud": "-32.8423476947952",
            "Longitud": "-71.0030921774993",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40601",
            "CodCom": "140",
            "Comuna": "OVALLE",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "CAMINO TUQUI S/N",
            "Email": "40601@escopec.cl",
            "Fono": "33 262-7431",
            "JefeEDS": "Aitor Lombera",
            "Latitud": "-30.5830760176076",
            "Longitud": "-71.1899688470136",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "25",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40602",
            "CodCom": "133",
            "Comuna": "LA SERENA",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE KM 480, SECTOR VEGAS NOR",
            "Email": "40602@escopec.cl",
            "Fono": "33 221-7940-8...",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.8382613344904",
            "Longitud": "-71.2595803407818",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "40603",
            "CodCom": "119",
            "Comuna": "LIMACHE",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "CAMINO INTERNACIONAL S/N",
            "Email": "40603@escopec.cl",
            "Fono": "56-33-450815",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.9292535988503",
            "Longitud": "-71.4263562091436",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "40609",
            "CodCom": "625",
            "Comuna": "VIÑA DEL MAR",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "TRONCAL SUR N°3290",
            "Email": "40609@escopec.cl",
            "Fono": "33 267-3837",
            "JefeEDS": "Alejandro Ruiz",
            "Latitud": "-33.0410538623066",
            "Longitud": "-71.5193352480530",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "28",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "40702",
            "CodCom": "74",
            "Comuna": "COQUIMBO",
            "CodReg": "4",
            "Region": "COQUIMBO",
            "Company": "COPEC",
            "Direccion": "LOS COPIHUES N° 650, SINDEMPART",
            "Email": "40702@escopec.cl",
            "Fono": "33 226-7483",
            "JefeEDS": "Carolina Guevara",
            "Latitud": "-29.9791240721750",
            "Longitud": "-71.3406807457226",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "40710",
            "CodCom": "162",
            "Comuna": "QUILLOTA",
            "CodReg": "5",
            "Region": "VALPARAÍSO",
            "Company": "COPEC",
            "Direccion": "AV. JUAN BAUTISTA ALBERDI, BULNES",
            "Email": "40710@escopec.cl",
            "Fono": "56-33-268853",
            "JefeEDS": "Alvaro Navarrete",
            "Latitud": "-32.8945987575406",
            "Longitud": "-71.2532528949415",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "24",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60001",
            "CodCom": "48",
            "Comuna": "HUECHURABA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "Av. Pedro fontova N° 6789",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3604840000000",
            "Longitud": "-70.6703850000000",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "12",
                    "14",
                    "26",
                    "30",
                    "39"
                ]
            }
        },
        {
            "CodES": "60002",
            "CodCom": "40",
            "Comuna": "ESTACION CENTRAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "5 DE ABRIL Nº 6198, 7 DE OCTUBRE",
            "Email": "60002@escopec.cl",
            "Fono": "56-2-7414215",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4707008208100",
            "Longitud": "-70.7186950976951",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60003",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AVENIDA GABRIELA N° 3950",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5823119042108",
            "Longitud": "-70.6064930286902",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60004",
            "CodCom": "230",
            "Comuna": "COLINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. CHAMISERO N° 10.410",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3070144734072",
            "Longitud": "-70.6642590146037",
            "Servicios": {
                "CodSer": [
                    "1",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "20",
                    "26",
                    "27",
                    "28",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60006",
            "CodCom": "94",
            "Comuna": "SAN BERNARDO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV.JORGE ALESSANDRI RODRIGUEZ 20011 AL 20055",
            "Email": "60006@escopec.cl",
            "Fono": "",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6328085442810",
            "Longitud": "-70.7142090797424",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "27",
                    "28",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60007",
            "CodCom": "628",
            "Comuna": "PEÑAFLOR",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "Balmaceda N°61",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6036010000000",
            "Longitud": "-70.8517900000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "11",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60008",
            "CodCom": "93",
            "Comuna": "LAMPA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV GRAL SAN MARTIN ESQ. LA MONTAÑA N° 2550",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3241683173071",
            "Longitud": "-70.7465330900971",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60009",
            "CodCom": "114",
            "Comuna": "ISLA DE MAIPO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SANTELICES Nº 71",
            "Email": "60009@escopec.cl",
            "Fono": "33 2819-3094",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.7559580579904",
            "Longitud": "-70.8971903371434",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60011",
            "CodCom": "60",
            "Comuna": "MACUL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "JOSE PEDRO ALESSANDRI Nº 5880",
            "Email": "60011@escopec.cl",
            "Fono": "33 2221-9657",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5051208149252",
            "Longitud": "-70.5916484177007",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "25",
                    "26",
                    "27",
                    "30",
                    "31",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60012",
            "CodCom": "344",
            "Comuna": "SAN RAMON",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. AMERICO VESPUCIO Nº 1316 -",
            "Email": "60012@escopec.cl",
            "Fono": "56-2-5482406",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.5399095120031",
            "Longitud": "-70.6498473581474",
            "Servicios": {
                "CodSer": [
                    "2",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60013",
            "CodCom": "175",
            "Comuna": "SANTA CRUZ",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. ERRAZURIZ Nº 1236",
            "Email": "60013@escopec.cl",
            "Fono": "33 282-1777",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.6324111119971",
            "Longitud": "-71.3521452677869",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "16",
                    "19",
                    "23",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60014",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "STA. ELENA Nº 2206",
            "Email": "60014@escopec.cl",
            "Fono": "56-2-5552517",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4723333675958",
            "Longitud": "-70.6261956481223",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60015",
            "CodCom": "47",
            "Comuna": "CONCHALI",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. PRINCIPAL Nº 1236, EL GUANACO",
            "Email": "60015@escopec.cl",
            "Fono": "56-2-6255615",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3806413006079",
            "Longitud": "-70.6595465585753",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60019",
            "CodCom": "50",
            "Comuna": "CERRILLOS",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "PEDRO AGUIRRE CERDA Nº 5355",
            "Email": "60019@escopec.cl",
            "Fono": "56-2-5572062",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4884494504663",
            "Longitud": "-70.7009492899826",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60021",
            "CodCom": "55",
            "Comuna": "QUINTA NORMAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VARGAS FONTECILLA Nº 4182, ESQ. CORONEL ROBLES",
            "Email": "60021@escopec.cl",
            "Fono": "33 2401-6102",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4332378106818",
            "Longitud": "-70.6920651390267",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60026",
            "CodCom": "666",
            "Comuna": "EL BOSQUE",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "GRAN AVENIDA Nº 9682",
            "Email": "60026@escopec.cl",
            "Fono": "33 2547-0124",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5486751079620",
            "Longitud": "-70.6703231940090",
            "Servicios": {
                "CodSer": [
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60028",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. ANDRES BELLO Nº 1051",
            "Email": "60028@escopec.cl",
            "Fono": "33 2235-2327",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4297829263774",
            "Longitud": "-70.6239158851074",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60029",
            "CodCom": "148",
            "Comuna": "TALAGANTE",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "RUTA 78, KM 31 1/2, PONIENTE",
            "Email": "60029@escopec.cl",
            "Fono": "56-2-4609642",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6333076180473",
            "Longitud": "-70.8690328751451",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "28",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60032",
            "CodCom": "148",
            "Comuna": "TALAGANTE",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "RUTA 78, KM 31 1/2, ORIENTE",
            "Email": "60032@escopec.cl",
            "Fono": "33 2460-9647",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6352583896204",
            "Longitud": "-70.8686832863289",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "28",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60034",
            "CodCom": "52",
            "Comuna": "PADRE HURTADO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO MELIPILLA Nº 1891",
            "Email": "60034@escopec.cl",
            "Fono": "33 2811-4768",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5668286638040",
            "Longitud": "-70.8126911693598",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31",
                    "33",
                    "37"
                ]
            }
        },
        {
            "CodES": "60035",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "PEDRO DE VALDIVIA Nº 2142",
            "Email": "60035@escopec.cl",
            "Fono": "33 2341-8668",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4421455192194",
            "Longitud": "-70.6063033972095",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "19",
                    "23",
                    "30",
                    "36",
                    "39"
                ]
            }
        },
        {
            "CodES": "60036",
            "CodCom": "128",
            "Comuna": "LA REINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AVDA BILBAO Nº5739, VESPUCIO",
            "Email": "60036@escopec.cl",
            "Fono": "33 2226-1554",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4315078676610",
            "Longitud": "-70.5740241500354",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "30",
                    "31",
                    "32",
                    "33",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60037",
            "CodCom": "121",
            "Comuna": "VITACURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VITACURA Nº 6380",
            "Email": "60037@escopec.cl",
            "Fono": "56-2-2193441",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3897202968989",
            "Longitud": "-70.5705198937107",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "27",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60039",
            "CodCom": "158",
            "Comuna": "EL MONTE",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. LIBERTADORES Nº 98",
            "Email": "60039@escopec.cl",
            "Fono": "33 2818-4608",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6796708432401",
            "Longitud": "-70.9793835895989",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60040",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. J. P. ALESSANDRI Nº 4003",
            "Email": "60040@escopec.cl",
            "Fono": "33 2283-4610",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4906726371326",
            "Longitud": "-70.5999795034981",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60041",
            "CodCom": "230",
            "Comuna": "COLINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CARRETERA SAN MARTIN Nº 401",
            "Email": "60041@escopec.cl",
            "Fono": "56-2-8441334",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.2013730526675",
            "Longitud": "-70.6733922304605",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60042",
            "CodCom": "40",
            "Comuna": "ESTACION CENTRAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "ECUADOR Nº 4603 ESQ. BLANCO GARCES",
            "Email": "60042@escopec.cl",
            "Fono": "56-02-2779419",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4529188091221",
            "Longitud": "-70.6983712511268",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "21",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60044",
            "CodCom": "53",
            "Comuna": "INDEPENDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. MIGUEL RAMIREZ Nº 1325",
            "Email": "60044@escopec.cl",
            "Fono": "33 221-6642",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1748978223539",
            "Longitud": "-70.7055121789967",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60045",
            "CodCom": "344",
            "Comuna": "SAN RAMON",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "STA. ROSA Nº 6565",
            "Email": "60045@escopec.cl",
            "Fono": "56-2-5254611",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.5212225573311",
            "Longitud": "-70.6367221729595",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "60046",
            "CodCom": "53",
            "Comuna": "INDEPENDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "GENERAL PRIETO N° 1500",
            "Email": "60046@escopec.cl",
            "Fono": "02-7325044",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.4294092878049",
            "Longitud": "-70.6588428359612",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60047",
            "CodCom": "47",
            "Comuna": "CONCHALI",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "INDEPENDENCIA Nº 3060",
            "Email": "60047@escopec.cl",
            "Fono": "56-2-7341943",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.3988333006999",
            "Longitud": "-70.6689402775323",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "60048",
            "CodCom": "43",
            "Comuna": "BUIN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "BALMACEDA Nº 901, ESQ. O'HIGGINS",
            "Email": "60048@escopec.cl",
            "Fono": "33 2821-2370",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.7315699163765",
            "Longitud": "-70.7488672714968",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60050",
            "CodCom": "121",
            "Comuna": "VITACURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VITACURA N° 5579",
            "Email": "60050@escopec.cl",
            "Fono": "56-2-2184263",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3937272589763",
            "Longitud": "-70.5770062208546",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "20",
                    "21",
                    "22",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60053",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. LA FLORIDA Nº 9871",
            "Email": "60053@escopec.cl",
            "Fono": "56-2-2874763",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5480641311041",
            "Longitud": "-70.5688165801799",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "28",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60054",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. BLANCO ENCALADA Nº 1899",
            "Email": "60054@escopec.cl",
            "Fono": "56-2-6732411",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4564784156125",
            "Longitud": "-70.6607447573458",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60055",
            "CodCom": "346",
            "Comuna": "SAN JOAQUIN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SIERRA BELLA Nº 3009",
            "Email": "60055@escopec.cl",
            "Fono": "56-2-5530937",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4837720888305",
            "Longitud": "-70.6323800662490",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60057",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. LAS CONDES Nº 12145",
            "Email": "60057@escopec.cl",
            "Fono": "22175642",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3738769239118",
            "Longitud": "-70.5196164733403",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60064",
            "CodCom": "346",
            "Comuna": "SAN JOAQUIN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV.SANTA ROSA N.4180 ESQ.SALESIANO",
            "Email": "60064@escopec.cl",
            "Fono": "33 2552-3457",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4950917824573",
            "Longitud": "-70.6393399097775",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60066",
            "CodCom": "50",
            "Comuna": "CERRILLOS",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. PEDRO AGUIRRE CERDA Nº 5903",
            "Email": "60066@escopec.cl",
            "Fono": "56-2-7414215",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4926500402052",
            "Longitud": "-70.7042365809891",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "60068",
            "CodCom": "93",
            "Comuna": "LAMPA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "RUTA G16 SITIO 122 ESQ. ARCO IRIS",
            "Email": "60068@escopec.cl",
            "Fono": "33 2842-1066",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3041930177207",
            "Longitud": "-70.8534168639922",
            "Servicios": {
                "CodSer": [
                    "2",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "26",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60069",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. IRARRAZAVAL Nº 1102",
            "Email": "60069@escopec.cl",
            "Fono": "33 2343-0935",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4530601858522",
            "Longitud": "-70.6191086385347",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60070",
            "CodCom": "230",
            "Comuna": "COLINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. CHICUREO N° 14.100 ESQ. AV EL VALLE",
            "Email": "60070@escopec.cl",
            "Fono": "33 29464745",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.2830509401870",
            "Longitud": "-70.6526269033755",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "28",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38",
                    "39"
                ]
            }
        },
        {
            "CodES": "60072",
            "CodCom": "172",
            "Comuna": "RENCA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CARRET. PANAMERICANA NORTE Nº 1767",
            "Email": "60072@escopec.cl",
            "Fono": "56-2-7372924",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.4072452140026",
            "Longitud": "-70.6796898845798",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "60073",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN RAMON N.2701, SAN CARLOS DE APOQUINDO",
            "Email": "60073@escopec.cl",
            "Fono": "6835-5642",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4074365205547",
            "Longitud": "-70.5109796118132",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "30",
                    "32",
                    "33",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60074",
            "CodCom": "129",
            "Comuna": "MAIPU",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AVDA. PAJARITOS N° 5.200",
            "Email": "60074@escopec.cl",
            "Fono": "33 2744-5935",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4739776706663",
            "Longitud": "-70.7402507062597",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "20",
                    "26",
                    "27",
                    "28",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60075",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. IRARRAZAVAL Nº 5277",
            "Email": "60075@escopec.cl",
            "Fono": "56-2-2266596",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4540323700751",
            "Longitud": "-70.5752302279567",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60076",
            "CodCom": "51",
            "Comuna": "SAN FERNANDO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA SUR KM 126,8",
            "Email": "60076@escopec.cl",
            "Fono": "56-9-0995554",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.5086873599566",
            "Longitud": "-70.9228382044236",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "35",
                    "36"
                ]
            }
        },
        {
            "CodES": "60077",
            "CodCom": "55",
            "Comuna": "QUINTA NORMAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN PABLO N° 4383 ESQUINA BISMARK",
            "Email": "60077@escopec.cl",
            "Fono": "8249-8995",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4383025491896",
            "Longitud": "-70.6942692449632",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "24",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60078",
            "CodCom": "87",
            "Comuna": "QUILICURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LAS TORRES 902 ESQ LO CRUZAT",
            "Email": "60078@escopec.cl",
            "Fono": "8768-3940",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3608898397271",
            "Longitud": "-70.7213099595404",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60079",
            "CodCom": "121",
            "Comuna": "VITACURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "COSTANERA NORTE KM 2,5 E-0",
            "Email": "60079@escopec.cl",
            "Fono": "56-9-75877148",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3751258309989",
            "Longitud": "-70.5318654892999",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "20",
                    "24",
                    "26",
                    "30",
                    "32",
                    "33",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60080",
            "CodCom": "148",
            "Comuna": "TALAGANTE",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. BERNARDO O'HIGGINS Nº 530",
            "Email": "60080@escopec.cl",
            "Fono": "33 2815-3311",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6612061406947",
            "Longitud": "-70.9213490796756",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "32",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60085",
            "CodCom": "118",
            "Comuna": "LO BARNECHEA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LOS CONDORES  Nº 225, FARELLONES",
            "Email": "60085@escopec.cl",
            "Fono": "56-2-3211044",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3545609177068",
            "Longitud": "-70.3168832659184",
            "Servicios": {
                "CodSer": [
                    "9",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60086",
            "CodCom": "172",
            "Comuna": "RENCA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV.JOSE M.BALMACEDA 4574",
            "Email": "60086@escopec.cl",
            "Fono": "33 2646-6273",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.4023142890167",
            "Longitud": "-70.7150127066225",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60087",
            "CodCom": "52",
            "Comuna": "PADRE HURTADO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO A MELIPILLA 1846",
            "Email": "60087@escopec.cl",
            "Fono": "33 2811-1499",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5669938986457",
            "Longitud": "-70.8108095407407",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60088",
            "CodCom": "94",
            "Comuna": "SAN BERNARDO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO NOS A LOS MORROS N°1275",
            "Email": "60088@escopec.cl",
            "Fono": "8559-4092",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6399110346562",
            "Longitud": "-70.6792402468747",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60090",
            "CodCom": "40",
            "Comuna": "ESTACION CENTRAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "5 DE ABRIL N° 4299 ESQUINA PLACILLA",
            "Email": "60090@escopec.cl",
            "Fono": "56-2-776411",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4593029941343",
            "Longitud": "-70.6932426447341",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "27",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60092",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. ELIODORO YAÑEZ Nº 1960",
            "Email": "60092@escopec.cl",
            "Fono": "33 2223-9087",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4314267805730",
            "Longitud": "-70.6102499856043",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60093",
            "CodCom": "192",
            "Comuna": "PAINE",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AVDA. PRESIDENTE PRIETO 486 LOTE A-1",
            "Email": "60093@escopec.cl",
            "Fono": "56-2-8241404",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.8132063956545",
            "Longitud": "-70.7497503347259",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "16",
                    "26",
                    "30",
                    "31",
                    "33",
                    "37"
                ]
            }
        },
        {
            "CodES": "60095",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CONCHA Y TORO Nº 0316",
            "Email": "60095@escopec.cl",
            "Fono": "33 2849-1040",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.6166563593111",
            "Longitud": "-70.5740708003571",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "17",
                    "24",
                    "26",
                    "27",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60096",
            "CodCom": "630",
            "Comuna": "PEÑALOLEN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. ORIENTALES Nº 5710",
            "Email": "60096@escopec.cl",
            "Fono": "2  2274418",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4623940571802",
            "Longitud": "-70.5731547326013",
            "Servicios": {
                "CodSer": [
                    "2",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "24",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60097",
            "CodCom": "127",
            "Comuna": "MOSTAZAL",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "SITIO 4 PARCELACION PATRIA NUEVA",
            "Email": "60097@escopec.cl",
            "Fono": "33 249-1954",
            "JefeEDS": "Erick Silva",
            "Latitud": "-33.9839253368856",
            "Longitud": "-70.7132252339082",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60098",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. MATTA Nº 788",
            "Email": "60098@escopec.cl",
            "Fono": "33 2554-1335",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4590030624875",
            "Longitud": "-70.6450230403949",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "60100",
            "CodCom": "129",
            "Comuna": "MAIPU",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "EL ROSAL 6349",
            "Email": "60100@escopec.cl",
            "Fono": "33 2743-8175",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4792909773394",
            "Longitud": "-70.7559448938521",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60101",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SALVADOR Nº 1368",
            "Email": "60101@escopec.cl",
            "Fono": "33 2417-9312",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4458241596128",
            "Longitud": "-70.6226344078276",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60102",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. FRANCISCO BILBAO Nº 960",
            "Email": "60102@escopec.cl",
            "Fono": "33 2205-9113",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4399623702950",
            "Longitud": "-70.6203605215636",
            "Servicios": {
                "CodSer": [
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60106",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV.MEMBRILLAR 490 AL 496",
            "Email": "60106@escopec.cl",
            "Fono": "+56 9 50778226",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1723185717824",
            "Longitud": "-70.7273590287549",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60107",
            "CodCom": "53",
            "Comuna": "INDEPENDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VIVACETA Nº 715, RETIRO",
            "Email": "60107@escopec.cl",
            "Fono": "56-2-7370919",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4239308813468",
            "Longitud": "-70.6617794601043",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "60108",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LOTA Nº 2864",
            "Email": "60108@escopec.cl",
            "Fono": "56-2-2323508",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4214929816299",
            "Longitud": "-70.5974500633134",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "27",
                    "28",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60109",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. ANDRES BELLO Nº 2722",
            "Email": "60109@escopec.cl",
            "Fono": "56-2-2311097",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4137958011168",
            "Longitud": "-70.6044896114639",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "20",
                    "26",
                    "27",
                    "28",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60110",
            "CodCom": "118",
            "Comuna": "LO BARNECHEA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV LA DEHESA N° 1310",
            "Email": "60110@escopec.cl",
            "Fono": "56-9-988248212",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3601677229216",
            "Longitud": "-70.5167284798638",
            "Servicios": {
                "CodSer": [
                    "1",
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "26",
                    "30",
                    "36",
                    "39"
                ]
            }
        },
        {
            "CodES": "60113",
            "CodCom": "94",
            "Comuna": "SAN BERNARDO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO LONGITUDINAL SUR KM 27",
            "Email": "60113@escopec.cl",
            "Fono": "33 2857-4019",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6696305967448",
            "Longitud": "-70.7219260091624",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "37"
                ]
            }
        },
        {
            "CodES": "60115",
            "CodCom": "40",
            "Comuna": "ESTACION CENTRAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "GENERAL VELASQUEZ Nº 301",
            "Email": "60115@escopec.cl",
            "Fono": "33 2776-0784",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4584000295822",
            "Longitud": "-70.6897051341366",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60120",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV.EYZAGUIRRE N° 2436",
            "Email": "60120@escopec.cl",
            "Fono": "73320871",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.6159140008819",
            "Longitud": "-70.6040167183260",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "21",
                    "25",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60123",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "ROSAS Nº 3218",
            "Email": "60123@escopec.cl",
            "Fono": "26819750",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4374898778464",
            "Longitud": "-70.6791944392879",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60125",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. LA FLORIDA Nº 11215",
            "Email": "60125@escopec.cl",
            "Fono": "33 2267-0020",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5568171877201",
            "Longitud": "-70.5610208490415",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60126",
            "CodCom": "53",
            "Comuna": "INDEPENDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE Nº 633",
            "Email": "60126@escopec.cl",
            "Fono": "33 2735-3019",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.4235574301791",
            "Longitud": "-70.6725936528504",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60128",
            "CodCom": "129",
            "Comuna": "MAIPU",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO LO SIERRA Nº 03000",
            "Email": "60128@escopec.cl",
            "Fono": "33 2854-0350",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.5331601674731",
            "Longitud": "-70.7023445681835",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60132",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VICUÑA MACKENNA Nº 654",
            "Email": "60132@escopec.cl",
            "Fono": "33 2222-6482",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4498622741997",
            "Longitud": "-70.6309988642206",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "13",
                    "17",
                    "19",
                    "23",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60133",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. COLON Nº 7400",
            "Email": "60133@escopec.cl",
            "Fono": "56-2-2290054",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4179383456230",
            "Longitud": "-70.5539825142726",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60134",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "PEDRO DE VALDIVIA Nº 4298",
            "Email": "60134@escopec.cl",
            "Fono": "56-2-2398499",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4620549970947",
            "Longitud": "-70.6059387951387",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "13",
                    "23",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60135",
            "CodCom": "128",
            "Comuna": "LA REINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. OSSA Nº 591 ESQ.SIMON BOLIVAR",
            "Email": "60135@escopec.cl",
            "Fono": "56-2-2261948",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4482530000000",
            "Longitud": "-70.5711800000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "27",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60137",
            "CodCom": "128",
            "Comuna": "LA REINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "PRINCIPE DE GALES Nº 6880",
            "Email": "60137@escopec.cl",
            "Fono": "33 2277-4898",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4384747334670",
            "Longitud": "-70.5610662435292",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60139",
            "CodCom": "109",
            "Comuna": "SAN MIGUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "STA. ROSA Nº 5499",
            "Email": "60139@escopec.cl",
            "Fono": "33 2511-4577",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.5067024964072",
            "Longitud": "-70.6392212029473",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60140",
            "CodCom": "46",
            "Comuna": "LA CISTERNA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "GRAN AVENIDA Nº 8765",
            "Email": "60140@escopec.cl",
            "Fono": "33 2419-4535",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.5366992900886",
            "Longitud": "-70.6646751349245",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60141",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN PABLO Nº 2602",
            "Email": "60141@escopec.cl",
            "Fono": "56-2-6825879",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4352595174433",
            "Longitud": "-70.6731864429015",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60142",
            "CodCom": "142",
            "Comuna": "PUDAHUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN PABLO Nº 8815",
            "Email": "60142@escopec.cl",
            "Fono": "56-2-6441457",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4437577618690",
            "Longitud": "-70.7553426480465",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60143",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "MAPOCHO Nº 1998/BRASIL",
            "Email": "60143@escopec.cl",
            "Fono": "33 2696-3851",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4323057443547",
            "Longitud": "-70.6646826655845",
            "Servicios": {
                "CodSer": [
                    "2",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60145",
            "CodCom": "46",
            "Comuna": "LA CISTERNA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "GRAN AV. Nº 9257, LOS MORROS",
            "Email": "60145@escopec.cl",
            "Fono": "56-2-5488255",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5434088877072",
            "Longitud": "-70.6661528181373",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60158",
            "CodCom": "94",
            "Comuna": "SAN BERNARDO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "FREIRE Nº 45",
            "Email": "60158@escopec.cl",
            "Fono": "56-2-8591743",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5872132504002",
            "Longitud": "-70.6998695428124",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "16",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60166",
            "CodCom": "172",
            "Comuna": "RENCA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. AMERICO VESPUCIO Nº 2001, LO BOZA",
            "Email": "60166@escopec.cl",
            "Fono": "33 24436380",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3865076020648",
            "Longitud": "-70.7591461403933",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60192",
            "CodCom": "230",
            "Comuna": "COLINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "Carretera General San Martín Antigua",
            "Email": "60192@escopec.cl",
            "Fono": "56-79874536",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.2150650000000",
            "Longitud": "-70.6851850000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "5",
                    "9",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60195",
            "CodCom": "630",
            "Comuna": "PEÑALOLEN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "Av. Departamental 7494",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Sebastián García",
            "Latitud": "-30.5118490000000",
            "Longitud": "-70.5545330000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "12",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60196",
            "CodCom": "347",
            "Comuna": "MACHALI",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. SAN JUAN N ° 1151",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1697060000000",
            "Longitud": "-70.6775040000000",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "39"
                ]
            }
        },
        {
            "CodES": "60197",
            "CodCom": "93",
            "Comuna": "LAMPA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "Av. España N°2469",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.2385222000000",
            "Longitud": "-70.8078388888889",
            "Servicios": {
                "CodSer": [
                    "2",
                    "12",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60220",
            "CodCom": "129",
            "Comuna": "MAIPU",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "ALBERTO LLONA Nº 640 .",
            "Email": "60220@escopec.cl",
            "Fono": "56-27666700",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.5212692229697",
            "Longitud": "-70.7559289813288",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "21",
                    "22",
                    "24",
                    "26",
                    "27",
                    "28",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60222",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. CONCHA Y TORO Nº 865",
            "Email": "60222@escopec.cl",
            "Fono": "56-2-8502645",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.6025005943042",
            "Longitud": "-70.5768752660002",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "13",
                    "17",
                    "19",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60223",
            "CodCom": "55",
            "Comuna": "QUINTA NORMAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. SAN PABLO Nº 4469",
            "Email": "60223@escopec.cl",
            "Fono": "62253787",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4385582288237",
            "Longitud": "-70.6960462138197",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60224",
            "CodCom": "109",
            "Comuna": "SAN MIGUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CARLOS VALDOVINOS Nº 2765",
            "Email": "60224@escopec.cl",
            "Fono": "33 2563-0305",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4784176915612",
            "Longitud": "-70.6713120429942",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60225",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "MILLAN Nº 1023",
            "Email": "60225@escopec.cl",
            "Fono": "56-72-230288",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1730401974786",
            "Longitud": "-70.7506238435918",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60226",
            "CodCom": "48",
            "Comuna": "HUECHURABA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "RECOLETA Nº 4406 ESQUINA VESPUCIO",
            "Email": "60226@escopec.cl",
            "Fono": "33 2625-9456",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3825683199349",
            "Longitud": "-70.6406190926295",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "60227",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN PABLO Nº 1990, BRASIL",
            "Email": "60227@escopec.cl",
            "Fono": "33 2696-6696",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4348773816483",
            "Longitud": "-70.6644507481463",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60228",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LIA AGUIRRE Nº 10",
            "Email": "60228@escopec.cl",
            "Fono": "56-2-2839442",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.5230362918527",
            "Longitud": "-70.5996931422905",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "27",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60229",
            "CodCom": "50",
            "Comuna": "CERRILLOS",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LO ERRAZURIZ Nº 713",
            "Email": "60229@escopec.cl",
            "Fono": "56-2-5574190",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4979971291492",
            "Longitud": "-70.7169960161586",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60230",
            "CodCom": "51",
            "Comuna": "SAN FERNANDO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. BERNARDO O'HIGGINS Nº 010",
            "Email": "60230@escopec.cl",
            "Fono": "56-72-712726",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.5955732892651",
            "Longitud": "-70.9876069682434",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60231",
            "CodCom": "52",
            "Comuna": "PADRE HURTADO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "RUTA 78, KM 25 1/2",
            "Email": "60231@escopec.cl",
            "Fono": "56-2-8142446",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5843272953461",
            "Longitud": "-70.8334664881086",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60232",
            "CodCom": "53",
            "Comuna": "INDEPENDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VIVACETA Nº 1108",
            "Email": "60232@escopec.cl",
            "Fono": "56-2-7351473",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4181441242592",
            "Longitud": "-70.6638470744845",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60235",
            "CodCom": "55",
            "Comuna": "QUINTA NORMAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SALVADOR GUTIERREZ Nº 4964",
            "Email": "60235@escopec.cl",
            "Fono": "33 2772-4338",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4251903159888",
            "Longitud": "-70.7027575013897",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "17",
                    "19",
                    "23",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60236",
            "CodCom": "43",
            "Comuna": "BUIN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LONGITUDINAL SUR Nº 334",
            "Email": "60236@escopec.cl",
            "Fono": "56-2-8213492",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.7335164752472",
            "Longitud": "-70.7334726445098",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "31",
                    "33",
                    "37"
                ]
            }
        },
        {
            "CodES": "60237",
            "CodCom": "55",
            "Comuna": "QUINTA NORMAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. CARRASCAL Nº 5875",
            "Email": "60237@escopec.cl",
            "Fono": "33 2773-1023",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4164571844912",
            "Longitud": "-70.7102931297294",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60238",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "TRINIDAD Nº 1694",
            "Email": "60238@escopec.cl",
            "Fono": "6606-6874",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5467585935108",
            "Longitud": "-70.6143870823900",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60240",
            "CodCom": "57",
            "Comuna": "QUINTA DE TILCOCO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "ARGOMEDO S/N",
            "Email": "60240@escopec.cl",
            "Fono": "56-72-541316",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.3483889085072",
            "Longitud": "-70.9492131564953",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60241",
            "CodCom": "58",
            "Comuna": "RECOLETA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. EL SALTO Nº 2098",
            "Email": "60241@escopec.cl",
            "Fono": "33 26228040",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4081459020721",
            "Longitud": "-70.6351874332995",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60242",
            "CodCom": "630",
            "Comuna": "PEÑALOLEN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. A.VESPUCIO, EL VALLE Nº 5780",
            "Email": "60242@escopec.cl",
            "Fono": "56-2-2729371",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4748836033848",
            "Longitud": "-70.5769865440307",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60243",
            "CodCom": "55",
            "Comuna": "QUINTA NORMAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "MAPOCHO Nº 3898",
            "Email": "60243@escopec.cl",
            "Fono": "33 2772-4385",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4328365244389",
            "Longitud": "-70.6881196788264",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60244",
            "CodCom": "60",
            "Comuna": "MACUL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VICUÑA MACKENNA Nº 3120, A",
            "Email": "60244@escopec.cl",
            "Fono": "56-2-2382136",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4790357208583",
            "Longitud": "-70.6215844278327",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60246",
            "CodCom": "40",
            "Comuna": "ESTACION CENTRAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "5 DE ABRIL Nº 4790",
            "Email": "60246@escopec.cl",
            "Fono": "33 2779-2785",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4626620797812",
            "Longitud": "-70.6993807322894",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60249",
            "CodCom": "346",
            "Comuna": "SAN JOAQUIN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VICUÑA MACKENNA Nº 3647",
            "Email": "60249@escopec.cl",
            "Fono": "33 2556-7147",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4873085411185",
            "Longitud": "-70.6190056186072",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "60251",
            "CodCom": "62",
            "Comuna": "MOLINA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "QUECHEREGUAS N° 1616",
            "Email": "60251@escopec.cl",
            "Fono": "56-75-494209",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-35.1114081806964",
            "Longitud": "-71.2789466855102",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60253",
            "CodCom": "58",
            "Comuna": "RECOLETA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. RECOLETA Nº 1207",
            "Email": "60253@escopec.cl",
            "Fono": "56-227324991",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4178375243766",
            "Longitud": "-70.6435989747847",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60255",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "TOBALABA Nº 5439",
            "Email": "60255@escopec.cl",
            "Fono": "33 2226-5993",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4379038761758",
            "Longitud": "-70.5744409382090",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60259",
            "CodCom": "638",
            "Comuna": "DOÑIHUE",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CARRETERA H-30 KM. 25 DOÑIHUE",
            "Email": "60259@escopec.cl",
            "Fono": "33 246-3084",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.2318403307104",
            "Longitud": "-70.9655238413520",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60260",
            "CodCom": "628",
            "Comuna": "PEÑAFLOR",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VICUÑA MACKENNA Nº 3957",
            "Email": "60260@escopec.cl",
            "Fono": "33 2812-1996",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6108730430401",
            "Longitud": "-70.9032224846624",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60261",
            "CodCom": "175",
            "Comuna": "SANTA CRUZ",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. ERRAZURIZ Nº 1240",
            "Email": "60261@escopec.cl",
            "Fono": "33 282-3538",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.6332065037090",
            "Longitud": "-71.3543130444739",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60263",
            "CodCom": "120",
            "Comuna": "CURICO",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "MANSO DE VELASCO Nº 215",
            "Email": "60263@escopec.cl",
            "Fono": "33 231-0303",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.9888129261622",
            "Longitud": "-71.2341964345809",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60265",
            "CodCom": "105",
            "Comuna": "LA PINTANA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. STA. ROSA Nº 13010",
            "Email": "60265@escopec.cl",
            "Fono": "56-2-25187768",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5858316141066",
            "Longitud": "-70.6280986884010",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60266",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CARRETERA EL COBRE Nº 512",
            "Email": "60266@escopec.cl",
            "Fono": "33 221-0988",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1826626289091",
            "Longitud": "-70.7272293864304",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "19",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60267",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "IRARRAZABAL Nº 4102",
            "Email": "60267@escopec.cl",
            "Fono": "33 2341-6112",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4553598190401",
            "Longitud": "-70.5882176846246",
            "Servicios": {
                "CodSer": [
                    "2",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30",
                    "36",
                    "39"
                ]
            }
        },
        {
            "CodES": "60268",
            "CodCom": "58",
            "Comuna": "RECOLETA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. RECOLETA Nº 601 DOMINICA",
            "Email": "60268@escopec.cl",
            "Fono": "33 2372-1208",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4251294746598",
            "Longitud": "-70.6454253474197",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "13",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60273",
            "CodCom": "134",
            "Comuna": "MALLOA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "BERNALES Nº 180",
            "Email": "60273@escopec.cl",
            "Fono": "56-72-387365",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.4410727095499",
            "Longitud": "-70.9446658278943",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "30"
                ]
            }
        },
        {
            "CodES": "60305",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "NATANIEL COX Nº 1802",
            "Email": "60305@escopec.cl",
            "Fono": "33 2463-9746",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4700867314765",
            "Longitud": "-70.6498287285543",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60308",
            "CodCom": "172",
            "Comuna": "RENCA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "DOMINGO STA. MARIA Nº 3329",
            "Email": "60308@escopec.cl",
            "Fono": "33 2641-1491",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.4092580953933",
            "Longitud": "-70.6932307259782",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60311",
            "CodCom": "129",
            "Comuna": "MAIPU",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AVDA. LOS PAJARITOS N° 3333/ESQUINA RAFAEL RIESCO BERNALES",
            "Email": "60311@escopec.cl",
            "Fono": "56-2-5313083",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4964688303405",
            "Longitud": "-70.7570317667324",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "20",
                    "26",
                    "28",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60312",
            "CodCom": "142",
            "Comuna": "PUDAHUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CARRETERA RUTA 68 STGO. VALPSO 12401",
            "Email": "60312@escopec.cl",
            "Fono": "56-2-6019702",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4403092172284",
            "Longitud": "-70.8045245889963",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "37"
                ]
            }
        },
        {
            "CodES": "60314",
            "CodCom": "177",
            "Comuna": "GRANEROS",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA SUR S/N",
            "Email": "60314@escopec.cl",
            "Fono": "56-72-471385",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.0700275333156",
            "Longitud": "-70.7113585643586",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "60317",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "ALAMEDA 790, SAN MATIN",
            "Email": "60317@escopec.cl",
            "Fono": "72 223-1214",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1651642711289",
            "Longitud": "-70.7451271196673",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "16",
                    "17",
                    "19",
                    "23",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "60320",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "O'CARROL Nº 1100",
            "Email": "60320@escopec.cl",
            "Fono": "56-72-225600",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1703071158618",
            "Longitud": "-70.7505066668828",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "30",
                    "32",
                    "38"
                ]
            }
        },
        {
            "CodES": "60321",
            "CodCom": "638",
            "Comuna": "DOÑIHUE",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. CACHAPOAL N° 2",
            "Email": "60321@escopec.cl",
            "Fono": "56-72-462245",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.2308955414321",
            "Longitud": "-70.9659873605898",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "60325",
            "CodCom": "143",
            "Comuna": "SAN VICENTE",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "DIEGO PORTALES N° 350",
            "Email": "60325@escopec.cl",
            "Fono": "33 257-2426",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.4411199782227",
            "Longitud": "-71.0733041183713",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "60326",
            "CodCom": "186",
            "Comuna": "PICHIDEGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "IGNACIO CARRERA PINTO S/N",
            "Email": "60326@escopec.cl",
            "Fono": "56-72-591008",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.3666775759491",
            "Longitud": "-71.2668068652657",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60332",
            "CodCom": "241",
            "Comuna": "LOLOL",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "LAS ACACIAS Nº 215",
            "Email": "60332@escopec.cl",
            "Fono": "33 223-6829",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.7248503468920",
            "Longitud": "-71.6414019668548",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60333",
            "CodCom": "175",
            "Comuna": "SANTA CRUZ",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "ORLANDI Nº 157",
            "Email": "60333@escopec.cl",
            "Fono": "72   825-343",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.6420207354958",
            "Longitud": "-71.3651018372050",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "32",
                    "33",
                    "38"
                ]
            }
        },
        {
            "CodES": "60334",
            "CodCom": "51",
            "Comuna": "SAN FERNANDO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. B. O'HIGGINS ESQ. M. VELASCO",
            "Email": "60334@escopec.cl",
            "Fono": "56-72-714343",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.5840902394424",
            "Longitud": "-70.9836743731274",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "30"
                ]
            }
        },
        {
            "CodES": "60335",
            "CodCom": "271",
            "Comuna": "NANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CARRETERA I-50, KM 24, NANCAGUA",
            "Email": "60335@escopec.cl",
            "Fono": "33 285-8090",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.6539098791825",
            "Longitud": "-71.2041447851852",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60336",
            "CodCom": "252",
            "Comuna": "CHIMBARONGO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA SUR KM 157",
            "Email": "60336@escopec.cl",
            "Fono": "56-74-781193",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.7348005191438",
            "Longitud": "-71.0396369327170",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60338",
            "CodCom": "120",
            "Comuna": "CURICO",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "LONGITUDINAL SUR KM 190, A.NEGRAS",
            "Email": "60338@escopec.cl",
            "Fono": "56-75-311623",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.9779425947672",
            "Longitud": "-71.2083179832171",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33"
                ]
            }
        },
        {
            "CodES": "60339",
            "CodCom": "120",
            "Comuna": "CURICO",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CARMEN Nº 890, C. HENRIQUEZ",
            "Email": "60339@escopec.cl",
            "Fono": "56-75-311062",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.9814698123892",
            "Longitud": "-71.2389293917422",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60341",
            "CodCom": "62",
            "Comuna": "MOLINA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "INDEPENDENCIA S/N, ESQ. L. C. MARTINEZ",
            "Email": "60341@escopec.cl",
            "Fono": "56-75493302",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-35.1135518408699",
            "Longitud": "-71.2821494851251",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60348",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "LONGITUDINAL SUR KM 90",
            "Email": "60348@escopec.cl",
            "Fono": "33 2957150",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.2069681054314",
            "Longitud": "-70.7649167601644",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60350",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. MIGUEL RAMIREZ Nº 199, LA COMPAÑIA",
            "Email": "60350@escopec.cl",
            "Fono": "33 211107",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1726114000292",
            "Longitud": "-70.7229578478218",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60351",
            "CodCom": "57",
            "Comuna": "QUINTA DE TILCOCO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "ARGOMEDO S/N, ESQ.MIGUEL CUADRA",
            "Email": "60351@escopec.cl",
            "Fono": "33 254-1195",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.3552161450534",
            "Longitud": "-70.9638706631325",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60353",
            "CodCom": "40",
            "Comuna": "ESTACION CENTRAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "GENERAL VELASQUEZ 1366",
            "Email": "60353@escopec.cl",
            "Fono": "33 2684-0138",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4664325496357",
            "Longitud": "-70.6878535749303",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "27",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60355",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VIEL Nº 1921",
            "Email": "60355@escopec.cl",
            "Fono": "56-2-5553765",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4721015156186",
            "Longitud": "-70.6564559542560",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60358",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. BUSTAMANTE Nº 648",
            "Email": "60358@escopec.cl",
            "Fono": "56-2-2045075",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4505106350214",
            "Longitud": "-70.6291828697269",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60359",
            "CodCom": "120",
            "Comuna": "CURICO",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. FREIRE Nº 681",
            "Email": "60359@escopec.cl",
            "Fono": "33 232-6623",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.9762239741663",
            "Longitud": "-71.2447694240560",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "16",
                    "17",
                    "19",
                    "24",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60364",
            "CodCom": "129",
            "Comuna": "MAIPU",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO MELIPILLA Nº 11820",
            "Email": "60364@escopec.cl",
            "Fono": "56-2-5576127",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.5265036151468",
            "Longitud": "-70.7541856905393",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "16",
                    "17",
                    "24",
                    "25",
                    "26",
                    "27",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60368",
            "CodCom": "123",
            "Comuna": "RENGO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "SAN MARTIN S/N, M. SOLIS",
            "Email": "60368@escopec.cl",
            "Fono": "56-72-515168",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.4051033573194",
            "Longitud": "-70.8610283661435",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60369",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "BASCUÑAN GUERRERO Nº 568",
            "Email": "60369@escopec.cl",
            "Fono": "33 2689-2514",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4567682615171",
            "Longitud": "-70.6737990517673",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60370",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "MANUEL MONTT Nº 1118 ESQ ALFEREZ REAL",
            "Email": "60370@escopec.cl",
            "Fono": "56-22444309",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4379935866553",
            "Longitud": "-70.6156293629940",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "24",
                    "26",
                    "27",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38",
                    "39"
                ]
            }
        },
        {
            "CodES": "60371",
            "CodCom": "58",
            "Comuna": "RECOLETA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "RECOLETA Nº 3805",
            "Email": "60371@escopec.cl",
            "Fono": "56-2-6211391",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3907918400960",
            "Longitud": "-70.6420792419823",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60374",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CACHAPOAL Nº 1420",
            "Email": "60374@escopec.cl",
            "Fono": "56-72-242386",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1892015131422",
            "Longitud": "-70.7511451358601",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "21",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60375",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "MANQUEHUE NORTE Nº 674, PDTE. RIESCO",
            "Email": "60375@escopec.cl",
            "Fono": "33 2224-5823",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4027324654918",
            "Longitud": "-70.5699100084035",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "20",
                    "26",
                    "27",
                    "28",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60380",
            "CodCom": "105",
            "Comuna": "LA PINTANA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "TENIENTE MONTT Nº 12112",
            "Email": "60380@escopec.cl",
            "Fono": "56-2-25421899",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5744730350643",
            "Longitud": "-70.6569924761895",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "15",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60384",
            "CodCom": "109",
            "Comuna": "SAN MIGUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "DEPARTAMENTAL Nº 1902 ESQ.OCHAGAVIA",
            "Email": "60384@escopec.cl",
            "Fono": "56-2-5213683",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.5006131876939",
            "Longitud": "-70.6668528626555",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "25",
                    "26",
                    "27",
                    "30",
                    "31",
                    "32",
                    "33",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60386",
            "CodCom": "142",
            "Comuna": "PUDAHUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN PABLO Nº 8529",
            "Email": "60386@escopec.cl",
            "Fono": "56-2 26499144",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4449175490878",
            "Longitud": "-70.7479084767366",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "21",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60387",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. LAS CONDES Nº 8170",
            "Email": "60387@escopec.cl",
            "Fono": "56-2-9515407",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3982903784467",
            "Longitud": "-70.5518142999599",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "27",
                    "30",
                    "32",
                    "33",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60388",
            "CodCom": "60",
            "Comuna": "MACUL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "ESCUELA AGRICOLA Nº 2589",
            "Email": "60388@escopec.cl",
            "Fono": "33 2839-3186",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4906931180882",
            "Longitud": "-70.6059419605909",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "21",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60390",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VICUÑA MACKENNA Nº 8257",
            "Email": "60390@escopec.cl",
            "Fono": "33 2281-7694",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5308636727332",
            "Longitud": "-70.5953675346027",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60392",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LOS LEONES Nº 2347",
            "Email": "60392@escopec.cl",
            "Fono": "56-225039765",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4405188779490",
            "Longitud": "-70.6001702705133",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38",
                    "39"
                ]
            }
        },
        {
            "CodES": "60394",
            "CodCom": "347",
            "Comuna": "MACHALI",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "ABDON YARRA N° 991",
            "Email": "60394@escopec.cl",
            "Fono": "33 241-2864",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1767892251467",
            "Longitud": "-70.6577502336854",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60395",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CARMEN Nº 185, Nº 191",
            "Email": "60395@escopec.cl",
            "Fono": "33 2639-8130",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4463809764482",
            "Longitud": "-70.6428649926061",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "26",
                    "30",
                    "33",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60401",
            "CodCom": "630",
            "Comuna": "PEÑALOLEN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AVDA. CONSISTORIAL N° 2591",
            "Email": "60401@escopec.cl",
            "Fono": "33 2292-2347",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4839157199788",
            "Longitud": "-70.5472462389560",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "27",
                    "28",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60403",
            "CodCom": "51",
            "Comuna": "SAN FERNANDO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. MANSO DE VELASCO Nº 115",
            "Email": "60403@escopec.cl",
            "Fono": "56-72-711546",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.5740796539002",
            "Longitud": "-70.9914433168465",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60404",
            "CodCom": "220",
            "Comuna": "CALERA DE TANGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO CALERA DE TANGO",
            "Email": "60404@escopec.cl",
            "Fono": "33 2855-5675",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6230551380534",
            "Longitud": "-70.7990603491575",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60405",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "ARTURO PRAT Nº 671",
            "Email": "60405@escopec.cl",
            "Fono": "56-2-6326340",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4537660786232",
            "Longitud": "-70.6487144357653",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60406",
            "CodCom": "87",
            "Comuna": "QUILICURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "JOSE FRANCISCO VERGARA Nº 019",
            "Email": "60406@escopec.cl",
            "Fono": "33 2627-2167",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3665107242047",
            "Longitud": "-70.7370388247228",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60407",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN EUGENIO Nº 1602",
            "Email": "60407@escopec.cl",
            "Fono": "33 2239-9429",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4670546529129",
            "Longitud": "-70.6231837268077",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60408",
            "CodCom": "53",
            "Comuna": "INDEPENDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "INDEPENDENCIA Nº 684",
            "Email": "60408@escopec.cl",
            "Fono": "56-2-7370699",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4234183144820",
            "Longitud": "-70.6555292032171",
            "Servicios": {
                "CodSer": [
                    "4",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "19",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "60409",
            "CodCom": "175",
            "Comuna": "SANTA CRUZ",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CRUCE CAMINO PICHILEMU",
            "Email": "60409@escopec.cl",
            "Fono": "56-74-822341",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.6263303800904",
            "Longitud": "-71.3454104093010",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "30"
                ]
            }
        },
        {
            "CodES": "60411",
            "CodCom": "195",
            "Comuna": "LO ESPEJO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO LO SIERRA Nº 03079",
            "Email": "60411@escopec.cl",
            "Fono": "33 2401-3935",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.5337941845909",
            "Longitud": "-70.6994465655164",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60412",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. MATTA Nº 665",
            "Email": "60412@escopec.cl",
            "Fono": "56-2-2222296",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4579009773076",
            "Longitud": "-70.6424974775777",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "19",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60413",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "JOSE PEDRO ALESSANDRI Nº 2526",
            "Email": "60413@escopec.cl",
            "Fono": "56-2-2719252",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4779819809124",
            "Longitud": "-70.5987145012711",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60415",
            "CodCom": "38",
            "Comuna": "LAS CABRAS",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "LAS ACACIAS S/N",
            "Email": "60415@escopec.cl",
            "Fono": "33 223-6829",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.2935293930761",
            "Longitud": "-71.3131605418653",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60416",
            "CodCom": "107",
            "Comuna": "PEDRO AGUIRRE CERDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "ISABEL RIQUELME Nº 2058",
            "Email": "60416@escopec.cl",
            "Fono": "56-2-5634163",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4764986853735",
            "Longitud": "-70.6632065728728",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "60417",
            "CodCom": "94",
            "Comuna": "SAN BERNARDO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "GRAN AVENIDA Nº 13040",
            "Email": "60417@escopec.cl",
            "Fono": "33 2529-9434",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5734006934367",
            "Longitud": "-70.6897117691884",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "60418",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CAMINO A DOÑIHUE KM 2",
            "Email": "60418@escopec.cl",
            "Fono": "56-72-236381",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1683193277493",
            "Longitud": "-70.7745880306081",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60419",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. FRANCISCO BILBAO Nº 2682",
            "Email": "60419@escopec.cl",
            "Fono": "33 2343-7309",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4359025857262",
            "Longitud": "-70.5958800369071",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "21",
                    "26",
                    "27",
                    "30",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60422",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN JOSE DE LA ESTRELLA Nº 1301",
            "Email": "60422@escopec.cl",
            "Fono": "33 2262-5164",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5536684260269",
            "Longitud": "-70.6014531597055",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60423",
            "CodCom": "654",
            "Comuna": "ÑUÑOA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "DIAGONAL ORIENTE Nº 4312",
            "Email": "60423@escopec.cl",
            "Fono": "33 2227-5041",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4510952517366",
            "Longitud": "-70.5855922745797",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60424",
            "CodCom": "55",
            "Comuna": "QUINTA NORMAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "ALBERTO EDWARDS Nº 4229, A. STGO.",
            "Email": "60424@escopec.cl",
            "Fono": "56-226259456",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4461262147050",
            "Longitud": "-70.6925626046699",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60425",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO EL ALBA N° 9610 / EL ALGARROBO",
            "Email": "60425@escopec.cl",
            "Fono": "33 22123612",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4014843371447",
            "Longitud": "-70.5328128158525",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60426",
            "CodCom": "93",
            "Comuna": "LAMPA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "PEDRO AGUIRRE CERDA Nº 1370",
            "Email": "60426@escopec.cl",
            "Fono": "56-2-8422223",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.2825095770742",
            "Longitud": "-70.8704738432393",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "60428",
            "CodCom": "272",
            "Comuna": "PICHILEMU",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. ORTUZAR Nº 292",
            "Email": "60428@escopec.cl",
            "Fono": "33 284-1880",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.3857997564621",
            "Longitud": "-72.0036499150818",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60430",
            "CodCom": "142",
            "Comuna": "PUDAHUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "J. J. PEREZ Nº 8408",
            "Email": "60430@escopec.cl",
            "Fono": "56-2-6436338",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4301544526804",
            "Longitud": "-70.7411356106848",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "60432",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CONCHA Y TORO Nº 05",
            "Email": "60432@escopec.cl",
            "Fono": "33 2851-8500",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.6124378975490",
            "Longitud": "-70.5755838108425",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "30"
                ]
            }
        },
        {
            "CodES": "60433",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "ISABEL LA CATOLICA Nº 4020",
            "Email": "60433@escopec.cl",
            "Fono": "56-2-2087672",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4293180711805",
            "Longitud": "-70.5818759822642",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "27",
                    "28",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60434",
            "CodCom": "650",
            "Comuna": "HUALAÑE",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. PRAT Nº 425",
            "Email": "60434@escopec.cl",
            "Fono": "56-75-481149",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.9748186132832",
            "Longitud": "-71.8044943856932",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60436",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "TRINIDAD Nº 1300",
            "Email": "60436@escopec.cl",
            "Fono": "33 2291-5121",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5471421545737",
            "Longitud": "-70.6040254605651",
            "Servicios": {
                "CodSer": [
                    "2",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60437",
            "CodCom": "236",
            "Comuna": "PEUMO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CAMINO PELEQUEN, SN. ANTONIO",
            "Email": "60437@escopec.cl",
            "Fono": "56-72-561588",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.3943349003310",
            "Longitud": "-71.1600219708755",
            "Servicios": {
                "CodSer": [
                    "4",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "60438",
            "CodCom": "204",
            "Comuna": "TENO",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. COMALLE Nº 196, ESQ. ARTURO PRAT",
            "Email": "60438@escopec.cl",
            "Fono": "33 2411163",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.8701510712674",
            "Longitud": "-71.1625441592295",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60439",
            "CodCom": "250",
            "Comuna": "SAGRADA FAMILIA",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. ESPERANZA S/N, SAN LUIS",
            "Email": "60439@escopec.cl",
            "Fono": "33 252-0613",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-35.0001728113907",
            "Longitud": "-71.3809163875376",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "14",
                    "30"
                ]
            }
        },
        {
            "CodES": "60441",
            "CodCom": "38",
            "Comuna": "LAS CABRAS",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "RUTA H-66, EL MANZANO, LOTE A SITIO A Y 3",
            "Email": "60441@escopec.cl",
            "Fono": "56-722506219",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1743095267766",
            "Longitud": "-71.3973815470979",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "60444",
            "CodCom": "558",
            "Comuna": "LICANTEN",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "AV. LAUTARO S/N",
            "Email": "60444@escopec.cl",
            "Fono": "33 246-0475",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.9832542936302",
            "Longitud": "-71.9908371829881",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60445",
            "CodCom": "558",
            "Comuna": "LICANTEN",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CAMINO PUBLICO S/N",
            "Email": "60445@escopec.cl",
            "Fono": "33 198-3735",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.8877708832744",
            "Longitud": "-72.1711593010650",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60447",
            "CodCom": "200",
            "Comuna": "NAVIDAD",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "IGNACIO CARRERA PINTO S/N",
            "Email": "60447@escopec.cl",
            "Fono": "56-72-242782",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-33.9461274552136",
            "Longitud": "-71.7382805571585",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60450",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. PROVIDENCIA Nº 1072",
            "Email": "60450@escopec.cl",
            "Fono": "2 2235-2327",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4304018163087",
            "Longitud": "-70.6231119064578",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60455",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "STA. ROSA Nº 25",
            "Email": "60455@escopec.cl",
            "Fono": "2 2277-2861",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.6184833240266",
            "Longitud": "-70.6261941149448",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "12",
                    "14",
                    "15",
                    "30"
                ]
            }
        },
        {
            "CodES": "60470",
            "CodCom": "127",
            "Comuna": "MOSTAZAL",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA SUR KM. 66,7",
            "Email": "60470@escopec.cl",
            "Fono": "56-72-492132",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.0112620212465",
            "Longitud": "-70.7038823183534",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "28",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "60471",
            "CodCom": "127",
            "Comuna": "MOSTAZAL",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA SUR KM 67,1",
            "Email": "60471@escopec.cl",
            "Fono": "56-72-492217",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.0166407786839",
            "Longitud": "-70.7026984490056",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "28",
                    "29",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "60490",
            "CodCom": "182",
            "Comuna": "CHEPICA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "18 DE SEPTIEMBRE Nº 3686",
            "Email": "60490@escopec.cl",
            "Fono": "56-74-817333",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.7294305998196",
            "Longitud": "-71.2719996751082",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36"
                ]
            }
        },
        {
            "CodES": "60503",
            "CodCom": "94",
            "Comuna": "SAN BERNARDO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN JOSE Nº 010 ESQUINA SAN MARTIN",
            "Email": "60503@escopec.cl",
            "Fono": "56-2-8595251",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.5984770768769",
            "Longitud": "-70.6946455172515",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "21",
                    "22",
                    "25",
                    "26",
                    "27",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60512",
            "CodCom": "118",
            "Comuna": "LO BARNECHEA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. LOS TRAPENSES 2250",
            "Email": "60512@escopec.cl",
            "Fono": "56-9-42417744",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3539879388388",
            "Longitud": "-70.5394161580837",
            "Servicios": {
                "CodSer": [
                    "1",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "27",
                    "28",
                    "30",
                    "33",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60517",
            "CodCom": "129",
            "Comuna": "MAIPU",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO LONQUEN N° 9521",
            "Email": "60517@escopec.cl",
            "Fono": "9-7542812",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.5199062970713",
            "Longitud": "-70.7237664185127",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "28",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60524",
            "CodCom": "107",
            "Comuna": "PEDRO AGUIRRE CERDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CARLOS VALDOVINOS Nº 2650",
            "Email": "60524@escopec.cl",
            "Fono": "2 2502-9530",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4788666328899",
            "Longitud": "-70.6711283080483",
            "Servicios": {
                "CodSer": [
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60540",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LORD COCHRANE Nº 152",
            "Email": "60540@escopec.cl",
            "Fono": "56-2-6725476",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4478090836109",
            "Longitud": "-70.6547092769584",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60561",
            "CodCom": "347",
            "Comuna": "MACHALI",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "30 KM AL NORESTEDE RANCAGUA",
            "Email": "",
            "Fono": "",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.2084029580000",
            "Longitud": "-70.5347361930000",
            "Servicios": {
                "CodSer": [
                    "9",
                    "11",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60601",
            "CodCom": "48",
            "Comuna": "HUECHURABA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AMERICO VESPUCIO Nº 1403",
            "Email": "60601@escopec.cl",
            "Fono": "26245149",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3704184317330",
            "Longitud": "-70.6714754661423",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "19",
                    "25",
                    "26",
                    "27",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60610",
            "CodCom": "85",
            "Comuna": "PIRQUE",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VIRGINIA SUBERCASEAUX Nº 1390",
            "Email": "60610@escopec.cl",
            "Fono": "56-2-8495262",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6411612253204",
            "Longitud": "-70.5864371869647",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60618",
            "CodCom": "55",
            "Comuna": "QUINTA NORMAL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "JOSE JOAQUIN PEREZ N° 5190",
            "Email": "60618@escopec.cl",
            "Fono": "2 2934-0688",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4326723939832",
            "Longitud": "-70.7068158685597",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60635",
            "CodCom": "121",
            "Comuna": "VITACURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. VITACURA Nº 4207, LOS LAURELES",
            "Email": "60635@escopec.cl",
            "Fono": "2 2228-1011",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3997013324728",
            "Longitud": "-70.5894246746695",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "11",
                    "12",
                    "17",
                    "23",
                    "26",
                    "30",
                    "36",
                    "39"
                ]
            }
        },
        {
            "CodES": "60640",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VICUÑA MACKENNA Nº 1990",
            "Email": "60640@escopec.cl",
            "Fono": "2 2554-4323",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.4695218658443",
            "Longitud": "-70.6244453869071",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60664",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "BASCUÑAN GUERRERO Nº 2445",
            "Email": "60664@escopec.cl",
            "Fono": "2 2891-0369",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4772851137794",
            "Longitud": "-70.6721096019332",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "21",
                    "25",
                    "26",
                    "30",
                    "31",
                    "37"
                ]
            }
        },
        {
            "CodES": "60691",
            "CodCom": "109",
            "Comuna": "SAN MIGUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMINO LO OVALLE Nº 669",
            "Email": "60691@escopec.cl",
            "Fono": "2 2526-6478",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.5169597662584",
            "Longitud": "-70.6459762334051",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60701",
            "CodCom": "89",
            "Comuna": "LAS CONDES",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "APOQUINDO Nº 7520",
            "Email": "60701@escopec.cl",
            "Fono": "56-23721208",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.4080777964061",
            "Longitud": "-70.5512515672223",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60711",
            "CodCom": "60",
            "Comuna": "MACUL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "VICUÑA MACKENNA Nº 5700",
            "Email": "60711@escopec.cl",
            "Fono": "2 2221-3101",
            "JefeEDS": "Sergio Aguirre",
            "Latitud": "-33.5078621825268",
            "Longitud": "-70.6134806925311",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "24",
                    "26",
                    "28",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60712",
            "CodCom": "118",
            "Comuna": "LO BARNECHEA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "LA DEHESA Nº 2016, EL RODEO",
            "Email": "60712@escopec.cl",
            "Fono": "2 2216-7363",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3525363206695",
            "Longitud": "-70.5191013427772",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "27",
                    "28",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60721",
            "CodCom": "230",
            "Comuna": "COLINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "PANAMERICANA NORTE KM 27 1/2 COLINA",
            "Email": "60721@escopec.cl",
            "Fono": "56-2-7451068",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.2162088118446",
            "Longitud": "-70.7668923818397",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "17",
                    "25",
                    "26",
                    "29",
                    "30",
                    "31",
                    "33",
                    "35",
                    "37"
                ]
            }
        },
        {
            "CodES": "60741",
            "CodCom": "121",
            "Comuna": "VITACURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "KM 7,55  EJE ORIENTE-PONIENTE C. NORTE",
            "Email": "60741@escopec.cl",
            "Fono": "2 2218-4796",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3819125740978",
            "Longitud": "-70.5889143900083",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60742",
            "CodCom": "121",
            "Comuna": "VITACURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "KM7,65, EJE ORIENTE-PONIENTE C. NORTE",
            "Email": "60742@escopec.cl",
            "Fono": "2 2218-6707",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3827112869639",
            "Longitud": "-70.5894488103498",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "7",
                    "8",
                    "9",
                    "11",
                    "12",
                    "14",
                    "24",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60745",
            "CodCom": "142",
            "Comuna": "PUDAHUEL",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "COSTANERA NORTE KM 33.75",
            "Email": "60745@escopec.cl",
            "Fono": "8448-2064",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.4343743865415",
            "Longitud": "-70.8106218327058",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "37"
                ]
            }
        },
        {
            "CodES": "60753",
            "CodCom": "42",
            "Comuna": "SANTIAGO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "SAN PABLO Nº 1571",
            "Email": "60753@escopec.cl",
            "Fono": "2 2696-9400",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4344693203590",
            "Longitud": "-70.6598441661418",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "27",
                    "28",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60758",
            "CodCom": "227",
            "Comuna": "LITUECHE",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CARDENAL CARO S/N ESQ. OBISPO LARRAÍN  792",
            "Email": "60758@escopec.cl",
            "Fono": "56-72-851262",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.1172071576435",
            "Longitud": "-71.7299284675245",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30"
                ]
            }
        },
        {
            "CodES": "60760",
            "CodCom": "129",
            "Comuna": "MAIPU",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CARMEN Nº 988",
            "Email": "60760@escopec.cl",
            "Fono": "6667-9822",
            "JefeEDS": "Javier Ananias",
            "Latitud": "-33.5195699327065",
            "Longitud": "-70.7635571102093",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60773",
            "CodCom": "86",
            "Comuna": "PROVIDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. SANTA  MARIA Nº  0740",
            "Email": "60773@escopec.cl",
            "Fono": "2 2737-2595",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4309939662275",
            "Longitud": "-70.6264144154372",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "30",
                    "36",
                    "37",
                    "39"
                ]
            }
        },
        {
            "CodES": "60781",
            "CodCom": "213",
            "Comuna": "REQUINOA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CARRETERA PANAMERICANA SUR KM. 95",
            "Email": "60781@escopec.cl",
            "Fono": "56-72-551317",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.2426760295700",
            "Longitud": "-70.7964511757525",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33",
                    "36"
                ]
            }
        },
        {
            "CodES": "60782",
            "CodCom": "120",
            "Comuna": "CURICO",
            "CodReg": "7",
            "Region": "MAULE",
            "Company": "COPEC",
            "Direccion": "CAMINO LONGITUDINAL SUR KM 187",
            "Email": "60782@escopec.cl",
            "Fono": "75 238-6902",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.9492385039583",
            "Longitud": "-71.1835627932453",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "25",
                    "26",
                    "30",
                    "31",
                    "33"
                ]
            }
        },
        {
            "CodES": "60783",
            "CodCom": "148",
            "Comuna": "TALAGANTE",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "21 DE MAYO Nº 1500, TREBULCO",
            "Email": "60783@escopec.cl",
            "Fono": "2 2815-4037",
            "JefeEDS": "Veronica Torreblanca",
            "Latitud": "-33.6709449535828",
            "Longitud": "-70.9265111576805",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "26",
                    "27",
                    "28",
                    "30",
                    "31",
                    "33",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60801",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. MEXICO Nº 3193, ESQ. EL PEÑON",
            "Email": "60801@escopec.cl",
            "Fono": "2 2875-9259",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5802643001816",
            "Longitud": "-70.5633003673964",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "9",
                    "11",
                    "12",
                    "14",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60814",
            "CodCom": "121",
            "Comuna": "VITACURA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. LAS CONDES Nº 10912",
            "Email": "60814@escopec.cl",
            "Fono": "2 2215-6573",
            "JefeEDS": "Patricio Peñaranda",
            "Latitud": "-33.3800292072026",
            "Longitud": "-70.5306719954266",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "20",
                    "26",
                    "27",
                    "28",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60823",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. AMERICO VESPUCIO Nº 9150",
            "Email": "60823@escopec.cl",
            "Fono": "2 2282-3236",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5372013871905",
            "Longitud": "-70.6089415546651",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "17",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60824",
            "CodCom": "49",
            "Comuna": "LA FLORIDA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. LA FLORIDA Nº 7165",
            "Email": "60824@escopec.cl",
            "Fono": "56-2-2850513",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5203455985272",
            "Longitud": "-70.5803777019684",
            "Servicios": {
                "CodSer": [
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "21",
                    "22",
                    "24",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60827",
            "CodCom": "39",
            "Comuna": "RANCAGUA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. RECREO Nº 238",
            "Email": "60827@escopec.cl",
            "Fono": "56-72-236829",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.1626634040787",
            "Longitud": "-70.7389481716954",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "6",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "17",
                    "20",
                    "26",
                    "28",
                    "30",
                    "32",
                    "36",
                    "38"
                ]
            }
        },
        {
            "CodES": "60828",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. GABRIELA Nº 1482",
            "Email": "60828@escopec.cl",
            "Fono": "2 2542-4864",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5819620672922",
            "Longitud": "-70.5989070058479",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "23",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60830",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CAMILO HENRIQUEZ Nº 4583",
            "Email": "60830@escopec.cl",
            "Fono": "2 2267-7415",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5579158051720",
            "Longitud": "-70.5591585843272",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "26",
                    "30",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60838",
            "CodCom": "159",
            "Comuna": "PERALILLO",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "CAMINO A PICHILEMU KM 65",
            "Email": "60838@escopec.cl",
            "Fono": "72 286-1313",
            "JefeEDS": "Marco Gallardo",
            "Latitud": "-34.4532153995674",
            "Longitud": "-71.5428298259831",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "25",
                    "26",
                    "30",
                    "31",
                    "36"
                ]
            }
        },
        {
            "CodES": "60840",
            "CodCom": "230",
            "Comuna": "COLINA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "CARRETERA GENERAL  SAN MARTIN KM 6,3",
            "Email": "60840@escopec.cl",
            "Fono": "2 2745-5495",
            "JefeEDS": "Juan Francisco Palma",
            "Latitud": "-33.3146718424889",
            "Longitud": "-70.7045750601678",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "26",
                    "28",
                    "30",
                    "31",
                    "36",
                    "37"
                ]
            }
        },
        {
            "CodES": "60855",
            "CodCom": "213",
            "Comuna": "REQUINOA",
            "CodReg": "6",
            "Region": "O HIGGINS",
            "Company": "COPEC",
            "Direccion": "AV. LEONARDO MURIALDO Nº 225",
            "Email": "60855@escopec.cl",
            "Fono": "72 255-3652",
            "JefeEDS": "Erick Silva",
            "Latitud": "-34.2872415457751",
            "Longitud": "-70.8199847109786",
            "Servicios": {
                "CodSer": [
                    "2",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "31"
                ]
            }
        },
        {
            "CodES": "60870",
            "CodCom": "248",
            "Comuna": "SAN JOSE DE MAIPO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "COMERCIO Nº 20173",
            "Email": "60870@escopec.cl",
            "Fono": "2 2861-1313",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.6441167904674",
            "Longitud": "-70.3528173539217",
            "Servicios": {
                "CodSer": [
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "16",
                    "26",
                    "30",
                    "37"
                ]
            }
        },
        {
            "CodES": "60890",
            "CodCom": "45",
            "Comuna": "PUENTE ALTO",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. CONCHA Y TORO Nº 3919",
            "Email": "60890@escopec.cl",
            "Fono": "2 295-5550",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5697957575515",
            "Longitud": "-70.5830259765080",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "21",
                    "22",
                    "24",
                    "26",
                    "28",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60968",
            "CodCom": "202",
            "Comuna": "LA GRANJA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "STA. ROSA Nº 9466",
            "Email": "60968@escopec.cl",
            "Fono": "2 2541-5106",
            "JefeEDS": "Emilio León",
            "Latitud": "-33.5472754595597",
            "Longitud": "-70.6329699049178",
            "Servicios": {
                "CodSer": [
                    "2",
                    "4",
                    "5",
                    "6",
                    "7",
                    "9",
                    "10",
                    "11",
                    "12",
                    "14",
                    "19",
                    "24",
                    "26",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60987",
            "CodCom": "630",
            "Comuna": "PEÑALOLEN",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. TOBALABA Nº  11567",
            "Email": "60987@escopec.cl",
            "Fono": "2 279-0454",
            "JefeEDS": "Sebastián García",
            "Latitud": "-33.4782000000000",
            "Longitud": "-70.5580100000000",
            "Servicios": {
                "CodSer": [
                    "1",
                    "4",
                    "5",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "17",
                    "19",
                    "21",
                    "22",
                    "26",
                    "27",
                    "28",
                    "30",
                    "32",
                    "36",
                    "37",
                    "38"
                ]
            }
        },
        {
            "CodES": "60999",
            "CodCom": "53",
            "Comuna": "INDEPENDENCIA",
            "CodReg": "13",
            "Region": "METROPOLITANA",
            "Company": "COPEC",
            "Direccion": "AV. INDEPENDENCIA Nº1517",
            "Email": "60999@escopec.cl",
            "Fono": "2 2737-1736",
            "JefeEDS": "Nelson Pardo",
            "Latitud": "-33.4131481129962",
            "Longitud": "-70.6580728970482",
            "Servicios": {
                "CodSer": [
                    "4",
                    "6",
                    "9",
                    "11",
                    "12",
                    "14",
                    "19",
                    "21",
                    "26",
                    "30",
                    "37"
                ]
            }
        }
    ];

    public static servicios:any =[
        {id:1, value: "Pronto",seleccionado:0},
        {id:2, value: "Punto",seleccionado:0},
        {id:4, value: "Cajero Automático",seleccionado:0},
        {id:5, value: "Baño",seleccionado:0},
        {id:6, value: "Lavamax",seleccionado:0},
        {id:7, value: "Zervo",seleccionado:0},
        {id:8, value: "Voltex",seleccionado:0},
        {id:9, value: "Cupón Electrónico",seleccionado:0},
        {id:10, value: "TCT",seleccionado:0},
        {id:11, value: "Mundo Copec Lanpass",seleccionado:0},
        {id:12, value: "Taxiamigo",seleccionado:0},
        {id:13, value: "Mobil",seleccionado:0},
        {id:14, value: "Pago Click",seleccionado:0},
        {id:15, value: "Autoservicio",seleccionado:0},
        {id:16, value: "TAE",seleccionado:0},
        {id:17, value: "Renova",seleccionado:0},
        {id:19, value: "Lavamax Autoservicio",seleccionado:0},
        {id:20, value: "Lavamax Automático",seleccionado:0},
        {id:21, value: "NeoGas",seleccionado:0},
        {id:22, value: "GeoGas",seleccionado:0},
        {id:23, value: "Full Zervo",seleccionado:0},
        {id:24, value: "Mixta Zervo",seleccionado:0},
        {id:25, value: "TCT Premium",seleccionado:0},
        {id:26, value: "Fusion",seleccionado:0},
        {id:27, value: "Wifi",seleccionado:0},
        {id:28, value: "Pantallas",seleccionado:0},
        {id:29, value: "Bluemax Surtidor",seleccionado:0},
        {id:30, value: "IP",seleccionado:0},
        {id:31, value: "Bluemax Bidón",seleccionado:0},
        {id:32, value: "Agenda Online",seleccionado:0},
        {id:33, value: "Chiletur",seleccionado:0},
        {id:34, value: "Pronto Ruta",seleccionado:0},
        {id:35, value: "Sala Descanso Transportistas",seleccionado:0},
        {id:36, value: "Nueva Imagen",seleccionado:0},
        {id:37, value: "Telemedición",seleccionado:0},
        {id:38, value: "LUB",seleccionado:0},
        {id:39, value: "Ciclista",seleccionado:0},
        {id:40, value: "Camaras",seleccionado:0},
        {id:41, value: "ProMae"}
];
}
