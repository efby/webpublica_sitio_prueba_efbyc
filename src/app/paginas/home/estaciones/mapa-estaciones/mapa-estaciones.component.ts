import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { MouseEvent, AgmInfoWindow } from '@agm/core';
import { MarcadoresTest } from './markerEstaciones';
import { Marcadores } from 'src/app/models/marcadores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MapsAPILoader } from '@agm/core';
import { InfoWindow } from '@agm/core/services/google-maps-types'
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { stringify } from 'querystring';
declare let google: any;

@Component({
  selector: 'app-mapa-estaciones',
  templateUrl: './mapa-estaciones.component.html',
  styleUrls: ['./mapa-estaciones.component.css']
})
export class MapaEstacionesComponent implements OnInit {
  @Input() latInit: number;
  @Input() lngInit: number;
  @Input() latDest: number;
  @Input() lngDest: number;
  @Input() zoom: number;
  @Output() emitLatLngDestino: EventEmitter<any> = new EventEmitter();
  @Output() emitTiempoDistancia: EventEmitter<any> = new EventEmitter();
  @Output() emitEstacionesCercanas: EventEmitter<any> = new EventEmitter();

  public pantallaInfo: InfoWindow;
  public latDestOld: number;
  public origin: any;
  public destination: any;
  public waypoints: any[] = [];
  previous: any;
  _directions: boolean = false;
  marcadores: Marcadores[] = [];
  tiempoDistancia: any;
  interval: any;


  constructor(private variablesApi: VariablesGenerales, private ref: ChangeDetectorRef, private mapsAPILoader: MapsAPILoader, private utils: UtilitariosService) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
    }, 0);
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }

  clickedMarker(label: string, index: number, infoWindow) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infoWindow;
  }

  placeMarker($event) {
    if (this.previous) {
      this.previous.close();
      this.previous = null;
    }
  }

  markerDragEnd(m: Marcadores, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  ngOnInit(): void {
    if (typeof this.latInit === "undefined") {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latInit = position.coords.latitude;
        this.lngInit = position.coords.longitude;
        this.zoom = 15;
      });
    }

    this.cargaEstaciones();
  }

  cargaEstaciones() {
    this._directions = false;
    this.marcadores = [];
    MarcadoresTest.markers.forEach(m => {
      let mark: Marcadores = {
        lat: parseFloat(m.Latitud),
        lng: parseFloat(m.Longitud),
        draggable: false,
        infoWindow: m.Direccion,
        email: m.Email,
        telefono: m.Fono,
        jefeEDS: m.JefeEDS,
        servicios: this.setServicios(m.Servicios.CodSer)
      };
      this.marcadores.push(mark);
    });
  }

  activateDireccion() {
    this.latDestOld = this.latDest;
    this.origin = { lat: this.latInit, lng: this.lngInit };
    this.destination = { lat: this.latDest, lng: this.lngDest };
    this._directions = true;
    const inicio = new google.maps.LatLng(this.latInit, this.lngInit);
    const destino = new google.maps.LatLng(this.latDest, this.lngDest);
    this.filtraEstacionesByRuta();
    this.distanciaInicioFinMarcador();
    this.mapsAPILoader.load().then(() => {
      const directionsService = new google.maps.DirectionsService;
      directionsService.route({
        origin: inicio,
        destination: destino,
        waypoints: [],
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
      }, (response, status) => {
        if (status === 'OK') {
          this.tiempoDistancia = { tiempo: response.routes[0].legs[0].duration.text, segundos: response.routes[0].legs[0].duration.value, distancia: response.routes[0].legs[0].distance.text, metros: response.routes[0].legs[0].distance.value };
          this.emitTiempoDistancia.emit(this.tiempoDistancia);
        } else {
          console.log('Directions request failed due to ' + status);
        }
      });
    });
  }

  async activateDirecctions() {
    if (this.latDest !== this.latInit && this.latDest !== this.latDestOld) {
      this.activateDireccion();
    }
  }

  async distanciaInicioFinMarcador() {
    const inicio = new google.maps.LatLng(this.latInit, this.lngInit);
    const destino = new google.maps.LatLng(this.latDest, this.lngDest);
    let direcciones = [];
    this.utils.showSpinner("Interno");
    for (const marker of this.marcadores) {
      let waypoint = [{ location: new google.maps.LatLng(marker.lat, marker.lng), stopover: true }];
      let respuesta = await this.getDatosViaje(inicio, destino, waypoint, marker);
      direcciones.push(respuesta);
    }
    direcciones = direcciones.filter(r => r.segundos > 0);
    direcciones = direcciones.sort((a, b) => {
      if (a.segundos < b.segundos) return -1;
      else if (a.segundos > b.segundos) return 1;
      else return 0;
    });
    direcciones = direcciones.splice(0, 4);
    let dirAuxiliar = []
    let dirPaso = [];
    direcciones.forEach(dir => {
      let auxiliar = this.marcadores.filter(r => r.lat === dir.location.lat && r.lng === dir.location.lng)[0];
      auxiliar.tiempoViaje = this.secondsToHuman(dir.segundos);
      auxiliar.desviacion = this.secondsToHuman((dir.segundos - this.tiempoDistancia.segundos))
      dirAuxiliar.push(auxiliar);
      dirPaso.push(auxiliar);
    });
    this.utils.hideSpinner("Interno");
    this.marcadores = dirPaso;
    this.emitEstacionesCercanas.emit(dirAuxiliar);
  }

  async getDatosViaje(inicio, destino, waypoint, marker): Promise<any> {
    return await new Promise((resolve, reject) => {
      this.mapsAPILoader.load().then(() => {
        const directionsService = new google.maps.DirectionsService;
        directionsService.route({
          origin: inicio,
          destination: destino,
          waypoints: waypoint,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, (response, status) => {
          if (status === 'OK') {
            let segundos = 0;
            let metros = 0;
            response.routes[0].legs.forEach(leg => {
              segundos += leg.duration.value;
              metros += leg.distance.value;
            });
            resolve({ segundos: segundos, metros: metros, location: { lat: marker.lat, lng: marker.lng } });
          } else {
            resolve({ segundos: 0, metros: 0, location: { lat: marker.lat, lng: marker.lng } });
          }
        });
      });
    });
  }

  secondsToHuman(time): string {
    let hours = Math.floor(time / 3600);
    let minutes = Math.floor((time % 3600) / 60);

    //Anteponiendo un 0 a los minutos si son menos de 10 
    let minutos = minutes < 10 ? '0' + minutes : minutes;
    let retorno = "";
    if (hours > 0) {
      retorno = hours + " h " + minutos + " min ";
    } else {
      retorno = minutos + " min ";
    }

    return retorno;
  }

  mtsToHuman(mts): string {
    let retorno: string;
    let km = Math.floor(mts / 1000);
    let mt = Math.floor((mts % 1000) / 100);

    retorno = km + "." + mt + " Km";
    return retorno;
  }

  getDirections(latitude, longitude, value) {
    this._directions = true;
    this.origin = { lat: this.latInit, lng: this.lngInit };
    this.destination = { lat: latitude, lng: longitude };
    if (this.previous) {
      this.previous.close();
      this.previous = null;
    }
    this.emitLatLngDestino.emit({ destino: this.destination, value: value });
    //this.filtraEstacionesByRuta();
    this.marcadores = this.marcadores.filter(r => r.lat == latitude && r.lng == longitude);
  }


  setServicios(servicios): any {
    let retorno = [];
    servicios.forEach(element => {
      let idServicio = MarcadoresTest.servicios.find(r => r.id == element)
      retorno.push(idServicio);
    });
    return retorno;
  }

  filtraEstacionesByRuta() {
    let latMin = Math.min(this.origin.lat, this.destination.lat);
    let lngMin = Math.min(this.origin.lng, this.destination.lng);
    let latMax = Math.max(this.origin.lat, this.destination.lat);
    let lngMax = Math.max(this.origin.lng, this.destination.lng);

    //rectangulo formado entre origen, destino,(latMax,lngMin),(latMin,lngMax) solo si son diferentes
    if (this.origin.lat !== this.destination.lat && this.origin.lng !== this.destination.lng) {
      this.marcadores = this.marcadores.filter(r => r.lat >= latMin);
      this.marcadores = this.marcadores.filter(r => r.lat <= latMax);

      this.marcadores = this.marcadores.filter(r => r.lng >= lngMin);
      this.marcadores = this.marcadores.filter(r => r.lng <= lngMax);
    }
  }

  async nuevaRuta3Puntos(lat: number, lng: number, mark) {
    this.waypoints = [{ location: new google.maps.LatLng(lat, lng), stopover: true }];
    this.marcadores = mark.filter(r => r.lat === lat && r.lng === lng);
    //deberia emitir el nuevo tiempo
    const inicio = new google.maps.LatLng(this.latInit, this.lngInit);
    const destino = new google.maps.LatLng(this.latDest, this.lngDest);
    let resp = await this.getDatosViaje(inicio, destino, this.waypoints, this.marcadores);

    let nvoTiempoDistancia = { tiempo: this.secondsToHuman(resp.segundos), segundos: resp.segundos, distancia: this.mtsToHuman(resp.metros), metros: resp.metros };
    this.emitTiempoDistancia.emit(nvoTiempoDistancia);
  }

  filtraByServices(services: any[]) {

    let result = this.marcadores;
    services.forEach(element => {
      let nuevo = [];
      result.forEach(res => {
        if (res.servicios.findIndex(r => r.id === element.id) >= 0) {
          nuevo.push(res);
        }
      });
      result = nuevo;
    });
    this.marcadores = result;
  }

}
