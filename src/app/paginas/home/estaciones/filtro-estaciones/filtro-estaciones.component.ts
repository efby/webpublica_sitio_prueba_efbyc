import { Component, OnInit, Output, EventEmitter, NgZone, Input, ChangeDetectorRef } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MapsAPILoader } from '@agm/core';

declare let google: any;
@Component({
  selector: 'app-filtro-estaciones',
  templateUrl: './filtro-estaciones.component.html',
  styleUrls: ['./filtro-estaciones.component.css']
})
export class FiltroEstacionesComponent implements OnInit {

  @Output() emitLatLngZoom: EventEmitter<any> = new EventEmitter();
  @Input() inputDireccionInicio: string = "";
  @Input() inputDireccionDestino: string = "";
  @Input() inicio: any;
  @Input() destino: any;
  @Input() tiempoDistancia: any = { tiempo: 0, distancia: 0 };
  inputDireccionIntermedio: string;
  private geoCoder;
  _isWaipoint: boolean = false;
  interval: any;

  constructor(private variablesApi: VariablesGenerales, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private ref: ChangeDetectorRef) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges();
    }, 0);
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }

  ngOnInit() {
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
      this.getAddress();
      const autocompleteInicio = new google.maps.places.Autocomplete(document.getElementById('inputDireccionInicio'), { componentRestrictions: { country: "cl" } });
      google.maps.event.addListener(autocompleteInicio, 'place_changed', () => {
        const place = autocompleteInicio.getPlace();
        this.inicio = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() };
        this.emit();
      });
      const autocompleteDestino = new google.maps.places.Autocomplete(document.getElementById('inputDireccionDestino'), { componentRestrictions: { country: "cl" } });
      google.maps.event.addListener(autocompleteDestino, 'place_changed', () => {
        const place = autocompleteDestino.getPlace();
        this.destino = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }
        this.emit();
      });
    });
  }


  emit() {
    let latInit: number = this.inicio.lat;
    let lngInit: number = this.inicio.lng;
    let latDest: number;
    let lngDest: number;
    if (this.inputDireccionDestino != "") {
      latDest = this.destino.lat;
      lngDest = this.destino.lng;
    }
    this.emitLatLngZoom.emit({ inicio: { lat: latInit, lng: lngInit }, destino: { lat: latDest, lng: lngDest } });
  }


  getAddress() {
    this.geoCoder.geocode({ 'location': { lat: this.inicio.lat, lng: this.inicio.lng } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.inputDireccionInicio = results[0].formatted_address;
        } else {
          console.log(results);
        }
      } else {
        console.log(status);
      }

    });
  }
  limpiar() {
    this.inputDireccionDestino = "";
    this.tiempoDistancia = { tiempo: 0, distancia: 0 };
    this._isWaipoint = false;
  }

}
