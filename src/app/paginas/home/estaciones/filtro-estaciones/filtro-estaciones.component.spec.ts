import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroEstacionesComponent } from './filtro-estaciones.component';

describe('FiltroEstacionesComponent', () => {
  let component: FiltroEstacionesComponent;
  let fixture: ComponentFixture<FiltroEstacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroEstacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroEstacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
