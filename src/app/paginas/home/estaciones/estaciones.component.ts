import { Component, OnInit, ViewChild } from '@angular/core';
import { Paginas } from 'src/app/globales/paginas';
import { BreadCrumbsService } from 'src/app/servicios/bread-crumbs.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { FiltroEstacionesComponent } from './filtro-estaciones/filtro-estaciones.component';
import { MapaEstacionesComponent } from './mapa-estaciones/mapa-estaciones.component';
import { Marcadores } from 'src/app/models/marcadores';
import { MarcadoresTest } from './mapa-estaciones/markerEstaciones';

@Component({
  selector: 'app-estaciones',
  templateUrl: './estaciones.component.html',
  styleUrls: ['./estaciones.component.css']
})
export class EstacionesComponent implements OnInit {
  @ViewChild(FiltroEstacionesComponent) private filtro: FiltroEstacionesComponent;
  @ViewChild(MapaEstacionesComponent) private mapa: MapaEstacionesComponent;
  _estacionesCollapsed: boolean = true;
  _filtrosCollapsed: boolean = true;
  inicio: any = { lat: -33.4144747, lng: -70.6016892 };
  destino: any = { lat: -33.4144747, lng: -70.6016892 };
  valueDestino: string = "";
  tiempoDistancia: any = { tiempo: 0, distancia: 0 };
  zoom: number = 15;
  marcadores: Marcadores[] = [];
  _estacionesCeranas: boolean = false;
  serviciosDisponibles = MarcadoresTest.servicios;

  migajas = [
    { title: "Home", class: "", url: "/" + Paginas.HomeMuevo },
    { title: "Estaciones", class: "active", url: "" }
  ];

  constructor(private emiter: BreadCrumbsService, private variablesApi: VariablesGenerales) {
    // navigator.geolocation.getCurrentPosition((position) => {
    //   this.inicio = { lat: position.coords.latitude, lng: position.coords.longitude };
    //   this.destino = { lat: position.coords.latitude, lng: position.coords.longitude };
    //   this.zoom = 15;
    // });

  }

  ngOnInit() {
    setTimeout(() => {
      this.emiter.emitChange({ migajas: this.migajas, muestraBreadcrumbs: true });
    });
  }

  getAddress(place: any) {
    this.inicio = place.inicio;
    this.destino = place.destino;
    this.zoom = 15;
    if (this.inicio.lat !== this.destino.lat && this.inicio.lng !== this.destino.lng) {
      setTimeout(() => {
        // this.mapa.cargaEstaciones();
        // this.mapa.activateDireccion();
      }, 100);

    }
  }

  getDestino(place: any) {
    this.destino = place.destino;
    this.valueDestino = place.value;
    this._estacionesCollapsed = false;
    this.filtro.inputDireccionIntermedio = "";
    this.filtro._isWaipoint = false;
    this._estacionesCeranas = false;
  }

  getTiempoDistancia(place: any) {
    this.tiempoDistancia = place;
  }

  getEstacionesCercanas(place: any) {
    this._estacionesCeranas = true;
    this.marcadores = place;
  }

  limpiar() {
    this.mapa.cargaEstaciones();
    this.filtro.limpiar();

    for (let index = 0; index < this.serviciosDisponibles.length; index++) {
      const element = this.serviciosDisponibles[index];
      element.seleccionado = 0;
      this.serviciosDisponibles[index] = element;
    }

    this.mapa.waypoints = [];
    this._estacionesCeranas = false;
    this._estacionesCollapsed = true;
    this._filtrosCollapsed = true;
    this.inicio = { lat: -33.4144747, lng: -70.6016892 };
    this.destino = { lat: -33.4144747, lng: -70.6016892 }
  }

  selServicio(id) {
    let idComponente = this.serviciosDisponibles.findIndex(r => r.id === id);
    this.serviciosDisponibles[idComponente].seleccionado = (this.serviciosDisponibles[idComponente].seleccionado === 1 ? 0 : 1);
  }

  buscarEstacion() {
    let servicios = this.serviciosDisponibles.filter(r => r.seleccionado === 1);
    this.mapa.cargaEstaciones();
    if (this.destino.lat === -33.4144747 && this.destino.lng === -70.6016892) {
      this.mapa.filtraByServices(servicios);
    } else {
      if (servicios.length > 0) {
        this.mapa.filtraByServices(servicios);
        this.mapa.activateDireccion();
      } else {
        this.mapa.activateDireccion();
      }
    }
    this._filtrosCollapsed = true;
  }

  nuevaRuta(lat, lng) {
    let mark = JSON.parse(JSON.stringify(this.marcadores));
    let ruta = mark.filter(r => r.lat === lat && r.lng == lng)[0];
    this.filtro.inputDireccionIntermedio = ruta.infoWindow;
    this.filtro._isWaipoint = true;
    this.mapa.nuevaRuta3Puntos(lat, lng, mark);
  }

}
