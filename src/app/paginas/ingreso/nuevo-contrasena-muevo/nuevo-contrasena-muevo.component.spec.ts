import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoContrasenaMuevoComponent } from './nuevo-contrasena-muevo.component';

describe('NuevoContrasenaMuevoComponent', () => {
  let component: NuevoContrasenaMuevoComponent;
  let fixture: ComponentFixture<NuevoContrasenaMuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoContrasenaMuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoContrasenaMuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
