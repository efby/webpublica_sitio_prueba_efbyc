import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Router } from '@angular/router';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-nuevo-contrasena-muevo',
  templateUrl: './nuevo-contrasena-muevo.component.html',
  styleUrls: ['./nuevo-contrasena-muevo.component.css']
})

export class NuevoContrasenaMuevoComponent implements OnInit {

  _btnSiguiente: boolean = true;
  _leido: boolean = false;
  pass: boolean = false;
  inputIngresoContrasena: string = "";
  inputRepiteContrasena: string = "";
  errorPass: string = "";
  passCorrecta: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private maquinaIngreso: MaquinaIngreso, private utils: UtilitariosService, private router: Router) { }

  ngOnInit() {
  }

  iniSession() {
    this.router.navigate([Paginas.IngresoInicioPage, "L"]);
  }

  validaMaxInput(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    if (item.value === "") {
      this.pass = false;
    } else {
      this.pass = true;
    }
    this.appMatchContrasena()
  }

  validateAll() {
    if (this._leido && this.inputIngresoContrasena !== "" && this.inputRepiteContrasena !== "" && this.errorPass == "") {
      this._btnSiguiente = false;
    } else {
      this._btnSiguiente = true;
    }
  }

  appMatchContrasena() {
    let passA = document.getElementById("dvIngresoContrasena");
    let passR = document.getElementById("inputRepiteContrasena");

    if (this.inputIngresoContrasena && this.inputIngresoContrasena.length < 4) {
      this.utils.pintaValidacion(passA, "");
      this.utils.pintaValidacion(passR, "");
      this.errorPass = "El PIN debe tener 4 dígitos";
      // this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "Error de Ingreso", textoMensaje: "La contraseña debe tener 4 Caracteres" });
      this._btnSiguiente = true;
      this.passCorrecta = false;
    } else {
      if (this.inputIngresoContrasena !== this.inputRepiteContrasena) {
        this.utils.pintaValidacion(passA, "");
        this.utils.pintaValidacion(passR, "");
        this.errorPass = "El PIN no coincide";
        this._btnSiguiente = true;
        this.passCorrecta = false;
      } else {
        this.utils.pintaValidacion(passA, "123");
        this.utils.pintaValidacion(passR, "123");
        this.errorPass = "";
        this.passCorrecta = true;
        this.validateAll();
      }
    }
  }

  ingresoCreaContrasena() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_BOTON_CREAR,
      datos: {
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        usuarioId: this.variablesApi.ingreso.usuario.usuarioId,
        nombre: this.variablesApi.ingreso.usuario.nombre,
        apellido: this.variablesApi.ingreso.usuario.apellido,
        mail: this.variablesApi.ingreso.usuario.mail,
        password: this.inputIngresoContrasena,
        numeroDocumento: this.variablesApi.ingreso.usuario.numeroDocumento
      }
    };
    this.maquinaIngreso.ingresoCreaContrasena(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          if (respuesta.paginaRespuesta === Paginas.ErrorNOK) {
            localStorage.clear();
            this.router.navigate([respuesta.paginaRespuesta]);
          } else {
            this.router.navigate([respuesta.paginaRespuesta]);
          }

        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  reintentar() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_BOTON_CREAR,
      datos: {
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        usuarioId: this.variablesApi.ingreso.usuario.usuarioId,
        nombre: this.variablesApi.ingreso.usuario.nombre,
        apellido: this.variablesApi.ingreso.usuario.apellido,
        mail: this.variablesApi.ingreso.usuario.mail,
        password: this.inputIngresoContrasena,
        numeroDocumento: this.variablesApi.ingreso.usuario.numeroDocumento
      }
    };
    this.maquinaIngreso.ingresoCreaContrasena(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }
    this.maquinaIngreso.ingresoCreaContrasena(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  modalActions(action) {
    switch (action) {
      case OpcionesModal.REINTENTAR:
        this.reintentar();
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }


}
