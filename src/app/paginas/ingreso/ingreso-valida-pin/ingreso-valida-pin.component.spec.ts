import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoValidaPinComponent } from './ingreso-valida-pin.component';

describe('IngresoValidaPinComponent', () => {
  let component: IngresoValidaPinComponent;
  let fixture: ComponentFixture<IngresoValidaPinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoValidaPinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoValidaPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
