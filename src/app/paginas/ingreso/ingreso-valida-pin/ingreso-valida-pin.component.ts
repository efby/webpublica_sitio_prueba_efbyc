import { Component, OnInit } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-ingreso-valida-pin',
  templateUrl: './ingreso-valida-pin.component.html',
  styleUrls: ['./ingreso-valida-pin.component.css']
})
export class IngresoValidaPinComponent implements OnInit {

  inputPinValor: string;

  constructor(private ingresoFunciones: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  validaMaxInput(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
  }

  mostrarAnimacion() {
    this.utils.pintaValidacion(document.getElementById("inputPinValor"), "");
  }

  validaPin() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputPinValor !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_VALIDAR,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          usuarioId: this.variablesApi.ingreso.usuario.usuarioId,
          pinValor: this.inputPinValor
        }
      };
      this.ingresoFunciones.ingresoValidaPin(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputPinValor"), this.inputPinValor);
      this.utils.hideSpinner("Interno");
    }
  }

  reintentar() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputPinValor !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_VALIDAR,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId != null ? this.variablesApi.ingreso.usuario.telefonoId : "56962346426",
          usuarioId: this.variablesApi.ingreso.usuario.usuarioId != null ? this.variablesApi.ingreso.usuario.usuarioId : "157603981",
          pinValor: this.inputPinValor
        }
      };
      this.ingresoFunciones.ingresoValidaPin(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputPinValor"), this.inputPinValor);
      this.utils.hideSpinner("Interno");
    }
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }
    this.ingresoFunciones.ingresoValidaPin(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  atras() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_ATRAS,
      datos: ""
    }
    this.ingresoFunciones.ingresoValidaPin(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  modalActions(action) {
    switch (action) {
      case OpcionesModal.REINTENTAR:
        this.reintentar();
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }


}
