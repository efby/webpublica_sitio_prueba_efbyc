import { Component, OnInit } from '@angular/core';
import { VariablesGenerales, DatosUsuario } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-muevo-recupera-pin',
  templateUrl: './muevo-recupera-pin.component.html',
  styleUrls: ['./muevo-recupera-pin.component.css']
})
export class MuevoRecuperaPinComponent implements OnInit {
  usuario: DatosUsuario;

  _btnSiguiente: boolean = true;
  _muestraTelefono: boolean = true;
  _validado: boolean = false;
  pass: boolean = false;
  passR: boolean = false;
  _full: boolean = false;
  recuperaCorreo: boolean = true;
  recuperaRut: boolean = false;
  solicitaRut: boolean = true;
  btnSolicitarRut: boolean = true;

  errorTelefono: string = "";
  errorCorreo: string = "";
  errorPass: string = "";
  errorRut: string = "";

  nombreUsuario: string = "";
  inputRut: string = "";

  inputNroTelefono: string = "";
  inputValidaCodigo1: string = "";
  inputValidaCodigo2: string = "";
  inputValidaCodigo3: string = "";

  inputIngresoContrasena: string = "";
  inputRepiteContrasena: string = "";
  passCorrecta: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private utils: UtilitariosService, private maquinaIngreso: MaquinaIngreso, private router: Router) { }

  ngOnInit() {
    this.usuario = this.variablesApi.ingreso.usuario;
  }

  cambiaVisiblePass() {
    if (this.inputIngresoContrasena !== "") {
      this.pass = !this.pass;
      this.passR = this.pass;
    }
  }

  nvaCuenta() {
    this.router.navigate([Paginas.MuevoNuevoAdministrador]);
  }

  validaMaxInput(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }

    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    if (item.id !== "inputNroTelefono") {
      if (item.value === "") {
        this.pass = false;
        this.passR = false;
      } else {
        switch (item.id) {
          case 'inputIngresoContrasena':
            this.pass = true;
            break;
          case 'inputRepiteContrasena':
            this.passR = true;
            this.appMatchContrasena();
          default:
            break;
        }
      }
    }
  }

  appMatchContrasena() {
    let passA = document.getElementById("dvIngresoContrasena");
    let passR = document.getElementById("inputRepiteContrasena");

    if (this.inputIngresoContrasena.length < 4) {
      this.utils.pintaValidacion(passA, "");
      this.utils.pintaValidacion(passR, "");
      this.errorPass = "El PIN debe tener 4 digitos";
      // this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "Error de Ingreso", textoMensaje: "La contraseña debe tener 4 Caracteres" });
      this.passCorrecta = false;
    } else {
      if (this.inputIngresoContrasena !== this.inputRepiteContrasena) {
        this.utils.pintaValidacion(passA, "");
        this.utils.pintaValidacion(passR, "");
        this.errorPass = "¡El PIN no coincide!";
        this.passCorrecta = false;
      } else {
        this.utils.pintaValidacion(passA, "123");
        this.utils.pintaValidacion(passR, "123");
        this.errorPass = "";
        this.passCorrecta = true;
      }
    }
  }


  validaMaxInputRut(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/g, '').replace(/(\..*)\./g, '$1');

    if (this.inputRut == "") {
      this.utils.pintaValidacion(document.getElementById("inputRut"), '');
      this.errorRut = "Campo Requerido.";
    } else if (this.inputRut.length < 7) {
      this.utils.pintaValidacion(document.getElementById("inputRut"), '');
      this.errorRut = "RUT no válido.";
    } else if (!this.utils.validateRut(this.inputRut)) {
      this.utils.pintaValidacion(document.getElementById("inputRut"), '');
      this.errorRut = "RUT no válido.";
    } else {
      this.utils.pintaValidacion(document.getElementById("inputRut"), this.inputRut);
      this.btnSolicitarRut = false;
    }

  }

  validaMaxInputCorreo(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }

    if (item.value.length == item.maxLength) {
      switch (item.id) {
        case "inputValidaCodigo1":
          document.getElementById("inputValidaCodigo2").focus();
          break;
        case "inputValidaCodigo2":
          document.getElementById("inputValidaCodigo3").focus();
          break;
        case "inputValidaCodigo3":
          this.ingresoCodigoCorreo();
          break;
        default:
          break;
      }
    } else if (item.value.length == 0) {
      switch (item.id) {
        case "inputValidaCodigo3":
          document.getElementById("inputValidaCodigo2").focus();
          break;
        case "inputValidaCodigo2":
          document.getElementById("inputValidaCodigo1").focus();
          break;
        default:
          break;
      }
    }
  }

  ingresoCodigoCorreo() {
    this._btnSiguiente = false;
  }

  validaCodigoRecuperacion(): boolean {
    if (this.passCorrecta && this.inputValidaCodigo1 != "" && this.inputValidaCodigo2 != "" && this.inputValidaCodigo3 != "") {
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), '123');
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), '123');
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), '123');
      this.errorCorreo = "";
      return true;
    } else {
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), this.inputValidaCodigo1);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), this.inputValidaCodigo2);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), this.inputValidaCodigo3);
      this.errorCorreo = "Debe ingresar un codigo válido";
      return false;
    }
  }

  ingresoContrasenaPost() {
    if (this.validaCodigoRecuperacion()) {
      this.utils.showSpinner("Interno");
      let actualUrl = this.router.url.substr(1);

      let action: RequestTipo = {
        accion: AccionTipo.ACCION_CLICK,
        datos: {
          codigo: this.inputValidaCodigo1 + this.inputValidaCodigo2 + this.inputValidaCodigo3,
          password: this.inputIngresoContrasena
        }
      };
      this.maquinaIngreso.IngresoTokenPassPost(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          if (actualUrl !== respuesta.paginaRespuesta) {
            if (respuesta.paginaRespuesta !== Paginas.MuevoNuevoAdministrador) {
              this.router.navigate([respuesta.paginaRespuesta]);
            }
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response, Paginas.IngresoTelefonoId, action);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.errorCorreo = respuesta.mensajeSalida.textoMensaje;
          // this.utils.msgPlain(respuesta.mensajeSalida);
          this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), '');
          this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), '');
          this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), '');
        }
      });
    }
  }

  muestraRut() {
    this.recuperaCorreo = false;
    this.recuperaRut = true;
    this._btnSiguiente = true;
    this.inputIngresoContrasena = "";
    this.inputRepiteContrasena = "";
    this.passCorrecta = false;
    this.pass = false;
    this.passR = false;
    this.inputValidaCodigo1 = "";
    this.inputValidaCodigo2 = "";
    this.inputValidaCodigo3 = "";
  }


  recuperarByRut() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
        rut: this.inputRut.replace(/\./g, '').replace(/\-/g, ''),
        numeroSerieRut: ''
      }
    };

    this.maquinaIngreso.IngresoTokenTelefonoPost(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          if (respuesta.paginaRespuesta !== Paginas.ErrorNOK) {
            this.router.navigate([respuesta.paginaRespuesta]);
          } else {
            this.utils.pintaValidacion(document.getElementById("inputRut"), '');
            this.errorRut = respuesta.mensajeSalida.textoMensaje;
          }
        }
        else {
          this.solicitaRut = false;
        }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  initSession() {
    this.router.navigate(["/"]);
  }

  solicitarRecuperaMail() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
      }
    };

    this.maquinaIngreso.IngresoTokenMailPost(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          if (respuesta.paginaRespuesta !== Paginas.ErrorNOK) {
            // this.router.navigate([respuesta.paginaRespuesta]);
          } else {
            this.utils.msgPlain(respuesta.mensajeSalida);
          }
        }
        else {
          this.ngOnInit();
        }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

  solicitarRecuperaSMS() {
    this.recuperarByRut();
  }

  recibirLlamada() {

  }

  modalActions(accion, origen, action) {
    switch (accion) {
      case OpcionesModal.REINTENTAR:
        // this.reintentar(action, origen);
        break;
      case OpcionesModal.SALIR:
        // this.salir();
        break;
      default:
        break;
    }
  }

}
