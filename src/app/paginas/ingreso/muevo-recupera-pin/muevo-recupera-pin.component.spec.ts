import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuevoRecuperaPinComponent } from './muevo-recupera-pin.component';

describe('MuevoRecuperaPinComponent', () => {
  let component: MuevoRecuperaPinComponent;
  let fixture: ComponentFixture<MuevoRecuperaPinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuevoRecuperaPinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuevoRecuperaPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
