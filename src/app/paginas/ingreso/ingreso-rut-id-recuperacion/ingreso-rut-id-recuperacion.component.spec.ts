import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoRutIdRecuperacionComponent } from './ingreso-rut-id-recuperacion.component';

describe('IngresoRutIdRecuperacionComponent', () => {
  let component: IngresoRutIdRecuperacionComponent;
  let fixture: ComponentFixture<IngresoRutIdRecuperacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoRutIdRecuperacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoRutIdRecuperacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
