import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RequestTipo, ResponseTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal, AnimacionTipo } from 'src/app/globales/enumeradores';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-ingreso-rut-id',
  templateUrl: './ingreso-rut-id.component.html',
  styleUrls: ['./ingreso-rut-id.component.css']
})
export class IngresoRutIdComponent implements OnInit {
  inputRut: string = "";
  inputCdDocumento: string = "";
  closeResult: string;
  modal: any = {
    title: "¿Donde encontrar su número de documento?"
  };
  isVisible: boolean = false;

  constructor(private ingresoFunciones: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales) {
  }

  ngOnInit() {
    this.inputRut = this.variablesApi.ingreso.usuario.usuarioId;
    this.inputCdDocumento = this.variablesApi.ingreso.usuario.numeroDocumento;
  }

  validaMaxInput(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    if (!this.utils.validateRut(item.value)) {
      this.utils.pintaValidacion(document.getElementById("inputRut"), "");
    } else {
      this.utils.pintaValidacion(document.getElementById("inputRut"), this.inputRut);
    }
  }



  mostrarAnimacion() {
    if (this.variablesApi.ingreso.animacionTipo === AnimacionTipo.USUARIO_TELEFONO_NO_ASOCIADO) {
      this.isVisible = true;

    } else if (this.variablesApi.ingreso.animacionTipo === AnimacionTipo.NUMERO_DOCUMENTO_INCORRECTO) {
      this.utils.pintaValidacion(document.getElementById("inputCdDocumento"), "");
    }
  }

  ingresoRutIdRegistro() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputRut !== "", this.inputCdDocumento !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_SIGUIENTE,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          usuarioId: this.inputRut.replace(/\./g, '').replace(/\-/g, ''),
          numeroDocumento: this.inputCdDocumento.replace(/\./g, '').replace(/\-/g, ''),
        }
      };
      this.ingresoFunciones.ingresoRutIdRegistro(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          console.log(respuesta);
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.mostrarAnimacion();
          this.utils.hideSpinner("Interno");
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputRut"), this.inputRut);
      this.utils.pintaValidacion(document.getElementById("inputCdDocumento"), this.inputCdDocumento);
      this.utils.hideSpinner("Interno");
    }
  }

  ingresoMostrarNroSerie() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_BOTON_CONSULTAR_NUMERO_SERIE,
      datos: {
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        usuarioId: this.inputRut.replace(/\./g, '').replace(/\-/g, ''),
        numeroDocumento: this.inputCdDocumento.replace(/\./g, '').replace(/\-/g, ''),
      }
    };
    this.ingresoFunciones.ingresoRutIdRegistro(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        console.log(respuesta);
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  reintentar() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputRut !== "", this.inputCdDocumento !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_SIGUIENTE,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          usuarioId: this.inputRut.replace(/\./g, '').replace(/\-/g, ''),
          numeroDocumento: this.inputCdDocumento.replace(/\./g, '').replace(/\-/g, ''),
        }
      };
      this.ingresoFunciones.ingresoRutIdRegistro(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          console.log(respuesta);
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.mostrarAnimacion();
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputRut"), this.inputRut);
      this.utils.pintaValidacion(document.getElementById("inputCdDocumento"), this.inputCdDocumento);
      this.utils.hideSpinner("Interno");
    }
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }
    this.ingresoFunciones.ingresoRutIdRegistro(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        console.log(respuesta);
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.mostrarAnimacion();
      }
    });
  }

  recuperarCuenta() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_BOTON_NO_PUEDO_ACCEDER_A_MI_CUENTA,
      datos: ""
    }
    this.ingresoFunciones.ingresoRutIdRegistro(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        console.log(respuesta);
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.mostrarAnimacion();
      }
    });
  }

  atras() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_ATRAS,
      datos: ""
    }
    this.ingresoFunciones.ingresoRutIdRegistro(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  modalActions(action) {
    switch (action) {
      case OpcionesModal.REINTENTAR:
        this.reintentar();
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

}
