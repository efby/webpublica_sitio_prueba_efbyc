import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoRutIdComponent } from './ingreso-rut-id.component';

describe('IngresoRutIdComponent', () => {
  let component: IngresoRutIdComponent;
  let fixture: ComponentFixture<IngresoRutIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoRutIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoRutIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
