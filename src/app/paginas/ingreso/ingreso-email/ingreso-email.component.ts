import { Component, OnInit } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router } from '@angular/router';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-ingreso-email',
  templateUrl: './ingreso-email.component.html',
  styleUrls: ['./ingreso-email.component.css']
})
export class IngresoEmailComponent implements OnInit {
  inputEmail: string;

  constructor(private ingresoFunciones: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
    this.inputEmail = this.variablesApi.ingreso.usuario.mail;
  }

  mostrarAnimacion() {
    this.utils.pintaValidacion(document.getElementById("inputEmail"), "");
  }

  ingresoMail() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputEmail !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_SIGUIENTE,
        datos: {
          email: this.inputEmail
        }
      };
      this.ingresoFunciones.ingresoMail(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputEmail"), this.inputEmail);
      this.utils.hideSpinner("Interno");
    }
  }

  reintentar() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputEmail !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_SIGUIENTE,
        datos: {
          email: this.inputEmail
        }
      };
      this.ingresoFunciones.ingresoMail(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputEmail"), this.inputEmail);
      this.utils.hideSpinner("Interno");
    }
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_REINTENTAR,
      datos: {
        email: this.inputEmail
      }
    };
    this.ingresoFunciones.ingresoMail(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  atras() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_ATRAS,
      datos: ""
    }
    this.ingresoFunciones.ingresoMail(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }


  modalActions(action) {
    switch (action) {
      case OpcionesModal.REINTENTAR:
        this.reintentar();
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

}
