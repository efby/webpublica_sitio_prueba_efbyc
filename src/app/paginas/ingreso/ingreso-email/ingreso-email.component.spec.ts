import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoEmailComponent } from './ingreso-email.component';

describe('IngresoEmailComponent', () => {
  let component: IngresoEmailComponent;
  let fixture: ComponentFixture<IngresoEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
