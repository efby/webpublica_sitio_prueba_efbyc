import { Component, OnInit } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { VariablesGenerales, Ingreso } from 'src/app/globales/variables-generales';
import { Paginas } from 'src/app/globales/paginas';
import { retry } from 'rxjs/operators';

@Component({
    selector: 'app-ingreso-cargando',
    templateUrl: './ingreso-cargando.component.html',
    styleUrls: ['./ingreso-cargando.component.css']
})
export class IngresoCargandoComponent implements OnInit {
    
    reTry: number = 0;
    navigatePage: string = "";

    constructor(private ingresoFunciones: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private route: ActivatedRoute) {
        let _navigate = this.route.snapshot.paramMap.get('navigate');
        switch (_navigate) {
            case "I":
                this.navigatePage = Paginas.MuevoNuevoAdministrador;
                this.ingresoFunciones.CloseSession();
            break;
            case "L":
                this.navigatePage = Paginas.MuevoNuevoLogin;
            break;
            default:
                //this.navigatePage = Paginas.ErrorSA;
                this.navigatePage = Paginas.MuevoNuevoAdministrador;
                
            break;
        }
    }
    
    ngOnInit() {
        if (this.reTry < 3) {
            this.reTry++;
            this.inicio();
        } else {
            this.reTry = 0;
        }
    }
    
    mostrarAnimacion() {
        
    }
    
    modalActions(action) {
        switch (action) {
            case OpcionesModal.REINTENTAR:
            this.reintentar();
            break;
            case OpcionesModal.SALIR:
            this.salir();
            break;
            default:
            break;
        }
    }
    
    inicio() {
        this.utils.showSpinner("Inicial");
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_INICIO,
            datos: ""
        }

        this.ingresoFunciones.inicioMuevo(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                console.log('RESPUESTA: ' + respuesta.paginaRespuesta);
                this.utils.hideSpinner("Inicial");
                if (actualUrl !== respuesta.paginaRespuesta) {
                    if (respuesta.paginaRespuesta === Paginas.ErrorNOK) {
                        localStorage.clear();
                        this.variablesApi.ingreso.usuario.nombre = "";
                        this.router.navigate([respuesta.paginaRespuesta]);
                    } 
                    else if (respuesta.paginaRespuesta === Paginas.MuevoNuevoLogin){ // || respuesta.paginaRespuesta === Paginas.MuevoNuevoAdministrador) {
                        let valor: number = respuesta.data && respuesta.data.action && respuesta.data.action == "token_revalidated" ? 1 : 0;
                        //let valor: number = this.variablesApi.ingreso.usuario.nombre === "" ? 0 : 1;
                        if (this.navigatePage == Paginas.MuevoNuevoLogin) {
                            this.router.navigate([Paginas.MuevoNuevoLogin, valor]);
                        }
                        else if(this.navigatePage != Paginas.MuevoNuevoLogin && valor == 1){
                            //para tokens caducados
                            this.router.navigate([Paginas.MuevoNuevoLogin, valor]);
                        }
                        // else if(this.navigatePage == Paginas.MuevoNuevoAdministrador)
                        // {
                        //     this.ingresoFunciones.CloseSession();
                        //     this.router.navigate([this.navigatePage]);
                        // }
                        else {
                            this.router.navigate([this.navigatePage]);
                        }
                    }
                    // else if(respuesta.paginaRespuesta == Paginas.MuevoNuevoAdministrador){
                    //     this.router.navigate([Paginas.IngresoInicioPage, 'I']);
                    // }
                    else if (respuesta.paginaRespuesta === Paginas.ErrorSA) {
                        localStorage.clear();
                        this.variablesApi.ingreso = new Ingreso();
                        this.ngOnInit();
                        //this.router.navigate([Paginas.IngresoInicioPage, 'L']);
                    } 
                    else {
                        this.router.navigate([respuesta.paginaRespuesta]);
                    }
                } else {
                    this.ngOnInit();
                }
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                this.utils.hideSpinner("Inicial");
                this.utils.openModal(respuesta.modalSalida).then(response => {
                    this.modalActions(response);
                });
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
                this.utils.hideSpinner("Interno");
                this.utils.msgPlain(respuesta.mensajeSalida);
                localStorage.clear();
                this.ngOnInit();
            }
        }, error => {
            console.log(error);
        });
    }
    
    reintentar() {
        this.utils.showSpinner("Inicial");
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_INICIO,
            datos: ""
        }
        this.ingresoFunciones.inicioMuevo(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.hideSpinner("Inicial");
                if (actualUrl !== respuesta.paginaRespuesta) {
                    this.router.navigate([respuesta.paginaRespuesta]);
                } else {
                    this.ngOnInit();
                }
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                this.utils.hideSpinner("Inicial");
                this.utils.openModal(respuesta.modalSalida).then(response => {
                    this.modalActions(response);
                });
            }
        });
    }
    
    salir() {
        this.utils.showSpinner("Inicial");
        let actualUrl = this.router.url.substr(1);
        let action: RequestTipo = {
            accion: AccionTipo.ACCION_SALIR,
            datos: ""
        }
        this.ingresoFunciones.inicioMuevo(action).then(respuesta => {
            if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
                this.utils.hideSpinner("Inicial");
                if (actualUrl !== respuesta.paginaRespuesta) {
                    this.router.navigate([respuesta.paginaRespuesta]);
                } else {
                    this.ngOnInit();
                }
            } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
                this.utils.hideSpinner("Inicial");
                this.utils.openModal(respuesta.modalSalida).then(response => {
                    this.modalActions(response);
                });
            }
        });
    }
    
}
