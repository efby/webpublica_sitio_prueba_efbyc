import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoCargandoComponent } from './ingreso-cargando.component';

describe('IngresoCargandoComponent', () => {
  let component: IngresoCargandoComponent;
  let fixture: ComponentFixture<IngresoCargandoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoCargandoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoCargandoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
