import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-ingreso-smsvalor',
  templateUrl: './ingreso-smsvalor.component.html',
  styleUrls: ['./ingreso-smsvalor.component.css']
})
export class IngresoSMSValorComponent implements OnInit {
  inputValidaCodigo1: string;
  inputValidaCodigo2: string;
  inputValidaCodigo3: string;

  constructor(private ingresoFunciones: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales) {
  }

  ngOnInit() {
    let auxiliar = this.variablesApi.ingreso.usuario.smsValor;
    this.inputValidaCodigo1 = auxiliar.substring(0, 2);
    this.inputValidaCodigo2 = auxiliar.substring(2, 2);
    this.inputValidaCodigo3 = auxiliar.substring(4, 2);
  }

  validaMaxInput(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');

    if (item.value.length == item.maxLength) {
      switch (item.id) {
        case "inputValidaCodigo1":
          document.getElementById("inputValidaCodigo2").focus();
          break;
        case "inputValidaCodigo2":
          document.getElementById("inputValidaCodigo3").focus();
          break;
        default:
          break;
      }
    } else if (item.value.length == 0) {
      switch (item.id) {
        case "inputValidaCodigo3":
          document.getElementById("inputValidaCodigo2").focus();
          break;
        case "inputValidaCodigo2":
          document.getElementById("inputValidaCodigo1").focus();
          break;
        default:
          break;
      }
    }
  }

  mostrarAnimacion() {
    this.inputValidaCodigo1 = "";
    this.inputValidaCodigo2 = "";
    this.inputValidaCodigo3 = "";
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), this.inputValidaCodigo1);
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), this.inputValidaCodigo2);
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), this.inputValidaCodigo3);
  }

  ingresoCodigoSMS() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputValidaCodigo1 !== "" && this.inputValidaCodigo2 !== "" && this.inputValidaCodigo3 !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_VALIDAR,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          smsToken: this.variablesApi.ingreso.usuario.smsToken,
          smsValor: this.inputValidaCodigo1 + this.inputValidaCodigo2 + this.inputValidaCodigo3
        }
      };
      this.ingresoFunciones.ingresoCodigoSMS(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.mostrarAnimacion();
        }
      });
    } else {
      this.utils.hideSpinner("Interno");
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), this.inputValidaCodigo1);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), this.inputValidaCodigo2);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), this.inputValidaCodigo3);
    }
  }

  reenviarSMS() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_REENVIAR_CODIGO,
      datos: {
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        smsToken: this.variablesApi.ingreso.usuario.smsToken
      }
    };
    this.ingresoFunciones.ingresoCodigoSMS(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.mostrarAnimacion();
      }
    });
  }

  recibirLlamada() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_RECIBIR_LLAMADA,
      datos: {
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        smsToken: this.variablesApi.ingreso.usuario.smsToken
      }
    };
    this.ingresoFunciones.ingresoCodigoSMS(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.mostrarAnimacion();
      }
    });
  }

  reintentar() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputValidaCodigo1 !== "" && this.inputValidaCodigo2 !== "" && this.inputValidaCodigo3 !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_VALIDAR,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          smsToken: this.variablesApi.ingreso.usuario.smsToken,
          smsValor: this.inputValidaCodigo1 + this.inputValidaCodigo2 + this.inputValidaCodigo3
        }
      };
      this.ingresoFunciones.ingresoCodigoSMS(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.hideSpinner("Interno");
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), this.inputValidaCodigo1);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), this.inputValidaCodigo2);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), this.inputValidaCodigo3);
    }
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }
    this.ingresoFunciones.ingresoCodigoSMS(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  atras() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_ATRAS,
      datos: ""
    }
    this.ingresoFunciones.ingresoCodigoSMS(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }


  modalActions(action) {
    switch (action) {
      case OpcionesModal.REINTENTAR:
        this.reintentar();
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

}
