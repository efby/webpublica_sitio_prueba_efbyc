import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoSMSValorComponent } from './ingreso-smsvalor.component';

describe('IngresoSMSValorComponent', () => {
  let component: IngresoSMSValorComponent;
  let fixture: ComponentFixture<IngresoSMSValorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoSMSValorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoSMSValorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
