import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoCreaContrasenaComponent } from './ingreso-crea-contrasena.component';

describe('IngresoCreaContrasenaComponent', () => {
  let component: IngresoCreaContrasenaComponent;
  let fixture: ComponentFixture<IngresoCreaContrasenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoCreaContrasenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoCreaContrasenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
