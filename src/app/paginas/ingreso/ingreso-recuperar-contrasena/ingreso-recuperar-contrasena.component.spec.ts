import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoRecuperarContrasenaComponent } from './ingreso-recuperar-contrasena.component';

describe('IngresoRecuperarContrasenaComponent', () => {
  let component: IngresoRecuperarContrasenaComponent;
  let fixture: ComponentFixture<IngresoRecuperarContrasenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoRecuperarContrasenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoRecuperarContrasenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
