import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoModificaContrasenaComponent } from './ingreso-modifica-contrasena.component';

describe('IngresoModificaContrasenaComponent', () => {
  let component: IngresoModificaContrasenaComponent;
  let fixture: ComponentFixture<IngresoModificaContrasenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoModificaContrasenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoModificaContrasenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
