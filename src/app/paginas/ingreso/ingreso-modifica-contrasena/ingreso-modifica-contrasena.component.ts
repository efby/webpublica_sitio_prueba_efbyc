import { Component, OnInit } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-ingreso-modifica-contrasena',
  templateUrl: './ingreso-modifica-contrasena.component.html',
  styleUrls: ['./ingreso-modifica-contrasena.component.css']
})
export class IngresoModificaContrasenaComponent implements OnInit {

  inputIngresoContrasena: string;
  inputRepiteContrasena: string;

  constructor(private ingresoFunciones: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  appMatchContrasena() {
    let passA = document.getElementById("inputIngresoContrasena");
    let passR = document.getElementById("inputRepiteContrasena");

    if (this.inputIngresoContrasena.length < 8) {
      passA.style.border = "1px solid #FF0000";
      this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "Error de Ingreso", textoMensaje: "La contraseña debe tener al menos 8 Caracteres" });
    } else {
      if (this.inputIngresoContrasena !== this.inputRepiteContrasena) {
        this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "Error de Ingreso", textoMensaje: "Las contraseñas deben ser iguales" });
        passA.style.border = "1px solid #FF0000";
        passR.style.border = "1px solid #FF0000";
      } else {
        passA.style.border = "none";
        passR.style.border = "none";
        passA.style.borderBottom = "1px solid #000000";
        passR.style.borderBottom = "1px solid #000000";
      }
    }
  }

  mostrarAnimacion() {
    this.utils.pintaValidacion(document.getElementById("inputIngresoContrasena"), "");
    this.utils.pintaValidacion(document.getElementById("inputRepiteContrasena"), "");
  }

  ingresoModificaContrasena() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputIngresoContrasena !== "" && this.inputRepiteContrasena !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_CREAR,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          usuarioId: this.variablesApi.ingreso.usuario.usuarioId,
          password: this.inputIngresoContrasena,
          numeroDocumento: this.variablesApi.ingreso.usuario.numeroDocumento
        }
      };
      this.ingresoFunciones.ingresoModificaContrasena(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputIngresoContrasena"), this.inputIngresoContrasena);
      this.utils.pintaValidacion(document.getElementById("inputRepiteContrasena"), this.inputRepiteContrasena);
      this.utils.hideSpinner("Interno");
    }
  }

  reintentar() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputIngresoContrasena !== "" && this.inputRepiteContrasena !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_CREAR,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          usuarioId: this.variablesApi.ingreso.usuario.usuarioId,
          password: this.inputIngresoContrasena,
          numeroDocumento: this.variablesApi.ingreso.usuario.numeroDocumento
        }
      };
      this.ingresoFunciones.ingresoModificaContrasena(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputIngresoContrasena"), this.inputIngresoContrasena);
      this.utils.pintaValidacion(document.getElementById("inputRepiteContrasena"), this.inputRepiteContrasena);
      this.utils.hideSpinner("Interno");
    }
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }
    this.ingresoFunciones.ingresoModificaContrasena(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  atras() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_ATRAS,
      datos: ""
    }
    this.ingresoFunciones.ingresoModificaContrasena(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  modalActions(action) {
    switch (action) {
      case OpcionesModal.REINTENTAR:
        this.reintentar();
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

}
