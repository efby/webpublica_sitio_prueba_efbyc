import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { PageBase } from '../../home-muevo/pageBase';

@Component({
  selector: 'app-nuevo-mail-muevo',
  templateUrl: './nuevo-mail-muevo.component.html',
  styleUrls: ['./nuevo-mail-muevo.component.css']
})

export class NuevoMailMuevoComponent extends PageBase implements OnInit {
  _btnSiguiente: boolean = true;
  inputMail: string = "";
  noPuedoAcceder: boolean = false;
  errorCorreo: string = "";

  constructor(private maquinaIngreso: MaquinaIngreso,public cdr:ChangeDetectorRef, public router: Router, public utils: UtilitariosService, private variablesApi: VariablesGenerales) { 
    super(cdr,utils,router)
  }

  ngOnInit() {
  }

  iniSession() {
    this.router.navigate([Paginas.IngresoInicioPage, "L"]);
  }
  irASoporte() {
    this.router.navigate([Paginas.MuevoNuevoSoporte]);
  }

  validateEmail() {
    console.log('validando', this.inputMail)
            //"^(([^<>()[\\]\\\\.,;:\\s@\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))\$";
            //RegExp regExp = new RegExp(p);

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(this.inputMail)) {
      this.utils.pintaValidacion(document.getElementById("inputMail"), this.inputMail)
      this._btnSiguiente = false;
      this.errorCorreo = "";
    } else {
      this.utils.pintaValidacion(document.getElementById("inputMail"), "");
      this.errorCorreo = "Email no válido.";
      document.getElementById("inputMail").focus();
      this._btnSiguiente = true;
    }
  }

  validaMaxInputMail(ingreso: any) {
    this.utils.validaMaxInputMail(ingreso);
    this.validateEmail();
  }

  ingresoAvanzar() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_BOTON_SIGUIENTE,
      datos: {
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
        nombre: this.variablesApi.ingreso.usuario.nombre,
        apellido: this.variablesApi.ingreso.usuario.apellido,
        usuarioId: this.variablesApi.ingreso.usuario.usuarioId,
        numeroDocumento: this.variablesApi.ingreso.usuario.numeroDocumento,
        mail: this.inputMail,
      }
    };

    this.maquinaIngreso.ingresoMail(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          if (respuesta.paginaRespuesta === Paginas.MuevoNuevoLogin) {
            this.router.navigate([respuesta.paginaRespuesta, 1]);
          } else {
            if(respuesta.paginaRespuesta === Paginas.MuevoNuevoContrasena){
              this.variablesApi.ingreso.esFlujoCreacion = true;
            }
            this.router.navigate([respuesta.paginaRespuesta]);
          }
        }
        else {
          this._btnSiguiente = true;
          this.errorCorreo = respuesta.mensajeSalida.textoMensaje;
          this.utils.pintaValidacion(document.getElementById("inputMail"), "");
          this.noPuedoAcceder = true;
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, Paginas.IngresoEmail, action);
        });
      }
    });
  }

  reintentar(action, origen) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    this.maquinaIngreso.reintentarIngreso(action, origen).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, origen, action);
        });
      }
    });
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }

    this.maquinaIngreso.ingresoTelefonoId(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, Paginas.ErrorSC, action);
        });
      }
    });
  }


  modalActions(accion, origen, action) {
    switch (accion) {
      case OpcionesModal.REINTENTAR:
        this.reintentar(action, origen);
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

}
