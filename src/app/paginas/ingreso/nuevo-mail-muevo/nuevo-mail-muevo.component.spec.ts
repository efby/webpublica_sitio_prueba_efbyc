import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoMailMuevoComponent } from './nuevo-mail-muevo.component';

describe('NuevoMailMuevoComponent', () => {
  let component: NuevoMailMuevoComponent;
  let fixture: ComponentFixture<NuevoMailMuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoMailMuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoMailMuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
