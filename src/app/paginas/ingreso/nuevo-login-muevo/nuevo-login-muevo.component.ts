import { Component, OnInit } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales, Ingreso } from 'src/app/globales/variables-generales';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-nuevo-login-muevo',
  templateUrl: './nuevo-login-muevo.component.html',
  styleUrls: ['./nuevo-login-muevo.component.css']
})
export class NuevoLoginMuevoComponent implements OnInit {
  _btnSiguiente: boolean = true;
  _muestraTelefono: boolean = true;
  _validado: boolean = false;
  pass: boolean = false;
  _full: boolean = false;

  passValida: boolean = true;

  errorTelefono: string = "";
  errorSms: string = "";
  errorPass: string = "";

  nombreUsuario: string = "";

  inputNroTelefono: string = "";
  inputValidaCodigo1: string;
  inputValidaCodigo2: string;
  inputValidaCodigo3: string;

  inputIngresoContrasena: string = "";

  constructor(private maquinaIngreso: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.variablesApi.ingreso.usuario.telefonoId !== "") {
      this.inputNroTelefono = this.variablesApi.ingreso.usuario.telefonoId.substring(3);
    }
    let _valido = parseInt(this.route.snapshot.paramMap.get('valido'));
    if (_valido === 1) {
      this._muestraTelefono = false;
      this._validado = true;
      this._btnSiguiente = false;
      this.nombreUsuario = this.variablesApi.ingreso.usuario.nombre;
      this.nombreUsuario = this.nombreUsuario.charAt(0).toUpperCase() + this.nombreUsuario.slice(1);
    } else {
      this._validado = false;
      this._btnSiguiente = true;
      this._muestraTelefono = true;
    }
  }

  nvaCuenta() {
    this.maquinaIngreso.CloseSession();
    this.router.navigate([Paginas.IngresoInicioPage, "I"]);
  }

  irASoporte() {
    this.router.navigate([Paginas.MuevoNuevoSoporte]);
  }

  cambiaVisiblePass() {
    if (this.inputIngresoContrasena !== "") {
      this.pass = !this.pass;
    }
  }

  validaMaxInputSMS(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');

    if (item.value.length == item.maxLength) {
      switch (item.id) {
        case "inputValidaCodigo1":
          document.getElementById("inputValidaCodigo2").focus();
          break;
        case "inputValidaCodigo2":
          document.getElementById("inputValidaCodigo3").focus();
          break;
        case "inputValidaCodigo3":
          this.ingresoCodigoSMS();
          break;
        default:
          break;
      }
    } else if (item.value.length == 0) {
      switch (item.id) {
        case "inputValidaCodigo3":
          document.getElementById("inputValidaCodigo2").focus();
          break;
        case "inputValidaCodigo2":
          document.getElementById("inputValidaCodigo1").focus();
          break;
        default:
          break;
      }
    }
  }

  appMatchContrasena() {
    if (this.inputIngresoContrasena.length < 4) {
      this.utils.pintaValidacion(document.getElementById("dvinputIngresoContrasena"), "");
      // this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "Error de Ingreso", textoMensaje: "El Pin debe tener 4 dígitos." });
      this.errorPass = "El Pin debe tener 4 dígitos.";
      this._btnSiguiente = false;
      this.passValida = true;
    } else {
      this.utils.pintaValidacion(document.getElementById("dvinputIngresoContrasena"), "123");
      this.passValida = false;
    }
  }

  validaMaxInput(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }

    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    if (item.id !== "inputNroTelefono") {
      if (item.value === "") {
        this.pass = false;
      } else {
        this.pass = true;
      }
    }

    if (item.id == "inputIngresoContrasena") {
      if (item.value.length == 4) {
        this.appMatchContrasena()
      }
    }
  }

  mostrarAnimacion() {
    this.utils.pintaValidacion(document.getElementById("inputNroTelefono"), "");
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "");
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "");
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "");
  }

  modificarTelefono() {
    this._muestraTelefono = true;
    debugger
    this.inputNroTelefono="";
    
  }

  solicitarSMS() {
    debugger
    if (this.inputNroTelefono !== "") {
      if (this.inputNroTelefono.length == 8) {
        this.ingresoTelefonoId();
      } else {
       // this.utils.pintaValidacion(document.getElementById("dvNroTelefono"), "");
        this.errorTelefono = "El número de teléfono debe tener 8 dígitos.";
      }
    } else {
      //this.utils.pintaValidacion(document.getElementById("dvNroTelefono"), this.inputNroTelefono);
      this.errorTelefono = "El número de teléfono debe tener 8 dígitos.";
    }
  }

  recibirLlamada() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_RECIBIR_LLAMADA,
      datos: {
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
      }
    };
    this.maquinaIngreso.ingresoCodigoSMS(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.inputValidaCodigo1 = "";
        this.inputValidaCodigo2 = "";
        this.inputValidaCodigo3 = "";
        if (actualUrl !== respuesta.paginaRespuesta) {
          if (respuesta.paginaRespuesta !== Paginas.MuevoNuevoAdministrador) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, Paginas.IngresoTelefonoId, action);
        });
      }
    });
  }

  ingresoTelefonoId() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputNroTelefono !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_CLICK,
        datos: {
          telefonoId: "569" + this.inputNroTelefono
        }
      };
      this.maquinaIngreso.ingresoTelefonoId(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this._muestraTelefono = false;
          this.utils.hideSpinner("Interno");
          this.inputValidaCodigo1 = "";
          this.inputValidaCodigo2 = "";
          this.inputValidaCodigo3 = "";
          if (actualUrl !== respuesta.paginaRespuesta) {
            if (respuesta.paginaRespuesta !== Paginas.MuevoNuevoAdministrador) {
              this.router.navigate([respuesta.paginaRespuesta]);
            }
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response, Paginas.IngresoTelefonoId, action);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.errorTelefono = respuesta.mensajeSalida.textoMensaje;
          // this.utils.msgPlain(respuesta.mensajeSalida);
          this.utils.pintaValidacion(document.getElementById("dvNroTelefono"), '');
        }
      });
    } else {
      this.utils.hideSpinner("Interno");
      this.utils.pintaValidacion(document.getElementById("dvNroTelefono"), this.inputNroTelefono);
    }
  }

  ingresoCodigoSMS() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputValidaCodigo1 !== "" && this.inputValidaCodigo2 !== "" && this.inputValidaCodigo3 !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_VALIDAR,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          smsToken: this.variablesApi.ingreso.usuario.smsToken,
          smsValor: this.inputValidaCodigo1 + this.inputValidaCodigo2 + this.inputValidaCodigo3
        }
      };
      this.maquinaIngreso.ingresoCodigoSMS(action).then(async respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (respuesta.paginaRespuesta == Paginas.ErrorNOK) {
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "");
            // document.getElementById("msgInicialSms").style.display = 'none';
            this.errorSms = respuesta.mensajeSalida.textoMensaje;
          } else if (actualUrl !== respuesta.paginaRespuesta) {
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "123");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "123");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "123");
            this.errorSms = "";
            if (respuesta.data.validado === 1) {
              this._validado = true;
              this._btnSiguiente = false;
              this.nombreUsuario = this.variablesApi.ingreso.usuario.nombre;
              this.nombreUsuario = this.nombreUsuario.charAt(0).toUpperCase() + this.nombreUsuario.slice(1);
            } else {
              this._validado = false;
              this._btnSiguiente = true;
              this.router.navigate([respuesta.paginaRespuesta, 1]);
            }
          } else {
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "123");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "123");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "123");
            this.errorSms = "";
            this._muestraTelefono = false;
            this._validado = true;
            this._btnSiguiente = false;
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response, Paginas.IngresoSMSValor, action);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.errorSms = respuesta.mensajeSalida.textoMensaje;
          this.mostrarAnimacion();

        }
      });
    } else {
      this.utils.hideSpinner("Interno");
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), this.inputValidaCodigo1);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), this.inputValidaCodigo2);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), this.inputValidaCodigo3);
    }
  }

  ingresoTokenPost() {
    if (!this.passValida) {
      this.utils.showSpinner("Interno");
      let actualUrl = this.router.url.substr(1);
      if (this.inputIngresoContrasena !== "") {
        let action: RequestTipo = {
          accion: AccionTipo.ACCION_CLICK,
          datos: {
            password: this.inputIngresoContrasena
          }
        };
        this.maquinaIngreso.IngresoTokenPost(action).then(respuesta => {
          if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
            this.utils.hideSpinner("Interno");
            if (this.variablesApi.home.fuerzaCambioCuenta) {
              this.utils.msgPlain(respuesta.mensajeSalida);
              // this.ngOnInit();
              this.router.navigate([respuesta.paginaRespuesta]);
            } else if (actualUrl !== respuesta.paginaRespuesta) {
              if (respuesta.paginaRespuesta !== Paginas.ErrorNOK) {
                this.errorPass = "";
                this.router.navigate([respuesta.paginaRespuesta]);
              } else {
                this.utils.pintaValidacion(document.getElementById("dvinputIngresoContrasena"), "");
                this.errorPass = respuesta.mensajeSalida.textoMensaje;
              }
            }
            else {
              this.ngOnInit();
            }
          }
          else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
            this.utils.hideSpinner("Interno");
            if (this.utils.msgPlain({ tipoMensaje: 'confirm', textoMensaje: 'No se ha podido establecer una conexión \n ¿Desea Reintentar?' })) {
              this.ingresoTokenPost();
            } else {
              localStorage.clear();
              this.variablesApi.authorizationToken = "";
              this.variablesApi.equipoSecret = "";
              this.variablesApi.ingreso = new Ingreso()
              this.router.navigate([Paginas.IngresoInicioPage]);
            }
          } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
            this.utils.hideSpinner("Interno");
            this.utils.msgPlain(respuesta.mensajeSalida);
            localStorage.clear();
            this.variablesApi.authorizationToken = "";
            this.variablesApi.equipoSecret = "";
            this.variablesApi.ingreso = new Ingreso()
            this.router.navigate([Paginas.IngresoInicioPage]);
          }
        });
      } else {
        this.utils.hideSpinner("Interno");
        this.utils.pintaValidacion(document.getElementById("dvinputIngresoContrasena"), this.inputIngresoContrasena);
        this.errorPass = "PIN no Válido.";
      }
    }
  }

  reintentar(action, origen) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    this.maquinaIngreso.reintentarIngreso(action, origen).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, origen, action);
        });
      }
    });
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }

    this.maquinaIngreso.ingresoTelefonoId(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, Paginas.ErrorSC, action);
        });
      }
    });
  }

  modalActions(accion, origen, action) {
    switch (accion) {
      case OpcionesModal.REINTENTAR:
        this.reintentar(action, origen);
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

  recuperarPin() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_CLICK,
      datos: {
      }
    };

    this.maquinaIngreso.IngresoTokenMailPost(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          if (respuesta.paginaRespuesta !== Paginas.ErrorNOK) {
            this.router.navigate([respuesta.paginaRespuesta]);
          } else {
            this.utils.msgPlain(respuesta.mensajeSalida);
          }
        }
        else {
          this.ngOnInit();
        }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        // if (this.utils.msgPlain({ tipoMensaje: 'confirm', textoMensaje: 'No se ha podido establecer una conexión \n ¿Desea Reintentar?' })) {
        //   // this.ingresoTokenPost();
        // } else {
        //   localStorage.clear();
        //   this.variablesApi.authorizationToken = "";
        //   this.variablesApi.equipoSecret = "";
        //   this.variablesApi.ingreso = new Ingreso()
        //   this.router.navigate([Paginas.IngresoInicioPage]);
        // }
      } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
        this.utils.hideSpinner("Interno");
        this.utils.msgPlain(respuesta.mensajeSalida);

      }
    });
  }

}
