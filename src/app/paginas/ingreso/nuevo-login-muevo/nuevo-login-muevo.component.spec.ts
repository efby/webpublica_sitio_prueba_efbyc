import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoLoginMuevoComponent } from './nuevo-login-muevo.component';

describe('NuevoLoginMuevoComponent', () => {
  let component: NuevoLoginMuevoComponent;
  let fixture: ComponentFixture<NuevoLoginMuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoLoginMuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoLoginMuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
