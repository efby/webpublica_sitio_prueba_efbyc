import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoConfirmaPinValorComponent } from './ingreso-confirma-pin-valor.component';

describe('IngresoConfirmaPinValorComponent', () => {
  let component: IngresoConfirmaPinValorComponent;
  let fixture: ComponentFixture<IngresoConfirmaPinValorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoConfirmaPinValorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoConfirmaPinValorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
