import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoPinValorComponent } from './ingreso-pin-valor.component';

describe('IngresoPinValorComponent', () => {
  let component: IngresoPinValorComponent;
  let fixture: ComponentFixture<IngresoPinValorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoPinValorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoPinValorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
