import { Component, OnInit } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { VariablesGenerales, Ingreso } from 'src/app/globales/variables-generales';
import { Paginas } from 'src/app/globales/paginas';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';

@Component({
  selector: 'app-nuevo-login-persistente-muevo',
  templateUrl: './nuevo-login-persistente-muevo.component.html',
  styleUrls: ['./nuevo-login-persistente-muevo.component.css']
})
export class NuevoLoginPersistenteMuevoComponent implements OnInit {
  inputNroTelefono: string = "";
  inputIngresoContrasena: string = "";
  pass: boolean = false;
  errorPass: string = "";

  constructor(private maquinaIngreso: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.variablesApi.ingreso.usuario.telefonoId !== "") {
      this.inputNroTelefono = this.variablesApi.ingreso.usuario.telefonoId.substring(2);
    }
  }
  iniSession() {
    this.router.navigate([Paginas.MuevoNuevoLogin]);
  }

  nvaCuenta() {
    this.router.navigate([Paginas.MuevoNuevoAdministrador]);
  }
  cambiaVisiblePass() {
    if (this.inputIngresoContrasena !== "") {
      this.pass = !this.pass;
    }
  }

  validaMaxInput(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    if (item.value === "") {
      this.pass = false;
    } else {
      this.pass = true;
    }
  }

  ingresoTokenPost() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputIngresoContrasena !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_CLICK,
        datos: {
          password: this.inputIngresoContrasena
        }
      };
      this.maquinaIngreso.IngresoTokenPost(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            if (respuesta.paginaRespuesta !== Paginas.ErrorNOK) {
              this.errorPass = "";
              this.router.navigate([respuesta.paginaRespuesta]);
            } else {
              this.utils.pintaValidacion(document.getElementById("dvinputIngresoContrasena"), "");
              this.errorPass = respuesta.mensajeSalida.textoMensaje;
            }
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          if (this.utils.msgPlain({ tipoMensaje: 'confirm', textoMensaje: 'No se ha podido establecer una conexión \n ¿Desea Reintentar?' })) {
            this.ingresoTokenPost();
          } else {
            localStorage.clear();
            this.variablesApi.authorizationToken = "";
            this.variablesApi.equipoSecret = "";
            this.variablesApi.ingreso = new Ingreso()
            this.router.navigate([Paginas.IngresoInicioPage]);
          }
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.errorPass = respuesta.mensajeSalida.textoMensaje;

        }
      });
    } else {
      this.utils.hideSpinner("Interno");
      this.utils.pintaValidacion(document.getElementById("dvinputIngresoContrasena"), this.inputIngresoContrasena);
      this.errorPass = "Contraseña no Valida";
    }
  }

  appMatchContrasena() {
    if (this.inputIngresoContrasena.length < 4) {
      this.utils.pintaValidacion(document.getElementById("dvinputIngresoContrasena"), "");
      // this.utils.msgPlain({ tipoMensaje: "alert", tituloMensaje: "Error de Ingreso", textoMensaje: "El Pin debe tener 4 digitos" });
      this.errorPass = "El Pin debe tener 4 digitos";
    } else {
      this.utils.pintaValidacion(document.getElementById("dvinputIngresoContrasena"), "123");
    }
  }

  reintentar(action, origen) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    this.maquinaIngreso.reintentarIngreso(action, origen).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, origen, action);
        });
      }
    });
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }

    this.maquinaIngreso.ingresoTelefonoId(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, Paginas.ErrorSC, action);
        });
      }
    });
  }

  modalActions(accion, origen, action) {
    switch (accion) {
      case OpcionesModal.REINTENTAR:
        this.reintentar(action, origen);
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

}
