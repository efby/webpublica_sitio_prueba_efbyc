import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoLoginPersistenteMuevoComponent } from './nuevo-login-persistente-muevo.component';

describe('NuevoLoginPersistenteMuevoComponent', () => {
  let component: NuevoLoginPersistenteMuevoComponent;
  let fixture: ComponentFixture<NuevoLoginPersistenteMuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoLoginPersistenteMuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoLoginPersistenteMuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
