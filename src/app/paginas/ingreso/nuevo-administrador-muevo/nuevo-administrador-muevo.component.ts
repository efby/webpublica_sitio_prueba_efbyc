import { Component, OnInit } from '@angular/core';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { AccionTipo, NavegacionTipo, OpcionesModal } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-nuevo-administrador-muevo',
  templateUrl: './nuevo-administrador-muevo.component.html',
  styleUrls: ['./nuevo-administrador-muevo.component.css']
})

export class NuevoAdministradorMuevoComponent implements OnInit {

  _btnSiguiente: boolean = true;
  _muestraTelefono: boolean = true;
  _validado: boolean = false;
  inputNroTelefono: string = "";
  inputValidaCodigo1: string;
  inputValidaCodigo2: string;
  inputValidaCodigo3: string;
  inputNombre: string = "";
  inputApellido: string = "";
  inputMail: string = "";
  inputRut: string = "";
  inputCdDocumento: string = "";
  isValido: boolean = false;

  errorTelefono: string;
  errorRut: string = "";
  errorSms: string = "";

  constructor(private maquinaIngreso: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales, private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.variablesApi.ingreso.usuario.telefonoId != null) {
      this.inputNroTelefono = this.variablesApi.ingreso.usuario.telefonoId.substring(3);
    }
    let _valido = parseInt(this.route.snapshot.paramMap.get('valido'));
    if (_valido === 1) {
      this._muestraTelefono = false;
      this._validado = true;
      this._btnSiguiente = false;
      this.isValido = true;
    } else if (isNaN(_valido)) {

    } else {
      this._validado = false;
      this._btnSiguiente = true;
      this._muestraTelefono = true;
      this.isValido = false;
    }
  }

  iniSession() {
    this.router.navigate([Paginas.IngresoInicioPage, "L"]);
  }

  validaAlldatos() {
    if (this.inputNroTelefono !== "" && this.inputNombre !== "" && this.inputApellido !== "" && this.inputRut !== "" && !this._validado) {
      this._btnSiguiente = false;
      this.utils.pintaValidacion(document.getElementById("inputRut"), this.inputRut);
      this.utils.pintaValidacion(document.getElementById("inputNombre"), this.inputNombre);
      this.utils.pintaValidacion(document.getElementById("inputApellido"), this.inputApellido);
      //this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), this.inputNroTelefono);

    } 
    else {
      this.utils.pintaValidacion(document.getElementById("inputRut"), this.inputRut);
      this.utils.pintaValidacion(document.getElementById("inputNombre"), this.inputNombre);
      this.utils.pintaValidacion(document.getElementById("inputApellido"), this.inputApellido);
      //this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), this.inputNroTelefono);
      this._btnSiguiente = true;
    }
  }

  validaMaxInputRut(ingreso: any) {
    let item = ingreso.srcElement;
    let rutValido: boolean = false;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/g, '').replace(/(\..*)\./g, '$1');

    if (this.inputRut == "") {
      this.utils.pintaValidacion(document.getElementById("inputRut"), '');
      this.errorRut = "Campo Requerido.";
      rutValido = false;
    } else if (this.inputRut.length < 7) {
      this.utils.pintaValidacion(document.getElementById("inputRut"), '');
      this.errorRut = "RUT no válido.";
      rutValido = false;
    } else if (!this.utils.validateRut(this.inputRut)) {
      this.utils.pintaValidacion(document.getElementById("inputRut"), '');
      this.errorRut = "RUT no válido.";
      rutValido = false;
    } else {
      this.utils.pintaValidacion(document.getElementById("inputRut"), this.inputRut);
      this.validaAlldatos();
      rutValido = true;
    }


    // if (this.isValido && rutValido) {
    //   setTimeout(() => {
    //     this.variablesApi.ingreso.usuario.nombre = this.inputNombre;
    //     this.variablesApi.ingreso.usuario.apellido = this.inputApellido;
    //     this.variablesApi.ingreso.usuario.usuarioId = this.inputRut.replace(/\./g, '').replace(/\-/g, '');
    //     this.router.navigate([Paginas.MuevoNuevoMail]);
    //   }, 1000);
    // }
  }

  avanzarTelefonoValido() {
    let rutValido: boolean = false;
    if (!this.utils.validateRut(this.inputRut)) {
      this.utils.pintaValidacion(document.getElementById("inputRut"), '');
      this.errorRut = "RUT no válido.";
      rutValido = false;
    } else {
      this.utils.pintaValidacion(document.getElementById("inputRut"), this.inputRut);
      this.validaAlldatos();
      rutValido = true;
    }

    if (this.isValido && rutValido) {
      setTimeout(() => {
        this.variablesApi.ingreso.usuario.nombre = this.inputNombre;
        this.variablesApi.ingreso.usuario.apellido = this.inputApellido;
        this.variablesApi.ingreso.usuario.usuarioId = this.inputRut.replace(/\./g, '').replace(/\-/g, '');
        this.router.navigate([Paginas.MuevoNuevoMail]);
      }, 1000);
    }
  }

  validaMaxInputSMS(ingreso: any) {
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');

    if (item.value.length == item.maxLength) {
      switch (item.id) {
        case "inputValidaCodigo1":
          document.getElementById("inputValidaCodigo2").focus();
          break;
        case "inputValidaCodigo2":
          document.getElementById("inputValidaCodigo3").focus();
          break;
        case "inputValidaCodigo3":
          this.ingresoCodigoSMS();
          break;
        default:
          break;
      }
    } else if (item.value.length == 0) {
      switch (item.id) {
        case "inputValidaCodigo3":
          document.getElementById("inputValidaCodigo2").focus();
          break;
        case "inputValidaCodigo2":
          document.getElementById("inputValidaCodigo1").focus();
          break;
        default:
          break;
      }
    }
  }

  validaMaxInput(ingreso: any) {
    this.errorTelefono=null
    let item = ingreso.srcElement;
    if (item.value.length > item.maxLength) {
      item.value = item.value.slice(0, item.maxLength);
    }
    item.value = item.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');

    if (item.id == "inputRut") {
      if (item.value.length == 9) {
        this.avanzarTelefonoValido()
      } else {
        this.avanzarTelefonoValido()
      }
    }
  }

  validaMaxInputText(ingreso: any) {
    this.utils.validaMaxInputText(ingreso);
  }

  // validaMaxInputMail(ingreso: any) {
  //   this.utils.validaMaxInputMail(ingreso);
  //   //this.validateEmail();
  // }

  mostrarAnimacion() {
    this.utils.pintaValidacion(document.getElementById("inputNroTelefono"), "");
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "");
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "");
    this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "");
  }

  modificarTelefono() {
    this._muestraTelefono = true;
    this.inputNroTelefono=null
  }

  solicitarSMS() {
    this.validaAlldatos();
    
    if (!this._btnSiguiente && this.inputNroTelefono !== "" && this.inputNroTelefono.length > 7 && this.utils.validateRut(this.inputRut) && this.inputRut.length >= 7) {
      this.ingresoTelefonoId();
      //this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), "123");
    } else {
      if (this.inputRut == "") {
        this.utils.pintaValidacion(document.getElementById("inputRut"), '');
        this.errorRut = "Campo Requerido";
      } else if (this.inputRut.length < 7) {
        this.utils.pintaValidacion(document.getElementById("inputRut"), '');
        this.errorRut = "Rut no válido";
      }
      else if (!this.utils.validateRut(this.inputRut)) {
        this.utils.pintaValidacion(document.getElementById("inputRut"), '');
        this.errorRut = "Rut no válido";
        if (this.inputNroTelefono.length <= 7) {
         // this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), "");
          this.errorTelefono = "El número de teléfono debe tener 8 digitos";
        }
      } 
      else if(this._muestraTelefono && this.inputNroTelefono == ""){
       // this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), '');
        this.errorTelefono = "Campo Requerido";
      }
      else if(this._muestraTelefono && this.inputNroTelefono != "" && this.inputNroTelefono.length < 8){
        //this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), "");
        this.errorTelefono = "El número de teléfono debe tener 8 digitos";
      }
      else if (this._btnSiguiente) {

      } 
      // else {
      //   this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), "");
      //   this.errorTelefono = "El número de teléfono debe tener 8 digitos";
      // }

    }
   
  }


  ingresoTelefonoId() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputNroTelefono !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_CLICK,
        datos: {
          telefonoId: "569" + this.inputNroTelefono
        }
      };
      this.maquinaIngreso.ingresoTelefonoId(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          this._muestraTelefono = false;
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response, Paginas.IngresoTelefonoId, action);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.errorTelefono = respuesta.mensajeSalida.textoMensaje;
          // this.utils.msgPlain(respuesta.mensajeSalida);
          this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), '');
        }
      });
    } else {
      this.utils.hideSpinner("Interno");
      this.utils.pintaValidacion(document.getElementById("DvnroTelefono"), this.inputNroTelefono);
    }
  }

  recibirLlamada() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_RECIBIR_LLAMADA,
      datos: {
        telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
      }
    };
    this.maquinaIngreso.ingresoCodigoSMS(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        this.inputValidaCodigo1 = "";
        this.inputValidaCodigo2 = "";
        this.inputValidaCodigo3 = "";
        if (actualUrl !== respuesta.paginaRespuesta) {
          if (respuesta.paginaRespuesta !== Paginas.MuevoNuevoAdministrador) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, Paginas.IngresoTelefonoId, action);
        });
      }
    });
  }

  ingresoCodigoSMS() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputValidaCodigo1 !== "" && this.inputValidaCodigo2 !== "" && this.inputValidaCodigo3 !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_VALIDAR,
        datos: {
          telefonoId: this.variablesApi.ingreso.usuario.telefonoId,
          smsToken: this.variablesApi.ingreso.usuario.smsToken,
          smsValor: this.inputValidaCodigo1 + this.inputValidaCodigo2 + this.inputValidaCodigo3
        }
      };
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "123");
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "123");
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "123");
      this.errorSms = "";
      this.maquinaIngreso.ingresoCodigoSMS(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (respuesta.paginaRespuesta == Paginas.ErrorNOK) {
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "");
            document.getElementById("msgInicialSms").style.display = 'none';
            this.errorSms = respuesta.mensajeSalida.textoMensaje;
          } else if (actualUrl !== respuesta.paginaRespuesta) {
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "123");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "123");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "123");
            this.errorSms = "";
            if (respuesta.paginaRespuesta === Paginas.MuevoNuevoLogin) {
              this.router.navigate([respuesta.paginaRespuesta, 1]);
            } else {
              this.router.navigate([respuesta.paginaRespuesta]);
            }
          }
          else {
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), "123");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), "123");
            this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), "123");
            this.errorSms = "";
            this._muestraTelefono = false;
            this._validado = true;
            this._btnSiguiente = true;
            // this.validaAlldatos();
            this.ngOnInit();
            setTimeout(() => {
              this.ingresoAvanzar();
            }, 1000);

          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response, Paginas.IngresoSMSValor, action);
          });
        } else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.ANIMACION) {
          this.utils.hideSpinner("Interno");
          this.mostrarAnimacion();
        }
      });
    } else {
      this.utils.hideSpinner("Interno");
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo1"), this.inputValidaCodigo1);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo2"), this.inputValidaCodigo2);
      this.utils.pintaValidacion(document.getElementById("inputValidaCodigo3"), this.inputValidaCodigo3);
    }
  }

  reintentar(action, origen) {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    this.maquinaIngreso.reintentarIngreso(action, origen).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, origen, action);
        });
      }
    });
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }

    this.maquinaIngreso.ingresoTelefonoId(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response, Paginas.ErrorSC, action);
        });
      }
    });
  }

  ingresoAvanzar() {
    this.variablesApi.ingreso.usuario.telefonoId = "56" + this.inputNroTelefono;
    this.variablesApi.ingreso.usuario.nombre = this.inputNombre;
    this.variablesApi.ingreso.usuario.apellido = this.inputApellido;
    this.variablesApi.ingreso.usuario.usuarioId = this.inputRut.replace(/\./g, '').replace(/\-/g, '');
    this.variablesApi.ingreso.usuario.numeroDocumento = "";

    this.router.navigate([Paginas.MuevoNuevoMail]);
  }

  modalActions(accion, origen, action) {
    switch (accion) {
      case OpcionesModal.REINTENTAR:
        this.reintentar(action, origen);
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

}
