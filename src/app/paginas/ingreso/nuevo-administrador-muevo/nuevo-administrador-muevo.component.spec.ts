import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoAdministradorMuevoComponent } from './nuevo-administrador-muevo.component';

describe('NuevoAdministradorMuevoComponent', () => {
  let component: NuevoAdministradorMuevoComponent;
  let fixture: ComponentFixture<NuevoAdministradorMuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoAdministradorMuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoAdministradorMuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
