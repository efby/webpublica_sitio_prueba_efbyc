import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoIngresoSoporteComponent } from './nuevo-ingreso-soporte.component';

describe('NuevoIngresoSoporteComponent', () => {
  let component: NuevoIngresoSoporteComponent;
  let fixture: ComponentFixture<NuevoIngresoSoporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoIngresoSoporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoIngresoSoporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
