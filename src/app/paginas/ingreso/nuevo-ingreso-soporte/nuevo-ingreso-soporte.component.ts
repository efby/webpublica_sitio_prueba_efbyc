import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Paginas } from 'src/app/globales/paginas';

@Component({
  selector: 'app-nuevo-ingreso-soporte',
  templateUrl: './nuevo-ingreso-soporte.component.html',
  styleUrls: ['./nuevo-ingreso-soporte.component.css']
})
export class NuevoIngresoSoporteComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  nvaCuenta() {
    this.router.navigate([Paginas.IngresoInicioPage, "I"]);
  }

  iniSession() {
    this.router.navigate([Paginas.IngresoInicioPage, "L"]);
  }

}
