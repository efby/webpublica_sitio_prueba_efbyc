import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { slideInAnimation } from 'src/app/route-animation';
import { RouterOutlet } from '@angular/router';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-ingreso',
  templateUrl: './ingreso.component.html',
  styleUrls: ['./ingreso.component.css'],
  animations: [slideInAnimation]
})
export class IngresoComponent implements OnInit {

  interval: any;
  isMobile: boolean = false;

  constructor(private variablesApi: VariablesGenerales, private ref: ChangeDetectorRef) {
    ref.detach();
    this.interval = setInterval(() => {
      this.ref.detectChanges()
      this.detectmob();
    }, 0);
    this.detectmob();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  detectmob() {
    let info = UtilitariosService.getPlatformInfo();
    this.isMobile = info.isMobile;
    
    //this.isMobile = window.innerWidth <= 767;
    // if (window.innerWidth <= 800 || window.innerHeight <= 600) {
    //   this.isMobile = true;
    // } else {
    //   this.isMobile = false;
    // }
    this.variablesApi.esMobile = this.isMobile;
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
