import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoContrasenaComponent } from './ingreso-contrasena.component';

describe('IngresoContrasenaComponent', () => {
  let component: IngresoContrasenaComponent;
  let fixture: ComponentFixture<IngresoContrasenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoContrasenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoContrasenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
