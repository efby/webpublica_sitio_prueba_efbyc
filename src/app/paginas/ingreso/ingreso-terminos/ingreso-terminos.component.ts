import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccionTipo } from 'src/app/globales/enumeradores';
import { Paginas } from 'src/app/globales/paginas';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { IngresoService } from 'src/app/servicios/ingreso/ingreso.service';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';
import { PageBase } from '../../home-muevo/pageBase';

@Component({
    selector: 'app-ingreso-terminos',
    templateUrl: './ingreso-terminos.component.html',
    styleUrls: ['./ingreso-terminos.component.scss']
})
export class IngresoTerminosComponent extends PageBase implements OnInit {
    
    terminosFlag: boolean;
    message: string;
    
    constructor(private variablesApi:VariablesGenerales, public cdr: ChangeDetectorRef, private maquinaIngreso: MaquinaIngreso, private ingresoService: IngresoService, public router: Router, public util: UtilitariosService) {
        super(cdr, util, router);

        this.terminosFlag = false;
        this.message = this.variablesApi.home.usuarioLogIn.terminosMensaje;
    }
    
    ngOnInit() {
        
    }

    nvaCuenta(){
        this.maquinaIngreso.CloseSession();
        this.router.navigate([Paginas.IngresoInicioPage, "I"]);
    }
    
    async next(){

        
        let usuarioObtener = await this.ingresoService.ingresoUsuarioObtener(false, true);
        if(usuarioObtener.estado === 'OK'){
            this.variablesApi.ingreso.usuario.aceptaTerminos = true;
            this.router.navigate([Paginas.HomeMuevo])
        }
        else{
            this._errorCallback(usuarioObtener.data.message);
        }
    }
    
}
