import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoTelefonoIdComponent } from './ingreso-telefono-id.component';

describe('IngresoTelefonoIdComponent', () => {
  let component: IngresoTelefonoIdComponent;
  let fixture: ComponentFixture<IngresoTelefonoIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoTelefonoIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoTelefonoIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
