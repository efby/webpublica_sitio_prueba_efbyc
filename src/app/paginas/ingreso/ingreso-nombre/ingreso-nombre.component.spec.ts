import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoNombreComponent } from './ingreso-nombre.component';

describe('IngresoNombreComponent', () => {
  let component: IngresoNombreComponent;
  let fixture: ComponentFixture<IngresoNombreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoNombreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoNombreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
