import { Component, OnInit } from '@angular/core';
import { MaquinaIngreso } from 'src/app/maquinas/maquina-ingreso';
import { Router } from '@angular/router';
import { VariablesGenerales } from 'src/app/globales/variables-generales';
import { AccionTipo, OpcionesModal, NavegacionTipo } from 'src/app/globales/enumeradores';
import { RequestTipo } from 'src/app/models/respuestas-tipo';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
  selector: 'app-ingreso-nombre',
  templateUrl: './ingreso-nombre.component.html',
  styleUrls: ['./ingreso-nombre.component.css']
})
export class IngresoNombreComponent implements OnInit {
  inputNombre: string;
  inputApellido: string;

  constructor(private ingresoFunciones: MaquinaIngreso, private router: Router, private utils: UtilitariosService, private variablesApi: VariablesGenerales) { }

  ngOnInit() {
    this.inputNombre = this.variablesApi.ingreso.usuario.nombre;
    this.inputApellido = this.variablesApi.ingreso.usuario.apellido;
  }

  mostrarAnimacion() {
    this.utils.pintaValidacion(document.getElementById("inputNombre"), "");
    this.utils.pintaValidacion(document.getElementById("inputApellido"), "");
  }

  ingresoNombre() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputNombre !== "" && this.inputApellido !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_SIGUIENTE,
        datos: {
          nombre: this.inputNombre,
          apellido: this.inputApellido
        }
      };
      this.ingresoFunciones.ingresoNombre(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputNombre"), this.inputNombre);
      this.utils.pintaValidacion(document.getElementById("inputApellido"), this.inputApellido);
      this.utils.hideSpinner("Interno");
    }
  }

  reintentar() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    if (this.inputNombre !== "" && this.inputApellido !== "") {
      let action: RequestTipo = {
        accion: AccionTipo.ACCION_BOTON_SIGUIENTE,
        datos: {
          nombre: this.inputNombre,
          apellido: this.inputApellido
        }
      };
      this.ingresoFunciones.ingresoNombre(action).then(respuesta => {
        if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
          this.utils.hideSpinner("Interno");
          if (actualUrl !== respuesta.paginaRespuesta) {
            this.router.navigate([respuesta.paginaRespuesta]);
          }
          else {
            this.ngOnInit();
          }
        }
        else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
          this.utils.hideSpinner("Interno");
          this.utils.openModal(respuesta.modalSalida).then(response => {
            this.modalActions(response);
          });
        }
      });
    } else {
      this.utils.pintaValidacion(document.getElementById("inputNombre"), this.inputNombre);
      this.utils.pintaValidacion(document.getElementById("inputApellido"), this.inputApellido);
      this.utils.hideSpinner("Interno");
    }
  }

  salir() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_SALIR,
      datos: ""
    }
    this.ingresoFunciones.ingresoNombre(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }

  atras() {
    this.utils.showSpinner("Interno");
    let actualUrl = this.router.url.substr(1);
    let action: RequestTipo = {
      accion: AccionTipo.ACCION_ATRAS,
      datos: ""
    }
    this.ingresoFunciones.ingresoNombre(action).then(respuesta => {
      if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.PAGINA) {
        this.utils.hideSpinner("Interno");
        if (actualUrl !== respuesta.paginaRespuesta) {
          this.router.navigate([respuesta.paginaRespuesta]);
        }
        else {
          this.ngOnInit();
        }
      }
      else if (this.variablesApi.ingreso.navegacionTipo === NavegacionTipo.MODAL) {
        this.utils.hideSpinner("Interno");
        this.utils.openModal(respuesta.modalSalida).then(response => {
          this.modalActions(response);
        });
      }
    });
  }


  modalActions(action) {
    switch (action) {
      case OpcionesModal.REINTENTAR:
        this.reintentar();
        break;
      case OpcionesModal.SALIR:
        this.salir();
        break;
      default:
        break;
    }
  }

}
