import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollPaginatorComponent } from './scroll-paginator.component';

describe('ScrollPaginatorComponent', () => {
  let component: ScrollPaginatorComponent;
  let fixture: ComponentFixture<ScrollPaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrollPaginatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollPaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
