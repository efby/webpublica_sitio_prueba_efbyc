import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IPagination } from 'src/app/globales/business-objects';

@Component({
    selector: 'app-scroll-paginator',
    templateUrl: './scroll-paginator.component.html',
    styleUrls: ['./scroll-paginator.component.scss']
})
export class ScrollPaginatorComponent implements OnInit {
    
    @Output()
    onNext = new EventEmitter();

    @Input()
    pagination:IPagination;

    @Input()
    isLoading: boolean;
    
    constructor() {
        // this.pagination = {
        //     currentPage: 0,
        //     pages: 0,
        //     totalRows: 0,
        //     positionFinal: 0,
        //     pagesSize: 0,
        //     rows: 0
        // }
    }
    
    ngOnInit() {

    }
    
    next(){
        this.onNext.emit();
    }
    
}
