import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UtilitariosService } from 'src/app/servicios/utilitarios.service';

@Component({
    selector: 'app-display-error-field',
    templateUrl: './display-error-field.component.html',
    styleUrls: ['./display-error-field.component.scss']
})
export class DisplayErrorFieldComponent implements OnInit {
    
    @Input() control: FormControl;
    @Input() displayName: string;
    @Input() displayError: boolean;
    
    public viewFieldError: boolean;
    
    constructor(private utils:UtilitariosService) { }
    
    ngOnInit() {
        this.viewFieldError = false;
        if(this.displayError)
            console.log(this.control.errors)
    }
    
}
