import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
//import { matRangeDatepickerRangeValue, matRangeDatepickerInputEvent } from 'mat-range-datepicker';

import * as moment from 'moment';
import 'moment/min/locales';
import { MatInput } from '@angular/material';
moment.locale('es')

@Component({
    selector: 'app-muevo-calendar',
    templateUrl: './muevo-calendar.component.html',
    styleUrls: ['./muevo-calendar.component.scss']
})
export class MuevoCalendarComponent implements OnInit {
    
    @Input()
    max: Date;

    @Input()
    defaultStartDate: Date;

    @Input()
    defaultEndDate: Date;

    @Input()
    showFilter: boolean = true;
    
    @Output()
    onSelectDates = new EventEmitter();
    
    @Output()
    onResetDates = new EventEmitter();
    
    @ViewChild('from', {
        read: MatInput
    }) startInput: MatInput;
    
    @ViewChild('to', {
        read: MatInput
    }) endInput: MatInput;
    
    start: IMuevoCalendarDate;
    end: IMuevoCalendarDate;
    
    datesSelecteds: boolean;
    
    //error control
    error: {
        state: boolean,
        message: string
    }
    
    constructor() { 
        this.resetDates();
        
        this.error = {
            state: false,
            message: ''
        }
    }
    
    ngOnInit() {
        if(this.defaultStartDate != null)
            this.start = this.generateMuevoCalendarDate({value: this.defaultStartDate})

        if(this.defaultEndDate != null)
            this.end = this.generateMuevoCalendarDate({value: this.defaultEndDate})
    }
    
    startChange(e, validateAndEmmit:boolean = true){
        this.start = this.generateMuevoCalendarDate(e);
        if(this.validateSelection() && validateAndEmmit)
            this.emittSelection();
    }
    
    endChange(e, validateAndEmmit:boolean = true){
        this.end = this.generateMuevoCalendarDate(e);
        if(this.validateSelection() && validateAndEmmit)
            this.emittSelection();
    }

    SetDates(start:Date, end:Date){
        this.start = this.generateMuevoCalendarDate({value: start});
        this.end = this.generateMuevoCalendarDate({value: end});

        this.startInput.value = this.start.value;
        this.endInput.value = this.end.value;
    }
    
    private validateSelection(): boolean{
        
        if(this.start.value == "" || this.end.value == "")
        return false;
        
        let start = moment(this.start.value);
        let end = moment(this.end.value);
        
        if(start > end){
            this.error = {
                message: 'La fecha de inicio no puede ser mayor a la fecha de termino',
                state: true
            }
            return false;
        }
        
        this.datesSelecteds = true;
        this.clearError();
        return true;
    }
    
    clearDates(){
        this.resetDates();
        this.onResetDates.emit();
    }
    
    private resetDates(){
        this.start = {
            value: '',
            display: ''
        }
        
        this.end = {
            value: '',
            display: ''
        }

        if(this.startInput)
            this.startInput.value = '';
        
        if(this.endInput)
            this.endInput.value = '';
        
        this.datesSelecteds = false;
    }
    
    private clearError(){
        this.error = {
            state: false,
            message: ''
        }
    }
    
    private emittSelection(){
        if(this.start.value === "")
        return;
        if(this.end.value === "")
        return;
        
        this.onSelectDates.emit({
            start: this.start.value,
            end: this.end.value
        })
    }
    
    private generateMuevoCalendarDate(e){

        let res: IMuevoCalendarDate = {
            value: moment(e.value).toISOString(),
            display: this.generateDisplay(e.value)
        }
        return res;
    }
    
    private generateDisplay(date:Date){
        let x = moment(date);
        return `${x.get('date')} ${x.format('MMMM')} de ${x.get('year')}`;
    }
    
}

export class IMuevoCalendarDate{
    value: string;
    display: string;
}
