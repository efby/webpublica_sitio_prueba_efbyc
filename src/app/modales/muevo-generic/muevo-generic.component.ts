import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { OptionsModalDefault } from 'src/app/models/global';

@Component({
    selector: 'app-muevo-generic',
    templateUrl: './muevo-generic.component.html',
    styleUrls: ['./muevo-generic.component.scss']
})
export class MuevoGenericComponent implements OnInit {
    @Input() public modal: OptionsModalDefault;
    @Input() public useCancel: boolean = true;
    @Output() passEntry: EventEmitter<any> = new EventEmitter();

    constructor() { }
    
    ngOnInit() {
    }
    
    response(respuesta: string) {
        switch (respuesta) {
          case "Aceptar":
            this.passEntry.emit(OpcionesModal.ACEPTAR);
            break;
          case "Cancelar":
            this.passEntry.emit(OpcionesModal.CANCELAR);
            break;
          default:
            break;
        }
      }
    
}
