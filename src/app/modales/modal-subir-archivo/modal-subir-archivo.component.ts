import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-modal-subir-archivo',
  templateUrl: './modal-subir-archivo.component.html',
  styleUrls: ['./modal-subir-archivo.component.css']
})
export class ModalSubirArchivoComponent implements OnInit {
  @Input() public modal;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  arrayBuffer: any;
  file: File;

  incomingfile(event) {
    this.file = event.target.files[0];
  }
  planilla: any;


  constructor() { }

  ngOnInit() {

  }

  Upload() {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      this.planilla = workbook.Sheets[first_sheet_name];
    }
    fileReader.readAsArrayBuffer(this.file);
  }

  response(respuesta: string) {
    switch (respuesta) {
      case "OK":
        this.passEntry.emit(this.planilla);
        break;
      default:
        break;
    }
  }
}
