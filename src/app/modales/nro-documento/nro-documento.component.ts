import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nro-documento',
  templateUrl: './nro-documento.component.html',
  styleUrls: ['./nro-documento.component.css']
})
export class NroDocumentoComponent implements OnInit {

  @Input() public modal;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  response(respuesta: string) {
    this.passEntry.emit(respuesta);
  }

}
