import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAceptarInvitacionComponent } from './modal-aceptar-invitacion.component';

describe('ModalAceptarInvitacionComponent', () => {
  let component: ModalAceptarInvitacionComponent;
  let fixture: ComponentFixture<ModalAceptarInvitacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAceptarInvitacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAceptarInvitacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
