import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { VariablesGenerales } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-modal-aceptar-invitacion',
  templateUrl: './modal-aceptar-invitacion.component.html',
  styleUrls: ['./modal-aceptar-invitacion.component.css']
})
export class ModalAceptarInvitacionComponent implements OnInit {
  @Input() public modal;
  @Output() passEntry: EventEmitter<OpcionesModal> = new EventEmitter();

  idInvitacion: number;

  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
    this.idInvitacion = this.modal.idInvitacion;
  }

  response(respuesta: string) {
    switch (respuesta) {
      case "Aceptar":
        this.aceptarInvitacion()
        break;
      case "Rechazar":
        this.rechazarInvitacion();
        break;
      case "Salir":
        this.passEntry.emit(OpcionesModal.SALIR);
        break;
      default:
        break;
    }
  }

  aceptarInvitacion() {
    console.log("Aceptada");
    let invitacion = this.variablesApi.home.invitaciones.filter(r => r.invitacionId = this.idInvitacion);
    this.passEntry.emit(OpcionesModal.OK);
  }
  rechazarInvitacion() {
    console.log("Rechazada");
    let invitacion = this.variablesApi.home.invitaciones.filter(r => r.invitacionId = this.idInvitacion);
    this.passEntry.emit(OpcionesModal.RECHAZAR);
  }

}
