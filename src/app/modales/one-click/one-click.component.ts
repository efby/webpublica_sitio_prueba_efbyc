import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { SafePipe } from "src/app/safe.pipe";

@Component({
  selector: 'app-one-click',
  templateUrl: './one-click.component.html',
  styleUrls: ['./one-click.component.css']
})
export class OneClickComponent implements OnInit {
  @Input() public url;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  yt: string = "Cargando...";

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.yt = '<iframe #iframe class="w-100" src="' + this.url + '" frameborder="0" style="height: 80vh;"></iframe>';
    }, 1000);
  }

  response(respuesta: string) {
    switch (respuesta) {
      case "Aceptar":
        this.passEntry.emit(OpcionesModal.ACEPTAR);
        break;
      case "Cancelar":
        this.passEntry.emit(OpcionesModal.CANCELAR);
        break;
      default:
        break;
    }
  }

}
