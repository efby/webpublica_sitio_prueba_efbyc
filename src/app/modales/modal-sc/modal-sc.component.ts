import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OpcionesModal } from 'src/app/globales/enumeradores';
import { VariablesGenerales, Home, Ingreso } from 'src/app/globales/variables-generales';

@Component({
  selector: 'app-modal-sc',
  templateUrl: './modal-sc.component.html',
  styleUrls: ['./modal-sc.component.css']
})
export class ModalSCComponent implements OnInit {
  @Input() public modal;
  @Output() passEntry: EventEmitter<OpcionesModal> = new EventEmitter();

  constructor(private variablesApi: VariablesGenerales) { }

  ngOnInit() {
  }

  response(respuesta: string) {
    switch (respuesta) {
      case "Reintentar":
        this.passEntry.emit(OpcionesModal.REINTENTAR);
        break;
      case "Salir":
        this.passEntry.emit(OpcionesModal.SALIR);
        break;
      default:
        break;
    }
  }

}
