import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-reintentar-salir',
  templateUrl: './reintentar-salir.component.html',
  styleUrls: ['./reintentar-salir.component.css']
})
export class ReintentarSalirComponent implements OnInit {

  @Input() public modal;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  response(respuesta: string) {
    this.passEntry.emit(respuesta);
  }

}
