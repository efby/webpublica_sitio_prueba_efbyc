
import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
    selector: '[loadingIndicator]'
})
export class LoadingIndicatorDirective {
    
    private ID_SPINNER = 'i-spinner';
    private _disabled: boolean = false;
    private _position: string;
    
    @Input() set position(value:string){
        this._position = value;
    }
    
    @Input() set disableElementOnWork(value: boolean){
        this._disabled = value;
    }
    
    @Input() set loadingIndicator(value: boolean) {
        let removeChild = child => { this.renderer.removeChild(this.eleRef.nativeElement, child); };
        let toggleChilds = (child, visible: boolean) => { this.renderer.setStyle(this.eleRef.nativeElement, 'display', 'none') }

        //toggle de estado (enable/disable) el elemento si es parametrizado
        if(this._disabled)
            this.eleRef.nativeElement.disabled = value;
        
        if(value){
            //crea spinner
            const i = this.renderer.createElement('i');
            this.renderer.setAttribute(i, 'id', this.ID_SPINNER);
            this.renderer.setAttribute(i, 'class', 'fas fa-spin fa-spinner');
            
            //respalda y quita los elementos
            let elements: any[] = [];
            Array.from(this.eleRef.nativeElement.children).forEach((child, i) => {
                //elements.push(child);
                //removeChild(child);
                toggleChilds(child, false);
            });
            
            // //agrega elementos segun position(i, element.childs)
            // if(this._position == POSITION_LOADING_INDICATOR.left.toString()){
            //     this.renderer.setAttribute(i, 'style', "margin-right: 15px")
            //     this.renderer.appendChild(this.eleRef.nativeElement, i);
            //     elements.map(x => {
            //         this.renderer.appendChild(this.eleRef.nativeElement, x);
            //     });
            // }
            // else{
            //     this.renderer.setAttribute(i, 'style', "margin-left: 15px")
            //     elements.map(x => {
            //         this.renderer.appendChild(this.eleRef.nativeElement, x);
            //     });
            //     this.renderer.appendChild(this.eleRef.nativeElement, i);
            // }
            
            
            
            //TESTS: Array.from(this.eleRef.nativeElement.children).forEach((child, i) => {});
        }else{
            //elimina si existe un spinner corriendo
            Array.from(this.eleRef.nativeElement.children).forEach((child, i) => {
                if(this.eleRef.nativeElement.children[i].id == this.ID_SPINNER)
                    //removeChild(child);
                toggleChilds(child, true);
            }); 

            
        }
    }
    
    constructor(
        private eleRef: ElementRef,
        private renderer: Renderer2) { 
            
        }
        
    }
    
    export enum POSITION_LOADING_INDICATOR{
        left = 'left',
        right = 'right'
    }
    