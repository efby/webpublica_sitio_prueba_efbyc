import { Directive, Input, HostBinding, Self, Inject } from '@angular/core';
import { NgControl } from '@angular/forms';
import { VALIDATION_BORDER_CONFIG, ValidationBorderConfig } from './validation-border.config';


@Directive({
    selector: '[validationBorder]'
})
export class ValidationBorderDirective {

    private readonly colors: {
        VALID: any;
        INVALID: any;
        PENDING: any;
        DISABLED: any;
    };

    @Input('force') validationBorderForceToShow: string;
    @Input('appValidationBorderWidth') borderWidth: string;
    @Input('appValidationBorderStyle') borderStyle: string;

    @HostBinding('style.border-style')
    get borderStyleCss() {
        return this.showBorder ? this.borderStyle : null;
    }

    @HostBinding('style.border-width')
    get borderWidthCss() {
        return this.showBorder ? this.borderWidth : null;
    }

    @HostBinding('style.border-color')
    get borderColorCss() {
        //console.log(this.validationBorderForceToShow);
        return this.showBorder && this.colors[this.ngControl.status].enabled ? this.colors[this.ngControl.status].value : null;
    }

    get showBorder() {
        //return this.ngControl.dirty || this.ngControl.touched;
        return this.validationBorderForceToShow || (!this.ngControl.valid && this.ngControl.dirty)
    }

    constructor(
        @Self() private ngControl: NgControl,
        @Inject(VALIDATION_BORDER_CONFIG) config: ValidationBorderConfig
    ) {
        this.colors = config.colors;
        this.borderWidth = config.borderWidth;
        this.borderStyle = config.borderStyle;
    }

}