import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidationBorderConfig, VALIDATION_BORDER_CONFIG } from './validation-border.config';
import { FormsModule } from '@angular/forms';
import { ValidationBorderDirective } from './validation-border.directive';

@NgModule({
    declarations: [ValidationBorderDirective],
    imports: [
        CommonModule, FormsModule
    ], 
    exports: [ValidationBorderDirective]
})
export class ValidationBorderModule { 
    static forRoot(config: ValidationBorderConfig): ModuleWithProviders {
        return {
            ngModule: ValidationBorderModule,
            providers: [{ provide: VALIDATION_BORDER_CONFIG, useValue: config }]
        };
    }
}
