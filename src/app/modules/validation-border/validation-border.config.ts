import { InjectionToken } from '@angular/core';

export interface ValidationBorderConfig {
    borderWidth: string;
    borderStyle: string;
    
    colors: {
        VALID: { value: string, enabled: boolean },
        INVALID: { value: string, enabled: boolean }
        PENDING: { value: string, enabled: boolean }
        DISABLED: { value: string, enabled: boolean }
    }
}

export const VALIDATION_BORDER_CONFIG = new InjectionToken<ValidationBorderConfig>('Validation Border Config');